#!/bin/bash

# switch to buildx
docker buildx prune -a ; docker buildx create --use

# clear if need
docker buildx prune -a

# --- projects
# MYCity - just /publish compile
# docker buildx build -f ./mycity2bin.Dockerfile -t bizit2/mycity2bin:latest . --push
docker buildx build -f ./mycity2bin.Dockerfile -t registry.biz-it.ru/mycity2bin:latest . --push
# MYCity
# docker buildx build --platform linux/amd64,linux/arm64 -f ./mycity2.Dockerfile -t bizit2/mycity2:latest . --push
docker buildx build --platform linux/amd64,linux/arm64 -f ./mycity2.Dockerfile -t registry.biz-it.ru/mycity2:latest . --push
# delo-integration
docker buildx build --platform linux/amd64,linux/arm64 -f ./delo-integration.Dockerfile -t bizit2/mycity2-delo:latest . --push



docker buildx build -f ./mycity2bin.Dockerfile -t bizit2/mycity2bin:latest . --push ; docker buildx build --platform linux/amd64,linux/arm64 -f ./mycity2.Dockerfile -t bizit2/mycity2:latest . --push