using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.DELO.RestApiManager;
using BIT.DELO.RestApiManager.Models;
using BIT.MyCity.ApiInterface;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Gc;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using log4net;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Subsystems.DeloAppealGetter
{
    public sealed class SubsystemNotDbAppeals : IExternalSubsystem
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(SubsystemNotDbAppeals));
        private readonly IServiceProvider _serviceProvider;
        private readonly IServiceScopeFactory _scopeFactory;

        public const string BaseSystemKey = "delo_not_db_appeals";
        internal static readonly string DefaultName = "Обращения граждан из системы Дело";
        internal static readonly string DefaultDescription = "Получает обращения граждан из системы Дело по номеру и дате";

        public string SystemKey => BaseSystemKey;

        public bool IsEnable
        {
            get
            {
                var settingsManager =
                    ServiceProviderServiceExtensions.GetRequiredService<SettingsManager>(_serviceProvider);

                return settingsManager.SettingValue<bool>($"subsystem.{SettingsKey.Enable}");
            }
        }

        public IEnumerable<Settings> DefaultSettings
        {
            get
            {
                var defaultSettings = SubsystemDefaultSettings;

                var result = defaultSettings as Settings[] ?? defaultSettings.ToArray();

                foreach (var settings in result)
                {
                    settings.Key = $"subsystem.{settings.Key}";
                }

                return result;
            }
        }

        public IEnumerable<ClaimInfoModel> ClaimInfos => Array.Empty<ClaimInfoModel>();

        public SubsystemNotDbAppeals(IServiceProvider serviceProvider, IServiceScopeFactory scopeFactory)
        {
            _serviceProvider = serviceProvider;
            _scopeFactory = scopeFactory;
        }
        
        public IEnumerable<Claim> GetCitizenSubsystemClaims(User user)
        {
            return new List<Claim>();
        }

        public async Task SendNewMessageEmail(Message message)
        { }

        public static class SettingsKey
        {
            public static readonly string Settings = $"{BaseSystemKey}.";
            public static readonly string Enable = $"{Settings}enable.";
            public static readonly string Name = $"{Settings}name.";
            public static readonly string Description = $"{Settings}description.";
            public static readonly string OnlyAuthorized = $"{Settings}only_auth.";
            public static readonly string HostUrl = $"{Settings}host_url.";
            public static readonly string ApiType = $"{Settings}api_type.";
            public static readonly string HostLogin = $"{Settings}host_login.";
            public static readonly string HostPassword = $"{Settings}host_password.";
            public static readonly string AnswerDocGroups = $"{Settings}answer_dg_due.";
            public static readonly string AnswerLinks = $"{Settings}answer_link_isn.";
            public static readonly string FileLifeTime = $"{Settings}file_life_time.";
        }

        private static IEnumerable<Settings> SubsystemDefaultSettings => new[]
        {
            new Settings
            {
                Key = SettingsKey.Enable,
                Description = "Активирован",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKey.Name,
                Description = "Название",
                Value = DefaultName,
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKey.Description,
                Description = "Описание",
                Value = DefaultDescription,
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKey.OnlyAuthorized,
                Description = "Только авторизованные",
                Value = "true",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKey.ApiType,
                Description = "Тип API",
                Value = ApiType.Rest.ToString(),
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(ApiType).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Values = new SettingsValueItem[]
                    {
                        new SettingsValueItem<ApiType>(ApiType.Rest, "Дело 22"),
                        new SettingsValueItem<ApiType>(ApiType.OData, "Дело 24")
                    },
                Weight = 5
            },
            new Settings
            {
                Key = SettingsKey.HostUrl,
                Description = "URL хоста Rest Api",
                Value = "http://",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 6
            },
            new Settings
            {
                Key = SettingsKey.HostLogin,
                Description = "Логин авторизации",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 7
            },
            new Settings
            {
                Key = SettingsKey.HostPassword,
                Description = "Пароль авторизации",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 8
            },
            new Settings
            {
                Key = SettingsKey.AnswerDocGroups,
                Description = "DUE групп документов ответа (разделитель '|')",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 9
            },
            new Settings
            {
                Key = SettingsKey.AnswerLinks,
                Description = "ISN связки ответа (разделитель '|')",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 10
            },
            new Settings
            {
                Key = SettingsKey.FileLifeTime,
                Description = "Время жизни файлов, мин (10-60)",
                Value = "20",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(int).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 11
            }
        };

        public void AddGqlQueryFields(ObjectGraphType<object> gql)
        {
            gql.Field<PaginationType<WebApi.Types.ExternalSystemAppeal, Model.ExternalSystemAppeal>>(
                name: nameof(Model.ExternalSystemAppeal).ToJsonFormat(),
                description: "Поиск обращений во внешней системе",
                arguments: new QueryArguments(
                    new QueryArgument<StringGraphType> { Name = "appealNumber", Description = "Номер обращения" },
                    new QueryArgument<DateGraphType> { Name = "appealDate", Description = "Дата обращения" }),
                resolve: GetExtDbAppealsPgAsync);
        }

        private async Task<Pagination<Model.ExternalSystemAppeal>> GetExtDbAppealsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                var number = context.GetArgument<string>("appealNumber");
                var date = context.GetArgument<DateTime>("appealDate");

                using var scope = _scopeFactory.CreateScope();
                var manager = scope.ServiceProvider.GetRequiredService<AppealNotDbManager>();
                var result = await manager.GetExternalSystemAppeal(number, date);
                return result;
            }
            catch (Exception ex)
            {
                context.ReportErrorMessage(ex, _log);
                return null;
            }
        }

        public void AddGqlMutationFields(ObjectGraphType gql)
        { }

        public void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<AppealNotDbManager>();

            services.AddDeloRestApi((sp, options) =>
            {
                var settingsManager = sp.GetRequiredService<SettingsManager>();

                options.CountSendThreads = 1;
                options.HostUrl =
                    settingsManager.SettingValue<string>($"subsystem.{SubsystemNotDbAppeals.SettingsKey.HostUrl}");
                options.ApiType =  settingsManager.SettingValue<ApiType>($"subsystem.{SubsystemNotDbAppeals.SettingsKey.ApiType}");
                options.ApplicationName = $"AppealNotDbManager_{DateTime.Now.Ticks}";
            });

            services.AddTransient<IGcCleaner, FileCleaner>();
        }
    }
}
