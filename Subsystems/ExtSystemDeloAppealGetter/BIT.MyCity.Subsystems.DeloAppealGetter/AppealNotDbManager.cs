using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.DELO.RestApiManager.ApiModels;
using BIT.DELO.RestApiManager.Interfaces;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Pagination;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Subsystems.DeloAppealGetter
{
    public sealed class AppealNotDbManager
    {
        //private readonly ILog _log = LogManager.GetLogger(typeof(AppealManager));

        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        private readonly SettingsManager _sm;

        public AppealNotDbManager(IServiceProvider serviceProvider, DocumentsContext ctx, UserManager um, SettingsManager sm)
        {
            _serviceProvider = serviceProvider;
            _ctx = ctx;
            _um = um;
            _sm = sm;
        }


        public async Task<Pagination<ExternalSystemAppeal>> GetExternalSystemAppeal(string appealNumber, DateTime appealDate)
        {
            CheckAvailableFind();

            using var restApiManager = _serviceProvider.GetRequiredService<IRestApiManager>();

            await OpenSession(restApiManager);

            var findRcArray = (await FindExternalAppeals(restApiManager, appealNumber, appealDate))
                .ToArray();

            var result = new Pagination<ExternalSystemAppeal>
            {
                Total = findRcArray.Length
            };

            var resultItems = new List<ExternalSystemAppeal>(findRcArray.Length);

            foreach (var docRc in findRcArray)
            {
                var item = new ExternalSystemAppeal(docRc.ISN_DOC, docRc.FREE_NUM, docRc.DOC_DATE);

                if (docRc.REF_LETTER_List.Any())
                {
                    var citizen = await GetCitizen(restApiManager,
                        docRc.REF_LETTER_List
                            .OrderBy(el=>el.ORDERNUM)
                            .First().ISN_CITIZEN);

                    item.CitizenName = citizen?.CITIZEN_SURNAME;
                }

                if (docRc.REF_RUBRIC_List.Any())
                {
                    var rubrics = (await FindRubricsAsync(restApiManager, docRc.REF_RUBRIC_List))
                        .ToDictionary(el=>el.DUE);

                    foreach (var refRubric in docRc.REF_RUBRIC_List)
                    {
                        var status = Convert.ToString(refRubric.AR_RUBRIC_VALUE_List.FirstOrDefault()?["EOS_SSTU_STATUS"]);
                        if (string.IsNullOrWhiteSpace(status))
                        {
                            continue;
                        }

                        item.Rubrics.Add(new ExternalSystemAppealRubric(rubrics[refRubric.DUE_RUBRIC].CLASSIF_NAME, status));
                    }
                }

                resultItems.Add(item);
            }

            result.Items = resultItems;

            await restApiManager.SessionCloseAsync();

            return result;
        }

        //private async Task<Rubric> GetRubric(DOC_RC docRc)
        //{
        //    Rubric rubric = null;

        //    if (docRc.DOC_WHO_List == null)
        //    {
        //        return null;
        //    }

        //    var hasRubrics = (await _ctx.Rubrics
        //            .Where(el => el.Rubricator2Subsystems.Any(x =>
        //                !x.Subsystem.Deleted && x.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey))
        //            .ToArrayAsync())
        //        .OrderByDescending(el => el.ExtId.Split('.').Length)
        //        .ToArray();

        //    foreach (var who in docRc.DOC_WHO_List)
        //    {
        //        rubric = hasRubrics
        //            .FirstOrDefault(el => who.DUE_PERSON.StartsWith(el.ExtId));

        //        if (rubric != null)
        //        {
        //            break;
        //        }
        //    }

        //    return rubric;
        //}

        //private async Task<Topic> GetTopic(DOC_RC docRc)
        //{
        //    Topic topic = null;

        //    if (docRc.REF_RUBRIC_List != null)
        //    {
        //        var hasTopic = (await _ctx.Topics
        //                .Where(el => el.Rubricator2Subsystems.Any(x =>
        //                    !x.Subsystem.Deleted && x.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey))
        //                .ToArrayAsync())
        //            .OrderByDescending(el => (el.ExtId?.Split('.').Length ?? 0))
        //            .ToArray();

        //        foreach (var rRubric in docRc.REF_RUBRIC_List)
        //        {
        //            topic = hasTopic
        //                .FirstOrDefault(el => rRubric.DUE_RUBRIC.StartsWith(el.ExtId));

        //            if (topic != null)
        //            {
        //                break;
        //            }
        //        }
        //    }

        //    return topic;
        //}

        //private async Task<IEnumerable<Message>> GetAnswers(IRestApiManager restApiManager, IEnumerable<CITIZEN> citizens, IReadOnlyCollection<REF_LINK> refLinkList)
        //{
        //    if (refLinkList == null)
        //    {
        //        return Array.Empty<Message>();
        //    }

        //    var linksIsnList =
        //        _sm.SettingValue<string>(
        //                $"subsystem.{SubsystemNotDbAppeals.SettingsKey.AnswerLinks}")?
        //            .Split('|')
        //            .Where(el => !string.IsNullOrWhiteSpace(el))
        //            .Select(el => int.Parse(el.Trim()))
        //            .ToArray()
        //        ?? Array.Empty<int>();

        //    var testLinks = (linksIsnList.Any()
        //        ? refLinkList.Where(el => linksIsnList.Contains(el.ISN_CLLINK))
        //        : refLinkList)
        //        .Where(el => el.KIND_LINKED_DOC == 3)
        //        .ToArray();

        //    if (!testLinks.Any())
        //    {
        //        return Array.Empty<Message>();
        //    }

        //    var citizensIsn = citizens
        //        .Select(el => el.ISN_CITIZEN)
        //        .ToArray();

        //    var linkedRcList = (await FindRcByIdentifierAsync(restApiManager, testLinks))
        //        .Where(el => el.REF_FILE_List.Any()
        //                     && el.REF_SEND_List.Any(x =>
        //                         x.ISN_CITIZEN.HasValue && citizensIsn.Contains(x.ISN_CITIZEN.Value)));

        //    var needDocGroups = _sm.SettingValue<string>(
        //                                $"subsystem.{SubsystemNotDbAppeals.SettingsKey.AnswerDocGroups}")?
        //                            .Split('|')
        //                            .Where(el => !string.IsNullOrWhiteSpace(el))
        //                            .ToArray()
        //                        ?? Array.Empty<string>();

        //    if (needDocGroups.Any())
        //    {
        //        linkedRcList = linkedRcList
        //            .Where(el => needDocGroups.Any(x => el.DUE_DOCGROUP.StartsWith(x)));
        //    }

        //    var result = new List<Message>();

        //    foreach (var docRc in linkedRcList)
        //    {
        //        var files = (await LoadRcFiles(restApiManager, docRc.REF_FILE_List))
        //            .ToList();

        //        result.Add(new Message
        //        {
        //            Id = -1,
        //            Kind = MessageKindEnum.SystemUserMessage,
        //            IsNode = false,
        //            Text = docRc.ANNOTAT,
        //            CreatedAt = docRc.DOC_DATE,
        //            AttachedFiles = files
        //        });
        //    }

        //    return result;
        //}

        //private static async Task<IEnumerable<DOC_RC>> FindRcByIdentifierAsync(IRestApiManager restApiManager,
        //    IEnumerable<REF_LINK> refLinks)
        //{
        //    var linkedRcIsn = refLinks
        //        .Where(el => el.ISN_LINKED_DOC.HasValue)
        //        .Select(el => (object)el.ISN_LINKED_DOC.Value);

        //    var findAnswer = await restApiManager.FindByIdentifiersAsync<DOC_RC>(linkedRcIsn.ToList(),
        //        new List<string>
        //        {
        //            nameof(DOC_RC.REF_SEND_List),
        //            nameof(DOC_RC.REF_FILE_List)
        //        });

        //    if (!findAnswer.Success)
        //    {
        //        throw new Exception($"Ошибка поиска во внешней системе. {findAnswer.ErrorMessage}");
        //    }

        //    return findAnswer.Entities;
        //}

        private static async Task<IEnumerable<RUBRIC_CL>> FindRubricsAsync(IRestApiManager restApiManager, IReadOnlyCollection<REF_RUBRIC> refRubricList)
        {
            if (refRubricList == null || !refRubricList.Any())
            {
                return new List<RUBRIC_CL>();
            }

            var depKeys = refRubricList.Select(el => el.DUE_RUBRIC);

            var findAnswer = await restApiManager.FindByIdentifiersAsync<RUBRIC_CL>(depKeys.ToList());

            if (!findAnswer.Success)
            {
                throw new Exception($"Ошибка поиска во внешней системе. {findAnswer.ErrorMessage}");
            }

            return refRubricList
                .Join(findAnswer.Entities,
                    rr => rr.DUE_RUBRIC,
                    ent => ent.DUE,
                    (rr, ent) => new { rr, ent })
                .OrderBy(el => el.rr.ORDERNUM)
                .Select(el => el.ent)
                .ToList();
        }

        //private static async Task<IEnumerable<DEPARTMENT>> FindWhoAsync(IRestApiManager restApiManager, IReadOnlyCollection<DOC_WHO> refSendList)
        //{
        //    if (refSendList == null || !refSendList.Any())
        //    {
        //        return new List<DEPARTMENT>();
        //    }

        //    var depKeys = refSendList.Select(el => el.DUE_PERSON);

        //    var findAnswer = await restApiManager.FindByIdentifiersAsync<DEPARTMENT>(depKeys.ToList());

        //    if (!findAnswer.Success)
        //    {
        //        throw new Exception($"Ошибка поиска во внешней системе. {findAnswer.ErrorMessage}");
        //    }

        //    return findAnswer.Entities;
        //}

        private void CheckAvailableFind()
        {
            var extDbAppealEnabled = _sm.SettingValue<bool>($"subsystem.{SubsystemNotDbAppeals.SettingsKey.Enable}");

            if (!extDbAppealEnabled)
            {
                throw new Exception("Просмотр обращений во внешней системе отключен.");
            }

            var onlyFoAuthorized = _sm.SettingValue<bool>($"subsystem.{SubsystemNotDbAppeals.SettingsKey.OnlyAuthorized}");

            if (onlyFoAuthorized && !_um.ClaimsPrincipalId().HasValue)
            {
                throw new Exception(
                    "Только авторизованные пользователи могут просматривать обращения во внешней системе.");
            }
        }

        private async Task OpenSession(IRestApiManager restApiManager)
        {
            var login = _sm.SettingValue<string>($"subsystem.{SubsystemNotDbAppeals.SettingsKey.HostLogin}");
            var password = _sm.SettingValue<string>($"subsystem.{SubsystemNotDbAppeals.SettingsKey.HostPassword}");
            var loginAnswer = await restApiManager.SessionOpenAsync(
                login,
                password);

            if (!loginAnswer.Success)
            {
                throw new Exception($"Не удалось подключиться к внешней системе. {loginAnswer.ErrorMessage}");
            }
        }

        private static async Task<IEnumerable<DOC_RC>> FindExternalAppeals(IRestApiManager restApiManager, string appealNumber, DateTime appealDate)
        {
            var findAnswer = await restApiManager.FindAsync<DOC_RC>(
                new Dictionary<string, string>
                {
                    {nameof(DOC_RC.FREE_NUM), appealNumber.ToUpper()},
                    {nameof(DOC_RC.DOC_DATE), $"{appealDate:dd/MM/yyyy}"},
                    {nameof(DOC_RC.KIND_DOC), "2"}
                },
                new[]
                {
                    nameof(DOC_RC.REF_LETTER_List),
                    $"{nameof(DOC_RC.REF_RUBRIC_List)}/{nameof(REF_RUBRIC.AR_RUBRIC_VALUE_List)}"
                    //nameof(DOC_RC.REF_FILE_List),
                    //nameof(DOC_RC.DOC_WHO_List),
                    //nameof(DOC_RC.REF_LINK_List)
                });

            if (!findAnswer.Success)
            {
                throw new Exception($"Ошибка поиска во внешней системе. {findAnswer.ErrorMessage}");
            }

            return findAnswer.Entities;
        }

        private static async Task<CITIZEN> GetCitizen(IRestApiManager restApiManager, long isn)
        {
            var findAnswer = await restApiManager.FindAsync<CITIZEN>(
                new Dictionary<string, string>
                {
                    {nameof(CITIZEN.ISN_CITIZEN), isn.ToString()}
                });

            if (!findAnswer.Success)
            {
                throw new Exception($"Ошибка поиска граждан во внешней БД. {findAnswer.ErrorMessage}");
            }

            return findAnswer.Entities.SingleOrDefault();
        }

        //private async Task<IEnumerable<AttachedFile>> LoadRcFiles(IRestApiManager restApiManager, IEnumerable<REF_FILE> refFile)
        //{
        //    var result = new List<AttachedFile>();

        //    var fm = _serviceProvider.GetRequiredService<AttachedFileManager>();

        //    foreach (var file in refFile.Where(el => el.IS_HIDDEN != 1))
        //    {
        //        var getAnswer = await restApiManager.GetFileAsync(file.ISN_REF_FILE);

        //        if (!getAnswer.Success)
        //        {
        //            throw new Exception($"Ошибка получения файла из внешней системы. {getAnswer.ErrorMessage}");
        //        }

        //        var fileContent = getAnswer.FileContent;

        //        result.Add(await fm.CreateDbAsync(
        //            new AttachedFile
        //            {
        //                Name = file.DESCRIPTION,
        //                IsPublic = true,
        //                Type = SubsystemNotDbAppeals.BaseSystemKey
        //            },
        //            fileContent));
        //    }

        //    return result;
        //}
    }
}
