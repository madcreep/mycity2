using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Gc;
using BIT.MyCity.Managers;

namespace BIT.MyCity.Subsystems.DeloAppealGetter
{
    public class FileCleaner : IGcCleaner
    {
        private int _lifeTime;

        public async Task ClearUnusedEntities()
        {}

        public IQueryable<long> UsingFilesIdsQuery(DocumentsContext ctx)
        {
            var lifeDate = DateTime.UtcNow.AddMinutes(-_lifeTime);

            return ctx.AttachedFiles
                .Where(el => el.Type == SubsystemNotDbAppeals.BaseSystemKey
                             && el.CreatedAt >= lifeDate)
                .Select(el => el.Id);
        }

        public FileCleaner(SettingsManager settingsManager)
        {
            _lifeTime = settingsManager.SettingValue<int>(
                $"subsystem.{SubsystemNotDbAppeals.SettingsKey.FileLifeTime}");

            if (_lifeTime < 10)
            {
                _lifeTime = 10;
            }

            if (_lifeTime > 60)
            {
                _lifeTime = 60;
            }
        }
    }
}
