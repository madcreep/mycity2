FROM bizit2/mycity2-delo:latest AS runtime

COPY ./dotnet_debug /usr/local/bin/
RUN chmod +x /usr/local/bin/dotnet_debug

RUN apt-get update
RUN apt-get install -y curl
RUN \
   # replace with the appropriate arch as needed
   curl -L "https://download.jetbrains.com/rider/ssh-remote-debugging/linux-x64/jetbrains_debugger_agent_20230319.24.0" \
    -o /usr/local/bin/debugger && \
    chmod +x /usr/local/bin/debugger

# debugging port
EXPOSE 7777

ENTRYPOINT ["dotnet_debug", "BIT.MyCity.DeloIntegration.dll"]