
using System;
using GraphQL.Server;
using GraphQL.Types;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using BIT.MyCity.Test.StarWars;
using BIT.MyCity.Test.StarWars.Types;

namespace BIT.MyCity.Test
{
    public static class MycityTest
    {

        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<StarWarsData>();
            services.AddSingleton<StarWarsQuery>();
            services.AddSingleton<StarWarsMutation>();
            services.AddSingleton<HumanType>();
            services.AddSingleton<HumanInputType>();
            services.AddSingleton<DroidType>();
            services.AddSingleton<CharacterInterface>();
            services.AddSingleton<EpisodeEnum>();
            services.AddSingleton<ISchema, StarWarsSchema>();
            services.AddSingleton<StarWarsSchema>();
        }

        public static void AddGraphTypesTo(GraphQL.Server.IGraphQLBuilder graphql)
        {
            graphql.AddGraphTypes(typeof(StarWarsSchema));
        }

        public static void UseGraphQL(Microsoft.AspNetCore.Builder.IApplicationBuilder app, string v)
        {
            app.UseGraphQL<StarWarsSchema>(v);
        }
    }
}
