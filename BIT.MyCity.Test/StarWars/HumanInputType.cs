using GraphQL.Types;
using BIT.MyCity.Test.StarWars.Types;


namespace BIT.MyCity.Test.StarWars
{
    public class HumanInputType : InputObjectGraphType<Human>
    {
        public HumanInputType()
        {
            Name = "HumanInput";
            Field(x => x.Name);
            Field(x => x.HomePlanet, nullable: true);
        }
    }
}
