using System;
using GraphQL.Types;

namespace BIT.MyCity.Test.StarWars
{
    
    public class StarWarsSchema : Schema
    {
        public StarWarsSchema(IServiceProvider provider)
            : base(provider)
        {
            
            Query = new StarWarsQuery( new StarWarsData());
            // Query = resolver.Resolve<TestQuery1>();
            Mutation = new StarWarsMutation (new StarWarsData());
        }
    }
}
