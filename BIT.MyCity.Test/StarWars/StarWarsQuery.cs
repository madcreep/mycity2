using System;
using GraphQL;
using GraphQL.Types;
using BIT.MyCity.Test.StarWars.Types;

namespace BIT.MyCity.Test.StarWars
{
    public class StarWarsQuery : ObjectGraphType<object>
    {
        public StarWarsQuery(StarWarsData data)
        {
            Name = "Query";

            Field<CharacterInterface>("hero", resolve: context => data.GetDroidByIdAsync("3"));
            Field<CharacterInterface>("huero", resolve: context => data.GetDroidByIdAsync("4"));

            Field<HumanType>(
                "human",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "id", Description = "id of the human" }
                ),
                resolve: context => data.GetHumanByIdAsync(context.GetArgument<string>("id"))
            );

            // Func<IResolveFieldContext, string, object> func = (context, id) => data.GetDroidByIdAsync(id);

            // FieldDelegate<DroidType>(
            //     "droid",
            //     arguments: new QueryArguments(
            //         new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "id", Description = "id of the droid" }
            //     ),
            //     resolve: func
            // );
        }
    }
}
