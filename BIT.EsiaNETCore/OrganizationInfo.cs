using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;

namespace BIT.EsiaNETCore
{
    public class PersonOrganization
    {
        internal PersonOrganization()
        {

        }

        internal PersonOrganization(JObject org)
        {
            if (org == null)
            {
                return;
            }

            Oid = EsiaHelpers.PropertyValueIfExists("oid", org);
            ShortName = EsiaHelpers.PropertyValueIfExists("shortName", org);
            FullName = EsiaHelpers.PropertyValueIfExists("fullName", org);
            Type = EsiaHelpers.PropertyValueIfExists("type", org)
                .ToOrganizationType();
            Ogrn = EsiaHelpers.PropertyValueIfExists("ogrn", org);
            Inn = EsiaHelpers.PropertyValueIfExists("inn", org);
            Kpp = EsiaHelpers.PropertyValueIfExists("kpp", org);
            Leg = EsiaHelpers.PropertyValueIfExists("leg", org);
            AgencyTerRang = EsiaHelpers.PropertyValueIfExists("agencyTerRang", org);
            AgencyType = EsiaHelpers.PropertyValueIfExists("agencyType", org)
                .ToAgencyType();
            StaffCount = EsiaHelpers.PropertyValueIfExists("staffCount", org);
            BranchesCount = EsiaHelpers.PropertyValueIfExists("branchesCount", org);
            
            ETag = EsiaHelpers.PropertyValueIfExists("eTag", org);

        }

        public string[] StateFacts { get; }

        public string Oid { get; }

        public string ShortName { get; }

        public string FullName { get; }

        public OrganizationType Type { get; }

        public string Ogrn { get; }

        public string Inn { get; }

        public string Kpp { get; }

        public string Leg { get; }

        public string AgencyTerRang { get; }

        public AgencyType? AgencyType { get; }

        public string StaffCount { get; }

        public string BranchesCount { get; }
        
        public string ETag { get; }
    }

    public class OrganizationInfo
    {
        internal OrganizationInfo(JObject orgInfo)
        {
            if (orgInfo == null)
            {
                return;
            }

            Id = EsiaHelpers.PropertyValueIfExists("id", orgInfo);
            ShortName = EsiaHelpers.PropertyValueIfExists("shortName", orgInfo);
            FullName = EsiaHelpers.PropertyValueIfExists("fullName", orgInfo);
            Type = EsiaHelpers.PropertyValueIfExists("type", orgInfo)
                .ToOrganizationType();
            Ogrn = EsiaHelpers.PropertyValueIfExists("ogrn", orgInfo);
            Inn = EsiaHelpers.PropertyValueIfExists("inn", orgInfo);
            Leg = EsiaHelpers.PropertyValueIfExists("leg", orgInfo);
            Kpp = EsiaHelpers.PropertyValueIfExists("kpp", orgInfo);
            AgencyTerRange = EsiaHelpers.PropertyValueIfExists("agencyTerRange", orgInfo);
            AgencyType = EsiaHelpers.PropertyValueIfExists("agencyType", orgInfo)
                .ToAgencyType();
        }

        /// <summary>
        /// Идентификатор организации в ЕСИА
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Сокращенное наименование организации
        /// </summary>
        public string ShortName { get; }

        /// <summary>
        /// Полное наименование организации
        /// </summary>
        public string FullName { get; }

        /// <summary>
        /// Тип организации. Для государственных организаций – “AGENCY”, для юридических лиц – “LEGAL”
        /// </summary>
        public OrganizationType Type { get; }

        /// <summary>
        /// ОГРН организации
        /// </summary>
        public string Ogrn { get; }

        /// <summary>
        /// ИНН организации
        /// </summary>
        public string Inn { get; }

        /// <summary>
        /// Код организационно-правовой формы по общероссийскому классификатору
        /// организационно-правовых форм
        /// </summary>
        public string Leg { get; }

        /// <summary>
        /// КПП организации
        /// </summary>
        public string Kpp { get; }

        /// <summary>
        /// территориальная принадлежность ОГВ
        /// (только для государственных организаций,
        /// код по справочнику «Субъекты Российской федерации» (ССРФ),
        /// для Российской Федерации используется код 00
        /// </summary>
        public string AgencyTerRange { get; }

        /// <summary>
        /// Тип ОГВ (только для государственных организаций)
        /// </summary>
        public AgencyType? AgencyType { get; }
    }
}
