using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using log4net;

namespace BIT.EsiaNETCore
{
    public class EsiaGostSigner : ISignProvider
    {
        private const string TempDir = "SignTempFiles";

        private readonly EsiaGostSgnerOptions _options;

        public EsiaGostSigner(EsiaGostSgnerOptions options)
        {
            _options = options;
        }

        public X509Certificate2 GetCertificate()
        {
            return new X509Certificate2();
        }

        public X509Certificate2 GetEsiaCertificate()
        {
            return new X509Certificate2();
        }

        public byte[] SignMessage(byte[] message, X509Certificate2 certificate)
        {
            try
            {
                var msgStr = Encoding.UTF8.GetString(message);

                if (!File.Exists(_options.OrganizationCertificateFile))
                {
                    throw new Exception($"Не найден сертификат организации ({_options.OrganizationCertificateFile})");
                }

                if (!File.Exists(_options.OrganizationCertificateKeyFile))
                {
                    throw new Exception($"Не найден ключ сертификата организации ({_options.OrganizationCertificateKeyFile})");
                }

                var resultData = new StringBuilder();
                var errorSb = new StringBuilder();

                using (var a = new Process
                {
                    StartInfo =
                    {
                        FileName = _options.OpenSslPath,
                        Arguments =
                            $"cms -sign -binary -stream -engine gost -signer \"{_options.OrganizationCertificateFile}\" -inkey \"{_options.OrganizationCertificateKeyFile}\" -nodetach -outform pem",
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true,
                        UseShellExecute = false
                    }
                })
                {

                    //_options.Log?.Debug($"openssl arguments : {a.StartInfo.Arguments}");


                    try
                    {
                        a.Start();
                        a.StandardInput.Write(msgStr);
                        a.StandardInput.Close();

                        var isKeyProcessing = false;

                        while (!a.StandardOutput.EndOfStream)
                        {
                            var line = a.StandardOutput.ReadLine();

                            switch (line)
                            {
                                case "-----BEGIN CMS-----":
                                    isKeyProcessing = true;
                                    break;
                                case "-----END CMS-----":
                                    isKeyProcessing = false;
                                    break;
                                default:
                                    {
                                        if (!string.IsNullOrWhiteSpace(line))
                                        {
                                            if (isKeyProcessing)
                                            {
                                                resultData.Append(line.Replace("\r", string.Empty));
                                            }
                                            else
                                            {
                                                errorSb.AppendLine(line);
                                            }
                                        }

                                        break;
                                    }
                            }
                        }

                        a.StandardOutput.Close();

                        a.Close();
                    }
                    finally
                    {
                        a.Dispose();
                    }
                }


                if (errorSb.Length > 0)
                {
                    _options.Log?.Warn($"Сообщение openssl : {errorSb}");
                }

                return Convert.FromBase64String(resultData.ToString());
            }
            catch (Exception ex)
            {
                _options.Log?.Error("Ошибка формирования цифровой подписи ЕСИА", ex);

                return null;
            }
        }

        public bool VerifyMessage(string alg, byte[] message, byte[] signature, X509Certificate2 certificate)
        {
            var signFileInfo = CreateSignFile(signature);

            if (signFileInfo == null)
            {
                return false;
            }

            var result = false;

            try
            {
                using (var a = new Process
                {
                    StartInfo =
                    {
                        FileName = _options.OpenSslPath,
                        Arguments =
                            $"dgst -verify \"{_options.EsiaCertificateFile}\" -signature \"{signFileInfo.FullName}\"",
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true,
                        UseShellExecute = false
                    }
                })
                {

                    try
                    {
                        a.Start();
                        a.StandardInput.Write(Encoding.UTF8.GetString(message));
                        a.StandardInput.Close();

                        while (!a.StandardOutput.EndOfStream)
                        {
                            var line = a.StandardOutput.ReadLine();

                            result |= line == "Verified OK";
                        }

                        a.StandardOutput.Close();

                        a.Close();
                    }
                    finally
                    {
                        a.Dispose();
                    }
                }

                if (!result)
                {
                    _options.Log?.Warn("Цифровая подпись ЕСИА не верифицирована!");
                }
            }
            catch (Exception ex)
            {
                _options.Log?.Error("Ошибка верификации цифровой подписи ЕСИА", ex);
            }
            finally
            {
                if (signFileInfo?.Exists ?? false)
                {
                    signFileInfo.Delete();
                }
            }

            return result;
        }

        private FileInfo CreateSignFile(byte[] signature)
        {
            try
            {
                var signFileInfo = new FileInfo(Path.Combine(TempDir, $"{Guid.NewGuid()}.sig"));

                if (signFileInfo.Directory == null)
                {
                    return null;
                }

                if (!signFileInfo.Directory.Exists)
                {
                    signFileInfo.Directory.Create();
                }

                using var stream = signFileInfo.Create();

                stream.Write(signature, 0, signature.Length);

                stream.Flush();

                stream.Close();

                stream.Dispose();

                return signFileInfo;
            }
            catch (Exception ex)
            {
                _options.Log.Error("Ошибка создания файла сигнатуры для верификации подписи ЕСИА", ex);
            }

            return null;
        }
    }

    public class EsiaGostSgnerOptions
    {
        public string OrganizationCertificateFile { get; set; }

        public string OrganizationCertificateKeyFile { get; set; }

        public string EsiaCertificateFile { get; set; }

        public string OpenSslPath { get; set; }

        public ILog Log { get; set; }
    }
}
