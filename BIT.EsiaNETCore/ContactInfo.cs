using Newtonsoft.Json.Linq;
namespace BIT.EsiaNETCore
{
    public enum ContactType
    {
        /// <summary>
        /// Тип не определен
        /// </summary>
        Unknown,
        /// <summary>
        /// Мобильный телефон
        /// </summary>
        Mobile,
        /// <summary>
        /// Домашний телефон
        /// </summary>
        Phone,
        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        Email,
        /// <summary>
        /// Адрес электронной почты организации
        /// </summary>
        OrgEmail,
        /// <summary>
        /// Служебный адрес электронной почты пользователя
        /// </summary>
        WorkEmail,
        /// <summary>
        /// Факс
        /// </summary>
        Fax
    }

    /// <summary>
    /// Provides owner contact information
    /// </summary>
    public class ContactInfo
    {
        //public ContactInfo(ContactType type, string value, bool verified = false)
        //{
        //    ContactType = type;
        //    Value = value;
        //    Verified = verified;
        //}

        public ContactInfo(JObject roleInfo)
        {
            var type = EsiaHelpers.PropertyValueIfExists("type", roleInfo);//["type"].Value<string>();
            ContactType contactType;

            switch (type)
            {
                case "MBT":
                    contactType = ContactType.Mobile;
                    break;
                case "EML":
                    contactType = ContactType.Email;
                    break;
                case "OEM":
                    contactType = ContactType.OrgEmail;
                    break;
                case "CEM":
                    contactType = ContactType.WorkEmail;
                    break;
                case "PHN":
                case "OPH":
                    contactType = ContactType.Phone;
                    break;
                case "OFX":
                    contactType = ContactType.Fax;
                    break;
                default:
                    contactType = ContactType.Unknown;
                    break;
            }

            ContactType = contactType;
            Value = EsiaHelpers.PropertyValueIfExists("value", roleInfo);
            Verified = EsiaHelpers.PropertyValueIfExists("vrfStu", roleInfo) == "VERIFIED";
        }
        
        /// <summary>
        /// Contact type
        /// </summary>
        public ContactType ContactType { get; }

        /// <summary>
        /// Contact value
        /// </summary>
        public string Value { get; }

        /// <summary>
        /// Contact is verified or not by government org
        /// </summary>
        public bool Verified { get; }
    }
}
