using Newtonsoft.Json.Linq;

namespace BIT.EsiaNETCore
{
    public class RoleInfo
    {
        internal RoleInfo(JObject roleInfo)
        {
            if (roleInfo == null)
            {
                return;
            }

            Id = EsiaHelpers.PropertyValueIfExists("oid", roleInfo);
            FullName = EsiaHelpers.PropertyValueIfExists("fullName", roleInfo);
            ShortName = EsiaHelpers.PropertyValueIfExists("shortName", roleInfo);
            Ogrn = EsiaHelpers.PropertyValueIfExists("ogrn", roleInfo);
            BranсhName = EsiaHelpers.PropertyValueIfExists("branсhName", roleInfo);
            BranсhOid = EsiaHelpers.PropertyValueIfExists("branсhOid", roleInfo);
            Chief = bool.TryParse(EsiaHelpers.PropertyValueIfExists("chief", roleInfo), out var isChief) && isChief;
            Email = EsiaHelpers.PropertyValueIfExists("email", roleInfo);
            Phone = EsiaHelpers.PropertyValueIfExists("phone", roleInfo);
            Active = bool.TryParse(EsiaHelpers.PropertyValueIfExists("active", roleInfo), out var IsActive) && IsActive;
            Type = EsiaHelpers.PropertyValueIfExists("type", roleInfo)
                .ToOrganizationType();
        }

        /// <summary>
        /// Идентификатор организации в ЕСИА
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Полное наименование организации
        /// </summary>
        public string FullName { get; }

        /// <summary>
        /// Краткое наименование организации
        /// </summary>
        public string ShortName { get; }

        /// <summary>
        /// ОГРН организации
        /// </summary>
        public string Ogrn { get; }

        /// <summary>
        /// Тип организации
        /// </summary>
        public OrganizationType Type { get; }

        /// <summary>
        /// Наименование филиала
        /// </summary>
        public string BranсhName { get; }

        /// <summary>
        /// Уникальный идентификатор филиала
        /// </summary>
        public string BranсhOid { get; }

        /// <summary>
        /// Является ли сотрудник руководителем организации
        /// </summary>
        public bool Chief { get; }

        /// <summary>
        /// Служебная электронная почта сотрудника
        /// </summary>
        public string Email { get; }

        /// <summary>
        /// Служебный номер телефона сотрудника
        /// </summary>
        public string Phone { get; }

        /// <summary>
        /// Признак блокировки сотрудника
        /// </summary>
        public bool Active { get; }
    }
}
