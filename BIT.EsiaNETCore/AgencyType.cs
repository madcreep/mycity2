using System;
using System.Collections.Generic;
using System.Text;

namespace BIT.EsiaNETCore
{
    /// <summary>
    /// Типы ОГВ
    /// </summary>
    public enum AgencyType
    {
        /// <summary>
        /// Неизвестно
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Федеральный орган исполнительной власти
        /// </summary>
        Fed = 1,

        /// <summary>
        /// Государственный внебюджетный фонд
        /// </summary>
        Fnd = 2,

        /// <summary>
        /// Орган исполнительной власти субъекта РФ
        /// </summary>
        Reg = 3,

        /// <summary>
        /// Орган местного самоуправления
        /// </summary>
        Lcl = 4,

        /// <summary>
        /// Государственное учреждение
        /// </summary>
        Gov = 5,

        /// <summary>
        /// Муниципальное учреждение
        /// </summary>
        Mcl = 6

    }

    internal static class AgencyTypeExtension
    {
        public static AgencyType ToAgencyType(this string typeStr)
        {
            return typeStr switch
            {
                "FED" => AgencyType.Fed,
                "FND" => AgencyType.Fnd,
                "REG" => AgencyType.Reg,
                "LCL" => AgencyType.Lcl,
                "GOV" => AgencyType.Gov,
                "MCL" => AgencyType.Mcl,
                _ => AgencyType.Unknown
            };
        }
    }
}
