using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;

namespace BIT.EsiaNETCore
{
    public class BrunchInfo
    {
        internal BrunchInfo(JObject orgInfo)
        {
            if (orgInfo == null)
            {
                return;
            }

            Name = EsiaHelpers.PropertyValueIfExists("name", orgInfo);
            Leg = EsiaHelpers.PropertyValueIfExists("leg", orgInfo);
            Kpp = EsiaHelpers.PropertyValueIfExists("kpp", orgInfo);
        }

        /// <summary>
        /// Наименование филиала
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Код организационно-правовой формы по общероссийскому классификатору
        /// организационно-правовых форм
        /// </summary>
        public string Leg { get; }

        /// <summary>
        /// КПП филиала
        /// </summary>
        public string Kpp { get; }
    }

    
}
