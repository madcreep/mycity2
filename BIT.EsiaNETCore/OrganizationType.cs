namespace BIT.EsiaNETCore
{
    /// <summary>
    /// Тип организации
    /// </summary>
    public enum OrganizationType
    {
        /// <summary>
        /// Неизвестная
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Индивидуальный предприниматель
        /// </summary>
        Business = 1,

        /// <summary>
        /// Юридическое лицо
        /// </summary>
        Legal = 2,

        /// <summary>
        /// Орган государственной власти
        /// </summary>
        Agency = 3
    }

    internal static class OrganizationTypeExtension
    {
        public static OrganizationType ToOrganizationType(this string typeStr)
        {
            return typeStr switch
            {
                "BUSINESS" => OrganizationType.Business,
                "LEGAL" => OrganizationType.Legal,
                "AGENCY" => OrganizationType.Agency,
                _ => OrganizationType.Unknown
            };
        }
    }
}
