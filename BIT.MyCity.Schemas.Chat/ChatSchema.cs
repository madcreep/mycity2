using GraphQL.Types;

namespace BIT.MyCity.Schemas.Chat
{
    public class ChatSchema : Schema
    {
        public ChatSchema(IChat chat) 
        {
            Query = new ChatQuery(chat);
            Mutation = new ChatMutation(chat);
            Subscription = new ChatSubscriptions(chat);
        }
    }
}
