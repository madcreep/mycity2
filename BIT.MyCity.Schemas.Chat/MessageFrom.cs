namespace BIT.MyCity.Schemas.Chat
{
    public class MessageFrom
    {
        public string Id { get; set; }

        public string DisplayName { get; set; }
    }
}
