using GraphQL.Types;

namespace BIT.MyCity.Schemas.Chat
{
    public class MessageFromType : ObjectGraphType<MessageFrom>
    {
        public MessageFromType()
        {
            Field(o => o.Id);
            Field(o => o.DisplayName);
        }
    }
}
