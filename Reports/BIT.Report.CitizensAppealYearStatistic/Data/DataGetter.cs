using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Reports;
using BIT.MyCity.Subsystems;
using BIT.Report.CitizensAppealYearStatistic.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.Report.CitizensAppealYearStatistic.Data
{
    internal class DataGetter
    {
        private readonly IServiceProvider _servicesProvider;
        private readonly ReportSettings _settings;

        public DataGetter(IServiceProvider servicesProvider, ReportSettings settings)
        {
            _servicesProvider = servicesProvider;
            _settings = settings;
        }

        public async Task<ReportData> GetDataAsync()
        {
            var ctx = _servicesProvider.GetRequiredService<DocumentsContext>();

            var result = InitReportData();

            result.MonthIntervals = GetMonthIntervals(result);

            var counterQuery = GetCounterQuery(result, ctx);

            var items = await GetCounterItemsAsync(counterQuery, result);

            var gradation = await GetGradationDictionaryAsync(ctx, result.Gradation);

            items = AddGradation(items, gradation, result);

            result.Items = SortItems(items, result);

            return result;
        }
        
        private ReportData InitReportData()
        {
            var result = new ReportData
            {
                TimeOffset = _settings.TimeOffset ?? 0,
                Year = int.Parse(_settings["YEAR"].Value),
                NotPrintEmpty = bool.Parse(_settings["NOT_PRINT_EMPTY"].Value),
                Gradation = int.Parse(_settings["GADATION_BY"].Value)
            };

            var settingsManager = _servicesProvider.GetRequiredService<SettingsManager>();

            result.GradationName = result.Gradation switch
            {
                1 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitleOne),
                2 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitleOne),
                3 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitleOne),
                4 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitleOne),
                _ => throw new NotImplementedException()
            };

            result.GradationNameMany = result.Gradation switch
            {
                1 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitle),
                2 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitle),
                3 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitle),
                4 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitle),
                _ => throw new NotImplementedException()
            };

            return result;
        }

        private static IReadOnlyDictionary<int, ReportSettingsDateRange> GetMonthIntervals(ReportData data)
        {
            var result = new Dictionary<int, ReportSettingsDateRange>(12);

            for (var month = 1; month <= 12; month++)
            {
                var start = new DateTime(data.Year, month, 1).AddHours(-data.TimeOffset);

                result.Add(month,
                    new ReportSettingsDateRange(start, start.AddMonths(1)));
            }

            return result;
        }

        private static IQueryable<object> GetCounterQuery(ReportData data, DocumentsContext ctx)
        {
            var appealQuery = ctx.Appeals
                .Where(el => el.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey
                && el.CreatedAt >= data.MonthIntervals[1].From.Value && el.CreatedAt < data.MonthIntervals[12].To.Value);
            
            var groupQuery = data.Gradation switch
            {
                1 => appealQuery.GroupBy(el => el.RubricId),
                2 => appealQuery.GroupBy(el => el.TopicId),
                3 => appealQuery.GroupBy(el => el.RegionId),
                4 => appealQuery.GroupBy(el => el.TerritoryId),
                _ => throw new NotImplementedException()
            };

            return groupQuery
                .Select(el => new
                {
                    Id = el.Key,
                    CountTotal = el.Count(),
                    CountPublic = el.Count(x => x.Public_),
                    Month_1 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[1].From.Value &&
                        x.CreatedAt < data.MonthIntervals[1].To.Value),
                    Month_2 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[2].From.Value &&
                        x.CreatedAt < data.MonthIntervals[2].To.Value),
                    Month_3 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[3].From.Value &&
                        x.CreatedAt < data.MonthIntervals[3].To.Value),
                    Month_4 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[4].From.Value &&
                        x.CreatedAt < data.MonthIntervals[4].To.Value),
                    Month_5 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[5].From.Value &&
                        x.CreatedAt < data.MonthIntervals[5].To.Value),
                    Month_6 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[6].From.Value &&
                        x.CreatedAt < data.MonthIntervals[6].To.Value),
                    Month_7 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[7].From.Value &&
                        x.CreatedAt < data.MonthIntervals[7].To.Value),
                    Month_8 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[8].From.Value &&
                        x.CreatedAt < data.MonthIntervals[8].To.Value),
                    Month_9 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[9].From.Value &&
                        x.CreatedAt < data.MonthIntervals[9].To.Value),
                    Month_10 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[10].From.Value &&
                        x.CreatedAt < data.MonthIntervals[10].To.Value),
                    Month_11 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[11].From.Value &&
                        x.CreatedAt < data.MonthIntervals[11].To.Value),
                    Month_12 = el.Count(x =>
                        x.CreatedAt >= data.MonthIntervals[12].From.Value &&
                        x.CreatedAt < data.MonthIntervals[12].To.Value)
                });
        }

        private static async Task<List<DataItem>> GetCounterItemsAsync(IQueryable<object> counterQuery, ReportData result)
        {
            var counter = await counterQuery
                .ToArrayAsync();

            const BindingFlags attr = BindingFlags.Public | BindingFlags.Instance;

            var items = new List<DataItem>();

            foreach (var c in counter)
            {
                var dataItem = new DataItem();

                foreach (var property in c.GetType().GetProperties(attr))
                {
                    if (!property.CanRead)
                    {
                        continue;
                    }

                    var value = property.GetValue(c, null);

                    switch (property.Name)
                    {
                        case "Id":
                            dataItem.Id = value == null ? 0L : Convert.ToInt64(value);
                            break;
                        case "CountTotal":
                            dataItem.CountTotal = Convert.ToUInt32(value);
                            break;
                        case "CountPublic":
                            dataItem.CountPublic = Convert.ToUInt32(value);
                            break;
                        default:
                            if (!property.Name.StartsWith("Month_"))
                            {
                                continue;
                            }

                            var month = int.Parse(property.Name.Split('_').Last());

                            dataItem.CountByMonth[month] = Convert.ToUInt32(value);

                            break;
                    }
                }

                items.Add(dataItem);
            }

            return items;
        }

        private static async Task<Dictionary<long, GradationItem>> GetGradationDictionaryAsync(DocumentsContext ctx, int gradation)
        {
            var rubricatorQuery = gradation switch
            {
                1 => ctx.Rubrics
                    .Where(el => el.Rubricator2Subsystems.Any(x => x.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey))
                    .Select(el => new GradationItem
                    {
                        Id = el.Id,
                        Name = el.Name
                    }),
                2 => ctx.Topics
                    .Where(el => el.Rubricator2Subsystems.Any(x => x.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey))
                    .Select(el => new GradationItem
                    {
                        Id = el.Id,
                        Name = el.Name
                    }),
                3 => ctx.Regions
                    .Where(el => el.Rubricator2Subsystems.Any(x => x.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey))
                    .Select(el => new GradationItem
                    {
                        Id = el.Id,
                        Name = el.Name
                    }),
                4 => ctx.Territories
                    .Where(el => el.Rubricator2Subsystems.Any(x => x.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey))
                    .Select(el => new GradationItem
                    {
                        Id = el.Id,
                        Name = el.Name
                    }),
                _ => throw new NotImplementedException()
            };

            var result = await rubricatorQuery
                .OrderBy(el => el.Name)
                .ToDictionaryAsync(el => el.Id);

            result.Add(0, new GradationItem {Part = 2, Name = "Не указано" });

            return result;
        }

        private static List<DataItem> AddGradation(IReadOnlyCollection<DataItem> items, Dictionary<long, GradationItem> gradation, ReportData data)
        {
            var result = new List<DataItem>();

            if (data.NotPrintEmpty)
            {
                foreach (var grItem in gradation.Values)
                {
                    var item = items.FirstOrDefault(el => el.Id == grItem.Id)
                               ?? new DataItem() { Id = grItem.Id };

                    item.Name = grItem.Name;
                    item.Part = grItem.Part;

                    result.Add(item);
                }
            }
            else
            {
                foreach (var item in items)
                {
                    item.Name = gradation.ContainsKey(item.Id)
                        ? gradation[item.Id].Name
                        : "<неизвестно>";

                    item.Part = gradation.ContainsKey(item.Id)
                        ? gradation[item.Id].Part
                        : 2;
                }

                result.AddRange(items);
            }

            return result;
        }

        private static DataItem[] SortItems(IEnumerable<DataItem> items, ReportData result)
        {
            return items
                .OrderBy(el=>el.Part)
                .ThenBy(el=>el.Name)
                .ToArray();
        }
    }
}
