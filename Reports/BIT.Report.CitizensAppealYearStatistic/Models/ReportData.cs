using System.Collections.Generic;
using BIT.MyCity.Reports;

namespace BIT.Report.CitizensAppealYearStatistic.Models
{
    internal sealed class ReportData
    {
        public int TimeOffset { get; set; }

        public int Year { get; set; }

        //public ReportSettingsDateRange Range { get; set; }

        public int Gradation { get; set; }

        public string GradationName { get; set; }

        public string GradationNameMany { get; set; }
        
        public bool NotPrintEmpty { get; set; }
        
        public DataItem[] Items { get; set; }

        public IReadOnlyDictionary<int, ReportSettingsDateRange> MonthIntervals { get; set; }
    }
}
