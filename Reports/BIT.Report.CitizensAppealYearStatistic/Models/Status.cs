namespace BIT.Report.CitizensAppealYearStatistic.Models
{
    internal class Status
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
