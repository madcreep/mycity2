namespace BIT.Report.CitizensAppealYearStatistic.Models
{
    internal class GradationItem
    {
        public uint Part { get; set; } = 1;

        public long Id { get; set; }

        public string Name { get; set; }
    }
}
