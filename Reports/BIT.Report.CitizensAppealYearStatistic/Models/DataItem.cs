using System.Collections.Generic;

namespace BIT.Report.CitizensAppealYearStatistic.Models
{
    internal class DataItem
    {
        public uint Part { get; set; }

        public long Id { get; set; }

        public string Name { get; set; }

        public uint CountTotal { get; set; }

        public uint CountPublic { get; set; }

        public Dictionary<int, uint> CountByMonth { get; } = new Dictionary<int, uint>(12);
        

        public DataItem()
        {
            for (var month = 1; month <= 12; month++)
            {
                CountByMonth.Add(month, 0);
            }
        }
    }
}
