using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Reports;
using BIT.MyCity.Subsystems;
using BIT.Report.CitizensAppealYearStatistic.Data;
using BIT.Report.CitizensAppealYearStatistic.Generators;
using BIT.Report.CitizensAppealYearStatistic.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.Report.CitizensAppealYearStatistic
{
    public sealed class Report : BaseReport
    {
        public override string SubsystemUid => SubsystemCitizenAppeals.BaseSystemKey;
        public override Guid Id { get; } = Guid.Parse("E8BF1AF1-5458-4B38-9FC6-8AC759667645");
        public override string Name => "Статистика обращений граждан за год";
        public override string Description => "Отчет по статистике обращений граждан за год";
        public override async Task<ReportSettings> GetSettings(IServiceProvider servicesProvider)
        {
            var ctx = servicesProvider.GetRequiredService<DocumentsContext>();

            var minDate = await ctx.Appeals
                .Where(el => el.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey)
                .MinAsync(el => el.CreatedAt);

            var years = new List<ReportSettingsValue>();

            for (var year = minDate.Year; year <= DateTime.Now.Year; year++)
            {
                years.Add(new ReportSettingsValue(year, year.ToString(), year, null, false));
            }

            var settingsManager = servicesProvider.GetRequiredService<SettingsManager>();
            
            return new ReportSettingsBuilder(Id)
                .AddDiscreteSelector("Год", "YEAR", DiscreteSelectorType.Combo,
                    years, true)
                .AddDiscreteSelector("Градация", "GADATION_BY", DiscreteSelectorType.Combo, new ReportSettingsValue[]
                {
                    new ReportSettingsValue(1, settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitle), 1, null, false),
                    new ReportSettingsValue(2, settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitle), 2, null, false),
                    new ReportSettingsValue(3, settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitle), 3, null, false),
                    new ReportSettingsValue(4, settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitle), 4, null, false)
                }, true)
                .AddSingleCheckbox("Не печатать пустые", "NOT_PRINT_EMPTY")
                .Build;
        }

        public override async Task<ExecuteReportResult> Execute(IServiceProvider servicesProvider, ReportSettings settings)
        {
            var result = new ExecuteReportResult(Id);

            var defSettings = await GetSettings(servicesProvider);

            var settingsErrors = TestSettings(servicesProvider, defSettings, settings)
                .ToArray();

            if (settingsErrors.Any())
            {
                result.Errors.AddRange(settingsErrors);
                return result;
            }

            var dataGetter = new DataGetter(servicesProvider, settings);

            var data = await dataGetter.GetDataAsync();

            var generator = new ExcelGenerator(data);

            var stream = await generator.GenerateAsync();

            result.File = await SaveFile(servicesProvider,
                GenerateFileName(data),
                stream);

            if (result.File == null)
            {
                result.Errors.Add("Ошибка при сохранении файла.");

                return result;
            }

            var fileManager = servicesProvider.GetRequiredService<AttachedFileManager>();

            result.Template = TemplateGenerator.Generate(result.File, fileManager);

            if (!string.IsNullOrWhiteSpace(result.Template))
            {
                return result;
            }

            result.Errors.Add("Ошибка при генерации разметки.");

            return result;
        }

        private string GenerateFileName(ReportData data)
        {
            return
                $"Статистика обращений граждан за {data.Year} год (Градация: {data.GradationNameMany}).xlsx";
        }
    }
}
