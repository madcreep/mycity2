using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BIT.Report.CitizensAppealStatistic.Models;
using Syncfusion.XlsIO;

namespace BIT.Report.CitizensAppealStatistic.Generators
{
    internal class ExcelGenerator
    {
        private const string ResourceName = "Report.xltx";
        private readonly ReportData _data;

        public ExcelGenerator(ReportData data)
        {
            _data = data;
        }

        public async Task<Stream> GenerateAsync()
        {
            var assembly = Assembly.GetExecutingAssembly();

            var resourceName = $"{typeof(Report).Namespace}.{ResourceName}";

            using var excelEngine = new ExcelEngine();

            var application = excelEngine.Excel;

            application.DefaultVersion = ExcelVersion.Excel2013;

            IWorkbook workbook;

            await using (var docStream = assembly.GetManifestResourceStream(resourceName))
            {
                workbook = application.Workbooks.Open(docStream);
            }

            var worksheet = workbook.Worksheets.First();

            AddTitle(worksheet);

            var lastRowIndex = AddItems(worksheet);

            FormatWorksheet(worksheet, lastRowIndex);

            var stream = new MemoryStream();

            workbook.SaveAs(stream);

            workbook.Close();

            excelEngine.Dispose();

            return stream;
        }

        private void FormatWorksheet(IWorksheet worksheet, int lastRowIndex)
        {
            var cellCount = 4 + _data.Statuses.Length;

            var range = worksheet.Range[1, 1, 4, cellCount];
            range.VerticalAlignment = ExcelVAlign.VAlignCenter;
            range.HorizontalAlignment = ExcelHAlign.HAlignCenter;
            range.CellStyle.Font.Bold = true;

            range = worksheet.Range[5, 1, lastRowIndex - 1, 1];
            range.VerticalAlignment = ExcelVAlign.VAlignTop;
            range.HorizontalAlignment = ExcelHAlign.HAlignCenter;

            range = worksheet.Range[5, 2, lastRowIndex - 1, 2];
            range.VerticalAlignment = ExcelVAlign.VAlignTop;
            range.HorizontalAlignment = ExcelHAlign.HAlignLeft;

            range = worksheet.Range[5, 3, lastRowIndex, cellCount];
            range.VerticalAlignment = ExcelVAlign.VAlignTop;
            range.HorizontalAlignment = ExcelHAlign.HAlignCenter;

            range = worksheet.Range[lastRowIndex, 1, lastRowIndex, cellCount];
            range.CellStyle.Font.Bold = true;

            range = worksheet.Range[lastRowIndex, 1];
            range.VerticalAlignment = ExcelVAlign.VAlignTop;
            range.HorizontalAlignment = ExcelHAlign.HAlignCenter;

            range = worksheet.Range[3, 1, lastRowIndex, cellCount];
            range.Borders[ExcelBordersIndex.EdgeTop].LineStyle = ExcelLineStyle.Thin;
            range.Borders[ExcelBordersIndex.EdgeRight].LineStyle = ExcelLineStyle.Thin;
            range.Borders[ExcelBordersIndex.EdgeBottom].LineStyle = ExcelLineStyle.Thin;
            range.Borders[ExcelBordersIndex.EdgeLeft].LineStyle = ExcelLineStyle.Thin;
        }

        private void AddTitle(IWorksheet worksheet)
        {
            worksheet.Range[1, 1, 2, _data.Statuses.Length + 4].Merge(true);
            worksheet.Range[1, 1].Text = $"Статистика обращений граждан за период{Environment.NewLine}c {DateString(_data.Range.From)} по {DateString(_data.Range.To)}";

            worksheet.Range[3, 2].Text = _data.GradationName;

            worksheet.Range[3, 4, 3, 4 + _data.Statuses.Length].Merge(true);
            worksheet.Range[3, 4].Text = "в том числе:";

            worksheet.Range[4, 4].Text = "Публичных";

            for (var i = 0; i < _data.Statuses.Length; i++)
            {
                worksheet.Range[4, 5 + i].Text = _data.Statuses[i].Name;
            }
        }

        private int AddItems(IWorksheet worksheet)
        {
            var hasAny = _data.Items.Any();

            var row = 5;
            int column;

            if (hasAny)
            {
                for (var i = 0; i < _data.Items.Length; i++)
                {
                    var item = _data.Items[i];

                    worksheet.Range[row, 1].Number = i + 1;
                    worksheet.Range[row, 2].Text = item.Name;
                    worksheet.Range[row, 3].Number = item.CountTotal;
                    worksheet.Range[row, 4].Number = item.CountPublic;

                    column = 5;

                    foreach (var key in item.CountStatus.Keys)
                    {
                        worksheet.Range[row, column++].Number = item.CountStatus[key];
                    }

                    row++;
                }
            }

            worksheet.Range[row, 1, row, 2].Merge(true);
            worksheet.Range[row, 1].Text = "ИТОГО :";

            worksheet.Range[row, 3].Number = hasAny
                ? _data.Items.Sum(el => el.CountTotal)
                : 0;

            worksheet.Range[row, 4].Number = hasAny
                ? _data.Items.Sum(el => el.CountPublic)
                : 0;

            column = 5;

            foreach (var statusId in _data.Statuses.Select(el => el.Id).OrderBy(el => el))
            {
                worksheet.Range[row, column++].Number = hasAny
                    ? _data.Items.Sum(el => el.CountStatus[statusId])
                    : 0;
            }

            return row;
        }

        public static string DateString(DateTime? date)
        {
            return date.HasValue
                ? date.Value.ToString("dd.MM.yyyy", CultureInfo.InvariantCulture)
                : "__.__.____";
        }
    }
}
