using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Reports;
using BIT.MyCity.Subsystems;
using BIT.Report.CitizensAppealStatistic.Data;
using BIT.Report.CitizensAppealStatistic.Generators;
using BIT.Report.CitizensAppealStatistic.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.Report.CitizensAppealStatistic
{
    public sealed class Report : BaseReport
    {
        public override string SubsystemUid => SubsystemCitizenAppeals.BaseSystemKey;
        public override Guid Id { get; } = Guid.Parse("9CA0170E-F945-4D16-B514-BE0D39D8127D");
        public override string Name => "Статистика обращений граждан за период";
        public override string Description => "Отчет по статистике обращений граждан за период";
        public override async Task<ReportSettings> GetSettings(IServiceProvider servicesProvider)
        {
            var ctx = servicesProvider.GetRequiredService<DocumentsContext>();
            var settingsManager = servicesProvider.GetRequiredService<SettingsManager>();

            var statuses = (await ctx.EnumVerbs
                    .Where(el => el.Group == "appealStatus" && el.SubsystemUID == SubsystemCitizenAppeals.BaseSystemKey
                                                            && !el.Deleted)
                    .Select(el => new {el.Value, el.ShortName})
                    .ToArrayAsync())
                .Select(el =>
                    new ReportSettingsValue(int.Parse(el.Value), el.ShortName, int.Parse(el.Value), null, false))
                .Where(el => el.Key >= 0)
                .OrderBy(el => el.Weight)
                .ToArray();

            return new ReportSettingsBuilder(Id)
                .AddDiscreteSelector("Градация", "GADATION_BY", DiscreteSelectorType.Combo, new ReportSettingsValue[]
                {
                    new ReportSettingsValue(1, settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitle), 1, null, false),
                    new ReportSettingsValue(2, settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitle), 2, null, false),
                    new ReportSettingsValue(3, settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitle), 3, null, false),
                    new ReportSettingsValue(4, settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitle), 4, null, false)
                }, true)
                .AddDateRange("Диапазон дат", "DATE_RANGE",
                    mode: DateRangeControlMode.Range)
                
                .AddListSelect("По статусам", "STATUSES",
                    statuses,
                    false, allEnable: true)
                .AddSingleCheckbox("Не печатать пустые", "NOT_PRINT_EMPTY")
                .Build;
        }

        public override async Task<ExecuteReportResult> Execute(IServiceProvider servicesProvider, ReportSettings settings)
        {
            var result = new ExecuteReportResult(Id);

            var defSettings = await GetSettings(servicesProvider);

            var settingsErrors = TestSettings(servicesProvider, defSettings, settings)
                .ToArray();

            if (settingsErrors.Any())
            {
                result.Errors.AddRange(settingsErrors);
                return result;
            }

            var dataGetter = new DataGetter(servicesProvider, settings);

            var data = await dataGetter.GetDataAsync();

            var generator = new ExcelGenerator(data);

            var stream = await generator.GenerateAsync();

            result.File = await SaveFile(servicesProvider,
                GenerateFileName(data),
                stream);

            if (result.File == null)
            {
                result.Errors.Add("Ошибка при сохранении файла.");

                return result;
            }

            var fileManager = servicesProvider.GetRequiredService<AttachedFileManager>();

            result.Template = TemplateGenerator.Generate(result.File, fileManager);

            if (!string.IsNullOrWhiteSpace(result.Template))
            {
                return result;
            }

            result.Errors.Add("Ошибка при генерации разметки.");

            return result;
        }

        private string GenerateFileName(ReportData data)
        {
            return
                $"Статистика обращений граждан за период c {ExcelGenerator.DateString(data.Range.From)} по {ExcelGenerator.DateString(data.Range.To)} (Градация: {data.GradationNameMany}).xlsx";
        }
    }
}
