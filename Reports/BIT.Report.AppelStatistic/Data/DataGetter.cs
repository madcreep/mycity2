using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Reports;
using BIT.MyCity.Subsystems;
using BIT.Report.CitizensAppealStatistic.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.Report.CitizensAppealStatistic.Data
{
    internal class DataGetter
    {
        private readonly IServiceProvider _servicesProvider;
        private readonly ReportSettings _settings;

        public DataGetter(IServiceProvider servicesProvider, ReportSettings settings)
        {
            _servicesProvider = servicesProvider;
            _settings = settings;
        }

        public async Task<ReportData> GetDataAsync()
        {
            var ctx = _servicesProvider.GetRequiredService<DocumentsContext>();

            var result = InitReportData();

            await AddNeedStatusesToResultAsync(result, ctx);
            
            var counterQuery = GetCounterQuery(result, ctx);

            var items = await GetCounterItemsAsync(counterQuery, result);

            var gradation = await GetGradationDictionaryAsync(ctx, result.Gradation);

            items = AddGradation(items, gradation, result);

            result.Items = SortItems(items, result);

            return result;
        }

        private static DataItem[] SortItems(IEnumerable<DataItem> items, ReportData result)
        {
            return items
                .OrderBy(el=>el.Name)
                .ToArray();
        }

        private static async Task<List<DataItem>> GetCounterItemsAsync(IQueryable<object> counterQuery, ReportData result)
        {
            var counter = await counterQuery
                .ToArrayAsync();

            const BindingFlags attr = BindingFlags.Public | BindingFlags.Instance;

            var items = new List<DataItem>();

            foreach (var c in counter)
            {
                var dataItem = new DataItem(result.Statuses);

                foreach (var property in c.GetType().GetProperties(attr))
                {
                    if (!property.CanRead)
                    {
                        continue;
                    }

                    var value = property.GetValue(c, null);

                    switch (property.Name)
                    {
                        case "Id":
                            dataItem.Id = value == null ? 0L : Convert.ToInt64(value);
                            break;
                        case "CountTotal":
                            dataItem.CountTotal = Convert.ToUInt32(value);
                            break;
                        case "CountPublic":
                            dataItem.CountPublic = Convert.ToUInt32(value);
                            break;
                        default:
                            if (!property.Name.StartsWith("CountStatus_"))
                            {
                                continue;
                            }

                            var statusId = int.Parse(property.Name.Split('_').Last());

                            dataItem.CountStatus[statusId] = Convert.ToUInt32(value);

                            break;
                    }
                }

                items.Add(dataItem);
            }

            return items;
        }

        private static IQueryable<object> GetCounterQuery(ReportData data, DocumentsContext ctx)
        {
            var dateFrom = data.Range.From?.AddHours(-data.TimeOffset);
            var dateTo = data.Range.To?.AddHours(-data.TimeOffset);

            var appealQuery = ctx.Appeals
                .Where(el => el.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey);

            if (dateFrom.HasValue)
            {
                appealQuery = appealQuery
                    .Where(el => el.CreatedAt >= dateFrom.Value);
            }

            if (dateTo.HasValue)
            {
                appealQuery = appealQuery
                    .Where(el => el.CreatedAt < dateTo.Value.AddDays(1));
            }

            var selectQuery =
                new StringBuilder(
                    "new {Id = el.Key, CountTotal = el.Count(), CountPublic = el.Count(x => x.Public_)");

            foreach (var state in data.Statuses)
            {
                selectQuery.Append($", CountStatus_{state.Id} = el.Count(x => x.SiteStatus == {state.Id})");
            }

            selectQuery.Append("}");

            var groupQuery = data.Gradation switch
            {
                1 => appealQuery.GroupBy(el => el.RubricId),
                2 => appealQuery.GroupBy(el => el.TopicId),
                3 => appealQuery.GroupBy(el => el.RegionId),
                4 => appealQuery.GroupBy(el => el.TerritoryId),
                _ => throw new NotImplementedException()
            };

            return groupQuery.SelectDynamic(el => selectQuery.ToString());
        }

        private ReportData InitReportData()
        {
            var result = new ReportData
            {
                TimeOffset = _settings.TimeOffset ?? 0,
                NotPrintEmpty = bool.Parse(_settings["NOT_PRINT_EMPTY"].Value),
                Gradation = int.Parse(_settings["GADATION_BY"].Value)
            };
            
            if (ReportSettingsDateRange.TryParse(_settings["DATE_RANGE"].Value, out var range))
            {
                result.Range = range;
            }

            var settingsManager = _servicesProvider.GetRequiredService<SettingsManager>();

            result.GradationName = result.Gradation switch
            {
                1 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitleOne),
                2 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitleOne),
                3 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitleOne),
                4 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitleOne),
                _ => throw new NotImplementedException()
            };

            result.GradationNameMany = result.Gradation switch
            {
                1 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitle),
                2 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitle),
                3 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitle),
                4 => settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitle),
                _ => throw new NotImplementedException()
            };

            return result;
        }

        private static List<DataItem> AddGradation(IReadOnlyCollection<DataItem> items, Dictionary<long, GradationItem> gradation, ReportData data)
        {
            var result = new List<DataItem>();

            if (data.NotPrintEmpty)
            {
                foreach (var grItem in gradation.Values)
                {
                    var item = items.FirstOrDefault(el => el.Id == grItem.Id)
                               ?? new DataItem(data.Statuses) { Id = grItem.Id };

                    item.Name = grItem.Name;
                    item.Part = grItem.Part;

                    result.Add(item);
                }
            }
            else
            {
                foreach (var item in items)
                {
                    item.Name = gradation.ContainsKey(item.Id)
                        ? gradation[item.Id].Name
                        : "<неизвестно>";

                    item.Part = gradation.ContainsKey(item.Id)
                        ? gradation[item.Id].Part
                        : 2;
                }

                result.AddRange(items);
            }

            return result;
        }

        private static async Task<Dictionary<long, GradationItem>> GetGradationDictionaryAsync(DocumentsContext ctx, int gradation)
        {
            var rubricatorQuery = gradation switch
            {
                1 => ctx.Rubrics
                    .Where(el => el.Rubricator2Subsystems.Any(x => x.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey))
                    .Select(el => new GradationItem
                    {
                        Id = el.Id,
                        Name = el.Name
                    }),
                2 => ctx.Topics
                    .Where(el => el.Rubricator2Subsystems.Any(x => x.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey))
                    .Select(el => new GradationItem
                    {
                        Id = el.Id,
                        Name = el.Name
                    }),
                3 => ctx.Regions
                    .Where(el => el.Rubricator2Subsystems.Any(x => x.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey))
                    .Select(el => new GradationItem
                    {
                        Id = el.Id,
                        Name = el.Name
                    }),
                4 => ctx.Territories
                    .Where(el => el.Rubricator2Subsystems.Any(x => x.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey))
                    .Select(el => new GradationItem
                    {
                        Id = el.Id,
                        Name = el.Name
                    }),
                _ => throw new NotImplementedException()
            };

            var result = await rubricatorQuery
                .OrderBy(el => el.Name)
                .ToDictionaryAsync(el => el.Id);

            result.Add(0, new GradationItem { Part = 2, Name = "Не указано"});

            return result;
        }

        private async Task AddNeedStatusesToResultAsync(ReportData result, DocumentsContext ctx)
        {
            var dbStatuses = (await ctx.EnumVerbs
                .Where(el => el.Group == "appealStatus" && el.SubsystemUID == SubsystemCitizenAppeals.BaseSystemKey
                                                        && !el.Deleted)
                .Select(el => new { el.Value, el.ShortName })
                .ToArrayAsync())
                .Select(el=>new Status
                {
                    Id = int.Parse(el.Value),
                    Name = el.ShortName
                })
                .Where(el => el.Id >= 0)
                .OrderBy(el=>el.Id)
                .ToArray();
            
            if (_settings["STATUSES"].AllSelected == true)
            {
                result.Statuses = dbStatuses;
                return;
            }

            if (string.IsNullOrWhiteSpace(_settings["STATUSES"].Value))
            {
                result.Statuses = new Status[0];
                return;
            }

            var needStatuses = _settings["STATUSES"].Value
                .Split('|')
                .Where(el => !string.IsNullOrWhiteSpace(el))
                .Select(el => int.Parse(el.Trim()))
                .OrderBy(el => el)
                .ToArray();

            result.Statuses = dbStatuses
                .Join(needStatuses,
                    dbs => dbs.Id,
                    nds => nds,
                    (dbs, nds) => dbs)
                .ToArray();
        }
    }
}
