using System.Collections.Generic;
using System.Linq;

namespace BIT.Report.CitizensAppealStatistic.Models
{
    internal class DataItem
    {
        public uint Part { get; set; }

        public long Id { get; set; }

        public string Name { get; set; }

        public uint CountTotal { get; set; }

        public uint CountPublic { get; set; }

        public Dictionary<int, uint> CountStatus { get; } = new Dictionary<int, uint>();

        public DataItem(IEnumerable<Status> statuses)
        {
            foreach (var status in statuses.OrderBy(el=>el.Id))
            {
                CountStatus.Add(status.Id, 0);
            }
        }
    }
}
