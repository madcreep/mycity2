using BIT.MyCity.Reports;

namespace BIT.Report.CitizensAppealStatistic.Models
{
    internal sealed class ReportData
    {
        public int TimeOffset { get; set; }

        public ReportSettingsDateRange Range { get; set; }

        public int Gradation { get; set; }

        public string GradationName { get; set; }

        public string GradationNameMany { get; set; }

        public bool IsExtended { get; set; }

        public bool NotPrintEmpty { get; set; }

        public Status[] Statuses { get; set; }

        public DataItem[] Items { get; set; }
        
    }
}
