using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Reports;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.TestReport
{
    public class Report2 : BaseReport
    {
        public override string SubsystemUid => "organizations";

        public override Guid Id { get; } = Guid.Parse("664B5B9D-AD4C-4412-9802-6473C94DAFFA");

        public override string Name => "Тестовый отчет (организации)";

        public override string Description => "Отчет проверки работоспособности системы отчетов";


        public override async Task<ReportSettings> GetSettings(IServiceProvider servicesProvider)
        {
            var ctx = servicesProvider.GetRequiredService<DocumentsContext>();

            var topics = await ctx.Topics
                .Where(el => el.Subsystem.UID == "citizens")
                .OrderBy(el => el.Weight)
                .Select(el => new ReportSettingsValue(el.Id, el.ShortName, el.Weight, null, false))
                .ToArrayAsync();

            return new ReportSettingsBuilder(Id)
                .AddCustomText("Введите строку", "CUSTOM_TEXT1", false,
                    CustomTextMode.SingleRow, 255, value: "Всем привет!")
                .AddCustomText("Введите тест", "CUSTOM_TEXT2", true,
                    CustomTextMode.MultiRows, 2000, 5, "Всем привет!")
                .AddDateRange("Диапазон дат (обязательный)", "DATE_RANGE1", true,
                    DateRangeControlMode.Range)
                .AddDateRange("Диапазон дат (не обязательный)", "DATE_RANGE2", false,
                    DateRangeControlMode.PlusNAndLater, -3,
                    availableDateRanges: new[]
                    {
                        DateRangeControlMode.Range,
                        DateRangeControlMode.CurrentDayPlusN,
                        DateRangeControlMode.PlusNAndLater,
                        DateRangeControlMode.PlusNAndBefore,
                        DateRangeControlMode.CurrentMonth,
                        DateRangeControlMode.LastMonth,
                        DateRangeControlMode.PrevMonth
                    })
                .AddDateRange("Диапазон дат (предустановленный)", "DATE_RANGE3",
                    mode: DateRangeControlMode.Range,
                    dateFrom: DateTime.Today.AddMonths(-1),
                    availableDateRanges: new[]
                    {
                        DateRangeControlMode.Range
                    })
                .AddSingleCheckbox("Краткий вывод", "IS_SHORT", true)
                .AddDiscreteSelector("Селектор 1 (Checkbox)", "SELECTOR_1", DiscreteSelectorType.Checkbox,
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(1, "Параметр 1", 1, null, false),
                        new ReportSettingsValue(2, "Параметр 2", 2, null, false),
                        new ReportSettingsValue(3, "Параметр 3", 3, null, false),
                        new ReportSettingsValue(4, "Параметр 4", 4, null, false)
                    },
                    true, 2, 2,
                    new[] { 2, 4 })
                .AddDiscreteSelector("Селектор 2 (Radio)", "SELECTOR_2", DiscreteSelectorType.Radio,
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(1, "Параметр 1", 1, null, false),
                        new ReportSettingsValue(2, "Параметр 2", 2, null, false),
                        new ReportSettingsValue(3, "Параметр 3", 3, null, false),
                        new ReportSettingsValue(4, "Параметр 4", 4, null, false)
                    },
                    true, 4, 3,
                    new[] { 3 })
                .AddDiscreteSelector("Селектор 2 (Combo)", "SELECTOR_3", DiscreteSelectorType.Combo,
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(1, "Параметр 1", 1, null, false),
                        new ReportSettingsValue(2, "Параметр 2", 2, null, false),
                        new ReportSettingsValue(3, "Параметр 3", 3, null, false),
                        new ReportSettingsValue(4, "Параметр 4", 4, null, false)
                    },
                    true, 4, 3,
                    new[] { 4 })
                .AddNumberRange("Диапазон чисел", "NUMBERS_1",
                    decimalPlaces: 2,
                    minValue: 0, maxValue: 100,
                    numberRangeType: NumberRangeType.Dual,
                    valueFrom: 11.11M, valueTo: 33.33M)
                .AddNumberRange("Число", "NUMBER_2",
                    decimalPlaces: 0,
                    minValue: -99, maxValue: 99,
                    numberRangeType: NumberRangeType.Single,
                    valueFrom: 10M)
                .AddListSelect("Справочник 1 (List)", "LIST_1",
                    topics,
                    true, allEnable: true, allSelected: true)
                .AddListSelect("Справочник 2 (List single)", "LIST_2",
                    topics,
                    false, SelectionMode.Single)
                .AddTreeSelect("Иерархический справочник", "TREE_1",
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(3, "Элемент 0.3.", 2, "0.3.", true),
                        new ReportSettingsValue(5, "Элемент 0.2.", 1, "0.2.", true),
                        new ReportSettingsValue(12, "Элемент 0.1.", 3, "0.1.", true),
                        new ReportSettingsValue(34, "Элемент 0.3.3.", 2, "0.3.3.", false),
                        new ReportSettingsValue(87, "Элемент 0.3.2.", 1, "0.3.2.", true),
                        new ReportSettingsValue(22, "Элемент 0.3.2.1.", 2, "0.3.2.1.", false),
                        new ReportSettingsValue(1234563, "Элемент 0.3.1.", 3, "0.3.1.", true),
                        new ReportSettingsValue(654, "Элемент 0.3.1.1.", 200, "0.3.1.1.", false)
                    },
                    true)
                .AddTreeSelect("Иерархический справочник (только один лист)", "TREE_2",
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(3, "Элемент 0.3.", 2, "0.3.", true),
                        new ReportSettingsValue(5, "Элемент 0.2.", 1, "0.2.", true),
                        new ReportSettingsValue(12, "Элемент 0.1.", 3, "0.1.", true),
                        new ReportSettingsValue(34, "Элемент 0.3.3.", 2, "0.3.3.", false),
                        new ReportSettingsValue(87, "Элемент 0.3.2.", 1, "0.3.2.", true),
                        new ReportSettingsValue(22, "Элемент 0.3.2.1.", 2, "0.3.2.1.", false),
                        new ReportSettingsValue(1234563, "Элемент 0.3.1.", 3, "0.3.1.", true),
                        new ReportSettingsValue(654, "Элемент 0.3.1.1.", 200, "0.3.1.1.", false)
                    },
                    true, TreeListElementSelection.Leaves, SelectionMode.Single)
                .AddTreeSelect("Иерархический справочник (только одну вершину)", "TREE_3",
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(3, "Элемент 0.3.", 2, "0.3.", true),
                        new ReportSettingsValue(5, "Элемент 0.2.", 1, "0.2.", true),
                        new ReportSettingsValue(12, "Элемент 0.1.", 3, "0.1.", true),
                        new ReportSettingsValue(34, "Элемент 0.3.3.", 2, "0.3.3.", false),
                        new ReportSettingsValue(87, "Элемент 0.3.2.", 1, "0.3.2.", true),
                        new ReportSettingsValue(22, "Элемент 0.3.2.1.", 2, "0.3.2.1.", false),
                        new ReportSettingsValue(1234563, "Элемент 0.3.1.", 3, "0.3.1.", true),
                        new ReportSettingsValue(654, "Элемент 0.3.1.1.", 200, "0.3.1.1.", false)
                    },
                    true, TreeListElementSelection.Nodes, SelectionMode.Single)
                .Build;
        }

        public override async Task<ExecuteReportResult> Execute(IServiceProvider servicesProvider, ReportSettings settings)
        {
            var result = new ExecuteReportResult(Id);

            var settingsErrors = TestSettings(servicesProvider, await GetSettings(servicesProvider), settings)
                .ToArray();

            if (settingsErrors.Any())
            {
                result.Errors.AddRange(settingsErrors);
                return result;
            }

            result.Template = "<h1>Тестирование прошло успешно</h1>";

            return result;
        }
    }
}
