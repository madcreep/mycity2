using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Reports;
using BIT.MyCity.Subsystems;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.XlsIO;

namespace BIT.TestReport
{
    public class Report1 : BaseReport
    {
        private const string ResourceName = "Report.xltx";

        public override string SubsystemUid => SubsystemCitizenAppeals.BaseSystemKey;

        public override Guid Id { get; } = Guid.Parse("0380493F-141D-4A13-9EF8-0F6D2BCA47B3");

        public override string Name => "Тестовый отчет (граждане)";

        public override string Description => "Отчет проверки работоспособности системы отчетов";


        public override async Task<ReportSettings> GetSettings(IServiceProvider servicesProvider)
        {
            var ctx = servicesProvider.GetRequiredService<DocumentsContext>();

            //var topics = await ctx.Topics
            //    .Where(el => el.Subsystem.UID == "citizens")
            //    .OrderBy(el => el.Weight)
            //    .Select(el => new ReportSettingsValue(el.Id, el.ShortName, el.Weight, null, false))
            //    .ToArrayAsync();

            var topics = new[]
            {
                new ReportSettingsValue(9, "Элемент 2", 2, null, false),
                new ReportSettingsValue(3, "Элемент 8", 8, null, false),
                new ReportSettingsValue(7, "Элемент 4", 4, null, false),
                new ReportSettingsValue(6, "Элемент 5", 5, null, false),
                new ReportSettingsValue(1, "Элемент 10", 10, null, false),
                new ReportSettingsValue(5, "Элемент 6", 6, null, false),
                new ReportSettingsValue(4, "Элемент 7", 7, null, false),
                new ReportSettingsValue(10, "Элемент 1", 1, null, false),
                new ReportSettingsValue(8, "Элемент 3", 3, null, false),
                new ReportSettingsValue(2, "Элемент 9", 9, null, false)
                
            };

            return new ReportSettingsBuilder(Id)
                .AddCustomText("Введите строку", "CUSTOM_TEXT1", false,
                    CustomTextMode.SingleRow, 255, value: "Всем привет!")
                .AddCustomText("Введите тест", "CUSTOM_TEXT2", true,
                    CustomTextMode.MultiRows, 2000, 5, "Всем привет!")
                .AddDateRange("Диапазон дат (обязательный)", "DATE_RANGE1", true,
                    DateRangeControlMode.Range)
                .AddDateRange("Диапазон дат (не обязательный)", "DATE_RANGE2", false,
                    DateRangeControlMode.PlusNAndLater, -3,
                    availableDateRanges: new[]
                    {
                        DateRangeControlMode.Range,
                        DateRangeControlMode.CurrentDayPlusN,
                        DateRangeControlMode.PlusNAndLater,
                        DateRangeControlMode.PlusNAndBefore,
                        DateRangeControlMode.CurrentMonth,
                        DateRangeControlMode.LastMonth,
                        DateRangeControlMode.PrevMonth
                    })
                .AddDateRange("Диапазон дат (предустановленный)", "DATE_RANGE3",
                    mode: DateRangeControlMode.Range,
                    dateFrom: DateTime.Today.AddMonths(-1),
                    availableDateRanges: new[]
                    {
                        DateRangeControlMode.Range
                    })
                .AddSingleCheckbox("Краткий вывод", "IS_SHORT", true)
                .AddDiscreteSelector("Селектор 1 (Checkbox)", "SELECTOR_1", DiscreteSelectorType.Checkbox,
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(1, "Параметр 1", 1, null, false),
                        new ReportSettingsValue(2, "Параметр 2", 2, null, false),
                        new ReportSettingsValue(3, "Параметр 3", 3, null, false),
                        new ReportSettingsValue(4, "Параметр 4", 4, null, false)
                    },
                    true, 2, 2,
                    new[] { 2, 4 })
                .AddDiscreteSelector("Селектор 2 (Radio)", "SELECTOR_2", DiscreteSelectorType.Radio,
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(1, "Параметр 1", 1, null, false),
                        new ReportSettingsValue(2, "Параметр 2", 2, null, false),
                        new ReportSettingsValue(3, "Параметр 3", 3, null, false),
                        new ReportSettingsValue(4, "Параметр 4", 4, null, false)
                    },
                    true, 4, 3,
                    new[] { 3 })
                .AddDiscreteSelector("Селектор 2 (Combo)", "SELECTOR_3", DiscreteSelectorType.Combo,
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(1, "Параметр 1", 1, null, false),
                        new ReportSettingsValue(2, "Параметр 2", 2, null, false),
                        new ReportSettingsValue(3, "Параметр 3", 3, null, false),
                        new ReportSettingsValue(4, "Параметр 4", 4, null, false)
                    },
                    true, 4, 3,
                    new[] { 4 })
                .AddNumberRange("Диапазон чисел", "NUMBERS_1",
                    decimalPlaces: 2,
                    minValue: 0, maxValue: 100,
                    numberRangeType: NumberRangeType.Dual,
                    valueFrom: 11.11M, valueTo: 33.33M)
                .AddNumberRange("Число", "NUMBER_2",
                    decimalPlaces: 0,
                    numberRangeType: NumberRangeType.Single,
                    valueFrom: 10M)
                .AddListSelect("Справочник 1 (List)", "LIST_1",
                    topics,
                    true, allEnable: true, allSelected: true)
                .AddListSelect("Справочник 2 (List single)", "LIST_2",
                    topics,
                    false, SelectionMode.Single)
                .AddTreeSelect("Иерархический справочник", "TREE_1",
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(3, "Элемент 0.3.", 2, "0.3.", true),
                        new ReportSettingsValue(5, "Элемент 0.2.", 1, "0.2.", true),
                        new ReportSettingsValue(12, "Элемент 0.1.", 3, "0.1.", true),
                        new ReportSettingsValue(34, "Элемент 0.3.3.", 2, "0.3.3.", false),
                        new ReportSettingsValue(87, "Элемент 0.3.2.", 1, "0.3.2.", true),
                        new ReportSettingsValue(22, "Элемент 0.3.2.1.", 2, "0.3.2.1.", false),
                        new ReportSettingsValue(1234563, "Элемент 0.3.1.", 3, "0.3.1.", true),
                        new ReportSettingsValue(654, "Элемент 0.3.1.1.", 200, "0.3.1.1.", false)
                    },
                    true)
                .AddTreeSelect("Иерархический справочник (только один лист)", "TREE_2",
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(3, "Элемент 0.3.", 2, "0.3.", true),
                        new ReportSettingsValue(5, "Элемент 0.2.", 1, "0.2.", true),
                        new ReportSettingsValue(12, "Элемент 0.1.", 3, "0.1.", true),
                        new ReportSettingsValue(34, "Элемент 0.3.3.", 2, "0.3.3.", false),
                        new ReportSettingsValue(87, "Элемент 0.3.2.", 1, "0.3.2.", true),
                        new ReportSettingsValue(22, "Элемент 0.3.2.1.", 2, "0.3.2.1.", false),
                        new ReportSettingsValue(1234563, "Элемент 0.3.1.", 3, "0.3.1.", true),
                        new ReportSettingsValue(654, "Элемент 0.3.1.1.", 200, "0.3.1.1.", false)
                    },
                    true, TreeListElementSelection.Leaves, SelectionMode.Single)
                .AddTreeSelect("Иерархический справочник (только одну вершину)", "TREE_3",
                    new List<ReportSettingsValue>
                    {
                        new ReportSettingsValue(3, "Элемент 0.3.", 2, "0.3.", true),
                        new ReportSettingsValue(5, "Элемент 0.2.", 1, "0.2.", true),
                        new ReportSettingsValue(12, "Элемент 0.1.", 3, "0.1.", true),
                        new ReportSettingsValue(34, "Элемент 0.3.3.", 2, "0.3.3.", false),
                        new ReportSettingsValue(87, "Элемент 0.3.2.", 1, "0.3.2.", true),
                        new ReportSettingsValue(22, "Элемент 0.3.2.1.", 2, "0.3.2.1.", false),
                        new ReportSettingsValue(1234563, "Элемент 0.3.1.", 3, "0.3.1.", true),
                        new ReportSettingsValue(654, "Элемент 0.3.1.1.", 200, "0.3.1.1.", false)
                    },
                    true, TreeListElementSelection.Nodes, SelectionMode.Single)
                .Build;
        }

        public override async Task<ExecuteReportResult> Execute(IServiceProvider servicesProvider, ReportSettings settings)
        {
            var result = new ExecuteReportResult(Id);

            var defSettings = await GetSettings(servicesProvider);

            var settingsErrors = TestSettings(servicesProvider, defSettings, settings)
                .ToArray();

            if (settingsErrors.Any())
            {
                result.Errors.AddRange(settingsErrors);
                return result;
            }

            var assembly = Assembly.GetExecutingAssembly();

            var resourceName = $"{typeof(Report1).Namespace}.{ResourceName}";

            using var excelEngine = new ExcelEngine();

            var application = excelEngine.Excel;

            application.DefaultVersion = ExcelVersion.Excel2013;

            IWorkbook workbook;

            await using (var docStream = assembly.GetManifestResourceStream(resourceName))
            {
                workbook = application.Workbooks.Open(docStream);
            }

            var worksheet = workbook.Worksheets.First();

            worksheet.Range[2, 1].Text = $"Смещение локального времени : {settings.TimeOffset}";

            var row = 3;

            foreach (var parameter in defSettings.Parameters)
            {
                row++;

                var newParameter = settings.Parameters.Single(el => el.Name == parameter.Name);

                worksheet.Range[row, 1].Text = parameter.Title ?? string.Empty;
                worksheet.Range[row, 2].Text = parameter.Name ?? string.Empty;
                worksheet.Range[row, 3].Text = newParameter.Value ?? string.Empty;
                worksheet.Range[row, 4].Text = newParameter.AllSelected?.ToString() ?? string.Empty;
                worksheet.Range[row, 5].Text = newParameter.Options ?? string.Empty;
            }

            var range = worksheet.Range[3, 1, row, 1];
            range.HorizontalAlignment = ExcelHAlign.HAlignLeft;
            range.VerticalAlignment = ExcelVAlign.VAlignCenter;

            range = worksheet.Range[3, 2, row, 4];
            range.HorizontalAlignment = ExcelHAlign.HAlignCenter;
            range.VerticalAlignment = ExcelVAlign.VAlignCenter;

            range = worksheet.Range[3, 5, row, 5];
            range.HorizontalAlignment = ExcelHAlign.HAlignLeft;
            range.VerticalAlignment = ExcelVAlign.VAlignCenter;


            range = worksheet.Range[3, 1, row, 5];
            range.Borders[ExcelBordersIndex.EdgeTop].LineStyle = ExcelLineStyle.Thin;
            range.Borders[ExcelBordersIndex.EdgeRight].LineStyle = ExcelLineStyle.Thin;
            range.Borders[ExcelBordersIndex.EdgeBottom].LineStyle = ExcelLineStyle.Thin;
            range.Borders[ExcelBordersIndex.EdgeLeft].LineStyle = ExcelLineStyle.Thin;

            await using (var stream = new MemoryStream())
            {
                workbook.SaveAs(stream);

                result.File = await SaveFile(servicesProvider,
                    $"Тестовый отчет {DateTime.Now.ToString("dd_MM_yyyy HH_mm_ss", CultureInfo.InvariantCulture)}.xlsx",
                    stream);
            }

            if (result.File == null)
            {
                result.Errors.Add("Ошибка при сохранении файла.");

                return result;
            }

            var fileManager = servicesProvider.GetRequiredService<AttachedFileManager>();

            result.Template = TemplateGenerator.Generate(result.File, fileManager);

            if (!string.IsNullOrWhiteSpace(result.Template))
            {
                return result;
            }

            result.Errors.Add("Ошибка при генерации разметки.");

            return result;

            //var sb = new StringBuilder();

            //sb.Append("<style>");
            //sb.Append("td:nth-child(1) {text-align: left;vertical-align: middle;}");
            //sb.Append("td:nth-child(2) {text-align: center;vertical-align: middle;}");
            //sb.Append("td:nth-child(3) {text-align: center;vertical-align: middle;}");
            //sb.Append("td:nth-child(4) {text-align: center;vertical-align: middle;}");
            //sb.Append("td:nth-child(5) {text-align: left;vertical-align: middle;}");
            //sb.Append("</style>");

            //sb.Append("<h1>Тестирование прошло успешно</h1>");

            //sb.Append($"<p>Смещение локального времени : {settings.TimeOffset}</p>");

            //sb.Append("<table border='1' cellspacing='0' cellpadding='7' width='100%'>");
            //sb.Append("<tr><th>Название</th><th>Имя параметра</th><th>Значение</th><th>Все</th><th>Опции</th></tr>");
            //foreach (var parameter in defSettings.Parameters)
            //{
            //    sb.Append("<tr>");
            //    sb.Append($"<td>{parameter.Title}</td>");
            //    sb.Append($"<td>{parameter.Name}</td>");

            //    var newParameter = settings.Parameters.Single(el => el.Name == parameter.Name);

            //    sb.Append($"<td>{newParameter.Value}</td>");
            //    sb.Append($"<td>{newParameter.AllSelected}</td>");
            //    sb.Append($"<td>{newParameter.Options}</td>");
            //    sb.Append("</tr>");
            //}

            //sb.Append("</table>");

            //result.Template = sb.ToString();

            //var stream = new MemoryStream();
            //var writer = new StreamWriter(stream);
            //await writer.WriteAsync(result.Template);
            //await writer.FlushAsync();
            //stream.Position = 0;

            //result.File = await SaveFile(servicesProvider,
            //    $"Тестовый отчет {DateTime.Now.ToString("dd_MM_yyyy HH_mm_ss", CultureInfo.InvariantCulture)}.docx",
            //    stream);

            //if (result.File == null)
            //{
            //    throw new Exception("Ошибка сохранения файла отчета.");
            //}

            //return result;
        }


    }
}
