using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using BIT.MyCity.Reports;
using BIT.MyCity.Services;
using BIT.MyCity.Database;
using log4net.DateFormatter;

namespace BIT.MyCity.NewAppealsReport
{
    public class NewAppealsReport : BaseReport
    {
        public override string Name
        {
            get { return "Поступившие за период"; }
        }

        public override string Description
        {
            get
            {
                return @"Обращения, поступившие в систему за указанный период времени.
        Параметры:
            - StartDate - начало периода, дата;
            - EddDate - конец периода, дата;
            - Rubric - идентификатор рубрики или массив таких идентификаторов (необязательый);
            - Topic - идентификатор темы обращения или массив таких идентификаторов (необязательный);
            - Status - статус обращения или массив статусов (необязательный);
            - ShowTopics - разбивать ли статистику обращений по столбцам, соответствующим темам (необязательный);
            - ShowStatus - разбивать ли статистику обращений по столбцам, соответствующим статусам (необязательный);
            - ShowStatus - разбивать ли статистику обращений по столбцам, соответствующим публичности (необязательный);
            - Extended - разбивать ли статистику обращений по столбцам, соответствующим месяцам (необязательный).";
            }
        }

        public override Guid Id
        {
            get { return Guid.Parse("ff637d20-88f4-4789-814c-b06a19e4c1de"); }
        }

        private IDictionary<string, Parameter> _parameters;

        public override IDictionary<string, Parameter> Parameters
        {
            get { return _parameters; }
        }

        public override ResultType ResultType
        {
            get { return ResultType.Both; }
        }

        public NewAppealsReport(ReportService service) : base(service)
        {
            _parameters = new Dictionary<string, Parameter>();
            _parameters.Add("StartDate", new Parameter("StartDate", ParamType.Date, RangeTypes.None, false, true));
            _parameters.Add("EndDate", new Parameter("EndDate", ParamType.Date, RangeTypes.None, false, true));
            _parameters.Add("Rubric", new Parameter("Rubric", ParamType.ID, RangeTypes.Array, false, false));
            _parameters.Add("Topic", new Parameter("Topic", ParamType.ID, RangeTypes.Array, false, false));
            _parameters.Add("Status", new Parameter("Status", ParamType.Number, RangeTypes.Array, false, false));
            _parameters.Add("ShowTopics",
                new Parameter("ShowTopics", ParamType.Boolean, RangeTypes.None, false, false));
            _parameters.Add("ShowStatus",
                new Parameter("ShowStatus", ParamType.Boolean, RangeTypes.None, false, false));
            _parameters.Add("ShowPublic",
                new Parameter("ShowPublic", ParamType.Boolean, RangeTypes.None, false, false));
            _parameters.Add("Extended", new Parameter("Extended", ParamType.Boolean, RangeTypes.None, false, false));
        }

        public override async Task<object> Execute(bool excel)
        {
            //var stats = _service.AppealStats
            //    .Where(st =>
            //        st.CreatedAt.HasValue && st.CreatedAt.Value.CompareTo(_parameters["StartDate"].Value) >= 0 &&
            //        st.CreatedAt.Value.CompareTo(_parameters["EndDate"].Value) >= 0 && !st.Deleted);
            //var whereParams = new[] {"Rubric", "Topic", "Status"};
            //foreach (var parameter in _parameters)
            //{
            //    if (!whereParams.Contains(parameter.Key)) continue;
            //    if (parameter.Value.Value == null) continue;

            //    if (parameter.Value.Value is IEnumerable)
            //    {
            //        stats = stats.Where(s =>
            //            (parameter.Value.Value as IEnumerable<object>).Contains(s.GetType()
            //                .GetProperty(parameter.Value.Name).GetValue(s)));
            //    }
            //    else
            //    {
            //        stats = stats.Where(s => s.GetType()
            //                                     .GetProperty(parameter.Value.Name).GetValue(s) ==
            //                                 parameter.Value.Value);
            //    }
            //}

            //stats = stats
            //    .Select(st =>
            //        new AppealStats
            //        {
            //            Approved = st.Approved,
            //            Deleted = false,
            //            Public = (bool)_parameters["ShowPublic"].Value && st.Public,
            //            Region = null,
            //            Topic = (bool)_parameters["ShowTopic"].Value ? st.Topic : null,
            //            Territory = null,
            //            Rubric = st.Rubric,
            //            Subsystem = null,
            //            CreatedAt = (bool)_parameters["Extended"].Value ? new DateTime(st.CreatedAt.Value.Year, st.CreatedAt.Value.Month, 1) : default,
            //            SiteStatus = (bool)_parameters["ShowStatus"].Value ? st.SiteStatus : 0, 
            //            SentToDelo = st.SentToDelo,
            //            AppealsCount = st.AppealsCount

            //        });
            return null;
        }
    }
}
