using System.IO;
using System.Threading.Tasks;
using RazorLight;

namespace BIT.MyCity.MailSender
{
    public static class RazorEngine
    {
        public static async Task<string> RenderAsync<TModel>(string key, string templateName, TModel model)
        {
            var engine = new RazorLightEngineBuilder()
                .UseMemoryCachingProvider()
                .UseEmbeddedResourcesProject(typeof(Program)) 
                .Build();
            var cacheResult = engine.Handler.Cache.RetrieveTemplate(key);
            if(cacheResult.Success)
            {
                return await engine.RenderTemplateAsync(cacheResult.Template.TemplatePageFactory(), model);
            }

            var path = Path.Combine(".", templateName + ".html");
            var template = File.ReadAllText(path);
            return await engine.CompileRenderStringAsync(key, template, model);
        }   
    }
}
