using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Shared;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace BIT.MyCity.MailSender
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;

        private readonly SmtpClient _smtpClient = new SmtpClient();
        //private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;
        
//        private static readonly RazorLightEngine Engine = new RazorLightEngineBuilder()
//            // required to have a default RazorLightProject type, but not required to create a template from string.
//            .UseEmbeddedResourcesProject(typeof(Program)) 
//            .UseMemoryCachingProvider()
//            .Build();
        
        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
            //_razorViewToStringRenderer = razorViewToStringRenderer;
            _smtpClient.Host = GlobalProperties.Host;
            _smtpClient.Port = GlobalProperties.Port;
            _smtpClient.EnableSsl = GlobalProperties.EnableSsl;
            _smtpClient.UseDefaultCredentials = GlobalProperties.UseDefaultCredentials;
            if (!_smtpClient.UseDefaultCredentials)
            {
                _smtpClient.Credentials = new NetworkCredential(GlobalProperties.Username, GlobalProperties.Password);
            }

        }

        private static string GetTemplate(string templateName)
        {
            var path = Path.Combine(".", templateName + ".html");
            return File.Exists(path) ? File.ReadAllText(path, Encoding.UTF8) : null;
        }

        private async Task<string> GenerateBody(MailTask mailInfo)
        {
            return await RazorEngine.RenderAsync<MailTask>(mailInfo.TemplateName, mailInfo.TemplateName, mailInfo);
        }
        
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            uint id = 1;
            while (!stoppingToken.IsCancellationRequested)
            {
                MessagesRepository<MailTask>.Messages.Enqueue(new MailTask 
                {
                    Data = JObject.FromObject(
                        new User
                        {
                            Id = id++, FullName = "Hello world", UserName = "Гражданинский" + id
                        }),
                    TemplateName = "Citizen",
                    Addressee = "s@biz-it.ru",
                    Text = "Гражданинский" + id
                });
                if (MessagesRepository<MailTask>.Messages.TryDequeue(out var mailTask))
                {
                    var body = await GenerateBody(mailTask);
                    string title = Regex.Match(body, @"\<title\b[^>]*\>\s*(?<Title>[\s\S]*?)\</title\>",
                        RegexOptions.IgnoreCase).Groups["Title"].Value;
                    var mailMessage = new MailMessage(GlobalProperties.From, mailTask.Addressee)
                    {
                        Subject = title, 
                        Body = body,
                        IsBodyHtml = true
                    };
                    Console.WriteLine(title);
                    Console.WriteLine(body);
                    try
                    {
//                        _smtpClient.Send(mailMessage);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Error sending message to {mailTask.Addressee}", ex);
                        MessagesRepository<MailTask>.Messages.Enqueue(mailTask);
                        await Task.Delay(1000, stoppingToken);
                    }
                }

                if (MessagesRepository<MailTask>.Messages.Count <= 0)
                {
                    await Task.Delay(1000, stoppingToken);
                }
            }
        }
    }
    
}
