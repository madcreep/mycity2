using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Shared;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace BIT.MyCity.MailSender
{
    [ApiController]
    [Route("mail")]
    public class SenderController : ControllerBase
    {
        private MailContext _context;
        
        public SenderController(MailContext context)
        {
            _context = context;
        }


        [HttpPost, Route("add/user")]
        public async Task<IActionResult> AddUserMail([FromBody] User user, string templateSubject, string addressee)
        {

            var entityHandler =
                await _context.Tasks.AddAsync(new MailTask
                {
                    Data = JObject.FromObject(user),
                    TemplateName = templateSubject,
                    Addressee = addressee
                });

            await _context.SaveChangesAsync();

            MessagesRepository<MailTask>.Messages.Enqueue(entityHandler.Entity);

            return Ok();
        }

        [HttpGet, Route("test")]
        public async Task<IActionResult> Test()
        {
            return Ok(new {Id = "1", Data = "2"});
        }
    }
}
