using BIT.MyCity.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Migrations.Internal;

namespace BIT.MyCity.MailSender
{
    public class PostgreHistoryRepository : NpgsqlHistoryRepository
    {
        public PostgreHistoryRepository(HistoryRepositoryDependencies dependencies)
            : base(dependencies)
        {
        }

        protected override void ConfigureTable(EntityTypeBuilder<HistoryRow> history)
        {
            base.ConfigureTable(history);
            history.Property(h => h.MigrationId).HasColumnName("migration_id");
            history.Property(h => h.ProductVersion).HasColumnName("product_version");
        }
    }

    public class MailContext : DbContext
    {
        public DbSet<MailTask> Tasks { get; set; }
//        public DbSet<MailTask<Citizen>>  CitizenTasks { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.ReplaceService<IHistoryRepository, PostgreHistoryRepository>()
                .UseNpgsql(GlobalProperties.ConnectionString,
                    options => options.MigrationsHistoryTable("_ef_migrations", "public")
                );
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            modelBuilder.Entity<MailTask>()
                .Property<string>("ExtendedData")
                .HasField("ExtendedData");
            //            modelBuilder.Entity<MailTask<Citizen>>().HasIndex(r => r.Id);
            
            base.OnModelCreating(modelBuilder);

        }
    }
}
