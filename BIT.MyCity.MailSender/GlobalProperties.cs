namespace BIT.MyCity.MailSender
{
    public class GlobalProperties
    {
        public static string ConnectionString { get; set; }
        public static string Host { get; set; }
        public static int Port { get; set; }
        public static bool EnableSsl { get; set; }
        public static bool UseDefaultCredentials { get; set; }
        public static string Username { get; set; }
        public static string Password { get; set; }
        public static string From { get; set; }
        
    }
}