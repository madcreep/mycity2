using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace BIT.MyCity.MailSender
{
    public class Startup
    {
        
        public Startup(IConfiguration configuration, IHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IHostEnvironment Environment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            GlobalProperties.ConnectionString = Configuration.GetConnectionString("MainDatabase");
            GlobalProperties.From = Configuration["From"];
            GlobalProperties.Host = Configuration["Host"];
            GlobalProperties.Port = int.Parse(Configuration["Port"]);
            GlobalProperties.EnableSsl = bool.Parse(Configuration["EnableSsl"]);
            GlobalProperties.UseDefaultCredentials = bool.Parse(Configuration["UseDefaultCredentials"]);
            GlobalProperties.Username = Configuration["Username"];
            GlobalProperties.Password = Configuration["Password"];
            //
            services.AddDbContext<MailContext>();
            services.AddHostedService<Worker>();
            
            //services.AddScoped<IRazorViewToStringRenderer, RazorViewToStringRenderer>();
            services.AddControllers();
            //services.AddControllersWithViews();
            services.AddRazorPages();
        }

        public void Configure(IApplicationBuilder app, IHostEnvironment env,
            ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            
            app.UseRouting();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapRazorPages();
            });
        }
    }
}