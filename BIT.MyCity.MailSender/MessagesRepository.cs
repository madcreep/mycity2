using System.Collections.Concurrent;

namespace BIT.MyCity.MailSender
{
    public static class MessagesRepository<T>
    {
        public static ConcurrentQueue<T> Messages { get; } = new ConcurrentQueue<T>();
        
    }
}