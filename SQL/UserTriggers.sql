-- BEFORE UPDATE

DROP TRIGGER bu_users_trigger ON public."AspNetUsers";

DROP FUNCTION public.user_before_update_func();

CREATE FUNCTION public.user_before_update_func() RETURNS trigger AS $user_before_update_func$
BEGIN
	NEW."UpdatedAt" := current_timestamp;
	RETURN NEW;
END;
$user_before_update_func$ LANGUAGE 'plpgsql';

ALTER FUNCTION public.user_before_update_func()
    OWNER TO postgres;
	
CREATE TRIGGER bu_users_trigger BEFORE UPDATE ON public."AspNetUsers"
    FOR EACH ROW EXECUTE PROCEDURE public.user_before_update_func();
	
-- BEFORE INSERT
	
DROP TRIGGER bi_users_trigger ON public."AspNetUsers";

DROP FUNCTION public.user_before_insert_func();

CREATE FUNCTION public.user_before_insert_func() RETURNS trigger AS $user_before_insert_func$
BEGIN
	NEW."CreatedAt" := current_timestamp;
	RETURN NEW;
END;
$user_before_insert_func$ LANGUAGE 'plpgsql';

ALTER FUNCTION public.user_before_insert_func()
    OWNER TO postgres;
	
CREATE TRIGGER bi_users_trigger BEFORE INSERT ON public."AspNetUsers"
    FOR EACH ROW EXECUTE PROCEDURE public.user_before_insert_func();
