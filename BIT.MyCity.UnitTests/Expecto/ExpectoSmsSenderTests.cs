using System;
using BIT.MyCity.Sms.Models;
using BIT.MyCity.SMSHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BIT.MyCity.UnitTests.Expecto
{
    [TestClass]
    public class ExpectoSmsSenderTests
    {
        private const string _login = "ex15928928058";
        private const string _password = "218505";
        private const string _sender = "PROFI";

        [TestMethod]
        public void SendMessageTest()
        {
            var helper = new ExpectoHelper(_login, _password, _sender);

            const string text = "Проверка sms сообщения ЭКСПЕКТО для ActiveCitizen. Код 123456. Тихомиров Эмиль";

            var message = new SmsMessage
            {
                PhoneNumber = "+7(905)4122269",
                //PhoneNumber = "+79283213770",
                //PhoneNumber = "+78652764410",
                Message = text
            };

            var task = helper.SendAsync(message);

            task.Wait();

            var sendResult = task.Result;

            Assert.IsNotNull(sendResult, "Результат отправки должен быть");

            Console.WriteLine("СМС Id: {0}; Статус: {1}; Ошибка: {2}",
                sendResult.SmsId,
                sendResult.Status,
                sendResult.ErrorMessage);

            Assert.AreEqual(SmsMessageStatus.Accepted, sendResult.Status, $"Статус отправки должен быть <{SmsMessageStatus.Accepted}>");
        }

        [TestMethod]
        public void GetStatusTest()
        {
            var helper = new ExpectoHelper(_login, _password, _sender);

            var smsId = new[]
            {
                "5473132758",
                "5473137153",
                "5473150617"
            };

            var task = helper.GetStatusAsync(smsId);

            task.Wait();

            var sendResult = task.Result;

            Assert.IsNotNull(sendResult, "Результат должен быть");

            foreach (var item in sendResult)
            {
                Console.WriteLine("СМС Id: {0}; Статус: {1}; Ошибка: {2}",
                    item.SmsId,
                    item.Status,
                    item.ErrorMessage);
            }
        }

        [TestMethod]
        public void GetBalanceTest()
        {
            var helper = new ExpectoHelper(_login, _password, _sender);

            var result = helper.Balance;

            Assert.IsNotNull(result, "Результат должен быть");


            Console.WriteLine($"Успех: {result.Success}");
            if (result.Success)
            {
                Console.WriteLine("Баланс: Валюта:{0}; Сумма: {1}; Количество СМС: {2}",
                    result.Currency,
                    result.Amount,
                    result.SmsCount
                );
            }
            else
            {
                Console.WriteLine($"IsUnauthorized: {result.IsUnauthorized}; ErrorMessage: {result.ErrorMessage};");
            }

            Assert.IsTrue(result.Success, "Результат должен быть положительный");
        }
    }
}
