using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BIT.MyCity.UnitTests.Settings
{
    [TestClass]
    public class SettingsTests
    {
        [TestMethod]
        public void SettingsValueItem_nullable_Test()
        {
            try
            {
                var test = new SettingsValueItem<decimal?>(null, "Test");
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }

        [TestMethod]
        public void SettingsValueItem_decimal_Test()
        {
            try
            {
                var test = new SettingsValueItem<decimal>(123m, "Test");
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }

        [TestMethod]
        public void SettingsValueItem_string_Test()
        {
            try
            {
                var test = new SettingsValueItem<string>("123m", "Test");
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }

        [TestMethod]
        public void SettingsValueItem_enum_Test()
        {
            try
            {
                var test = new SettingsValueItem<SettingNodeType>(SettingNodeType.Group, "Test");
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }

            var v = SettingNodeType.Group.Equals(SettingNodeType.Value);
        }

        [TestMethod]
        public void SettingsValueItem_classEquatable_Test()
        {
            try
            {
                var test = new SettingsValueItem<EquatableClass>(null, "Test");
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
        }

        [TestMethod]
        public void SettingsValueItem_classEquatableOther_Test()
        {
            try
            {
                var test = new SettingsValueItem<EquatableClassOther>(null, "Test");

                Assert.Fail("Должно вызываться исключение");
            }
            catch
            {
               //ignore
            }
        }

        [TestMethod]
        public void SettingsValueItem_classNotEquatable_Test()
        {
            try
            {
                var test = new SettingsValueItem<NotEquatableClass>(null, "Test");

                Assert.Fail("Должно вызываться исключение");
            }
            catch
            {
                //ignore
            }
        }

        [TestMethod]
        public void SettingsValueItem_Equals_Test()
        {
            var x = new[]
            {
                new SettingsValueItem<EquatableClass>(null, "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(1), "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(2), "Test"),
            };

            var y = new[]
            {
                new SettingsValueItem<EquatableClass>(null, "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(1), "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(2), "Test"),
            };

            Console.WriteLine(x.First().Value);

            Console.WriteLine(y.First().Value);

            var result = SettingsManager.ValuesEqual(x, y);

            Assert.IsTrue(result, "Объекты должны быть равны");
        }

        [TestMethod]
        public void SettingsValueItem_NotEquals_ByValue_Test()
        {
            var x = new[]
            {
                new SettingsValueItem<EquatableClass>(null, "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(1), "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(2), "Test")
            };

            var y = new[]
            {
                new SettingsValueItem<EquatableClass>(null, "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(1), "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(3), "Test")
            };

            var result = SettingsManager.ValuesEqual(x, y);

            Assert.IsFalse(result, "Объекты не должны быть равны");
        }

        [TestMethod]
        public void SettingsValueItem_NotEquals_ByLength_Test()
        {
            var x = new[]
            {
                new SettingsValueItem<EquatableClass>(null, "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(1), "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(2), "Test"),
            };

            var y = new[]
            {
                new SettingsValueItem<EquatableClass>(null, "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(1), "Test"),
            };

            var result = SettingsManager.ValuesEqual(x, y);

            Assert.IsFalse(result, "Объекты не должны быть равны");
        }

        [TestMethod]
        public void SettingsValueItem_NotEquals_ByType_Test()
        {
            var x = new[]
            {
                new SettingsValueItem<EquatableClass>(null, "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(1), "Test"),
                new SettingsValueItem<EquatableClass>(new EquatableClass(2), "Test"),
            };

            var y = new[]
            {
                new SettingsValueItem<EquatableClass1>(null, "Test"),
                new SettingsValueItem<EquatableClass1>(new EquatableClass1(1), "Test"),
                new SettingsValueItem<EquatableClass1>(new EquatableClass1(2), "Test"),
            };

            var result = SettingsManager.ValuesEqual(x, y);

            Assert.IsFalse(result, "Объекты не должны быть равны");
        }

        [TestMethod]
        public void SettingsValueItem_EnumParseTest()
        {
            var test = SettingNodeType.Group;

            var s1 = test.ToString();

            var s2 = ((int)test).ToString();

            if (!Enum.TryParse(typeof(SettingNodeType), s1, out var t1))
            {
                Assert.Fail("Not parse s1");
            }

            Console.WriteLine("Parse s1 ({0}) => {1}", s1, t1);

            if (!Enum.TryParse(typeof(SettingNodeType), s2, out var t2))
            {
                Assert.Fail("Not parse s2");
            }

            Console.WriteLine("Parse s2 ({0}) => {1}", s2, t2);
        }

        public class EquatableClass : IEquatable<EquatableClass>
        {
            public object Value { get; set; }

            public EquatableClass(object value)
            {
                Value = value;
            }

            public bool Equals(EquatableClass other)
            {
                Console.WriteLine($"EquatableClass.Equals : {Value}{other.Value}");

                return (Value == null && other.Value == null)
                    || (Value != null && Value.Equals(other.Value));
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((EquatableClass) obj);
            }

            public override int GetHashCode()
            {
                return Value.GetHashCode();
            }
        }

        public class EquatableClass1 : IEquatable<EquatableClass1>
        {
            private object _value;

            public EquatableClass1(object value)
            {
                _value = value;
            }

            public bool Equals(EquatableClass1 other)
            {
                return (_value == null && other._value == null)
                       || (_value != null && _value.Equals(other._value));
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((EquatableClass1)obj);
            }

            public override int GetHashCode()
            {
                return _value.GetHashCode();
            }
        }

        public class EquatableClassOther : IEquatable<NotEquatableClass>
        {
            public bool Equals(NotEquatableClass other)
            {
                throw new NotImplementedException();
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((EquatableClassOther) obj);
            }

            public override int GetHashCode()
            {
                throw new NotImplementedException();
            }
        }

        public class NotEquatableClass
        {

        }
    }
}



