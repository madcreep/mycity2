using System;
using System.IO;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Sms.Models;
using BIT.MyCity.SMSHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BIT.MyCity.UnitTests.StatusEmail
{
    [TestClass]
    public class HierrarchyTests
    {
        [TestMethod]
        public void CreateFileParts()
        {
            const int cluster = 4096;

            const string file = @"C:\tmp\Test.txt";

            using var stream = File.OpenRead(file);

            var buffer = new byte[cluster];
            
            var fileLength = stream.Length;

            var allReading = 0;
            
            Console.WriteLine($"Length : {fileLength}");

            do
            {
                var reading = stream.Read(buffer, 0, buffer.Length);

                allReading += reading;

                Console.WriteLine(new string('-', 30));

                Console.WriteLine(Convert.ToBase64String(buffer, 0, reading));
                
            } while (allReading < fileLength);
        }

        [TestMethod]
        public void CreateSettingsJsonTest()
        {
            var settings = new AppealStatusSettings
            {
                StatusOptions = new StatusOptions
                {
                    NextStatus = new[] { 2, 4, 8, 33 },
                    Email = new AppealStatusEmail
                    {
                        Send = true,
                        Template = "myTemplateName",
                        SendAdditional = new AppealStatusSendAdditional
                        {
                            SendFiles = true,
                            MessageKind = MessageKindEnum.SystemUserMessage.ToString(),
                            SendMessage = true
                        }
                    }
                }
            };

            Console.WriteLine(settings.SerializeJson());

            Enum.TryParse<MessageKindEnum>(settings.StatusOptions.Email.SendAdditional.MessageKind,
                out var kindMessage);

            Console.WriteLine("kindMessage = {0}", kindMessage);
        }

        [TestMethod]
        public void DeserializeSettingsJsonTest()
        {

            var json = @"{
""StatusOptions"": {
    ""Color"": ""red"",
    ""NextStatus"": [],
    ""Email"": {
        ""Template"": ""appealCitizen"",
        ""SendAdditional"": {
            ""SendFiles"": true,
            ""MessageKind"": ""SystemUserMessage"",
            ""SendMessage"": true
        }
    }
}
}";
            

            if (json.TryDeserializeJson<AppealStatusSettings>(out var result))
            {
                Console.WriteLine(result.StatusOptions.Email.SendAdditional.MessageKind);
            }

            Assert.IsNotNull(result, "Объект должен десериализоваться");
        }
    }
}



