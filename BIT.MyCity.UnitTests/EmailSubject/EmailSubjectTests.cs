using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Sms.Models;
using BIT.MyCity.SMSHelpers;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BIT.MyCity.UnitTests.EmailSubject
{
    [TestClass]
    public class EmailSubjectTests
    {
        [TestMethod]
        public void SubjectTemplateKeysTest()
        {
            var template = "Тестовое сообщение от {Appeal.CreatedAt}} № {Appeal.Id}";

            var result = GetSubjectTemplateKeys(template);

            Console.WriteLine(string.Join(Environment.NewLine, result));
        }

        [TestMethod]
        public void SubjectGenerationTest()
        {
            var message = new Message
            {
                Id = 123456,
                CreatedAt = DateTime.UtcNow,
                Author = new User
                {
                    FullName = "Иванов Иван"
                }
            };

            dynamic model = new ExpandoObject();

            model.Message = message;
            model.TestStr = "Тестовое сообщение";
            model.Number = 1234567890L;
            model.Enum = AppealTypeEnum.FEEDBACK;

            var keys = new[]
            {
                "{ TestStr}",
                "{Message.Id }",
                "{ Message.CreatedAt }",
                "{Message.Author.FullName}",
                "{Number}",
                "{Enum}",
                "{Message.Author.FullName1}",
                "{User.Author.FullName}"
            };

            var settings = new EmailTemplateSettings
            {
                DateTimeFormat = "dd.MM.yyyy"
                //DateTimeFormat = ""
            };

            Dictionary<string, string> result = GetKeysValues(keys, model, settings);

            foreach (var (key, value) in result)
            {
                Console.WriteLine("{0} : {1}", key, value);
            }
        }

        [TestMethod]
        public void EmailTemplateSettingsToJsonTest()
        {
            var test = new EmailTemplateSettings
            {
                SubjectTemplate = "SubjectTemplate {Appeal.Id}!",
                DateTimeFormat = "dd.MM.yyyy HH.mm.ss",
                EntityInclude = new Dictionary<string, string[]>
                {
                    {"Appel", new []
                    {
                        nameof(Appeal.Author),
                        nameof(Appeal.Messages)
                    }}
                }
            };


            var serializeJson = test.SerializeJson();

            Console.WriteLine(serializeJson);
        }

        private IEnumerable<string> GetSubjectTemplateKeys(string template)
        {
            const string pattern = "\\{.+?\\}";

            var regex = new Regex(pattern);

            var matces = regex.Matches(template);

            return matces
                .Select(el => el.Value)
                .Distinct()
                .ToArray();
        }

        private Dictionary<string, string> GetKeysValues(IEnumerable<string> keys, object model, EmailTemplateSettings settings)
        {
            return keys
                .ToDictionary(key => key, key =>
                    GetKeyValue(
                        key
                            .Trim('{', '}', ' ')
                            .Split('.'),
                        model,
                        settings));
        }

        private string GetKeyValue(string[] keys, object model, EmailTemplateSettings settings)
        {
            if (!keys.Any() || model == null)
            {
                return null;
            }

            var result = model;

            foreach (var key in keys)
            {
                var type = result.GetType();

                if (result is IDynamicMetaObjectProvider provider)
                {

                    try
                    {
                        var param = Expression.Parameter(typeof(object));

                        var metaObject = provider.GetMetaObject(param);

                        var binder = (GetMemberBinder)Microsoft.CSharp.RuntimeBinder.Binder
                            .GetMember(0, key, type, new[] { CSharpArgumentInfo.Create(0, null) });

                        var ret = metaObject.BindGetMember(binder);

                        var final = Expression.Block(
                            Expression.Label(CallSiteBinder.UpdateLabel),
                            ret.Expression
                        );

                        var lambda = Expression.Lambda(final, param);

                        var del = lambda.Compile();

                        result = del.DynamicInvoke(result);
                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                {
                    var propertyInfo = type.GetProperty(key);

                    if (propertyInfo == null)
                    {
                        return null;
                    }

                    result = propertyInfo.GetValue(result);

                    if (result == null)
                    {
                        return null;
                    }
                }
            }

            return result is DateTime date
                ? date.ToLocalTime().ToString(settings.DateTimeFormat)
                : result.ToString();
        }



    }
}



