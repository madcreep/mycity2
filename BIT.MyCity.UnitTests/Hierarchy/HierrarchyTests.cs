using System;
using System.Linq;
using BIT.MyCity.Database;
using BIT.MyCity.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BIT.MyCity.UnitTests.Hierarchy
{
    [TestClass]
    public class HierrarchyTests
    {
        private const string StattDue = "0.";
        private static string _base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        [TestMethod]
        public void CreateDueTest()
        {
            for (var i = 0L; i <= 10L; i++)
            {
                var index = i * 100000;
                Console.Write($"{index.ToString().PadLeft(8, '0')} => ");
                var due = string.Empty;
                while (index > 0)
                {
                    due += _base[(int)(index % _base.Length)];
                    index /= _base.Length;
                }
                if (string.IsNullOrEmpty(due))
                {
                    due = "0";
                }
                
                Console.WriteLine($"{due}");
            }
        }

        //[TestMethod]
        //public void RebaseHierarchyWithoutBaseTest()
        //{
        //    var testArray = new[]
        //    {
        //        new TestHierarchy(1, "A."),
        //        new TestHierarchy(2, "B."),
        //        new TestHierarchy(3, "C."),
        //        new TestHierarchy(4, "A.D."),
        //        new TestHierarchy(5, "A.5."),
        //        new TestHierarchy(6, "A.D.F."),
        //        new TestHierarchy(7, "A.D.F.7."),
        //        new TestHierarchy(8, "B.D.D.B.5."),
        //        new TestHierarchy(9, "C.H."),
        //        new TestHierarchy(10, "C.H.10"),
        //        new TestHierarchy(11, "LOE.H.10")
        //    };

        //    HierarchyHelper.RebaseHierarchy(testArray);

        //    foreach (var item in testArray.OrderBy(el=>el.Due))
        //    {
        //        Console.WriteLine(
        //            $"{item.Id.ToString().PadLeft(5)} => {item.Layer} : {item.IsNode.ToString().PadRight(5)} : {item.Due.PadRight(30)} : {item.ParentId.ToString().PadRight(5)} : {item.ParentType}");
        //    }
        //}

        //[TestMethod]
        //public void RebaseHierarchyWithBaseEqualClassTest()
        //{
        //    var baseEntity = new TestHierarchy(1124, "0.D%68.S6.");

        //    var testArray = new[]
        //    {
        //        new TestHierarchy(1, "A."),
        //        new TestHierarchy(2, "B."),
        //        new TestHierarchy(3, "C."),
        //        new TestHierarchy(4, "A.D."),
        //        new TestHierarchy(5, "A.5."),
        //        new TestHierarchy(6, "A.D.F."),
        //        new TestHierarchy(7, "A.D.F.7."),
        //        new TestHierarchy(8, "B.D.D.B.5."),
        //        new TestHierarchy(9, "C.H."),
        //        new TestHierarchy(10, "C.H.10"),
        //        new TestHierarchy(11, "LOE.H.10")
        //    };

        //    HierarchyHelper.RebaseHierarchy(testArray, baseEntity);

        //    Console.WriteLine($"{baseEntity.Id.ToString().PadLeft(5)} => {baseEntity.Layer} : {baseEntity.IsNode.ToString().PadRight(5)} : {baseEntity.ParentId.ToString().PadRight(5)} : {baseEntity.Due.PadRight(30)} : {baseEntity.ParentType}");

        //    Console.WriteLine(new string('-', 100));

        //    foreach (var item in testArray.OrderBy(el => el.Due))
        //    {
        //        Console.WriteLine($"{item.Id.ToString().PadLeft(5)} => {item.Layer} : {item.IsNode.ToString().PadRight(5)} : {item.Due.PadRight(30)} : {item.ParentId.ToString().PadRight(5)} : {item.ParentType}");
        //    }
        //}

        //[TestMethod]
        //public void RebaseHierarchyWithBaseNotEqualClassTest()
        //{
        //    var baseEntity = new BaseHierarchy(1124, "0.D%68.S6.");

        //    var testArray = new[]
        //    {
        //        new TestHierarchy(1, "A."),
        //        new TestHierarchy(2, "B."),
        //        new TestHierarchy(3, "C."),
        //        new TestHierarchy(4, "A.D."),
        //        new TestHierarchy(5, "A.5."),
        //        new TestHierarchy(6, "A.D.F."),
        //        new TestHierarchy(7, "A.D.F.7."),
        //        new TestHierarchy(8, "B.D.D.B.5."),
        //        new TestHierarchy(9, "C.H."),
        //        new TestHierarchy(10, "C.H.10"),
        //        new TestHierarchy(11, "LOE.H.10")
        //    };

        //    HierarchyHelper.RebaseHierarchy(testArray, baseEntity);

        //    Console.WriteLine($"{baseEntity.Id.ToString().PadLeft(5)} => {baseEntity.Layer} : {baseEntity.IsNode.ToString().PadRight(5)} : {baseEntity.ParentId.ToString().PadRight(5)} : {baseEntity.Due.PadRight(30)} : {baseEntity.ParentType}");

        //    Console.WriteLine(new string('-', 100));

        //    foreach (var item in testArray.OrderBy(el => el.Due))
        //    {
        //        Console.WriteLine($"{item.Id.ToString().PadLeft(5)} => {item.Layer} : {item.IsNode.ToString().PadRight(5)} : {item.Due.PadRight(30)} : {item.ParentId.ToString().PadRight(5)} : {item.ParentType}");
        //    }
        //}

        //[TestMethod]
        //public void RebaseHierarchyWithBaseIKeyClassTest()
        //{
        //    var baseEntity = new BaseIKey(1124);

        //    var testArray = new[]
        //    {
        //        new TestHierarchy(1, "A."),
        //        new TestHierarchy(2, "B."),
        //        new TestHierarchy(3, "C."),
        //        new TestHierarchy(4, "A.D."),
        //        new TestHierarchy(5, "A.5."),
        //        new TestHierarchy(6, "A.D.F."),
        //        new TestHierarchy(7, "A.D.F.7."),
        //        new TestHierarchy(8, "B.D.D.B.5."),
        //        new TestHierarchy(9, "C.H."),
        //        new TestHierarchy(10, "C.H.10"),
        //        new TestHierarchy(11, "LOE.H.10")
        //    };

        //    HierarchyHelper.RebaseHierarchy(testArray, baseEntity);

        //    Console.WriteLine($"{baseEntity.Id.ToString().PadLeft(5)}");

        //    Console.WriteLine(new string('-', 100));

        //    foreach (var item in testArray.OrderBy(el => el.Due))
        //    {
        //        Console.WriteLine($"{item.Id.ToString().PadLeft(5)} => {item.Layer} : {item.IsNode.ToString().PadRight(5)} : {item.Due.PadRight(30)} : {item.ParentId.ToString().PadRight(5)} : {item.ParentType}");
        //    }
        //}
        
        //private class TestHierarchy : IHierarchical
        //{
        //    public long Id { get; set; }
        //    public long? ParentId { get; set; }
        //    public string ParentType { get; set; }
        //    public bool IsNode { get; set; }
        //    public int Layer { get; set; }
        //    public string Due { get; set; }

        //    public TestHierarchy(long id, string due)
        //    {
        //        Id = id;
        //        Due = due;
        //    }
        //}

        //private class BaseHierarchy : IHierarchical
        //{
        //    public long Id { get; set; }
        //    public long? ParentId { get; set; }
        //    public string ParentType { get; set; }
        //    public bool IsNode { get; set; }
        //    public int Layer { get; set; }
        //    public string Due { get; set; }

        //    public BaseHierarchy(long id, string due)
        //    {
        //        Id = id;
        //        Due = due;
        //    }
        //}

        //private class BaseIKey : IKey
        //{
        //    public long Id { get; set; }

        //    public BaseIKey(long id)
        //    {
        //        Id = id;
        //    }
        //}
    }
}



