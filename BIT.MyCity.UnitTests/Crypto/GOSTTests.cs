using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MimeKit.Cryptography;

namespace BIT.MyCity.UnitTests.Crypto
{
    [TestClass]
    public class GOSTTests
    {
        [TestMethod]
        public void StandartVerifyTest()
        {
            try
            {
                var message = File.ReadAllBytes(@"C:\tmp\GostSignVerify\message.txt");
                var signature = File.ReadAllBytes(@"C:\tmp\GostSignVerify\message.txt.sig");

                var cert = new X509Certificate2(@"C:\tmp\GostSignVerify\esia_test.pem");

                var csp = (RSACryptoServiceProvider) cert.PublicKey.Key;

                var result = csp.VerifyHash(message, signature, HashAlgorithmName.SHA1, RSASignaturePadding.Pss);

                Console.WriteLine(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [TestMethod]
        public void VerifyTest()
        {
            try
            {
                var message = File.ReadAllBytes(@"C:\tmp\GostSignVerify\message.txt");

                var a = new Process
                {
                    StartInfo =
                    {
                        FileName = "openssl",
                        //Arguments = $"dgst -verify {@"C:\tmp\GostSignVerify\esia_pub_test.key"} -signature {@"C:\tmp\GostSignVerify\message.txt.sig"} {@"C:\tmp\GostSignVerify\message.txt"}",
                        Arguments = $"dgst -verify {@"C:\tmp\GostSignVerify\esia_pub_test.key"} -signature {@"C:\tmp\GostSignVerify\message.txt.sig"}",
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true,
                        UseShellExecute = false
                    }
                };

                //_options.Log?.Debug($"openssl arguments : {a.StartInfo.Arguments}");

                a.Start();
                a.StandardInput.Write(System.Text.Encoding.UTF8.GetString(message));
                a.StandardInput.Close();
                var result = false;
                while (!a.StandardOutput.EndOfStream)
                {
                    var line = a.StandardOutput.ReadLine();

                    Console.WriteLine(line);

                    result |= line == "Verified OK";
                }

                a.StandardOutput.Close();

                a.Close();

                a.Dispose();

                Console.WriteLine(result.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}



