using System;
using BIT.MyCity.Database;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MimeKit;
using MailKit.Net.Smtp;
using MailKit.Security;

namespace BIT.MyCity.UnitTests.EmailSubject
{
    [TestClass]
    public class SendEmailTests
    {
        [TestMethod]
        public void SubjectTemplateKeysTest()
        {
            var email = new Email
            {
                MailMessage = new MailMessage
                {
                    ToName = "Эмиль Тихомиров",
                    //Email = "t_emil@mail.ru",
                    //Email = "admin@sochiadm.ru",
                    Email = "t_emil@mail.ru",
                    Subject = "Проверка почты"
                },
                Template = "Проверочное письмо"
            };

            var result = false;

            for (int i = 0; i < 100; i++)
            {
                result = SendEmail(email);

                Console.WriteLine($"{i+1.ToString().PadLeft(3,'0')} => {result}");
            }

            Console.ReadLine();

            Assert.IsTrue(result, "Сообщение должно отправляться");
        }

        private bool SendEmail(Email message)
        {
            try
            {
                //var smtpHost = "mail.sochiadm.ru";
                var smtpHost = "smtp.mail.ru";
                var smtpPort = 465;
                var useSsl = true;
                var checkCertificateRevocation = true;
                var userLogin = "t_emil@mail.ru";
                var userPassword = "Yurban22~";
                var fromName = "Администрация сайта";
                //var fromAddress = "gorod@sochiadm.ru";
                var fromAddress = "t_emil@mail.ru";

                var emailMessage = new MimeMessage();

                emailMessage.From.Add(new MailboxAddress(fromName, fromAddress));

                emailMessage.To.Add(new MailboxAddress(
                    message.MailMessage.ToName,
                    message.MailMessage.Email));

                emailMessage.Subject = string.IsNullOrWhiteSpace(message.MailMessage.Subject)
                    ? "Проверка"
                    : message.MailMessage.Subject;

                var builder = new BodyBuilder
                {
                    HtmlBody = message.Template
                };

                emailMessage.Body = builder.ToMessageBody();

                    

                using (var client = new SmtpClient())
                {
                    client.CheckCertificateRevocation = checkCertificateRevocation;

                    client.Connect(
                        smtpHost,
                        smtpPort,
                        useSsl ? SecureSocketOptions.Auto : SecureSocketOptions.None);

                    

                    if (!string.IsNullOrWhiteSpace(userLogin))
                    {
                        client.Authenticate(
                            userLogin,
                            userPassword);
                    }

                    client.Send(emailMessage);

                    client.Disconnect(true);
                }

                return true;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex);

                return false;
            }
        }
    }
}



