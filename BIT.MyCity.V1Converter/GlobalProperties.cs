namespace BIT.MyCity.V1Converter
{
    public class GlobalProperties
    {
        public static string V1Url {get; set;}
        public static string V1Username {get; set;}
        public static string V1Password {get; set;}

        public static string ApiPath { get; set; }
    }
}