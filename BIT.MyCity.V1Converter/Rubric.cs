using System.Collections.Generic;

namespace BIT.MyCity.V1Converter
{
    public class Rubric
    {
        public string Id {get;set;}
        public string Name { get; set; }
        public string ExtId { get;set;}
        public string Description { get;set;}
        public string ShortName {get;set;}
        public int Order {get;set;}
        public string Parent {get;set;}
        public bool Hide {get;set;}
        public bool DeleteMark {get;set;}
        public string Moderator {get;set;}
        public string DocGroup {get;set;}
        public List<string> Receivers {get;set;}
        public List<string> Users {get;set;}
        public List<string> StatusModerators {get;set;}
        public bool UseForPa {get;set;}

    }
}