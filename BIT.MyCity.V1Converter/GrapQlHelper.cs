using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.V1Converter.Models;
using GraphQL;
using GraphQL.Client.Http;
using Microsoft.Extensions.Logging;

namespace BIT.MyCity.V1Converter
{
    public class GraphQlHelper
    {

        private ILogger<GraphQlHelper> _logger;
        private readonly GraphQLHttpClient _graphQLClient;

        public GraphQlHelper(ILogger<GraphQlHelper> logger, GraphQLHttpClient graphQlClient) {
            _logger = logger;
            _graphQLClient = graphQlClient;
        }

        private void ProcessErrors<T>(GraphQLResponse<T> response)
        {
            if (response.Errors != null && response.Errors.Length > 0)
            {
                // var ex = new Exception(string.Join(',', response.Errors.Select(e => $"{e.Message}; {string.Join('/', e.Path)}").ToArray()));
                var ex = new Exception(string.Join(',', response.Errors.Select(e => $"{e.Message}; ").ToArray()));
                _logger.LogError(ex, "Error on GraphQL request");
                throw ex;
            }
        }

        private GraphQLRequest GetFullCatalogQuery(string entityName, string[] subsystemId)
        {
            if (subsystemId == null || subsystemId.Length == 0)
            {
                return new GraphQLRequest
                {
                    Query = @"query " + entityName + @"{
                        " + entityName + @" {
                            items {
                                id
                                extId
                                name
                                shortName
                                weight
                            }
                        }
                    }"
                };
            }
            return new GraphQLRequest
            {
                Query = @"query " + entityName + @"($subsystem: [String]!) {
                        " + entityName + @" (filter: {name: " + "\"subsystem.uID\"" + @", in: $subsystem}) {
                            items {
                                id
                                extId
                                name
                                shortName
                                weight
                            }
                        }
                    }",
                Variables = new { subsystem = subsystemId }
            };
        }

        private string GetNormalizedName(string name)
        {
            return name.Substring(0, 1).ToUpper() + name.Substring(1);
        }


        private GraphQLRequest GetCreateMutation(string entityName, object entity)
        {
            var entityNormalizedName = GetNormalizedName(entityName);
            return new GraphQLRequest
            {
                Query =
                @"mutation create" + entityNormalizedName + @"($value: create" + entityNormalizedName + @"!) {
                    create" + entityNormalizedName + @"(" + entityName + @": $value) {
                        id
                        name
                    }
                }",
                Variables = new
                {
                    value = entity
                }

            };
        }
        private GraphQLRequest GetUpdateMutation(string entityName, object entity, long id, bool delete = false)
        {
            var entityNormalizedName = entityName.Substring(0, 1).ToUpper() + entityName.Substring(1);
            return new GraphQLRequest
            {
                Query =
                        @"mutation update" + entityNormalizedName + @"($id: ID!, $value: update" + entityNormalizedName + @"!) {
                            update" + entityNormalizedName + @"(id: $id, " + entityName + @": $value) {
                                id
                                name
                            }
                        }",
                Variables = new
                {
                    id = id,
                    value = entity
                }

            };
        }

        public async Task SaveRubric(IList<Rubric> values, string subsystemIds)
        {
            _logger.LogDebug($"Subsystem IDs: {subsystemIds}");
            string[] subsystems = null;
            if (!string.IsNullOrEmpty(subsystemIds))
            {
                subsystems = subsystemIds.Split(',');
            }
            var query = GetFullCatalogQuery("rubric", null);
            var response = await _graphQLClient.SendQueryAsync<Query>(query);
            ProcessErrors<Query>(response);
            var entityNormalizedName = GetNormalizedName("rubric");
            var property = response.Data.GetType().GetProperty(entityNormalizedName);
            var paginatedValue = property.GetValue(response.Data);
            var itemsProperty = paginatedValue.GetType().GetProperty("Items");
            var dbValues = (ICollection<Models.Rubric>)itemsProperty.GetValue(paginatedValue);
            var valueIds = new Dictionary<string, long?>();
            var propertyName = "";
            foreach (var value in values)
            {
                var dbValue = dbValues.FirstOrDefault(v => v.ExtId == value.ExtId);
                GraphQLRequest saveMutation = null;
                object entity = null;
                if (subsystems == null || subsystems.Length == 0)
                {
                    entity = new
                    {
                        Name = value.Name,
                        ShortName = value.ShortName,
                        Weight = value.Order,
                        ExtId = value.ExtId,
                        IsNode = false
                    };
                }
                else
                {
                    entity = new
                    {
                        Name = value.Name,
                        ShortName = value.ShortName,
                        Weight = value.Order,
                        ExtId = value.ExtId,
                        SubSystemUIDs = subsystems,
                        IsNode = false,
                    };
                }

                // }
                if (dbValue == null)
                {
                    saveMutation = GetCreateMutation("rubric", entity);
                    propertyName = "Create" + entityNormalizedName;
                }
                else
                {
                    saveMutation = GetUpdateMutation("rubric", entity, dbValue.Id.Value);
                    propertyName = "Update" + entityNormalizedName;
                }
                var resp = await _graphQLClient.SendMutationAsync<Mutation>(saveMutation);
                ProcessErrors<Mutation>(resp);
                if (resp.Errors != null && resp.Errors.Length > 0)
                {
                    var ex = new Exception(string.Join(',', resp.Errors.Select(e => e.Message).ToArray()));
                    _logger.LogError(ex, "Error on GraphQL request - saving catalog");
                    throw ex;
                }
                var respProperty = resp.Data.GetType().GetProperty(propertyName);
                var propValue = respProperty.GetValue(resp.Data);
                if (propValue != null)
                {
                    var idProp = propValue.GetType().GetProperty("Id");
                    valueIds.Add(value.ExtId, (long?)idProp.GetValue(propValue));
                }
            }
            var valuesToDelete = dbValues.Where(dbVal => values.All(val => val.ExtId != dbVal.ExtId)).ToList();
            foreach (var value in valuesToDelete)
            {
                value.Deleted = true;
                var mutation = GetUpdateMutation("rubric", new { Deleted = true }, value.Id.Value, true);
                var updateResp = await _graphQLClient.SendMutationAsync<Mutation>(mutation);
                ProcessErrors<Mutation>(updateResp);
            }

        }

    }
}