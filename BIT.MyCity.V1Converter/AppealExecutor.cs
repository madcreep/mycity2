namespace BIT.MyCity.V1Converter
{
    public class AppealExecutor
    {
        //public WeakReference<Appeal> Appeal { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string DepartmentId { get; set; }
        public string Position { get; set; }
        public string Organization { get; set; }
    }
}