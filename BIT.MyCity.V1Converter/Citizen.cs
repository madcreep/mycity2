    public class Citizen
    {
        public string ExtId { get; set; }
        public string Id { get; set; }
        public bool Confirmed { get; set; }
        public string Zipcode { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Inn { get; set; }
        public string Snils { get; set; }
        public byte Sex { get; set; }
        public string Series { get; set; }
        public string Number { get; set; }
        public string Given { get; set; }
    }
