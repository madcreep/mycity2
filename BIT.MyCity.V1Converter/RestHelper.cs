using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace BIT.MyCity.V1Converter
{
    public class RestException : Exception
    {
        public RestException(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }

        public RestException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; set; }

    }
    internal class RestHelper
    {
        private readonly ILogger<RestHelper> _logger;

        public RestHelper(ILogger<RestHelper> logger) 
        {
            _logger = logger;
        }
        
        public string Login()
        {
            var client = new RestClient(GlobalProperties.V1Url);
            var request = new RestRequest("converter/login", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddParameter("name", GlobalProperties.V1Username);
            request.AddParameter("password", GlobalProperties.V1Password);
            var response = client.Execute(request);
            string result = null;
            if (response.ResponseStatus == ResponseStatus.Completed && response.StatusCode == HttpStatusCode.OK)
            {
                var headers = response.Headers;
                foreach (var parameter in headers)
                {
                    if (parameter.Name.Equals("set-cookie", StringComparison.OrdinalIgnoreCase))
                    {
                        result = parameter.Value.ToString();
                    }
                }

            }
            return result;
        }

        public async Task<string> Logout(string cookie)
        {
            var client = new RestClient(GlobalProperties.V1Url);
            var request = new RestRequest("converter/logout", Method.GET) { RequestFormat = DataFormat.Json };
            request.AddHeader("Cookie", cookie);
            var response = await client.ExecuteAsync(request);
            string result = null;
            if (response.ResponseStatus == ResponseStatus.Completed && response.StatusCode == HttpStatusCode.OK)
            {
                return response.Content;

            }
            return result;
        }

        public async Task<List<Appeal>> GetAppeals(string cookie)
        {
                        var client = new RestClient(GlobalProperties.V1Url);
            var request = new RestRequest("converter/appeals", Method.GET) {RequestFormat = DataFormat.Json};
            request.AddHeader("Cookie", cookie);
            var response = await client.ExecuteAsync<List<Appeal>>(request);
            if (response.ResponseStatus == ResponseStatus.Completed && response.StatusCode == HttpStatusCode.OK)
            {
                return response.Data;
            }
            throw new RestException(response.StatusCode);
        }
        public async Task<List<Rubric>> GetRubrics(string cookie)
        {
                        var client = new RestClient(GlobalProperties.V1Url);
            var request = new RestRequest("converter/rubrics", Method.GET) {RequestFormat = DataFormat.Json};
            request.AddHeader("Cookie", cookie);
            var response = await client.ExecuteAsync<List<Rubric>>(request);
            if (response.ResponseStatus == ResponseStatus.Completed && response.StatusCode == HttpStatusCode.OK)
            {
                return response.Data;
            }
            throw new RestException(response.StatusCode);
        }
        public async Task<List<Client>> GetClients(string cookie)
        {
                        var client = new RestClient(GlobalProperties.V1Url);
            var request = new RestRequest("converter/clients", Method.GET) {RequestFormat = DataFormat.Json};
            request.AddHeader("Cookie", cookie);
            var response = await client.ExecuteAsync<List<Client>>(request);
            if (response.ResponseStatus == ResponseStatus.Completed && response.StatusCode == HttpStatusCode.OK)
            {
                return response.Data;
            }
            throw new RestException(response.StatusCode);
        }

        public async Task MarkConverted(string cookie, string entityName, string id)
        {
            var client = new RestClient(GlobalProperties.V1Url);
            var request = new RestRequest("converter/markConverted/{entityName}/{id}", Method.GET) {RequestFormat = DataFormat.Json};
            request.AddUrlSegment("entityName", entityName);
            request.AddUrlSegment("id", id);
            request.AddHeader("Cookie", cookie);
            var response = await client.ExecuteAsync<List<Client>>(request);
            if (response.ResponseStatus == ResponseStatus.Completed && response.StatusCode == HttpStatusCode.OK)
            {
                return;
            }
            throw new RestException(response.StatusCode);
        }


    }
}