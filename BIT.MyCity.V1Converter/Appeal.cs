using System;
using System.Collections.Generic;

namespace BIT.MyCity.V1Converter
{
    public class Appeal
    {

        public string Id { get; set; } // ИД в Node.js
        public string Number { get; set; } // number в Node.js
        public string ExtId { get; set; } // ИД в системе "ДЕЛО"
        public string ExtNumber { get; set; } // Номер документа в системе "ДЕЛО"
        public DateTime? ExtDate { get; set; } // Дата документа в системе "ДЕЛО"
        public string Rubric { get; set; } // Орган власти - подразделение в системе "ДЕЛО" ИЛИ вид заявки
        public string Docgroup { get; set; } // Группа документов в системе "ДЕЛО". Связана с Rubric 1-1
        public string RubricName { get; set; } // Группа документов в системе "ДЕЛО". Связана с Rubric 1-1
        public string Receivers { get; set; } // Список получателей
        public Citizen Author { get; set; } // Гражданин(ка)
        public string Description { get; set; } // Текст
        public bool Public { get; set; }
        public bool Collective { get; set; }
        public bool Approved { get; set; }
        public AppealStatus Status { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Place { get; set; }
        public List<AppealFile> Files { get; set; } 
        public List<AppealExecutor> Executors { get; set; }
        public List<AppealExecutor> PrincipalExecutors { get; set; }
        public bool AnswerByPost { get; set; }
        public bool ExternalExec { get; set; }
        public bool Esia { get; set; }
        public int FileCount { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public string Zipcode { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string AuthorName { get; set; }
        public string Topic { get; set; }
        public string Territory { get; set; }
        public string Addressee { get; set; }
        public AppealExecutor Executor { get; set; }
        public DateTime? ResDate { get; set; }
        public DateTime? PlanDate { get; set; }
        public DateTime? FactDate { get; set; }
        public string ResStatus { get; set; }
        public string ResText { get; set; }

        public string RegionName { get; set; }
        public List<AppealComment> Comments { get; set; }
    }

    public enum AppealStatus
    {
        Saved,
        Registered,
        Accepted,
        AcceptedExternal,
        AcceptedOtherDepartment,
        Finished,
        Rejected,
        Prolonged,
        Transferred,
        Discontinued
    }

    public enum AppealAuthorType
    {   //тип заявителя
        Individual,//физическое
        SoleProprietor,//ИП
        LegalPerson //юридическое
    }

}