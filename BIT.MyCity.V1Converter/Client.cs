namespace BIT.MyCity.V1Converter
{
    public class Client
    {
        public string ExtId { get; set; }
        public string DeloId { get; set; }
        public string Login { get; set; }
        public string Name { get; set; }
        public string Passport { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string Region1 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Password { get; set; }
        public string Inn { get; set; }
        public string Snils { get; set; }
        public byte Sex { get; set; }
        public string Series { get; set; }
        public string Number { get; set; }
        public string Given { get; set; }
    }
}