using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.Client.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using GraphQL.Client.Serializer.Newtonsoft;

namespace BIT.MyCity.V1Converter
{
    public class Program
    {
        private static ILoggerFactory _loggerFactory;
        
        public static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            //CreateHostBuilder(args).Build().Run();

            var serviceProvider = serviceCollection.BuildServiceProvider();  

            var restHelper = serviceProvider.GetService<RestHelper>();
            var graphQlHelper = serviceProvider.GetService<GraphQlHelper>();
            
            var cookie = restHelper.Login();
            if (!string.IsNullOrEmpty(cookie)) 
            {
                await ConvertRubrics(cookie, restHelper, graphQlHelper);
            }
        }

        private static async Task ConvertRubrics(string cookie, RestHelper restHelper, GraphQlHelper graphQlHelper) {
            var rubrics = await restHelper.GetRubrics(cookie);
            await graphQlHelper.SaveRubric(rubrics, "citizens");
            foreach (var rubric in rubrics) 
            {
                await restHelper.MarkConverted(cookie, "Rubric", rubric.Id);
            }
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            if (_loggerFactory == null) 
            {
                _loggerFactory = new LoggerFactory();
                _loggerFactory.AddLog4Net();
            }
            services.AddSingleton(_loggerFactory);
            services.AddScoped<GraphQLHttpClient>(x =>
                new GraphQLHttpClient(GlobalProperties.ApiPath, new NewtonsoftJsonSerializer())
            );
            services.AddLogging(configure => 
            {
                configure.AddConsole();
                configure.AddLog4Net();
            })
                .AddTransient<RestHelper>()
                .AddTransient<GraphQlHelper>();

            Utils.LoggerFactory = _loggerFactory;
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
