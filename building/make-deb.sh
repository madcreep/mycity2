#!/bin/bash

DOTNETPROJECT='BIT.MyCity'

PACKETNAME='mycity'
DEBTEMPLATE='./deb_template/'
BIZPREFIX=`cat .prefix`
RELEASENUM=`cat  ./release.gen`
VERSION=`cat ./VERSION`-$RELEASENUM
BUILDROOT='./deb'
NAMEVER=$PACKETNAME\_$VERSION
BUILDDIR=$BUILDROOT/$NAMEVER

echo $VERSION
mkdir $BUILDDIR
# copy allData
cp -R $DEBTEMPLATE/* $BUILDDIR
cp ./sbin/* $BUILDDIR/usr/local/bin
cp ../Doc/Public/*.pdf $BUILDDIR/usr/share/mycity/
cp ../Doc/Private/*.pdf $BUILDDIR/usr/share/mycity/

cat $DEBTEMPLATE/DEBIAN/control | sed "s/Version: %version%/Version: $VERSION/" > $BUILDDIR/DEBIAN/control

cp -R /Users/hedgehog/biz/mycity2/building/deb_root/* $BUILDDIR
cp -r ../$DOTNETPROJECT/bin/Debug/netcoreapp3.1/publish/* $BUILDDIR/usr/lib/mycity/

cd $BUILDROOT
pwd
dpkg-deb --build $NAMEVER

ln -sf $NAMEVER.deb latest
echo $MYCITYBUILDFILENAME