#!/bin/bash

DOTNETPROJECT='BIT.MyCity'

PACKETNAME='mycity'
SRCTEMPLATE='./fpm_template/'
DEBTEMPLATE='./deb_template/'
BIZPREFIX=`cat .prefix`
RELEASENUM=`cat  ./release.gen`
VERSION=`cat ./VERSION`-$RELEASENUM
BUILDROOT='./fpm'
NAMEVER=$PACKETNAME\_$VERSION
BUILDDIR=$BUILDROOT/$NAMEVER
FULLDEBPATH=`pwd`/fpm/$NAMEVER\_all.deb

echo $VERSION
mkdir $BUILDDIR
# copy allData
cp -R $SRCTEMPLATE/* $BUILDDIR
cp ./sbin/* $BUILDDIR/usr/local/bin
cp ../Doc/Public/*.pdf $BUILDDIR/usr/share/mycity/
cp ../Doc/Private/*.pdf $BUILDDIR/usr/share/mycity/

# cp -R $DEBTEMPLATE/* $BUILDDIR
# cat $SRCTEMPLATE/DEBIAN/control | sed "s/Version: %version%/Version: $VERSION/" > $BUILDDIR/DEBIAN/control

cp -R /Users/hedgehog/biz/mycity2/building/deb_root/* $BUILDDIR
cp -n -r ../$DOTNETPROJECT/bin/Debug/netcoreapp3.1/publish/* $BUILDDIR/usr/lib/mycity/
rm -f $BUILDDIR/usr/lib/mycity/appsettings.json

#cd $BUILDDIR
pwd
# dpkg-deb --build $NAMEVER

fpm -s dir -t deb \
	--chdir $BUILDDIR \
	--name mycity \
	--architecture noarch \
	--package ./fpm/ \
	--version $VERSION \
	--config-files /etc/mycity/angconfig.json \
	--config-files /etc/mycity/appsettings.json \
	--config-files /etc/mycity/push.json \
	--config-files /etc/systemd/system/mycity.service \
	--deb-systemd-enable \
	--deb-systemd-auto-start \
	--deb-systemd-restart-after-upgrade \
	--after-install $BUILDDIR/usr/lib/mycity/scripts/install \
	--after-upgrade $BUILDDIR/usr/lib/mycity/scripts/upgrade \
	--before-upgrade $BUILDDIR/usr/lib/mycity/scripts/bupgrade \
	--before-install $BUILDDIR/usr/lib/mycity/scripts/binstall \
	--force \
    .
echo $FULLDEBPATH
ln -sf $FULLDEBPATH ./fpm/latest
# echo $MYCITYBUILDFILENAME
