# ~/biz/mycity2-rpm/mycity2-rpm.spec
# Файл спецификаций для программы mycity2
# Общее описание программы
Summary: Серверная часть для mycity2
# Название пакета
Name: mycity2
# Его версия
Version: 1.0
# Релиз
Release: %(cat ./release.gen)
# Группа программного обеспечения. Группы (или категории) используются многими
# программами для манипуляции пакетами, например gnorpm, которая строит дерево
# категорий установленного программного обеспечения
Group: Daemons
# Если хотите, можете указать свое имя, но обычно указывается GPL
License: BIZ
# Можно также указать и copyleft
# Copyright: GPL
# Наш пакет ни с чем не кофликтует
# Conflicts: 
# Менеджер сам заполнит это поле при необходимости (только для создания двоичных пакетов!)
# Require: 
# Информация о создателе пакета
Packager: Derevenskikh Aleksander [derevenskihav@biz-it.ru]
URL: https://biz-it.ru
# Тэги Summary, Name, Version, Release, Group, License явлются обязательными
# Из вышеуказанной информации видно, что будет создан пакет:
# port-1.0-99.i686.rpm
# Архитектура у вас может отличаться
Requires: dotnet-sdk-3.1, jq
#BuildArch: x86_64
BuildArch: noarch

Exclusiveos: linux 

Source0: %{expand:%%(pwd)}

BuildRoot: %{_topdir}/BUILD/%{name}-%{version}-%{release}

# Полное описание пакета
%description
Серверная часть mycity2

# define _rpmfilename %%{ARCH}/1%%{NAME}-%%{VERSION}-%%{RELEASE}.rpm


# Файлы, которые будут помещены в пакет
%files
%defattr(644,root,root)
%defattr(755,root,root)
/opt/mycity2/bin
/opt/mycity2/sys
/opt/mycity2/opts
/opt/mycity2/docs



#%build
#echo "mycity2 build"

# Действия, выполняемые при создании пакета
%install
echo "mycity2 install"
mkdir -p $RPM_BUILD_ROOT/opt/mycity2/bin
cd $RPM_BUILD_ROOT
cp -r %{SOURCEURL0}/sbin/ ./opt/mycity2/bin
mkdir -p $RPM_BUILD_ROOT/opt/mycity2/sys
cp -r %{SOURCEURL0}/sys/ ./opt/mycity2/sys
mkdir -p $RPM_BUILD_ROOT/opt/mycity2/opts
cp -r %{SOURCEURL0}/opts/ ./opt/mycity2/opts
mkdir -p $RPM_BUILD_ROOT/opt/mycity2/docs
cp -r %{SOURCEURL0}/opts/ ./opt/mycity2/docs

#rm -rf $RPM_BUILD_ROOT

%pre
mkdir -p $RPM_BUILD_ROOT/opt/mycity2/config
if [ $1 == 1 ];then
   echo "-----------------------"
   echo "RPM is getting installed"   
   echo "-----------------------"
elif [ $1 -gt 1 ];then
   echo "-----------------------"
   chfile=/opt/mycity2/sys/appsettings.json
   if [ ! -L $chfile ]; then
	   echo "appsettings.json regular file detected - moved to /opt/mycity2/config/"
   		mv $chfile /opt/mycity2/config/
   fi
   chfile=/opt/mycity2/sys/push.json
   if [ ! -L $chfile ]; then
	   echo "push.json regular file detected - moved to /opt/mycity2/config/"
   		mv $chfile /opt/mycity2/config/
   fi
   echo "-----------------------"  
fi

%post
MAINDIR="/opt/mycity2"
if [ $1 == 1 ];then
   echo "-----------------------"
	echo "Generate configs"

	FILE=$MAINDIR/config/appsettings.json
	if [ -f "$FILE" ]; then
	    echo "$FILE exists."
	else 
	    echo "$FILE does not exist."
	    echo "generate jwt secret"
	    cat $MAINDIR/opts/appsettings.template | jq ".JwtConfig.secret=\"`openssl rand 64 | base64`\"" > $MAINDIR/config/appsettings.json	        
	fi
	FILE=$MAINDIR/config/push.json
	if [ -f "$FILE" ]; then
	    echo "$FILE exists."
	else 
	    cat $MAINDIR/opts/push.template > $MAINDIR/config/push.json	    
	fi   
   echo "-----------------------"
elif [ $1 == 2 ];then
   echo "-----------------------"
   echo "post RPM is getting upgraded"
   echo "-----------------------"
fi

chfile=/opt/mycity2/sys/ClientApp/dist/assets/angconfig.json
if [ ! -f $chfile ]; then
   echo "angconfig.json file not detected - creating"
      cp /opt/mycity2/opts/angconfig.json $chfile
fi

chfile=$MAINDIR/config/mycity.service
if [ ! -f $chfile ]; then
   
   if [ -f /etc/systemd/system/mycity.service ]; then
      echo "mycity.service file not detected - copyng existed"
      cp /etc/systemd/system/mycity.service $chfile   
   else
      echo "mycity.service file not detected - copyng default"
      cp /opt/mycity2/opts/mycity.service $chfile   
   fi
fi

echo "-----------------------"
%{__ln_s} -f $MAINDIR/config/appsettings.json $MAINDIR/sys/appsettings.json
%{__ln_s} -f $MAINDIR/config/push.json $MAINDIR/sys/push.json
%{__ln_s} -f $MAINDIR/sys/ClientApp/dist/assets/angconfig.json $MAINDIR/config/angconfig.json
%{__ln_s} -f $MAINDIR/bin/mycity-install /usr/bin/mycity-install
%{__ln_s} -f $MAINDIR/bin/mycity-gostcheck /usr/bin/mycity-gostcheck
%{__ln_s} -f $MAINDIR/bin/mycity-logs /usr/bin/mycity-logs
%{__ln_s} -f $MAINDIR/bin/mycity-start /usr/bin/mycity-start
%{__ln_s} -f $MAINDIR/bin/mycity-stop /usr/bin/mycity-stop
%{__ln_s} -f $MAINDIR/bin/mycity-update /usr/bin/mycity-update
%{__ln_s} -f $MAINDIR/config/mycity.service /etc/systemd/system/mycity.service

%build
echo "mycity2 build section"


%clean
#rm -r -f "$RPM_BUILD_ROOT"

# Файл port копируется в каталог /usr/bin (права доступа 555)
# install -s -m 555 -o 0 -g 0 /root/port/port /usr/bin
# Файл port.1 копируется в каталог /usr/man/man1 (права доступа 444)
# install -m 444 -o 0 -g 0 /root/port/port.1 /usr/man/man1
# install -d -m 755 %{buildroot}/opt/mycity2
# echo "-----"
# echo "====="
# cp -a /Users/hedgehog/biz/mycity2-rpm/README %{buildroot}
# install --help

# /%{_sysconfdir}/
%preun
if [ $1 == 1 ];then
   echo "-----------------------"
   echo "preun RPM is getting upgraded"   
   echo "-----------------------"
elif [ $1 == 0 ];then
   echo "--------------------"
   echo "preun RPM is getting removed/uninstalled"
   echo "Put your script here which will be called before uninstallation of this rpm"
   echo "--------------------"
fi

%postun
case "$1" in
  0) # last one out put out the lights
    rm -f /usr/bin/mycity-install
    rm -f /usr/bin/mycity-gostcheck
    rm -f /usr/bin/mycity-logs
    rm -f /usr/bin/mycity-start
    rm -f /usr/bin/mycity-stop
    systemctl stop mycity.service
    rm -f /etc/systemd/system/mycity.service
  ;;
esac

