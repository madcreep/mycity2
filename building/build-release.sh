#/bin/sh

DOTNETPROJECT='BIT.MyCity'
RELEASENUM=`./buildver-increment.sh`
BIZPREFIX=`cat ./.prefix`
echo 'release: '$RELEASENUM

BUILDVERSION=2.0.0.$RELEASENUM

cd ../$DOTNETPROJECT/
rm -r ./bin/Debug/netcoreapp3.1/publish/*

echo $RELEASENUM > ./ClientApp/release.gen


echo $BUILDVERSION-$BIZPREFIX
dotnet clean
dotnet publish /p:Version=$BUILDVERSION-$BIZPREFIX-release  /p:AssemblyVersion=$BUILDVERSION

# dotnet publish  /p:AssemblyVersion=1.2.3.4
