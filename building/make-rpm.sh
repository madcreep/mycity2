#!/bin/bash
#cp ./mycity2-rpm.spec ~/rpmbuild/SPECS/
# rm ./bit-mycity.tar.gz
# tar -czvf ./bit-mycity.tar.gz -C ../BIT.MyCity/bin/Debug/netcoreapp3.1/publish .

PACKETNAME='mycity2'
SPECFILE='./mycity2-rpm.spec'
ARCH='noarch'
RPMBINDIR="/Users/hedgehog/rpmbuild/RPMS/${ARCH}/"
DOTNETPROJECT='BIT.MyCity'
BIZPREFIX=`cat .prefix`

RELEASENUM=`cat  ./release.gen`
echo 'release: '$RELEASENUM

export BUILDVERSION=2.0.$RELEASENUM-$BIZPREFIX

MYCITYBUILDFILENAME=mycity2-$BUILDVERSION.rpm

echo 'file: '$MYCITYBUILDFILENAME

mkdir ./sys
rm -r -f ./sys/*
cp -r ../$DOTNETPROJECT/bin/Debug/netcoreapp3.1/publish/* ./sys/
rm -f ./sys/appsettings.json
rm -f ./sys/ClientApp/dist/assets/angconfig.json

# mkdir ./opts
# rm -r -f ./opts/*
# cp -r ../System/* ./opts/

mkdir ./docs
cp -r ../Doc/Public* ./docs/

rpmbuild -bb $SPECFILE  \
	--define "_target_os linux" \
	--define "_rpmfilename %%{ARCH}/$MYCITYBUILDFILENAME" 
# rm -r -f ./opts/*
#rm -r -f ./sys/*


cd $RPMBINDIR

ln -sf $RPMBINDIR/$MYCITYBUILDFILENAME latest-$BIZPREFIX
echo $MYCITYBUILDFILENAME

