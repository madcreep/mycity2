# ~/biz/mycity2-rpm/mycity2di-rpm.spec
# Файл спецификаций для программы mycity2-delo-integration
# Общее описание программы
Summary: Серверная часть для mycity2-delo-integration
# Название пакета
Name: mycity2-delo-integration
# Его версия
Version: 2.0
# Релиз
Release: %(cat ./release.gen)
# Группа программного обеспечения. Группы (или категории) используются многими
# программами для манипуляции пакетами, например gnorpm, которая строит дерево
# категорий установленного программного обеспечения
Group: Daemons
# Если хотите, можете указать свое имя, но обычно указывается GPL
License: BIZ
# Можно также указать и copyleft
# Copyright: GPL
# Наш пакет ни с чем не кофликтует
# Conflicts: 
# Менеджер сам заполнит это поле при необходимости (только для создания двоичных пакетов!)
# Require: 
# Информация о создателе пакета
Packager: Derevenskikh Aleksander [derevenskihav@biz-it.ru]
URL: https://biz-it.ru
# Тэги Summary, Name, Version, Release, Group, License явлются обязательными
# Из вышеуказанной информации видно, что будет создан пакет:
# port-1.0-99.i686.rpm
# Архитектура у вас может отличаться
Requires: dotnet-sdk-3.1, jq

BuildArch: noarch

Exclusiveos: linux 

Source0: %{expand:%%(pwd)}

BuildRoot: %{_topdir}/BUILD/%{name}-%{version}-%{release}

# Полное описание пакета
%description
Серверная часть mycity2-delo-integration

# define _rpmfilename %%{ARCH}/1%%{NAME}-%%{VERSION}-%%{RELEASE}.rpm


# Файлы, которые будут помещены в пакет
%files
%defattr(644,root,root)
%defattr(755,root,root)
/opt/mycity2-delo/bin
/opt/mycity2-delo/sys
/opt/mycity2-delo/opts
/opt/mycity2-delo/docs



#%build

# Действия, выполняемые при создании пакета
%install

mkdir -p $RPM_BUILD_ROOT/opt/mycity2-delo/bin
cd $RPM_BUILD_ROOT
cp -r %{SOURCEURL0}/sbin/ ./opt/mycity2-delo/bin
mkdir -p $RPM_BUILD_ROOT/opt/mycity2-delo/sys
cp -r %{SOURCEURL0}/sys/ ./opt/mycity2-delo/sys
mkdir -p $RPM_BUILD_ROOT/opt/mycity2-delo/opts
cp -r %{SOURCEURL0}/opts/ ./opt/mycity2-delo/opts
mkdir -p $RPM_BUILD_ROOT/opt/mycity2-delo/docs
cp -r %{SOURCEURL0}/opts/ ./opt/mycity2-delo/docs

#rm -rf $RPM_BUILD_ROOT

%pre
mkdir -p $RPM_BUILD_ROOT/opt/mycity2-delo/config
if [ $1 == 1 ];then
   echo "-----------------------"
   echo "RPM is getting installed"   
   echo "-----------------------"
elif [ $1 -gt 1 ];then
   echo "-----------------------"
   chfile=/opt/mycity2-delo/sys/appsettings.json
   if [ ! -L $chfile ]; then
      echo "appsettings.json regular file detected - moved to /opt/mycity2-delo/config/"
         mv $chfile /opt/mycity2-delo/config/
   fi

fi

%post
MAINDIR="/opt/mycity2-delo"
if [ $1 == 1 ];then
   echo "-----------------------"
#	FILE=$MAINDIR/config/push.json
#	if [ -f "$FILE" ]; then
#	    echo "$FILE exists."
#	else 
#	    cat $MAINDIR/opts/push.template > $MAINDIR/config/push.json	    
#	fi   
 #  echo "-----------------------"
elif [ $1 == 2 ];then
   echo "-----------------------"
   echo "post RPM is getting upgraded"
   echo "-----------------------"
fi

echo "Update configs"
FILE=$MAINDIR/config/appsettings.json
if [ -f "$FILE" ]; then
    echo "$FILE exists."
else 
    echo "$FILE does not exist."
    cat $MAINDIR/opts/appsettings.template > $MAINDIR/config/appsettings.json         
fi


chfile=$MAINDIR/config/mycity-delo.service
if [ ! -f $chfile ]; then
   if [ -f /etc/systemd/system/mycity-delo.service ]; then
      echo "mycity-delo.service file not detected - copyng existed"
      cp /etc/systemd/system/mycity-delo.service $chfile   
   else
      echo "mycity-delo.service file not detected - copyng default"
      cp /opt/mycity-delo/opts/mycity-delo.service $chfile   
   fi
fi

echo "-----------------------"
%{__ln_s} -f $MAINDIR/config/appsettings.json $MAINDIR/sys/appsettings.json
%{__ln_s} -f $MAINDIR/bin/mycity-delo-install /usr/bin/mycity-delo-install
%{__ln_s} -f $MAINDIR/bin/mycity-delo-logs /usr/bin/mycity-delo-logs
%{__ln_s} -f $MAINDIR/bin/mycity-delo-start /usr/bin/mycity-delo-start
%{__ln_s} -f $MAINDIR/bin/mycity-delo-stop /usr/bin/mycity-delo-stop
%{__ln_s} -f $MAINDIR/bin/mycity-delo-update /usr/bin/mycity-delo-update
%{__ln_s} -f $MAINDIR/config/mycity-delo.service /etc/systemd/system/mycity-delo.service

%build
echo "mycity2di build section"


%clean
#rm -r -f "$RPM_BUILD_ROOT"

# Файл port копируется в каталог /usr/bin (права доступа 555)
# install -s -m 555 -o 0 -g 0 /root/port/port /usr/bin
# Файл port.1 копируется в каталог /usr/man/man1 (права доступа 444)
# install -m 444 -o 0 -g 0 /root/port/port.1 /usr/man/man1
# install -d -m 755 %{buildroot}/opt/mycity2
# echo "-----"
# echo "====="
# cp -a /Users/hedgehog/biz/mycity2-rpm/README %{buildroot}
# install --help

# /%{_sysconfdir}/
%preun
if [ $1 == 1 ];then
   echo "-----------------------"
   echo "preun RPM is getting upgraded"   
   echo "-----------------------"
elif [ $1 == 0 ];then
   echo "--------------------"
   echo "preun RPM is getting removed/uninstalled"
#   echo "Put your script here which will be called before uninstallation of this rpm"
   echo "--------------------"
fi

%postun
case "$1" in
  0) # last one out put out the lights
    rm -f /usr/bin/mycity-delo-install
    rm -f /usr/bin/mycity-delo-logs
    rm -f /usr/bin/mycity-delo-start
    rm -f /usr/bin/mycity-delo-stop
    systemctl stop mycity-delo.service
    rm -f /etc/systemd/system/mycity-delo.service
  ;;
esac

