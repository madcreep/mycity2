#!/bin/bash



PACKETNAME='mycity2di'
RPMBINDIR='/Users/hedgehog/rpmbuild/RPMS/noarch/'
DOTNETPROJECT='BIT.MyCity.DeloIntegration'
BIZPREFIX=`cat .prefix`

RELEASENUM=`cat  ./release.gen`
echo 'release: '$RELEASENUM

export BUILDVERSION=2.0.$RELEASENUM-$BIZPREFIX

MYCITYBUILDFILENAME=$PACKETNAME-$BUILDVERSION.rpm

echo 'file: '$MYCITYBUILDFILENAME

mkdir ./sys
rm -r -f ./sys/*
cp -r ../$DOTNETPROJECT/bin/Debug/netcoreapp3.1/publish/* ./sys/

#mkdir ./opts
#rm -r -f ./opts/*
#cp -r ../System/* ./opts/

mkdir ./docs
cp -r ../Doc/Public* ./docs/

rpmbuild -bb ./mycity2di-rpm.spec  \
	--define "_target_os linux" \
	--define "_rpmfilename %%{ARCH}/$MYCITYBUILDFILENAME" 
# rm -r -f ./opts/*
#rm -r -f ./sys/*


cd $RPMBINDIR

ln -sf ./$MYCITYBUILDFILENAME di-latest-$BIZPREFIX
echo $MYCITYBUILDFILENAME

