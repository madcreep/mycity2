#/bin/sh

DOTNETPROJECT='BIT.MyCity.DeloIntegration'
BIZPREFIX=`cat ./.prefix`
RELEASENUM=`./buildver-increment.sh`
echo 'release: '$RELEASENUM

BUILDVERSION=2.0.0.$RELEASENUM

cd ../$DOTNETPROJECT/
rm -r ./bin/Debug/netcoreapp3.1/publish/*


echo $BUILDVERSION-$BIZPREFIX
dotnet clean
dotnet publish /p:Version=$BUILDVERSION-$BIZPREFIX-release  /p:AssemblyVersion=$BUILDVERSION

# dotnet publish  /p:AssemblyVersion=1.2.3.4
