FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env

WORKDIR /src
COPY . ./
RUN dotnet restore ./BIT.MyCity.DeloIntegration
RUN dotnet publish ./BIT.MyCity.DeloIntegration -o /publish

FROM mcr.microsoft.com/dotnet/aspnet:6.0 as runtime

WORKDIR /publish
COPY --from=build-env /publish .
ENV ASPNETCORE_URLS http://+:5000

RUN apt-get install -y tzdata
RUN echo "Europe/Moscow" > /etc/timezone
RUN ln -fs /usr/share/zoneinfo/Europe/Moscow /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata

ENTRYPOINT ["dotnet", "BIT.MyCity.DeloIntegration.dll"]