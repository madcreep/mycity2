using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphQL.Client.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using GraphQL.Client.Serializer.Newtonsoft;
using System.Net.Http;

namespace BIT.MyCity.DeloIntegration
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostEnvironment environment, ILoggerFactory loggerFactory)
        {
            Configuration = configuration;
            Environment = environment;
            loggerFactory.AddLog4Net();
        }

        public IHostEnvironment Environment { get; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);
            GlobalProperties.Username = Configuration["Username"];
            GlobalProperties.PasswordHash = Configuration["PasswordHash"];
            GlobalProperties.Salt = Configuration["Salt"];
            GlobalProperties.ApiPath = Configuration["ApiPath"];
            GlobalProperties.FileApiPath = Configuration["FileApiPath"];
            var timeOffset = Configuration["TimeOffset"];
            if (!string.IsNullOrEmpty(timeOffset))
            {
                GlobalProperties.TimeOffset = int.Parse(timeOffset);
            }

            // services.Configure<AuthOptions>(Configuration.GetSection("AuthOptions"));
            // services.AddSingleton<UserService>();
            services.AddScoped<GraphQLHttpClient>(x =>
                new GraphQLHttpClient(GlobalProperties.ApiPath, new NewtonsoftJsonSerializer())
            );

            //var authOptions = Configuration.GetSection("AuthOptions").Get<AuthOptions>();
            //GlobalProperties.AuthOptions = authOptions;

            services.AddControllers();
            services.AddRazorPages();
            services.AddLogging(builder => builder.AddLog4Net());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Delo}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }

    public class AuthOptions
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string SecureKey { get; set; }
    }
}
