using System;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace BIT.MyCity.DeloIntegration
{
    public interface IUserService
    {
        bool IsValidUser(UserModel user);
    }
    public class UserService : IUserService
    {
        public bool IsValidUser(UserModel user)
        {
            return GlobalProperties.Username == user.Username &&
                   GlobalProperties.PasswordHash == GlobalProperties.GetHash(user.Password);
        }

    }
}