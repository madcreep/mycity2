using System;

namespace BIT.MyCity.DeloIntegration.Models
{
    public interface ICatalog
    {
         string Name { get; set; }
         string ShortName { get; set; }
         int? Weight { get; set; }
         string ExtId { get; set; }
         bool? Deleted { get; set; }
         long? Id { get; set; }

         bool? IsNode { get; set; }
         object ParentId { get; set; }
    }
}