namespace BIT.MyCity.DeloIntegration.Models
{
    public class LoginResponse
    {
        public LoginResultType Login { get; set; }
    }

    // public class Login
    // {
    //     public string Token { get; set; }
    //     public string LoginStatus { get; set; }
    //     public string ErrorMessage { get; set; }
    // }
}