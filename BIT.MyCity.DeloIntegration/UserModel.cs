using System.ComponentModel.DataAnnotations;

namespace BIT.MyCity.DeloIntegration
{
    public class UserModel
    {
        [Required(ErrorMessage = "Username is required")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }
}
