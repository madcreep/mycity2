using Google.Apis.Requests;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;

namespace BIT.MyCity.DeloIntegration
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //CreateHostBuilder(args).Build().Run();
            CreateWebHostBuilder(args)
                // .ConfigureKestrel(serverOptions =>
                // {
                //     serverOptions.Limits.MaxRequestBodySize = 50 * 1024 * 1024;
                // })
                .Build().Run();

        }

        // public static IHostBuilder CreateHostBuilder(string[] args) =>
        //     Host.CreateDefaultBuilder(args)
        //         .ConfigureWebHostDefaults(webBuilder =>
        //         {
        //             webBuilder.UseStartup<Startup>();
        //         })
        //         .ConfigureLogging(logging =>
        //         {
        //             logging.AddConsole();
        //         });
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

    }
}
