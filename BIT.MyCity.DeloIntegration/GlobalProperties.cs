using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace BIT.MyCity.DeloIntegration
{
    public static class GlobalProperties
    {
        public static string Username { get; set; }
        public static string PasswordHash { get; set; }
        public static string Salt { get; set; }
        public static string ApiPath { get; set; }
        public static string FileApiPath { get; set; }

        public static int? TimeOffset { get; set; }

        public static AuthOptions AuthOptions { get; set; }

        public static string GetHash(string password)
        {
            //            byte[] salt = new byte[128 / 8];
            //            using (var rng = RandomNumberGenerator.Create())
            //            {
            //                rng.GetBytes(salt);
            //            }
            //            Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");

            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            return Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: Convert.FromBase64String(Salt),
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
        }
    }
}