﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using BIT.MyCity.DeloIntegration.Models;
using GraphQL;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using System.Net.Http;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Net;

namespace BIT.MyCity.DeloIntegration.Controllers
{

    enum SiteStatuses : int
    {
        Saved = 0,
        Registered = 1,
        Accepted = 2,
        AcceptedExternal = 3,
        AcceptedOtherDepartment = 4,
        Finished = 5,
        Rejected = 6,
        Prolonged = 7,
        Transferred = 8,
        Discontinued = 9
    }

    class QueryVariables
    {
        public object range { get; set; }
        public Filter[] filter { get; set; }
    }
    [ApiController, Route("api/delo")]
    public class DeloController : ControllerBase
    {

        private readonly GraphQLHttpClient _graphQLClient;
        private readonly ILogger<DeloController> _logger;
        private const string _appealFragment = @"{
                        address 
                        addressee
                        approved 
                        author {id fullName email loginSystem addresses {addrType fiasCode zipCode region city area street building partialAddress house flat}}
                        attachedFiles {id extId name path weight}
                        authorName
                        clientType
                        createdAt
                        description
                        email
                        appealExecutors {id extId name shortName organization position department}
                        appealPrincipalExecutors {id extId name shortName organization position department}
                        phone
                        extId
                        extNumber
                        firstName
                        patronim
                        lastName
                        id
                        oldSystemId
                        inn
                        municipality
                        number
                        organization {
                            id fullName inn kpp ogrn shortName email
                            addresses {fullAddress partialAddress zipCode city region} 
                            phones {type number}
                        }
                        territory {
                            name shortName extId
                        }
                        public_
                        region1
                        rubric {name extType extId docgroup docGroupForUL id}
                        regNumber
                        regDate
                        siteStatus
                        subsystem { uID }
                        topic {name extId id}
                        topicUpdated
                        zip
                        signatory
                        post
                    }";

        private const string deleteMutation = @"mutation delete($name: String!, $id: Long!) {
                delete (entity: {name: $name, id: $id})
            }";

        private const string sendMessageMutation = @"mutation
                                                  createEmail($emailTo: String, $nameTo: String, $subject: String!, $message: String!, $files: [ID], $withDeliveryNotice: Boolean!, $duplicateToSender: Boolean!) { 
                                                    createEmail(
                                                        customEmail:{
                                                            to: {
                                                                email: $emailTo,
                                                                name: $nameTo
                                                            },
                                                            subject: $subject,
                                                            message: $message,
                                                            filesId: $files,
                                                            withDeliveryNotice: $withDeliveryNotice,
                                                            duplicateToSender: $duplicateToSender
                                                        }
                                                    )
                                                }";
        private void ProcessErrors<T>(GraphQLResponse<T> response)
        {
            if (response != null && response.Errors != null && response.Errors.Length > 0)
            {
                // var ex = new Exception(string.Join(',', response.Errors.Select(e => $"{e.Message}; {string.Join('/', e.Path)}").ToArray()));
                var ex = new Exception(string.Join(',', response.Errors.Select(e => $"{e.Message}; ").ToArray()));
                _logger.LogError(ex, "Error on GraphQL request");
                throw ex;
            }
        }

        private void ProcessErrors<T>(GraphQLRequest request, GraphQLResponse<T> response)
        {
            if (response.Errors != null && response.Errors.Length > 0)
            {
                // var ex = new Exception(string.Join(',', response.Errors.Select(e => $"{e.Message}; {string.Join('/', e.Path)}").ToArray()));
                var ex = new Exception(string.Join(',', response.Errors.Select(e => $"{e.Message}; ").ToArray()));
                _logger.LogError(ex, "Error on GraphQL request");
                _logger.LogError($"Request was: {System.Text.Json.JsonSerializer.Serialize(request)}");
                _logger.LogDebug($"Errors: {JsonConvert.SerializeObject(response.Errors)}");
                throw ex;
            }
        }

        private void SetHeader()
        {
            if (Request.Headers.Any(h => h.Key == "Authorization") && _graphQLClient.HttpClient.DefaultRequestHeaders.All(h => h.Key != "Authorization"))
            {
                var token = Request.Headers.FirstOrDefault(v => v.Key == "Authorization").Value.ToString();
                if (!string.IsNullOrEmpty(token))
                {
                    _graphQLClient.HttpClient.DefaultRequestHeaders.Add("Authorization", $"{token}");
                }
            }
        }

        public DeloController(ILogger<DeloController> logger, GraphQLHttpClient graphQLClient)
        {
            _logger = logger;
            _graphQLClient = graphQLClient;
        }

        private GraphQLRequest GetSearchQuery()
        {
            return new GraphQLRequest
            {
                Query = @"query appeal($range: Range, $filter: [Filter]) {
                    appeal(range: $range, filter:$filter) {
                        items {
                            id
                            oldSystemId
                            extId
                            siteStatus
                            description
                            appealExecutors { id extId name shortName organization department position }
                            appealPrincipalExecutors { id extId name shortName organization department position }
                        }
                    }
                }"
            };
        }
        private GraphQLRequest GetFullCatalogQuery(string entityName, string[] subsystemId)
        {
            if (subsystemId == null || subsystemId.Length == 0)
            {
                return new GraphQLRequest
                {
                    Query = @"query " + entityName + @"{
                        " + entityName + @"(range: {skip: 0, take: 10000}, filter: {name: ""extId"", not: true, equal: null, withDeleted: true}) {
                            items {
                                id
                                extId
                                name
                                shortName
                                weight
                                deleted
                            }
                        }
                    }"
                };
            }
            return new GraphQLRequest
            {
                Query = @"query " + entityName + @"($subsystem: [String]!) {
                        " + entityName + @" (filter: {name: " + "\"subsystem.uID\"" + @", in: $subsystem, withDeleted: true}, range: {skip: 0, take: 10000}) {
                            items {
                                id
                                extId
                                name
                                shortName
                                weight
                                deleted
                            }
                        }
                    }",
                Variables = new { subsystem = subsystemId }
            };
        }

        private GraphQLRequest GetCreateMutation(string entityName, object entity)
        {
            var entityNormalizedName = GetNormalizedName(entityName);
            return new GraphQLRequest
            {
                Query =
                @"mutation create" + entityNormalizedName + @"($value: create" + entityNormalizedName + @"!) {
                    create" + entityNormalizedName + @"(" + entityName + @": $value) {
                        id
                        name
                    }
                }",
                Variables = new
                {
                    value = entity
                }

            };
        }

        private GraphQLRequest GetUpdateMutation(string entityName, object entity, long id, bool delete = false)
        {
            var entityNormalizedName = entityName.Substring(0, 1).ToUpper() + entityName.Substring(1);
            return new GraphQLRequest
            {
                Query =
                        @"mutation update" + entityNormalizedName + @"($id: ID!, $value: update" + entityNormalizedName + @"!) {
                            update" + entityNormalizedName + @"(id: $id, " + entityName + @": $value) {
                                id
                                name
                            }
                        }",
                Variables = new
                {
                    id = id,
                    value = entity
                }

            };
        }

        private string GetNormalizedName(string name)
        {
            return name.Substring(0, 1).ToUpper() + name.Substring(1);
        }

        private async Task<Appeal> GetAppeal(string extId)
        {
            var query = GetSearchQuery();
            query.Variables = new
            {
                range = new { skip = 0, take = 1 },
                filter = new [] { new { name = "extId", equal = extId } }
            };
            _logger.LogDebug($"Vars: {JsonConvert.SerializeObject(query.Variables)}");
            var response = await _graphQLClient.SendQueryAsync<Query>(query);
            ProcessErrors<Query>(response);
            if (response.Data?.Appeal?.Items == null || response.Data.Appeal.Items.Count == 0)
            {
                _logger.LogDebug($"Appeal was not found");
                return null;
            } 
            _logger.LogDebug($"Appeal was found");
            return response.Data.Appeal.Items.FirstOrDefault();

        }

        private async Task<Appeal> GetAppealById(long? id)
        {
            if (id == null) return null;
            var query = GetSearchQuery();
            query.Variables = new
            {
                range = new { skip = 0, take = 1 },
                filter = new [] { new { name = "id", equal = id } }
            };
            var response = await _graphQLClient.SendQueryAsync<Query>(query);
            ProcessErrors<Query>(response);
            if (response.Data == null || response.Data.Appeal == null || response.Data.Appeal.Items.Count == 0)
            {
                _logger.LogDebug($"appeal wasn't found by id: {id}");
                return null;
            }
            return response.Data.Appeal.Items.FirstOrDefault();

        }
        private async Task<Appeal> GetAppealByOldSystemId(string? id)
        {
            var query = GetSearchQuery();
            query.Variables = new
            {
                range = new { skip = 0, take = 1 },
                filter = new Filter[] { new Filter { Name = "oldSystemId", Equal = id } }
            };
            var response = await _graphQLClient.SendQueryAsync<Query>(query);
            ProcessErrors<Query>(response);
            if (response.Data == null || response.Data.Appeal == null || response.Data.Appeal.Items.Count == 0)
            {
                return null;
            }
            return response.Data.Appeal.Items.FirstOrDefault();

        }

        [HttpPost, Route("auth"), AllowAnonymous]
        public async Task<IActionResult> Auth(UserModel user)
        {
            SetHeader();
            var loginQuery = new GraphQLRequest
            {
                Query =
                @"query login($user: LoginType!) {
                    login(login: $user) {
                        token
                        loginStatus
                        errorMessage
                    }
                }",
                Variables = new
                {
                    user = new
                    {
                        login = user.Username,
                        password = user.Password
                    }
                }
            };
            var response = await _graphQLClient.SendQueryAsync<Query>(loginQuery);
            ProcessErrors<Query>(response);
            return Ok(response.Data.Login);
        }

        private async Task<GraphQLResponse<Query>> GetSettings(string name) 
        {
            var query = new GraphQLRequest
            {
                Query =
                @"query settings($key: [String]) {
                    settings(key:$key) {
                        value
                    }
                }",
                Variables = new
                {
                    key = name
                },


            };

            try {
                var response = await _graphQLClient.SendQueryAsync<Query>(query);
                ProcessErrors<Query>(response);
                return response;
            } catch (Exception ex) {
                _logger.LogError(ex, $"Error on getting appeals {ex.Message}");
                throw ex;
            }

        }
        private async Task<GraphQLResponse<Query>> GetAppeals(object filter)
        {
            var query = new GraphQLRequest
            {
                Query =
                @"query appeal($range: Range, $filter: [Filter]) {
                    appeal(range: $range, filter:$filter) {
                        items {
                            ...appealFragment
                        }
                    }
                }
                fragment appealFragment on Appeal " + _appealFragment,
                Variables = new
                {
                    range = new { orderBy = new[] { new { field = "createdAt", direction = "ASC" } }, skip = 0, take = 100 },
                    filter = filter
                },


            };

            try {
                var response = await _graphQLClient.SendQueryAsync<Query>(query);
                ProcessErrors<Query>(response);
                return response;
            } catch (Exception ex) {
                _logger.LogError(ex, $"Error on getting appeals {ex.Message}");
                throw ex;
            }

        }

        private async Task ProcessNewAppealsQuery(List<object> filter, List<Appeal> result) 
        {
            _logger.LogDebug($"Filter: {JsonConvert.SerializeObject(filter)}");

            var response = await GetAppeals(filter.ToArray());
            _logger.LogDebug($"Items: {JsonConvert.SerializeObject(response.Data.Appeal.Items)}");
            foreach (var appeal in response.Data.Appeal.Items)
            {
                if (appeal.Author.LoginSystem == LoginSystem.Esia)
                {
                    appeal.Esia = true;
                }
            }
            result.AddRange(response.Data.Appeal.Items);
        }

        [HttpGet, Route("new_appeals/{subsystemId}")]
        public async Task<IActionResult> Appeals(string subsystemId)
        {
            _logger.LogDebug("new appeals request from: {0}", HttpContext.Connection.RemoteIpAddress);
            SetHeader();

            var filter = new List<object> 
            {
                new {name = "sentToDelo", equal = "false"},
                new {name = "appealType", equal = "NORMAL"}
            };
            var result = new List<Appeal>();
            var union = false;
            if (subsystemId == "citizens")
            {
                var settings = await GetSettings("subsystem.citizens.autoregister_wait.");
                var minutes = 0;
                if (settings != null && settings.Data != null && settings.Data.Settings != null)
                {
                    var value = settings.Data.Settings.FirstOrDefault().Value;
                    if (value != null)
                    {
                        minutes = int.Parse(value);
                    }

                    if (minutes < 0)
                    {
                        filter.Add(new { name = "deloStatus", equal = "2register" });
                    }
                    else if (minutes > 0)
                    {
                        union = true;
                        filter.Add(new
                        {
                            name = "createdAt",
                            lessOrEqual = DateTime.Now.AddMinutes(-minutes).ToString("yyyy-MM-ddTHH:mm:ss")
                        });
                    }
                }
            }
            filter.Add(new {name = "subsystem.uID", equal = subsystemId});

            await ProcessNewAppealsQuery(filter, result);
            if(union) 
            {
                filter.RemoveAt(filter.Count - 1);
                filter.Add(new {name = "deloStatus", equal = "2register"});
                await ProcessNewAppealsQuery(filter, result);

            }
            return Ok(result);
        }

        [HttpGet, Route("updated_appeals")]
        public async Task<IActionResult> UpdatedAppeals()
        {
            SetHeader();
            var response = await GetAppeals(new object[]
                    {
                        // new {name = "uploaded", equal = "true"},
                        new {name = "sentToDelo", equal = "true"},
                        new {name = "needToUpdateInExt", equal = "true"},
                        new {name = "appealType", equal = "NORMAL"}
                    });
            foreach (var appeal in response.Data.Appeal.Items)
            {
                if (appeal.Author.LoginSystem == LoginSystem.Esia)
                {
                    appeal.Esia = true;
                }
            }
            return Ok(response.Data.Appeal.Items);
        }

        private DateTime? FromString(object value)
        {
            if (value == null) return null;
            var strValue = value.ToString();
            strValue = strValue.Replace('z', 'Z');
            var formatStr = $"yyyy-MM-ddTHH:mm:ss{(strValue.EndsWith("Z") ? "Z" : "")}";
            if (strValue.Length == 25)
            {
                formatStr += "zzz";
            }
            var result = DateTime.ParseExact(strValue, formatStr, CultureInfo.InvariantCulture);
            if (GlobalProperties.TimeOffset != null)
            {
                result = result.AddHours((double)GlobalProperties.TimeOffset);
            }
            return result;

        }

        private GraphQLRequest GetAppealSaveQuery(Appeal appeal, int? siteStatus)
        {
            // var idDefinition = string.IsNullOrEmpty(appeal.OldSystemId) ? "$id: ID!" : "$oldSystemId: String!";
            // var idParameter = string.IsNullOrEmpty(appeal.OldSystemId) ? "id: $id" : "oldSystemId: $oldSystemId";
            // var idField = string.IsNullOrEmpty(appeal.OldSystemId) ? "id" : "oldSystemId";
            var idDefinition = "$id: ID!";
            var idParameter = "id: $id";
            var idField = "id";
            var graphAppeal = new
            {
                ExternalExec = appeal.ExternalExec == null ? false : appeal.ExternalExec.Value,
                ExtNumber = appeal.ExtNumber,
                ExtDate = FromString(appeal.ExtDate),
                ExtId = appeal.ExtId,
                DeloStatus = appeal.DeloStatus,
                DeloText = appeal.DeloText,
                FactDate = FromString(appeal.FactDate),
                ResDate = FromString(appeal.ResDate),
                SentToDelo = appeal.SentToDelo,
                TopicUpdated = appeal.TopicUpdated,
                NeedToUpdateInExt = appeal.NeedToUpdateInExt,
                SiteStatus = siteStatus
            };
            object variables;
            // if (string.IsNullOrEmpty(appeal.OldSystemId))
            // {
                variables = new
                {
                    id = appeal.Id,
                    appeal = graphAppeal
                };
            // }
            // else
            // {
            //     variables = new
            //     {
            //         oldSystemId = appeal.OldSystemId,
            //         appeal = graphAppeal
            //     };
            // }

            var query = new GraphQLRequest
            {
                Query =
                    @"mutation updateApeal(" + idDefinition + @", $appeal: updateAppeal!) {
                    updateAppeal(" + idParameter + @", appeal: $appeal) {
                        " + idField + @"
                    }
                }",
                Variables = variables
            };
            return query;
        }
        
        [HttpPost, Route("save_appeal")]
        public async Task<IActionResult> SaveAppeal(Appeal appeal)
        {
            SetHeader();
            _logger.LogDebug($"Appeal date: {appeal.ExtDate}");
            var dbAppeal = string.IsNullOrEmpty(appeal.OldSystemId)
                ? await GetAppealById(appeal.Id)
                : await GetAppealByOldSystemId(appeal.OldSystemId);
            if (dbAppeal == null)
            {
                return Ok();
            }
            //var id = dbAppeal.Id;
            //_logger.LogDebug($"Id: {id}");
            var siteStatus = dbAppeal.SiteStatus;
            if (siteStatus == null || siteStatus < 1)
            {
                siteStatus = 1;
            }

            if (!string.IsNullOrEmpty(appeal.OldSystemId))
            {
                var idQuery = new GraphQLRequest
                {
                    Query = @"query appeal($filter: [Filter]) {
	                        appeal(filter:$filter) {
		                        items {
			                        id
		                        }
	                        }
                        }",
                    Variables = new
                    {
                        filter = new[] { new { name = "oldSystemId", equal = appeal.OldSystemId } }
                    }
                };
                var resp = await _graphQLClient.SendQueryAsync<Query>(idQuery);
                ProcessErrors<Query>(resp);
                if (resp.Data?.Appeal?.Items == null || resp.Data?.Appeal?.Items.Count == 0)
                {
                    throw new Exception("Appeal not found");
                }
                appeal.Id = resp.Data?.Appeal?.Items.FirstOrDefault()?.Id;
            }

            var query = GetAppealSaveQuery(appeal, siteStatus);
            _logger.LogDebug(JsonConvert.SerializeObject(query));
            var appealResponse = await _graphQLClient.SendMutationAsync<Mutation>(query);
            _logger.LogDebug(JsonConvert.SerializeObject(appealResponse));
            ProcessErrors<Mutation>(appealResponse);
            return Ok(appealResponse.Data.UpdateAppeal);

        }

        private async Task SaveCatalog<T>(IList<T> values, string entityName, string subsystemIds) where T : ICatalog
        {
            _logger.LogDebug($"Subsystem IDs: {subsystemIds}");
            string[] subsystems = null;
            if (!string.IsNullOrEmpty(subsystemIds))
            {
                subsystems = subsystemIds.Split(',');
            }
            var query = GetFullCatalogQuery(entityName, entityName == "topic" ? subsystems : null);
            _logger.LogDebug($"Full query: {JsonConvert.SerializeObject(query)}");
            var response = await _graphQLClient.SendQueryAsync<Query>(query);
            ProcessErrors<Query>(response);
            var entityNormalizedName = GetNormalizedName(entityName);
            var property = response.Data.GetType().GetProperty(entityNormalizedName);
            var paginatedValue = property.GetValue(response.Data);
            var itemsProperty = paginatedValue.GetType().GetProperty("Items");
            var dbValues = (ICollection<T>)itemsProperty.GetValue(paginatedValue);
            _logger.LogDebug($"Found {dbValues.Count} values");
            var valueIds = new Dictionary<string, long?>();
            var propertyName = "";
            foreach (var value in values)
            {
                _logger.LogDebug($"Processing {value.Name}");
                var dbValue = dbValues.FirstOrDefault(v => v.ExtId == value.ExtId);
                _logger.LogDebug($"dbValue {dbValue}");
                GraphQLRequest saveMutation = null;
                object entity = null;
                if (subsystems == null || subsystems.Length == 0 || (dbValue != null && (entityName == "topic" || entityName == "rubric")))
                {
                    _logger.LogDebug($"No subsystem");

                    entity = new
                    {
                        Name = value.Name,
                        ShortName = value.ShortName,
                        Weight = value.Weight,
                        ExtId = value.ExtId,
                        IsNode = value.IsNode,
                        Deleted = false
                    };
                }
                else
                {
                    _logger.LogDebug($"SubSystemUIDs {subsystems}");
                    entity = new
                    {
                        Name = value.Name,
                        ShortName = value.ShortName,
                        Weight = value.Weight,
                        ExtId = value.ExtId,
                        SubSystemUIDs = subsystems,
                        IsNode = value.IsNode,
                        Deleted = false
                    };
                }

                // }
                if (dbValue == null)
                {
                    _logger.LogDebug($"Creating entity {value.Name}");
                    saveMutation = GetCreateMutation(entityName, entity);
                    propertyName = "Create" + entityNormalizedName;
                }
                else
                {
                    _logger.LogDebug($"Updating entity {value.Name}");
                    saveMutation = GetUpdateMutation(entityName, entity, dbValue.Id.Value);
                    propertyName = "Update" + entityNormalizedName;
                }
                _logger.LogDebug($"Sending mutation {JsonConvert.SerializeObject(saveMutation)}");
                var resp = await _graphQLClient.SendMutationAsync<Mutation>(saveMutation);
                _logger.LogDebug($"resp: {(resp == null ? "NULL" : JsonConvert.SerializeObject(resp))}");
                ProcessErrors<Mutation>(resp);
                if (resp.Errors != null && resp.Errors.Length > 0)
                {
                    var ex = new Exception(string.Join(',', resp.Errors.Select(e => e.Message).ToArray()));
                    _logger.LogError(ex, "Error on GraphQL request - saving catalog");
                    throw ex;
                }
                var respProperty = resp.Data.GetType().GetProperty(propertyName);
                var propValue = respProperty.GetValue(resp.Data);
                if (propValue != null)
                {
                    var idProp = propValue.GetType().GetProperty("Id");
                    valueIds.Add(value.ExtId, (long?)idProp.GetValue(propValue));
                }
            }
            var valuesToDelete = dbValues.Where(dbVal => values.All(val => val.ExtId != dbVal.ExtId)).ToList();
            foreach (var value in valuesToDelete)
            {
                value.Deleted = true;
                var mutation = GetUpdateMutation(entityName, new { Deleted = true }, value.Id.Value, true);
                var updateResp = await _graphQLClient.SendMutationAsync<Mutation>(mutation);
                ProcessErrors<Mutation>(updateResp);
            }

        }

        [HttpPost, Route("save_territory/{subsystemId}")]
        public async Task<IActionResult> SaveTerritories(IList<Territory> values, string subsystemId)
        {
            SetHeader();
            await SaveCatalog(values, "territory", subsystemId);
            return Ok();
        }

        [HttpPost, Route("save_topic/{subsystemId}")]
        public async Task<IActionResult> SaveTopics(IList<Topic> values, string subsystemId)
        {
            SetHeader();
            await SaveCatalog(values, "topic", subsystemId);
            return Ok();
        }

        [HttpPost, Route("save_rubric/{subsystemId}")]
        public async Task<IActionResult> SaveRubrics(IList<Rubric> values, string subsystemId)
        {
            try {
                _logger.LogDebug($"Rubric subsystem ids: {subsystemId}");
                SetHeader();
                var query = new GraphQLHttpRequest
                {
                    Query =
                        @"query {
                            subsystem
                                {
                                    items
                                {
                                    id
                                    uID
                                    name
                                }
                                    }
                            }"
                };
                var subsystemItems = await _graphQLClient.SendQueryAsync<Query>(query);
                _logger.LogDebug($"subsystemItems: {subsystemItems.Data.Subsystem.Items}");
                var settings = new Dictionary<string, bool>();
                query = new GraphQLHttpRequest 
                {
                    Query = @"query settings($key:[String]){
                        settings(key: $key)
                            {
                                key
                            value
                                }
                        }"
                    
                };
                var subIds = subsystemId.Split(',');
                foreach (var subsys in subsystemItems.Data.Subsystem.Items) 
                {
                    var subId = subsys.Uid;
                    _logger.LogDebug($"subId: {subId}");
                    if (!subIds.Any(v => v == subId)) 
                    {
                        continue;
                    }
                    _logger.LogDebug($"Checking subId: {subId}");
                    query.Variables = new {key = "subsystem." + subId + ".enable."};
                    _logger.LogDebug($"Sending query");
                    var result = await _graphQLClient.SendQueryAsync<Query>(query);
                    _logger.LogDebug($"result: {result}");
                    if (result.Data.Settings != null && result.Data.Settings.Any())
                    {
                        settings[subId] = bool.Parse(result.Data.Settings.FirstOrDefault().Value);
                    }
                    else
                    {
                        settings[subId] = false;
                    }
                }
                var newSubsystems = string.Join(",", settings.Where(s => s.Value).Select(v => v.Key));
                _logger.LogDebug($"New subsystems: {newSubsystems}");
                await SaveCatalog(values, "rubric", newSubsystems);
            } catch (Exception ex) {
                _logger.LogError($"{ex.Message}: {ex.StackTrace}", ex);
                throw;
            }
            return Ok();
        }

        [HttpPost, Route("save_region/{subsystemId}")]
        public async Task<IActionResult> SaveRegions(IList<Region> values, string subsystemId)
        {
            SetHeader();
            await SaveCatalog(values, "region", subsystemId);
            return Ok();
        }

        [HttpPost, Route("save_file")]
        public async Task<IActionResult> SaveFile(AttachedFile file)
        {
            SetHeader();
            _logger.LogDebug("Saving file with extId {0} and name {1}", file.ExtId, file.Name);
            var request = new GraphQLHttpRequest
            {
                Query = @"mutation updateAttachedFile($id: ID!, $file: updateAttachedFile!) {
                            updateAttachedFile(id: $id, attachedFile: $file) {
                                id
                                extId,
                                name,
                                weight
                                private
                            }
                    }",
                Variables = new
                {
                    id = file.Id,
                    file = new
                    {
                        ExtId = file.ExtId,
                        Name = file.Name,
                        Weight = file.Weight
                    }
                }
            };
            var response = await _graphQLClient.SendMutationAsync<Mutation>(request);
            ProcessErrors<Mutation>(response);
            return Ok(response.Data.UpdateAttachedFile);
        }

        [HttpPost, Route("save_message/{appealExtId}/{status}")]
        public async Task<IActionResult> SaveMessage(string appealExtId, Message message, int? status)
        {
            SetHeader();
            var appeal = await GetAppeal(appealExtId);
            if (appeal == null || (appeal.SiteStatus != null && appeal.SiteStatus == status))
            {
                return Ok();
            }
            var query = new GraphQLRequest
            {
                Query =
                @"mutation createMessage($value: createMessage!) {
                    createMessage(message: $value) {
                        id
                    }
                }",
                Variables = new
                {
                    value = new
                    {
                        Weight = message.Weight,
                        ParentId = appeal.Id,
                        ParentType = "Appeal",
                        Text = message.Text,
                        Kind = "SYSTEM_USER_MESSAGE",
                        Approved = "ACCEPTED",
                        // moderationStage = "DONE"
                    }
                }

            };
            var response = await _graphQLClient.SendMutationAsync<Mutation>(query);
            ProcessErrors<Mutation>(query, response);
            if (response.Data == null || response.Data.CreateMessage == null)
            {
                return BadRequest();
            }
            _logger.LogDebug($"Created message id: {response.Data.CreateMessage.Id}");
            return Ok(response.Data.CreateMessage.Id);
        }

        [HttpPost, Route("send_file/{messageId}")]
        public async Task<IActionResult> SendFile(string messageId, IFormFile file)
        {
            if (messageId == "null") messageId = null;
            _logger.LogDebug("Message id: {0}, file: {1}", messageId, file.Name);
            SetHeader();
            var request = new GraphQLRequest
            {
                Query = @"mutation fileUpload($file: FileUpload!) {
                    fileUpload(file: $file) {
                        id
                        next
                        percent
                    }
                }"
            };
            var filePath = Path.GetTempFileName();
            using (var stream = System.IO.File.Create(filePath))
            {
                await file.CopyToAsync(stream);
            }
            long fileId = 0;
            using (var stream = System.IO.File.Open(filePath, FileMode.Open))
            {
                using (var binaryReader = new BinaryReader(stream))
                {
                    byte[] buffer;
                    long offset = 0;
                    while ((buffer = binaryReader.ReadBytes(4096 * 100)).Length > 0)
                    {
                        var str = Convert.ToBase64String(buffer);
                        if (fileId == 0)
                        {
                            request.Variables = new
                            {
                                file = new
                                {
                                    name = file.Name,
                                    len = file.Length,
                                    pos = offset,
                                    data = str,
                                    parentId = string.IsNullOrEmpty(messageId) ? null : messageId,
                                    parentType = string.IsNullOrEmpty(messageId) ? null : "Message"
                                }
                            };
                        }
                        else
                        {
                            request.Variables = new
                            {
                                file = new
                                {
                                    id = fileId,
                                    pos = offset,
                                    data = str,
                                    parentId = string.IsNullOrEmpty(messageId) ? null : messageId,
                                    parentType = string.IsNullOrEmpty(messageId) ? null : "Message"
                                }
                            };
                        }
                        // _logger.LogDebug(JsonConvert.SerializeObject(request));
                        var response = await _graphQLClient.SendMutationAsync<Mutation>(request);
                        ProcessErrors<Mutation>(response);
                        if (fileId == 0)
                        {
                            fileId = (long)response.Data.FileUpload.Id;
                        }
                        offset += buffer.Length;
                    }

                }
            }
            System.IO.File.Delete(filePath);
            return Ok(fileId);
        }


        [HttpDelete, Route("delete_message/{messageId}")]
        public async Task<IActionResult> DeleteMessage(long messageId)
        {
            _logger.LogDebug($"Message id: {messageId}");
            SetHeader();
            var request = new GraphQLRequest
            {
                Query = deleteMutation,
                Variables = new {
                    name = "message",
                    id = messageId
                }
            };
            var response = await _graphQLClient.SendMutationAsync<Mutation>(request);
            return Ok(response.Data);
        }

        [HttpDelete, Route("delete_file/{fileId}")]
        public async Task<IActionResult> DeleteFile(long fileId)
        {
            _logger.LogDebug($"File id: {fileId}");
            SetHeader();
            var request = new GraphQLRequest
            {
                Query = deleteMutation,
                Variables = new {
                    name = "attachedFile",
                    id = fileId
                }
            };
            var response = await _graphQLClient.SendMutationAsync<Mutation>(request);
            return Ok(response.Data);
        }

        [HttpDelete, Route("{name}/{id}")]
        public async Task<IActionResult> Delete(string name, long id)
        {
            _logger.LogDebug($"Entity {name} with id {id}");
            SetHeader();
            SetHeader();
            var request = new GraphQLRequest
            {
                Query = deleteMutation,
                Variables = new {
                    name = name,
                    id = id
                }
            };
            var response = await _graphQLClient.SendMutationAsync<Mutation>(request);
            return Ok(response.Data);
        }



        [HttpGet, Route("file/{id}")]
        public async Task<IActionResult> GetFile(string id)
        {
            SetHeader();
            _logger.LogDebug($"Getting file {id}");
            using (var client = new HttpClient())
            {
                var token = Request.Headers.FirstOrDefault(h => h.Key == "Authorization").Value.ToString();
                client.DefaultRequestHeaders.Add("authorization", token);
                _logger.LogDebug($"Getting there: {GlobalProperties.FileApiPath + "/" + id}");
                var response = await client.GetAsync(GlobalProperties.FileApiPath + "/" + id);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    _logger.LogError($"Something's wrong: {response}");
                    return StatusCode((int)response.StatusCode);
                }
                System.Net.Http.HttpContent content = response.Content;
                var contentStream = await content.ReadAsStreamAsync();
                //var contentType = response.Headers.GetValues("Content-Type").FirstOrDefault();
                return new FileStreamResult(contentStream, new MediaTypeHeaderValue("application/octet-stream"));
            }
        }

        [HttpPost, Route("set_status")]
        public async Task<IActionResult> SetStatus(Appeal appeal)
        {
            SetHeader();
            var dbAppeal = await GetAppeal(appeal.ExtId);
            if (dbAppeal == null)
            {
                _logger.LogDebug($"appeal {appeal.ExtId} not found");
                return Ok();
            }
            if (appeal.SiteStatus == dbAppeal.SiteStatus)
            {
                return Ok();
            }
            var request = new GraphQLRequest
            {
                Query = @"mutation updateAppeal($id: ID!, $appeal: updateAppeal!) {
                    updateAppeal(id: $id, appeal: $appeal) {
                        id
                        siteStatus
                    }
                }",
                Variables = new
                {
                    id = dbAppeal.Id,
                    appeal = new
                    {
                        siteStatus = appeal.SiteStatus
                    }
                }
            };
            var response = await _graphQLClient.SendMutationAsync<Mutation>(request);
            ProcessErrors<Mutation>(response);
            return Ok(response.Data.UpdateAppeal);

        }

        private async Task<List<long?>> ProcessExecutors(ICollection<AppealExecutor> newCollection, ICollection<AppealExecutor> dbCollection)
        {
            var result = new List<long?>();
            var execFragment = @"{
                    id
                    extId
                    name
                    shortName
                    organization
                    department
                    position
                }";
            var updateRequest = new GraphQLHttpRequest
            {
                Query = @"mutation updateAppealExecutor($id: ID!, $executor: updateAppealExecutor!) {
                            updateAppealExecutor(id: $id, appealExecutor: $executor) {
                                ...executorFragment
                            }
                        }
                        fragment executorFragment on AppealExecutor " + execFragment
            };
            var createRequest = new GraphQLHttpRequest
            {
                Query = @"mutation createAppealExecutor($executor: createAppealExecutor!) {
                            createAppealExecutor(appealExecutor: $executor) {
                                ...executorFragment
                            }
                        }
                        fragment executorFragment on AppealExecutor " + execFragment
            };
            var query = new GraphQLHttpRequest
            {
                Query = @"query appealExecutor($filter: [Filter]) {
                            appealExecutor(range: {skip: 0, take: 1}, 
                                            filter:$filter) {
                                items {
                                    ...executorFragment
                                }
                            }
                        }
                        fragment executorFragment on AppealExecutor " + execFragment
            };
            var execsToAdd = new List<AppealExecutor>();
            if (newCollection != null)
            {
                foreach (var exec in newCollection)
                {
                    _logger.LogDebug($"Processing exec {exec}");
                    if (exec == null) continue;
                    GraphQLHttpRequest request = null;
                    var dbExecutor = dbCollection == null ? null : dbCollection.FirstOrDefault(e => e != null && e.ExtId == exec.ExtId);
                    _logger.LogDebug($"DB exec {dbExecutor}");
                    var updateExec = new
                    {
                        extId = exec.ExtId,
                        organization = exec.Organization,
                        position = exec.Position,
                        department = exec.Department,
                        name = exec.Name,
                        shortName = exec.ShortName
                    };
                    var addValue = false;
                    if (dbExecutor == null)
                    {
                        _logger.LogDebug($"DB exec is null");
                        query.Variables = new
                        {
                            filter = new[] { new { name = "extId", equal = exec.ExtId } }
                        };
                        _logger.LogDebug($"Sendig query");
                        var resp = await _graphQLClient.SendQueryAsync<Query>(query);
                        ProcessErrors<Query>(resp);
                        if (resp.Data == null || resp.Data.AppealExecutor == null || resp.Data.AppealExecutor.Items == null || resp.Data.AppealExecutor.Items.Count == 0)
                        {

                            _logger.LogDebug($"Exec not found {updateRequest}");
                            createRequest.Variables = new
                            {
                                executor = updateExec
                            };
                            request = createRequest;
                        }
                        else
                        {
                            dbExecutor = resp.Data.AppealExecutor.Items.FirstOrDefault();
                            _logger.LogDebug($"Exec found {dbExecutor}");
                            updateRequest.Variables = new
                            {
                                id = dbExecutor.Id,
                                executor = updateExec
                            };
                            request = updateRequest;
                        }
                        addValue = true;
                    }
                    else
                    {
                        _logger.LogDebug($"DB is not null");
                        updateRequest.Variables = new
                        {
                            id = dbExecutor.Id,
                            executor = updateExec
                        };
                        request = updateRequest;
                        if (dbCollection != null)
                        {
                            dbCollection.Remove(dbExecutor);
                        }
                    }
                    _logger.LogDebug($"Sending mutation");
                    var response = await _graphQLClient.SendMutationAsync<Mutation>(request);
                    ProcessErrors<Mutation>(response);
                    if (response.Data != null && addValue)
                    {
                        _logger.LogDebug($"Received data");
                        if (response.Data.UpdateAppealExecutor != null)
                            result.Add(response.Data.UpdateAppealExecutor.Id);
                        else
                            result.Add(response.Data.CreateAppealExecutor.Id);
                    }
                }
            }
            if (dbCollection != null && dbCollection.Any())
            {
                _logger.LogDebug($"Adding result");
                result.AddRange(dbCollection.Select(val => -val.Id));
                _logger.LogDebug($"IDs to remove from collection: {string.Join(',', dbCollection.Select(val => -val.Id).ToArray())}");
            }
            _logger.LogDebug($"All done");
            return result;
        }

        [HttpPost, Route("set_executors")]
        public async Task<IActionResult> SetExecutors(Appeal appeal)
        {
            SetHeader();
            var dbAppeal = await GetAppeal(appeal.ExtId);
            if (dbAppeal == null)
            {
                _logger.LogDebug($"appeal {appeal.ExtId} not found");
                return Ok();
            }
            var newExec = await ProcessExecutors(appeal.AppealExecutors, dbAppeal.AppealExecutors);
            _logger.LogDebug($"Execs: {string.Join(',', newExec)}");
            var newPrincipalExec = await ProcessExecutors(appeal.AppealPrincipalExecutors, dbAppeal.AppealPrincipalExecutors);
            var updateRequest = new GraphQLHttpRequest
            {
                Query = @"mutation updateAppeal($id: ID!, $appeal: updateAppeal!) {
                            updateAppeal(id: $id, appeal: $appeal) {
                                id
                            }
                        }",
                Variables = new
                {
                    id = dbAppeal.Id,
                    appeal = new
                    {
                        appealExecutorIdsChange = newExec,
                        appealPrincipalExecutorIdsChange = newPrincipalExec
                    }
                }
            };
            _logger.LogDebug($"{JsonConvert.SerializeObject(updateRequest)}");
            var response = await _graphQLClient.SendMutationAsync<Mutation>(updateRequest);
            ProcessErrors<Mutation>(response);
            return Ok(response.Data.UpdateAppeal);
        }

        [HttpPost, Route("set_resolution")]
        public async Task<IActionResult> SetResolutionAttributes(Appeal appeal)
        {
            SetHeader();
            var dbAppeal = await GetAppeal(appeal.ExtId);
            if (dbAppeal == null)
            {
                _logger.LogDebug($"Appeal not found: {appeal.ExtId}");
                return Ok();
            }

            var updateRequest = new GraphQLHttpRequest
            {
                Query = @"mutation updateAppeal($id: ID!, $appeal: updateAppeal!) {
                            updateAppeal(id: $id, appeal: $appeal) {
                                id
                            }
                        }",
                Variables = new
                {
                    id = dbAppeal.Id,
                    appeal = new
                    {
                        planDate = FromString(appeal.PlanDate),
                        factDate = FromString(appeal.FactDate),
                        resDate = FromString(appeal.ResDate),
                        deloText = appeal.DeloText,
                        deloStatus = appeal.DeloStatus
                    }
                }
            };
            var response = await _graphQLClient.SendMutationAsync<Mutation>(updateRequest);
            _logger.LogDebug($"set resolution: {JsonConvert.SerializeObject(response)}");
            ProcessErrors<Mutation>(response);
            return Ok(response.Data.UpdateAppeal);
        }

        [HttpGet, Route("get_requests")]
        public async Task<IActionResult> GetAppealRequests()
        {
            throw new NotImplementedException("get_requests isn't implemented yet");
        }
        
        [HttpPost, Route("send_response")]
        public async Task<IActionResult> SendAppealResponse(List<Appeal> appealResponse)
        {
            throw new NotImplementedException("get_requests isn't implemented yet");
        }

        [HttpPost, Route("save_notification/{appealExtId}/{status}")]
        public async Task<IActionResult> SaveNotification(string appealExtId, Message message, int? status)
        {
            SetHeader();
            var query = new GraphQLRequest
            {
                Query =
                    @"mutation createMessage($value: createMessage!) {
                    createMessage(message: $value) {
                        id
                    }
                }",
                Variables = new
                {
                    value = new
                    {
                        Weight = message.Weight,
                        //ParentType = "Notification",
                        Text = message.Text,
                        Kind = "SYSTEM_USER_MESSAGE",
                        Approved = "ACCEPTED",
                        // moderationStage = "DONE"
                    }
                }

            };
            var response = await _graphQLClient.SendMutationAsync<Mutation>(query);
            ProcessErrors<Mutation>(query, response);
            if (response.Data == null || response.Data.CreateMessage == null)
            {
                return BadRequest();
            }
            _logger.LogDebug($"Created message id: {response.Data.CreateMessage.Id}");
            return Ok(response.Data.CreateMessage.Id);

        }

        [HttpPost, Route("send_notification")]
        public async Task<IActionResult> SendNotification(Message message)
        {
            SetHeader();
            _logger.LogDebug($"Sending message to {message.RootAppeal.Author.FullName}  with email {message.RootAppeal.Author.Email}");
            _logger.LogDebug($"Message: {message}; files: {JsonConvert.SerializeObject(message.AttachedFiles.Select(f => f.Id).ToArray())}");
            Appeal? appeal = null;
            if (message.RootAppeal.Id != null)
            {
                _logger.LogDebug($"Id: {message.RootAppeal.Id}");
                appeal = await GetAppealById(message.RootAppeal.Id);
            }
            else if (!string.IsNullOrEmpty(message.RootAppeal.OldSystemId))
            {
                _logger.LogDebug($"Old system ID: {message.RootAppeal.OldSystemId}");
                appeal = await GetAppealByOldSystemId(message.RootAppeal.OldSystemId);
            }
            
            if (appeal == null && !string.IsNullOrEmpty(message.RootAppeal.ExtId))
            {
                _logger.LogDebug($"Ext ID: {message.RootAppeal.ExtId}");
                appeal = await GetAppeal(message.RootAppeal.ExtId);
            }
            _logger.LogDebug($"Apepal == null : {appeal == null}");
            _logger.LogDebug($"Description: {appeal?.Description}");
            _logger.LogDebug($"Description text: {(appeal == null ? message.RootAppeal.Description : appeal.Description)}");
            message.Text = message.Text.Replace("%Description%", appeal == null ? message.RootAppeal.Description : appeal.Description);
            SetHeader();
            var request = new GraphQLRequest
            {
                Query = sendMessageMutation,
                Variables = new {
                    emailTo = message.RootAppeal.Author.Email,
                    nameTo = message.RootAppeal.Author.FullName,
                    subject = "Электронная приемная граждан",
                    message = message.Text,
                    files = message.AttachedFiles.Select(f => f.Id).ToArray(),
                    withDeliveryNotice = true,
                    duplicateToSender = true
                }
            };
            _logger.LogDebug($"Sending mutation createEmail");
            var response = await _graphQLClient.SendMutationAsync<Mutation>(request);
            
            _logger.LogDebug($"Notification was sent with result: {response.Data}");
            ProcessErrors<Mutation>(request, response);
            if (response.Errors != null && response.Errors.Any())
            {
                _logger.LogError($"Errors: {JsonConvert.SerializeObject(response.Errors)}");
                throw new Exception(response.Errors[0].Message);
            }

            return Ok(response.Data);
        }

    }
}
