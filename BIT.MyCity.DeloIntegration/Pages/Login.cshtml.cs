using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BIT.MyCity.DeloIntegration.Pages
{
    public class LoginModel : PageModel
    {
        public string Username { get; set; }
        public string Password { get; set; }

        // public async Task<IActionResult> OnPostAsync()
        // {
        //     if (!ModelState.IsValid)
        //     {
        //         return Page();
        //     }

        //     return Redirect("/api/delo/auth");
        //     //return RedirectToPage("./Index");
        // }
    }
}