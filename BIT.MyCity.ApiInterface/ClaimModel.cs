namespace BIT.MyCity.ApiInterface
{
  public class ClaimModel
  {
    public string Name { get; set; }

    public string Value { get; set; }
  }
}
