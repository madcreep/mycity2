using System;
using System.Collections.Generic;

namespace BIT.MyCity.ApiInterface
{
  public class RoleModel
  {
    public long? Id { get; set; }

    public string Name { get; set; }

    public string Description { get; set; }

    public IEnumerable<ClaimModel> Claims { get; set; }

    public bool Editable { get; set; }

    public bool IsAdminRegister { get; set; }

    public DateTime? CreatedAt { get; set; }

    public DateTime? UpdatedAt { get; set; }

    public DateTime? DisconnectedTimestamp { get; set; }

    public bool? Disconnected { get; set; }
  }
}
