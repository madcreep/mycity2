using System;
using System.Collections.Generic;
using System.Linq;

namespace BIT.MyCity.ApiInterface
{
    public class ClaimInfoModel
    {
        public string ClaimType { get; set; }

        public List<ClaimValueModel> Values { get; }

        public string Description { get; }

        public ClaimEntity[] ClaimEntity { get; }

        public ClaimInfoModel(string claimType, IEnumerable<ClaimValueModel> values, string description, params ClaimEntity[] claimEntity)
        {
            if (string.IsNullOrWhiteSpace(claimType)
                || string.IsNullOrWhiteSpace(description))
            {
                throw new ArgumentException();
            }

            ClaimType = claimType.Trim();
            Values = values?.ToList() ?? new List<ClaimValueModel>();
            Description = description.Trim();
            ClaimEntity = claimEntity ?? new ClaimEntity[0];
        }
    }

    public class ClaimValueModel
    {
        public ClaimValueModel(int weight, string value, string description)
        {
            if (string.IsNullOrWhiteSpace(value)
                || string.IsNullOrWhiteSpace(description))
            {
                throw new ArgumentException();
            }

            Value = value.Trim();
            Description = description.Trim();
            Weight = weight;
        }

        public string Value { get; }

        public string Description { get; }

        public int Weight { get; set; }
    }
}
