using System.Text.Json.Serialization;

namespace BIT.MyCity.ApiInterface.Models
{
  public class OperationResult
  {
    [JsonPropertyName("success")]
    public bool Success { get; set; }

    [JsonPropertyName("errorMessage")]
    public string[] ErrorMessages { get; set; }

    protected OperationResult(bool success, params string[] errorMessages)
    {
        Success = success;
        ErrorMessages = errorMessages;
    }
        
    public OperationResult(params string[] errorMessages)
      : this(false, errorMessages)
    { }

    public OperationResult()
      : this(true, null)
    { }
  }

  public sealed class OperationResult<T> : OperationResult where T : class
     
  {
    [JsonPropertyName("data")]
    public T Data { get; set; }

    private OperationResult(bool success, T data, params string[] errorMessages)
      : base(success, errorMessages)
    {
      Data = data;
    }

    public OperationResult()
      : this(true, null, null)
    { }

    public OperationResult(params string[] errorMessages)
    :this(false, null, errorMessages)
    { }

    public OperationResult(T data, params string[] errorMessages)
      : this(false, data, errorMessages)
    { }

    public OperationResult(T data)
      : this(true, data, null)
    { }
  }
}
