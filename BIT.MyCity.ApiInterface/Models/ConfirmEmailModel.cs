using System.ComponentModel.DataAnnotations;

namespace BIT.MyCity.ApiInterface.Models
{
    public class ConfirmEmailModel
    {
        [Required]
        public long UserId { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public int Sh { get; set; }
    }
}
