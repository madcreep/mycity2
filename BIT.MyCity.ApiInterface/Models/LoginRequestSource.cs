namespace BIT.MyCity.ApiInterface.Models
{
  public enum LoginRequestSource
  {
    Undefined,

    Site,

    Mobile
  }
}
