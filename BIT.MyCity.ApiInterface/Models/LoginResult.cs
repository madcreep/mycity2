using System;

namespace BIT.MyCity.ApiInterface.Models
{
    public class LoginResult
    {
        public string Key { get; set; }

        public string Token { get; set; }

        public LoginStatus LoginStatus { get; set; }

        public string ErrorMessage { get; set; }
    }
}
