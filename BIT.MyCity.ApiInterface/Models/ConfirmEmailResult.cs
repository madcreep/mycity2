namespace BIT.MyCity.ApiInterface.Models
{
    public class ConfirmEmailResult
    {
        public bool Succsess { get; set; }

        public string ErrorMessage { get; set; }

        public ConfirmEmailResult()
        : this(null)
        { }

        public ConfirmEmailResult(string errorMessage)
        {
            Succsess = string.IsNullOrWhiteSpace(errorMessage);

            ErrorMessage = errorMessage;
        }
    }
}
