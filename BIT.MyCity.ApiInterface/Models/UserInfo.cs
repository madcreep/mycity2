using System;
using System.Collections.Generic;
using System.Text;

namespace BIT.MyCity.ApiInterface.Models
{
  public class UserInfo
  {
    public string UserName { get; set; }

    public LoginSystem LoginSystem { get; set; }
  }
}
