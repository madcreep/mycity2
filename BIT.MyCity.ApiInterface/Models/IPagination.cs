namespace BIT.MyCity.ApiInterface.Models
{
    public interface IPagination
    {
        int? Skip { get; set; }

        int? Take { get; set; }

        OrderByItem[] OrderBy { get; set; }
    }
}
