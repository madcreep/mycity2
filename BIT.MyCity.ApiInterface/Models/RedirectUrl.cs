namespace BIT.MyCity.ApiInterface.Models
{
    public class RedirectUrl
    {
        public LoginSystem LoginSystem { get; set; }

        public string Url { get; set; }
    }
}
