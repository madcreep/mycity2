namespace BIT.MyCity.ApiInterface.Models
{
    public enum SearchDirection
    {
        Asc = 0,
        Desc = 1
    }
}
