using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace BIT.MyCity.ApiInterface.Models
{
    public class UserFilterModel : IPagination
    {
        public IEnumerable<long> Id { get; set; }

        public string UserName { get; set; }

        public IEnumerable<LoginSystem> LoginSystems { get; set; }

        public string FullName { get; set; }

        public DateTime? CreatedAtFrom { get; set; }

        public DateTime? CreatedAtTo { get; set; }

        public DateTime? UpdatedAtFrom { get; set; }

        public DateTime? UpdatedAtTo { get; set; }

        public bool? Disconnected { get; set; }

        public string Email { get; set; }

        public bool? EmailConfirmed { get; set; }

        public IEnumerable<long> RoleIds { get; set; }

        public IEnumerable<IdentityUserClaim<long>> Claims { get; set; }

        public int? Skip { get; set; }

        public int? Take { get; set; }

        public OrderByItem[] OrderBy { get; set; }
    }
}
