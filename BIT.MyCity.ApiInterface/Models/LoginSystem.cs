using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace BIT.MyCity.ApiInterface.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum LoginSystem
    {
        /// <summary>
        /// Формы
        /// </summary>
        [EnumMember(Value = "FORM")]
        Form = 0,
        /// <summary>
        /// Гос услуги
        /// </summary>
        [EnumMember(Value = "ESIA")]
        Esia = 1,
        /// <summary>
        /// В контакте
        /// </summary>
        [EnumMember(Value = "VK")]
        Vk = 2,
        /// <summary>
        /// Google
        /// </summary>
        [EnumMember(Value = "GOOGLE")]
        Google = 3,
        /// <summary>
        /// Facebook
        /// </summary>
        [EnumMember(Value = "FACEBOOK")]
        Facebook = 4
    }
}
