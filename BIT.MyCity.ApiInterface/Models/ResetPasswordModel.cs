using System;
using System.Collections.Generic;
using System.Text;

namespace BIT.MyCity.ApiInterface.Models
{
  public class ResetPasswordModel
  {
    public long? Id { get; set; }

    public string OldPassword { get; set; }

    public string NewPassword { get; set; }

    public string ConfirmPassword { get; set; }
  }
}
