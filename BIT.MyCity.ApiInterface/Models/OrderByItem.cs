using Microsoft.VisualBasic.FileIO;

namespace BIT.MyCity.ApiInterface.Models
{
    public class OrderByItem
    {
        public string Field { get; set; }

        public SearchDirection Direction { get; set; }
    }
}
