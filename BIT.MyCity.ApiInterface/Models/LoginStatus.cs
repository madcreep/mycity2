namespace BIT.MyCity.ApiInterface.Models
{
  public enum LoginStatus
  {
    Ok = 0,
    NotFound = 1,
    Unauthorized = 2,
    InSubscribe = 3
  }
}
