namespace BIT.MyCity.ApiInterface.Models
{
    public class LoginModel
    {
        public string SubscribeKey { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public LoginSystem LoginSystem { get; set; }

        public string Code { get; set; }

        public string CallbackUrl { get; set; }

        public LoginRequestSource LoginRequestSource { get; set; }

        public string RequestCodeError { get; set; }

        public bool RememberMe { get; set; }
    }
}
