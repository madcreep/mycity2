#!/bin/bash

#brew install markdown htmldoc
#markdown <file.md> | htmldoc --cont --headfootsize 8.0 --linkcolor blue --linkstyle plain --format pdf14 - > <file.pdf>
#https://gist.github.com/davisford/01b4eea0f1ddfb858d89

if (( $# != 2 )) 
then 
  echo "Usage: md2pdf <input.md> <output.pdf>"
  exit 1
fi
markdown $1 | htmldoc --cont --headfootsize 8.0 --linkcolor blue --linkstyle plain --format pdf14 - > $2