# Описание API

<details>
  <summary><strong>Table of Contents</strong></summary>

  * [Query](#query)
  * [Mutation](#mutation)
  * [Objects](#objects)
    * [AddressType](#addresstype)
    * [ApiInformation](#apiinformation)
    * [Appeal](#appeal)
    * [AppealExecutor](#appealexecutor)
    * [AppealExecutorPageType](#appealexecutorpagetype)
    * [AppealPageType](#appealpagetype)
    * [AppealStats](#appealstats)
    * [AppealStatsPageType](#appealstatspagetype)
    * [AppealsStatBy](#appealsstatby)
    * [AppealsStatByPageType](#appealsstatbypagetype)
    * [Appointment](#appointment)
    * [AppointmentNotification](#appointmentnotification)
    * [AppointmentNotificationPageType](#appointmentnotificationpagetype)
    * [AppointmentPageType](#appointmentpagetype)
    * [AppointmentRange](#appointmentrange)
    * [AppointmentRangePageType](#appointmentrangepagetype)
    * [AttachedFile](#attachedfile)
    * [AttachedFilePageType](#attachedfilepagetype)
    * [Balance](#balance)
    * [ClaimInfoType](#claiminfotype)
    * [ClaimInfoValueType](#claiminfovaluetype)
    * [ClaimType](#claimtype)
    * [EmailTemplatePageType](#emailtemplatepagetype)
    * [EmailTemplateType](#emailtemplatetype)
    * [EnumVerb](#enumverb)
    * [EnumVerbPageType](#enumverbpagetype)
    * [FileDownload](#filedownload)
    * [FileUploadResult](#fileuploadresult)
    * [LikeReport](#likereport)
    * [LikeWIL](#likewil)
    * [LoginResultType](#loginresulttype)
    * [Message](#message)
    * [MessagePageType](#messagepagetype)
    * [MessageSubscribeGet](#messagesubscribeget)
    * [MessageSubscribeGet_](#messagesubscribeget_)
    * [News](#news)
    * [NewsPageType](#newspagetype)
    * [Officer](#officer)
    * [OfficerPageType](#officerpagetype)
    * [OperationResultType](#operationresulttype)
    * [OrganizationType](#organizationtype)
    * [PToken](#ptoken)
    * [PTokenPageType](#ptokenpagetype)
    * [PhoneType](#phonetype)
    * [ProtocolRecord](#protocolrecord)
    * [ProtocolRecordPageType](#protocolrecordpagetype)
    * [RedirectUrl](#redirecturl)
    * [RegardReport](#regardreport)
    * [RegardWIR](#regardwir)
    * [Region](#region)
    * [RegionPageType](#regionpagetype)
    * [RolePageType](#rolepagetype)
    * [RoleSortedType](#rolesortedtype)
    * [RoleType](#roletype)
    * [Rubric](#rubric)
    * [RubricPageType](#rubricpagetype)
    * [Settings](#settings)
    * [Subs2DueMessagesPageType](#subs2duemessagespagetype)
    * [Subscriptions](#subscriptions)
    * [Subsystem](#subsystem)
    * [SubsystemMainEntityField](#subsystemmainentityfield)
    * [SubsystemPageType](#subsystempagetype)
    * [Territory](#territory)
    * [TerritoryPageType](#territorypagetype)
    * [Topic](#topic)
    * [TopicPageType](#topicpagetype)
    * [User](#user)
    * [UserIdentityDocumentsType](#useridentitydocumentstype)
    * [UserOfSubsystem](#userofsubsystem)
    * [UserOrganizationPageType](#userorganizationpagetype)
    * [UserPageType](#userpagetype)
    * [ViewReport](#viewreport)
    * [ViewWIV](#viewwiv)
    * [Vote](#vote)
    * [VoteItem](#voteitem)
    * [VoteItemPageType](#voteitempagetype)
    * [VoteOffer](#voteoffer)
    * [VotePageType](#votepagetype)
    * [VoteRoot](#voteroot)
    * [VoteRootPageType](#voterootpagetype)
    * [VoteWIV](#votewiv)
    * [createSettingsValueItem](#createsettingsvalueitem)
  * [Inputs](#inputs)
    * [ClaimInputType](#claiminputtype)
    * [ConfirmEmailType](#confirmemailtype)
    * [EntitySelector](#entityselector)
    * [FileDownloadRequest](#filedownloadrequest)
    * [FileUpload](#fileupload)
    * [Filter](#filter)
    * [ForgotPasswordType](#forgotpasswordtype)
    * [LikeRequest](#likerequest)
    * [LoginType](#logintype)
    * [MessageSubscribeSet](#messagesubscribeset)
    * [OrderByItemType](#orderbyitemtype)
    * [PhoneInputType](#phoneinputtype)
    * [QueryOptions](#queryoptions)
    * [Range](#range)
    * [RangeSearch](#rangesearch)
    * [RedirectInputUrl](#redirectinputurl)
    * [RegardRequest](#regardrequest)
    * [RegisterModelType](#registermodeltype)
    * [ResetPasswordInputType](#resetpasswordinputtype)
    * [RestorePasswordType](#restorepasswordtype)
    * [RoleInputType](#roleinputtype)
    * [RoleSortingType](#rolesortingtype)
    * [UserInputType](#userinputtype)
    * [ViewRequest](#viewrequest)
    * [VoteAction](#voteaction)
    * [approveAppeal](#approveappeal)
    * [approveAttachedFile](#approveattachedfile)
    * [approveMessage](#approvemessage)
    * [approveNews](#approvenews)
    * [createAppeal](#createappeal)
    * [createAppealExecutor](#createappealexecutor)
    * [createAppointment](#createappointment)
    * [createAppointmentNotification](#createappointmentnotification)
    * [createAppointmentRange](#createappointmentrange)
    * [createAttachedFile](#createattachedfile)
    * [createEnumVerb](#createenumverb)
    * [createFBAppeal](#createfbappeal)
    * [createMessage](#createmessage)
    * [createNews](#createnews)
    * [createOfficer](#createofficer)
    * [createPToken](#createptoken)
    * [createRegion](#createregion)
    * [createRubric](#createrubric)
    * [createSubsystem](#createsubsystem)
    * [createSubsystemMainEntityField](#createsubsystemmainentityfield)
    * [createTerritory](#createterritory)
    * [createTopic](#createtopic)
    * [createVote](#createvote)
    * [createVoteItem](#createvoteitem)
    * [createVoteRoot](#createvoteroot)
    * [deleteMessage](#deletemessage)
    * [deletePToken](#deleteptoken)
    * [updateAppeal](#updateappeal)
    * [updateAppealExecutor](#updateappealexecutor)
    * [updateAppointment](#updateappointment)
    * [updateAppointmentNotification](#updateappointmentnotification)
    * [updateAppointmentRange](#updateappointmentrange)
    * [updateAttachedFile](#updateattachedfile)
    * [updateEnumVerb](#updateenumverb)
    * [updateMessage](#updatemessage)
    * [updateNews](#updatenews)
    * [updateOfficer](#updateofficer)
    * [updateRegion](#updateregion)
    * [updateRubric](#updaterubric)
    * [updateSettings](#updatesettings)
    * [updateSubsystem](#updatesubsystem)
    * [updateTerritory](#updateterritory)
    * [updateTopic](#updatetopic)
    * [updateVote](#updatevote)
    * [updateVoteItem](#updatevoteitem)
    * [updateVoteRoot](#updatevoteroot)
  * [Enums](#enums)
    * [AddrType](#addrtype)
    * [AgencyType](#agencytype)
    * [AppealTypeEnum](#appealtypeenum)
    * [ApprovedEnum](#approvedenum)
    * [ClaimEntity](#claimentity)
    * [DocType](#doctype)
    * [FilterNextOpEnum](#filternextopenum)
    * [Gender](#gender)
    * [LikeRequestEnum](#likerequestenum)
    * [LikeStateEnum](#likestateenum)
    * [LoginRequestSource](#loginrequestsource)
    * [LoginSystem](#loginsystem)
    * [MessageKindEnum](#messagekindenum)
    * [ModeEnum](#modeenum)
    * [ModerationStageEnum](#moderationstageenum)
    * [NewsStateEnum](#newsstateenum)
    * [RegardModeEnum](#regardmodeenum)
    * [RightLevel](#rightlevel)
    * [SearchDirection](#searchdirection)
    * [SettingNodeType](#settingnodetype)
    * [SettingType](#settingtype)
    * [SkEncodedImageFormat](#skencodedimageformat)
    * [StatusEnum](#statusenum)
    * [SubscribeToAnswersEnum](#subscribetoanswersenum)
    * [TypePhone](#typephone)
  * [Scalars](#scalars)
    * [BigInt](#bigint)
    * [Boolean](#boolean)
    * [Byte](#byte)
    * [Date](#date)
    * [DateTime](#datetime)
    * [DateTimeOffset](#datetimeoffset)
    * [Decimal](#decimal)
    * [Float](#float)
    * [Guid](#guid)
    * [ID](#id)
    * [Int](#int)
    * [Long](#long)
    * [Milliseconds](#milliseconds)
    * [SByte](#sbyte)
    * [Seconds](#seconds)
    * [Short](#short)
    * [String](#string)
    * [UInt](#uint)
    * [ULong](#ulong)
    * [UShort](#ushort)
    * [Uri](#uri)

</details>

## Query
<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>apiInformation</strong></td>
<td valign="top"><a href="#apiinformation">ApiInformation</a></td>
<td>

Системная информация серверной части. Версии.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appeal</strong></td>
<td valign="top"><a href="#appealpagetype">AppealPageType</a></td>
<td>

Получение списка обращений

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutor</strong></td>
<td valign="top"><a href="#appealexecutorpagetype">AppealExecutorPageType</a></td>
<td>

Получение списка исполнителей

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealFB</strong></td>
<td valign="top"><a href="#appealpagetype">AppealPageType</a></td>
<td>

Получение списка сообщений обратной связи

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">email</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Email

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealMessageList</strong> ⚠️</td>
<td valign="top"><a href="#messagepagetype">MessagePageType</a></td>
<td>

Получение комментариев к обращению в виде массива

<p>⚠️ <strong>DEPRECATED</strong></p>
<blockquote>

Используйте обобщенный метод messageList

</blockquote>
</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Id обращения (Appeal)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealsStatBy</strong></td>
<td valign="top"><a href="#appealsstatbypagetype">AppealsStatByPageType</a></td>
<td>

Получение статистики по рубрикаторам и датам

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">offsetHours</td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Сдвиг временных меток

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealStats</strong></td>
<td valign="top"><a href="#appealstatspagetype">AppealStatsPageType</a></td>
<td>

Получение статистики

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealWhereExecutor</strong></td>
<td valign="top"><a href="#appealpagetype">AppealPageType</a></td>
<td>

Получение списка обращений с указанным исполнителем

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a></td>
<td>

ID исполнителя

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">subsystemUIDs</td>
<td valign="top">[<a href="#string">String</a>]</td>
<td>

Подсистемы

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealWhereModerator</strong></td>
<td valign="top"><a href="#appealpagetype">AppealPageType</a></td>
<td>

Получение списка обращений с указанным модератором

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a></td>
<td>

ID модератора

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">subsystemUIDs</td>
<td valign="top">[<a href="#string">String</a>]</td>
<td>

Подсистемы

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointment</strong></td>
<td valign="top"><a href="#appointmentpagetype">AppointmentPageType</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointmentNotification</strong></td>
<td valign="top"><a href="#appointmentnotificationpagetype">AppointmentNotificationPageType</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointmentRange</strong></td>
<td valign="top"><a href="#appointmentrangepagetype">AppointmentRangePageType</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFile</strong></td>
<td valign="top"><a href="#attachedfilepagetype">AttachedFilePageType</a></td>
<td>

Получение списка присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>availableExecutors</strong></td>
<td valign="top"><a href="#userpagetype">UserPageType</a></td>
<td>

Получение списка исполнителей по подсистемам (Policy = ManageUsersOrModerate)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Сортировка и пагинация

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">subsystemUid</td>
<td valign="top">[<a href="#string">String</a>]</td>
<td>

UID подсистем

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>availableModerators</strong></td>
<td valign="top"><a href="#userpagetype">UserPageType</a></td>
<td>

Получение списка модераторов по подсистемам (Policy = ManageUsersOrModerate)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Сортировка и пагинация

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">subsystemUid</td>
<td valign="top">[<a href="#string">String</a>]</td>
<td>

UID подсистем

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>claimsInfo</strong></td>
<td valign="top">[<a href="#claiminfotype">ClaimInfoType</a>]</td>
<td>

Получение информации о доступных разрешениях (Policy = ManageUsersOrRoles)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmEmail</strong></td>
<td valign="top"><a href="#operationresulttype">OperationResultType</a></td>
<td>

Подтверждение электронной почты

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">confirmEmail</td>
<td valign="top"><a href="#confirmemailtype">ConfirmEmailType</a>!</td>
<td>

Параметры подтверждения электронной почты

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmPhone</strong></td>
<td valign="top"><a href="#operationresulttype">OperationResultType</a></td>
<td>

Отправлка СМС подтверждения телефонного номера (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">phoneId</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Идентификатор номера телефона

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currentUser</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Получение текущего пользователя (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currentUserOrganization</strong></td>
<td valign="top"><a href="#userorganizationpagetype">UserOrganizationPageType</a></td>
<td>

Запрос организаций текущего пользователя (Policy = SubsystemOrganizations)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Сортировка и пагинация

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>emailTemplate</strong></td>
<td valign="top"><a href="#emailtemplatepagetype">EmailTemplatePageType</a></td>
<td>

Получение списка шаблонов Email (Policy = ManageSettings)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Сортировка и пагинация

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>enumVerb</strong></td>
<td valign="top"><a href="#enumverbpagetype">EnumVerbPageType</a></td>
<td>

Получение списка записей

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fileDownload</strong></td>
<td valign="top"><a href="#filedownload">FileDownload</a></td>
<td>

Скачивание файла с сервера

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">file</td>
<td valign="top"><a href="#filedownloadrequest">FileDownloadRequest</a>!</td>
<td>

Запрос на скачивание

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ftsAppeal</strong></td>
<td valign="top"><a href="#appealpagetype">AppealPageType</a></td>
<td>

Поиск обращений

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#rangesearch">RangeSearch</a></td>
<td>

Пагинация

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">query</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Поисковый запрос

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>login</strong></td>
<td valign="top"><a href="#loginresulttype">LoginResultType</a></td>
<td>

Получение токена авторизации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">login</td>
<td valign="top"><a href="#logintype">LoginType</a>!</td>
<td>

Параметры авторизации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>loginModes</strong></td>
<td valign="top">[<a href="#loginsystem">LoginSystem</a>]</td>
<td>

Запрос мнемоник доступных внешних систем авторизации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>loginRedirectUrl</strong></td>
<td valign="top"><a href="#redirecturl">RedirectUrl</a></td>
<td>

Запрос адреса страницы авторизации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">redirectUrl</td>
<td valign="top"><a href="#redirectinputurl">RedirectInputUrl</a>!</td>
<td>

Адрес перернаправления

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>logoutRedirectUrl</strong></td>
<td valign="top"><a href="#redirecturl">RedirectUrl</a></td>
<td>

Запрос адреса страницы авторизации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">redirectUrl</td>
<td valign="top"><a href="#redirectinputurl">RedirectInputUrl</a>!</td>
<td>

Адрес перернаправления

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>message</strong></td>
<td valign="top"><a href="#messagepagetype">MessagePageType</a></td>
<td>

Получение списка сообщений/комментариев

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>messageList</strong></td>
<td valign="top"><a href="#messagepagetype">MessagePageType</a></td>
<td>

Получение выбранного сообщения и ответов на него в виде массива

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Id объекта

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">type</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Тип объекта

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>messageSubscribes</strong></td>
<td valign="top"><a href="#subs2duemessagespagetype">Subs2DueMessagesPageType</a></td>
<td>

Получение всех подписок на комментарии пользователя

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>news</strong></td>
<td valign="top"><a href="#newspagetype">NewsPageType</a></td>
<td>

Получение списка новостей

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officer</strong></td>
<td valign="top"><a href="#officerpagetype">OfficerPageType</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>protocolRecord</strong></td>
<td valign="top"><a href="#protocolrecordpagetype">ProtocolRecordPageType</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pToken</strong></td>
<td valign="top"><a href="#ptokenpagetype">PTokenPageType</a></td>
<td>

Получение списка токенов (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">userId</td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id пользователя

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#regionpagetype">RegionPageType</a></td>
<td>

получение списка Регионов

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>restorePassword</strong></td>
<td valign="top"><a href="#operationresulttype">OperationResultType</a></td>
<td>

Восстановление забытого пароля

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">restorePassword</td>
<td valign="top"><a href="#forgotpasswordtype">ForgotPasswordType</a>!</td>
<td>

Параметры восстановления пароля для пользователей зарегистрированных через формы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>role</strong></td>
<td valign="top"><a href="#rolepagetype">RolePageType</a></td>
<td>

Запрос ролей (Policy = ManageUsersOrRoles)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Сортировка и пагинация

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubric</strong></td>
<td valign="top"><a href="#rubricpagetype">RubricPageType</a></td>
<td>

получение списка Рубрик

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>searchAppeal</strong></td>
<td valign="top"><a href="#appealpagetype">AppealPageType</a></td>
<td>

Поиск обращений

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#rangesearch">RangeSearch</a></td>
<td>

Пагинация

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">query</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Поисковый запрос

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>searchMessage</strong></td>
<td valign="top"><a href="#messagepagetype">MessagePageType</a></td>
<td>

Поиск сообщений/комментариев

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#rangesearch">RangeSearch</a></td>
<td>

Пагинация

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">query</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Поисковый запрос

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>searchNews</strong></td>
<td valign="top"><a href="#newspagetype">NewsPageType</a></td>
<td>

Поиск новостей

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#rangesearch">RangeSearch</a></td>
<td>

Пагинация

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">query</td>
<td valign="top"><a href="#string">String</a></td>
<td>

Поисковый запрос

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sendConfirmEmail</strong></td>
<td valign="top"><a href="#operationresulttype">OperationResultType</a></td>
<td>

Послать электронное письмо подтверждения Email (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>settings</strong></td>
<td valign="top">[<a href="#settings">Settings</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">key</td>
<td valign="top">[<a href="#string">String</a>]</td>
<td>

Запрашиваемые ключи настроек

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>smsBalance</strong></td>
<td valign="top"><a href="#balance">Balance</a></td>
<td>

Запрос баланса СМС сервиса (Policy = ManageSettings)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystem</strong></td>
<td valign="top"><a href="#subsystempagetype">SubsystemPageType</a></td>
<td>

Получение списка и параметров подсистем

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territory</strong></td>
<td valign="top"><a href="#territorypagetype">TerritoryPageType</a></td>
<td>

получение списка Территорий

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topic</strong></td>
<td valign="top"><a href="#topicpagetype">TopicPageType</a></td>
<td>

получение списка Топиков

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateToken</strong></td>
<td valign="top"><a href="#loginresulttype">LoginResultType</a></td>
<td>

Обновление токена авторизации (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#userpagetype">UserPageType</a></td>
<td>

Получение списка пользователей (Policy = ManageUsersOrModerate)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Сортировка и пагинация

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userOrganization</strong></td>
<td valign="top"><a href="#userorganizationpagetype">UserOrganizationPageType</a></td>
<td>

Запрос организаций текущего пользователя (Policy = ManageUsersOrModerateOrganizations)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Сортировка и пагинация

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vote</strong></td>
<td valign="top"><a href="#votepagetype">VotePageType</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItem</strong></td>
<td valign="top"><a href="#voteitempagetype">VoteItemPageType</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteMessageList</strong> ⚠️</td>
<td valign="top"><a href="#messagepagetype">MessagePageType</a></td>
<td>

Получение комментариев к голосованию в виде массива

<p>⚠️ <strong>DEPRECATED</strong></p>
<blockquote>

Используйте обобщенный метод messageList

</blockquote>
</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Id голосования (Vote)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteRoot</strong></td>
<td valign="top"><a href="#voterootpagetype">VoteRootPageType</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">options</td>
<td valign="top"><a href="#queryoptions">QueryOptions</a></td>
<td>

Параметры

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">range</td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Объект для сортировки и пагинации

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">filter</td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
</tbody>
</table>

## Mutation
<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>approveAppeal</strong></td>
<td valign="top"><a href="#appeal">Appeal</a></td>
<td>

Принятие к публикации или отклонение обращения (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Id обращения

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appealApprove</td>
<td valign="top"><a href="#approveappeal">approveAppeal</a>!</td>
<td>

Поля обращения для изменения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>approveAttachedFile</strong></td>
<td valign="top"><a href="#attachedfile">AttachedFile</a></td>
<td>

Публикация файла (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Id присоединенного файла

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">attachedFileApprove</td>
<td valign="top"><a href="#approveattachedfile">approveAttachedFile</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>approveMessage</strong></td>
<td valign="top"><a href="#messagepagetype">MessagePageType</a></td>
<td>

Принятие к публикации или отклонение комментария (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Id комментария

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">messageApprove</td>
<td valign="top"><a href="#approvemessage">approveMessage</a>!</td>
<td>

Поля комментария для изменения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>approveNews</strong></td>
<td valign="top"><a href="#news">News</a></td>
<td>

Принятие к публикации или отклонение новости (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Id новости

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">newsApprove</td>
<td valign="top"><a href="#approvenews">approveNews</a>!</td>
<td>

Поля новости для изменения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmPhone</strong></td>
<td valign="top"><a href="#operationresulttype">OperationResultType</a></td>
<td>

Отправлка СМС подтверждения телефонного номера (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">phoneId</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Идентификатор номера телефона

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">code</td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Код подтверждения номера телефона

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createAppeal</strong></td>
<td valign="top"><a href="#appeal">Appeal</a></td>
<td>

Создание нового обращения

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appeal</td>
<td valign="top"><a href="#createappeal">createAppeal</a>!</td>
<td>

Обращение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createAppealExecutor</strong></td>
<td valign="top"><a href="#appealexecutor">AppealExecutor</a></td>
<td>

Создание нового исполнителя (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appealExecutor</td>
<td valign="top"><a href="#createappealexecutor">createAppealExecutor</a>!</td>
<td>

Исполнитель

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createAppointment</strong></td>
<td valign="top"><a href="#appointment">Appointment</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appointment</td>
<td valign="top"><a href="#createappointment">createAppointment</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createAppointmentNotification</strong></td>
<td valign="top"><a href="#appointmentnotification">AppointmentNotification</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appointmentNotification</td>
<td valign="top"><a href="#createappointmentnotification">createAppointmentNotification</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createAppointmentRange</strong></td>
<td valign="top"><a href="#appointmentrange">AppointmentRange</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appointmentRange</td>
<td valign="top"><a href="#createappointmentrange">createAppointmentRange</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createAttachedFile</strong></td>
<td valign="top"><a href="#attachedfile">AttachedFile</a></td>
<td>

Создание файла (ссылки)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">attachedFile</td>
<td valign="top"><a href="#createattachedfile">createAttachedFile</a>!</td>
<td>

Присоединенный файл

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createEnumVerb</strong></td>
<td valign="top"><a href="#enumverb">EnumVerb</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">enumVerb</td>
<td valign="top"><a href="#createenumverb">createEnumVerb</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createFBAppeal</strong></td>
<td valign="top"><a href="#appeal">Appeal</a></td>
<td>

Создание нового сообщения обратной связи

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appeal</td>
<td valign="top"><a href="#createfbappeal">createFBAppeal</a>!</td>
<td>

Сообщение обратной связи

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createMessage</strong></td>
<td valign="top"><a href="#message">Message</a></td>
<td>

Создать новое сообщение/комментарий. Сообщение должно быть привязано
(parentId/parentType) к обращению (Appeal), сообщению (Message), новости
(News) либо к голосованию (Vote) (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">message</td>
<td valign="top"><a href="#createmessage">createMessage</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createNews</strong></td>
<td valign="top"><a href="#news">News</a></td>
<td>

Создание новой новости (Policy = CreateNews)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">news</td>
<td valign="top"><a href="#createnews">createNews</a>!</td>
<td>

Новость

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createOfficer</strong></td>
<td valign="top"><a href="#officer">Officer</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">officer</td>
<td valign="top"><a href="#createofficer">createOfficer</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createPToken</strong></td>
<td valign="top"><a href="#ptoken">PToken</a></td>
<td>

Регистрация нового токена (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">pToken</td>
<td valign="top"><a href="#createptoken">createPToken</a>!</td>
<td>

Токен

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createRegion</strong></td>
<td valign="top"><a href="#region">Region</a></td>
<td>

создание нового Региона (Policy = ManageRegionsWithSubsystem)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">region</td>
<td valign="top"><a href="#createregion">createRegion</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createRole</strong></td>
<td valign="top"><a href="#roletype">RoleType</a></td>
<td>

Создание роли (Policy = ManageRoles)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">role</td>
<td valign="top"><a href="#roleinputtype">RoleInputType</a>!</td>
<td>

Параметры создаваемой роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createRubric</strong></td>
<td valign="top"><a href="#rubric">Rubric</a></td>
<td>

создание новой Рубрики (Policy = ManageRubricsWithSubsystem)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">rubric</td>
<td valign="top"><a href="#createrubric">createRubric</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createSubsystem</strong></td>
<td valign="top"><a href="#subsystem">Subsystem</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">subsystem</td>
<td valign="top"><a href="#createsubsystem">createSubsystem</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createTerritory</strong></td>
<td valign="top"><a href="#territory">Territory</a></td>
<td>

создание новой Территории (Policy = ManageTerritoriesWithSubsystem)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">territory</td>
<td valign="top"><a href="#createterritory">createTerritory</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createTopic</strong></td>
<td valign="top"><a href="#topic">Topic</a></td>
<td>

создание нового Топика (Policy = ManageTopicsWithSubsystem)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">topic</td>
<td valign="top"><a href="#createtopic">createTopic</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createUser</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Создание пользователя (Policy = Manage)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">user</td>
<td valign="top"><a href="#registermodeltype">RegisterModelType</a>!</td>
<td>

Параметры создаваемого пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createVote</strong></td>
<td valign="top"><a href="#vote">Vote</a></td>
<td>

 (Policy = CreateVoting)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">vote</td>
<td valign="top"><a href="#createvote">createVote</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createVoteItem</strong></td>
<td valign="top"><a href="#voteitem">VoteItem</a></td>
<td>

 (Policy = ModerateVoting)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">voteItem</td>
<td valign="top"><a href="#createvoteitem">createVoteItem</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createVoteRoot</strong></td>
<td valign="top"><a href="#voteroot">VoteRoot</a></td>
<td>

 (Policy = CreateVoting)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">voteRoot</td>
<td valign="top"><a href="#createvoteroot">createVoteRoot</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>delete</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">entity</td>
<td valign="top"><a href="#entityselector">EntitySelector</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleteMessageWithComment</strong></td>
<td valign="top"><a href="#message">Message</a></td>
<td>

Удаление сообщения с описанием причины (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Id комментария

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">messageDelete</td>
<td valign="top"><a href="#deletemessage">deleteMessage</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deletePToken</strong></td>
<td valign="top"><a href="#ptoken">PToken</a></td>
<td>

Удаление токена (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">userId</td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id пользователя

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">pToken</td>
<td valign="top"><a href="#deleteptoken">deletePToken</a>!</td>
<td>

Токен

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fileUpload</strong></td>
<td valign="top"><a href="#fileuploadresult">FileUploadResult</a></td>
<td>

Загрузка файла на сервер

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">file</td>
<td valign="top"><a href="#fileupload">FileUpload</a>!</td>
<td>

Передаваемый файл или его часть

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>like</strong></td>
<td valign="top"><a href="#likereport">LikeReport</a></td>
<td>

Поставить лайк (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">like</td>
<td valign="top"><a href="#likerequest">LikeRequest</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>likeAppeal</strong></td>
<td valign="top"><a href="#likereport">LikeReport</a></td>
<td>

Поставить лайк Обращению (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">like</td>
<td valign="top"><a href="#likerequest">LikeRequest</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>likeMessage</strong></td>
<td valign="top"><a href="#likereport">LikeReport</a></td>
<td>

Поставить лайк Сообщению (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">like</td>
<td valign="top"><a href="#likerequest">LikeRequest</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>messageSubscribe</strong></td>
<td valign="top"><a href="#messagesubscribeget">MessageSubscribeGet</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">subscribe</td>
<td valign="top"><a href="#messagesubscribeset">MessageSubscribeSet</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>messageUnSubscribe</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">unsubscribe_mask</td>
<td valign="top"><a href="#messagesubscribeset">MessageSubscribeSet</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regard</strong></td>
<td valign="top"><a href="#regardreport">RegardReport</a></td>
<td>

Поставить оценку (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">regard</td>
<td valign="top"><a href="#regardrequest">RegardRequest</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>registerUser</strong></td>
<td valign="top"><a href="#loginresulttype">LoginResultType</a></td>
<td>

Регистрация пользователя

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">user</td>
<td valign="top"><a href="#registermodeltype">RegisterModelType</a>!</td>
<td>

Параметры регистрируемого пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>removeRole</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удаление роли (Policy = ManageRoles)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Идентификатор роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>resetPassword</strong></td>
<td valign="top"><a href="#operationresulttype">OperationResultType</a></td>
<td>

Смена пароля (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">resetPassword</td>
<td valign="top"><a href="#resetpasswordinputtype">ResetPasswordInputType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>restorePassword</strong></td>
<td valign="top"><a href="#operationresulttype">OperationResultType</a></td>
<td>

Восстановление пароля

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">restorePassword</td>
<td valign="top"><a href="#restorepasswordtype">RestorePasswordType</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>setSettings</strong></td>
<td valign="top">[<a href="#settings">Settings</a>]</td>
<td>

 (Policy = ManageSettings)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">settings</td>
<td valign="top">[<a href="#updatesettings">updateSettings</a>]</td>
<td>

Установка настроек

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sortRole</strong></td>
<td valign="top">[<a href="#rolesortedtype">RoleSortedType</a>]</td>
<td>

Сортировка ролей (Policy = ManageRoles)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">role</td>
<td valign="top">[<a href="#rolesortingtype">RoleSortingType</a>]!</td>
<td>

Роли для сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateAppeal</strong></td>
<td valign="top"><a href="#appeal">Appeal</a></td>
<td>

Изменение существующего обращения (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Id обращения

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appeal</td>
<td valign="top"><a href="#updateappeal">updateAppeal</a>!</td>
<td>

Поля обращения для изменения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateAppealExecutor</strong></td>
<td valign="top"><a href="#appealexecutor">AppealExecutor</a></td>
<td>

Изменение существующего исполнителя (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Id исполнителя

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appealExecutor</td>
<td valign="top"><a href="#updateappealexecutor">updateAppealExecutor</a>!</td>
<td>

Поля исполнителя для изменения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateAppointment</strong></td>
<td valign="top"><a href="#appointment">Appointment</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appointment</td>
<td valign="top"><a href="#updateappointment">updateAppointment</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateAppointmentNotification</strong></td>
<td valign="top"><a href="#appointmentnotification">AppointmentNotification</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appointmentNotification</td>
<td valign="top"><a href="#updateappointmentnotification">updateAppointmentNotification</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateAppointmentRange</strong></td>
<td valign="top"><a href="#appointmentrange">AppointmentRange</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">appointmentRange</td>
<td valign="top"><a href="#updateappointmentrange">updateAppointmentRange</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateAttachedFile</strong></td>
<td valign="top"><a href="#attachedfile">AttachedFile</a></td>
<td>

Изменение ссылки на файл (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Id присоединенного файла

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">attachedFile</td>
<td valign="top"><a href="#updateattachedfile">updateAttachedFile</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateByIDEnumVerb</strong></td>
<td valign="top"><a href="#enumverb">EnumVerb</a></td>
<td>

Обновление по ID (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">enumVerb</td>
<td valign="top"><a href="#updateenumverb">updateEnumVerb</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateByKeyEnumVerb</strong></td>
<td valign="top"><a href="#enumverb">EnumVerb</a></td>
<td>

Обновление по тройке полей {value, group, subsystemUID} (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">value</td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">group</td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">subsystemUID</td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">enumVerb</td>
<td valign="top"><a href="#updateenumverb">updateEnumVerb</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateEnumVerb</strong></td>
<td valign="top"><a href="#enumverb">EnumVerb</a></td>
<td>

Если в теле объекта задан id, то объект ищется по id и остальные поля
редактируются. Если id не задан, то должна быть задана тройка полей {value,
group, subsystemUID} и по ней ищется объект.  (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">enumVerb</td>
<td valign="top"><a href="#updateenumverb">updateEnumVerb</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateMessage</strong></td>
<td valign="top"><a href="#message">Message</a></td>
<td>

Изменить существующее сообщение (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">message</td>
<td valign="top"><a href="#updatemessage">updateMessage</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateNews</strong></td>
<td valign="top"><a href="#news">News</a></td>
<td>

Изменение существующей новости (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Id новости

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">news</td>
<td valign="top"><a href="#updatenews">updateNews</a>!</td>
<td>

Поля новости для изменения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateOfficer</strong></td>
<td valign="top"><a href="#officer">Officer</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">officer</td>
<td valign="top"><a href="#updateofficer">updateOfficer</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateRegion</strong></td>
<td valign="top"><a href="#region">Region</a></td>
<td>

изменение существующего Региона (Policy = ManageRegionsWithSubsystem)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">region</td>
<td valign="top"><a href="#updateregion">updateRegion</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateRole</strong></td>
<td valign="top"><a href="#roletype">RoleType</a></td>
<td>

Редактирование роли (Policy = ManageRoles)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Идентификатор роли

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">role</td>
<td valign="top"><a href="#roleinputtype">RoleInputType</a>!</td>
<td>

Параметры редактирования роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateRubric</strong></td>
<td valign="top"><a href="#rubric">Rubric</a></td>
<td>

изменение существующей Рубрики (Policy = ManageRubricsWithSubsystem)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">rubric</td>
<td valign="top"><a href="#updaterubric">updateRubric</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateSubsystem</strong></td>
<td valign="top"><a href="#subsystem">Subsystem</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">uid</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">subsystem</td>
<td valign="top"><a href="#updatesubsystem">updateSubsystem</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateTerritory</strong></td>
<td valign="top"><a href="#territory">Territory</a></td>
<td>

изменение существующей Территории (Policy = ManageTerritoriesWithSubsystem)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">territory</td>
<td valign="top"><a href="#updateterritory">updateTerritory</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateTopic</strong></td>
<td valign="top"><a href="#topic">Topic</a></td>
<td>

изменение существующего Топика (Policy = ManageTopicsWithSubsystem)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">topic</td>
<td valign="top"><a href="#updatetopic">updateTopic</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateUser</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Редактирование пользователя (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор редактируемого пользователя

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">user</td>
<td valign="top"><a href="#userinputtype">UserInputType</a>!</td>
<td>

Редактируемые значения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateVote</strong></td>
<td valign="top"><a href="#vote">Vote</a></td>
<td>

 (Policy = ModerateVoting)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">vote</td>
<td valign="top"><a href="#updatevote">updateVote</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateVoteItem</strong></td>
<td valign="top"><a href="#voteitem">VoteItem</a></td>
<td>

 (Policy = ModerateVoting)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">voteItem</td>
<td valign="top"><a href="#updatevoteitem">updateVoteItem</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updateVoteRoot</strong></td>
<td valign="top"><a href="#voteroot">VoteRoot</a></td>
<td>

 (Policy = ModerateVoting)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">voteRoot</td>
<td valign="top"><a href="#updatevoteroot">updateVoteRoot</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>view</strong></td>
<td valign="top"><a href="#viewreport">ViewReport</a></td>
<td>

Увидеть/развидеть (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">view</td>
<td valign="top"><a href="#viewrequest">ViewRequest</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteAction</strong></td>
<td valign="top"><a href="#vote">Vote</a></td>
<td>

 (Policy = Vote)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">voteAction</td>
<td valign="top"><a href="#voteaction">VoteAction</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>votesAction</strong></td>
<td valign="top"><a href="#votepagetype">VotePageType</a></td>
<td>

 (Policy = Vote)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">voteAction</td>
<td valign="top">[<a href="#voteaction">VoteAction</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteStart</strong></td>
<td valign="top"><a href="#voteroot">VoteRoot</a></td>
<td>

 (Policy = ManageVoting)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteStop</strong></td>
<td valign="top"><a href="#voteroot">VoteRoot</a></td>
<td>

 (Policy = ManageVoting)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">id</td>
<td valign="top"><a href="#id">ID</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>writeEnumVerb</strong></td>
<td valign="top"><a href="#enumverb">EnumVerb</a></td>
<td>

 (Policy = Authorized)

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">enumVerb</td>
<td valign="top"><a href="#createenumverb">createEnumVerb</a></td>
<td></td>
</tr>
</tbody>
</table>

## Objects

### AddressType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>additionArea</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Доп. территория

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>additionAreaStreet</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Улица на доп. территории

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addrType</strong></td>
<td valign="top"><a href="#addrtype">AddrType</a></td>
<td>

Тип адреса

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>area</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Район

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>building</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Строение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>city</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Город

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>countryId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор страны

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>district</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Внутригородской район

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fiasCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Код КЛАДР

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>flat</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Квартира

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>frame</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Корпус

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fullAddress</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Строка полного адреса

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>house</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Дом

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>partialAddress</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Строка адреса (не включая дом, строение, корпус, номер квартиры)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Регион

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>settlement</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Поселение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>street</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Улица

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zipCode</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Индекс

</td>
</tr>
</tbody>
</table>

### ApiInformation

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>appBuildTime</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата сборки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appConfiguration</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Конфигурация сборки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appVersion</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Версия сборки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>aspDotnetVersion</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Framework version

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>aSPNETCORE_ENVIRONMENT</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

ASPNETCORE_ENVIRONMENT

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dbVersion</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

DB Info

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>osDescription</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Host OS version

</td>
</tr>
</tbody>
</table>

### Appeal

Обращение

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addressee</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Должностное лицо - адресат сообщения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>answerByPost</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutors</strong></td>
<td valign="top">[<a href="#appealexecutor">AppealExecutor</a>]</td>
<td>

Исполнители

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalExecutors</strong></td>
<td valign="top">[<a href="#appealexecutor">AppealExecutor</a>]</td>
<td>

Ответственные исполнители

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealType</strong></td>
<td valign="top"><a href="#appealtypeenum">AppealTypeEnum</a></td>
<td>

Тип обращения (обычное/сообщение обратной связи)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>approved</strong></td>
<td valign="top"><a href="#approvedenum">ApprovedEnum</a></td>
<td>

Принято к публикации/отказано

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFiles</strong></td>
<td valign="top">[<a href="#attachedfile">AttachedFile</a>]</td>
<td>

Присоединенные файлы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>author</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Автор

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authorName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>clientType</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>collective</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deloStatus</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deloText</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата регистрации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>externalExec</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extNumber</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Рег №

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>factDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>filesPush</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>firstName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fulltext</strong> ⚠️</td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

<p>⚠️ <strong>DEPRECATED</strong></p>
<blockquote>

Не пригодилась

</blockquote>
</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>hasUnapprovedMessages</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Есть неотмодерированные комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inn</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>latitude</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

Место на карте: широта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>likesCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Счетчик лайков

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>likeWIL</strong></td>
<td valign="top"><a href="#likewil">LikeWIL</a></td>
<td>

Как я лайкнул

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>longitude</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

Место на карте: долгота

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>markSend</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>messages</strong></td>
<td valign="top">[<a href="#message">Message</a>]</td>
<td>

Ответы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>messagesCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Всего комментариев

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderationStage</strong></td>
<td valign="top"><a href="#moderationstageenum">ModerationStageEnum</a></td>
<td>

Стадии модерации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderators</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Модераторы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>municipality</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>needToUpdateInExt</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Нужно обновить во внешней системе

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>organization</strong></td>
<td valign="top"><a href="#organizationtype">OrganizationType</a></td>
<td>

Организация

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>patronim</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>place</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Место на карте: описание

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>planDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>platform</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>post</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Должность

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>public_</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Публичность подтверждена

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>publicGranted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Публичность запрошена

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pushesSent</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regardMode</strong></td>
<td valign="top"><a href="#regardmodeenum">RegardModeEnum</a></td>
<td>

Режим оценки отчетов исполнителей

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата регистрации документа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#region">Region</a></td>
<td>

Регион

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region1</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regNumber</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Регистрационный номер документа в организации/ИП

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rejectionReason</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Причина отказа в публикации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>resDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubric</strong></td>
<td valign="top"><a href="#rubric">Rubric</a></td>
<td>

Рубрика

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sentToDelo</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>signatory</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Подписал

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>siteStatus</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusConfirmed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subscribeToAnswers</strong></td>
<td valign="top"><a href="#subscribetoanswersenum">SubscribeToAnswersEnum</a></td>
<td>

Уведомлять об ответах на данное обращение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystem</strong></td>
<td valign="top"><a href="#subsystem">Subsystem</a></td>
<td>

Подсистема

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territory</strong></td>
<td valign="top"><a href="#territory">Territory</a></td>
<td>

Территория

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>title</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topic</strong></td>
<td valign="top"><a href="#topic">Topic</a></td>
<td>

Топик

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicUpdated</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>unapprovedMessages</strong></td>
<td valign="top">[<a href="#message">Message</a>]</td>
<td>

неотмодерированные комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>unapprovedMessagesCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>uploaded</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usersLiked</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Кто поставил лайк или дизлайк

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>views</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>viewsCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Счетчик просмотров

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>viewWIV</strong></td>
<td valign="top"><a href="#viewwiv">ViewWIV</a></td>
<td>

Как я посмотрел

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>wORegNumberRegDate</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Без номера

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zip</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
</tbody>
</table>

### AppealExecutor

Исполнитель

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>appeals</strong></td>
<td valign="top">[<a href="#appeal">Appeal</a>]</td>
<td>

Обращения, в которых есть такой исполнитель

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealsPrincipal</strong></td>
<td valign="top">[<a href="#appeal">Appeal</a>]</td>
<td>

Обращения, в которых есть такой ответствнный исполнитель

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>department</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>departmentId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>organization</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>position</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Пользователь

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### AppealExecutorPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#appealexecutor">AppealExecutor</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### AppealPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#appeal">Appeal</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### AppealStats

Статистика обращений

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>appealsCount</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Количество обращений

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>approved</strong></td>
<td valign="top"><a href="#approvedenum">ApprovedEnum</a></td>
<td>

Одобренные

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удаленные

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>public</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Публичные

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#region">Region</a></td>
<td>

Регион

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubric</strong></td>
<td valign="top"><a href="#rubric">Rubric</a></td>
<td>

Рубрика

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sentToDelo</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Зарегистрировано в Деле

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>siteStatus</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Статус

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystem</strong></td>
<td valign="top"><a href="#subsystem">Subsystem</a></td>
<td>

Подсистема

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territory</strong></td>
<td valign="top"><a href="#territory">Territory</a></td>
<td>

Территория

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topic</strong></td>
<td valign="top"><a href="#topic">Topic</a></td>
<td>

Тема

</td>
</tr>
</tbody>
</table>

### AppealStatsPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#appealstats">AppealStats</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### AppealsStatBy

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>appealCount</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAtDay</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAtHour</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAtMonth</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAtYear</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#region">Region</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubric</strong></td>
<td valign="top"><a href="#rubric">Rubric</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territory</strong></td>
<td valign="top"><a href="#territory">Territory</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topic</strong></td>
<td valign="top"><a href="#topic">Topic</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAtDay</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAtHour</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAtMonth</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAtYear</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
</tbody>
</table>

### AppealsStatByPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#appealsstatby">AppealsStatBy</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### Appointment

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>admin</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>answers</strong></td>
<td valign="top">[<a href="#appointmentnotification">AppointmentNotification</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointmentRange</strong></td>
<td valign="top"><a href="#appointmentrange">AppointmentRange</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFiles</strong></td>
<td valign="top">[<a href="#attachedfile">AttachedFile</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authorName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authorNameUpperCase</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>date</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>municipality</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officer</strong></td>
<td valign="top"><a href="#officer">Officer</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rejectionReason</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubric</strong></td>
<td valign="top"><a href="#rubric">Rubric</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topic</strong></td>
<td valign="top"><a href="#topic">Topic</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zip</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### AppointmentNotification

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>appointment</strong></td>
<td valign="top"><a href="#appointment">Appointment</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>date</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>html</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### AppointmentNotificationPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#appointmentnotification">AppointmentNotification</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### AppointmentPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#appointment">Appointment</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### AppointmentRange

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>appointments</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>date</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleteMark</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>end</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officer</strong></td>
<td valign="top"><a href="#officer">Officer</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officerName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>registeredAppointments</strong></td>
<td valign="top">[<a href="#appointment">Appointment</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricOrder</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>start</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### AppointmentRangePageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#appointmentrange">AppointmentRange</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### AttachedFile

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>contentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Content-Type

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Внешний идентификатор

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>incomplete</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Не докачан

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isPublic</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Файл публичный

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>len</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Длина

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя файла

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>path</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Путь

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>private</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Файл приватный

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sequre</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Без доступа по чтению

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thumb</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Миниатюра

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>uploadedBy</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Кем загружен на сервер

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### AttachedFilePageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#attachedfile">AttachedFile</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### Balance

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>amount</strong></td>
<td valign="top"><a href="#decimal">Decimal</a></td>
<td>

Сумма

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>currency</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Валюта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>smsCount</strong></td>
<td valign="top"><a href="#uint">UInt</a></td>
<td>

Количество доступных отправок СМС

</td>
</tr>
</tbody>
</table>

### ClaimInfoType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>claimEntity</strong></td>
<td valign="top">[<a href="#claimentity">ClaimEntity</a>]</td>
<td>

Сущности к которым может применяться разрешение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>claimType</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Тип разрешения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Описание разрешения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>values</strong></td>
<td valign="top">[<a href="#claiminfovaluetype">ClaimInfoValueType</a>]</td>
<td>

Возможные значения

</td>
</tr>
</tbody>
</table>

### ClaimInfoValueType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Описание

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Значение

</td>
</tr>
</tbody>
</table>

### ClaimType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Тип разрешения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Значение разрешения

</td>
</tr>
</tbody>
</table>

### EmailTemplatePageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#emailtemplatetype">EmailTemplateType</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### EmailTemplateType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Ключ

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>template</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Шаблон

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата обновления

</td>
</tr>
</tbody>
</table>

### EnumVerb

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>customSettings</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystemUID</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### EnumVerbPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#enumverb">EnumVerb</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### FileDownload

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>data</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Данные в base64

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id файла

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pos</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Позиция в файле

</td>
</tr>
</tbody>
</table>

### FileUploadResult

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id файла

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>next</strong></td>
<td valign="top"><a href="#long">Long</a></td>
<td>

Ближайшая к началу позиция файла где данные еще не загружены

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>percent</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Процент загруженности (0-100)

</td>
</tr>
</tbody>
</table>

### LikeReport

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>dislikeCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Счетчик дизлайков

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>likeCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Счетчик лайков

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>liked</strong></td>
<td valign="top"><a href="#likestateenum">LikeStateEnum</a></td>
<td>

NONE - ничего,LIKED - стоит лайк, DISLIKED - cтоит дизлайк

</td>
</tr>
</tbody>
</table>

### LikeWIL

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>liked</strong></td>
<td valign="top"><a href="#likestateenum">LikeStateEnum</a></td>
<td>

NONE - ничего,LIKED - стоит лайк, DISLIKED - cтоит дизлайк

</td>
</tr>
</tbody>
</table>

### LoginResultType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>errorMessage</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Сообщение об ошибке

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Ключ подписки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>loginStatus</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Статус операции авторизации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>token</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Токен

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Пользователь

</td>
</tr>
</tbody>
</table>

### Message

Сообщение/комментарий

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>appeal</strong></td>
<td valign="top"><a href="#appeal">Appeal</a></td>
<td>

Корневое обращение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutor</strong></td>
<td valign="top"><a href="#appealexecutor">AppealExecutor</a></td>
<td>

Автор сообщения: Исполнитель (может быть внешним)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>approved</strong></td>
<td valign="top"><a href="#approvedenum">ApprovedEnum</a></td>
<td>

Принято к публикации/отказано

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFiles</strong></td>
<td valign="top">[<a href="#attachedfile">AttachedFile</a>]</td>
<td>

присоединенные файлы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>author</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Автор сообщения: Пользователь (внутренний)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>dislikesCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Счетчик дизлайков

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>due</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Ключ для упрощения доступа к иерархическим объектам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#messagekindenum">MessageKindEnum</a></td>
<td>

Вид сообщения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>likesCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Счетчик лайков

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>likeWIL</strong></td>
<td valign="top"><a href="#likewil">LikeWIL</a></td>
<td>

Как я лайкнул

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderationStage</strong></td>
<td valign="top"><a href="#moderationstageenum">ModerationStageEnum</a></td>
<td>

Стадии модерации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regardAverage</strong></td>
<td valign="top"><a href="#float">Float</a>!</td>
<td>

Средняя оценка

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regardsCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Счетчик оценок

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regardWIR</strong></td>
<td valign="top"><a href="#regardwir">RegardWIR</a></td>
<td>

Как я оценил

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subscribeToAnswers</strong></td>
<td valign="top"><a href="#subscribetoanswersenum">SubscribeToAnswersEnum</a></td>
<td>

Уведомлять об ответах на данное сообщение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Текст сообщения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usersLiked</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Лайки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usersRegard</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Оценки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usersViewed</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Просмотры

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>viewsCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Счетчик просмотров

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>viewWIV</strong></td>
<td valign="top"><a href="#viewwiv">ViewWIV</a></td>
<td>

Как я посмотрел

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteRoot</strong></td>
<td valign="top"><a href="#voteroot">VoteRoot</a></td>
<td>

Корневое голосование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### MessagePageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#message">Message</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### MessageSubscribeGet

Подписка на комментарии

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>due</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

DUE

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>level</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Уровень вложенности

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rootType</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Корневой объект

</td>
</tr>
</tbody>
</table>

### MessageSubscribeGet_

Подписка на комментарии

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>due</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

DUE

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>level</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Уровень вложенности

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rootType</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Корневой объект

</td>
</tr>
</tbody>
</table>

### News

Новость

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>attachedFiles</strong></td>
<td valign="top">[<a href="#attachedfile">AttachedFile</a>]</td>
<td>

Присоединенные файлы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>author</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Автор

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>commentsAllowed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Пользователи могут оставлять комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>likesCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Счетчик лайков

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>likeWIL</strong></td>
<td valign="top"><a href="#likewil">LikeWIL</a></td>
<td>

Как я лайкнул

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>messages</strong></td>
<td valign="top">[<a href="#message">Message</a>]</td>
<td>

Ответы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#region">Region</a></td>
<td>

Регион

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubric</strong></td>
<td valign="top"><a href="#rubric">Rubric</a></td>
<td>

Рубрика

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortText</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Краткий текст новости / начало текста новости

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>state</strong></td>
<td valign="top"><a href="#newsstateenum">NewsStateEnum</a></td>
<td>

Состояние

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territory</strong></td>
<td valign="top"><a href="#territory">Territory</a></td>
<td>

Территория

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Содержимое новости

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topic</strong></td>
<td valign="top"><a href="#topic">Topic</a></td>
<td>

Топик

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usersLiked</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Кто поставил лайк или дизлайк

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>viewsCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Счетчик просмотров

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>viewWIV</strong></td>
<td valign="top"><a href="#viewwiv">ViewWIV</a></td>
<td>

Как я посмотрел

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### NewsPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#news">News</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### Officer

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>authority</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>place</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>position</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ranges</strong></td>
<td valign="top">[<a href="#appointmentrange">AppointmentRange</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubric</strong></td>
<td valign="top"><a href="#rubric">Rubric</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### OfficerPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#officer">Officer</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### OperationResultType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>errorMessages</strong></td>
<td valign="top">[<a href="#string">String</a>]</td>
<td>

Сообщение об ошибке

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>success</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Результат операции

</td>
</tr>
</tbody>
</table>

### OrganizationType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addresses</strong></td>
<td valign="top">[<a href="#addresstype">AddressType</a>]</td>
<td>

Адреса

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>agencyTerRang</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Территориальная принадлежность ОГВ (только для государственных организаций,
код по справочнику «Субъекты Российской федерации» (ССРФ)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>agencyType</strong></td>
<td valign="top"><a href="#agencytype">AgencyType</a></td>
<td>

тип ОГВ

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>chiefIds</strong></td>
<td valign="top">[<a href="#long">Long</a>!]</td>
<td>

Идентификаторы руководителей организации (Policy = ManageUsersOrModerateOrganizations)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Email

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>emailVerified</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Email подтвержден

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fullName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Полное наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inn</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

ИНН

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isBrunch</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Признак филиала

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kpp</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

КПП

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>leg</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Код организационно-правовой формы по общероссийскому классификатору организационно-правовых форм

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>ogrn</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

ОГРН

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parent</strong></td>
<td valign="top">[<a href="#organizationtype">OrganizationType</a>]</td>
<td>

Головная организация (Policy = ManageUsersOrModerateOrganizations)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phones</strong></td>
<td valign="top">[<a href="#phonetype">PhoneType</a>]</td>
<td>

Телефоны

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Краткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userIds</strong></td>
<td valign="top">[<a href="#long">Long</a>!]</td>
<td>

Идентификаторы сотрудников организации (Policy = ManageUsersOrModerateOrganizations)

</td>
</tr>
</tbody>
</table>

### PToken

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deviceType</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>expiredAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата протухания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### PTokenPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#ptoken">PToken</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### PhoneType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>additional</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Добавочный номер

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Признак подтвержденного номера

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Номер телефона

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#typephone">TypePhone</a></td>
<td>

Тип телефона

</td>
</tr>
</tbody>
</table>

### ProtocolRecord

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>action</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appeal</strong></td>
<td valign="top"><a href="#appeal">Appeal</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutor</strong></td>
<td valign="top"><a href="#appealexecutor">AppealExecutor</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointment</strong></td>
<td valign="top"><a href="#appointment">Appointment</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointmentNotification</strong></td>
<td valign="top"><a href="#appointmentnotification">AppointmentNotification</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointmentRange</strong></td>
<td valign="top"><a href="#appointmentrange">AppointmentRange</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFile</strong></td>
<td valign="top"><a href="#attachedfile">AttachedFile</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>data</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>message</strong></td>
<td valign="top"><a href="#message">Message</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officer</strong></td>
<td valign="top"><a href="#officer">Officer</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#region">Region</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubric</strong></td>
<td valign="top"><a href="#rubric">Rubric</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystem</strong></td>
<td valign="top"><a href="#subsystem">Subsystem</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territory</strong></td>
<td valign="top"><a href="#territory">Territory</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topic</strong></td>
<td valign="top"><a href="#topic">Topic</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vote</strong></td>
<td valign="top"><a href="#vote">Vote</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItem</strong></td>
<td valign="top"><a href="#voteitem">VoteItem</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### ProtocolRecordPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#protocolrecord">ProtocolRecord</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### RedirectUrl

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>loginSystem</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Мнемоника системы авторизации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>url</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

URL перенаправления

</td>
</tr>
</tbody>
</table>

### RegardReport

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>regard</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Оценка

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regardCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Сколько юзеров оценили

</td>
</tr>
</tbody>
</table>

### RegardWIR

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>regard</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Оценка

</td>
</tr>
</tbody>
</table>

### Region

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>due</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Ключ для упрощения доступа к иерархическим объектам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executors</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Исполнители

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorsOfSubsystems</strong></td>
<td valign="top">[<a href="#userofsubsystem">UserOfSubsystem</a>]</td>
<td>

Исполнители по подсистемам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderators</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Модераторы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorsOfSubsystems</strong></td>
<td valign="top">[<a href="#userofsubsystem">UserOfSubsystem</a>]</td>
<td>

Модераторы по подсистемам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystems</strong></td>
<td valign="top">[<a href="#subsystem">Subsystem</a>]</td>
<td>

Подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### RegionPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#region">Region</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### RolePageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#roletype">RoleType</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### RoleSortedType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Признак сортироваки

</td>
</tr>
</tbody>
</table>

### RoleType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>claims</strong></td>
<td valign="top">[<a href="#claimtype">ClaimType</a>]</td>
<td>

Разрешения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания (UTC)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>disconnected</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Признак блокированной роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>disconnectedTimestamp</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата блокирования (UTC)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>editable</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Возможность редактирования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isAdminRegister</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Возможность назначать создаваемым пользователям

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Название роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата редактирования (UTC)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>users</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Пользователи включенные в роль

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Признак сортироваки

</td>
</tr>
</tbody>
</table>

### Rubric

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>docgroup</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Группа документов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>docGroupForUL</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Группа документов для ЮЛ

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>due</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Ключ для упрощения доступа к иерархическим объектам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executors</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Исполнители

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorsOfSubsystems</strong></td>
<td valign="top">[<a href="#userofsubsystem">UserOfSubsystem</a>]</td>
<td>

Исполнители по подсистемам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderators</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Модераторы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorsOfSubsystems</strong></td>
<td valign="top">[<a href="#userofsubsystem">UserOfSubsystem</a>]</td>
<td>

Модераторы по подсистемам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>receivers</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystems</strong></td>
<td valign="top">[<a href="#subsystem">Subsystem</a>]</td>
<td>

Подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useForPA</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Запись на прием

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### RubricPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#rubric">Rubric</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### Settings

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>attachedFiles</strong></td>
<td valign="top">[<a href="#attachedfile">AttachedFile</a>]</td>
<td>

Присоединенные файлы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание настройки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Ключ

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>readingRightLevel</strong></td>
<td valign="top"><a href="#rightlevel">RightLevel</a></td>
<td>

Требуемый уровень доступа по чтению

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>settingNodeType</strong></td>
<td valign="top"><a href="#settingnodetype">SettingNodeType</a></td>
<td>

Тип узла настройки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>settingType</strong></td>
<td valign="top"><a href="#settingtype">SettingType</a></td>
<td>

Тип настройки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Значение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>values</strong></td>
<td valign="top">[<a href="#createsettingsvalueitem">createSettingsValueItem</a>]</td>
<td>

Возможные значения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>valueType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Тип значения настройки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Вес (признак сортировки)

</td>
</tr>
</tbody>
</table>

### Subs2DueMessagesPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#messagesubscribeget_">MessageSubscribeGet_</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### Subscriptions

Подписки на всякие события

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>login</strong></td>
<td valign="top"><a href="#loginresulttype">LoginResultType</a></td>
<td>

Подписка на авторизацию

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">login</td>
<td valign="top"><a href="#logintype">LoginType</a>!</td>
<td>

Параметры авторизации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>newMessage</strong></td>
<td valign="top"><a href="#message">Message</a></td>
<td>

Подписка на обновление комментариев

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">subscribe</td>
<td valign="top"><a href="#messagesubscribeset">MessageSubscribeSet</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>newSettings</strong></td>
<td valign="top"><a href="#settings">Settings</a></td>
<td>

Подписка на обновление настроек

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">startsWith</td>
<td valign="top">[<a href="#string">String</a>]</td>
<td>

Должно начинаться с

</td>
</tr>
<tr>
<td colspan="2" align="right" valign="top">in</td>
<td valign="top">[<a href="#string">String</a>]</td>
<td>

Должно совпадать с

</td>
</tr>
</tbody>
</table>

### Subsystem

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isPublicDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mainEntityFields</strong></td>
<td valign="top">[<a href="#subsystemmainentityfield">SubsystemMainEntityField</a>]</td>
<td>

Поля основного объекта подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mainEntityName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя API основного объекта подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderator</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Модератор подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Печатное наименование подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>privateOnly</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regions</strong></td>
<td valign="top">[<a href="#region">Region</a>]</td>
<td>

Список регионов подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubrics</strong></td>
<td valign="top">[<a href="#rubric">Rubric</a>]</td>
<td>

Список рубрик подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territories</strong></td>
<td valign="top">[<a href="#territory">Territory</a>]</td>
<td>

Список территорий подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topics</strong></td>
<td valign="top">[<a href="#topic">Topic</a>]</td>
<td>

Список топиков подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>uID</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Уникальное наименование подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useAttaches</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useComments</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useDislikes</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useInMobileClient</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useLikesForComments</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useLikesForMain</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useMaps</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useRates</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useRegionsDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useRubricsDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useTerritoriesDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useTopicsDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### SubsystemMainEntityField

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание поля для пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nameAPI</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Имя поля объекта в подсистеме

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nameAPISecond</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

имя другого поля, содержимое которого выводим если основное не заполнено

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nameDisplayed</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя поля объекта в для пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>placeholder</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

выводим это если поле не заполнено

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>required</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

обязательно к заполнению при создании объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystem</strong></td>
<td valign="top"><a href="#subsystem">Subsystem</a></td>
<td>

подсистема

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

вес для сортировки

</td>
</tr>
</tbody>
</table>

### SubsystemPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#subsystem">Subsystem</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### Territory

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>due</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Ключ для упрощения доступа к иерархическим объектам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executors</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Исполнители

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorsOfSubsystems</strong></td>
<td valign="top">[<a href="#userofsubsystem">UserOfSubsystem</a>]</td>
<td>

Исполнители по подсистемам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderators</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Модераторы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorsOfSubsystems</strong></td>
<td valign="top">[<a href="#userofsubsystem">UserOfSubsystem</a>]</td>
<td>

Модераторы по подсистемам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystems</strong></td>
<td valign="top">[<a href="#subsystem">Subsystem</a>]</td>
<td>

Подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### TerritoryPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#territory">Territory</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### Topic

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>due</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Ключ для упрощения доступа к иерархическим объектам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executors</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Исполнители

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorsOfSubsystems</strong></td>
<td valign="top">[<a href="#userofsubsystem">UserOfSubsystem</a>]</td>
<td>

Исполнители по подсистемам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderators</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Модераторы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorsOfSubsystems</strong></td>
<td valign="top">[<a href="#userofsubsystem">UserOfSubsystem</a>]</td>
<td>

Модераторы по подсистемам

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystem</strong> ⚠️</td>
<td valign="top"><a href="#subsystem">Subsystem</a></td>
<td>

Подсистема

<p>⚠️ <strong>DEPRECATED</strong></p>
<blockquote>

Теперь в 'Subsystems'

</blockquote>
</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystems</strong></td>
<td valign="top">[<a href="#subsystem">Subsystem</a>]</td>
<td>

Подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### TopicPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#topic">Topic</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### User

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>addresses</strong></td>
<td valign="top">[<a href="#addresstype">AddressType</a>]</td>
<td>

Телефоны

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>allClaims</strong></td>
<td valign="top">[<a href="#claimtype">ClaimType</a>]</td>
<td>

Сводный список разрешений

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>claims</strong></td>
<td valign="top">[<a href="#claimtype">ClaimType</a>]</td>
<td>

Разрешения (Policy = Manage)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>disconnected</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Признак блокировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>disconnectedTimestamp</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Штамп времени блокировки (Policy = ManageUsersOrModerate)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Адрес электронной почты

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>emailConfirmed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Признак подтвержденного адреса электронной почты

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>firstName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fullName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Полное Имя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gender</strong></td>
<td valign="top"><a href="#gender">Gender</a></td>
<td>

Пол

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inn</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

ИНН

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Фамилия

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>loginSystem</strong></td>
<td valign="top"><a href="#loginsystem">LoginSystem</a></td>
<td>

Система логирования (Policy = ManageUsersOrModerate)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>middleName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Отчество

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>organizations</strong></td>
<td valign="top">[<a href="#organizationtype">OrganizationType</a>]</td>
<td>

Организации (Policy = ManageUsersOrModerateOrganizationsOrSubsystemOrganization)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phones</strong></td>
<td valign="top">[<a href="#phonetype">PhoneType</a>]</td>
<td>

Телефоны

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>profileConfirmed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Признак подтвержденного профиля

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roles</strong></td>
<td valign="top">[<a href="#roletype">RoleType</a>]</td>
<td>

Роли (Policy = Manage)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>selfFormRegister</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Признак пользователя зарегистрировавшегося через формы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>snils</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

СНИЛС

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта (Policy = Manage)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userIdentityDocuments</strong></td>
<td valign="top">[<a href="#useridentitydocumentstype">UserIdentityDocumentsType</a>]</td>
<td>

Документы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Логин

</td>
</tr>
</tbody>
</table>

### UserIdentityDocumentsType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>expiryDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Cрок действия документа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>issueDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата выдачи

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>issuedBy</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Кем выдан

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>issueId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Код подразделения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Номер документа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>series</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Серия документа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#doctype">DocType</a></td>
<td>

Тип документа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>verified</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Подтвержден

</td>
</tr>
</tbody>
</table>

### UserOfSubsystem

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>subsystem</strong></td>
<td valign="top"><a href="#subsystem">Subsystem</a></td>
<td>

Подсистема

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Пользователь

</td>
</tr>
</tbody>
</table>

### UserOrganizationPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#organizationtype">OrganizationType</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### UserPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### ViewReport

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>viewCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Сколько юзеров просмотрели

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>viewed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Просмотрено

</td>
</tr>
</tbody>
</table>

### ViewWIV

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>viewed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Я видел

</td>
</tr>
</tbody>
</table>

### Vote

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>attachedFiles</strong></td>
<td valign="top">[<a href="#attachedfile">AttachedFile</a>]</td>
<td>

Присоединенные файлы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>commentsAllowed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Пользователи могут оставлять комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>messages</strong></td>
<td valign="top">[<a href="#message">Message</a>]</td>
<td>

Ответы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#modeenum">ModeEnum</a></td>
<td>

Режим голосования ONE_SELECT: можно голосовать за 1 пункт, MANY_SELECT - за несколько пунктов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>modeLimitValue</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Максимальное число пунктов в режиме MANY_SELECT

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>offerEnabled</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Можно предложить свой вариант

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>offers</strong></td>
<td valign="top">[<a href="#voteoffer">VoteOffer</a>]</td>
<td>

Индивидуальные предложения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>offersCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Сколько пользователей внесли свои предложения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentVoteItems</strong></td>
<td valign="top">[<a href="#voteitem">VoteItem</a>]</td>
<td>

Пункты голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>textResult</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание итогов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>users</strong></td>
<td valign="top">[<a href="#user">User</a>]</td>
<td>

Голосовавшие пользователи

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usersCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Сколько пользователей голосовало

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItems</strong></td>
<td valign="top">[<a href="#voteitem">VoteItem</a>]</td>
<td>

Пункты голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteRoot</strong></td>
<td valign="top"><a href="#voteroot">VoteRoot</a></td>
<td>

Корень опроса

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteWIV</strong></td>
<td valign="top"><a href="#votewiv">VoteWIV</a></td>
<td>

Как я проголосовал

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### VoteItem

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>attachedFiles</strong></td>
<td valign="top">[<a href="#attachedfile">AttachedFile</a>]</td>
<td>

Присоединенные файлы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>counter</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Сколько пользователей проголосовало за данный пункт

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Пространное описание

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nextVote</strong></td>
<td valign="top"><a href="#vote">Vote</a></td>
<td>

Голосование, (следующий узел)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Порядковый номер пункта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>vote</strong></td>
<td valign="top"><a href="#vote">Vote</a></td>
<td>

Голосование, (текущий узел)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### VoteItemPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#voteitem">VoteItem</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### VoteOffer

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Текст индивидуального предложения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>user</strong></td>
<td valign="top"><a href="#user">User</a></td>
<td>

Кто внёс предложение

</td>
</tr>
</tbody>
</table>

### VotePageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#vote">Vote</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### VoteRoot

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>archiveAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда архивируется

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>archivedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда заархивировалось

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFiles</strong></td>
<td valign="top">[<a href="#attachedfile">AttachedFile</a>]</td>
<td>

Присоединенные файлы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>commentsAllowed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Пользователи могут оставлять комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>createdAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата создания объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>endAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда завершится

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>endedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда завершилось

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>messages</strong></td>
<td valign="top">[<a href="#message">Message</a>]</td>
<td>

Ответы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#region">Region</a></td>
<td>

Id Region

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubric</strong></td>
<td valign="top"><a href="#rubric">Rubric</a></td>
<td>

Id Rubric

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда начнется

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда началось

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>state</strong></td>
<td valign="top"><a href="#statusenum">StatusEnum</a></td>
<td>

Текущее состояние (БУДЕТ/ИДЕТ/ЗАКОНЧИЛОСЬ)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territory</strong></td>
<td valign="top"><a href="#territory">Territory</a></td>
<td>

Id Territory

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>textResult</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание итогов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topic</strong></td>
<td valign="top"><a href="#topic">Topic</a></td>
<td>

Id Topic

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>updatedAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата изменения объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>usersCount</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Сколько пользователей голосовало

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>votes</strong></td>
<td valign="top">[<a href="#vote">Vote</a>]</td>
<td>

Узлы голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
</tbody>
</table>

### VoteRootPageType

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>items</strong></td>
<td valign="top">[<a href="#voteroot">VoteRoot</a>]</td>
<td>

Элементы коллекции

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>total</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Размер выборки

</td>
</tr>
</tbody>
</table>

### VoteWIV

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Текст индивидуального предложения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Голосование имело место быть

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItemsIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Список Id пунктов голосования

</td>
</tr>
</tbody>
</table>

### createSettingsValueItem

<table>
<thead>
<tr>
<th align="left">Field</th>
<th align="right">Argument</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>displayText</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Отображаемый текст

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Значение параметра

</td>
</tr>
</tbody>
</table>

## Inputs

### ClaimInputType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Тип разрешения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Значение разрешения

</td>
</tr>
</tbody>
</table>

### ConfirmEmailType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>userId</strong></td>
<td valign="top"><a href="#long">Long</a></td>
<td>

Идентификатор пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Код подтверждения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sh</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Хэш код Email

</td>
</tr>
</tbody>
</table>

### EntitySelector

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Имя объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

id

</td>
</tr>
</tbody>
</table>

### FileDownloadRequest

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id файла

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>len</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Запрашиваемая длина

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pos</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Запрашиваемая позиция в файле

</td>
</tr>
</tbody>
</table>

### FileUpload

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя файла. Передается в первом кусочке

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id файла. Передается в последующих кусочках

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>len</strong></td>
<td valign="top"><a href="#long">Long</a></td>
<td>

Общая длина файла. Передается в первом кусочке

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pos</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Позиция передаваемых данных

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>data</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Данные в base64

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id объекта, куда аттачить файл

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Тип объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>contentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Content-Type

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sequre</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Без доступа по чтению

</td>
</tr>
</tbody>
</table>

### Filter

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя поля

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>like</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Поиск подстроки (%ищу%), начала (ищу%) или конца (%ищу) строки в любом регистре

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>not</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Инверсия условия

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>equal</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Равенство. Для строк с учетом регистра.

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>more</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Больше

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>less</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Меньше

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>greaterOrEqual</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Больше или равно

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lessOrEqual</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Меньше или равно

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>op</strong></td>
<td valign="top"><a href="#filternextopenum">FilterNextOpEnum</a></td>
<td>

Объединение с накопленным результатом

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>in</strong></td>
<td valign="top">[<a href="#string">String</a>]</td>
<td>

Есть в списке

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>withDeleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Включить удаленные элементы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>stacked</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Использование стека

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>where</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Условия фильтрации через DynamicLinq

</td>
</tr>
</tbody>
</table>

### ForgotPasswordType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>login</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Логин

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Адрес электронной почты

</td>
</tr>
</tbody>
</table>

### LikeRequest

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Тип объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>liked</strong></td>
<td valign="top"><a href="#likerequestenum">LikeRequestEnum</a></td>
<td>

NONE - ничего не делать,LIKE - поставить лайк,LIKE_OFF - снять лайк,DISLIKE -
поставить дизлайк,DISLIKE_OFF - снять дизлайк

</td>
</tr>
</tbody>
</table>

### LoginType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>subscribeKey</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Ключ подписки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>login</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Логин

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Пароль

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>loginSystem</strong></td>
<td valign="top"><a href="#loginsystem">LoginSystem</a></td>
<td>

Система авторизации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Код доступа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>callbackUrl</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

URL-обратного вызова

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>requestCodeError</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание ошибки запроса кода доступа через сторонние системы авторизации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>loginRequestSource</strong></td>
<td valign="top"><a href="#loginrequestsource">LoginRequestSource</a></td>
<td>

Тип системы через которую запорсили авторизацию (Возможные значения SITE или MOBILE)

</td>
</tr>
</tbody>
</table>

### MessageSubscribeSet

Подписка на комментарии

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>entityType</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Имя объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>entityId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

ID объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>level</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Уровень вложенности

</td>
</tr>
</tbody>
</table>

### OrderByItemType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>field</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Имя поля для сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>direction</strong></td>
<td valign="top"><a href="#searchdirection">SearchDirection</a></td>
<td>

Направление сортировки

</td>
</tr>
</tbody>
</table>

### PhoneInputType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#long">Long</a></td>
<td>

Идентификатор объекта (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#typephone">TypePhone</a></td>
<td>

Тип телефона (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Номер телефона (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>additional</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Добавочный номер (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Признак подтвержденного номера (Policy = Manage)

</td>
</tr>
</tbody>
</table>

### QueryOptions

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>filters</strong></td>
<td valign="top">[<a href="#filter">Filter</a>]</td>
<td>

Фильтр

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>range</strong></td>
<td valign="top"><a href="#range">Range</a></td>
<td>

Сортировка и пагинация

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>withDeleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Включить удаленные

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>withLinkedByOrganizations</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Включить связанные через организации пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thumbSize</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Желаемый размер миниатюры в аттачах, например 100x100

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thumbSizeTolerance</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Допустимое отклонение размера миниатюры в аттачах, например 20%x15%, 10x10

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>thumbFormat</strong></td>
<td valign="top"><a href="#skencodedimageformat">SkEncodedImageFormat</a></td>
<td>

Формат миниатюры

</td>
</tr>
</tbody>
</table>

### Range

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>orderBy</strong></td>
<td valign="top">[<a href="#orderbyitemtype">OrderByItemType</a>]</td>
<td>

Массив полей для сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>skip</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Сколько элементов пропустить с начала

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>take</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Сколько элементов вернуть

</td>
</tr>
</tbody>
</table>

### RangeSearch

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>skip</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Сколько элементов пропустить с начала

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>take</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Сколько элементов вернуть

</td>
</tr>
</tbody>
</table>

### RedirectInputUrl

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>loginSystem</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Мнемоника системы авторизации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>url</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

URL перенаправления

</td>
</tr>
</tbody>
</table>

### RegardRequest

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Тип объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regard</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Оценка

</td>
</tr>
</tbody>
</table>

### RegisterModelType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>userName</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Логин

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Пароль

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmPassword</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Подтверждение пароля

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fullName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Полное Имя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gender</strong></td>
<td valign="top"><a href="#gender">Gender</a></td>
<td>

Пол

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Адрес электронной почты

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>emailConfirmed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Признак подтвержденности электронной почты

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>disconnected</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Признак отключенного пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phones</strong></td>
<td valign="top">[<a href="#phoneinputtype">PhoneInputType</a>]</td>
<td>

Телефоны

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>claims</strong></td>
<td valign="top">[<a href="#claiminputtype">ClaimInputType</a>]</td>
<td>

Клаймы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roleIds</strong></td>
<td valign="top">[<a href="#bigint">BigInt</a>]</td>
<td>

Идентификаторы ролей

</td>
</tr>
</tbody>
</table>

### ResetPasswordInputType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>oldPassword</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Старый пароль

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>newPassword</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Новый пароль

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmPassword</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Повторение нового пароля

</td>
</tr>
</tbody>
</table>

### RestorePasswordType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>userId</strong></td>
<td valign="top"><a href="#long">Long</a>!</td>
<td>

Идентификатор пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>code</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Код восстановления пароля

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>password</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Новый пароль

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>confirmPassword</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Подтверждение пароля

</td>
</tr>
</tbody>
</table>

### RoleInputType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Название роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>disconnected</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Признак отключения роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>claims</strong></td>
<td valign="top">[<a href="#claiminputtype">ClaimInputType</a>]</td>
<td>

Разрешения

</td>
</tr>
</tbody>
</table>

### RoleSortingType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор роли

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a>!</td>
<td>

Признак сортироваки

</td>
</tr>
</tbody>
</table>

### UserInputType

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>fullName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Полное Имя (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Логин (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>gender</strong></td>
<td valign="top"><a href="#gender">Gender</a></td>
<td>

Пол (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Адрес электронной почты (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>emailConfirmed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Признак подтвержденности электронной почты (Policy = Manage)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>disconnected</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Признак отключенного пользователя (Policy = Manage)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phones</strong></td>
<td valign="top">[<a href="#phoneinputtype">PhoneInputType</a>]</td>
<td>

Телефоны (Policy = LoginSystemForm)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>claims</strong></td>
<td valign="top">[<a href="#claiminputtype">ClaimInputType</a>]</td>
<td>

Клаймы (Policy = Manage)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>roleIds</strong></td>
<td valign="top">[<a href="#bigint">BigInt</a>]</td>
<td>

Идентификаторы ролей (Policy = Manage)

</td>
</tr>
</tbody>
</table>

### ViewRequest

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>type</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Тип объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>viewed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a>!</td>
<td>

Просмотрено

</td>
</tr>
</tbody>
</table>

### VoteAction

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>voteId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItemIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Массив Id выбранных пунктов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItemNumbers</strong></td>
<td valign="top">[<a href="#int">Int</a>]</td>
<td>

Массив номеров выбранных пунктов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>comment</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Текст индивидуального предложения

</td>
</tr>
</tbody>
</table>

### approveAppeal

Принятие к публикации или отклонение обращения

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>approvedEnum</strong></td>
<td valign="top"><a href="#approvedenum">ApprovedEnum</a></td>
<td>

Принято к публикации/отказано

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rejectionReason</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Причина отказа в публикации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderationStage</strong></td>
<td valign="top"><a href="#moderationstageenum">ModerationStageEnum</a></td>
<td>

Стадии модерации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>public_</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Публичное

</td>
</tr>
</tbody>
</table>

### approveAttachedFile

Принятие к публикации или отклонение приложенного файла

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>public_</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Публичный

</td>
</tr>
</tbody>
</table>

### approveMessage

Принятие к публикации или отклонение комментария

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>approvedEnum</strong></td>
<td valign="top"><a href="#approvedenum">ApprovedEnum</a></td>
<td>

Принято к публикации/отказано

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderationStage</strong></td>
<td valign="top"><a href="#moderationstageenum">ModerationStageEnum</a></td>
<td>

Стадии модерации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>public_</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Публичное

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>reason</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Создать комментарий с описанием причины

</td>
</tr>
</tbody>
</table>

### approveNews

Публикация или снятие с публикации новости

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>state</strong></td>
<td valign="top"><a href="#newsstateenum">NewsStateEnum</a></td>
<td>

Состояние

</td>
</tr>
</tbody>
</table>

### createAppeal

Обращение

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата регистрации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extNumber</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Рег №

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addressee</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Должностное лицо - адресат сообщения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authorName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>firstName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>patronim</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>title</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicUpdated</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>siteStatus</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>markSend</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusConfirmed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pushesSent</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fulltext</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>platform</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sentToDelo</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>answerByPost</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>externalExec</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zip</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region1</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>municipality</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>views</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>uploaded</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>clientType</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inn</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>resDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>planDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>factDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deloText</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deloStatus</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>collective</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>filesPush</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>publicGranted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Публичность запрошена

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>signatory</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Подписал

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>post</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Должность

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regNumber</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Регистрационный номер документа в организации/ИП

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата регистрации документа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>wORegNumberRegDate</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Без номера

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>latitude</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

Место на карте: широта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>longitude</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

Место на карте: долгота

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>place</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Место на карте: описание

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regardMode</strong></td>
<td valign="top"><a href="#regardmodeenum">RegardModeEnum</a></td>
<td>

Режим оценки отчетов исполнителей

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subscribeToAnswers</strong></td>
<td valign="top"><a href="#subscribetoanswersenum">SubscribeToAnswersEnum</a></td>
<td>

Уведомлять об ответах на данное обращение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Рубрики

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regionId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Региона

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Топика

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territoryId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Территории

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorsIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Модераторов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorsIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Модераторов (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>organizationId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Организации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Исполнителей

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Исполнителей (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalExecutorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Ответственных исполнителей

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalExecutorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Ответственных исполнителей (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystemId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Уникальное наименование подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealType</strong></td>
<td valign="top"><a href="#appealtypeenum">AppealTypeEnum</a></td>
<td>

Тип обращения (обычное/сообщение обратной связи)

</td>
</tr>
</tbody>
</table>

### createAppealExecutor

Обращение

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>department</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>departmentId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>position</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>organization</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id обращений, в которых есть такой исполнитель

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id обращений, в которых есть такой ответственный исполнитель

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id обращений, в которых есть такой исполнитель (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id обращений, в которых есть такой ответственный исполнитель (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id пользователя

</td>
</tr>
</tbody>
</table>

### createAppointment

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>date</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rejectionReason</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authorName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authorNameUpperCase</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>municipality</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zip</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>admin</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>answersIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>answersIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officerId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointmentRangeId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### createAppointmentNotification

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>date</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>html</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointmentId</strong></td>
<td valign="top"><a href="#long">Long</a></td>
<td></td>
</tr>
</tbody>
</table>

### createAppointmentRange

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>date</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>start</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>end</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointments</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officerName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricOrder</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleteMark</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officerId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>registeredAppointmentsIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>registeredAppointmentsIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
</tbody>
</table>

### createAttachedFile

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя файла

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>path</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Путь

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Внешний идентификатор

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>private</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Файл приватный

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isPublic</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Файл публичный

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>contentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Content-Type

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sequre</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Без доступа по чтению

</td>
</tr>
</tbody>
</table>

### createEnumVerb

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystemUID</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>customSettings</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### createFBAppeal

Сообщение обратной связи

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата регистрации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extNumber</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Рег №

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addressee</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Должностное лицо - адресат сообщения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authorName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>firstName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>patronim</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>title</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicUpdated</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>siteStatus</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>markSend</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusConfirmed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pushesSent</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fulltext</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>platform</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sentToDelo</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>answerByPost</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>externalExec</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zip</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region1</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>municipality</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>views</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>uploaded</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>clientType</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inn</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>resDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>planDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>factDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deloText</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deloStatus</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>collective</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>filesPush</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>publicGranted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Публичность запрошена

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>signatory</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Подписал

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>post</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Должность

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regNumber</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Регистрационный номер документа в организации/ИП

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата регистрации документа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>wORegNumberRegDate</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Без номера

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>latitude</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

Место на карте: широта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>longitude</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

Место на карте: долгота

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>place</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Место на карте: описание

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regardMode</strong></td>
<td valign="top"><a href="#regardmodeenum">RegardModeEnum</a></td>
<td>

Режим оценки отчетов исполнителей

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subscribeToAnswers</strong></td>
<td valign="top"><a href="#subscribetoanswersenum">SubscribeToAnswersEnum</a></td>
<td>

Уведомлять об ответах на данное обращение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Рубрики

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regionId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Региона

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Топика

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territoryId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Территории

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorsIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Модераторов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorsIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Модераторов (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>organizationId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Организации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Исполнителей

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Исполнителей (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalExecutorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Ответственных исполнителей

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalExecutorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Ответственных исполнителей (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystemId</strong></td>
<td valign="top"><a href="#id">ID</a>!</td>
<td>

Уникальное наименование подсистемы

</td>
</tr>
</tbody>
</table>

### createMessage

Сообщение/комментарий

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Текст сообщения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>kind</strong></td>
<td valign="top"><a href="#messagekindenum">MessageKindEnum</a></td>
<td>

Вид сообщения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutorId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Исполнитель

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subscribeToAnswers</strong></td>
<td valign="top"><a href="#subscribetoanswersenum">SubscribeToAnswersEnum</a></td>
<td>

Уведомлять об ответах на данное сообщение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>approved</strong></td>
<td valign="top"><a href="#approvedenum">ApprovedEnum</a></td>
<td>

Принято к публикации/отказано

</td>
</tr>
</tbody>
</table>

### createNews

Обращение

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Содержимое новости

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortText</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Краткий текст новости / начало текста новости

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>commentsAllowed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Пользователи могут оставлять комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Рубрики

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regionId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Региона

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Топика

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territoryId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Территории

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов (+/-)

</td>
</tr>
</tbody>
</table>

### createOfficer

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>position</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authority</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>place</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rangesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rangesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
</tbody>
</table>

### createPToken

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>deviceType</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>expiredAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата протухания объекта

</td>
</tr>
</tbody>
</table>

### createRegion

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания модераторов (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания исполнителей (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (Policy = ManageRegionsSubsystem)

</td>
</tr>
</tbody>
</table>

### createRubric

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

--- (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>docgroup</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Группа документов (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>receivers</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

--- (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useForPA</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Запись на прием (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>docGroupForUL</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Группа документов для ЮЛ (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания модераторов (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания исполнителей (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (Policy = ManageRubricsSubsystem)

</td>
</tr>
</tbody>
</table>

### createSubsystem

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>uID</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Уникальное наименование подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Печатное наименование подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regionIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id регионов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id рубрик

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territoryIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id территорий

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id топиков

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id модератора подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mainEntityName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя API основного объекта подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mainEntityFields</strong></td>
<td valign="top">[<a href="#createsubsystemmainentityfield">createSubsystemMainEntityField</a>]</td>
<td>

Поля основного объекта подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isPublicDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>privateOnly</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useLikesForMain</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useLikesForComments</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useDislikes</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useRates</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useComments</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useAttaches</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useMaps</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useRubricsDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useTerritoriesDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useRegionsDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useTopicsDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useInMobileClient</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
</tbody>
</table>

### createSubsystemMainEntityField

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>nameAPI</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td>

Имя поля объекта в подсистеме

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nameDisplayed</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя поля объекта в для пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание поля для пользователя

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nameAPISecond</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

имя другого поля, содержимое которого выводим если основное не заполнено

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>required</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

обязательно к заполнению при создании объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

вес для сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>placeholder</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

выводим это если поле не заполнено

</td>
</tr>
</tbody>
</table>

### createTerritory

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания модераторов (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания исполнителей (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (Policy = ManageTerritoriesSubsystem)

</td>
</tr>
</tbody>
</table>

### createTopic

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование типа родителя (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания модераторов (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания исполнителей (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystemId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Уникальное наименование подсистемы (Policy = ManageTopicsSubsystem)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (Policy = ManageTopicsSubsystem)

</td>
</tr>
</tbody>
</table>

### createVote

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>textResult</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание итогов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#modeenum">ModeEnum</a></td>
<td>

Режим голосования ONE_SELECT: можно голосовать за 1 пункт, MANY_SELECT - за несколько пунктов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>offerEnabled</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Можно предложить свой вариант

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>modeLimitValue</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Максимальное число пунктов в режиме MANY_SELECT

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>commentsAllowed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Пользователи могут оставлять комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItemsIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Список Id пунктов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentVoteItemsIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Родительские пункты многоузлового голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteRootId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id корня

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItems</strong></td>
<td valign="top">[<a href="#createvoteitem">createVoteItem</a>]</td>
<td>

Пункты голосования

</td>
</tr>
</tbody>
</table>

### createVoteItem

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Пространное описание

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Порядковый номер пункта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id голосования, (текущий узел)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nextVoteId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id голосования, (следующего узла)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов

</td>
</tr>
</tbody>
</table>

### createVoteRoot

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда начнется

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>endAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда завершится

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>archiveAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда архивируется

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>textResult</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание итогов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>commentsAllowed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Пользователи могут оставлять комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>votesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Список Id узлов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteEntryId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id узла входа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regionId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Region

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Rubric

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territoryId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Territory

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Topic

</td>
</tr>
</tbody>
</table>

### deleteMessage

Удаление комментария

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>reason</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Причина удаления

</td>
</tr>
</tbody>
</table>

### deletePToken

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a>!</td>
<td></td>
</tr>
</tbody>
</table>

### updateAppeal

Обращение

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата регистрации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extNumber</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Рег №

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>addressee</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Должностное лицо - адресат сообщения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authorName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>firstName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>lastName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>patronim</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>title</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicUpdated</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>siteStatus</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>markSend</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>statusConfirmed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>pushesSent</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>fulltext</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>platform</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>sentToDelo</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>answerByPost</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>externalExec</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zip</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region1</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>municipality</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>views</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>uploaded</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>clientType</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>inn</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>resDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>planDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>factDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deloText</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deloStatus</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>collective</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>filesPush</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>publicGranted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Публичность запрошена

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>signatory</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Подписал

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>post</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Должность

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regNumber</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Регистрационный номер документа в организации/ИП

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regDate</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Дата регистрации документа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>wORegNumberRegDate</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Без номера

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>latitude</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

Место на карте: широта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>longitude</strong></td>
<td valign="top"><a href="#float">Float</a></td>
<td>

Место на карте: долгота

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>place</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Место на карте: описание

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regardMode</strong></td>
<td valign="top"><a href="#regardmodeenum">RegardModeEnum</a></td>
<td>

Режим оценки отчетов исполнителей

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subscribeToAnswers</strong></td>
<td valign="top"><a href="#subscribetoanswersenum">SubscribeToAnswersEnum</a></td>
<td>

Уведомлять об ответах на данное обращение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Рубрики

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regionId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Региона

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Топика

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territoryId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Территории

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorsIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Модераторов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorsIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Модераторов (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>organizationId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Организации

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Исполнителей

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealExecutorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Исполнителей (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalExecutorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Ответственных исполнителей

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalExecutorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Ответственных исполнителей (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>needToUpdateInExt</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Нужно обновить во внешней системе

</td>
</tr>
</tbody>
</table>

### updateAppealExecutor

Обращение

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>department</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>departmentId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>position</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>organization</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

---

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id обращений, в которых есть такой исполнитель

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id обращений, в которых есть такой ответственный исполнитель

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id обращений, в которых есть такой исполнитель (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appealPrincipalIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id обращений, в которых есть такой ответственный исполнитель (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>userId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id пользователя

</td>
</tr>
</tbody>
</table>

### updateAppointment

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>date</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rejectionReason</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authorName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authorNameUpperCase</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>region</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>municipality</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>address</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>zip</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>phone</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>email</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>admin</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>answersIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>answersIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officerId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointmentRangeId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
</tbody>
</table>

### updateAppointmentNotification

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>date</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>status</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>html</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointmentId</strong></td>
<td valign="top"><a href="#long">Long</a></td>
<td></td>
</tr>
</tbody>
</table>

### updateAppointmentRange

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>date</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>start</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>end</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>appointments</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officerName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricOrder</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleteMark</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>officerId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>registeredAppointmentsIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>registeredAppointmentsIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
</tbody>
</table>

### updateAttachedFile

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя файла

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>path</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Путь

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Внешний идентификатор

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>private</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Файл приватный

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isPublic</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Файл публичный

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>contentType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Content-Type

</td>
</tr>
</tbody>
</table>

### updateEnumVerb

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>id</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор объекта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>group</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subsystemUID</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>customSettings</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
</tbody>
</table>

### updateMessage

Сообщение/комментарий

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Текст сообщения

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subscribeToAnswers</strong></td>
<td valign="top"><a href="#subscribetoanswersenum">SubscribeToAnswersEnum</a></td>
<td>

Уведомлять об ответах на данное сообщение

</td>
</tr>
</tbody>
</table>

### updateNews

Новость

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Содержимое новости

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortText</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Краткий текст новости / начало текста новости

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>commentsAllowed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Пользователи могут оставлять комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Рубрики

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regionId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Региона

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Топика

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territoryId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Территории

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов (+/-)

</td>
</tr>
</tbody>
</table>

### updateOfficer

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>position</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>authority</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>place</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rangesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rangesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td></td>
</tr>
</tbody>
</table>

### updateRegion

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (+/-) (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания модераторов (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (+/-) (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания исполнителей (Policy = ManageRegions)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (Policy = ManageRegionsSubsystem)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (+/-) (Policy = ManageRegionsSubsystem)

</td>
</tr>
</tbody>
</table>

### updateRubric

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extType</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

--- (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>docgroup</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Группа документов (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>receivers</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

--- (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useForPA</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Запись на прием (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>docGroupForUL</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Группа документов для ЮЛ (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (+/-) (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания модераторов (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (+/-) (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания исполнителей (Policy = ManageRubrics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (Policy = ManageRubricsSubsystem)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (+/-) (Policy = ManageRubricsSubsystem)

</td>
</tr>
</tbody>
</table>

### updateSettings

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>key</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Ключ

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>value</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Значение

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов

</td>
</tr>
</tbody>
</table>

### updateSubsystem

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Печатное наименование подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regionIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id регионов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id рубрик

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territoryIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id территорий

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id топиков

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id модератора подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regionIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id регионов (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id рубрик (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territoryIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id территорий (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id топиков (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mainEntityName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Имя API основного объекта подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mainEntityFields</strong></td>
<td valign="top">[<a href="#createsubsystemmainentityfield">createSubsystemMainEntityField</a>]</td>
<td>

Поля основного объекта подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mainEntityFieldsUpdate</strong></td>
<td valign="top">[<a href="#createsubsystemmainentityfield">createSubsystemMainEntityField</a>]</td>
<td>

Поля основного объекта подсистемы

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isPublicDefault</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>privateOnly</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useLikesForMain</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useLikesForComments</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useDislikes</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useRates</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useComments</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useAttaches</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useMaps</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useRubricsDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useTerritoriesDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useRegionsDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>useTopicsDescription</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

?

</td>
</tr>
</tbody>
</table>

### updateTerritory

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (+/-) (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания модераторов (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (+/-) (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания исполнителей (Policy = ManageTerritories)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (Policy = ManageTerritoriesSubsystem)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (+/-) (Policy = ManageTerritoriesSubsystem)

</td>
</tr>
</tbody>
</table>

### updateTopic

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>isNode</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Является папкой (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>extId</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Идентификатор во внешней системе (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Идентификатор родителя (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id модераторов (+/-) (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>moderatorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания модераторов (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id исполнителей (+/-) (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>executorIdsSubSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем для задания исполнителей (Policy = ManageTopics)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDs</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (Policy = ManageTopicsSubsystem)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>subSystemUIDsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

UID подсистем (+/-) (Policy = ManageTopicsSubsystem)

</td>
</tr>
</tbody>
</table>

### updateVote

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>textResult</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание итогов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>mode</strong></td>
<td valign="top"><a href="#modeenum">ModeEnum</a></td>
<td>

Режим голосования ONE_SELECT: можно голосовать за 1 пункт, MANY_SELECT - за несколько пунктов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>offerEnabled</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Можно предложить свой вариант

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>modeLimitValue</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Максимальное число пунктов в режиме MANY_SELECT

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>commentsAllowed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Пользователи могут оставлять комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItemsIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Список Id пунктов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentVoteItemsIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Родительские пункты многоузлового голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteRootId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id корня

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItems</strong></td>
<td valign="top">[<a href="#updatevoteitem">updateVoteItem</a>]</td>
<td>

Пункты голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteItemsIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Список Id пунктов голосования (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>parentVoteItemsIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Родительские пункты многоузлового голосования (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов (+/-)

</td>
</tr>
</tbody>
</table>

### updateVoteItem

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>description</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Пространное описание

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>number</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Порядковый номер пункта

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id голосования, (текущий узел)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>nextVoteId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id голосования, (следующего узла)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id Присоединенных файлов (+/-)

</td>
</tr>
</tbody>
</table>

### updateVoteRoot

<table>
<thead>
<tr>
<th colspan="2" align="left">Field</th>
<th align="left">Type</th>
<th align="left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2" valign="top"><strong>weight</strong></td>
<td valign="top"><a href="#int">Int</a></td>
<td>

Для особой сортировки

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>deleted</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Удален

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>name</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>shortName</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Короткое наименование

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>startAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда начнется

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>endAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда завершится

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>archiveAt</strong></td>
<td valign="top"><a href="#datetime">DateTime</a></td>
<td>

Когда архивируется

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>text</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>textResult</strong></td>
<td valign="top"><a href="#string">String</a></td>
<td>

Описание итогов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>commentsAllowed</strong></td>
<td valign="top"><a href="#boolean">Boolean</a></td>
<td>

Пользователи могут оставлять комментарии

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>votesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Список Id узлов голосования

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIds</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>voteEntryId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id узла входа

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>regionId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Region

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>rubricId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Rubric

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>territoryId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Territory

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>topicId</strong></td>
<td valign="top"><a href="#id">ID</a></td>
<td>

Id Topic

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>votesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Список Id узлов голосования (+/-)

</td>
</tr>
<tr>
<td colspan="2" valign="top"><strong>attachedFilesIdsChange</strong></td>
<td valign="top">[<a href="#id">ID</a>]</td>
<td>

Id присоединенных файлов (+/-)

</td>
</tr>
</tbody>
</table>

## Enums

### AddrType

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>UNKOWN</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>RESIDENTIAL</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>REGISTRATION</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>LEGAL</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ACTUAL</strong></td>
<td></td>
</tr>
</tbody>
</table>

### AgencyType

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>UNKNOWN</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>FED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>FND</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>REG</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>LCL</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>GOV</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MCL</strong></td>
<td></td>
</tr>
</tbody>
</table>

### AppealTypeEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>NORMAL</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>FEEDBACK</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ApprovedEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>UNDEFINED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ACCEPTED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>REJECTED</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ClaimEntity

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>USER</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ROLE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>SYSTEM</strong></td>
<td></td>
</tr>
</tbody>
</table>

### DocType

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>NONE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>PASSPORT</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>FOREIGN</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DRIVING_LICENSE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MILITARY</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>FOREIGN_PASSPORT</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MEDICAL_POLICY</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>BIRTH_CERT</strong></td>
<td></td>
</tr>
</tbody>
</table>

### FilterNextOpEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>AND</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>OR</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>EXCEPT</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>PUSH</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>XY</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>POP</strong></td>
<td></td>
</tr>
</tbody>
</table>

### Gender

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>MALE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>FEMALE</strong></td>
<td></td>
</tr>
</tbody>
</table>

### LikeRequestEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>NONE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>LIKE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>LIKE_OFF</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DISLIKE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DISLIKE_OFF</strong></td>
<td></td>
</tr>
</tbody>
</table>

### LikeStateEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>NONE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>LIKED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DISLIKED</strong></td>
<td></td>
</tr>
</tbody>
</table>

### LoginRequestSource

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>UNDEFINED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>SITE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MOBILE</strong></td>
<td></td>
</tr>
</tbody>
</table>

### LoginSystem

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>FORM</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ESIA</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>VK</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>GOOGLE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>FACEBOOK</strong></td>
<td></td>
</tr>
</tbody>
</table>

### MessageKindEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>NORMAL</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>REASON_TO_PARENT_MESSAGE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>SYSTEM_USER_MESSAGE</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ModeEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>ONE_SELECT</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MANY_SELECT</strong></td>
<td></td>
</tr>
</tbody>
</table>

### ModerationStageEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>WAIT</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>INPROGRESS</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DONE</strong></td>
<td></td>
</tr>
</tbody>
</table>

### NewsStateEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>DRAFT</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MODERATE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>PUBLISHED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>UNPUBLISHED</strong></td>
<td></td>
</tr>
</tbody>
</table>

### RegardModeEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>DISABLED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>APPEALAUTHOR</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ANYUSER</strong></td>
<td></td>
</tr>
</tbody>
</table>

### RightLevel

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>UNDEFINED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>UNAUTHORIZED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>AUTHORIZED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MANAGER</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ADMINISTRATOR_SETTINGS</strong></td>
<td></td>
</tr>
</tbody>
</table>

### SearchDirection

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>ASC</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DESC</strong></td>
<td></td>
</tr>
</tbody>
</table>

### SettingNodeType

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>NONE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>TAB_GROUP</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>GROUP</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>VALUE</strong></td>
<td></td>
</tr>
</tbody>
</table>

### SettingType

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>UNDEFINED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>VALUE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>FILE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>VALUE_AND_FILE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>SECRET_FILE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>VALUE_AND_SECRET_FILE</strong></td>
<td></td>
</tr>
</tbody>
</table>

### SkEncodedImageFormat

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>BMP</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>GIF</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ICO</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>JPEG</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>PNG</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>WBMP</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>WEBP</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>PKM</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>KTX</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>ASTC</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>DNG</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>HEIF</strong></td>
<td></td>
</tr>
</tbody>
</table>

### StatusEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>DRAFT</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IS_PLANNED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IN_PROGRESS</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IS_DONE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>IS_ARCHIVED</strong></td>
<td></td>
</tr>
</tbody>
</table>

### SubscribeToAnswersEnum

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>NOT_SUBSCRIBED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>SUBSCRIBED</strong></td>
<td></td>
</tr>
</tbody>
</table>

### TypePhone

<table>
<thead>
<th align="left">Value</th>
<th align="left">Description</th>
</thead>
<tbody>
<tr>
<td valign="top"><strong>UNDEFINED</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>PHONE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>WORK</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>MOBILE</strong></td>
<td></td>
</tr>
<tr>
<td valign="top"><strong>FAX</strong></td>
<td></td>
</tr>
</tbody>
</table>

## Scalars

### BigInt

### Boolean

The `Boolean` scalar type represents `true` or `false`.

### Byte

### Date

The `Date` scalar type represents a year, month and day in accordance with the
[ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.

### DateTime

The `DateTime` scalar type represents a date and time. `DateTime` expects
timestamps to be formatted in accordance with the
[ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.

### DateTimeOffset

The `DateTimeOffset` scalar type represents a date, time and offset from UTC.
`DateTimeOffset` expects timestamps to be formatted in accordance with the
[ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.

### Decimal

### Float

The `Float` scalar type represents signed double-precision fractional values as specified by [IEEE 754](https://en.wikipedia.org/wiki/IEEE_floating_point).

### Guid

### ID

The `ID` scalar type represents a unique identifier, often used to refetch an object or as key for a cache. The ID type appears in a JSON response as a String; however, it is not intended to be human-readable. When expected as an input type, any string (such as `"4"`) or integer (such as `4`) input value will be accepted as an ID.

### Int

The `Int` scalar type represents non-fractional signed whole numeric values. Int can represent values between -(2^31) and 2^31 - 1.

### Long

### Milliseconds

The `Milliseconds` scalar type represents a period of time represented as the total number of milliseconds.

### SByte

### Seconds

The `Seconds` scalar type represents a period of time represented as the total number of seconds.

### Short

### String

The `String` scalar type represents textual data, represented as UTF-8 character sequences. The String type is most often used by GraphQL to represent free-form human-readable text.

### UInt

### ULong

### UShort

### Uri

