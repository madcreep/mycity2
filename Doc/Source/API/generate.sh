#!/bin/bash
../../../BIT.MyCity/ClientApp/

cdir=${PWD}
cd ../../../BIT.MyCity/ClientApp/
npm run gql:update-schema
cd $cdir
npx graphql-markdown  \
	--title 'Описание API' \
	--prologue '#Пролог' \
	--epilogue '#Эпилог' \
	--header "Authorization=Bearer token" \
	../../../BIT.MyCity/ClientApp/src/app/data-services/graphql/schema.graphql > api.md