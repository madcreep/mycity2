#FROM --platform=$BUILDPLATFORM bizit2/mycity2bin:latest AS mycity2bin
FROM --platform=$BUILDPLATFORM registry.biz-it.ru/mycity2bin:latest AS mycity2bin

# FROM bizit2/gost-included:latest as openssl-gost
# FROM registry.biz-it.ru/alpinegost:2024 as openssl-gost
# FROM registry.biz-it.ru/alpinegost:2024 as openssl-gost
FROM registry.biz-it.ru/gost-bin:2024 as openssl-gost
# FROM registry.biz-it.ru/gost-included:latest as openssl-gost

# FROM mcr.microsoft.com/dotnet/aspnet:6.0 as runtime
FROM registry.biz-it.ru/dotnet_aspnet:6.0 as runtime

WORKDIR /publish

COPY --from=mycity2bin /publish .

ENV ASPNETCORE_URLS http://+:5000

RUN apt-get install -y tzdata
RUN echo "Europe/Moscow" > /etc/timezone
RUN ln -fs /usr/share/zoneinfo/Europe/Moscow /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata

COPY --from=openssl-gost /usr/local/ssl /usr/local/ssl
COPY --from=openssl-gost /usr/local/ssl/openssl.cnf /etc/ssl/openssl.cnf
COPY --from=openssl-gost /usr/local/ssl/bin/openssl /usr/bin/openssl
COPY --from=openssl-gost /usr/local/curl /usr/local/curl
COPY --from=openssl-gost /usr/local/curl/bin/curl /usr/bin/curl
COPY --from=openssl-gost /usr/local/bin/gostsum /usr/local/bin/gostsum
COPY --from=openssl-gost /usr/local/bin/gost12sum /usr/local/bin/gost12sum

COPY --from=openssl-gost /usr/local/ssl/lib/pkgconfig/* /usr/lib/x86_64-linux-gnu/pkgconfig/
COPY --from=openssl-gost /usr/local/curl/lib/pkgconfig/* /usr/lib/x86_64-linux-gnu/pkgconfig/

RUN echo "/usr/local/ssl/lib" >> /etc/ld.so.conf.d/ssl.conf && ldconfig

ENTRYPOINT ["dotnet", "BIT.MyCity.dll"]

