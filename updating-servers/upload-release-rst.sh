#/bin/sh
# cd ./BIT.MyCity
#ssh root@172.30.0.46
PREFIX='biz'
RPMBINDIR='/Users/hedgehog/rpmbuild/RPMS/noarch/'
LATESTBUILDFILE=$RPMBINDIR"latest-"$PREFIX

SSHFOLDER="root@172.30.0.46:/root/mycity_distr/"
# MYRPMNAME1=`readlink $RPMBINDIR"latest-"$PREFIX`
# MYRPMNAME2=`readlink $RPMBINDIR"di-latest-"$PREFIX`
MYRPMNAME1=`ls -ls $RPMBINDIR"latest-"$PREFIX | sed "s/.*latest-"$PREFIX" -> \.\///"`
MYRPMNAME2=`ls -ls $RPMBINDIR"di-latest-"$PREFIX | sed "s/.*di-latest-"$PREFIX" -> \.\///"`

echo 'file: '$MYRPMNAME1
echo 'file: '$MYRPMNAME2

ssh root@172.30.0.46 'rm -rf /root/mycity_distr/* ; mkdir /root/mycity_distr'
scp ./rpm-mycity-install.sh ./rpm-mycity-update.sh $RPMBINDIR$MYRPMNAME1 $RPMBINDIR$MYRPMNAME2 $SSHFOLDER
