#/bin/sh
# cd ./BIT.MyCity
#sudo route add 91.236.200.0/24 192.168.120.210
PREFIX='biz'
RPMBINDIR='/Users/hedgehog/rpmbuild/RPMS/noarch/'
LATESTBUILDFILE=$RPMBINDIR"latest-"$PREFIX

MYRPMNAME1=`readlink $RPMBINDIR"latest-"$PREFIX`
MYRPMNAME2=`ls -ls $RPMBINDIR"di-latest-"$PREFIX | sed "s/.*di-latest-"$PREFIX" -> \.\///"`
echo 'file: '$MYRPMNAME1
echo 'file: '$MYRPMNAME2

ssh root@91.236.200.180 'rm -rf /root/mycity/dist/'
ssh root@91.236.200.180 'mkdir /root/mycity/dist'
# rsync -avz ./BIT.MyCity/bin/Debug/netcoreapp3.1/publish/* root@91.236.200.180:/root/mycity/dist
# rsync -avz ./System/* root@91.236.200.180:/root/mycity/system
scp $MYRPMNAME1 root@91.236.200.180:/root/mycity/dist
scp $RPMBINDIR$MYRPMNAME2 root@91.236.200.180:/root/mycity/dist
