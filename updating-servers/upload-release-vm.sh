#/bin/sh
# cd ./BIT.MyCity
PREFIX='biz'
#RPMBINDIR='/Users/hedgehog/rpmbuild/RPMS/x86_64/'
RPMBINDIR='/Users/hedgehog/rpmbuild/RPMS/noarch/'
LATESTBUILDFILE=$RPMBINDIR"latest-"$PREFIX

#MYRPMNAME1=`ls -ls $RPMBINDIR"latest-"$PREFIX | sed "s/.*latest-"$PREFIX" -> \.\///"`
MYRPMNAME1=`readlink $RPMBINDIR"latest-"$PREFIX`
MYRPMNAME2=`ls -ls $RPMBINDIR"di-latest-"$PREFIX | sed "s/.*di-latest-"$PREFIX" -> \.\///"`
echo 'file: '$MYRPMNAME1
echo 'file: '$MYRPMNAME2

SSHUSERHOST='root@mycity.expert-doc.ru'
TARGETPATH=$SSHUSERHOST:/root/dist/

ssh $SSHUSERHOST 'rm -rf /root/dist/* ; mkdir /root/dist'
scp $MYRPMNAME1 $RPMBINDIR$MYRPMNAME2 $TARGETPATH
