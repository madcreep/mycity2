#/bin/sh
# cd ./BIT.MyCity
PREFIX='biz'
RPMBINDIR='/Users/hedgehog/rpmbuild/RPMS/x86_64/'
LATESTBUILDFILE=$RPMBINDIR"latest-"$PREFIX

MYRPMNAME1=`ls -ls $RPMBINDIR"latest-"$PREFIX | sed "s/.*latest-"$PREFIX" -> \.\///"`
MYRPMNAME2=`ls -ls $RPMBINDIR"di-latest-"$PREFIX | sed "s/.*di-latest-"$PREFIX" -> \.\///"`
echo 'file: '$MYRPMNAME1
echo 'file: '$MYRPMNAME2

SSHUSERHOST='epgadmin@91.236.200.185'
TARGETPATH=$SSHUSERHOST:/home/epgadmin/dist/

ssh $SSHUSERHOST 'rm -rf ~/dist/* ; mkdir ~/dist'
scp $RPMBINDIR$MYRPMNAME1 $RPMBINDIR$MYRPMNAME2 $TARGETPATH