using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using BIT.MyCity.Database;

namespace BIT.MyCity.Middleware
{
    public class TokenMiddleware
    {
        private readonly RequestDelegate _next;
        public TokenMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, [FromServices]UserManager<User> userManager)
        {
            if (!context.User.Identity.IsAuthenticated)
            {
                await _next.Invoke(context);

                return;
            }

            var token = context.Request.Headers["Authorization"].ToString();

            if (string.IsNullOrWhiteSpace(token))
            {
                await _next.Invoke(context);
            }
            else
            {
                var idClaim = context.User.Claims?.FirstOrDefault(el => el.Type == JwtRegisteredClaimNames.Jti);

                var user = await userManager.FindByIdAsync(idClaim?.Value);

                if (user == null)
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;

                    await context.Response.WriteAsync("Not found User");
                }

                var tokenParts = token.Split(' ');

                token = tokenParts.Length > 1 ? tokenParts[1] : tokenParts[0];

                var hasToken = await userManager.GetAuthenticationTokenAsync(user, "MyLoginProvider", "Authorization");

                if (hasToken == null || token != hasToken)
                {
                    context.Response.StatusCode = StatusCodes.Status401Unauthorized;

                    await context.Response.WriteAsync("Token is invalid");
                }
                else
                {
                    await _next.Invoke(context);
                }
            }
        }
    }
}
