using System;

namespace BIT.MyCity.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ChangeJournalEntityAttribute : Attribute
    {
        public ChangeJournalEntityAttribute(string friendlyName)
        {
            FriendlyName = friendlyName;
        }

        public string FriendlyName { get; }
    }
}
