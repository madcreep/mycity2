using System;

namespace BIT.MyCity.Attributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class MapNameEnumAttribute : Attribute
    {
        public MapNameEnumAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}
