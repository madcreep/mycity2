using System;
using System.Collections.Generic;
using System.Linq;
using BIT.MyCity.Database;
using BIT.MyCity.Helpers;
using GraphQL;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace BIT.MyCity.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ChangeJournalPropertyAttribute : Attribute
    {
        public ChangeJournalPropertyAttribute(string friendlyName, string referencePropertyName = null)
        {
            FriendlyName = friendlyName;

            ReferencePropertyName = referencePropertyName;
        }
        
        public string FriendlyName { get; }
        
        public string ReferencePropertyName { get; }

        public virtual string GetReferencePropertyCurrentValue(PropertyEntry el)
        {
            if (el.CurrentValue == null)
            {
                return "null";
            }

            if (!el.Metadata.IsForeignKey() || string.IsNullOrWhiteSpace(ReferencePropertyName))
            {
                return el.Metadata.PropertyInfo.PropertyType.IsEnum
                ? (el.CurrentValue as Enum).MapName()
                : el.CurrentValue?.ToString();
            }
            
            var containingForeignKeys = el.Metadata
                .GetContainingForeignKeys()
                .ToArray();

            if (containingForeignKeys.Length != 1)
            {
                return null;
            }

            var propertyName = containingForeignKeys
                .First()
                .DependentToPrincipal?.Name;

            if (string.IsNullOrWhiteSpace(propertyName))
            {
                return null;
            }

            var navigationEntry = el.EntityEntry.Navigation(propertyName);

            navigationEntry.Load();

            if (navigationEntry.Metadata.IsCollection)
            {
                return string.Join(";",
                    (navigationEntry.CurrentValue as IEnumerable<MemberEntry>)?
                    .Select(el => el.Metadata.PropertyInfo.PropertyType.IsEnum
                ? (el.GetPropertyValue(ReferencePropertyName) as Enum).MapName()
                : el.GetPropertyValue(ReferencePropertyName)?.ToString()));
            }

            return navigationEntry.Metadata.PropertyInfo.PropertyType.IsEnum
                ? (navigationEntry.CurrentValue as Enum).MapName()
                : navigationEntry
                    .CurrentValue?.GetPropertyValue(ReferencePropertyName)?.ToString();
        }
    }

    public class ChangeJournalAppealStatusAttribute : ChangeJournalPropertyAttribute
    {
        public ChangeJournalAppealStatusAttribute(string friendlyName)
            : base(friendlyName)
        { }

        public override string GetReferencePropertyCurrentValue(PropertyEntry entry)
        {
            if (!(entry?.EntityEntry.Context is DocumentsContext context))
            {
                return null;
            }

            var entryValue = entry.CurrentValue?.ToString();

            entry.EntityEntry.Navigation(nameof(Appeal.Subsystem)).Load();

            if (entry.EntityEntry.Navigation(nameof(Appeal.Subsystem)).CurrentValue is Subsystem subsystem)
            {
                return context.EnumVerbs
                    .SingleOrDefault(el => el.Group == "appealStatus"
                                           && el.SubsystemUID == subsystem.UID
                                           && el.Value == entryValue
                                           && !el.Deleted)?
                    .ShortName;
            }

            return "null";
        }
    }
}
