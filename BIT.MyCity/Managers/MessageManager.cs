using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Subsystems;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL.Utilities;
using log4net;
using Microsoft.EntityFrameworkCore;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.MyCity.Services;
using LinqKit;

namespace BIT.MyCity.Managers
{
    public class MessageManager
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(MessageManager));
        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        private readonly SettingsManager _sm;
        private readonly ElasticClient _elasticClient;
        private readonly GQLSubscriptionService _gqlSubscriptionService;

        public MessageManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
            _sm = _serviceProvider.GetRequiredService<SettingsManager>();
            try
            {
                _elasticClient = (ElasticClient)_serviceProvider.GetRequiredService<IElasticClient>();
            }
            catch
            {
                //ignore
            }
            _gqlSubscriptionService = _serviceProvider.GetRequiredService<GQLSubscriptionService>();
        }


        public async Task<Message> CreateDbAsync(Message message, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            /*
             ParentId,
             ParentType - обязательны и должны быть валидны
             */
            var user = await _um.CurrentUser();

            message.Author = user ?? throw new Exception($"Нужно авторизоваться");

            var parent = await message.GetParentAsync(
                _ctx,
                new List<string>
                {
                    nameof(Message),
                    nameof(Appeal),
                    nameof(Vote),
                    nameof(VoteRoot),
                    nameof(News)
                });

            if (parent == null)
            {
                throw new Exception("Parent not found!");
            }

            //await BaseConstants.ModifyIdsList(nameof(obj.AttachedFiles), obj, obj, _ctx.AttachedFiles, _ctx);
            await UpdateAttachedFilesIds(message, message);

            string ssUid = null;

            int level;

            switch (parent)
            {
                case Appeal appeal:
                    message.Appeal = appeal;

                    message.AppealId = appeal.Id;

                    ssUid = message.Appeal.Subsystem?.UID;

                    level = 1;

                    break;
                case Vote vote1:
                    {
                        message.Vote = vote1;

                        message.VoteId = vote1.Id;

                        var vote = await _ctx.Votes
                            .Where(el => el.Id == message.ParentId)
                            .Include(el => el.VoteRoot)
                            .FirstOrDefaultAsync();

                        if (message.Vote.CommentsAllowed.HasValue && message.Vote.CommentsAllowed.Value == false)
                        {
                            throw new Exception($"Comments not allowed on Vote {vote.Id}");
                        }

                        if (message.Vote.VoteRoot == null)
                        {
                            await _ctx.Entry(message).Reference(el => el.VoteRoot).LoadAsync();
                        }

                        if (message.Vote?.VoteRoot != null && vote.VoteRoot.CommentsAllowed == false)
                        {
                            throw new Exception($"Comments not allowed on VoteRoot {vote.VoteRoot.Id}");
                        }

                        ssUid = SSUID.vote;

                        level = 1;

                        break;
                    }
                case VoteRoot voteRoot:
                    {
                        message.VoteRoot = voteRoot;

                        message.VoteRootId = voteRoot.Id;

                        if (message.VoteRoot.CommentsAllowed == false)
                        {
                            throw new Exception($"Comments not allowed on VoteRoot {message.VoteRoot.Id}");
                        }

                        ssUid = SSUID.vote;

                        level = 1;

                        break;
                    }
                case News news:
                    {
                        message.News = news;

                        message.NewsId = news.Id;

                        if (message.News.CommentsAllowed == false)
                        {
                            throw new Exception($"Comments not allowed on News {message.News.Id}");
                        }

                        if (message.News != null && message.News.State != NewsStateEnum.PUBLISHED)
                        {
                            throw new Exception($"Comments not allowed on News not in {NewsStateEnum.PUBLISHED} state {message.News.Id}");
                        }

                        ssUid = SSUID.news;

                        level = 1;

                        break;
                    }
                case Message _:
                    {
                        var mess = await _ctx.Messages
                            .Where(el => el.Id == ((Message)parent).Id)
                            .Include(el => el.Appeal)
                            .ThenInclude(el=>el.Subsystem)
                            .Include(el => el.VoteRoot)
                            .Include(el => el.Vote)
                            .Include(el => el.News)
                            .FirstOrDefaultAsync();

                        message.Appeal = mess.Appeal;

                        if (mess.Appeal != null)
                        {
                            message.AppealId = mess.Appeal.Id;
                            ssUid = message.Appeal.Subsystem?.UID;
                        }

                        message.VoteRoot = mess.VoteRoot;

                        if (mess.VoteRoot != null)
                        {
                            message.VoteRootId = mess.VoteRoot.Id;
                            ssUid = SSUID.vote;
                        }

                        message.Vote = mess.Vote;

                        if (mess.Vote != null)
                        {
                            message.VoteId = mess.Vote.Id;
                            ssUid = SSUID.vote;
                        }

                        message.News = mess.News;

                        if (mess.News != null)
                        {
                            message.NewsId = mess.News.Id;
                            ssUid = SSUID.news;
                        }

                        level = 2;

                        break;
                    }
                default:
                    throw new Exception("Неизвестный родитель сообщения");
            }

            var flags = await message.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()?.Claims, _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Create))
            {
                throw new Exception($"Вам это нельзя");
            }

            if (message.Approved != ApprovedEnum.UNDEFINED && !flags.HasFlag(BaseConstants.AllowedActionFlags.Moderate))
            {
                throw new Exception($"Управление публикацией разрешено только модератору");
            }

            if (ssUid != null && updateKeys != null && !updateKeys.ContainsKey(nameof(Message.SubscribeToAnswers).ToJsonFormat()))
            {
                message.SubscribeToAnswers = _sm.SettingValue<SubscribeToAnswersEnum>(level == 1
                    ? $"subsystem.{ssUid}.subscribe_to_answers."
                    : $"subsystem.{ssUid}.subscribe_to_answers_to_answers.");
            }

            _ctx.Messages.Add(message);

            message.ViewSet(true, message.Author);

            await _ctx.SaveWeightAsync(message);

            await _ctx.SaveDueAsync(message, parent);

            if (_elasticClient != null)
            {
                await _elasticClient.IndexDocumentAsync(new ESMessage(message));
            }

            _ctx.ProtocolRecords.Add(new ProtocolRecord
            {
                User = await _um.CurrentUser(),
                Message = message,
                Subsystem = message.Appeal?.Subsystem,
                Action = $"Message Created {message.Id}, parent {message.ParentType}:{message.ParentId}"
            });

            await _ctx.SaveChangesAsync();

            _gqlSubscriptionService?.OnMessageChanged(message);

            await SendEmail(message, parent as Message);

            return await GetMessageAsync(message.Id, requestedFields);
        }


        public async Task<Message> GetMessageAsync(long id)
        {
            return await _ctx.Messages
                .Where(el => el.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<Pagination<Message>> SearchMessagesAsync(string query, QueryOptions options)
        {
            if (_elasticClient == null)
            {
                return null;
            }
            options.RequestedFields.TryGetValue(nameof(Pagination<Message>.Items).ToJsonFormat(), out var items);

            var result = new Pagination<Message>();

            query ??= "";

            var skip = options.Range?.Skip ?? 0;
            var take = options.Range?.Take ?? 100;

            var response = await _elasticClient.SearchAsync<ESMessage>(
                s => s.Query(q => q.QueryString(d => d.Query(query)))
                    .From(skip)
                    .Size(take)
            );

            if (!response.IsValid)
            {
                return result;
            }

            result.Total = response.Total;

            if (!options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                return result;
            }

            var ids = response.Documents
                .Select(el => el.Id)
                .ToList();

            if (ids.Count > 0)
            {
                result.Items = await QueryableIncludeExtensions.IncludeByNotMappedFields(_ctx.Messages
                        .Where(el => ids.Contains(el.Id))
                        .IncludeByFields(items), items)
                    .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                    .ToListAsync();
            }

            return result;
        }

        public async Task<Pagination<Message>> GetMessagesAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<Message>.Items).ToJsonFormat(), out var items);

            var request = _ctx.Messages
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm)
                .FiltersBy(options.Filters, options.WithDeleted);

            var result = new Pagination<Message>();

            if (options.RequestedFields.ContainsKey(nameof(Pagination<Message>.Total).ToJsonFormat()))
            {
                result.Total = await request.LongCountAsync();
            }

            request = request.GetRange(options.Range?.Take, options.Range?.Skip);

            if (!options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                return result;
            }

            request = request
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy);

            result.Items = await request.ToListAsync();

            return result;
        }

        public async Task<Pagination<Subs2DueMessages>> GetMessageSubscribesAsync(QueryOptions options)
        {
            var userId = _um.ClaimsPrincipalId();
            if (userId == null)
            {
                throw new Exception($"Нужно авторизоваться");
            }
            var obj = _ctx.Subs2DueMessages
                .Where(el => el.UserId == userId.Value)
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(Subs2DueMessages.Due), options.Range?.OrderBy)
                ;
            return await BaseConstants.MakePaginationResult(obj, options);
        }

        public async Task<Pagination<Message>> GetMessagesAsync(List<long> ids, GraphQlKeyDictionary requestedFields)
        {
            requestedFields.TryGetValue(nameof(Pagination<Message>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.Messages
                .Where(el => ids.Contains(el.Id))
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                ;
            var result = new Pagination<Message>();
            if (requestedFields.ContainsKey(nameof(Pagination<Message>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            if (requestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }

        public async Task<List<Message>> GetAllMessagesAsync(GraphQlKeyDictionary requestedFields, List<GraphQlQueryFilter> filters, SelectionRange range)
        {
            var obj = await _ctx.Messages
                .MakeQueryEntity(requestedFields, filters, range)
                .ToListAsync();
            return obj;
        }

        private async Task<Pagination<Message>> GetLinkedMessages(BaseIdEntity entity, QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<Message>.Items).ToJsonFormat(), out var items);
            var result = new Pagination<Message>();
            var ml = new List<Message>();


            if (entity != null)
            {
                if (entity.GetType() == typeof(Message))
                {
                    ml.AddRange(await _ctx.Messages
                        .Where(el => el.Due.StartsWith(((Message)entity).Due))
                        .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm)
                        .FiltersBy(options.Filters, options.WithDeleted)
                        .IncludeByFields(items)
                        .IncludeByNotMappedFields(items)
                        .ToListAsync() ?? new List<Message>());
                }
                else if (entity.GetType() == typeof(Appeal))
                {
                    ml.AddRange(await _ctx.Messages
                        .Where(el => el.Appeal.Id == entity.Id)
                        .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm)
                        .FiltersBy(options.Filters, options.WithDeleted)
                        .IncludeByFields(items)
                        .IncludeByNotMappedFields(items)
                        .ToListAsync() ?? new List<Message>());
                }
                else if (entity.GetType() == typeof(Vote))
                {
                    ml.AddRange(await _ctx.Messages
                        .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                        .Where(el => el.Vote.Id == entity.Id)
                        .FiltersBy(options.Filters, options.WithDeleted)
                        .IncludeByFields(items)
                        .IncludeByNotMappedFields(items)
                        .ToListAsync() ?? new List<Message>());
                }
                else if (entity.GetType() == typeof(VoteRoot))
                {
                    ml.AddRange(await _ctx.Messages
                        .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                        .Where(el => el.VoteRoot.Id == entity.Id)
                        .FiltersBy(options.Filters, options.WithDeleted)
                        .IncludeByFields(items)
                        .IncludeByNotMappedFields(items)
                        .ToListAsync() ?? new List<Message>());
                }
                else if (entity.GetType() == typeof(News))
                {
                    ml.AddRange(await _ctx.Messages
                        .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                        .Where(el => el.News.Id == entity.Id)
                        .FiltersBy(options.Filters, options.WithDeleted)
                        .IncludeByFields(items)
                        .IncludeByNotMappedFields(items)
                        .ToListAsync() ?? new List<Message>());
                }
            }
            if (options.RequestedFields.ContainsKey(nameof(Pagination<Message>.Total).ToJsonFormat()))
            {
                result.Total = ml.Count();
            }
            if (options.Range?.Skip != null)
            {
                ml = ml.Skip(options.Range.Skip.Value).ToList();
            }
            if (options.Range?.Take != null)
            {
                ml = ml.Take(options.Range.Take.Value).ToList();
            }
            result.Items = ml;

            return result;
        }

        public async Task<Pagination<Message>> GetEntityMessageListAsync<T>(long id, QueryOptions options, DbSet<T> db) where T : BaseIdEntity
        {
            var obj = await db
                .Where(el => el.Id == id)
                .FirstOrDefaultAsync();
            return await GetLinkedMessages(obj, options);
        }
        public async Task<Pagination<Message>> GetMessageListAsync(long id, string type, QueryOptions options)
        {
            var entity = await BaseConstants.GetEntityAsync(id, type, _ctx);
            if (entity != null)
            {
                return await GetLinkedMessages((BaseIdEntity)entity, options);
            }

            throw new Exception($"Entity '{type}':{id} not found");
        }
        private async Task<Message> GetMessageAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            return await _ctx.Messages
                    .Where(el => el.Id == id)
                    .IncludeByFields(requestedFields)
                    .IncludeByNotMappedFields(requestedFields)
                    .FirstOrDefaultAsync();
        }

        public async Task<Message> UpdateDbAsync(long id, Message src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var userId = _um.ClaimsPrincipalId();
            if (userId == null)
            {
                throw new Exception($"Нужно авторизоваться");
            }
            var dest = await _ctx.Messages
                .Where(el => el.Id == id)
                .Include(el => el.AttachedFilesToMessages)
                .ThenInclude(el => el.AttachedFile)
                .Include(el => el.Appeal).ThenInclude(el => el.Subsystem)
                .Include(el => el.VoteRoot)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"Message {id} not found");
            }
            if (!(await dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()?.Claims, _sm, _ctx)).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }
            Message old = (Message)dest.Clone();
            //await BaseConstants.ModifyIdsList(nameof(src.AttachedFiles), src, dest, _ctx.AttachedFiles, _ctx, el => el.AttachedFiles);
            await UpdateAttachedFilesIds(src, dest);
            BaseConstants.UpdateEntityByFieldsList(dest, src, updateKeys);
            await _ctx.SaveChangesAsync();
            if (_elasticClient != null)
            {
                var esMessage = new ESMessage(dest);
                await _elasticClient.UpdateAsync<ESMessage>(esMessage, u => u.Doc(esMessage));
            }
            var diff = old.MemberwiseDiff(dest);
            if (!string.IsNullOrEmpty(diff))
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = await _um.CurrentUser(),
                    Message = dest,
                    Subsystem = dest.Appeal?.Subsystem,
                    Action = $"Message changed {dest.Id}, parent {dest.ParentType}:{dest.ParentId}",
                    Data = diff
                });
                await _ctx.SaveChangesAsync();
            }
            _gqlSubscriptionService?.OnMessageChanged(dest);
            return await GetMessageAsync(id, requestedFields);
        }
        public async Task<Pagination<Message>> ApproveAsync(long id, MessageApprove src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var userId = _um.ClaimsPrincipalId();
            if (userId == null)
            {
                throw new Exception($"Нужно авторизоваться");
            }
            var dest = await _ctx.Messages
                .Where(el => el.Id == id)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"MessageId {id} not found");
            }
            if (!(await dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()?.Claims, _sm, _ctx)).HasFlag(BaseConstants.AllowedActionFlags.Moderate))
            {
                throw new Exception($"Вам это нельзя");
            }
            
            var messList = new List<Message>();
            var trn = await _ctx.Database.BeginTransactionAsync();
            try
            {
                if (src.ApprovedEnum.HasValue && src.ApprovedEnum.Value == ApprovedEnum.ACCEPTED)
                {
                    /*акцептим всех предков*/
                    var dueList = new List<string>();
                    string due = "";
                    foreach (var d in dest.Due.Split('.'))
                    {
                        due += d + ".";
                        dueList.Add(due);
                    }
                    messList = await _ctx.Messages.Where(el => !el.Deleted && dueList.Contains(el.Due)).ToListAsync();
                    var err = messList.Where(el => el.Approved != ApprovedEnum.ACCEPTED && el.Approved != ApprovedEnum.UNDEFINED && el.Due != dest.Due).ToList();
                    await ApproveSub1Async(src, messList, err);
                }
                else if (src.ApprovedEnum.HasValue && src.ApprovedEnum.Value == ApprovedEnum.REJECTED)
                {
                    /*REJECTED всех потомков*/
                    messList = await _ctx.Messages.Where(el => !el.Deleted && el.Due.StartsWith(dest.Due) && el.Kind != MessageKindEnum.ReasonToParentMessage).ToListAsync();
                    var err = messList.Where(el => el.Approved != ApprovedEnum.REJECTED && el.Approved != ApprovedEnum.UNDEFINED && el.Due != dest.Due).ToList();
                    await ApproveSub1Async(src, messList, err);
                }
                else
                {
                    await ApproveSub1Async(src, new List<Message> { dest }, new List<Message>());
                }
                if (messList.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(src.Reason))
                    {
                        var nm = await dest.MakeChild(_ctx);
                        nm.Text = src.Reason;
                        nm.Kind = MessageKindEnum.ReasonToParentMessage;
                        nm.Approved = ApprovedEnum.ACCEPTED;
                        nm.ModerationStage = ModerationStageEnum.DONE;
                        await CreateDbAsync(nm, null, null);
                        messList.Add(nm);
                    }
                }
                await _ctx.SaveChangesAsync();
            }
            catch
            {
                await trn.RollbackAsync();
                throw;
            }
            await trn.CommitAsync();
            return await GetMessagesAsync(messList.Select(el => el.Id).ToList(), requestedFields);
        }

        private async Task ApproveSub1Async(MessageApprove src, List<Message> messList, List<Message> err)
        {
            if (err.Count > 0)
            {
                var errors = err
                    .Where(er => src.ApprovedEnum != null)
                    .Aggregate("", (current, er) => current + $"Сообщение {er.Id} нельзя сделать {(src.ApprovedEnum ?? ApprovedEnum.UNDEFINED)}, так как оно уже {er.Approved}\n");
                throw new Exception(errors);
            }

            foreach (var mess in messList)
            {
                var old = (Message)mess.Clone();
                if (src.ApprovedEnum != null) mess.Approved = src.ApprovedEnum.Value;
                if (src.ModerationStage.HasValue)
                {
                    mess.ModerationStage = src.ModerationStage.Value;
                }
                if (src.Public_.HasValue)
                {
                    mess.Public_ = src.Public_.Value;
                }
                var diff = old.MemberwiseDiff(mess);
                if (!string.IsNullOrEmpty(diff))
                {
                    _ctx.ProtocolRecords.Add(new ProtocolRecord
                    {
                        User = await _um.CurrentUser(),
                        Message = mess,
                        Subsystem = mess.Appeal?.Subsystem,
                        Action = $"Message moderated {mess.Id}, parent {mess.ParentType}:{mess.ParentId}",
                        Data = diff
                    });
                }
            }
        }

        public async Task<Message> DeleteAsync(long id, MessageDelete src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var userId = _um.ClaimsPrincipalId();
            if (userId == null)
            {
                throw new Exception($"Нужно авторизоваться");
            }
            var dest = await _ctx.Messages
                .Where(el => el.Id == id)
                .Include(message => message.Appeal)
                .ThenInclude(appeal => appeal.Subsystem)
                .FirstOrDefaultAsync();
            if (dest == null || dest.Deleted)
            {
                throw new Exception($"MessageId {id} not found");
            }
            if (!(await dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()?.Claims, _sm, _ctx)).HasFlag(BaseConstants.AllowedActionFlags.Moderate))
            {
                throw new Exception($"Вам это нельзя");
            }
            Message nm = null;
            var trn = await _ctx.Database.BeginTransactionAsync();
            try
            {
                if (!string.IsNullOrWhiteSpace(src.Reason))
                {
                    nm = await dest.MakeChild(_ctx);
                    nm.Text = src.Reason;
                    await CreateDbAsync(nm, null, null);
                }
                dest.Deleted = true;
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = await _um.CurrentUser(),
                    Message = dest,
                    Subsystem = dest.Appeal?.Subsystem,
                    Action = $"Message deleted {dest.Id}, parent {dest.ParentType}:{dest.ParentId}"
                });
                await _ctx.SaveChangesAsync();
            }
            catch
            {
                await trn.RollbackAsync();
                throw;
            }
            await trn.CommitAsync();

            return nm == null ? null : await GetMessageAsync(nm.Id, requestedFields);
        }

        //private async Task<List<User>> UsersSubscribed(Message mess)
        //{
        //    var rootType = BaseConstants.GetMessageRootType(mess);
        //    var dl = mess.Due.Split('.', StringSplitOptions.RemoveEmptyEntries);
        //    var gl = new List<string>();
        //    var ce = string.Empty;
        //    foreach (var a in dl)
        //    {
        //        ce = ce + a + ".";
        //        gl.Add(ce);
        //    }
        //    var ul = await _ctx.Subs2DueMessages
        //        .Where(el => el.RootType == rootType && gl.Contains(el.Due))
        //        .Select(el => el.UserId)
        //        .Distinct()
        //        .ToListAsync();
        //    var result = new List<User>();
        //    foreach (var u in ul)
        //    {
        //        var user = await _ctx.Users
        //            .Where(el => !el.Disconnected && el.Id == u)
        //            .Include(el => el.AllClaims)
        //            .FirstOrDefaultAsync();
        //        if (user != null)
        //        {
        //            var flags = await mess.GetAllowedActions(user.Id, user.AllClaims, _sm, _ctx);
        //            if (flags.HasFlag(BaseConstants.AllowedActionFlags.Read))
        //            {
        //                result.Add(user);
        //            }
        //        }
        //    }
        //    return result;
        //}

        public async Task<MessageSubscribeGet> MessageSubscribeAsync(MessageSubscribeSet src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var user = await _um.CurrentUser();
            if (user == null)
            {
                throw new Exception($"Нужно авторизоваться");
            }
            var e = await BaseConstants.GetEntityAsync(src.EntityId, src.EntityType, _ctx);
            if (e == null)
            {
                throw new Exception($"Объект {src.EntityType}:{src.EntityId} не найден");
            }
            if (!(e is Message) && !e.GetType().GetInterfaces().Contains(typeof(IHasMessages)))
            {
                throw new Exception($"Объект {src.EntityType} не поддерживает комментарии");
            }
            var dest = new Subs2DueMessages { Level = src.Level, User = user };
            if (e is Message oMessage)
            {
                dest.Due = oMessage.Due;
                dest.RootType = BaseConstants.GetMessageRootType(oMessage);
            }
            else
            {
                dest.RootType = ((IBaseIdEntity)e).Type;
                dest.Due = BaseConstants.Int2Due(((IBaseIdEntity)e).Id);
            }
            var flags = await BaseConstants.GetAllowedActionsAsync(e, _um, _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Read))
            {
                throw new Exception($"Вам это нельзя");
            }
            var level2 = dest.Due.Count(el => el == '.');
            var needToCreate = true;
            var dest1 = dest;
            var es = await _ctx.Subs2DueMessages
                .Where(el => el.RootType == dest1.RootType && el.UserId == dest1.User.Id)
                .ToListAsync();
            foreach (var a in es)
            {
                if (dest.Due.StartsWith(a.Due))
                {
                    //уже есть подписка равная или ближе к корню
                    var level1 = a.Due.Count(el => el == '.');
                    if (level1 + a.Level >= level2 + dest.Level)
                    {
                        //старая подписка полностью покрывает текущую. Изменения не требуются.
                        needToCreate = false;
                        dest = a;
                        break;
                    }
                    else if (level1 + a.Level < level2)
                    {
                        //старая не достает до новой. Нужно создать подписку
                    }
                    else if (level1 + a.Level >= level2 && level1 + a.Level < level2 + dest.Level)
                    {
                        //новая расширяет старую. Надо внести изменения в старую
                        a.Level = level2 + dest.Level - level1;
                        needToCreate = false;
                        dest = a;
                        break;
                    }
                }
                else if (a.Due.StartsWith(dest.Due))
                {
                    //есть подписка глубже, чем текущая
                    var level1 = a.Due.Where(el => el == '.').Count();
                    if (level2 + dest.Level >= level1 + a.Level)
                    {
                        //новая полностью перекрывает старую. Старую удаляем
                        _ctx.Subs2DueMessages.Remove(a);
                    }
                    else if (level2 + dest.Level >= level1 && level2 + dest.Level < level1 + a.Level)
                    {
                        //новая достает до старой, но не перекрывает ее до конца. Новую надо продлить до конца старой, а старую - удалить
                        dest.Level = level1 + a.Level - level2;
                        _ctx.Subs2DueMessages.Remove(a);
                    }
                }
            }
            if (needToCreate)
            {
                await _ctx.Subs2DueMessages.AddAsync(dest);
            }
            await _ctx.SaveChangesAsync();
            var result = new MessageSubscribeGet { RootType = dest.RootType, Level = dest.Level, Due = dest.Due };
            return result;
        }
        public async Task<string> MessageUnSubscribeAsync(MessageUnSubscribe src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            User user = await _um.CurrentUser();
            if (user == null)
            {
                throw new Exception($"Нужно авторизоваться");
            }
            var q = _ctx.Subs2DueMessages.Where(el => el.UserId == user.Id);
            if (updateKeys.ContainsKey(nameof(src.RootType).ToJsonFormat()))
            {
                q = q.Where(el => el.RootType == src.RootType);
            }
            if (updateKeys.ContainsKey(nameof(src.Due).ToJsonFormat()))
            {
                q = q.Where(el => el.Due == src.Due);
            }
            if (updateKeys.ContainsKey(nameof(src.EntityType).ToJsonFormat()) && updateKeys.ContainsKey(nameof(src.EntityId).ToJsonFormat()))
            {
                var e = await BaseConstants.GetEntityAsync(src.EntityId, src.EntityType, _ctx);
                if (e == null)
                {
                    throw new Exception($"Объект {src.EntityType}:{src.EntityId} не найден");
                }
                var dest = new Subs2DueMessages();
                if (e is Message)
                {
                    var oMessage = (Message)e;
                    dest.Due = oMessage.Due;
                    dest.RootType = BaseConstants.GetMessageRootType(oMessage);
                }
                else
                {
                    dest.RootType = ((IBaseIdEntity)e).Type;
                    dest.Due = BaseConstants.Int2Due(((IBaseIdEntity)e).Id);
                }
                q = q.Where(el => el.RootType == dest.RootType && el.Due == dest.Due);
            }
            var ql = await q.ToListAsync();
            if (ql.Any())
            {
                _ctx.Subs2DueMessages.RemoveRange(ql);
            }
            var result = $"Removed {ql.Count} entries";
            await _ctx.SaveChangesAsync();
            return result;
        }

        public async Task<LikeReport> LikeAsync(LikeRequest like)
        {
            var user = await _um.CurrentUser();
            if (user == null && like.Liked != LikeRequestEnum.NONE)
            {
                throw new Exception($"Аноним не может лайкать");
            }
            var obj = await _ctx.Messages
                .Where(el => el.Id == like.Id)
                .Include(el => el.UserLikeMessage)
                .Include(message => message.Appeal)
                .ThenInclude(appeal => appeal.Subsystem)
                .FirstOrDefaultAsync();
            if (obj == null)
            {
                throw new Exception($"MessageId {like.Id} not found");
            }
            if (user != null && like.Liked != LikeRequestEnum.NONE && obj.AuthorId == user.Id)
            {
                throw new Exception($"Нельзя лайкать свои обращения");
            }
            var likesCount = obj.LikesCount;
            var dislikesCount = obj.DislikesCount;
            var liked = obj.LikeSet(like.Liked, user);
            if (likesCount != obj.LikesCount || dislikesCount != obj.DislikesCount)
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = user,
                    Message = obj,
                    Subsystem = obj.Appeal?.Subsystem,
                    Action = $"{obj.Type}:{obj.Id} Like status changed"
                });
                await _ctx.SaveChangesAsync();
            }
            return new LikeReport { Liked = liked, LikeCount = obj.LikesCount, DislikeCount = obj.DislikesCount };
        }
        public async Task<ViewReport> ViewAsync(ViewRequest view)
        {
            var user = await _um.CurrentUser();
            if (user == null)
            {
                throw new Exception($"Аноним не может отмечать просмотренность");
            }
            var obj = await _ctx.Messages
                .Where(el => el.Id == view.Id)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm)
                .Include(el => el.UserViewMessage)
                .FirstOrDefaultAsync();
            if (obj == null)
            {
                throw new Exception($"{view.Type}:{view.Id} not found");
            }
            var viewCount = obj.ViewsCount;
            var viewed = obj.ViewSet(view.Viewed, user);
            if (viewCount != obj.ViewsCount)
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = user,
                    Message = obj,
                    Subsystem = obj.Appeal?.Subsystem,
                    Action = $"{obj.Type}:{obj.Id} View status changed"
                });
                await _ctx.SaveChangesAsync();
            }
            return new ViewReport { Viewed = viewed.Viewed, ViewCount = obj.ViewsCount, CreatedAt = viewed.CreatedAt, UpdatedAt = viewed.UpdatedAt };
        }

        public async Task<RegardReport> RegardAsync(RegardRequest req)
        {
            if (req.Regard < 0 || req.Regard > 10)
            {
                throw new Exception($"Оценка должна находитться в диапазоне 0..10");
            }
            var user = await _um.CurrentUser();
            if (user == null)
            {
                throw new Exception($"Аноним не может ставить оценку");
            }
            var obj = await _ctx.Messages
                .Where(el => el.Id == req.Id)
                .Include(el => el.UserRegardMessage)
                .Include(el => el.Appeal)
                .ThenInclude(appeal => appeal.Subsystem)
                .FirstOrDefaultAsync();
            if (obj == null)
            {
                throw new Exception($"{req.Type}:{req.Id} not found");
            }
            if (obj.Appeal == null || !obj.AppealExecutorId.HasValue)
            {
                throw new Exception($"Оценки можно ставить только отчету исполнителя обращения");
            }
            if (obj.Appeal.RegardMode == RegardModeEnum.DISABLED)
            {
                throw new Exception($"Ответам на обращение {obj.AppealId} запрещено ставить оценки");
            }
            if (obj.Appeal.RegardMode == RegardModeEnum.APPEALAUTHOR && obj.Appeal.AuthorId != user.Id)
            {
                throw new Exception($"Оценку может ставить только автор обращения {obj.AppealId}");
            }
            if (obj.UserRegardMessage.Any(el => el.UserId == user.Id))
            {
                throw new Exception($"Вы уже поставили свою оценку");
            }
            var oldCount = obj.RegardsCount;
            var oldRegard = obj.UserRegardMessage?.Where(el => el.UserId == user.Id).FirstOrDefault()?.Regard;
            var regard = obj.RegardSet(req.Regard, user);
            if (oldCount != obj.RegardsCount || oldRegard != regard)
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = user,
                    Message = obj,
                    Subsystem = obj.Appeal?.Subsystem,
                    Action = $"{obj.Type}:{obj.Id} Regard status changed"
                });
                await _ctx.SaveChangesAsync();
            }
            return new RegardReport { Regard = regard, RegardCount = obj.RegardsCount };
        }

        //public event ObjectChangedHandler MessageChanged;

        //private void OnMessageChanged(Message changedMessage)
        //{
        //    ThreadPool.QueueUserWorkItem((s) =>
        //    {
        //        MessageChanged?.Invoke(
        //            this,
        //            new ObjectChangedArgs(s));

        //    }, changedMessage);
        //}

        private async Task SendEmail(Message message, Message parent)
        {
            if (parent != null)
            {
                await SendMessageToMessageEmail(message, parent);

                return;
            }

            var subsystemService = _serviceProvider.GetRequiredService<SubsystemService>();

            var subsystems = subsystemService.Subsystems;

            if (subsystems == null)
            {
                _log.Warn("При отправке Email о Message - список подсистем пуст");

                return;
            }

            var taskArray = subsystems
                .Where(el => el.IsEnable)
                .Select(subsystem => subsystem.SendNewMessageEmail(message))
                .ToArray();

            if (!taskArray.Any())
            {
                _log.Warn("При отправке Email о Message - список задач на отправку пуст");

                return;
            }

            Task.WaitAll(taskArray);

            //if (message.NewsId.HasValue)
            //{
            //    var subsystemService = _serviceProvider.GetRequiredService<SubsystemService>();

            //    var subsystem = subsystemService.Subsystem("news");

            //    await subsystem.SendNewMessageEmail(message);

            //    return;
            //}

            //if (message.VoteId.HasValue)
            //{
            //    var subsystemService = _serviceProvider.GetRequiredService<SubsystemService>();

            //    var subsystem = subsystemService.Subsystem("vote");

            //    await subsystem.SendNewMessageEmail(message);

            //    return;
            //}

            //if (message.AppealId.HasValue)
            //{
            //    var appeal = message.Appeal
            //                 ?? await _ctx.Appeals
            //                     .FirstOrDefaultAsync(el => el.Id == message.AppealId.Value);

            //    var subsystemService = _serviceProvider.GetRequiredService<SubsystemService>();

            //    var subsystemOrganization = subsystemService.Subsystem("organizations");

            //    var subsystemCitizen = subsystemService.Subsystem("citizens");

            //    var tasks = new List<Task>();

            //    tasks.Add(subsystemOrganization.SendNewMessageEmail(message));

            //    tasks.Add(subsystemCitizen.SendNewMessageEmail(message));

            //    Task.WaitAll(tasks.ToArray());

            //    return;


            //}
        }

        private async Task SendMessageToMessageEmail(Message message, Message parent)
        {
            if (parent.SubscribeToAnswers == SubscribeToAnswersEnum.Not_Subscribed)
            {
                return;
            }

            if (parent.Author == null)
            {
                parent.Author = await _ctx.Users
                    .FirstOrDefaultAsync(el => el.Id == parent.AuthorId);
            }

            if (string.IsNullOrWhiteSpace(parent.Author?.Email))
            {
                return;
            }

            var mailMessage = new MailMessage
            {
                Email = parent.Author.Email,
                Subject = $"Сообщение от {parent.CreatedAt:dd.MM.yyyy}",
                TemplateKey = "messageToMessage",
                AddingEntity = new List<EmailEntityMetadata>
                {
                    EmailEntityMetadata.Generate(_ctx, message),
                    EmailEntityMetadata.Generate(_ctx, parent, "ParentMessage")
                },
                TemplateModel = new
                {
                    ParentMessage = parent
                },
                FilesIds = message.AttachedFiles?
                    .Select(el => el.Id)
                    .ToArray()
            };

            var emailService = _serviceProvider.GetRequiredService<MailService>();

            await emailService.SendEmailAsync(mailMessage);
        }

        //private async Task SendMessageToSubscribedEmail(Message message, Message parent)
        //{
        //    var emailService = _serviceProvider.GetRequiredService<MailService>();

        //    var subscribedUsers = (await UsersSubscribed(message))
        //        .Where(user => !string.IsNullOrWhiteSpace(user.Email))
        //        .ToArray();

        //    if (!subscribedUsers.Any())
        //    {
        //        return;
        //    }

        //    var mailMessage = new MailMessage
        //    {
        //        Email = parent.Author.Email,
        //        Subject = $"Сообщение от {parent.CreatedAt:dd.MM.yyyy}",
        //        TemplateKey = "messageToMessageForSubscribed",
        //        AddingEntity = new List<EmailEntityMetadata>
        //        {
        //            EmailEntityMetadata.Generate(_ctx, message)
        //        },
        //        TemplateModel = new
        //        {
        //            ParentMessage = parent
        //        },
        //        FilesIds = message.AttachedFiles?
        //            .Select(el => el.Id)
        //            .ToArray()
        //    };

        //    foreach (var user in subscribedUsers)
        //    {
        //        mailMessage.AddingEntity = new List<EmailEntityMetadata>
        //        {
        //            EmailEntityMetadata.Generate(_ctx, user),
        //            EmailEntityMetadata.Generate(_ctx, message)
        //        };

        //        await emailService.SendEmailAsync(mailMessage);
        //    }
        //}

        private async Task UpdateAttachedFilesIds(Message srcMessage, Message destMessage)
        {
            if (srcMessage.AttachedFilesIds != null)
            {
                destMessage.AttachedFilesToMessages ??= new List<AttachedFilesToMessages>();

                var files = (from s in srcMessage.AttachedFilesIds
                            join d in destMessage.AttachedFilesToMessages on s equals d.AttachedFileId
                                into dd
                            from ddd in dd.DefaultIfEmpty()
                            select new { src = s, dest = ddd })
                    .ToArray();

                destMessage.AttachedFilesToMessages.Clear();

                destMessage.AttachedFilesToMessages.AddRange(
                    files
                        .Where(el => el.dest != null)
                        .Select(el => el.dest));

                var addIds = files
                    .Where(el => el.dest == null)
                    .Select(el => el!.src)
                    .ToArray();

                var attachedFiles = await _ctx.AttachedFiles
                    .Where(el => addIds.Contains(el.Id))
                    .ToArrayAsync();
                destMessage.AttachedFilesToMessages.AddRange(attachedFiles
                    .Select(el => new AttachedFilesToMessages { AttachedFile = el }));
            }
        }
    }



    public static class MessageExtensions
    {
        public static IQueryable<Message> IncludeByNotMappedFields(this IQueryable<Message> obj, GraphQlKeyDictionary fields)
        {
            if (fields == null || obj == null)
            {
                return obj;
            }

            if (fields.ContainsKey(nameof(Message.AttachedFiles).ToJsonFormat()))
            {
                obj = obj.Include(el => el.AttachedFilesToMessages).ThenInclude(el => el.AttachedFile);
            }
            if (fields.ContainsKey(nameof(Message.UserLikeMessage).ToJsonFormat()) || fields.ContainsKey(nameof(Message.LikeWIL).ToJsonFormat()))
            {
                obj = obj.Include(el => el.UserLikeMessage).ThenInclude(el => el.User);
            }
            if (fields.ContainsKey(nameof(Message.UsersViewed).ToJsonFormat()) || fields.ContainsKey(nameof(Message.ViewWIV).ToJsonFormat()))
            {
                obj = obj.Include(el => el.UserViewMessage).ThenInclude(el => el.User);
            }
            if (fields.ContainsKey(nameof(Message.UsersRegard).ToJsonFormat()) || fields.ContainsKey(nameof(Message.RegardWIR).ToJsonFormat()))
            {
                obj = obj.Include(el => el.UserRegardMessage).ThenInclude(el => el.User);
            }
            return obj;
        }

        public static IQueryable<Message> FilterByReadAllowed(this IQueryable<Message> main, long? userId, ClaimsPrincipal claimsPrincipal, SettingsManager sm)
        {
            var predicate = PredicateBuilder.New<Message>();

            var moderatorInPredicate = PredicateFilterByReadAllowedSubsystemRights(userId, claimsPrincipal, sm);

            predicate = predicate.And(moderatorInPredicate);

            return main.Where(predicate);
        }

        public static ExpressionStarter<Message> PredicateFilterByReadAllowedSubsystemRights(long? userId, ClaimsPrincipal claimsPrincipal, SettingsManager sm)
        {

            QueriableFilterExtensions.GetFromClaimsARs(claimsPrincipal?.Claims, out var ssAllRegions, out var ssAllRubrics, out var ssAllTerritories, out var ssAllTopics);

            var predicate = PredicateBuilder.New<Message>();

            var orPredicates = new List<ExpressionStarter<Message>>
            {
                GetCitizensPredicate(userId, claimsPrincipal, sm, ssAllRubrics, ssAllRegions, ssAllTerritories,
                    ssAllTopics),
                GetOrganizationPredicate(userId, claimsPrincipal, sm, ssAllRubrics, ssAllRegions, ssAllTerritories,
                    ssAllTopics),
                GetVotePredicate(userId, claimsPrincipal, sm, ssAllRubrics, ssAllRegions, ssAllTerritories, ssAllTopics),
                GetNewsPredicate(userId, claimsPrincipal, sm, ssAllRubrics, ssAllRegions, ssAllTerritories, ssAllTopics)
            };

            if (orPredicates.Any(el => el != null))
            {
                predicate = orPredicates.Where(el => el != null)
                    .Aggregate(predicate, (current, orPredicate) => current.Or(orPredicate));
                
            }
            else
            {
                predicate = predicate.And(el => false);
            }

            return predicate;
        }

        private static ExpressionStarter<Message> GetCitizensPredicate(long? userId, ClaimsPrincipal claimsPrincipal, SettingsManager sm,
            ICollection<string> ssAllRubrics, ICollection<string> ssAllRegions, ICollection<string> ssAllTerritories, ICollection<string> ssAllTopics)
        {
            if (!sm.SettingValue<bool>($"subsystem.{SSUID.citizens}.enable."))
            {
                return null;
            }

            var result = PredicateBuilder.New<Message>();

            result = result.And(el => el.Appeal.Subsystem.UID == SSUID.citizens);

            if ((claimsPrincipal?.Identity?.IsAuthenticated ?? false)
                && userId.HasValue
                && claimsPrincipal.HasClaim(el =>
                    el.Type == ClaimName.Subsystem && el.Value ==
                    $"{SubsystemCitizenAppeals.BaseSystemKey}.{SSClaimSuffix.Ss.available}"))
            {
                var hasClaimModerate = claimsPrincipal.HasClaim(el =>
                    el.Type == ClaimName.Moderate && el.Value ==
                    $"{SubsystemCitizenAppeals.BaseSystemKey}.{SSClaimSuffix.Md.enable}");

                var hasClaimFbAdmin = claimsPrincipal.HasClaim(el =>
                    el.Type == ClaimName.Subsystem && el.Value ==
                    $"{SubsystemCitizenAppeals.BaseSystemKey}.{SSClaimSuffix.Ss.feedback_admin}");

                if (hasClaimModerate || hasClaimFbAdmin)
                {
                    var orPredicate = PredicateBuilder.New<Message>();


                    if (hasClaimModerate)
                    {
                        var moderatePredicate = PredicateBuilder.New<Message>();

                        moderatePredicate = moderatePredicate.And(el => el.Appeal.AppealType == AppealTypeEnum.NORMAL);

                        if (!ssAllRubrics.Contains(SSUID.citizens))
                        {
                            moderatePredicate = moderatePredicate.And(el =>
                                el.Appeal.Rubric != null &&
                                el.Appeal.Rubric.UserModerator.Any(um => um.UserId == userId.Value));
                        }

                        if (!ssAllRegions.Contains(SSUID.citizens))
                        {
                            moderatePredicate = moderatePredicate.And(el =>
                                el.Appeal.Region != null &&
                                el.Appeal.Region.UserModerator.Any(um => um.UserId == userId.Value));
                        }

                        if (!ssAllTerritories.Contains(SSUID.citizens))
                        {
                            moderatePredicate = moderatePredicate.And(el =>
                                el.Appeal.Territory != null &&
                                el.Appeal.Territory.UserModerator.Any(um => um.UserId == userId.Value));
                        }

                        if (!ssAllTopics.Contains(SSUID.citizens))
                        {
                            moderatePredicate = moderatePredicate.And(el =>
                                el.Appeal.Topic != null &&
                                el.Appeal.Topic.UserModerator.Any(um => um.UserId == userId.Value));
                        }

                        orPredicate = orPredicate.Or(moderatePredicate);
                    }

                    if (hasClaimFbAdmin)
                    {
                        orPredicate = orPredicate.Or(el=>el.Appeal.AppealType == AppealTypeEnum.FEEDBACK);
                    }

                    result = result.And(orPredicate);
                }
                else
                {
                    result =  result.And(el => el.Public_ || el.Appeal.AuthorId == userId.Value);
                }
            }
            else
            {
                result = result.And(el => el.Public_);
            }

            return result;
        }

        private static ExpressionStarter<Message> GetOrganizationPredicate(long? userId, ClaimsPrincipal claimsPrincipal,
           SettingsManager sm, ICollection<string> ssAllRubrics, ICollection<string> ssAllRegions, ICollection<string> ssAllTerritories, ICollection<string> ssAllTopics)
        {
            if (!sm.SettingValue<bool>($"subsystem.{SSUID.organizations}.enable."))
            {
                return null;
            }

            var result = PredicateBuilder.New<Message>();

            result = result.And(el => el.Appeal.Subsystem.UID == SSUID.organizations);

            if ((claimsPrincipal?.Identity?.IsAuthenticated ?? false)
                && userId.HasValue
                && claimsPrincipal.HasClaim(el =>
                    el.Type == ClaimName.Subsystem && el.Value ==
                    $"{SubsystemOrganizationAppeals.BaseSystemKey}.{SSClaimSuffix.Ss.available}"))
            {
                var hasClaimModerate = claimsPrincipal.HasClaim(el =>
                    el.Type == ClaimName.Moderate && el.Value ==
                    $"{SubsystemOrganizationAppeals.BaseSystemKey}.{SSClaimSuffix.Md.enable}");

                if (hasClaimModerate)
                {
                    if (!ssAllRubrics.Contains(SSUID.organizations))
                    {
                        result = result.And(el =>
                            el.Appeal.Rubric != null &&
                            el.Appeal.Rubric.UserModerator.Any(um => um.UserId == userId));
                    }

                    if (!ssAllRegions.Contains(SSUID.organizations))
                    {
                        result = result.And(el =>
                            el.Appeal.Region != null &&
                            el.Appeal.Region.UserModerator.Any(um => um.UserId == userId));
                    }

                    if (!ssAllTerritories.Contains(SSUID.organizations))
                    {
                        result = result.And(el =>
                            el.Appeal.Territory != null &&
                            el.Appeal.Territory.UserModerator.Any(um => um.UserId == userId));
                    }

                    if (!ssAllTopics.Contains(SSUID.organizations))
                    {
                        result = result.And(el =>
                            el.Appeal.Topic != null &&
                            el.Appeal.Topic.UserModerator.Any(um => um.UserId == userId));
                    }
                }
                else
                {
                    result = result.And(el => el.Public_
                                              || el.Appeal.AuthorId == userId.Value
                                              || el.Appeal.Organization.Users2Organizations.Any(u =>
                                                  u.UserId == userId.Value));
                }
            }
            else
            {
                result = result.And(el => el.Public_);
            }

            return result;
        }

        private static ExpressionStarter<Message> GetVotePredicate(long? userId, ClaimsPrincipal claimsPrincipal, SettingsManager sm,
            ICollection<string> ssAllRubrics, ICollection<string> ssAllRegions, ICollection<string> ssAllTerritories, ICollection<string> ssAllTopics)
        {
            if (!sm.SettingValue<bool>($"subsystem.{SSUID.vote}.enable."))
            {
                return null;
            }
            var result = PredicateBuilder.New<Message>();

            result = result.And(el => (el.VoteId != null || el.VoteRootId != null));

            if ((claimsPrincipal?.Identity?.IsAuthenticated ?? false)
                && userId.HasValue
                && claimsPrincipal.HasClaim(el =>
                    el.Type == ClaimName.Subsystem && el.Value ==
                    $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Ss.available}"))
            {
                var hasClaimModerate = claimsPrincipal.HasClaim(el =>
                    el.Type == ClaimName.Moderate && el.Value ==
                    $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Md.enable}");

                if (hasClaimModerate)
                {
                    if (!ssAllRubrics.Contains(SSUID.vote))
                    {
                        result = result.And(el =>
                            (el.VoteRoot.Rubric != null &&
                             el.VoteRoot.Rubric.UserModerator.Any(um => um.UserId == userId.Value) ||
                             (el.Vote.VoteRoot.Rubric != null &&
                              el.Vote.VoteRoot.Rubric.UserModerator.Any(um => um.UserId == userId.Value))));
                    }

                    if (!ssAllRegions.Contains(SSUID.vote))
                    {
                        result = result.And(el =>
                            (el.VoteRoot.Region != null &&
                             el.VoteRoot.Region.UserModerator.Any(um => um.UserId == userId.Value) ||
                             (el.Vote.VoteRoot.Region != null &&
                              el.Vote.VoteRoot.Region.UserModerator.Any(um => um.UserId == userId.Value))));
                    }

                    if (!ssAllTerritories.Contains(SSUID.vote))
                    {
                        result = result.And(el =>
                            (el.VoteRoot.Territory != null &&
                             el.VoteRoot.Territory.UserModerator.Any(um => um.UserId == userId.Value) ||
                             (el.Vote.VoteRoot.Territory != null &&
                              el.Vote.VoteRoot.Territory.UserModerator.Any(um => um.UserId == userId.Value))));
                    }

                    if (!ssAllTopics.Contains(SSUID.vote))
                    {
                        result = result.And(el =>
                            (el.VoteRoot.Topic != null &&
                             el.VoteRoot.Topic.UserModerator.Any(um => um.UserId == userId.Value) ||
                             (el.Vote.VoteRoot.Topic != null &&
                              el.Vote.VoteRoot.Topic.UserModerator.Any(um => um.UserId == userId.Value))));
                    }
                }
                else
                {
                    result = result.And(el => el.Public_ || el.Author.Id == userId.Value);
                }
            }
            else
            {
                result = result.And(el => el.Public_);
            }

            return result;
        }

        private static ExpressionStarter<Message> GetNewsPredicate(long? userId, ClaimsPrincipal claimsPrincipal, SettingsManager sm,
            ICollection<string> ssAllRubrics, ICollection<string> ssAllRegions, ICollection<string> ssAllTerritories, ICollection<string> ssAllTopics)
        {
            if (!sm.SettingValue<bool>($"subsystem.{SSUID.news}.enable."))
            {
                return null;
            }
            var result = PredicateBuilder.New<Message>();

            result = result.And(el => el.NewsId != null);

            if ((claimsPrincipal?.Identity?.IsAuthenticated ?? false)
                && userId.HasValue
                && claimsPrincipal.HasClaim(el =>
                    el.Type == ClaimName.Subsystem && el.Value ==
                    $"{SubsystemNews.BaseSystemKey}.{SSClaimSuffix.Ss.available}"))
            {
                var hasClaimModerate = claimsPrincipal.HasClaim(el =>
                    el.Type == ClaimName.Moderate && el.Value ==
                    $"{SubsystemNews.BaseSystemKey}.{SSClaimSuffix.Md.enable}");
                if (hasClaimModerate)
                {
                    if (!ssAllRubrics.Contains(SSUID.vote))
                    {
                        result = result.And(el =>
                            el.News.Rubric != null &&
                            el.News.Rubric.UserModerator.Any(um => um.UserId == userId.Value));
                    }

                    if (!ssAllRegions.Contains(SSUID.vote))
                    {
                        result = result.And(el =>
                            el.News.Region != null &&
                            el.News.Region.UserModerator.Any(um => um.UserId == userId.Value));
                    }

                    if (!ssAllTerritories.Contains(SSUID.vote))
                    {
                        result = result.And(el =>
                            el.News.Territory != null &&
                            el.News.Territory.UserModerator.Any(um => um.UserId == userId.Value));
                    }

                    if (!ssAllTopics.Contains(SSUID.vote))
                    {
                        result = result.And(el =>
                            el.News.Topic != null &&
                            el.News.Topic.UserModerator.Any(um => um.UserId == userId.Value));
                    }
                }
                else
                {
                    result = result.And(el => el.Public_ || el.Author.Id == userId.Value);
                }
            }
            else
            {
                result = result.And(el => el.Public_);
            }

            return result;
        }

        public static IQueryable<Message> FilterByReadAllowedRubricatorModerator(
            this IQueryable<Message> main,
            long? userId)
        {
            var moderatorIn = main.Where(el => false);
            /*список модераторов по рубрике*/
            moderatorIn = moderatorIn.Concat(main.Where(el => el.Appeal.Rubric != null && el.Appeal.Rubric.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem == el.Appeal.Subsystem))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.VoteRoot.Rubric != null && el.VoteRoot.Rubric.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.vote))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.Vote.VoteRoot.Rubric != null && el.Vote.VoteRoot.Rubric.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.vote))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.News.Rubric != null && el.News.Rubric.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.news))));
            /*список модераторов по региону*/
            moderatorIn = moderatorIn.Concat(main.Where(el => el.Appeal.Region != null && el.Appeal.Region.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem == el.Appeal.Subsystem))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.VoteRoot.Region != null && el.VoteRoot.Region.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.vote))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.Vote.VoteRoot.Region != null && el.Vote.VoteRoot.Region.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.vote))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.News.Region != null && el.News.Region.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.news))));
            /*список модераторов по территории*/
            moderatorIn = moderatorIn.Concat(main.Where(el => el.Appeal.Territory != null && el.Appeal.Territory.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem == el.Appeal.Subsystem))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.VoteRoot.Territory != null && el.VoteRoot.Territory.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.vote))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.Vote.VoteRoot.Territory != null && el.Vote.VoteRoot.Territory.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.vote))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.News.Territory != null && el.News.Territory.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.news))));
            /*список модераторов по топику*/
            moderatorIn = moderatorIn.Concat(main.Where(el => el.Appeal.Topic != null && el.Appeal.Topic.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem == el.Appeal.Subsystem))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.VoteRoot.Topic != null && el.VoteRoot.Topic.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.vote))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.Vote.VoteRoot.Topic != null && el.Vote.VoteRoot.Topic.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.vote))));
            moderatorIn = moderatorIn.Concat(main.Where(el => el.News.Topic != null && el.News.Topic.UserModerator.Any(um => um.UserId == userId.Value && (um.Subsystem.UID == SSUID.news))));
            return moderatorIn;
        }

        public static async Task<BaseConstants.AllowedActionFlags> GetAllowedActions(long messageId, long? userId, ClaimsPrincipal claimsPrincipal, DocumentsContext ctx, SettingsManager sm)
        {
            var message = await ctx.Messages
                .Where(el => el.Id == messageId)
                .FirstOrDefaultAsync();
            var flags = BaseConstants.AllowedActionFlags.None;
            if (message != null)
            {
                flags |= await message.GetAllowedActions(userId, claimsPrincipal?.Claims, sm, ctx);
            }
            return flags;
        }

        public static async Task<BaseConstants.AllowedActionFlags> GetAllowedActions(
            this Message message,
            long? userId,
            IEnumerable<Claim> claims,
            SettingsManager sm,
            DocumentsContext ctx
            )
        {
            string ssUid = null;
            BaseConstants.AllowedActionFlags flags = 0;
            do
            {
                if (message == null)
                {
                    break;
                }
                if (message.Public_)
                {
                    flags |= BaseConstants.AllowedActionFlags.Read;
                }
                if (userId == null)
                {
                    break;
                }

                var claimsArray = claims as Claim[] ?? claims.ToArray();

                flags |= message.GetAllowedSuperAdminActions(claimsArray);

                /*право чтения по признаку авторства*/
                while (!flags.HasFlag(BaseConstants.AllowedActionFlags.Read))
                {
                    /*Я - автор сообщения*/
                    if (userId.Value == message.AuthorId)
                    {
                        flags |= BaseConstants.AllowedActionFlags.Read;
                        flags |= BaseConstants.AllowedActionFlags.AuthorOf;
                        break;
                    }
                    /*Я - автор обращения*/
                    if (message.Appeal == null && message.AppealId.HasValue)
                    {
                        await message.LoadReference(message.Appeal, ctx);
                    }
                    if (message.Appeal != null && message.Appeal.AuthorId == userId.Value)
                    {
                        flags |= BaseConstants.AllowedActionFlags.Read;
                        flags |= BaseConstants.AllowedActionFlags.AuthorOf;
                        break;
                    }
                    /*Я - автор новости*/
                    if (message.News == null && message.NewsId.HasValue)
                    {
                        await message.LoadReference(message.News, ctx);
                    }
                    if (message.News != null && message.News.AuthorId == userId.Value)
                    {
                        flags |= BaseConstants.AllowedActionFlags.Read;
                        flags |= BaseConstants.AllowedActionFlags.AuthorOf;
                    }
                    break;
                }
                ssUid = await GetSS_UID(message, ctx);
                if (
                    ssUid != null &&
                    !claimsArray.Any(el => el.Type == ClaimName.Subsystem && el.Value == $"{ssUid}.{SSClaimSuffix.Ss.available}"))
                {
                    break;
                }
                if (
                    ssUid != null &&
                    claimsArray.Any(el => el.Type == ClaimName.Subsystem && el.Value == $"{ssUid}.{SSClaimSuffix.Ss.create_message}")
                    )
                {
                    flags |= BaseConstants.AllowedActionFlags.Create;
                }

                /*Админ фидбеков может читать всю ветку комментариев и отвечать на сам фидбек*/
                if (
                    ssUid != null &&
                    claimsArray.Any(el => el.Type == ClaimName.Subsystem && el.Value == $"{ssUid}.{SSClaimSuffix.Ss.feedback_admin}")
                    )
                {
                    /*т.к. ss_uid уже есть, то Appeal уже загружен если он есть*/
                    if (message.Appeal != null && message.Appeal.AppealType == AppealTypeEnum.FEEDBACK)
                    {
                        /*читаем всю ветку*/
                        flags |= BaseConstants.AllowedActionFlags.Read;
                        if (message.ParentType == nameof(Appeal))
                        {
                            /*создаем только 1-й ответ*/
                            flags |= BaseConstants.AllowedActionFlags.Create;
                        }
                    }
                }

                if (
                    (!flags.HasFlag(BaseConstants.AllowedActionFlags.Read) || !flags.HasFlag(BaseConstants.AllowedActionFlags.Moderate)) &&
                    message.AppealId.HasValue)
                {
                    await message.LoadReference(message.Appeal, ctx);
                    if (message.Appeal != null)
                    {
                        flags |=
                            (await message.Appeal.GetAllowedModerationActions(userId, claimsArray, sm, ctx, null)) &
                            (BaseConstants.AllowedActionFlags.Read | BaseConstants.AllowedActionFlags.Moderate);
                    }
                }
                if (
                    (!flags.HasFlag(BaseConstants.AllowedActionFlags.Read) || !flags.HasFlag(BaseConstants.AllowedActionFlags.Moderate)) &&
                    message.VoteId.HasValue)
                {
                    await message.LoadReference(message.Vote, ctx);
                    if (message.Vote != null)
                    {
                        if (message.Vote.CommentsAllowed.HasValue && message.Vote.CommentsAllowed.Value == false)
                        {
                            flags &= ~BaseConstants.AllowedActionFlags.Create;
                        }
                        await message.Vote.LoadReference(message.Vote.VoteRoot, ctx);
                        if (message.Vote.VoteRoot != null)
                        {
                            if (message.Vote.VoteRoot.CommentsAllowed == false)
                            {
                                flags &= ~BaseConstants.AllowedActionFlags.Create;
                            }
                            flags |=
                                (await message.Vote.VoteRoot.GetAllowedModerationActions(userId, claimsArray, sm, ctx, SSUID.vote)) &
                                (BaseConstants.AllowedActionFlags.Read | BaseConstants.AllowedActionFlags.Moderate);
                        }
                    }
                }
                if (
                    (!flags.HasFlag(BaseConstants.AllowedActionFlags.Read) || !flags.HasFlag(BaseConstants.AllowedActionFlags.Moderate)) &&
                    message.VoteRootId.HasValue)
                {
                    await message.LoadReference(message.VoteRoot, ctx);
                    if (message.VoteRoot != null)
                    {
                        if (message.VoteRoot.CommentsAllowed == false)
                        {
                            flags &= ~BaseConstants.AllowedActionFlags.Create;
                        }
                        flags |=
                            (await message.VoteRoot.GetAllowedModerationActions(userId, claimsArray, sm, ctx, SSUID.vote)) &
                            (BaseConstants.AllowedActionFlags.Read | BaseConstants.AllowedActionFlags.Moderate);
                    }
                }
            } while (false);
            message.GetAMAFinal(sm, ref flags, ssUid);
            return flags;
        }

        private static async Task<string> GetSS_UID(Message message, DocumentsContext ctx)
        {
            string ssUid = null;
            do
            {
                if (message == null)
                {
                    break;
                }
                if (message.Appeal == null && message.AppealId.HasValue)
                {
                    await message.LoadReference(message.Appeal, ctx);
                }
                if (message.Appeal != null)
                {
                    await message.Appeal.LoadReference(message.Appeal.Subsystem, ctx);
                    ssUid = message.Appeal.Subsystem?.UID;
                    break;
                }
                if (message.VoteRoot == null && message.VoteRootId.HasValue)
                {
                    await message.LoadReference(message.VoteRoot, ctx);
                }
                if (message.VoteRoot != null)
                {
                    ssUid = SSUID.vote;
                    break;
                }
                if (message.Vote == null && message.VoteId.HasValue)
                {
                    await message.LoadReference(message.Vote, ctx);
                }
                if (message.Vote != null)
                {
                    ssUid = SSUID.vote;
                    break;
                }
                if (message.News == null && message.NewsId.HasValue)
                {
                    await message.LoadReference(message.News, ctx);
                }
                if (message.News != null)
                {
                    ssUid = SSUID.news;
                    break;
                }
            } while (false);

            //if (obj.AppealId.HasValue)
            //{
            //    await obj.LoadReference(obj.Appeal, ctx);
            //    if (obj.Appeal != null)
            //    {
            //        await obj.Appeal.LoadReference(obj.Appeal.Subsystem, ctx);
            //        ss_uid = obj.Appeal.Subsystem?.UID;
            //    }
            //}
            //else if (obj.VoteRootId.HasValue)
            //{
            //    await obj.LoadReference(obj.VoteRoot, ctx);
            //    if (obj.VoteRoot != null)
            //    {
            //        ss_uid = SSUID.vote;
            //    }
            //}
            //else if (obj.VoteId.HasValue)
            //{
            //    await obj.LoadReference(obj.Vote, ctx);
            //    if (obj.Vote != null)
            //    {
            //        ss_uid = SSUID.vote;
            //    }
            //}
            //else if (obj.NewsId.HasValue)
            //{
            //    await obj.LoadReference(obj.News, ctx);
            //    if (obj.News != null)
            //    {
            //        ss_uid = SSUID.news;
            //    }
            //}
            return ssUid;
        }

        public static IQueryable<Message> MakeQueryEntity(this IQueryable<Message> obj, GraphQlKeyDictionary requestedFields, List<GraphQlQueryFilter> filters, SelectionRange range, bool last = false)
        {
            obj = obj
                .IncludeByFields(requestedFields, last)
                .IncludeByNotMappedFields(requestedFields)
                .FiltersBy(filters)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), range?.OrderBy)
                .GetRange(range?.Take, range?.Skip);
            return obj;
        }
    }
}
