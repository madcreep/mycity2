namespace BIT.MyCity.Managers
{
    public static class SettingsKeys
    {
        public static readonly string System = "system.";
        public static readonly string SystemAuth = $"{System}auth.";
        public static readonly string SystemAuthAdditional = $"{SystemAuth}additional.";
        public static readonly string SystemAuthAdditionalUpdatePrevious = $"{SystemAuthAdditional}update_previous.";
        public static readonly string SystemAuthToken = $"{SystemAuth}token.";
        public static readonly string SystemAuthTokenLifetime = $"{SystemAuthToken}lifetime.";
        public static readonly string SystemAuthTokenLifetimeCitizen = $"{SystemAuthTokenLifetime}citizen.";
        public static readonly string SystemAuthTokenLifetimeCitizenMobile = $"{SystemAuthTokenLifetime}citizen_mobile.";
        public static readonly string SystemAuthTokenLifetimeManager = $"{SystemAuthTokenLifetime}manager.";

        public static readonly string SystemAuthEsia = $"{SystemAuth}esia.";
        public static readonly string SystemAuthEsiaEnabled = $"{SystemAuthEsia}enabled.";
        public static readonly string SystemAuthEsiaTestMode = $"{SystemAuthEsia}test_mode.";
        public static readonly string SystemAuthEsiaScoupeCitizen = $"{SystemAuthEsia}scoupe_citizen.";
        public static readonly string SystemAuthEsiaScoupeOrganization = $"{SystemAuthEsia}scoupe_organization.";
        public static readonly string SystemAuthEsiaMnemonics = $"{SystemAuthEsia}mnemonics.";
        public static readonly string SystemAuthEsiaOpensslPath = $"{SystemAuthEsia}openssl_path.";
        public static readonly string SystemAuthEsiaCerificate = $"{SystemAuthEsia}certificate.";
        public static readonly string SystemAuthEsiaCerificateEsia = $"{SystemAuthEsiaCerificate}esia.";
        public static readonly string SystemAuthEsiaCerificateOrganization = $"{SystemAuthEsiaCerificate}organization.";
        public static readonly string SystemAuthEsiaCerificateOrganizationKey = $"{SystemAuthEsiaCerificate}organization_key.";

        public static readonly string SystemAuthVk = $"{SystemAuth}vk.";
        public static readonly string SystemAuthVkEnabled = $"{SystemAuthVk}enabled.";
        public static readonly string SystemAuthVkAppKey = $"{SystemAuthVk}app_key.";
        public static readonly string SystemAuthVkSecretKey = $"{SystemAuthVk}secret_key.";
        public static readonly string SystemAuthVkServiceKey = $"{SystemAuthVk}service_key.";

        public static readonly string SystemAuthFacebook = $"{SystemAuth}facebook.";
        public static readonly string SystemAuthFacebookEnabled = $"{SystemAuthFacebook}enabled.";
        public static readonly string SystemAuthFacebookAppKey = $"{SystemAuthFacebook}app_key.";
        public static readonly string SystemAuthFacebookSecretKey = $"{SystemAuthFacebook}secret_key.";

        public static readonly string SystemAuthSms = $"{SystemAuth}sms.";
        public static readonly string SystemAuthSmsConfirmCodeLifetime = $"{SystemAuthSms}confirm_code_lifetime.";
        
        public static readonly string SystemAuthForm = $"{SystemAuth}form.";
        public static readonly string SystemAuthFormEnabled = $"{SystemAuthForm}enabled.";
        public static readonly string SystemAuthFormAuthAfterRegistration = $"{SystemAuthForm}auth_after_registration.";
        public static readonly string SystemAuthFormConfirmEmailUrl = $"{SystemAuthForm}confirm_email_url.";
        public static readonly string SystemAuthFormResetPasswordUrl = $"{SystemAuthForm}reset_password_url.";
        public static readonly string SystemAuthFormDisableNotConfirmEmail = $"{SystemAuthForm}disable_not_confirm_email.";



        public static readonly string SystemSmtp = $"{System}smtp.";
        public static readonly string SystemSmtpEnable = $"{SystemSmtp}enable.";
        public static readonly string SystemSmtpHost = $"{SystemSmtp}host.";
        public static readonly string SystemSmtpUser = $"{SystemSmtp}user.";
        public static readonly string SystemSmtpPassword = $"{SystemSmtp}password.";
        public static readonly string SystemSmtpFrom= $"{SystemSmtp}from.";
        public static readonly string SystemSmtpFromName = $"{SystemSmtp}from_name.";
        public static readonly string SystemSmtpPort = $"{SystemSmtp}port.";
        public static readonly string SystemSmtpUseSsl = $"{SystemSmtp}use_ssl.";
        public static readonly string SystemSmtpCheckCertificateRevocation = $"{SystemSmtp}check_certificate_revocation.";
        public static readonly string SystemSmtpReSendPeriod = $"{SystemSmtp}resend_period.";

        public static readonly string SystemApi = $"{System}api.";
        public static readonly string SystemApiBaseUrl = $"{SystemApi}base_url.";

        public static readonly string SystemApiNotification = $"{SystemApi}notification.";
        public static readonly string SystemApiNotificationAppeals = $"{SystemApiNotification}appeals.";
        public static readonly string SystemApiNotificationAppealsRevisionTime = $"{SystemApiNotificationAppeals}revision_time.";
        public static readonly string SystemApiNotificationAppealsAppealIntrval = $"{SystemApiNotificationAppeals}appeal_intrval.";

        public static readonly string SystemWeb = $"{System}web.";
        public static readonly string SystemWebGeneral = $"{SystemWeb}general.";
        public static readonly string SystemWebGeneralBaseUrl = $"{SystemWebGeneral}base_url.";
        public static readonly string SystemWebGeneralMainTitle = $"{SystemWebGeneral}maintitle.";
        public static readonly string SystemWebTerritory = $"{SystemWeb}territory.";
        public static readonly string SystemWebTerritoryTitle = $"{SystemWebTerritory}title.";
        public static readonly string SystemWebTerritoryTitleOne = $"{SystemWebTerritory}title_one.";
        public static readonly string SystemWebTerritoryAddEnabled = $"{SystemWebTerritory}add_enabled.";
        public static readonly string SystemWebRubric = $"{SystemWeb}rubric.";
        public static readonly string SystemWebRubricTitle = $"{SystemWebRubric}title.";
        public static readonly string SystemWebRubricTitleOne = $"{SystemWebRubric}title_one.";
        public static readonly string SystemWebRubricAddEnabled = $"{SystemWebRubric}add_enabled.";
        public static readonly string SystemWebRegion = $"{SystemWeb}region.";
        public static readonly string SystemWebRegionTitle = $"{SystemWebRegion}title.";
        public static readonly string SystemWebRegionTitleOne = $"{SystemWebRegion}title_one.";
        public static readonly string SystemWebRegionAddEnabled = $"{SystemWebRegion}add_enabled.";
        public static readonly string SystemWebTopic = $"{SystemWeb}topic.";
        public static readonly string SystemWebTopicTitle = $"{SystemWebTopic}title.";
        public static readonly string SystemWebTopicTitleOne = $"{SystemWebTopic}title_one.";
        public static readonly string SystemWebTopicAddEnabled = $"{SystemWebTopic}add_enabled.";

        public static readonly string SystemSms = $"{System}sms.";
        public static readonly string SystemSmsProvider = $"{SystemSms}provider.";
        public static readonly string SystemSmsLogin = $"{SystemSms}login.";
        public static readonly string SystemSmsPassword = $"{SystemSms}password.";
        public static readonly string SystemSmsSender = $"{SystemSms}sender.";

        public static readonly string SystemAttach = $"{System}attach.";
        public static readonly string SystemAttachAllowedExtensions = $"{SystemAttach}allowed_extensions.";
        public static readonly string SystemAttachThumbnails = $"{SystemAttach}thumbnails.";
        public static readonly string SystemAttachThumbnailsGql = $"{SystemAttachThumbnails}gql.";
        public static readonly string SystemAttachThumbnailsGqlHeight = $"{SystemAttachThumbnailsGql}height.";
        public static readonly string SystemAttachThumbnailsGqlWidth = $"{SystemAttachThumbnailsGql}width.";
        public static readonly string SystemAttachThumbnailsGqlMinHeight = $"{SystemAttachThumbnailsGql}minheight.";
        public static readonly string SystemAttachThumbnailsGqlMinWidth = $"{SystemAttachThumbnailsGql}minwidth.";
        public static readonly string SystemAttachThumbnailsGqlMaxHeight = $"{SystemAttachThumbnailsGql}maxheight.";
        public static readonly string SystemAttachThumbnailsGqlMaxWidth = $"{SystemAttachThumbnailsGql}maxwidth.";
        
        public static readonly string SystemOptions = $"{System}options.";
        public static readonly string SystemOptionsJournal = $"{SystemOptions}journal.";
        public static readonly string SystemOptionsJournalSavePeriod = $"{SystemOptionsJournal}save_period.";
        public static readonly string SystemOptionsJournalClearHour = $"{SystemOptionsJournal}clear_hour.";

        public static readonly string SystemOptionsFileCleaner = $"{SystemOptions}file_cleaner.";
        public static readonly string SystemOptionsFileCleanerUncheckPeriod = $"{SystemOptionsFileCleaner}uncheck_period.";
        public static readonly string SystemOptionsFileCleanerClearPeriod = $"{SystemOptionsFileCleaner}clear_period.";
    }
}
