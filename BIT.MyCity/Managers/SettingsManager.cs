using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Services;
using BIT.MyCity.WebApi.Types;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BIT.MyCity.Managers
{
    public sealed partial class SettingsManager
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(SettingsManager));

        private Lazy<dynamic> _settings;

        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IServiceProvider _serviceProvider;

        public SettingsManager(IServiceScopeFactory scopeFactory, IServiceProvider serviceProvider)
        {
            _scopeFactory = scopeFactory;
            _serviceProvider = serviceProvider;

            _settings = new Lazy<dynamic>(GetSettingsDynamic());
        }

        public void ReloadSettings()
        {
            _settings = new Lazy<dynamic>(GetSettingsDynamic());
        }

        private dynamic AllSettings => _settings.Value;

        public string SettingValue(string key)
        {
            var dyn = this[key];

            return (dyn?.Item as Settings)?.Value;
        }

        public T SettingValue<T>(string key)
        {
            try
            {
                var value = SettingValue(key);

                var type = typeof(T);

                T result;

                if (type.IsPrimitive || type.IsEnum || type == typeof(string))
                {
                    if (type.IsEnum)
                    {
                        result = (T)Enum.Parse(type, value);
                    }
                    else
                    {
                        result = (T)Convert.ChangeType(value, type);
                    }
                }
                else
                {
                    result = (T)(value == null ? null : JsonConvert.DeserializeObject(value, type));
                }
                
                return result;
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка приведения к типу ({typeof(T).Name}) значения настройки \"{key}\"", ex);

                throw;
            }
        }

        public Dictionary<string, string> Settings(params string[] key)
        {
            return GetSettingsList(key)?
                .ToDictionary(el => el.Key, el => el.Value)
                ?? new Dictionary<string, string>();
        }

        private List<Settings> GetSettingsList(params string[] keys)
        {
            var result = new List<Settings>();
            if (keys == null || !keys.Any())
            {
                GetSettingsToList(AllSettings, result);
            }
            else
            {
                foreach (var key in keys.Distinct())
                {
                    var settingsObject = this[key];

                    GetSettingsToList(settingsObject, result);
                }
            }

            return result;
        }

        private static void GetSettingsToList(dynamic obj, ICollection<Settings> list)
        {
            if (!(obj is IDictionary<string, object> dict))
            {
                return;
            }

            if (obj.Item is Settings setting)
            {
                list.Add(setting);
            }

            foreach (var item in dict)
            {
                if (item.Value is ExpandoObject expObj)
                {
                    GetSettingsToList(expObj, list);
                }
            }
        }

        private dynamic this[string key]
        {
            get
            {
                dynamic tmp = AllSettings;

                if (key == null)
                {
                    return tmp;
                }

                var keys = key.Trim('.').Split('.');

                foreach (var tKey in keys)
                {
                    tmp = TryGetKeyValue(tmp, tKey);

                    if (tmp == null)
                    {
                        break;
                    }
                }

                return tmp;
            }
        }

        public async Task<OperationResult<List<Settings>>> Set(IEnumerable<Settings> settings)
        {
            return await Set(settings.ToArray());
        }

        private async Task<OperationResult<List<Settings>>> Set(params Settings[] settings)
        {
            if (!CheckSettingValues(settings, out var errors))
            {
                return new OperationResult<List<Settings>>(null, errors);
            }

            var result = new List<Settings>(settings.Length);

            using (var scope = _scopeFactory.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                foreach (var setting in settings)
                {
                    var updatedSettingTuple = await SetInner(setting, context);

                    var updatedSetting = updatedSettingTuple.Item1;

                    result.Add(updatedSetting);

                    if (updatedSettingTuple.Item2)
                    {
                        var gqlSubscriptionService = _serviceProvider.GetRequiredService<GQLSubscriptionService>();
                        gqlSubscriptionService?.OnSettingsChanged(updatedSetting);
                    }
                }

                await context.SaveChangesAsync();
            }

            return new OperationResult<List<Settings>>(result);
        }

        private bool CheckSettingValues(Settings[] settings, out List<string> errors)
        {
            errors = new List<string>();

            foreach (var setting in settings)
            {
                if (!CheckKey(setting.Key, out var errorMessage))
                {
                    errors.Add(errorMessage);

                    continue;
                }

                if (!CheckSettingValue(this[setting.Key]?.Item as Settings, setting, out var settingErrors))
                {
                    errors.AddRange(settingErrors);
                }
            }

            return !errors.Any();
        }

        private bool CheckSettingValue(Settings dest, Settings src, out List<string> errors)
        {
            errors = new List<string>();

            if (dest == null)
            {
                //if (!src.Key.StartsWith("system."))
                //{
                //    src.Nullable = true;

                //    src.ValueType = typeof(string).FullName;

                //    return true;
                //}

                errors.Add($"Не найден параметр для обновления с ключом <{src.Key}>");

                return false;
            }

            if (!dest.Nullable && src.Value == null)
            {
                errors.Add($"Значение параметра <{dest.Key}> не может быть null");
            }
            else
            {
                if (dest.Values != null)
                {
                    //var typeValues = Type.GetType(dest.ValueType);

                    //var srcValue = JsonConvert.DeserializeObject(src.Value, typeValues);

                    //var existsValues = dest.Values
                    //    .Select(el => JsonConvert.DeserializeObject(el.Value, typeValues));

                    var existsValues = dest.Values
                        .Select(el => el.Value);

                    if (!existsValues.Contains(src.Value))
                    {
                        errors.Add(
                            $"Значение параметра <{dest.Description}> должно иметь значение из списка допустимых");
                    }
                }
                else
                {
                    if (dest.ValueType == typeof(Uri).FullName)
                    {
                        if (!Uri.IsWellFormedUriString(src.Value, UriKind.Absolute))
                        {
                            errors.Add($"Значение параметра <{dest.Description}> должно иметь тип {dest.ValueType}");
                        }
                    }
                    else
                    {
                        var type = Type.GetType(dest.ValueType);

                        if (type == null || !type.IsPrimitive && !type.IsEnum && type != typeof(string))
                        {
                            return !errors.Any();
                        }

                        if (type.IsEnum)
                        {
                            if (!Enum.TryParse(type, src.Value, out _))
                            {
                                errors.Add($"Значение параметра <{dest.Description}> должно иметь тип {type.Name}");
                            }
                        }
                        else
                        {
                            try
                            {
                                _ = Convert.ChangeType(src.Value, type, CultureInfo.InvariantCulture);
                            }
                            catch
                            {
                                errors.Add($"Значение параметра <{dest.Description}> должно иметь тип {type.Name}");
                            }
                        }
                    }
                }
            }

            return !errors.Any();
        }

        private static bool CheckKey(IEnumerable<string> keys, out List<string> errors)
        {
            errors = new List<string>();

            var keysArray = keys as string[] ?? keys?.ToArray();

            if (keysArray == null || !keysArray.Any())
            {
                return true;
            }

            foreach (var key in keysArray)
            {
                if (!CheckKey(key, out var error))
                {
                    errors.Add(error);
                }
            }

            return !errors.Any();
        }

        private static bool CheckKey(string key, out string errorMessage)
        {
            const string pattern = @"^([a-z0-9_]+\.)+$";

            if (key == null || !Regex.IsMatch(key, pattern))
            {
                errorMessage = $"Не верный формат ключа <{key}>. Элемент ключа может содержать символы [a-z,0-9,_] ) и должен заканчиваться точкой.";
            }
            else
            {
                errorMessage = null;
            }

            return errorMessage == null;
        }

        public async Task<Settings> Set(Settings setting)
        {
            Settings result;

            using var scope = _scopeFactory.CreateScope();

            var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

            result = (await SetInner(setting, context)).Item1;

            await context.SaveChangesAsync();

            return result;
        }

        //public async Task<OperationResult<bool>> Delete(IEnumerable<string> keys)
        //{
        //    return await Delete(keys.ToArray());
        //}

        //public async Task<OperationResult<bool>> Delete(params string[] keys)
        //{
        //    if (!CheckKey(keys, out var errors))
        //    {
        //        return new OperationResult<bool>(false, errors);
        //    }

        //    var forDelete = GetSettingsList(keys);

        //    using (var scope = _scopeFactory.CreateScope())
        //    {
        //        var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

        //        context.Settings.AttachRange(forDelete);

        //        context.Settings.RemoveRange(forDelete);

        //        await context.SaveChangesAsync();
        //    }

        //    foreach (var item in forDelete)
        //    {
        //        this[item.Key].Item = null;
        //    }

        //    return new OperationResult<bool>(true);
        //}

        private async Task<Tuple<Settings, bool>> SetInner(Settings settings, DocumentsContext context)
        {
            /*чтобы вернуть признак изменения объекта пришлось сделать так*/
            var dynSet = this[settings.Key];

            if (dynSet == null)
            {
                var keys = settings.Key.Split('.');

                var tmp = AllSettings;

                var lastObject = tmp;

                var newKey = string.Empty;

                foreach (var key in keys)
                {
                    tmp = TryGetKeyValue(tmp, key);

                    newKey += $"{key}.";

                    if (tmp == null)
                    {
                        tmp = CreateSettingsDynamicObject(settings.Key == newKey ? settings : null, key, newKey);

                        ((IDictionary<string, object>)lastObject)[key] = tmp;
                    }

                    lastObject = tmp;
                }

                context.Settings.Add(settings);

                if ((settings.SettingType == SettingType.File || settings.SettingType == SettingType.SecretFile
                    || settings.SettingType == SettingType.ValueAndFile || settings.SettingType == SettingType.ValueAndSecretFile)
                    && (settings.AttachedFilesIds?.Any(el => el > 0) ?? false))
                {
                    settings.AttachedFilesToSettings ??= new List<AttachedFilesToSettings>();

                    var files = (from s in settings.AttachedFilesIds
                                join d in settings.AttachedFilesToSettings on s equals d.AttachedFileId
                            into dd
                                from ddd in dd.DefaultIfEmpty()
                                select new { src = s, dest = ddd })
                        .ToArray();

                    settings.AttachedFilesToSettings.Clear();

                    settings.AttachedFilesToSettings.AddRange(files
                        .Where(el => el.dest != null)
                        .Select(el => el.dest));

                    var addIds
                        = files.Where(x => x.dest == null)
                            .Select(x => x.src)
                            .ToArray();

                    settings.AttachedFilesToSettings.AddRange((await context.AttachedFiles
                            .Where(el => addIds.Contains(el.Id))
                            .ToArrayAsync())
                        .Select(el => new AttachedFilesToSettings { AttachedFile = el }));
                }

                return Tuple.Create(settings, true);
            }

            var changed = false;

            if (!(dynSet.Item is Settings dest))
            {
                throw new Exception("Не удалось преобразовать к типу Settings");
            }

            context.Settings.Attach(dest);

            if ((dest.SettingType == SettingType.File || dest.SettingType == SettingType.SecretFile
                                                          || dest.SettingType == SettingType.ValueAndFile ||
                                                          dest.SettingType == SettingType.ValueAndSecretFile)
                && settings.AttachedFilesIds != null)
            {
                dest.AttachedFilesToSettings ??= new List<AttachedFilesToSettings>();

                var files = (from s in settings.AttachedFilesIds
                        join d in dest.AttachedFilesToSettings on s equals d.AttachedFileId
                            into dd
                        from ddd in dd.DefaultIfEmpty()
                        select new {src = s, dest = ddd})
                    .ToArray();

                dest.AttachedFilesToSettings.Clear();

                dest.AttachedFilesToSettings.AddRange(files
                    .Where(el => el.dest != null)
                    .Select(el => el.dest));

                var addIds
                    = files.Where(x => x.dest == null)
                        .Select(x => x.src)
                        .ToArray();

                dest.AttachedFilesToSettings.AddRange((await context.AttachedFiles
                        .Where(el => addIds.Contains(el.Id))
                        .ToArrayAsync())
                    .Select(el => new AttachedFilesToSettings {AttachedFile = el}));
            }

            if (dest.Value != settings.Value)
            {
                dest.Value = settings.Value;
                changed = true;
            }

            return Tuple.Create(dest, changed);
        }

        private static dynamic TryGetKeyValue(dynamic obj, string key)
        {
            if (!(obj is IDictionary<string, object> dict))
            {
                return null;
            }

            return dict.ContainsKey(key) ? dict[key] : null;
        }

        public async Task<OperationResult<List<Settings>>> GetSettingsAsync(IEnumerable<string> keys)
        {
            var keyArray = keys as string[] ?? keys?.ToArray();

            if (!CheckKey(keyArray, out var errors))
            {
                return new OperationResult<List<Settings>>(null, errors);
            }

            var result = GetSettingsList(keyArray);

            result = FilterByRights(result);

            return new OperationResult<List<Settings>>(result);
        }

        private List<Settings> FilterByRights(List<Settings> result)
        {
            using var scope = _scopeFactory.CreateScope();

            var httpContextAccessor = scope.ServiceProvider.GetRequiredService<IHttpContextAccessor>();

            var principal = httpContextAccessor.HttpContext.User;

            var rightLevel = GetRightLevel(principal);

            return result
                .Where(el => el.ReadingRightLevel <= rightLevel)
                .OrderBy(el => el.Key)
                .ToList();
        }

        public static RightLevel GetRightLevel(System.Security.Claims.ClaimsPrincipal principal)
        {
            RightLevel rightLevel;
            if (principal == null)
            {
                rightLevel = RightLevel.Undefined;
            }
            else if (principal.HasClaim(ClaimName.Manage, "settings"))
            {
                rightLevel = RightLevel.AdministratorSettings;
            }
            else if (principal.HasClaim(_ => _.Type == ClaimName.Manage || _.Type == ClaimName.Moderate))
            {
                rightLevel = RightLevel.Manager;
            }
            else if (principal.Identity.IsAuthenticated)
            {
                rightLevel = RightLevel.Authorized;
            }
            else
            {
                rightLevel = RightLevel.Unauthorized;
            }

            return rightLevel;
        }

        private dynamic GetSettingsDynamic()
        {
            using var scope = _scopeFactory.CreateScope();

            var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

            var settingsArray = context.Settings
                .Include(el => el.AttachedFilesToSettings)
                .ThenInclude(el => el.AttachedFile)
                .ToArray();

            return ToDynamicParseKey(
                settingsArray);
        }

        private static dynamic ToDynamicParseKey(IEnumerable<Settings> inObj)
        {
            var tmpArray = inObj
                .OrderBy(el => el.Key);

            var result = CreateSettingsDynamicObject(null);

            var stack = new Stack<object>();

            foreach (var item in tmpArray)
            {
                if (!stack.TryPeek(out dynamic top))
                {
                    top = null;
                }

                while (top != null && !item.Key.StartsWith(top.TreeKey))
                {
                    stack.Pop();

                    if (!stack.TryPeek(out top))
                    {
                        top = null;
                    }
                }

                var keys = item.Key
                    .Substring(top == null ? 0 : top.TreeKey.Length)
                    .Trim('.')
                    .Split('.');

                var newKey = top == null ? string.Empty : top.TreeKey;

                for (var i = 0; i < keys.Length; i++)
                {
                    var key = keys[i];

                    if (string.IsNullOrWhiteSpace(key))
                    {
                        continue;
                    }

                    newKey += $"{key}.";

                    var tmpObj = CreateSettingsDynamicObject(i < keys.Length - 1 ? null : item, key, newKey);

                    if (top != null)
                    {
                        ((IDictionary<string, object>)top)[key] = tmpObj;
                    }
                    else
                    {
                        ((IDictionary<string, object>)result)[key] = tmpObj;
                    }

                    stack.Push(tmpObj);

                    top = tmpObj;
                }
            }

            return result;
        }

        private static dynamic CreateSettingsDynamicObject(Settings item, string key = null, string treeKey = null)
        {
            dynamic result = new ExpandoObject();
            result.Key = key ?? string.Empty;
            result.TreeKey = treeKey ?? string.Empty;
            result.Item = item;
            result.Value = GetDynamicSettingsValue(result);
            return result;
        }

        private static string GetDynamicSettingsValue(dynamic dynamicObject)
        {
            return (dynamicObject?.Item as Settings)?.Value;
        }
    }

    public enum SettingType
    {
        Undefined = 0,
        Value = 1,
        File = 2,
        ValueAndFile = 3,
        SecretFile = 4,
        ValueAndSecretFile = 5,
    }

    public enum SettingNodeType
    {
        /// <summary>
        /// Не отображаемая
        /// </summary>
        None = 0,
        /// <summary>
        /// Группа элементов (Вкладка)
        /// </summary>
        TabGroup = 1,
        /// <summary>
        /// Группа элементов
        /// </summary>
        Group = 2,
        /// <summary>
        /// Элемент со значением
        /// </summary>
        Value = 3
    }
}
