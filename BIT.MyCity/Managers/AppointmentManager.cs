using BIT.MyCity.Database;
using BIT.MyCity.Model;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BIT.MyCity.Extensions;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.WebApi.Types.Pagination;

namespace BIT.MyCity.Managers
{
    public class AppointmentManager
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        public AppointmentManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
        }

        public async Task<Pagination<Appointment>> GetAppointmentsAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<Appointment>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.Appointments
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<Appointment>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<Appointment>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }
        private async Task<Appointment> GetAppointmentAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            return await _ctx.Appointments
                    .Where(el => el.Id == id)
                    .IncludeByFields(requestedFields)
                    .IncludeByNotMappedFields(requestedFields)
                    .FirstOrDefaultAsync();
        }
        public async Task<Appointment> CreateDbAsync(Appointment src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            if (!src.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }
            src.Rubric = await BaseConstants.GetObjById(src.RubricId, _ctx.Rubrics);
            src.Officer = await BaseConstants.GetObjById(src.OfficerId, _ctx.Officers);
            src.AppointmentRange = await BaseConstants.GetObjById(src.AppointmentRangeId, _ctx.AppointmentRanges);
            src.Topic = await BaseConstants.GetObjById(src.TopicId, _ctx.Topics);
            _ctx.Appointments.Add(src);
            await BaseConstants.ModifyIdsList(nameof(src.AttachedFiles), src, src, _ctx.AttachedFiles, _ctx);
            await BaseConstants.ModifyIdsList(nameof(src.Answers), src, src, _ctx.AppointmentNotifications, _ctx);
            await _ctx.SaveWeightAsync(src);
            await _ctx.SaveChangesAsync();
            return await GetAppointmentAsync(src.Id, requestedFields);
        }
        public async Task<Appointment> UpdateDbAsync(long id, Appointment src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.Appointments
                .Where(el => el.Id == id)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"AppointmentId {id} not found");
            }
            if (!dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }

            await BaseConstants.ModifyIdsList(nameof(src.AttachedFiles), src, dest, _ctx.AttachedFiles, _ctx, el => el.AttachedFiles);
            await BaseConstants.ModifyIdsList(nameof(src.Answers), src, dest, _ctx.AppointmentNotifications, _ctx, el => el.Answers);

            BaseConstants.UpdateEntityByFieldsList(dest, src, updateFields);
            await _ctx.SaveChangesAsync();
            return await GetAppointmentAsync(dest.Id, requestedFields);
        }
        public async Task<Pagination<AppointmentNotification>> GetAppointmentNotificationsAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<AppointmentNotification>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.AppointmentNotifications
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<AppointmentNotification>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<AppointmentNotification>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }
        private async Task<AppointmentNotification> GetAppointmentNotificationAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            return await _ctx.AppointmentNotifications
                    .Where(el => el.Id == id)
                    .IncludeByFields(requestedFields)
                    .IncludeByNotMappedFields(requestedFields)
                    .FirstOrDefaultAsync();
        }
        public async Task<AppointmentNotification> CreateDbAsync(AppointmentNotification src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            if (!src.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }
            src.Appointment = await BaseConstants.GetObjById(src.AppointmentId, _ctx.Appointments);
            _ctx.AppointmentNotifications.Add(src);
            await _ctx.SaveWeightAsync(src);
            await _ctx.SaveChangesAsync();
            return await GetAppointmentNotificationAsync(src.Id, requestedFields);
        }
        public async Task<AppointmentNotification> UpdateDbAsync(long id, AppointmentNotification src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.AppointmentNotifications
                .Where(el => el.Id == id)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"AppointmentNotificationId {id} not found");
            }
            if (!dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }
            BaseConstants.UpdateEntityByFieldsList(dest, src, updateFields);
            await _ctx.SaveChangesAsync();
            return await GetAppointmentNotificationAsync(dest.Id, requestedFields);
        }
        public async Task<Pagination<AppointmentRange>> GetAppointmentRangesAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<AppointmentRange>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.AppointmentRanges
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<AppointmentRange>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<AppointmentRange>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }
        private async Task<AppointmentRange> GetAppointmentRangeAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            return await _ctx.AppointmentRanges
                    .Where(el => el.Id == id)
                    .IncludeByFields(requestedFields)
                    .IncludeByNotMappedFields(requestedFields)
                    .FirstOrDefaultAsync();
        }
        public async Task<AppointmentRange> CreateDbAsync(AppointmentRange src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            if (!src.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }
            src.Officer = await BaseConstants.GetObjById(src.OfficerId, _ctx.Officers);
            await BaseConstants.ModifyIdsList(nameof(src.RegisteredAppointments), src, src, _ctx.Appointments, _ctx);
            _ctx.AppointmentRanges.Add(src);
            await _ctx.SaveWeightAsync(src);
            await _ctx.SaveChangesAsync();
            return await GetAppointmentRangeAsync(src.Id, requestedFields);
        }
        public async Task<AppointmentRange> UpdateDbAsync(long id, AppointmentRange src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.AppointmentRanges
                .Where(el => el.Id == id)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"AppointmentRangeId {id} not found");
            }
            if (!dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }
            await BaseConstants.ModifyIdsList(nameof(src.RegisteredAppointments), src, dest, _ctx.Appointments, _ctx, el => el.RegisteredAppointments);
            BaseConstants.UpdateEntityByFieldsList(dest, src, updateFields);
            await _ctx.SaveChangesAsync();
            return await GetAppointmentRangeAsync(dest.Id, requestedFields);
        }
        public async Task<Pagination<Officer>> GetOfficersAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<Officer>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.Officers
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<Officer>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<Officer>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }
        private async Task<Officer> GetOfficerAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            return await _ctx.Officers
                    .Where(el => el.Id == id)
                    .IncludeByFields(requestedFields)
                    .IncludeByNotMappedFields(requestedFields)
                    .FirstOrDefaultAsync();
        }
        public async Task<Officer> CreateDbAsync(Officer src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            if (!src.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }
            src.Rubric = await BaseConstants.GetObjById(src.RubricId, _ctx.Rubrics);
            _ctx.Officers.Add(src);
            await BaseConstants.ModifyIdsList(nameof(src.Ranges), src, src, _ctx.AppointmentRanges, _ctx);
            await _ctx.SaveWeightAsync(src);
            await _ctx.SaveChangesAsync();
            return await GetOfficerAsync(src.Id, requestedFields);
        }
        public async Task<Officer> UpdateDbAsync(long id, Officer src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.Officers
                .Where(el => el.Id == id)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"OfficerId {id} not found");
            }
            if (!dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }
            await BaseConstants.ModifyIdsList(nameof(src.Ranges), src, src, _ctx.AppointmentRanges, _ctx, el => el.Ranges);
            BaseConstants.UpdateEntityByFieldsList(dest, src, updateFields);
            await _ctx.SaveChangesAsync();
            return await GetOfficerAsync(dest.Id, requestedFields);
        }
    }
    public static class AppointmentExtensions
    {
        public static BaseConstants.AllowedActionFlags GetAllowedActions(this Appointment obj, long? userId, System.Security.Claims.ClaimsPrincipal claimsPrincipal)
        {
            BaseConstants.AllowedActionFlags flags = 0;
            do
            {
                if (obj == null)
                {
                    break;
                }
                flags |= obj.GetAllowedSuperAdminActions(claimsPrincipal);
                flags |= BaseConstants.AllowedActionFlags.Read;
                flags |= obj.GetAllowedModerationActions(userId, claimsPrincipal);
            } while (false);
            return flags;
        }
        public static BaseConstants.AllowedActionFlags GetAllowedModerationActions(this Appointment obj, long? userId, System.Security.Claims.ClaimsPrincipal claimsPrincipal)
        {
            BaseConstants.AllowedActionFlags flags = 0;
            do
            {
                if (obj == null)
                {
                    break;
                }
                if (userId == null)
                {
                    break;
                }
                if (claimsPrincipal == null)
                {
                    break;
                }
                flags |= obj.GetAllowedSuperAdminActions(claimsPrincipal);
                if (claimsPrincipal.Claims.Any(el => el.Type == ClaimName.Moderate))
                {
                    flags |= BaseConstants.AllowedActionFlags.Read | BaseConstants.AllowedActionFlags.Edit | BaseConstants.AllowedActionFlags.Moderate;
                }
            } while (false);
            return flags;
        }

    }

}
