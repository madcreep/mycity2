#nullable enable
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;

namespace BIT.MyCity.Managers
{
    public class CustomStorageManager
    {
        private readonly DocumentsContext _context;

        public CustomStorageManager(DocumentsContext context)
        {
            _context = context;
        }

        public async Task SetValueAsync([NotNull] string key,
            [NotNull] string ownerName,
            [NotNull] string ownerId,
            string? value)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (ownerName == null) throw new ArgumentNullException(nameof(ownerName));
            if (ownerId == null) throw new ArgumentNullException(nameof(ownerId));

            var tmpKey = key.Trim();
            var tmpOwnerName = ownerName.Trim();
            var tmpOwnerId = ownerId.Trim();

            if (tmpKey.Contains('%'))
            {
                throw new Exception("Символ '%' запрещен для использования в ключе.");
            }

            if (tmpKey.Length > 256)
            {
                throw new Exception("Длинна ключа не должна превышать 256 символов.");
            }

            if (tmpOwnerName.Length > 256)
            {
                throw new Exception("Имя владельца не должно превышать 256 символов.");
            }

            if (tmpOwnerId.Length > 256)
            {
                throw new Exception("Идентификатор владельца не должен превышать 256 символов.");
            }

            if (string.IsNullOrWhiteSpace(tmpKey)
                || string.IsNullOrWhiteSpace(tmpOwnerName)
                || string.IsNullOrWhiteSpace(tmpOwnerId))
            {
                throw new Exception($"Параметры '{nameof(key)}', '{nameof(ownerName)}', '{nameof(ownerId)}' должны иметь длинну большую 0.");
            }

            if (string.IsNullOrWhiteSpace(value))
            {
                await _context.CustomStorage
                    .Where(el => el.Key == tmpKey && el.OwnerName == tmpOwnerName && el.OwnerId == tmpOwnerId)
                    .DeleteAsync();

                return;
            }

            var dbValues = await _context.CustomStorage
                .Where(el => el.Key == tmpKey && el.OwnerName == tmpOwnerName && el.OwnerId == tmpOwnerId)
                .OrderBy(el => el.Weight)
                .ToArrayAsync();

            var weight = 0;

            foreach (var str in Split(value.Trim(), 2048))
            {
                weight++;

                var csItem = dbValues.SingleOrDefault(el => el.Weight == weight);

                if (csItem == null)
                {
                    csItem = new CustomStorageItem
                    {
                        Key = tmpKey,
                        OwnerName = tmpOwnerName,
                        OwnerId = tmpOwnerId,
                        Weight = weight,
                        Value = str
                    };

                    await _context.CustomStorage.AddAsync(csItem);
                }
                else
                {
                    csItem.Value = str;
                }
            }
            
            await _context.CustomStorage
                .Where(el => el.Key == tmpKey && el.OwnerName == tmpOwnerName && el.OwnerId == tmpOwnerId)
                .Where(el => el.Weight < 1 || el.Weight > weight)
                .DeleteAsync();

            await _context.SaveChangesAsync();
        }

        public async Task SetValueAsync([NotNull] string key,
            [NotNull] string ownerName,
            [NotNull] string ownerId,
            object? value)
        {
            var json = value == null
                ? null
                : JsonConvert.SerializeObject(value, Formatting.None);

            await SetValueAsync(key, ownerName, ownerId, json);
        }

        public async Task<string?> GetValueAsync(string? key, string? ownerName, string? ownerId)
        {
            IQueryable<CustomStorageItem> query = _context.CustomStorage;

            query = WhereKey(query, key);

            if (!string.IsNullOrWhiteSpace(ownerName))
            {
                var tmp = ownerName.Trim();

                query = query
                    .Where(el => el.OwnerName == tmp);
            }

            if (!string.IsNullOrWhiteSpace(ownerId))
            {
                var tmp = ownerId.Trim();

                query = query
                    .Where(el => el.OwnerId == tmp);
            }
            
            var values = await query
                .OrderBy(el => el.Weight)
                .Select(el => el.Value)
                .ToArrayAsync();
            
            return string.Concat(values);
        }

        public async Task<object?> GetValueAsync<T>(string? key, string? ownerName, string? ownerId)
        {
            var json = await GetValueAsync(key, ownerName, ownerId);

            return string.IsNullOrWhiteSpace(json)
                ? null
                : JsonConvert.DeserializeObject(json, typeof(T));
        }

        private static IEnumerable<string> Split(string text, int size)
        {
            for (var i = 0; i < text.Length; i += size)
                yield return text.Substring(i, Math.Min(size, text.Length - i));
        }

        private static IQueryable<CustomStorageItem> WhereKey(IQueryable<CustomStorageItem> query, string? value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return query;
            }

            var tmpValue = value.Trim();

            var hasStart = tmpValue.StartsWith('%');
            var hasEnd = tmpValue.EndsWith('%');

            tmpValue = tmpValue.Trim('%');

            if (hasStart && hasEnd)
            {
                return query
                    .Where(el => el.Key.Contains(tmpValue));
            }

            if (hasStart)
            {
                return query
                    .Where(el => el.Key.EndsWith(tmpValue));
            }

            if (hasEnd)
            {
                return query
                    .Where(el => el.Key.StartsWith(tmpValue));
            }

            return query
                .Where(el => el.Key == tmpValue);
        }

        public async Task DeleteAsync(string? key = null, string? ownerName = null, string? ownerId = null)
        {
            IQueryable<CustomStorageItem> query = _context.CustomStorage;

            query = WhereKey(query, key);

            if (!string.IsNullOrWhiteSpace(ownerName))
            {
                var tmp = ownerName.Trim();

                query = query
                    .Where(el => el.OwnerName == tmp);
            }

            if (!string.IsNullOrWhiteSpace(ownerId))
            {
                var tmp = ownerId.Trim();

                query = query
                    .Where(el => el.OwnerId == tmp);
            }

            await query
                .DeleteAsync();
        }
    }
}
