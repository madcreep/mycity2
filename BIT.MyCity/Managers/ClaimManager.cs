using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface;
using BIT.MyCity.ApiInterface.Models;
using Microsoft.AspNetCore.Identity;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Managers
{
    public class ClaimManager
    {
        private readonly IServiceProvider _serviceProvider;

        public ClaimManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        private IEnumerable<ClaimInfoModel> _claims
        {
            get
            {
                var weight = 0;

                var loginSystem = Enum.GetNames(typeof(LoginSystem))
                    .Select(el => new ClaimValueModel(++weight, el, el))
                    .ToArray();

                var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

                return new[]
                {
                    new ClaimInfoModel(ClaimName.Manage,
                        new[]
                        {
                            new ClaimValueModel(1, "users", "Пользователями"),
                            new ClaimValueModel(2, "roles", "Ролями"),

                            new ClaimValueModel(3, "rubrics", $"Справочником \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitle)}\""),
                            new ClaimValueModel(4, "territories", $"Справочником \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitle)}\""),
                            new ClaimValueModel(5, "regions", $"Справочником \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitle)}\""),
                            new ClaimValueModel(6, "topics", $"Справочником \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitle)}\""),

                            new ClaimValueModel(11, "settings", "Настройками"),
                            new ClaimValueModel(12, "journal", "Журналом"),
                            new ClaimValueModel(13, "custom_email", "Созданием пользовательских Email"),
                            new ClaimValueModel(14, "reports", "Созданием отчетов")
                        },
                        "Разрешения управления",
                        ClaimEntity.Role, ClaimEntity.User),

                    new ClaimInfoModel(ClaimName.Сonfirmed, null, "Запись подтверждена"),

                    new ClaimInfoModel(ClaimName.SuperAdmin, null, "Супер администратор"),

                    new ClaimInfoModel(ClaimName.LoginSystem, loginSystem, "Система авторизации"),

                    new ClaimInfoModel(ClaimName.Service, new[]
                        {
                            new ClaimValueModel(1, "delo", "Сервис")
                        }, "System service",
                        ClaimEntity.Role, ClaimEntity.User),
                };
            }
        }

        public IEnumerable<ClaimInfoModel> ClaimInfos
        {
            get
            {
                var subsystemService = _serviceProvider.GetRequiredService<SubsystemService>();

                var result = subsystemService.Subsystems
                    .Aggregate(
                        _claims,
                        (current, subsystem) => current.Union(subsystem.ClaimInfos));

                return result;
            }
        }

        private bool CheckClaim(Claim claim, ClaimEntity claimEntity)
        {
            return ClaimInfos.Any(el => el.ClaimType == claim.Type
                                        && (el.ClaimEntity.Contains(claimEntity))
                                        && el.Values.Any(x => x.Value == claim.Value));
        }

        private bool CheckClaim(IdentityRoleClaim<long> claim)
        {
            return ClaimInfos.Any(el => el.ClaimType == claim.ClaimType
                                        && (el.ClaimEntity.Contains(ClaimEntity.Role))
                                        && el.Values.Any(x => x.Value == claim.ClaimValue));
        }

        private bool CheckClaim(IdentityUserClaim<long> claim)
        {
            return ClaimInfos.Any(el => el.ClaimType == claim.ClaimType
                                        && (el.ClaimEntity.Contains(ClaimEntity.User))
                                        && el.Values.Any(x => x.Value == claim.ClaimValue));
        }

        private bool CheckClaim(ClaimModel claim, ClaimEntity claimEntity)
        {
            return ClaimInfos.Any(el => el.ClaimType == claim.Name
                                        && (el.ClaimEntity.Contains(claimEntity))
                                        && el.Values.Any(x => x.Value == claim.Value));
        }

        public bool CheckClaims(IEnumerable<Claim> claims, ClaimEntity claimEntity)
        {
            if (claims == null)
            {
                return true;
            }

            return claims
                .Aggregate(true, (current, claim) => current & CheckClaim(claim, claimEntity));
        }

        public bool CheckClaims(IEnumerable<IdentityRoleClaim<long>> claims)
        {
            if (claims == null)
            {
                return true;
            }

            return claims
                .Aggregate(true, (current, claim) => current & CheckClaim(claim));
        }

        public bool CheckClaims(IEnumerable<IdentityUserClaim<long>> claims)
        {
            if (claims == null)
            {
                return true;
            }

            return claims
                .Aggregate(true, (current, claim) => current & CheckClaim(claim));
        }

        public bool CheckClaims(IEnumerable<ClaimModel> claims, ClaimEntity claimEntity)
        {
            if (claims == null)
            {
                return true;
            }

            return claims
                .Aggregate(true, (current, claim) => current & CheckClaim(claim, claimEntity));
        }

        public async Task<IdentityResult> AddUserClaim(User user, Claim claim)
        {
            if (!CheckClaim(claim, ClaimEntity.User))
            {
                return IdentityResult.Failed(new IdentityError { Code = "BagClaims", Description = "Разрешение не применимо к пользователю" });
            }

            var userManager = (UserManager<User>)_serviceProvider.GetService(typeof(UserManager<User>));

            var hasClaims = await userManager.GetClaimsAsync(user);

            var superAdminClaim = hasClaims
              .FirstOrDefault(el => el.Type == claim.Type && el.Value == claim.Value);

            if (superAdminClaim == null)
            {
                return (await userManager.AddClaimAsync(user, claim));
            }

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> RemoveUserClaim(User user, Claim claim)
        {
            var userManager = (UserManager<User>)_serviceProvider.GetService(typeof(UserManager<User>));

            var hasClaims = await userManager.GetClaimsAsync(user);

            var dbClaim = hasClaims
              .FirstOrDefault(el => el.Type == claim.Type && el.Value == claim.Value);

            if (dbClaim != null)
            {
                return await userManager.RemoveClaimAsync(user, dbClaim);
            }

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> UpdateUserClaims(User user, IEnumerable<Claim> needClaims, bool withoutCheck = false)
        {
            if (needClaims == null)
            {
                throw new ArgumentNullException(nameof(needClaims));
            }

            var needClaimsList = needClaims as List<Claim> ?? needClaims.ToList();

            if (!withoutCheck && !CheckClaims(needClaimsList, ClaimEntity.User))
            {
                return IdentityResult.Failed(new IdentityError { Code = "BagClaims", Description = "Разрешения не применимы к пользователям" });
            }

            var isSuperAdmin = user.IdentityClaims
                .Any(el => el.ClaimType == ClaimName.SuperAdmin);

            if (isSuperAdmin)
            {
                if (needClaimsList.All(el => el.Type != ClaimName.SuperAdmin))
                {
                    needClaimsList.Add(new Claim(ClaimName.SuperAdmin, ""));
                }

                if (!needClaimsList
                    .Any(el => el.Type == ClaimName.Manage && el.Value == "users"))
                {
                    needClaimsList.Add(new Claim(ClaimName.Manage, "users"));
                }
            }

            // Обработка клайма LoginSystem
            var lsClaims = needClaimsList
                .Where(el => el.Type == ClaimName.LoginSystem);
            foreach (var lsClaim in lsClaims)
            {
                needClaimsList.Remove(lsClaim);
            }

            needClaimsList.AddRange(user.IdentityClaims
                .Where(el=>el.ClaimType == ClaimName.LoginSystem)
                .Select(el=> new Claim(el.ClaimType, el.ClaimValue)));

            var forDelete = (from uc in user.IdentityClaims
                             join nc in needClaimsList
                                 on new { Type = uc.ClaimType, Value = uc.ClaimValue }
                                 equals new { nc.Type, nc.Value } into joinNClaims
                             from joinNc in joinNClaims.DefaultIfEmpty()
                             where joinNc == null
                             select uc)
                .ToArray();

            if (forDelete.Any())
            {
                foreach (var claim in forDelete)
                {
                    user.IdentityClaims.Remove(claim);
                }
            }

            var forAdd = from nc in needClaimsList
                         join uc in user.IdentityClaims
                       on new { nc.Type, nc.Value }
                       equals new { Type = uc.ClaimType, Value = uc.ClaimValue } into joinUClaims
                         from joinUc in joinUClaims.DefaultIfEmpty()
                         where joinUc == null
                         select new IdentityUserClaim<long> { ClaimType = nc.Type, ClaimValue = nc.Value };


            user.IdentityClaims.AddRange(forAdd);

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> UpdateRoleClaimsAsync(Role role, IEnumerable<Claim> needClaims, bool systemSet = false)
        {
            if (!systemSet && !CheckClaims(needClaims, ClaimEntity.Role))
            {
                return IdentityResult.Failed(new IdentityError { Code = "BagClaims", Description = "Разрешения не применимы к ролям" });
            }

            var roleManager = (RoleManager<Role>)_serviceProvider.GetService(typeof(RoleManager<Role>));

            var hasClaims = await roleManager.GetClaimsAsync(role);

            GetAddDeleteClaims(needClaims, hasClaims, out var forDelete, out var forAdd);

            var result = IdentityResult.Success;

            foreach (var claim in forDelete)
            {
                result = await roleManager.RemoveClaimAsync(role, claim);

                if (!result.Succeeded)
                {
                    break;
                }
            }

            if (!result.Succeeded)
            {
                return result;
            }

            foreach (var claim in forAdd)
            {
                result = await roleManager.AddClaimAsync(role, claim);

                if (!result.Succeeded)
                {
                    break;
                }
            }

            return result;

        }

        private static void GetAddDeleteClaims(IEnumerable<Claim> needClaims, IList<Claim> hasClaims, out IEnumerable<Claim> forDelete,
          out IEnumerable<Claim> forAdd)
        {
            var claimComparer = new ClaimComparer();

            forDelete = hasClaims
              .Except(needClaims, claimComparer);

            forAdd = needClaims
              .Except(hasClaims, claimComparer);
        }

        public void RemoveUnusedClaims(User user)
        {
            var toRemoveTypes = ClaimInfos
                .Where(el => !el.ClaimEntity.Any())
                .Select(el => el.ClaimType);

            var unsupportClaimTypes = user.IdentityClaims
                .Join(toRemoveTypes,
                    uc => uc.ClaimType,
                    ci => ci,
                    (uc, ci) => uc)
                .ToArray();

            foreach (var identityUserClaim in unsupportClaimTypes)
            {
                user.IdentityClaims.Remove(identityUserClaim);
            }
        }
    }

    internal class ClaimComparer : IEqualityComparer<Claim>
    {
        public bool Equals(Claim x, Claim y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (ReferenceEquals(x, null) || ReferenceEquals(y, null))
            {
                return false;
            }

            return x.Type == y.Type && x.Value == y.Value;
        }

        public int GetHashCode([NotNull] Claim claim)
        {
            var hashProductName = claim.Type == null ? 0 : claim.Type.GetHashCode();

            var hashProductCode = claim.Value == null ? 0 : claim.Value.GetHashCode();

            return hashProductName ^ hashProductCode;
        }
    }
}
