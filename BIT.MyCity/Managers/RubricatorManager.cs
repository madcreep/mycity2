using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using BIT.MyCity.Subsystems;
using BIT.MyCity.WebApi.Types.Pagination;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Helpers;
using log4net;
using Microsoft.EntityFrameworkCore.Storage;

namespace BIT.MyCity.Managers
{
    public class RubricatorManager
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(RubricatorManager));

        private readonly IServiceProvider _serviceProvider;
        private readonly UserManager _um;
        private readonly DocumentsContext _ctx;

        public RubricatorManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
        }
        
        #region Get Rubricators
        public async Task<Pagination<T>> GetRubricatorsAsync<T>(QueryOptions options)
            where T: class, IRubricator
        {
            options
                .RequestedFields
                .TryGetValue(nameof(Pagination<T>.Items).ToJsonFormat(), out var items);

            if (options.Filters != null)
            {
                foreach (var queryFilter in options.Filters)
                {
                    var prefix = $"Subsystem.";
                    if (queryFilter.Name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
                    {
                        queryFilter.Name = $"{nameof(Rubric.Rubricator2Subsystems)}.{nameof(Rubric2Subsystem.Subsystem)}.{queryFilter.Name.Substring(prefix.Length)}";
                    }
                }
            }

            var dbSet = _ctx.DbSet<T>(typeof(T));

            var obj = dbSet
                .IncludeByFields(items)
                .IncludeByNotMappedFields<IRubricator>(items)
                .Cast<T>()
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy);
                

            var result = new Pagination<T>();

            if (options.RequestedFields.ContainsKey(nameof(Pagination<Rubric>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }


            if (!options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                return result;
            }

            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);

            result.Items = await obj
                .ToListAsync();

            //HierarchyHelper.RebaseHierarchy(result.Items);

            return result;
        }
        //public async Task<Pagination<Rubric>> GetRubricsAsync(QueryOptions options)
        //{
        //    //var tt = GetRubricatorsAsync<Rubric>(options);

        //    options
        //        .RequestedFields
        //        .TryGetValue(nameof(Pagination<Rubric>.Items).ToJsonFormat(), out var items);

        //    if (options.Filters != null)
        //    {
        //        foreach (var queryFilter in options.Filters)
        //        {
        //            var prefix = $"Subsystem.";
        //            if (queryFilter.Name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
        //            {
        //                queryFilter.Name = $"{nameof(Rubric.Rubricator2Subsystems)}.{nameof(Rubric2Subsystem.Subsystem)}.{queryFilter.Name.Substring(prefix.Length)}";
        //            }
        //        }
        //    }

        //    var obj = _ctx.Rubrics
        //        .IncludeByFields(items)
        //        .IncludeByNotMappedFields(items)
        //        .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
        //        .FiltersBy(options.Filters, options.WithDeleted)
        //        .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy);

        //    var result = new Pagination<Rubric>();

        //    if (options.RequestedFields.ContainsKey(nameof(Pagination<Rubric>.Total).ToJsonFormat()))
        //    {
        //        result.Total = await obj.LongCountAsync();
        //    }


        //    if (!options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
        //    {
        //        return result;
        //    }

        //    obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);

        //    result.Items = await obj.ToListAsync();

        //    return result;
        //}

        //public async Task<Pagination<Region>> GetRegionsAsync(QueryOptions options)
        //{
        //    options.RequestedFields.TryGetValue(nameof(Pagination<Region>.Items).ToJsonFormat(), out var items);
        //    if (options.Filters != null)
        //    {
        //        foreach (var a in options.Filters)
        //        {
        //            var prefix = $"Subsystem.";
        //            if (a.Name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
        //            {
        //                a.Name = $"{nameof(Region.Rubricator2Subsystems)}.{nameof(Region2Subsystem.Subsystem)}.{a.Name.Substring(prefix.Length)}";
        //            }
        //        }
        //    }
        //    var obj = _ctx.Regions
        //        .IncludeByFields(items)
        //        .IncludeByNotMappedFields(items)
        //        .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
        //        .FiltersBy(options.Filters, options.WithDeleted)
        //        .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
        //        ;
        //    var result = new Pagination<Region>();
        //    if (options.RequestedFields.ContainsKey(nameof(Pagination<Region>.Total).ToJsonFormat()))
        //    {
        //        result.Total = await obj.LongCountAsync();
        //    }
        //    obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
        //    if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
        //    {
        //        result.Items = await obj.ToListAsync();
        //    }
        //    return result;
        //}

        //public async Task<Pagination<Territory>> GetTerritoriesAsync(QueryOptions options)
        //{
        //    options.RequestedFields.TryGetValue(nameof(Pagination<Territory>.Items).ToJsonFormat(), out var items);
        //    if (options.Filters != null)
        //    {
        //        foreach (var a in options.Filters)
        //        {
        //            var prefix = $"Subsystem.";
        //            if (a.Name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
        //            {
        //                a.Name = $"{nameof(Territory.Rubricator2Subsystems)}.{nameof(Territory2Subsystem.Subsystem)}.{a.Name.Substring(prefix.Length)}";
        //            }
        //        }
        //    }
        //    var obj = _ctx.Territories
        //        .IncludeByFields(items)
        //        .IncludeByNotMappedFields(items)
        //        .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
        //        .FiltersBy(options.Filters, options.WithDeleted)
        //        .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
        //        ;
        //    var result = new Pagination<Territory>();
        //    if (options.RequestedFields.ContainsKey(nameof(Pagination<Territory>.Total).ToJsonFormat()))
        //    {
        //        result.Total = await obj.LongCountAsync();
        //    }
        //    obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
        //    if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
        //    {
        //        result.Items = await obj.ToListAsync();
        //    }
        //    return result;
        //}

        //public async Task<Pagination<Topic>> GetTopicsAsync(QueryOptions options)
        //{
        //    return await GetRubricatorsAsync<Topic>(options);

        //    //options.RequestedFields.TryGetValue(nameof(Pagination<Topic>.Items).ToJsonFormat(), out var items);
        //    //if (options.Filters != null)
        //    //{
        //    //    foreach (var a in options.Filters)
        //    //    {
        //    //        var prefix = $"Subsystem.";
        //    //        if (a.Name.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
        //    //        {
        //    //            a.Name = $"{nameof(Topic.Rubricator2Subsystems)}.{nameof(Topic2Subsystem.Subsystem)}.{a.Name.Substring(prefix.Length)}";
        //    //        }
        //    //    }
        //    //}
        //    //var obj = _ctx.Topics
        //    //    .IncludeByFields(items)
        //    //    .IncludeByNotMappedFields(items)
        //    //    .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
        //    //    .FiltersBy(options.Filters, options.WithDeleted)
        //    //    .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
        //    //    ;
        //    //var result = new Pagination<Topic>();
        //    //if (options.RequestedFields.ContainsKey(nameof(Pagination<Topic>.Total).ToJsonFormat()))
        //    //{
        //    //    result.Total = await obj.LongCountAsync();
        //    //}
        //    //obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
        //    //if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
        //    //{
        //    //    result.Items = await obj.ToListAsync();
        //    //}
        //    //return result;
        //} 
        #endregion
        
        #region Create Rubricator
        public async Task<IRubricator> CreateAsync<T>(T obj, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
            where T : IRubricator
        {
            if (obj == null)
            {
                return null;
            }

            var suffix = GetSuffix(obj);

            await CheckCreateRights(obj, updateKeys, suffix);
            
            IDbContextTransaction trn = null;

            try
            {
                trn = await _ctx.Database.BeginTransactionAsync();
                
                if (updateKeys.ContainsKey(nameof(obj.SubSystemUIDs).ToJsonFormat()))
                {
                    await SubsystemsSet(obj, obj);

                    updateKeys.Remove(nameof(obj.SubSystemUIDs).ToJsonFormat());
                }

                if (updateKeys.ContainsKey(nameof(obj.Moderators).ToJsonFormat()))
                {
                    await ModeratorsSet(obj, obj);

                    updateKeys.Remove(nameof(obj.Moderators).ToJsonFormat());
                }

                if (updateKeys.ContainsKey(nameof(obj.Executors).ToJsonFormat()))
                {
                    await ExecutorsSet(obj, obj);
                    
                    updateKeys.Remove(nameof(obj.Executors).ToJsonFormat());
                }

                await _ctx.AddAsync(obj);

                await _ctx.SaveChangesAsync();

                if (obj is IHierarchical hierarchyEnt)
                {
                    await HierarchyHelper.UpdateHierarchy(_ctx, hierarchyEnt, null);
                }

                await WeightHelper.SetNewWeightAsync(_ctx, obj);
                
                await _ctx.SaveChangesAsync();

                await trn.CommitAsync();
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка создания записи рубрикатора <{obj.GetType().Name}>", ex);

                if (trn != null)
                {
                    await trn.RollbackAsync();
                }

                throw;
            }
            
            return  await GetRubricatorAsync(obj.GetType(), obj.Id, requestedFields);
        }
        
        private async Task CheckCreateRights(IRubricatorFields catalog, GraphQlKeyDictionary updateKeys, string suffix)
        {
            var subsystemService = _serviceProvider.GetRequiredService<SubsystemService>();

            var claimsPrincipalId = _um.ClaimsPrincipalId();

            if (!claimsPrincipalId.HasValue)
            {
                throw new Exception("Вы не авторизованы");
            }

            var needKeys = new GraphQlKeyDictionary
            {
                {nameof(User.AllClaims).ToJsonFormat(), new GraphQlKeyDictionary() }
            };

            var user = await _um.GetUserByIdAsync(claimsPrincipalId.Value, needKeys);

            if (!user.HasClaim(ClaimName.Manage, suffix))
            {
                throw new Exception($"У Вас нет прав на создание элемента справочника ({ClaimName.Manage}:{suffix})");
            }

            if (updateKeys.ContainsKey(nameof(IRubricatorFields.SubSystemUIDs).ToJsonFormat()))
            {
                foreach (var subsystemUid in catalog.SubSystemUIDs ?? new List<string>())
                {
                    if (subsystemService.Subsystems.All(el => el.SystemKey != subsystemUid))
                    {
                        throw new Exception($"Неизвестня подсистема '{subsystemUid}'");
                    }

                    if (!user.HasClaim(ClaimName.Subsystem, $"{subsystemUid}.{SSClaimSuffix.Ss.available}")
                        || !user.HasClaim(ClaimName.Manage, $"{subsystemUid}.{suffix}"))
                    {
                        throw new Exception(
                            $"У Вас нет прав для привязки к подсистеме <{subsystemUid}> ({ClaimName.Manage}:{subsystemUid}.{suffix})");
                    }
                }
            }

            if (updateKeys.ContainsKey(nameof(IRubricatorFields.ModeratorsSet).ToJsonFormat()))
            {
                var subsystemUidArray = (catalog.ModeratorsSet ?? new List<ClerkInSubsystemSet>())
                    .Select(el => el.SubsystemUid)
                    .ToArray();

                foreach (var subsystemUid in subsystemUidArray)
                {
                    if (subsystemService.Subsystems.All(el => el.SystemKey != subsystemUid))
                    {
                        throw new Exception($"Неизвестня подсистема '{subsystemUid}'");
                    }

                    if (!user.HasClaim(ClaimName.Subsystem, $"{subsystemUid}.{SSClaimSuffix.Ss.available}")
                        || !user.HasClaim(ClaimName.Manage, $"{subsystemUid}.{suffix}"))
                    {
                        throw new Exception(
                            $"У Вас нет прав для привязки пользователей к подсистеме <{subsystemUid}> ({ClaimName.Manage}:{subsystemUid}.{suffix})");
                    }
                }
            }
        }
        #endregion

        #region Update Rubricator
        private async Task CheckChangeRights(IRubricatorFields srcCatalog, IRubricatorFields destCatalog, GraphQlKeyDictionary updateKeys, string suffix)
        {
            var subsystemService = _serviceProvider.GetRequiredService<SubsystemService>();

            var claimsPrincipalId = _um.ClaimsPrincipalId();

            if (!claimsPrincipalId.HasValue)
            {
                throw new Exception("Вы не авторизованы");
            }

            var needKeys = new GraphQlKeyDictionary
            {
                {nameof(User.AllClaims).ToJsonFormat(), new GraphQlKeyDictionary() }
            };

            var user = await _um.GetUserByIdAsync(claimsPrincipalId.Value, needKeys);

            //var user = _um.CurrentClaimsPrincipal();

            if (updateKeys.ContainsKey(nameof(IRubricatorFields.SubSystemUIDs).ToJsonFormat()))
            {
                var destSsUidArray = destCatalog.Subsystems
                    .Select(el => el.UID)
                    .ToArray();

                var exceptUidArray = destSsUidArray
                    .Except(srcCatalog.SubSystemUIDs)
                    .Union(srcCatalog.SubSystemUIDs.Except(destSsUidArray))
                    .Distinct()
                    .ToArray();

                if (exceptUidArray.Any())
                {

                    foreach (var subsystemUid in exceptUidArray)
                    {
                        if (subsystemService.Subsystems.All(el => el.SystemKey != subsystemUid))
                        {
                            throw new Exception($"Неизвестня подсистема '{subsystemUid}'");
                        }

                        if (!user.HasClaim(ClaimName.Subsystem, $"{subsystemUid}.{SSClaimSuffix.Ss.available}")
                            || !user.HasClaim(ClaimName.Manage, $"{subsystemUid}.{suffix}"))
                        {
                            throw new Exception(
                                $"У Вас нет прав для изменения привязки к подсистеме <{subsystemUid}> ({ClaimName.Manage}:{subsystemUid}.{suffix})");
                        }
                    }
                }
            }

            if (updateKeys.ContainsKey(nameof(IRubricatorFields.Moderators).ToJsonFormat()))
            {
                foreach (var srcModerator in srcCatalog.ModeratorsSet)
                {
                    var subsystemUid = srcModerator.SubsystemUid;

                    if (subsystemService.Subsystems.All(el => el.SystemKey != subsystemUid))
                    {
                        throw new Exception($"Неизвестня подсистема '{subsystemUid}'");
                    }

                    var srcModeratorsId = srcModerator.UserIds ?? new List<long>();

                    var destModeratorsId = destCatalog.Moderators
                        .Where(el => el.SubsystemUid == subsystemUid)
                        .SelectMany(el => el.Users.Select(x => x.Id))
                        .ToList();

                    var exceptId = srcModeratorsId.Except(destModeratorsId)
                        .Union(destModeratorsId.Except(srcModeratorsId))
                        .Distinct();

                    if (exceptId.Any()
                        && (!user.HasClaim(ClaimName.Subsystem, $"{subsystemUid}.{SSClaimSuffix.Ss.available}")
                            || !user.HasClaim(ClaimName.Manage, $"{subsystemUid}.{suffix}")))
                    {
                        throw new Exception(
                            $"У Вас нет прав для изменения привязки пользователей к подсистеме <{subsystemUid}> ({ClaimName.Manage}:{subsystemUid}.{suffix})");
                    }
                }
            }
        }

        public async Task<IRubricator> UpdateAsync(long id, IRubricator src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var dbSet = _ctx.DbSet<IRubricator>(src.GetType());

            var dest = await dbSet
                .Where(el => el.Id == id)
                .Include("Rubricator2Subsystems.Subsystem")
                .Include("UserModerator.Subsystem")
                .Include("UserModerator.User.IdentityClaims")
                .Include("UserModerator.User.UserRole.Role.IdentityClaims")
                .Include("UserExecutor.User")
                .Include("Rubricator2Subsystems.Subsystem")
                .FirstOrDefaultAsync();

            long? oldWeight = null;

            if (updateKeys.ContainsKey(nameof(ISortable.Weight).ToJsonFormat()))
            {
                oldWeight = dest.Weight;
            }

            if (dest == null)
            {
                throw new Exception($"Id {id} not found");
            }

            ParentInfo oldParent = null;

            if (dest.ParentId.HasValue && !string.IsNullOrWhiteSpace(dest.ParentType))
            {
                oldParent = new ParentInfo(dest.ParentId.Value, dest.ParentType);
            }

            var suffix = GetSuffix(src);

            await CheckChangeRights(src, dest, updateKeys, suffix);

            var trn = await _ctx.Database.BeginTransactionAsync();

            try
            {
                await UpdateAsync(src, dest, updateKeys);

                if (updateKeys.ContainsKey(nameof(IRubricator.ParentId).ToJsonFormat())
                    && dest is IHierarchical hierarchicalDest)
                {
                    hierarchicalDest.ParentId = src.ParentId;

                    hierarchicalDest.ParentType = src.GetType().Name;

                    await HierarchyHelper.UpdateHierarchy(_ctx, hierarchicalDest, oldParent);
                }

                await _ctx.SaveChangesAsync();

                if (updateKeys.ContainsKey(nameof(IDeletable.Deleted).ToJsonFormat()))
                {
                    await HierarchyHelper.UpdateDeleted(_ctx, dest);
                }

                if (updateKeys.ContainsKey(nameof(ISortable.Weight).ToJsonFormat()))
                {
                    await WeightHelper.SetNewWeightAsync(_ctx, dest, oldWeight);
                }
                

                await _ctx.SaveChangesAsync();

                await trn.CommitAsync();
            }
            catch
            {
                await trn.RollbackAsync();
                throw;
            }
            
            return await GetRubricatorAsync(src.GetType(), id, requestedFields);
        }
        
        private async Task UpdateAsync<T>(T src, T dest, GraphQlKeyDictionary updateKeys) where T : IRubricatorFields
        {
            if (updateKeys.ContainsKey(nameof(src.SubSystemUIDs).ToJsonFormat()))
            {
                await SubsystemsSet(src, dest);
            }

            if (updateKeys.ContainsKey(nameof(src.Moderators).ToJsonFormat()))
            {
                await ModeratorsSet(src, dest);

                updateKeys.Remove(nameof(src.Moderators).ToJsonFormat());
            }

            Subsystem subsystem = null;
            var i2 = 0;
            if (updateKeys.ContainsKey(nameof(src.ExecutorIds).ToJsonFormat()))
            {
                subsystem = await ExecutorsSet(src, dest);
                i2 += src.ExecutorIds.Count();
            }
            subsystem = await ExecutorsChange(src, dest, subsystem, i2);

            i2 += src.ExecutorIdsChange.Count();

            BaseConstants.UpdateEntityByFieldsList(dest, src, updateKeys);
        }
        #endregion

        #region Private
        private async Task<IRubricator> GetRubricatorAsync(Type type, long id, GraphQlKeyDictionary requestedFields)
        {
            var dbSet = _ctx.DbSet<IRubricator>(type);

            if (dbSet == null)
            {
                return null;
            }

            return await dbSet
                .Where(el => !el.Deleted && el.Id == id)
                .IncludeByFields(requestedFields)
                .FirstOrDefaultAsync();
        }

        private static string GetSuffix<T>(T obj) where T : IRubricator
        {
            var suffix = obj switch
            {
                Rubric _ => SSClaimSuffix.Mu.rubrics,
                Topic _ => SSClaimSuffix.Mu.topics,
                Territory _ => SSClaimSuffix.Mu.territories,
                Region _ => SSClaimSuffix.Mu.regions,
                _ => throw new ArgumentOutOfRangeException(nameof(obj), obj, null)
            };

            return suffix;
        }

        private async Task SubsystemsSet<T>(T src, T dest) where T : IRubricatorFields
        {
            src.SubSystemUIDs ??= new List<string>();

            dest.SubsystemsClear();

            foreach (var ssUid in src.SubSystemUIDs ?? new List<string>())
            {
                var ss = await _ctx.Subsystems.Where(el => !el.Deleted && el.UID == ssUid).FirstOrDefaultAsync();
                if (ss == null)
                {
                    throw new Exception($"Subsystem '{ssUid}' not found");
                }
                dest.SubsystemsAdd(ss);
            }
        }

        private async Task<Subsystem> ExecutorsChange<T>(T src, T dest, Subsystem subsystem, int i2) where T : IRubricatorFields
        {
            for (var i = 0; i < src.ExecutorIdsChange.Count(); i++)
            {
                var executorId = Math.Abs(src.ExecutorIdsChange[i]);
                if (i + i2 < src.ExecutorIdsSubSystemUIDs.Count())
                {
                    subsystem = await _ctx.Subsystems.Where(el => !el.Deleted && el.UID == src.ExecutorIdsSubSystemUIDs[i + i2]).FirstOrDefaultAsync();
                    if (subsystem == null)
                    {
                        throw new Exception($"Subsystem '{src.ExecutorIdsSubSystemUIDs[i + i2]}' not found");
                    }
                }
                var executor = await _ctx.Users.Where(el => !el.Disconnected && el.Id == executorId).FirstOrDefaultAsync();
                if (executor == null)
                {
                    throw new Exception($"Executor '{executorId}' not found");
                }
                if (src.ExecutorIdsChange[i] > 0)
                {
                    if (subsystem == null)
                    {
                        foreach (var ss in dest.Subsystems)
                        {
                            dest.UserExecutorAdd(executor, ss);
                        }
                    }
                    else
                    {
                        dest.UserExecutorAdd(executor, subsystem);
                    }
                }
                else
                {
                    dest.UserExecutorRemove(executor, subsystem);
                }
            }
            return subsystem;
        }

        private async Task ModeratorsSet<T>(T src, T dest) where T : IRubricatorFields
        {
            foreach (var item in src.ModeratorsSet ?? new List<ClerkInSubsystemSet>())
            {
                if (dest.Subsystems.All(el => el.UID != item.SubsystemUid))
                {
                    throw new Exception($"Subsystem '{item.SubsystemUid}' not available to set");
                }

                var subsystem = await _ctx.Subsystems
                    .Where(el => !el.Deleted && el.UID == item.SubsystemUid)
                    .FirstOrDefaultAsync();

                if (subsystem == null)
                {
                    throw new Exception($"Subsystem '{item.SubsystemUid}' not found");
                }

                var destSubsystemClerks = dest.Moderators
                    .Where(el => el.SubsystemUid == item.SubsystemUid)
                    .SelectMany(el => el.Users).Distinct()
                    .ToArray();

                var itemUserIds = item.UserIds ?? new List<long>();

                var forDelete = (from du in destSubsystemClerks
                                 join uId in itemUserIds on du.Id equals uId
                                     into joins
                                 from j in joins.DefaultIfEmpty()
                                 where j == 0
                                 select du)
                    .ToArray();

                foreach (var clerk in forDelete)
                {
                    dest.UserModeratorRemove(clerk, subsystem);
                }

                var forAdd = (from uId in itemUserIds
                              join du in destSubsystemClerks on uId equals du.Id
                                  into joins
                              from j in joins.DefaultIfEmpty()
                              where j == null
                              select uId)
                    .Distinct()
                    .ToArray();

                var usersForAdd = await _ctx.Users
                    .Include(el => el.IdentityClaims)
                    .Include(el => el.UserRole)
                    .ThenInclude(el => el.Role)
                    .ThenInclude(el => el.IdentityClaims)
                    .Where(el => !el.Disconnected && forAdd.Contains(el.Id))
                    .ToArrayAsync();

                foreach (var clerk in usersForAdd)
                {
                    if (!clerk.AllClaims.Any(el => el.Type == ClaimName.Moderate && el.Value == $"{subsystem.UID}.{SSClaimSuffix.Md.enable}"))
                    {
                        throw new Exception($"User '{clerk.FullName}' не является Модератором для подсистемы {subsystem.UID}");
                    }

                    dest.UserModeratorAdd(clerk, subsystem);
                }
            }

            //Subsystem subsystem = null;
            //dest.UserModeratorClear();
            //for (var i = 0; i < src.ModeratorIds.Count; i++)
            //{
            //    if (i < src.ModeratorIdsSubSystemUIDs.Count)
            //    {
            //        subsystem = await _ctx.Subsystems.Where(el => !el.Deleted && el.UID == src.ModeratorIdsSubSystemUIDs[i]).FirstOrDefaultAsync();
            //        if (subsystem == null)
            //        {
            //            throw new Exception($"Subsystem '{src.ModeratorIdsSubSystemUIDs[i]}' not found");
            //        }
            //    }
            //    var moderator = await _ctx.Users.Where(el => !el.Disconnected && el.Id == src.ModeratorIds[i]).FirstOrDefaultAsync();
            //    if (moderator == null)
            //    {
            //        throw new Exception($"Moderator '{src.ModeratorIds[i]}' not found");
            //    }
            //    if (subsystem == null)
            //    {
            //        foreach (var ss in dest.Subsystems)
            //        {
            //            dest.UserModeratorAdd(moderator, ss);
            //        }
            //    }
            //    else
            //    {
            //        dest.UserModeratorAdd(moderator, subsystem);
            //    }
            //}
            //return subsystem;
        }

        private async Task<Subsystem> ExecutorsSet<T>(T src, T dest) where T : IRubricatorFields
        {
            Subsystem subsystem = null;
            dest.UserExecutorClear();
            for (int i = 0; i < src.ExecutorIds.Count(); i++)
            {
                var id = src.ExecutorIds[i];
                if (i < src.ExecutorIdsSubSystemUIDs.Count())
                {
                    var ssuid = src.ExecutorIdsSubSystemUIDs[i];
                    subsystem = await _ctx.Subsystems.Where(el => !el.Deleted && el.UID == ssuid).FirstOrDefaultAsync();
                    if (subsystem == null)
                    {
                        throw new Exception($"Subsystem '{ssuid}' not found");
                    }
                }
                var executor = await _ctx.Users.Where(el => !el.Disconnected && el.Id == id).FirstOrDefaultAsync();
                if (executor == null)
                {
                    throw new Exception($"Executor '{id}' not found");
                }
                if (subsystem == null)
                {
                    foreach (var ss in dest.Subsystems)
                    {
                        dest.UserExecutorAdd(executor, ss);
                    }
                }
                else
                {
                    dest.UserExecutorAdd(executor, subsystem);
                }
            }
            return subsystem;
        } 
        #endregion
    }
}
