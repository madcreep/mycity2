using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using BIT.MyCity.WebApi.Types.Pagination;
using log4net;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Managers
{
    public class EmailManager
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(EmailManager));

        private readonly IServiceProvider _serviceProvider;

        public EmailManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<Pagination<EmailTemplate>> GetTemplates(SelectionRange range, List<GraphQlQueryFilter> filter,
            GraphQlKeyDictionary requestedKeys)
        {
            var result = new Pagination<EmailTemplate>();

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            var request = context.EmailTemplates
                .FiltersBy(filter);

            if (requestedKeys.ContainsKey(nameof(Pagination<User>.Total).ToJsonFormat()))
            {
                result.Total = await request.LongCountAsync();
            }


            request = request
                .OrderByFieldNames(nameof(EmailTemplate.Key), range?.OrderBy)
                .GetRange(range?.Take, range?.Skip);

            result.Items = await request.ToArrayAsync();

            return result;
        }

        public async Task<EmailTemplate> UpdateTemplate(string key, EmailTemplate template)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return null;
            }

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            var dbTemplate = await context.EmailTemplates
                .Where(el => el.Key == key)
                .SingleOrDefaultAsync();

            if (dbTemplate == null)
            {
                _log.Error($"Ошибка редактирования шаблона Email. Шаблон с ключом <{key}> не найден.");
            }

            if (template == null || string.IsNullOrWhiteSpace(template.Template))
            {
                return dbTemplate;
            }

            if (dbTemplate.Template == template.Template)
            {
                return dbTemplate;
            }

            dbTemplate.Template = template.Template;

            await context.SaveChangesAsync();

            return dbTemplate;
        }

        public async Task<List<string>> CreateCustomMessage(CustomEmail msg)
        {
            var mailService = _serviceProvider.GetRequiredService<MailService>();

            var errors = await GetCustomMessageErrorsAsync(msg);

            if (errors.Any())
            {
                return errors;
            }

            var messageId = await mailService.SendEmailAsync(msg);

            if (messageId <= 0)
            {
                errors.Add("Ошибка добавления сообщения в базу данных");
            }

            return errors;
        }

        private async Task<List<string>> GetCustomMessageErrorsAsync(CustomEmail msg)
        {
            var result = new List<string>();

            if (msg.To == null || !msg.To.Any())
            {
                result.Add("Не указан адресат сообщения.");
            }
            else
            {
                var msgList = msg.To
                    .Where(el => string.IsNullOrWhiteSpace(el.Email));

                result.AddRange(msgList.Select(el => $"Не указан адрес электронной почты аолучателя ({(el.Name ?? "<undefined>")})"));
            }

            if (string.IsNullOrWhiteSpace(msg.Subject))
            {
                result.Add("Не указана тема сообщения.");
            }

            if (msg.FilesId == null || !msg.FilesId.Any())
            {
                return result;
            }

            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            var dbId = await ctx.AttachedFiles
                .Where(el => msg.FilesId.Contains(el.Id) && !el.Deleted)
                .Select(el => el.Id)
                .ToArrayAsync();

            var notId = msg.FilesId.Distinct()
                .Except(dbId)
                .ToArray();

            if (notId.Any())
            {
                result.Add($"Отсутствуют файлы с идентификаторами ({string.Join(", ", notId)})");
            }
            
            return result;
        }
    }
}
