using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Managers
{
    public class TerritoryManager
    {
        private readonly IServiceProvider _servicesProvider;

        public TerritoryManager(IServiceProvider servicesProvider)
        {
            _servicesProvider = servicesProvider;
        }

        public async Task<IdentityResult> CheckTerritoriesForNewUserAsync(List<UserSubsystemDirectoryId> territoryIds)
        {
            if (territoryIds == null || !territoryIds.Any())
            {
                return IdentityResult.Success;
            }

            var context = _servicesProvider.GetRequiredService<DocumentsContext>();

            var subsystems = await context.Subsystems
                .ToDictionaryAsync(el => el.UID);

            var result = new List<IdentityError>();

            foreach (var ssItem in territoryIds)
            {
                if (string.IsNullOrWhiteSpace(ssItem.SubsystemUid))
                {
                    result.Add(new IdentityError
                    {
                        Code = "TestTerritoriesError",
                        Description = "Не указана подсистема для терриорий"
                    });

                    continue;
                }

                if (!subsystems.ContainsKey(ssItem.SubsystemUid))
                {
                    result.Add(new IdentityError
                    {
                        Code = "TestTerritoriesError",
                        Description = $"Подсистема указанная для территорий \"{ssItem.SubsystemUid}\" отсутствует"
                    });

                    continue;
                }

                var testIds = ssItem.Ids
                    .Distinct()
                    .ToArray();

                if (testIds.Length != ssItem.Ids.Length)
                {
                    result.Add(new IdentityError
                    {
                        Code = "TestTerritoriesError",
                        Description = $"Переданы дублирующиеся территории для подсистемы \"{ssItem.SubsystemUid}\""
                    });
                }

                var ssTerritoryIds = subsystems[ssItem.SubsystemUid]
                    .TerritoryIds
                    ?? new List<long>();

                var contains = ssTerritoryIds
                    .Intersect(ssItem.Ids)
                    .ToArray();

                if (contains.Length != testIds.Length)
                {
                    var idStr = string.Join(" ", testIds.Except(ssTerritoryIds));

                    result.Add(new IdentityError
                    {
                        Code = "TestTerritoriesError",
                        Description = $"Переданы отсутствующие в подсистеме \"{ssItem.SubsystemUid}\" территории : {idStr}"
                    });
                }
            }
            
            return result.Any()
                ? IdentityResult.Failed(result.ToArray())
                : IdentityResult.Success;
        }
    }
}
