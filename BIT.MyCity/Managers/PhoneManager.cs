using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using BIT.MyCity.Sms.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SmsMessage = BIT.MyCity.Database.SmsMessage;

namespace BIT.MyCity.Managers
{
    public class PhoneManager
    {
        private readonly IServiceProvider _servicesProvider;

        public PhoneManager(IServiceProvider servicesProvider)
        {
            _servicesProvider = servicesProvider;
        }

        public async Task<OperationResult> SendConfirmPhoneSms(long phoneId)
        {
            var userManager = _servicesProvider.GetRequiredService<UserManager>();

            var userId = userManager.ClaimsPrincipalId();

            if (!userId.HasValue)
            {
                return new OperationResult("Пользователь не найден.");
            }

            var keys = new GraphQlKeyDictionary
            {
                {nameof(User.Phones).ToJsonFormat(), new GraphQlKeyDictionary()}
            };

            var user = await userManager.GetUserByIdAsync(userId.Value, keys);

            if (user == null || user.Disconnected)
            {
                return new OperationResult("Пользователь не найден.");
            }

            var userPhone = user.UserPhones
                .SingleOrDefault(el => el.PhoneId == phoneId);

            if (userPhone == null)
            {
                return new OperationResult("Не найден телефонный номер.");
            }

            if (userPhone.Phone.Type != TypePhone.Mobile)
            {
                return new OperationResult("Номер не является мобильным.");
            }

            if (userPhone.Confirmed)
            {
                return new OperationResult("Телефонный номер уже подтвержден.");
            }

            var context = _servicesProvider.GetRequiredService<DocumentsContext>();

            var messages = await context.SmsMessages
                .Where(el => el.UserId == userId.Value && el.PhoneNumber == NormalizePhoneNumber(userPhone.Phone.Number)
                                                       && el.SmsType == SmsType.ConfirmPhone)
                .ToArrayAsync();

            if (messages.Any())
            {
                var countDeliveredMessages = messages
                    .Count(el => el.Status == SmsMessageStatus.Delivered);

                if (countDeliveredMessages > 3)
                {
                    return new OperationResult("Вам уже отправлено 3 СМС подтверждения номера телефона.");
                }

                var lastMessage = messages
                    .OrderByDescending(el => el.CreatedAt)
                    .First();

                switch (lastMessage.Status)
                {
                    case SmsMessageStatus.FatalError:
                        return new OperationResult("На данный номер СМС не отправляются. Измените номер телефона.");
                    case SmsMessageStatus.Delivered when lastMessage.CreatedAt.Date == DateTime.UtcNow.Date:
                        return new OperationResult("СМС подтверждения уже отправлено. Повторное можно отправить завтра.");
                    case SmsMessageStatus.Created:
                    case SmsMessageStatus.Accepted:
                    case SmsMessageStatus.Queued:
                        return new OperationResult("СМС подтверждения уже отправлено. Ожидайте.");
                }
            }

            var code = GenerateConfirmCode();

            var settingsManager = _servicesProvider.GetRequiredService<SettingsManager>();

            var smsLifetime = settingsManager.SettingValue<uint>(SettingsKeys.SystemAuthSmsConfirmCodeLifetime);

            var lifetime = smsLifetime < 5 ? 10 : smsLifetime;

            var message = CreateConfirmPhoneMessage(userPhone.Phone, code, lifetime);

            message.UserId = user.Id;

            var smsService = _servicesProvider.GetRequiredService<SmsService>();

            await smsService.SendAsync(message);

            if (message.Status == SmsMessageStatus.Error
                || message.Status == SmsMessageStatus.BalanceError
                || message.Status == SmsMessageStatus.FatalError)
            {
                return new OperationResult(message.ErrorMessage);
            }

            userPhone.ConfirmCode = code;
            userPhone.ConfirmCodeLifeTime = DateTime.UtcNow.AddMinutes(lifetime);

            await context.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult> ConfirmPhone(long phoneId, string code)
        {
            var userManager = _servicesProvider.GetRequiredService<UserManager>();

            var userId = userManager.ClaimsPrincipalId();

            if (!userId.HasValue)
            {
                return new OperationResult("Пользователь не найден.");
            }

            var keys = new GraphQlKeyDictionary
            {
                {nameof(User.Phones).ToJsonFormat(), new GraphQlKeyDictionary()}
            };

            var user = await userManager.GetUserByIdAsync(userId.Value, keys);

            if (user == null || user.Disconnected)
            {
                return new OperationResult("Пользователь не найден.");
            }

            var userPhone = user.UserPhones
                .SingleOrDefault(el => el.PhoneId == phoneId);

            if (userPhone == null)
            {
                return new OperationResult("Не найден телефонный номер.");
            }

            if (userPhone.Phone.Type != TypePhone.Mobile)
            {
                return new OperationResult("Номер не является мобильным.");
            }

            if (userPhone.Confirmed)
            {
                return new OperationResult();
            }

            if (userPhone.ConfirmCode != code)
            {
                return new OperationResult("Номер не подтвержден.");
            }

            if (userPhone.ConfirmCodeLifeTime < DateTime.UtcNow)
            {
                return new OperationResult("Номер не подтвержден.");
            }

            userPhone.Confirmed = true;
            userPhone.ConfirmCode = null;
            userPhone.ConfirmCodeLifeTime = null;

            var context = _servicesProvider.GetRequiredService<DocumentsContext>();

            await context.SaveChangesAsync();

            return new OperationResult();

        }

        private static SmsMessage CreateConfirmPhoneMessage(Phone phone, string code, uint lifetime)
        {
            var number = NormalizePhoneNumber(phone.Number);

            var result = new SmsMessage
            {
                SmsType = SmsType.ConfirmPhone,
                PhoneNumber = number,
                LifeTime = DateTime.UtcNow.AddMinutes(lifetime - 2),
                Message = $"Код подтверждения номера телефона {code}"
            };

            return result;
        }

        private static string GenerateConfirmCode()
        {
            var rnd = new Random((int)DateTime.Now.Ticks);

            var value = rnd.Next(1, 999999);

            var result = $"{value:000000}";

            return result;
        }

        public static string NormalizePhoneNumber(string phoneNumber)
        {
            if (phoneNumber == null)
            {
                return null;
            }

            var result = Regex.Replace(phoneNumber, "[^\\d\\+]", string.Empty);

            return result;
        }
    }
}
