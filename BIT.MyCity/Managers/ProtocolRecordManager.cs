﻿using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Pagination;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BIT.MyCity.Managers
{
    public class ProtocolRecordManager
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        public ProtocolRecordManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
        }
        public async Task<Pagination<ProtocolRecord>> GetProtocolRecordAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<ProtocolRecord>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.ProtocolRecords
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<ProtocolRecord>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<ProtocolRecord>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }
    }
    public static class ProtocolRecordExtensions
    {
    }
}
