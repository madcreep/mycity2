using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Utilites;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL.Utilities;

namespace BIT.MyCity.Managers
{
    public class RoleManager
    {
        private readonly IServiceProvider _serviceProvider;

        public RoleManager(IServiceProvider servicesProvider)
        {
            _serviceProvider = servicesProvider;
        }

        public async Task<Pagination<Role>> GetRolesAsync(SelectionRange range, List<GraphQlQueryFilter> filter,
            GraphQlKeyDictionary requestedKeys)
        {
            var result = new Pagination<Role>();

                         var context = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));

            var query = (context.Roles as IQueryable<Role>)
                .FiltersBy(filter);

            if (requestedKeys.ContainsKey(nameof(Pagination<Role>.Total).ToJsonFormat()))
            {
                result.Total = await query.LongCountAsync();
            }

            if (!requestedKeys.ContainsKey(nameof(Pagination<Role>.Items).ToJsonFormat()))
            {
                return result;
            }

            query = query
                .IncludeByKeys(requestedKeys[nameof(Pagination<Role>.Items).ToJsonFormat()])
                .OrderByFieldNames(nameof(Role.Weight), range?.OrderBy)
                .GetRange(range?.Take, range?.Skip);

            var roles = await query.ToArrayAsync();

            result.Items = roles;

            return result;
        }

        public async Task<Role> GetRoleAsync(long id, GraphQlKeyDictionary requestedKeys)
        {
            var context = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));

            var query = context.Roles
                .ById(id)
                .IncludeByKeys(requestedKeys);

            var roles = await query.SingleOrDefaultAsync();

            return roles;
        }

        public async Task<Role> GetRole(long id, GraphQlKeyDictionary requestedKeys)
        {
            var context = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));

            var role = await context.Roles
                .ById(id)
                .IncludeByKeys(requestedKeys)
                .SingleOrDefaultAsync();

            return role;
        }

        internal async Task<bool> HasRoleWidthName(string roleName)
        {
            var roleManager = _serviceProvider.GetRequiredService<RoleManager<Role>>();

            return await roleManager.RoleExistsAsync(roleName);
        }

        public async Task<OperationResult<Role>> AddRoleAsync(Role model, GraphQlKeyDictionary keys, GraphQlKeyDictionary requestedKeys)
        {
            model.IdentityClaims = RemoveDublicatesClaims(model.IdentityClaims);

            var checkResult = (await CheckRole(model, keys))
                .ToList();

            if (!string.IsNullOrWhiteSpace(model.Name))
            {
                if (await HasRoleWidthName(model.Name))
                {
                    checkResult.Add("Роль с таким именем уже существует");
                }
            }

            var context = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));

            if (model.Weight <= 0)
            {
                var maxWeight = await context.Roles
                    .MaxAsync(el => (int?)el.Weight)
                    ?? 0;

                model.Weight = maxWeight + 1;
            }
            else
            {
                var hasWeight = context.Roles
                    .Any(el => el.Weight == model.Weight);

                if (hasWeight)
                {
                    checkResult.Add($"Роль с весом {model.Weight} уже существует");
                }
            }

            if (checkResult.Any())
            {
                return new OperationResult<Role>(null, checkResult);
            }

            model.Editable = true;
            model.IsAdminRegister = true;


            UpdateRoleNormalizeName(model);

            context.Roles.Add(model);

            await context.SaveChangesAsync();

            return new OperationResult<Role>(await GetRoleAsync(model.Id, requestedKeys));
        }

        public async Task<OperationResult<IEnumerable<Role>>> SortRolesAsync(IEnumerable<Role> roles)
        {
            var sortingRoles = roles as Role[] ?? roles.ToArray();

            if (!sortingRoles?.Any() ?? true)
            {
                return new OperationResult<IEnumerable<Role>>(null, "Для сортировки необходимо не менее одной записи");
            }

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            var ids = sortingRoles
                .Select(el => el.Id)
                .Distinct()
                .ToArray();

            if (sortingRoles.Count() != ids.Length)
            {
                return new OperationResult<IEnumerable<Role>>(null, "Переданы дублирующиеся роли");
            }

            var weights = sortingRoles
                .Select(el => el.Weight)
                .Distinct()
                .ToArray();

            if (sortingRoles.Length != weights.Length)
            {
                return new OperationResult<IEnumerable<Role>>(null, "Переданы дублирующиеся веса");
            }

            if (weights.Any(el => el <= 0))
            {
                return new OperationResult<IEnumerable<Role>>(null, "Веса не могут быть меньше 1");
            }

            var sortingDbRoles = context.Roles
                .Where(el => ids.Contains(el.Id))
                .ToArray();

            if (sortingRoles.Length != sortingDbRoles.Length)
            {
                return new OperationResult<IEnumerable<Role>>(null, "Переданы роли отсутсвующие в БД");
            }

            if (sortingRoles.Length > 1)
            {
                var dbWeigts = sortingDbRoles
                    .Select(el => el.Weight)
                    .Distinct();

                if (weights.Intersect(dbWeigts).Count() != weights.Length)
                {
                    return new OperationResult<IEnumerable<Role>>(null, "Переданы не валидные веса");
                }

                var forUpdate = (from dbRole in sortingDbRoles
                    join sRole in sortingRoles on dbRole.Id equals sRole.Id
                    where dbRole.Weight != sRole.Weight
                    select new { dbRole, sRole });

                var transaction = await context.Database.BeginTransactionAsync();

                foreach (var item in forUpdate)
                {
                    item.dbRole.Weight = -item.sRole.Weight;
                }

                await context.SaveChangesAsync();

                foreach (var item in forUpdate)
                {
                    item.dbRole.Weight = item.sRole.Weight;
                }

                await context.SaveChangesAsync();

                await transaction.CommitAsync();

                return new OperationResult<IEnumerable<Role>>(sortingDbRoles.OrderBy(el => el.Weight));
            }

            var insertedRole = sortingRoles.Single();

            var insertedDbRole = sortingDbRoles.Single();

            var updatedRoles = new List<Role>();

            updatedRoles.Add(insertedDbRole);

            if (insertedDbRole.Weight == insertedRole.Weight)
            {
                return new OperationResult<IEnumerable<Role>>(updatedRoles);
            }

            var movTop = insertedRole.Weight <= insertedDbRole.Weight;

            var startWeight = movTop
                ? insertedRole.Weight
                : insertedDbRole.Weight;

            var endWeight= movTop
                ? insertedDbRole.Weight
                : insertedRole.Weight;

            var requestRoles = context.Roles
                .Where(el => el.Id != insertedRole.Id && el.Weight >= startWeight && el.Weight <= endWeight);

            if (movTop)
            {
                requestRoles = requestRoles
                    .OrderBy(el => el.Weight);
            }
            else
            {
                var maxWeight = context.Roles
                        .Max(el => el.Weight);

                if (endWeight > maxWeight)
                {
                    endWeight = maxWeight;
                }

                requestRoles = requestRoles
                    .OrderByDescending(el => el.Weight);
            }

            sortingDbRoles = requestRoles
                .ToArray();

            var currentWeight = movTop
                ? startWeight
                : endWeight;

            insertedDbRole.Weight = -currentWeight;

            var transact = await context.Database.BeginTransactionAsync();

            foreach (var dbRole in sortingDbRoles)
            {
                if (dbRole.Weight != currentWeight)
                {
                    break;
                }

                currentWeight += movTop
                    ? 1
                    : -1;

                dbRole.Weight = -currentWeight;

                updatedRoles.Add(dbRole);
            }

            await context.SaveChangesAsync();

            foreach (var role in updatedRoles)
            {
                role.Weight = -role.Weight;
            }

            await context.SaveChangesAsync();

            await transact.CommitAsync();

            return new OperationResult<IEnumerable<Role>>(updatedRoles.OrderBy(el=>el.Weight));
        }

        public async Task<OperationResult<Role>> UpdateRoleAsync(long id, Role model, GraphQlKeyDictionary keys, GraphQlKeyDictionary requestedKeys)
        {
            model.IdentityClaims = RemoveDublicatesClaims(model.IdentityClaims);

            var checkResult = (await CheckRole(model, keys))
                .ToList();

            if (checkResult.Any())
            {
                return new OperationResult<Role>(null, checkResult);
            }

            var context = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));

            var dbRole = await GetRoleAsync(id, keys);

            if (dbRole == null)
            {
                return new OperationResult<Role>(null, $"Роль с id={id} не найдена");
            }

            if (!dbRole.Editable)
            {
                return new OperationResult<Role>(null, $"Роль с id={id} не редактируемая");
            }

            var transaction = await context.Database.BeginTransactionAsync();

            var identityResult = await UpdateRoleClaims(dbRole, model, keys);

            if (identityResult.Succeeded)
            {
                UpdateUtilites.UpdateEntityByFieldsList(dbRole, model, keys.Keys);

                UpdateRoleNormalizeName(dbRole);

                await context.SaveChangesAsync();

                await transaction.CommitAsync();

                return new OperationResult<Role>(await GetRoleAsync(id, requestedKeys));
            }

            await transaction.RollbackAsync();

            return new OperationResult<Role>(null, "Обновление роли не выполнено");
        }

        public async Task<bool> RemoveRole(long id)
        {
            var roleManager = (RoleManager<Role>)_serviceProvider.GetService(typeof(RoleManager<Role>));

            var role = await roleManager.FindByIdAsync(id.ToString());

            if (role == null)
            {
                return true;
            }

            if (!role.Editable)
            {
                return false;
            }

            var result = await roleManager.DeleteAsync(role);

            return result.Succeeded;
        }

        private async Task<IdentityResult> UpdateRoleClaims(Role dbRole, Role model, GraphQlKeyDictionary keys)
        {
            var claimKey = nameof(Role.Claims).ToJsonFormat();

            if (!keys.ContainsKey(claimKey))
            {
                return IdentityResult.Success;
            }

            keys.Remove(claimKey);

            if (model.IdentityClaims == null)
            {
                model.IdentityClaims = new List<IdentityRoleClaim<long>>();
            }

            var claimManager = _serviceProvider.GetRequiredService<ClaimManager>();

            var result = await claimManager.UpdateRoleClaimsAsync(dbRole, model.IdentityClaims.Select(el => new Claim(el.ClaimType, el.ClaimValue)));

            return result;
        }

        private async Task<IEnumerable<string>> CheckRole(Role model, GraphQlKeyDictionary keys)
        {
            var result = new List<string>();

            if (keys.ContainsKey(nameof(Role.Name).ToJsonFormat())
                && string.IsNullOrWhiteSpace(model.Name))
            {
                result.Add("Название роли не может быть пустым");
            }

            if (keys.ContainsKey(nameof(Role.IdentityClaims).ToJsonFormat()))
            {
                var claManager = _serviceProvider.GetRequiredService<ClaimManager>();

                if (!claManager.CheckClaims(model.IdentityClaims))
                {
                    result.Add("Переданы разрешения не применимые к роли");
                }
            }

            return result;
        }

        private List<IdentityRoleClaim<long>> RemoveDublicatesClaims(List<IdentityRoleClaim<long>> modelClaims)
        {
            return modelClaims?
                .OrderByDescending(el => el.Id)
                .GroupBy(el => new { el.ClaimType, el.ClaimValue })
                .Select(el => el.First())
                .ToList();
        }

        public async Task<IdentityResult> CheckRolesForNewUserAsync(IEnumerable<long> checkRolesIds)
        {
            if (checkRolesIds == null || !checkRolesIds.Any())
            {
                return IdentityResult.Success;
            }

            var result = new List<IdentityError>();

            var testIds = checkRolesIds
                .Distinct()
                .ToArray();

            if (checkRolesIds.Count() != testIds.Length)
            {
                result.Add(new IdentityError
                {
                    Code = "DublicateRoles",
                    Description = "Переданы дублирующиеся роли"
                });
            }

            var roleManager = _serviceProvider.GetRequiredService<RoleManager<Role>>();

            var dbRoles = await roleManager.Roles
                .Where(el => checkRolesIds.Distinct().Contains(el.Id))
                .Select(el => new { el.Id, el.Name, el.IsAdminRegister })
                .ToArrayAsync();

            if (dbRoles.Length != testIds.Length)
            {
                var idStr = string.Join(" ", testIds.Except(dbRoles.Select(el => el.Id)));

                result.Add(new IdentityError
                {
                    Code = "MissingRoles",
                    Description = $"Переданы отсутствующие роли : {idStr}"
                });
            }

            var notAddRoles = dbRoles
                .Where(el => !el.IsAdminRegister)
                .ToArray();

            if (notAddRoles.Any())
            {
                var nameStr = string.Join(" ", notAddRoles.Select(el => el.Name));

                result.Add(new IdentityError
                {
                    Code = "NotApplicableRoles",
                    Description = $"Переданы не приименимые роли : {nameStr}"
                });
            }

            return result.Any()
                ? IdentityResult.Failed(result.ToArray())
                : IdentityResult.Success;
        }

        private void UpdateRoleNormalizeName(Role role)
        {
            var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();

            var normalizeName = keyNormalizer.NormalizeName(role.Name);

            if (role.NormalizedName != normalizeName)
            {
                role.NormalizedName = normalizeName;
            }
        }
    }
}
