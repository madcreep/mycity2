using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Subsystems;
using BIT.MyCity.WebApi.Types.Pagination;
using log4net;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BIT.MyCity.Managers
{
    public class VoteManager
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        private readonly SettingsManager _sm;
        private readonly ILog _log = LogManager.GetLogger(typeof(VoteManager));
        public VoteManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
            _sm = _serviceProvider.GetRequiredService<SettingsManager>();
        }
        #region START
        public async Task<VoteRoot> VoteStartAsync(long id, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.VoteRoots
                .Where(el => el.Id == id)
                .Where(el => !el.Deleted)
                .Where(el => (el.State == VoteRoot.StatusEnum.IS_PLANNED) || (el.State == VoteRoot.StatusEnum.DRAFT))
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"Голосование {id} не найдено, не сконфигурировано, уже начато либо уже завершено");
            }
            var flags = await dest.GetAllowedActionsAsync(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Tune))
            {
                throw new Exception($"Вам это нельзя");
            }
            if (dest.State == VoteRoot.StatusEnum.DRAFT)
            {
                if (dest.StartAt.HasValue && dest.StartAt.Value > DateTime.UtcNow)
                {
                    dest.State = VoteRoot.StatusEnum.IS_PLANNED;
                    _log.Info($"Vote {dest.Id} Planned to {dest.StartAt.Value}");
                }
                else
                {
                    dest.State = VoteRoot.StatusEnum.IN_PROGRESS;
                    dest.StartedAt = DateTime.UtcNow;
                }
            }
            else if (dest.State == VoteRoot.StatusEnum.IS_PLANNED)
            {
                _log.Info($"VoteRoot {dest.Id} Force Started from {dest.State} state");
                dest.State = VoteRoot.StatusEnum.IN_PROGRESS;
                dest.StartedAt = DateTime.UtcNow;
            }

            await _ctx.SaveChangesAsync();
            return await GetVoteRootAsync(id, requestedFields);
        }
        #endregion
        #region STOP
        public async Task<VoteRoot> VoteStopAsync(long id, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.VoteRoots
                .Where(el => el.Id == id)
                .Where(el => !el.Deleted)
                .Where(el => (el.State == VoteRoot.StatusEnum.IN_PROGRESS) || (el.State == VoteRoot.StatusEnum.IS_DONE))
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"Голосование {id} не найдено, не начато или уже заархивировано");
            }
            var flags = await dest.GetAllowedActionsAsync(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Tune))
            {
                throw new Exception($"Вам это нельзя");
            }
            if (dest.State == VoteRoot.StatusEnum.IN_PROGRESS)
            {
                dest.State = VoteRoot.StatusEnum.IS_DONE;
                dest.EndedAt = DateTime.UtcNow;
                _log.Info($"Vote {dest.Id} Stopped");
            }
            else if (dest.State == VoteRoot.StatusEnum.IS_DONE)
            {
                dest.State = VoteRoot.StatusEnum.IS_ARCHIVED;
                dest.ArchivedAt = DateTime.UtcNow;
                _log.Info($"VoteRoot {dest.Id} Archived");
            }
            await _ctx.SaveChangesAsync();
            return await GetVoteRootAsync(id, requestedFields);
        }
        #endregion
        #region ACTION
        public async Task<Vote> VoteActionAsync(VoteAction vote, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var user = await _um.CurrentUser();
            if (user == null)
            {
                throw new Exception($"Аноноим не может принимать участия в опросах и голосованиях");
            }

            var trn = await _ctx.Database.BeginTransactionAsync();
            try
            {
                await VoteActionAsync(vote);
                await _ctx.SaveChangesAsync();
            }
            catch
            {
                await trn.RollbackAsync();
                throw;
            }
            await trn.CommitAsync();
            return await GetVoteAsync(vote.VoteId, requestedFields);
        }

        public async Task<Pagination<Vote>> VotesActionAsync(IEnumerable<VoteAction> votes, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var user = await _um.CurrentUser();
            if (user == null)
            {
                throw new Exception($"Аноноим не может принимать участия в опросах и голосованиях");
            }
            requestedFields.TryGetValue(nameof(Pagination<Vote>.Items).ToJsonFormat(), out var items);
            Pagination<Vote> result = new Pagination<Vote>();
            var trn = await _ctx.Database.BeginTransactionAsync();
            try
            {
                foreach (var vote in votes)
                {
                    await VoteActionAsync(vote);
                }
                await _ctx.SaveChangesAsync();
            }
            catch
            {
                await trn.RollbackAsync();
                throw;
            }
            await trn.CommitAsync();
            result.Items = await GetVotesAsync(votes.Select(el => el.VoteId).ToList(), requestedFields);
            result.Total = result.Items.Count();
            return result;
        }

        private async Task VoteActionAsync(VoteAction vote)
        {
            Vote dest = await _ctx.Votes
                .Where(el => el.Id == vote.VoteId)
                .Where(el => !el.Deleted)
                .Include(el => el.VoteItems)
                .Include(el => el.UserVote)
                .Include(el => el.VoteRoot)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"Голосование {vote.VoteId} не найдено");
            }
            if (dest.VoteRoot == null)
            {
                throw new Exception($"Голосование {vote.VoteId} не имеет корня");
            }
            if (dest.VoteRoot.State != VoteRoot.StatusEnum.IN_PROGRESS)
            {
                throw new Exception($"Голосование {vote.VoteId} еще не начато или уже завершено");
            }
            var flags = await dest.GetAllowedActionsAsync(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.VoteAllowed))
            {
                throw new Exception($"Вам нельзя голосовать");
            }
            var user = await _um.CurrentUser();
            if (dest.UserVote.Where(el => el.UserId == user.Id).Any())
            {
                throw new Exception($"Вы уже проголосовали а это {vote.VoteId}");
            }
            var Ids = new List<long>();
            foreach (var a in vote.VoteItemIds)
            {
                if (!dest.VoteItems.Select(el => el.Id).Contains(a))
                {
                    throw new Exception($"Нет пункта с id {a} в этом {vote.VoteId} голосовании");
                }
                if (!Ids.Contains(a))
                {
                    Ids.Add(a);
                }
            }
            foreach (var a in vote.VoteItemNumbers)
            {
                var b = dest.VoteItems.Where(el => el.Number == a).FirstOrDefault();
                if (b == null)
                {
                    throw new Exception($"Нет пункта с номером {a} в этом {vote.VoteId} голосовании");
                }
                if (!Ids.Contains(b.Id))
                {
                    Ids.Add(b.Id);
                }
            }
            if (Ids.Count == 0)
            {
                if (!dest.OfferEnabled || string.IsNullOrWhiteSpace(vote.Comment))
                {
                    throw new Exception($"Не выбрано ни одного пункта в {vote.VoteId}");
                }
            }
            if (dest.Mode == Vote.ModeEnum.ONE_SELECT && Ids.Count > 1)
            {
                throw new Exception($"В данном {vote.VoteId} голосовании нельзя выбирать больше одного пункта");
            }
            if (dest.Mode == Vote.ModeEnum.MANY_SELECT && Ids.Count > dest.ModeLimitValue)
            {
                throw new Exception($"В данном {vote.VoteId} голосовании нельзя выбирать больше {dest.ModeLimitValue} пунктов");
            }
            var uv = new UserVote { User = user, UserId = user.Id, Vote = dest, VoteId = dest.Id, VotepunktIds = Ids, Comment = vote.Comment };
            dest.UserVote.Add(uv);
            foreach (var vp in dest.VoteItems)
            {
                vp.Counter = dest.UserVote.Where(el => el.VotepunktIds.Contains(vp.Id)).Count();
            }
            dest.OffersCount = dest.UserVote.Where(el => !string.IsNullOrWhiteSpace(el.Comment)).Count();
        }
        #endregion
        private async Task<VoteRoot> GetVoteRootAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            var obj = await _ctx.VoteRoots
                   .Where(el => el.Id == id)
                   .IncludeByFields(requestedFields)
                   .IncludeByNotMappedFields(requestedFields)
                   .SingleOrDefaultAsync();
            return obj;
        }

        private async Task<Vote> GetVoteAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            var obj = await _ctx.Votes
                   .Where(el => el.Id == id)
                   .IncludeByFields(requestedFields)
                   .IncludeByNotMappedFields(requestedFields)
                   .SingleOrDefaultAsync();
            return obj;
        }

        private async Task<List<Vote>> GetVotesAsync(IEnumerable<long> ids, GraphQlKeyDictionary requestedFields)
        {
            var obj = await _ctx.Votes
                   .Where(el => ids.Contains(el.Id))
                   .IncludeByFields(requestedFields)
                   .IncludeByNotMappedFields(requestedFields)
                   .OrderBy(el => el.Weight)
                   .ToListAsync();
            return obj;
        }

        #region VOTE
        public VoteWIV GetWIV(Vote obj)
        {
            VoteWIV result = new VoteWIV();
            var userId = _um.ClaimsPrincipalId();
            if (userId != null)
            {
                var userVote = obj.UserVote.Where(el => el.UserId == userId).FirstOrDefault();
                if (userVote != null)
                {
                    result.Voted = true;
                    result.VoteItemsIds = userVote.VotepunktIds;
                    result.Text = userVote.Comment;
                }
            }
            return result;
        }
        public async Task<Pagination<Vote>> GetVotesAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<Vote>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.Votes
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm)
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<Vote>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<Vote>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }
        public async Task<Vote> CreateDbAsync(Vote obj, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var cl = _um.CurrentClaimsPrincipal();
            obj.VoteRoot = await BaseConstants.GetObjById(obj.VoteRootId, _ctx.VoteRoots);
            var flags = await obj.GetAllowedActionsAsync(_um.ClaimsPrincipalId(), cl, _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Create))
            {
                throw new Exception($"Вам нельзя создавать голосования");
            }

            var trn = await _ctx.Database.BeginTransactionAsync();
            try
            {
                if (obj.VoteItems != null)
                {
                    foreach (var el in obj.VoteItems)
                    {
                        await CreateDbAsync(el, null, null);
                    }
                    //                    obj.VoteItems.ForEach(async el => { await CreateDbAsync(el, null, null); }); - этот вариант однако валится. многопоточный чтоль?
                }
                else
                {
                    await BaseConstants.CheckAddIdsList(nameof(obj.VoteItems), obj, _ctx.VoteItems, el => el.VoteId.HasValue, el => throw new Exception($"VoteItem[{el.Id}] used by Vote[{el.VoteId.Value}]"));
                    await BaseConstants.ModifyIdsList(nameof(obj.VoteItems), obj, obj, _ctx.VoteItems, _ctx);
                }
                await BaseConstants.ModifyIdsList(nameof(obj.ParentVoteItems), obj, obj, _ctx.VoteItems, _ctx);
                await BaseConstants.ModifyIdsList(nameof(obj.AttachedFiles), obj, obj, _ctx.AttachedFiles, _ctx);
                _ctx.Votes.Add(obj);
                obj.ParentVoteItems?.ForEach(el => el.NextVote = obj);
                await _ctx.SaveWeightAsync(obj);
            }
            catch
            {
                await trn.RollbackAsync();
                throw;
            }
            await trn.CommitAsync();
            return await GetVoteAsync(obj.Id, requestedFields);
        }
        public async Task<Vote> UpdateDbAsync(long id, Vote src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.Votes
                .Where(el => el.Id == id)
                .Include(el => el.VoteRoot)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"Vote {id} not found");
            }
            src.VoteRoot = await BaseConstants.GetObjById(src.VoteRootId, _ctx.VoteRoots);
            var flags = await dest.GetAllowedActionsAsync(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам нельзя редактировать голосования");
            }
            if (dest.VoteRoot.State != VoteRoot.StatusEnum.DRAFT && !BaseConstants.VoteAlwaysEditableInSettings(_sm))
            {
                throw new Exception($"Редактировать можно только черновик голосования");
            }

            var trn = await _ctx.Database.BeginTransactionAsync();
            try
            {
                if (src.VoteItems != null)
                {
                    if (dest.VoteItems == null)
                    {
                        await _ctx.Entry(dest).Collection(el => el.VoteItems).LoadAsync();
                    }
                    await VoteItemsClear(dest);
                    foreach (var el in src.VoteItems)
                    {
                        dest.VoteItems.Add(await CreateDbAsync(el, null, null));
                    }
                    //                    src.VoteItems?.ForEach(async el => { dest.VoteItems.Add(await CreateDbAsync(el, null, null)); });
                }
                else
                {
                    await BaseConstants.ModifyIdsList(nameof(src.VoteItems), src, dest, _ctx.VoteItems, _ctx, el => el.VoteItems);
                }
                await BaseConstants.ModifyIdsList(nameof(src.ParentVoteItems), src, dest, _ctx.VoteItems, _ctx, el => el.ParentVoteItems);
                await BaseConstants.ModifyIdsList(nameof(src.AttachedFiles), src, dest, _ctx.AttachedFiles, _ctx, el => el.AttachedFiles);
                BaseConstants.UpdateEntityByFieldsList(dest, src, updateKeys, new string[] { "VoteItems" });
                dest.VoteRoot = src.VoteRoot ?? dest.VoteRoot;
                await _ctx.SaveChangesAsync();
            }
            catch
            {
                await trn.RollbackAsync();
                throw;
            }
            await trn.CommitAsync();
            return await GetVoteAsync(id, requestedFields);
        }

        private async Task VoteItemsClear(Vote dest)
        {
            foreach (var e in dest.VoteItems.Where(el => el != null))
            {
                if (e.AttachedFiles == null)
                {
                    await _ctx.Entry(e).Collection(el => el.AttachedFiles).LoadAsync();
                }
                e.AttachedFiles?.Clear();
                _ctx.VoteItems.Remove(e);
            }
            dest.VoteItems.Clear();
        }
        #endregion
        #region VOTEROOT
        public async Task<Pagination<VoteRoot>> GetVoteRootsAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<VoteRoot>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.VoteRoots
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm)
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<VoteRoot>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<VoteRoot>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                var it = await obj.ToListAsync();
                foreach (var a in it.Where(el => el.Votes != null))
                {
                    a.Votes = a.Votes.OrderBy(el => el.Weight).ToList();
                    foreach(var vo in a.Votes.Where(el => el.VoteItems != null))
                    {
                        vo.VoteItems = vo.VoteItems.OrderBy(el => el.Weight).ToList();
                    }
                }
                result.Items = it;
            }
            return result;
        }

        public async Task<VoteRoot> CreateDbAsync(VoteRoot obj, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            obj.Region = await BaseConstants.GetObjById(obj.RegionId, _ctx.Regions);
            obj.Rubric = await BaseConstants.GetObjById(obj.RubricId, _ctx.Rubrics);
            obj.Territory = await BaseConstants.GetObjById(obj.TerritoryId, _ctx.Territories);
            obj.Topic = await BaseConstants.GetObjById(obj.TopicId, _ctx.Topics);
            var flags = await obj.GetAllowedActionsAsync(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Create))
            {
                throw new Exception($"Вам нельзя создавать голосования");
            }
            if ((obj.StartAt ?? obj.EndAt ?? obj.ArchiveAt) != null && !flags.HasFlag(BaseConstants.AllowedActionFlags.Tune))
            {
                throw new Exception($"Вам нельзя задавать интервалы времени голосования");
            }
            if (!updateKeys.ContainsKey(nameof(VoteRoot.SubscribeToAnswers).ToJsonFormat()))
            {
                obj.SubscribeToAnswers = _sm.SettingValue<SubscribeToAnswersEnum>($"subsystem.{SSUID.vote}.subscribe_to_answers.");
            }

            obj.VotesIds = obj.VotesIds.AddIfNotExist(obj.VoteEntryId);
            if (obj.VotesIds != null)
            {
                await BaseConstants.CheckAddIdsList(nameof(obj.Votes), obj, _ctx.Votes, el => el.VoteRootId.HasValue, el => throw new Exception($"Узел {el.Id} уже принадлежит корню {el.VoteRootId.Value}"));
            }
            await BaseConstants.ModifyIdsList(nameof(obj.Votes), obj, obj, _ctx.Votes, _ctx);
            if (obj.VoteEntryId.HasValue)
            {
                obj.VoteEntry = obj.Votes.Single(el => el.Id == obj.VoteEntryId.Value);
            }
            await BaseConstants.ModifyIdsList(nameof(obj.AttachedFiles), obj, obj, _ctx.AttachedFiles, _ctx);
            _ctx.VoteRoots.Add(obj);
            await _ctx.SaveWeightAsync(obj);
            return await GetVoteRootAsync(obj.Id, requestedFields);
        }
        public async Task<VoteRoot> UpdateDbAsync(long id, VoteRoot src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.VoteRoots
                .Where(el => el.Id == id)
                .Include(el => el.VoteEntry)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"VoteRoot {id} not found");
            }
            var flags = await dest.GetAllowedActionsAsync(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам нельзя редактировать голосования");
            }
            if ((src.StartAt ?? src.EndAt ?? src.ArchiveAt) != null && !flags.HasFlag(BaseConstants.AllowedActionFlags.Tune))
            {
                throw new Exception($"Вам нельзя задавать интервалы времени голосования");
            }
            if (dest.State != VoteRoot.StatusEnum.DRAFT && !BaseConstants.VoteAlwaysEditableInSettings(_sm))
            {
                throw new Exception($"Редактировать можно только черновик голосования");
            }

            await BaseConstants.ModifyIdsList(nameof(src.AttachedFiles), src, dest, _ctx.AttachedFiles, _ctx, el => el.AttachedFiles);
            await BaseConstants.ModifyIdsList(nameof(src.Votes), src, dest, _ctx.Votes, _ctx, el => el.Votes);
            dest.VoteEntry = await BaseConstants.GetObjById(src.VoteEntryId, _ctx.Votes) ?? dest.VoteEntry;
            if (dest.VoteEntry != null && dest.Votes != null && !dest.Votes.Contains(dest.VoteEntry))
            {
                dest.VoteEntry = null;
            }
            dest.Region = await BaseConstants.GetObjById(src.RegionId, _ctx.Regions) ?? dest.Region;
            dest.Rubric = await BaseConstants.GetObjById(src.RubricId, _ctx.Rubrics) ?? dest.Rubric;
            dest.Territory = await BaseConstants.GetObjById(src.TerritoryId, _ctx.Territories) ?? dest.Territory;
            dest.Topic = await BaseConstants.GetObjById(src.TopicId, _ctx.Topics) ?? dest.Topic;
            BaseConstants.UpdateEntityByFieldsList(dest, src, updateKeys);
            await _ctx.SaveChangesAsync();
            return await GetVoteRootAsync(id, requestedFields);
        }
        #endregion
        #region VOTEITEM
        public async Task<Pagination<VoteItem>> GetVoteItemsAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<VoteItem>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.VoteItems
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm)
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<VoteItem>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<VoteItem>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }
        private async Task<VoteItem> GetVoteItemAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            return await _ctx.VoteItems
                    .Where(el => el.Id == id)
                    .IncludeByFields(requestedFields)
                    .IncludeByNotMappedFields(requestedFields)
                    .FirstOrDefaultAsync();
        }

        public async Task<VoteItem> CreateDbAsync(VoteItem obj, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            var flags = await obj.GetAllowedActionsAsync(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Create))
            {
                throw new Exception($"Вам это нельзя");
            }
            obj.Vote = await NewMethod(obj.VoteId);
            obj.NextVote = await NewMethod(obj.NextVoteId);
            NewMethod1(obj);
            obj.Vote?.VoteItems.Add(obj);
            _ctx.VoteItems.Add(obj);
            await BaseConstants.ModifyIdsList(nameof(obj.AttachedFiles), obj, obj, _ctx.AttachedFiles, _ctx);
            await _ctx.SaveWeightAsync(obj);
            return await GetVoteItemAsync(obj.Id, requestedFields);
        }

        private static void NewMethod1(VoteItem obj)
        {
            if (obj.Vote?.VoteRoot != null && obj.NextVote?.VoteRoot != null && obj.Vote.VoteRoot != obj.NextVote.VoteRoot)
            {
                throw new Exception($"Текущий и следующий узел принадлежат разным голосованиям ({obj.Vote.Id}=>{obj.Vote.VoteRoot.Id} <> {obj.NextVote.Id}=>{obj.NextVote.VoteRoot.Id})");
            }
        }
        public async Task<VoteItem> UpdateDbAsync(long id, VoteItem src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.VoteItems
                .Where(el => el.Id == id)
                .Include(el => el.Vote).ThenInclude(el => el.VoteRoot)
                .Include(el => el.NextVote).ThenInclude(el => el.VoteRoot)
                .FirstOrDefaultAsync();
            if (dest.Vote?.VoteRoot != null && dest.Vote.VoteRoot.State != VoteRoot.StatusEnum.DRAFT && !BaseConstants.VoteAlwaysEditableInSettings(_sm))
            {
                throw new Exception($"Можно изменять пункты только в черновике голосования {id}");
            }
            var flags = await dest.GetAllowedActionsAsync(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx);
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя {id}");
            }
            src.Vote = await NewMethod(src.VoteId) ?? dest.Vote;
            src.NextVote = await NewMethod(src.NextVoteId) ?? dest.NextVote;
            NewMethod1(src);
            await BaseConstants.ModifyIdsList(nameof(src.AttachedFiles), src, dest, _ctx.AttachedFiles, _ctx, el => el.AttachedFiles);
            BaseConstants.UpdateEntityByFieldsList(dest, src, updateKeys);
            dest.Vote = src.Vote;
            dest.NextVote = src.NextVote;
            await _ctx.SaveChangesAsync();
            return await GetVoteItemAsync(id, requestedFields);
        }
        #endregion
        private async Task<Vote> NewMethod(long? id)
        {
            Vote vote = null;
            if (id.HasValue)
            {
                vote = await _ctx.Votes
                    .Where(el => !el.Deleted)
                    .Where(el => el.Id == id.Value)
                    .Include(el => el.VoteRoot)
                    .FirstOrDefaultAsync();
                if (vote == null)
                {
                    throw new Exception($"Не найден узел {id.Value}");
                }
                if (vote?.VoteRoot != null && vote.VoteRoot.State != VoteRoot.StatusEnum.DRAFT)
                {
                    throw new Exception($"Пункты можно добавлять только в черновик голосования (id {id.Value})");
                }
            }
            return vote;
        }

    }
    public static class VoteExtensions
    {
        public static IQueryable<Vote> IncludeByNotMappedFields(this IQueryable<Vote> obj, GraphQlKeyDictionary fields)
        {
            if (fields != null && obj != null)
            {
                if (IncludePredicateUserVoteUser(fields))
                {
                    obj = obj.Include(el => el.UserVote).ThenInclude(el => el.User);
                }
                else if (IncludePredicateUserVote(fields))
                {
                    obj = obj.Include(el => el.UserVote);
                }
            }
            return obj;
        }

        private static bool IncludePredicateUserVoteUser(GraphQlKeyDictionary f)
        {
            return
                f.ContainsKey(nameof(Vote.Users).ToJsonFormat()) ||
                f.ContainsKey(nameof(Vote.Offers).ToJsonFormat());
        }

        private static bool IncludePredicateUserVote(GraphQlKeyDictionary f)
        {
            return
                f.ContainsKey(nameof(Vote.UsersCount).ToJsonFormat()) ||
                f.ContainsKey(nameof(Vote.VoteWIV).ToJsonFormat());
        }

        public static IQueryable<VoteRoot> IncludeByNotMappedFields(this IQueryable<VoteRoot> obj, GraphQlKeyDictionary fields)
        {
            if (fields != null && obj != null)
            {
                if (
                    fields.ContainsKey(nameof(VoteRoot.Votes).ToJsonFormat())
                    && IncludePredicateUserVoteUser(fields[nameof(VoteRoot.Votes).ToJsonFormat()])
                    )
                {
                    obj = obj.Include(el => el.Votes).ThenInclude(el => el.UserVote).ThenInclude(el => el.User);
                }
                else if (
                   (
                   fields.ContainsKey(nameof(VoteRoot.Votes).ToJsonFormat())
                   && IncludePredicateUserVote(fields[nameof(VoteRoot.Votes).ToJsonFormat()])
                   ) || fields.ContainsKey(nameof(VoteRoot.UsersCount).ToJsonFormat())
                   )
                {
                    obj = obj.Include(el => el.Votes).ThenInclude(el => el.UserVote);
                }
            }
            return obj;
        }

        public static IQueryable<Vote> FilterByReadAllowed(
            this IQueryable<Vote> main,
            long? userId,
            ClaimsPrincipal claimsPrincipal,
            SettingsManager sm
            )
        {
            if (GAAExtensions.GetAASS(SSClaimSuffix.Ss.create_voting, claimsPrincipal?.Claims, SSUID.vote).HasFlag(BaseConstants.AllowedActionFlags.Read))
            {
                return main;
            }
            var ss_enabled = new List<string>();
            var result = main.Where(el => false);
            var publ = main.Where(el => false);
            if (sm.SettingValue<bool>($"subsystem.{SSUID.vote}.enable."))
            {
                publ = publ.Concat(
                    main.Where(el =>
                        el.VoteRoot.State == VoteRoot.StatusEnum.IS_PLANNED ||
                        el.VoteRoot.State == VoteRoot.StatusEnum.IN_PROGRESS ||
                        el.VoteRoot.State == VoteRoot.StatusEnum.IS_DONE
                        )
                    );
            }
            if (userId.HasValue)
            {
                result = result.Concat(main.FilterByReadAllowedSubsystemRights(claimsPrincipal));
                result = result.Concat(main.FilterByReadAllowedRubricatorModerator(userId));
            }

            return result.Concat(publ).Distinct();
        }

        public static IQueryable<VoteRoot> FilterByReadAllowed(
            this IQueryable<VoteRoot> main,
            long? userId,
            ClaimsPrincipal claimsPrincipal,
            SettingsManager sm
            )
        {
            if (GAAExtensions.GetAASS(SSClaimSuffix.Ss.create_voting, claimsPrincipal?.Claims, SSUID.vote).HasFlag(BaseConstants.AllowedActionFlags.Read))
            {
                return main;
            }
            var ss_enabled = new List<string>();
            var result = main.Where(el => false);
            var publ = main.Where(el => false);
            if (sm.SettingValue<bool>($"subsystem.{SSUID.vote}.enable."))
            {
                publ = publ.Concat(
                    main.Where(el =>
                        el.State == VoteRoot.StatusEnum.IS_PLANNED ||
                        el.State == VoteRoot.StatusEnum.IN_PROGRESS ||
                        el.State == VoteRoot.StatusEnum.IS_DONE
                        )
                    );
            }
            if (userId.HasValue)
            {
                result = result.Concat(main.FilterByReadAllowedSubsystemRights(userId, claimsPrincipal?.Claims, SSUID.vote));
                result = result.Concat(main.FilterByReadAllowedRubricatorModerator(userId, SSUID.vote));
            }

            return result.Concat(publ).Distinct();
        }

        public static IQueryable<Vote> FilterByReadAllowedSubsystemRights(
            this IQueryable<Vote> main,
            ClaimsPrincipal claimsPrincipal
            )
        {
            QueriableFilterExtensions.GetFromClaimsARs(claimsPrincipal?.Claims, out List<string> ss_all_regions, out List<string> ss_all_rubrics, out List<string> ss_all_territories, out List<string> ss_all_topics);
            return
                main.Where(el => el.VoteRoot.Region != null && ss_all_regions.Contains(SSUID.vote))
            .Concat(main.Where(el => el.VoteRoot.Rubric != null && ss_all_rubrics.Contains(SSUID.vote)))
            .Concat(main.Where(el => el.VoteRoot.Territory != null && ss_all_territories.Contains(SSUID.vote)))
            .Concat(main.Where(el => el.VoteRoot.Topic != null && ss_all_topics.Contains(SSUID.vote)));
        }

        public static IQueryable<Vote> FilterByReadAllowedRubricatorModerator(
            this IQueryable<Vote> main,
            long? userId
            )
        {
            return main.Where(el => el.VoteRoot.Region != null && el.VoteRoot.Region.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == SSUID.vote))
                .Concat(main.Where(el => el.VoteRoot.Rubric != null && el.VoteRoot.Rubric.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == SSUID.vote)))
                .Concat(main.Where(el => el.VoteRoot.Territory != null && el.VoteRoot.Territory.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == SSUID.vote)))
                .Concat(main.Where(el => el.VoteRoot.Topic != null && el.VoteRoot.Topic.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == SSUID.vote)));
        }

        public static IQueryable<VoteItem> FilterByReadAllowedSubsystemRights(
            this IQueryable<VoteItem> main,
            ClaimsPrincipal claimsPrincipal
            )
        {
            QueriableFilterExtensions.GetFromClaimsARs(claimsPrincipal?.Claims, out List<string> ss_all_regions, out List<string> ss_all_rubrics, out List<string> ss_all_territories, out List<string> ss_all_topics);
            return
                main.Where(el => el.Vote.VoteRoot.Region != null && ss_all_regions.Contains(SSUID.vote))
            .Concat(main.Where(el => el.Vote.VoteRoot.Rubric != null && ss_all_rubrics.Contains(SSUID.vote)))
            .Concat(main.Where(el => el.Vote.VoteRoot.Territory != null && ss_all_territories.Contains(SSUID.vote)))
            .Concat(main.Where(el => el.Vote.VoteRoot.Topic != null && ss_all_topics.Contains(SSUID.vote)));
        }
        public static IQueryable<VoteItem> FilterByReadAllowedRubricatorModerator(
            this IQueryable<VoteItem> main,
            long? userId
            )
        {
            return main.Where(el => el.Vote.VoteRoot.Region != null && el.Vote.VoteRoot.Region.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == SSUID.vote))
                .Concat(main.Where(el => el.Vote.VoteRoot.Rubric != null && el.Vote.VoteRoot.Rubric.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == SSUID.vote)))
                .Concat(main.Where(el => el.Vote.VoteRoot.Territory != null && el.Vote.VoteRoot.Territory.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == SSUID.vote)))
                .Concat(main.Where(el => el.Vote.VoteRoot.Topic != null && el.Vote.VoteRoot.Topic.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == SSUID.vote)));
        }

        public static IQueryable<VoteItem> FilterByReadAllowed(
            this IQueryable<VoteItem> main,
            long? userId,
            ClaimsPrincipal claimsPrincipal,
            SettingsManager sm
            )
        {
            var ss_enabled = new List<string>();
            var result = main.Where(el => false);
            var publ = main.Where(el => false);
            if (sm.SettingValue<bool>($"subsystem.{SSUID.vote}.enable."))
            {
                publ = publ.Concat(
                    main.Where(el =>
                        el.Vote.VoteRoot.State == VoteRoot.StatusEnum.IS_PLANNED ||
                        el.Vote.VoteRoot.State == VoteRoot.StatusEnum.IN_PROGRESS ||
                        el.Vote.VoteRoot.State == VoteRoot.StatusEnum.IS_DONE
                        )
                    );
            }
            if (userId.HasValue)
            {
                result = result.Concat(main.FilterByReadAllowedSubsystemRights(claimsPrincipal));
                result = result.Concat(main.FilterByReadAllowedRubricatorModerator(userId));
            }

            return result.Concat(publ).Distinct();
        }


        private static VoteRoot.StatusEnum GetVoteState(this VoteRoot obj)
        {
            return obj != null ? obj.State : default;
        }

        private static VoteRoot.StatusEnum GetVoteState(this Vote obj)
        {
            return obj?.VoteRoot != null ? obj.VoteRoot.State : default;
        }

        private static VoteRoot.StatusEnum GetVoteState(this VoteItem obj)
        {
            return obj?.Vote?.VoteRoot != null ? obj.Vote.VoteRoot.State : default;
        }

        public static async Task<BaseConstants.AllowedActionFlags> GetAllowedActionsAsync(
            this VoteRoot obj,
            long? userId,
            ClaimsPrincipal claimsPrincipal,
            SettingsManager sm,
            DocumentsContext ctx
            )
        {
            BaseConstants.AllowedActionFlags flags = BaseConstants.AllowedActionFlags.None;
            if (obj != null)
            {
                flags |= GetVoteClaimsFlagsByState(claimsPrincipal, obj.GetVoteState());
                flags |= GAAExtensions.GetAASS(SSClaimSuffix.Ss.create_voting, claimsPrincipal?.Claims, SSUID.vote);
                flags |= await obj.GetAllowedModerationActions(userId, claimsPrincipal?.Claims, sm, ctx, SSUID.vote);
            }
            if (
                !flags.HasFlag(BaseConstants.AllowedActionFlags.Moderate) &&
                !BaseConstants.SubsystemIsEnabledInSettings(sm, SSUID.vote)
                )
            {
                flags = BaseConstants.AllowedActionFlags.None;
            }
            return flags;
        }

        public static async Task<BaseConstants.AllowedActionFlags> GetAllowedActionsAsync(
            this Vote obj,
            long? userId,
            ClaimsPrincipal claimsPrincipal,
            SettingsManager sm,
            DocumentsContext ctx
            )
        {
            BaseConstants.AllowedActionFlags flags = BaseConstants.AllowedActionFlags.None;
            if (obj != null)
            {
                flags |= GetVoteClaimsFlagsByState(claimsPrincipal, obj.GetVoteState());
                flags |= GAAExtensions.GetAASS(SSClaimSuffix.Ss.create_voting, claimsPrincipal?.Claims, SSUID.vote);
                await obj.LoadReference(obj.VoteRoot, ctx);
                flags |= await obj.VoteRoot.GetAllowedModerationActions(userId, claimsPrincipal?.Claims, sm, ctx, SSUID.vote);
            }
            if (
                    !flags.HasFlag(BaseConstants.AllowedActionFlags.Moderate) &&
                    !BaseConstants.SubsystemIsEnabledInSettings(sm, SSUID.vote)
                    )
            {
                flags = BaseConstants.AllowedActionFlags.None;
            }
            return flags;
        }

        public static async Task<BaseConstants.AllowedActionFlags> GetAllowedActionsAsync(
            this VoteItem obj,
            long? userId,
            ClaimsPrincipal claimsPrincipal,
            SettingsManager sm,
            DocumentsContext ctx
            )
        {
            BaseConstants.AllowedActionFlags flags = BaseConstants.AllowedActionFlags.None;
            if (obj != null)
            {
                flags |= GetVoteClaimsFlagsByState(claimsPrincipal, obj.GetVoteState());
                flags |= GAAExtensions.GetAASS(SSClaimSuffix.Ss.create_voting, claimsPrincipal?.Claims, SSUID.vote);
                await obj.LoadReference(obj.Vote, ctx);
                if (obj.Vote != null)
                {
                    await obj.Vote.LoadReference(obj.Vote.VoteRoot, ctx);
                    flags |= await obj.Vote.VoteRoot.GetAllowedModerationActions(userId, claimsPrincipal?.Claims, sm, ctx, SSUID.vote);
                }
            }
            if (
                    !flags.HasFlag(BaseConstants.AllowedActionFlags.Moderate) &&
                    !BaseConstants.SubsystemIsEnabledInSettings(sm, SSUID.vote)
                    )
            {
                flags = BaseConstants.AllowedActionFlags.None;
            }
            return flags;
        }

        private static BaseConstants.AllowedActionFlags GetVoteClaimsFlagsByState(ClaimsPrincipal claimsPrincipal, VoteRoot.StatusEnum state)
        {
            BaseConstants.AllowedActionFlags flags = BaseConstants.AllowedActionFlags.None;
            if (!claimsPrincipal.Claims.Any(el => el.Type == ClaimName.Subsystem && el.Value == $"{SSUID.vote}.{SSClaimSuffix.Ss.available}"))
            {
                return flags;
            }
            if (
                state == VoteRoot.StatusEnum.IS_PLANNED ||
                state == VoteRoot.StatusEnum.IN_PROGRESS ||
                state == VoteRoot.StatusEnum.IS_DONE
                )
            {
                flags |= BaseConstants.AllowedActionFlags.Read;
                if (claimsPrincipal.Claims.Any(el => el.Type == ClaimName.Subsystem && el.Value == $"{SSUID.vote}.{SSClaimSuffix.Ss.allowed}"))
                {
                    flags |= BaseConstants.AllowedActionFlags.VoteAllowed;
                }
            }
            if (claimsPrincipal.Claims.Any(el => el.Type == ClaimName.Manage && el.Value == $"{SSUID.vote}.{SSClaimSuffix.Mu.control}"))
            {
                flags |= BaseConstants.AllowedActionFlags.Read;
                flags |= BaseConstants.AllowedActionFlags.Tune;
            }
            if (claimsPrincipal.Claims.Any(el => el.Type == ClaimName.Subsystem && el.Value == $"{SSUID.vote}.{SSClaimSuffix.Ss.create_voting}"))
            {
                flags |= BaseConstants.AllowedActionFlags.Read;
                flags |= BaseConstants.AllowedActionFlags.Edit;
                flags |= BaseConstants.AllowedActionFlags.Create;
            }
            if (claimsPrincipal.Claims.Any(el => el.Type == ClaimName.Moderate && el.Value == $"{SSUID.vote}.{SSClaimSuffix.Md.enable}"))
            {
                flags |= BaseConstants.AllowedActionFlags.Read;
                flags |= BaseConstants.AllowedActionFlags.Edit;
                flags |= BaseConstants.AllowedActionFlags.Moderate;
            }
            return flags;
        }
    }
}

