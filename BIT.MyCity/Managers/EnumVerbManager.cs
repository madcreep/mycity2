using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Subsystems;
using BIT.MyCity.WebApi.Types.Pagination;
using log4net;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BIT.MyCity.Managers
{
    public class EnumVerbManager
    {
        private readonly ILog _log = log4net.LogManager.GetLogger(typeof(EnumVerbManager));
        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        public EnumVerbManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
        }

        public async Task<Pagination<EnumVerb>> GetEnumVerbsAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<EnumVerb>.Items).ToJsonFormat(), out var items);

            var obj = _ctx.EnumVerbs
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<EnumVerb>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<EnumVerb>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }

        private async Task<EnumVerb> GetEnumVerbAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            return await _ctx.EnumVerbs
                    .Where(el => el.Id == id)
                    .IncludeByFields(requestedFields)
                    .IncludeByNotMappedFields(requestedFields)
                    .FirstOrDefaultAsync();
        }

        internal async Task<EnumVerb> GetEnumVerbAsync(string value, string subsystemUid, string group)
        {
            return await _ctx.EnumVerbs
                .FirstOrDefaultAsync(el =>
                    el.Value == value &&
                    el.SubsystemUID == subsystemUid &&
                    el.Group == group);
        }

        internal async Task<EnumVerb> GetEnumVerbAsync(int value, string subsystemUid, string group)
        {
            return await GetEnumVerbAsync(value.ToString(), subsystemUid, group);
        }

        public async Task<EnumVerb> CreateDbAsync(EnumVerb obj, GraphQlKeyDictionary updatedKeys, GraphQlKeyDictionary requestedFields)
        {
            if (_um.CurrentClaimsPrincipal().Claims.All(el => el.Type != ClaimName.SuperAdmin))
            {
                throw new Exception($"Access denied");
            }
            if (await _ctx.EnumVerbs.AnyAsync(el
            => el.Value == obj.Value
            && el.Group == obj.Group
            && el.SubsystemUID == obj.SubsystemUID
            ))
            {
                throw new Exception($"EnumVerb '{obj.Value},{obj.Group},{obj.SubsystemUID}' already exist");
            }
            if (!string.IsNullOrWhiteSpace(obj.SubsystemUID) && !await _ctx.Subsystems.AnyAsync(el => !el.Deleted && el.UID == obj.SubsystemUID))
            {
                throw new Exception($"Subsystem '{obj.SubsystemUID}' not found");
            }
            if (obj.CustomSettings != null)
            {
                if (!obj.CustomSettings.IsJson())
                {
                    throw new Exception($"EnumVerb.CustomSettings is not valid JSON object");
                }
            }
            _ctx.EnumVerbs.Add(obj);
            await _ctx.SaveChangesAsync();
            return await GetEnumVerbAsync(obj.Id, requestedFields);
        }

        public async Task<EnumVerb> UpdateDbAsync(long id, EnumVerb obj, GraphQlKeyDictionary updatedKeys, GraphQlKeyDictionary requestedFields)
        {
            if (!_um.CurrentClaimsPrincipal().Claims.Any(el => el.Type == ClaimName.SuperAdmin))
            {
                throw new Exception($"Access denied");
            }

            var dest = await _ctx.EnumVerbs
                .Where(el => el.Id == id).FirstOrDefaultAsync();

            if (dest == null)
            {
                throw new Exception($"EnumVerb '{id}' not found");
            }
            if (updatedKeys.ContainsKey(nameof(obj.CustomSettings).ToJsonFormat()))
            {
                if (!obj.CustomSettings.IsJson())
                {
                    throw new Exception($"EnumVerb.CustomSettings is not valid JSON object");
                }
            }

            return await UpdateDbAsync0(obj, dest, updatedKeys, requestedFields);
        }

        public async Task<EnumVerb> UpdateDbAsync(string value, string group, string subsystemUID, EnumVerb obj, GraphQlKeyDictionary updatedKeys, GraphQlKeyDictionary requestedFields)
        {
            if (!_um.CurrentClaimsPrincipal().Claims.Any(el => el.Type == ClaimName.SuperAdmin))
            {
                throw new Exception($"Access denied");
            }
            var dest = await _ctx.EnumVerbs.Where(el
                => el.Value == value
                && el.Group == group
                && el.SubsystemUID == subsystemUID
                )
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"EnumVerb '{value},{group},{subsystemUID}' not found");
            }

            return await UpdateDbAsync0(obj, dest, updatedKeys, requestedFields);
        }

        private async Task<EnumVerb> UpdateDbAsync0(EnumVerb obj, EnumVerb dest, GraphQlKeyDictionary updatedKeys, GraphQlKeyDictionary requestedFields)
        {
            if (updatedKeys.ContainsKey(nameof(obj.SubsystemUID).ToJsonFormat()))
            {
                if (!(await _ctx.Subsystems.AnyAsync(el => !el.Deleted && el.UID == obj.SubsystemUID)))
                {
                    throw new Exception($"Subsystem '{obj.SubsystemUID}' not found");
                }
            }
            if (!updatedKeys.ContainsKey(nameof(obj.Value).ToJsonFormat()))
            {
                obj.Value = dest.Value;
            }
            if (!updatedKeys.ContainsKey(nameof(obj.Group).ToJsonFormat()))
            {
                obj.Group = dest.Group;
            }
            if (!updatedKeys.ContainsKey(nameof(obj.SubsystemUID).ToJsonFormat()))
            {
                obj.SubsystemUID = dest.SubsystemUID;
            }
            if (!updatedKeys.ContainsKey(nameof(obj.CustomSettings).ToJsonFormat()))
            {
                obj.CustomSettings = dest.CustomSettings;
            }
            var d2 = await _ctx.EnumVerbs.Where(el
                => el.Value == obj.Value
                && el.Group == obj.Group
                && el.SubsystemUID == obj.SubsystemUID
                )
                .FirstOrDefaultAsync();

            if (d2 != null && d2.Id != dest.Id)
            {
                throw new Exception($"EnumVerb '{obj.Value},{obj.Group},{obj.SubsystemUID}' already exist at {d2.Id}");
            }

            if (dest.Deleted && (!updatedKeys.ContainsKey(nameof(obj.Deleted).ToJsonFormat()) || obj.Deleted))
            {
                throw new Exception("EnumVerb update not allowed in Deleted state");
            }

            BaseConstants.UpdateEntityByFieldsList(dest, obj, updatedKeys,
                new[]
                {
                    nameof(EnumVerb.Id),
                }
            );

            await _ctx.SaveChangesAsync();

            return await GetEnumVerbAsync(dest.Id, requestedFields);
        }

        public async Task<EnumVerb> UpdateDbAsync(EnumVerb obj, GraphQlKeyDictionary updatedKeys, GraphQlKeyDictionary requestedFields)
        {
            if (_um.CurrentClaimsPrincipal().Claims.All(el => el.Type != ClaimName.SuperAdmin))
            {
                throw new Exception($"Access denied");
            }
            EnumVerb dest;
            if (updatedKeys.ContainsKey(nameof(obj.Id).ToJsonFormat()))
            {
                dest = await _ctx.EnumVerbs.Where(el => el.Id == obj.Id).FirstOrDefaultAsync();
                if (dest == null)
                {
                    throw new Exception($"EnumVerb '{obj.Id}' not found");
                }
            }
            else
            {
                dest = await _ctx.EnumVerbs.Where(el
                    => el.Value == obj.Value
                    && el.Group == obj.Group
                    && el.SubsystemUID == obj.SubsystemUID
                    )
                    .FirstOrDefaultAsync();
                if (dest == null)
                {
                    throw new Exception($"EnumVerb '{obj.Value},{obj.Group},{obj.SubsystemUID}' not found");
                }
            }
            return await UpdateDbAsync0(obj, dest, updatedKeys, requestedFields);
        }

        public async Task<EnumVerb> WriteDbAsync(EnumVerb obj, GraphQlKeyDictionary updatedKeys, GraphQlKeyDictionary requestedFields)
        {
            if (_um.CurrentClaimsPrincipal().Claims.All(el => el.Type != ClaimName.SuperAdmin))
            {
                throw new Exception($"Access denied");
            }
            var dest = await _ctx.EnumVerbs.Where(el
                => el.Value == obj.Value
                && el.Group == obj.Group
                && el.SubsystemUID == obj.SubsystemUID
                )
                .FirstOrDefaultAsync();
            if (dest != null)
            {
                _ctx.EnumVerbs.Remove(dest);
            }
            return await CreateDbAsync(obj, updatedKeys, requestedFields);
        }

        public async Task<bool> SetDefaultValues()
        {
            try
            {
                var result = false;

                var defaultValues = _defaultEnumVerb;

                var defGroups = defaultValues
                    .GroupBy(el => new { el.Group, el.SubsystemUID })
                    .ToDictionary(el => el.Key, el => el.ToArray());

                var hasGroups = await _ctx.EnumVerbs
                    .GroupBy(el => new { el.Group, el.SubsystemUID })
                    .Select(el => el.Key)
                    .ToArrayAsync();

                var forAdd = (from defGr in defGroups
                              join hasGroup in hasGroups on defGr.Key equals hasGroup into hasGr
                              from gr in hasGr.DefaultIfEmpty()
                              where gr == null
                              select defGr.Value)
                    .ToArray();

                foreach (var val in forAdd)
                {
                    _ctx.EnumVerbs.AddRange(val);

                    result = true;
                }

                if (result)
                {
                    await _ctx.SaveChangesAsync();
                }

                return true;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка установки начальных значений EnumVerbs", ex);

                return false;
            }
        }

        //private async Task<bool> SetDefaultValuesForGroup(string group)
        //{
        //    if (await _ctx.EnumVerbs.AnyAsync(el => el.Group == @group))
        //    {
        //        return false;
        //    }

        //    _ctx.EnumVerbs.AddRange(_defaultEnumVerb.Where(el => el.Group == @group));

        //    return true;
        //}
        //private async Task<bool> SetDefaultValuesForSS(string ss)
        //{
        //    var dirty = false;
        //    if (!await _ctx.EnumVerbs.AnyAsync(el => el.SubsystemUID == ss))
        //    {
        //        foreach (var obj in _defaultEnumVerb.Where(el => el.SubsystemUID == ss))
        //        {
        //            _ctx.EnumVerbs.Add(obj);
        //            dirty = true;
        //        }
        //    }
        //    return dirty;
        //}

        //public async Task<bool> SetDefaultValues()
        //{
        //    try
        //    {
        //        var dirty = false;

        //        dirty |= await SetDefaultValuesForGroup("appealStatus");
        //        dirty |= await SetDefaultValuesForGroup("feedbackStatus");
        //        dirty |= await SetDefaultValuesForSS("email");

        //        if (dirty)
        //        {
        //            await _ctx.SaveChangesAsync();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _log.Error("Error set EnumVerbs defaults", ex);
        //        return false;
        //    }
        //    return true;
        //}

        private static readonly EnumVerb[] _defaultEnumVerb =
        {
            #region appealStatus

            new EnumVerb
            {
                Group = "appealStatus",
                Value = "-1",
                ShortName = "Черновик",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new []{0},
                        Email = new AppealStatusEmail
                        {
                            Send = false
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "0",
                ShortName = "Отправлено",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new []{1},
                        IsInitial = true,
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealCitizen"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "0",
                ShortName = "Отправлено",
                SubsystemUID = SubsystemOrganizationAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new []{1},
                        IsInitial = true,
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealOrganization"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "1",
                ShortName = "Зарегистрировано",
                Name = "Ваше обращение зарегистрировано",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new []{2},
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealCitizen"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "1",
                ShortName = "Зарегистрировано",
                Name = "Ваше обращение зарегистрировано",
                SubsystemUID = SubsystemOrganizationAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new []{2},
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealOrganization"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "2",
                ShortName = "На рассмотрении",
                Name = "Ваше обращение принято к рассмотрению",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new []{3},
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealCitizen"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "2",
                ShortName = "На рассмотрении",
                Name = "Ваше обращение принято к рассмотрению",
                SubsystemUID = SubsystemOrganizationAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new []{3},
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealOrganization"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "3",
                ShortName = "Направлено по компетенции",
                Name = "Ваше обращение направлено по компетенции",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new []{4},
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealCitizen"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "3",
                ShortName = "Направлено по компетенции",
                Name = "Ваше обращение направлено по компетенции",
                SubsystemUID = SubsystemOrganizationAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new []{4},
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealOrganization"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "4",
                ShortName = "На рассмотрении",
                Name = "Ваше обращение принято к рассмотрению",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new []{5, 6, 7, 8, 9},
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealCitizen"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "5",
                ShortName = "Завершено",
                Name = "Ваше обращение рассмотрено",
                Description = "Ответ направлен на указанный Вами при регистрации обращения адрес электронной почты",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new int [0],
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealCitizen",
                            SendAdditional = new AppealStatusSendAdditional
                            {
                                SendFiles = true,
                                SendMessage = true,
                                MessageKind = MessageKindEnum.SystemUserMessage.ToString()
                            }
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "5",
                ShortName = "Дан ответ",
                Name = "Ваше обращение рассмотрено",
                Description = "Ответ направлен на указанный Вами при регистрации обращения адрес электронной почты",
                SubsystemUID = SubsystemOrganizationAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new int [0],
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealOrganization",
                            SendAdditional = new AppealStatusSendAdditional
                            {
                                SendFiles = true,
                                SendMessage = true,
                                MessageKind = MessageKindEnum.SystemUserMessage.ToString()
                            }
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "6",
                ShortName = "Не требует ответа",
                Name = "Ваше обращение рассмотрено",
                Description = "Вашему обрещению присвоен статус \"Не требует ответа\"",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new int [0],
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealCitizen"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "6",
                ShortName = "Не требует ответа",
                Name = "Ваше обращение рассмотрено",
                Description = "Вашему обрещению присвоен статус \"Не требует ответа\"",
                SubsystemUID = SubsystemOrganizationAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                {
                    StatusOptions = new StatusOptions
                    {
                        NextStatus = new int[0],
                        Email = new AppealStatusEmail
                        {
                            Send = true,
                            Template = "appealOrganization"
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "7",
                ShortName = "Продлено",
                Name = "Срок рассмотрения Вашего обращения продлен",
                Description = "Срок рассмотрения Вашего обращения продлен",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                    {
                        StatusOptions = new StatusOptions
                        {
                            NextStatus = new [] {5},
                            Email = new AppealStatusEmail
                            {
                                Send = true,
                                Template = "appealCitizen"
                            }
                        }
                    }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "8",
                ShortName = "Передано депутату",
                Name = "Ваше обращение передано депутату",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                    {
                        StatusOptions = new StatusOptions
                        {
                            NextStatus = new [] {5, 7, 9},
                            Email = new AppealStatusEmail
                            {
                                Send = true,
                                Template = "appealCitizen"
                            }
                        }
                    }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "appealStatus",
                Value = "9",
                ShortName = "Рассмотрение прекращено по заявлению гражданина",
                Name = "Рассмотрение Вашего обращения прекращено",
                Description = "Рассмотрение Вашего обращения прекращено",
                SubsystemUID = SubsystemCitizenAppeals.BaseSystemKey,
                CustomSettings = new AppealStatusSettings
                    {
                        StatusOptions = new StatusOptions
                        {
                            NextStatus = new int[0],
                            Email = new AppealStatusEmail
                            {
                                Send = true,
                                Template = "appealCitizen"
                            }
                        }
                    }.SerializeJson()
            },

            #endregion

            #region feedbackStatus

            new EnumVerb
            {
                Group = "feedbackStatus",
                Value = "0",
                ShortName = "Отправлено",
                Name = "Отправлено",
                Description = "которое создано и направлено заявителем, " +
                              "но еще не имеющее никакой регистрационной информации в серверной части. " +
                              "Такие обращения отображаются в личном кабинете заявителя " +
                              "(в случае направления обращения авторизованным в ЕСИА пользователем)",
                SubsystemUID = null,
                CustomSettings = new AppealStatusSettings
                    {
                        StatusOptions = new StatusOptions
                        {
                            NextStatus = new [] {1},
                            Email = new AppealStatusEmail
                            {
                                Send = true,
                                Template = "appealFeedback"
                            }
                        }
                    }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "feedbackStatus",
                Value = "1",
                ShortName = "Зарегистрировано",
                Name = "Зарегистрировано",
                Description = "зарегистрировано в серверной части Электронной приемной",
                SubsystemUID = null,
                CustomSettings = new AppealStatusSettings
                    {
                        StatusOptions = new StatusOptions
                        {
                            NextStatus = new [] {2},
                            IsInitial = true,
                            Email = new AppealStatusEmail
                            {
                                Send = true,
                                Template = "appealFeedback"
                            }
                        }
                    }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "feedbackStatus",
                Value = "2",
                ShortName = "На рассмотрении",
                Name = "На рассмотрении",
                Description = "находящееся в работе. Статус меняется в ручном режиме администратором",
                SubsystemUID = null,
                CustomSettings = new AppealStatusSettings
                    {
                        StatusOptions = new StatusOptions
                        {
                            NextStatus = new [] {5, 6},
                            Email = new AppealStatusEmail
                            {
                                Send = true,
                                Template = "appealFeedback"
                            }
                        }
                    }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "feedbackStatus",
                Value = "5",
                ShortName = "Дан ответ",
                Name = "Дан ответ",
                Description = "дан ответ администратором, такой статус устанавливается в ручном режиме администратором",
                SubsystemUID = null,
                CustomSettings =new AppealStatusSettings
                    {
                        StatusOptions = new StatusOptions
                        {
                            NextStatus = new int[0],
                            Email = new AppealStatusEmail
                            {
                                Send = true,
                                Template = "appealFeedback",
                                SendAdditional = new AppealStatusSendAdditional
                                {
                                    SendFiles = true,
                                    SendMessage = true,
                                    MessageKind = MessageKindEnum.SystemUserMessage.ToString()
                                }
                            }
                        }
                    }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "feedbackStatus",
                Value = "6",
                ShortName = "Не подлежит рассмотрению",
                Name = "Не подлежит рассмотрению",
                Description = "такой статус устанавливается администратором в том случае, " +
                              "когда обращение, направленное по каналу обратной связи, " +
                              "не касается функционирования цифровой платформы. " +
                              "Уведомления об изменении статуса должны отображаться в личном кабинете " +
                              "авторизованного пользователя и направляться на электронную почту",
                SubsystemUID = null,
                CustomSettings = new AppealStatusSettings
                    {
                        StatusOptions = new StatusOptions
                        {
                            NextStatus = new int[0],
                            Email = new AppealStatusEmail
                            {
                                Send = true,
                                Template = "appealFeedback"
                            }
                        }
                    }.SerializeJson()
            },

            #endregion

            #region emailSettings

            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "appealCitizen",
                //ShortName = "",
                ShortName = "Сообщение от гражданина",
                //Description= "",
                
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Сообщение от {Appeal.CreatedAt}",
                    EntityInclude = new Dictionary<string, string[]>
                    {
                        {
                            nameof(Appeal), new[]
                            {
                                nameof(Appeal.Author),
                                nameof(Appeal.Subsystem),
                                nameof(Appeal.Rubric),
                                nameof(Appeal.Topic),
                                nameof(Appeal.Territory),
                                nameof(Appeal.Region)
                            }
                        }
                    },
                    DateTimeFormat = "dd.MM.yyyy"
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "appealOrganization",
                //ShortName = "",
                ShortName = "Обращение организации",
                //Description= "",
                
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Письмо от {Appeal.CreatedAt}",
                    EntityInclude = new Dictionary<string, string[]>
                    {
                        {
                            nameof(User), new[]
                            {
                                $"{nameof(User.Users2Organizations)}.{nameof(User2Organization.UserOrganization)}"
                            }
                        },
                        {
                            nameof(Appeal), new[]
                            {
                                nameof(Appeal.Author),
                                nameof(Appeal.Subsystem),
                                nameof(Appeal.Rubric),
                                nameof(Appeal.Topic),
                                nameof(Appeal.Territory),
                                nameof(Appeal.Region)
                            }
                        }
                    },
                    DateTimeFormat = "dd.MM.yyyy"
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "appealFeedback",
                //ShortName = "",
                ShortName = "Сообщение обратной связи",
                //Description= "",
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Cообщение от {Appeal.CreatedAt}",
                    EntityInclude = new Dictionary<string, string[]>
                    {
                        {
                            nameof(Appeal), new[]
                            {
                                nameof(Appeal.Author),
                                nameof(Appeal.Subsystem)
                            }
                        }
                    },
                    DateTimeFormat = "dd.MM.yyyy"
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "appealFeedbackRegisteredModerator",
                //ShortName = "",
                ShortName = "Настройки письма обратной связи",
                //Description= "",
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Зарегистрировано новое сообщение от {Appeal.CreatedAt}",
                    EntityInclude = new Dictionary<string, string[]>
                    {
                        {
                            nameof(Appeal), new[]
                            {
                                nameof(Appeal.Author),
                                nameof(Appeal.Subsystem)
                            }
                        }
                    },
                    DateTimeFormat = "dd.MM.yyyy HH.mm.ss"
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "messageToMessage",
                //ShortName = "",
                ShortName = "Собщение на сообщение",
                //Description= "",
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Сообщение от {ParentMessage.CreatedAt}",
                    DateTimeFormat = "dd.MM.yyyy"
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "messageToMessageForSubscribed",
                //ShortName = "",
                ShortName = "Собщение по подписке",
                //Description= "",
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Сообщение от {ParentMessage.CreatedAt}",
                    DateTimeFormat = "dd.MM.yyyy"
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "appealRejectedCitizen",
                //ShortName = "",
                ShortName = "Отказ в публикации сообщения",
                //Description= "",
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Отказ в публикации сообщения от {Appeal.CreatedAt}",
                    DateTimeFormat = "dd.MM.yyyy",
                    EntityInclude = new Dictionary<string, string[]>
                    {
                        {
                            nameof(Appeal), new[]
                            {
                                nameof(Appeal.Author),
                                nameof(Appeal.Subsystem)
                            }
                        }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "messageToAppealCitizen",
                //ShortName = "",
                ShortName = "Сообщение на обращение граждан",
                //Description= "",
                
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Обращение от {Appeal.CreatedAt}",
                    DateTimeFormat = "dd.MM.yyyy"
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "messageToAppealOrganization",
                //ShortName = "",
                ShortName = "Сообщение на обращение организации",
                //Description= "",   
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Обращение от {Appeal.CreatedAt}",
                    DateTimeFormat = "dd.MM.yyyy"
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "restorePassword",
                //ShortName = "",
                ShortName = "Восстановление пароля",
                //Description= "",
                
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Восстановление пароля ({system.name})",
                    DateTimeFormat = "dd.MM.yyyy"
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "confirmEmail",
                //ShortName = "",
                ShortName = "Подтверждение Email",
                //Description= "",
                
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Подтверждение Email",
                    DateTimeFormat = "dd.MM.yyyy"
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "appealRegisteredModerator",
                //ShortName = "",
                ShortName = "Модерирование сообщения",
                //Description= "",
                
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "В системе \"{{system.name}}\" зарегистрировано новое обращение",
                    DateTimeFormat = "dd.MM.yyyy",
                    EntityInclude = new Dictionary<string, string[]>
                    {
                        {
                            nameof(Appeal), new[]
                            {
                                nameof(Appeal.Author),
                                nameof(Appeal.Subsystem),
                                nameof(Appeal.Rubric),
                                nameof(Appeal.Topic),
                                nameof(Appeal.Territory),
                                nameof(Appeal.Region)
                            }
                        }
                    },
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "messageToNews",
                //ShortName = "",
                ShortName = "Сообщение к новости",
                //Description= "",
                
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Новость от {News.CreatedAt}",
                    DateTimeFormat = "dd.MM.yyyy",
                    EntityInclude = new Dictionary<string, string[]>
                    {
                        { nameof(News), new []{nameof(News.Author)} }
                    }
                }.SerializeJson()
            },
            new EnumVerb
            {
                Group = "email",
                //SubsystemUID = "",
                Value = "messageToVote",
                //ShortName = "",
                ShortName = "Сообщение к голосованию",
                //Description= "",
                
                CustomSettings = new EmailTemplateSettings
                {
                    SubjectTemplate = "Голосование от {Vote.CreatedAt}",
                    DateTimeFormat = "dd.MM.yyyy"
                }.SerializeJson()
            }, 
            
            #endregion

            #region groupDescriptors
            new EnumVerb
            {
                Group = "groupDescriptor",
                Value = "groupDescriptor",
                ShortName = "Группы справочников",

                CustomSettings = new GroupDescriptorSettings
                {
                    Readonly = true,
                }.SerializeJson(),
            },

            new EnumVerb
            {
                Group = "groupDescriptor",
                Value = "appealStatus",
                ShortName = "Статусы обращений",
            },

            new EnumVerb
            {
                Group = "groupDescriptor",
                Value = "feedbackStatus",
                ShortName = "Сообщения обратной связи",
            },
            new EnumVerb
            {
                Group = "groupDescriptor",
                Value = "externalLink",
                ShortName = "Внешние линки для админки",
            },
            new EnumVerb
            {
                Group = "groupDescriptor",
                Value = "email",
                ShortName = "Шаблоны почтовой рассылки",
            }, 


            #endregion

        };

        public static class EnumVerbManagerExtensions
        {
        }
    }
}
