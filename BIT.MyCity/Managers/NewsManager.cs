using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Subsystems;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL.Utilities;
using log4net;
using Microsoft.EntityFrameworkCore;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BIT.MyCity.Managers
{
    public class NewsManager
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(NewsManager));

        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        private readonly SettingsManager _sm;
        private readonly ElasticClient _elasticClient;
        public NewsManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
            _sm = _serviceProvider.GetRequiredService<SettingsManager>();
            try
            {
                _elasticClient = (ElasticClient)_serviceProvider.GetRequiredService<IElasticClient>();
            }
            catch { }
        }

        public async Task<Pagination<News>> GetNewsAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<News>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.News
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm)
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<News>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<News>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }

        public async Task<Pagination<News>> SearchNewsAsync(string query, QueryOptions options)
        {
            if (_elasticClient == null)
            {
                return null;
            }
            options.RequestedFields.TryGetValue(nameof(Pagination<News>.Items).ToJsonFormat(), out var items);
            var result = new Pagination<News>();

            query ??= "";
            int skip = options.Range?.Skip ?? 0;
            int take = options.Range?.Take ?? 100;
            var response = await _elasticClient.SearchAsync<ESNews>(
                s => s.Query(q => q.QueryString(d => d.Query(query)))
                    .From(skip)
                    .Size(take)
                    //.Analyzer("russian_morphology")
                    );
            if (response.IsValid)
            {
                result.Total = response.Total;
                if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
                {
                    var ids = response.Documents.Select(el => el.Id).ToList();
                    if (ids.Count > 0)
                    {
                        result.Items = await _ctx.News
                            .Where(el => ids.Contains(el.Id))
                            .IncludeByFields(items)
                            .IncludeByNotMappedFields(items)
                            .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                            .ToListAsync();
                    }
                }
            }
            return result;
        }
        public async Task<News> CreateDbAsync(News src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            var user = await _um.CurrentUser();

            src.Author = user ?? throw new Exception($"Нужно авторизоваться");

            src.Rubric = await BaseConstants.GetObjById(src.RubricId, _ctx.Rubrics);

            src.Region = await BaseConstants.GetObjById(src.RegionId, _ctx.Regions);

            src.Territory = await BaseConstants.GetObjById(src.TerritoryId, _ctx.Territories);

            src.Topic = await BaseConstants.GetObjById(src.TopicId, _ctx.Topics);

            if (!(await src.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx)).HasFlag(BaseConstants.AllowedActionFlags.Create))
            {
                throw new Exception($"Вам это нельзя");
            }

            if (!updateFields.ContainsKey(nameof(News.SubscribeToAnswers).ToJsonFormat()))
            {
                src.SubscribeToAnswers = _sm.SettingValue<SubscribeToAnswersEnum>($"subsystem.{SSUID.news}.subscribe_to_answers.");
            }

            var trn = await _ctx.Database.BeginTransactionAsync();

            try
            {
                if (updateFields.ContainsKey(nameof(src.Text).ToJsonFormat()) && !updateFields.ContainsKey(nameof(src.ShortText).ToJsonFormat()))
                {
                    src.ShortText = src.Text.Substring(0, Math.Min(src.Text.Length, 500));
                }

                await BaseConstants.ModifyIdsList(nameof(src.AttachedFiles), src, src, _ctx.AttachedFiles, _ctx);

                _ctx.News.Add(src);

                src.ViewSet(true, src.Author);

                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = user,
                    Action = $"News {src.Name} created"
                });

                await _ctx.SaveWeightAsync(src);
            }
            catch
            {
                await trn.RollbackAsync();

                throw;
            }

            await trn.CommitAsync();

            if (_elasticClient != null)
            {
                await _elasticClient.IndexDocumentAsync(new ESNews(src));
            }

            return await GetNewsAsync(src.Id, requestedFields);
        }

        public async Task<News> UpdateDbAsync(long id, News src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var userId = _um.ClaimsPrincipalId();
            if (userId == null)
            {
                throw new Exception($"Нужно авторизоваться");
            }

            var dest = await _ctx.News
                .Where(el => el.Id == id)
                .Where(el => !el.Deleted)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"NewsId {id} not found");
            }
            if (!(await dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx)).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }
            var old = (News)dest.Clone();
            dest.Rubric = await BaseConstants.GetObjById(src.RubricId, _ctx.Rubrics) ?? dest.Rubric;
            dest.Region = await BaseConstants.GetObjById(src.RegionId, _ctx.Regions) ?? dest.Region;
            dest.Territory = await BaseConstants.GetObjById(src.TerritoryId, _ctx.Territories) ?? dest.Territory;
            dest.Topic = await BaseConstants.GetObjById(src.TopicId, _ctx.Topics) ?? dest.Topic;

            await BaseConstants.ModifyIdsList(nameof(src.AttachedFiles), src, dest, _ctx.AttachedFiles, _ctx, el => el.AttachedFiles);

            BaseConstants.UpdateEntityByFieldsList(dest, src, updateKeys);
            var diff = old.MemberwiseDiff(dest);
            if (!string.IsNullOrEmpty(diff))
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = await _um.CurrentUser(),
                    Action = $"News {dest.Id} changed",
                    Data = diff
                });
            }
            await _ctx.SaveChangesAsync();
            if (_elasticClient != null)
            {
                var esNews = new ESNews(dest);
                await _elasticClient?.UpdateAsync<ESNews>(esNews, u => u.Doc(esNews));
            }
            return await GetNewsAsync(id, requestedFields);
        }

        private async Task<News> GetNewsAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            return await _ctx.News
                .Where(el => el.Id == id)
                .IncludeByFields(requestedFields)
                .IncludeByNotMappedFields(requestedFields)
                .FirstOrDefaultAsync();
        }

        public async Task<News> ApproveAsync(long id, NewsApprove src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var userId = _um.ClaimsPrincipalId();
            if (userId == null)
            {
                throw new Exception($"Нужно авторизоваться");
            }
            var dest = await _ctx.News
                .Where(el => el.Id == id)
                .Where(el => !el.Deleted)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"NewsId {id} not found");
            }
            if (!(await dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm, _ctx)).HasFlag(BaseConstants.AllowedActionFlags.Moderate))
            {
                throw new Exception($"Вам это нельзя");
            }
            News old = (News)dest.Clone();
            dest.State = src.State;
            var diff = old.MemberwiseDiff(dest);
            if (!string.IsNullOrEmpty(diff))
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = await _um.CurrentUser(),
                    Action = $"News moderated {dest.Id}",
                    Data = diff
                });
            }
            await _ctx.SaveChangesAsync();
            return await GetNewsAsync(id, requestedFields);
        }

        public async Task<LikeReport> LikeAsync(LikeRequest like)
        {
            var user = await _um.CurrentUser();
            if (user == null && like.Liked != LikeRequestEnum.NONE)
            {
                throw new Exception($"Аноним не может лайкать");
            }
            var obj = await _ctx.News
                .Where(el => !el.Deleted)
                .Where(el => el.Id == like.Id)
                .Include(el => el.UserLike)
                .FirstOrDefaultAsync();
            if (obj == null)
            {
                throw new Exception($"{like.Type}:{like.Id} not found");
            }

            if (user != null && like.Liked != LikeRequestEnum.NONE && obj.AuthorId == user.Id)
            {
                throw new Exception($"Нельзя лайкать свои новости");
            }
            var LikesCount = obj.LikesCount;
            var DislikesCount = obj.DislikesCount;
            var liked = obj.LikeSet(like.Liked, user);
            if (LikesCount != obj.LikesCount || DislikesCount != obj.DislikesCount)
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = user,
                    Action = $"News {obj.Id} Like status changed"
                });
                await _ctx.SaveChangesAsync();
            }
            return new LikeReport { Liked = liked, LikeCount = obj.LikesCount, DislikeCount = obj.DislikesCount };
        }

        public async Task<ViewReport> ViewAsync(ViewRequest view)
        {
            var user = await _um.CurrentUser();
            if (user == null)
            {
                throw new Exception($"Аноним не может отмечать просмотренность");
            }
            var obj = await _ctx.News
                .Where(el => el.Id == view.Id)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm)
                .Include(el => el.UserView)
                .FirstOrDefaultAsync();
            if (obj == null)
            {
                throw new Exception($"{view.Type}:{view.Id} not found");
            }
            var ViewCount = obj.ViewsCount;
            var viewed = obj.ViewSet(view.Viewed, user);
            if (ViewCount != obj.ViewsCount)
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = user,
                    Action = $"News {obj.Id} View status changed"
                });
                await _ctx.SaveChangesAsync();
            }
            return new ViewReport { Viewed = viewed.Viewed, ViewCount = obj.ViewsCount, CreatedAt = viewed.CreatedAt, UpdatedAt = viewed.UpdatedAt };
        }
    }
    public static class NewsExtensions
    {
        public static IQueryable<News> IncludeByNotMappedFields(
            this IQueryable<News> obj,
            GraphQlKeyDictionary fields)
        {
            if (fields != null && obj != null)
            {
                if (fields.ContainsKey(nameof(News.UsersLiked).ToJsonFormat()) || fields.ContainsKey(nameof(News.LikeWIL).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.UserLike).ThenInclude(el => el.User);
                }
                if (fields.ContainsKey(nameof(News.UsersViewed).ToJsonFormat()) || fields.ContainsKey(nameof(News.ViewWIV).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.UserView).ThenInclude(el => el.User);
                }
            }
            return obj;
        }
        public static IQueryable<News> FilterByReadAllowed(
            this IQueryable<News> main,
            long? userId,
            System.Security.Claims.ClaimsPrincipal claimsPrincipal,
            SettingsManager sm
            )
        {
            var ss_enabled = new List<string>();
            var result = main.Where(el => false);
            var publ = main.Where(el => false);
            if (sm.SettingValue<bool>($"subsystem.{SSUID.news}.enable."))
            {
                publ = publ.Concat(main.Where(el => el.State == NewsStateEnum.PUBLISHED));
            }
            if (userId.HasValue)
            {
                var my = main.Where(el => el.AuthorId == userId.Value);

                var moderatorIn = main.FilterByReadAllowedSubsystemRights(userId, claimsPrincipal?.Claims, SSUID.news);

                moderatorIn = moderatorIn.Concat(main.FilterByReadAllowedRubricatorModerator(userId, SSUID.news));

                result = result.Concat(my);
                result = result.Concat(moderatorIn);
            }
            return result.Concat(publ).Distinct();
        }

        public static async Task<BaseConstants.AllowedActionFlags> GetAllowedActions(
            this News obj,
            long? userId,
            System.Security.Claims.ClaimsPrincipal claimsPrincipal,
            SettingsManager sm,
            DocumentsContext ctx
            )
        {
            BaseConstants.AllowedActionFlags flags = 0;
            do
            {
                if (obj == null)
                {
                    break;
                }
                if (obj.State == NewsStateEnum.PUBLISHED)
                {
                    flags |= BaseConstants.AllowedActionFlags.Read;
                }
                if (userId == null)
                {
                    break;
                }
                flags |= obj.GetAllowedSuperAdminActions(claimsPrincipal);
                if (userId.Value == obj.AuthorId)
                {
                    flags |= BaseConstants.AllowedActionFlags.Read;
                    //if (obj.ModerationStage == ModerationStageEnum.WAIT)
                    //{
                    //    flags |= BaseConstants.AllowedActionFlags.Edit;
                    //}
                }
                flags |= (await obj.GetAllowedModerationActions(userId, claimsPrincipal?.Claims, sm, ctx, SSUID.news));
            } while (false);
            return flags;
        }
    }
}
