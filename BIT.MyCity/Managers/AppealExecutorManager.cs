﻿using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Pagination;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BIT.MyCity.Managers
{
    public class AppealExecutorManager
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        private readonly SubsystemManager _ssm;
        public AppealExecutorManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
            _ssm = (SubsystemManager)_serviceProvider.GetService(typeof(SubsystemManager));
        }

        public async Task<Pagination<AppealExecutor>> GetAppealExecutorsAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<AppealExecutor>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.AppealExecutors
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<AppealExecutor>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<AppealExecutor>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }

        public async Task<AppealExecutor> GetAppealExecutorAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            var obj = await _ctx.AppealExecutors
                .Where(el => el.Id == id)
                .IncludeByFields(requestedFields)
                .IncludeByNotMappedFields(requestedFields)
                .FirstOrDefaultAsync();
            return obj;
        }

        public async Task<AppealExecutor> CreateDbAsync(AppealExecutor src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            await BaseConstants.UpdateObjIdsList(
                destIds: new List<long>(),
                srcIds: src.AppealIds,
                db: _ctx.Appeals,
                funcAdd: obj => src.Appeal2Executors.Add(new Appeal2Executor { Appeal = obj, AppealExecutor = src }));
            await BaseConstants.UpdateObjIdsList(
                destIds: new List<long>(),
                srcIds: src.AppealPrincipalIds,
                db: _ctx.Appeals,
                funcAdd: obj => src.Appeal2PrincipalExecutors.Add(new Appeal2PrincipalExecutor { Appeal = obj, AppealPrincipalExecutor = src }));
            if (src.UserId.HasValue && src.UserId.Value > 0)
            {
                src.User = await _ctx.Users.Where(el => el.Id == src.UserId.Value).Where(el => !el.Disconnected).FirstOrDefaultAsync();
            }
            _ctx.AppealExecutors.Add(src);
            await _ctx.SaveWeightAsync(src);
            await _ctx.SaveDueAsync(src, BaseConstants.GetParentAsync(src, _ctx, new List<string> { nameof(Appeal) }));
            return await GetAppealExecutorAsync(src.Id, requestedFields);
        }
        public async Task<AppealExecutor> UpdateDbAsync(long id, AppealExecutor src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.AppealExecutors
                .Where(el => el.Id == id)
                .Include(el => el.Appeal2Executors).ThenInclude(el => el.Appeal)
                .Include(el => el.Appeal2PrincipalExecutors).ThenInclude(el => el.Appeal)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"AppealExecutorId {id} not found");
            }
            if (updateKeys.ContainsKey(nameof(AppealExecutor.AppealIds).ToJsonFormat()))
            {
                await BaseConstants.SetT2LinkedEntIdsAsync(
                src.AppealIds,
                dest.Appeal2Executors,
                async uid => await _ctx.Appeals.Where(el => !el.Deleted && el.Id == uid).FirstOrDefaultAsync(),
                (u, uid) => new Appeal2Executor { Appeal = u, AppealId = uid, AppealExecutor = dest, AppealExecutorId = dest.Id },
                uid => dest.Appeal2Executors.Any(el => el.AppealExecutorId == uid)
                );
            }
            if (updateKeys.ContainsKey(nameof(AppealExecutor.AppealPrincipalIds).ToJsonFormat()))
            {
                await BaseConstants.SetT2LinkedEntIdsAsync(
                src.AppealPrincipalIds,
                dest.Appeal2PrincipalExecutors,
                async uid => await _ctx.Appeals.Where(el => !el.Deleted && el.Id == uid).FirstOrDefaultAsync(),
                (u, uid) => new Appeal2PrincipalExecutor { Appeal = u, AppealId = uid, AppealPrincipalExecutor = dest, AppealPrincipalExecutorId = dest.Id },
                uid => dest.Appeal2PrincipalExecutors.Any(el => el.AppealPrincipalExecutorId == uid)
                );
            }
            await BaseConstants.UpdateObjIdsList(
                destIds: dest.Appeal2Executors.Select(el => el.AppealId).ToList(),
                srcIds: src.AppealIdsChange,
                db: _ctx.Appeals,
                funcAdd: obj => dest.Appeal2Executors.Add(new Appeal2Executor { Appeal = obj, AppealExecutor = src }),
                funcDel: obj => dest.Appeal2Executors.Remove(dest.Appeal2Executors.Where(el => el.AppealId == obj.Id).First())
                );
            await BaseConstants.UpdateObjIdsList(
                destIds: dest.Appeal2PrincipalExecutors.Select(el => el.AppealId).ToList(),
                srcIds: src.AppealPrincipalIdsChange,
                db: _ctx.Appeals,
                funcAdd: obj => dest.Appeal2PrincipalExecutors.Add(new Appeal2PrincipalExecutor { Appeal = obj, AppealPrincipalExecutor = src }),
                funcDel: obj => dest.Appeal2PrincipalExecutors.Remove(dest.Appeal2PrincipalExecutors.Where(el => el.AppealId == obj.Id).First())
                );
            if (src.UserId.HasValue && src.UserId.Value > 0)
            {
                src.User = await _ctx.Users.Where(el => el.Id == src.UserId.Value).Where(el => !el.Disconnected).FirstOrDefaultAsync();
            }
            BaseConstants.UpdateEntityByFieldsList(dest, src, updateKeys);
            await _ctx.SaveChangesAsync();
            return await GetAppealExecutorAsync(dest.Id, requestedFields);
        }
    }

    public static class AppealExecutorExtensions
    {
        public static IQueryable<AppealExecutor> IncludeByNotMappedFields(this IQueryable<AppealExecutor> obj, GraphQlKeyDictionary fields)
        {
            if (fields != null && obj != null)
            {
                if (fields.ContainsKey(nameof(AppealExecutor.Appeals).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.Appeal2Executors).ThenInclude(el => el.Appeal);
                }
                if (fields.ContainsKey(nameof(AppealExecutor.AppealsPrincipal).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.Appeal2PrincipalExecutors).ThenInclude(el => el.Appeal);
                }
            }
            return obj;
        }
    }
}
