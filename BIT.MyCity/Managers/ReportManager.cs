using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Reports;
using BIT.MyCity.Services;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Managers
{
    public class ReportManager
    {
        private const string ReportSettingsKeyPrefix = "ReportSettings#";

        private readonly IServiceProvider _servicesProvider;
        private readonly ReportService _reportService;

        public ReportManager(IServiceProvider servicesProvider, ReportService reportService)
        {
            _servicesProvider = servicesProvider;
            _reportService = reportService;
        }

        private IEnumerable<IReport> GetReports()
        {
            return _reportService.Reports;
        }

        public async Task<List<ReportSettings>> GetReportSettingsAsync(Guid? reportId, GraphQlKeyDictionary requestedFields)
        {
            var reportMetadata = new List<IReport>();
            
            if (reportId.HasValue)
            {
                var rMetadata = _reportService.GetReport(reportId.Value);

                if (rMetadata == null)
                {
                    throw new Exception($"Отсутствует отчет с Id = {reportId}");
                }

                reportMetadata.Add(rMetadata);
            }
            else
            {
                reportMetadata.AddRange(
                    GetReports()
                        .OrderBy(el=>el.Name));
            }

            var needParameters = requestedFields.ContainsKey(nameof(ReportSettings.Parameters).ToJsonFormat());

            var result = new List<ReportSettings>();

            foreach (var report in reportMetadata)
            {
                if (needParameters)
                {
                    var reportSettings = await report.GetSettings(_servicesProvider)
                                         ?? new ReportSettings
                                         {
                                             ReportId = report.Id
                                         };
                    

                    reportSettings.Name = report.Name;

                    reportSettings.Description = report.Description;

                    result.Add(reportSettings);

                    continue;
                }

                result.Add(new ReportSettings
                {
                    ReportId = report.Id,
                    Name = report.Name,
                    Description = report.Description
                });
            }
            
            return result;
        }

        public async Task<ExecuteReportResult> ExecuteReportAsync(ReportSettings reportSettings)
        {
            var reportMetadata = _reportService.GetReport(reportSettings.ReportId);

            if (reportMetadata == null)
            {
                return new ExecuteReportResult(reportSettings.ReportId)
                {
                    Errors = {$"Отсутствует отчет с Id = {reportSettings.ReportId}"}
                };
                //throw new Exception($"Отсутствует отчет с Id = {reportSettings.ReportId}");
            }

            var executeReportResult = await reportMetadata.Execute(_servicesProvider, reportSettings);
            
            return executeReportResult;
        }

        public async Task<ReportSavedSettings> SaveUserReportSettingsAsync(Guid reportId, ReportSavedSettings reportSettings)
        {
            if (reportSettings == null || string.IsNullOrWhiteSpace(reportSettings.Name))
            {
                return null;
            }

            var reportMetadata = _reportService.GetReport(reportId);

            if (reportMetadata == null)
            {
                return null;
            }

            var userManager = _servicesProvider.GetRequiredService<UserManager>();

            var userId = userManager.ClaimsPrincipalId();

            if (!userId.HasValue)
            {
                throw new Exception("Только авторизованный пользователь может сохранять параметры отчета.");
            }
            
            var resultSettings = reportSettings.Parameters
                .ToList();

            var defaultSettings = await reportMetadata.GetSettings(_servicesProvider);

            var joinSettings = resultSettings
                .Join(defaultSettings.Parameters,
                    rs => rs.Name,
                    ds => ds.Name,
                    (rs, ds) => new {rs, ds.ControlType});

            foreach (var item in joinSettings)
            {
                if (item.ControlType == ReportSettingsControlType.DateRange)
                {
                    var options = item.rs.GetOptions<DateRangeSingleSettingsSaveOptions>();

                    if (options == null)
                    {
                        return null;
                    }

                    item.rs.SetOptions(options);
                }
                else
                {
                    item.rs.Options = null;
                }
            }
            
            var csManager = _servicesProvider.GetRequiredService<CustomStorageManager>();

            if (!(await csManager.GetValueAsync<List<ReportSavedSettings>>(
                GetReportSavedSettingsKey(reportId),
                nameof(User),
                userId.Value.ToString()) is List<ReportSavedSettings> saved))
            {
                saved = new List<ReportSavedSettings>();
            }

            var savedSetting = saved.SingleOrDefault(el => el.Id == reportSettings.Id);

            if (savedSetting == null && reportSettings.Id.HasValue)
            {
                return null;
            }

            ReportSavedSettings result;

            if (savedSetting == null)
            {
                result = new ReportSavedSettings
                {
                    Id = saved.Any()
                        ? saved.Max(el=>el.Id) + 1
                        : 1,
                    Name = reportSettings.Name.Trim(),
                    Parameters = resultSettings
                };
                
                saved.Add(result);
            }
            else
            {
                savedSetting.Name = reportSettings.Name.Trim();
                savedSetting.Parameters = resultSettings;

                result = savedSetting;
            }

            await csManager.SetValueAsync(
                GetReportSavedSettingsKey(reportId),
                nameof(User),
                userId.Value.ToString(),
                saved);

            return result;
        }

        public async Task<IEnumerable<ReportSavedSettings>> GetUserReportSettingsAsync(Guid reportId)
        {
            var reportMetadata = _reportService.GetReport(reportId);

            if (reportMetadata == null)
            {
                return null;
            }

            var userManager = _servicesProvider.GetRequiredService<UserManager>();

            var userId = userManager.ClaimsPrincipalId();

            if (!userId.HasValue)
            {
                throw new Exception("Только авторизованный пользователь может получать параметры отчета.");
            }

            var csManager = _servicesProvider.GetRequiredService<CustomStorageManager>();

            return !(await csManager.GetValueAsync<List<ReportSavedSettings>>(
                GetReportSavedSettingsKey(reportId),
                nameof(User),
                userId.Value.ToString()) is List<ReportSavedSettings> saved)
                ? null
                : saved.OrderBy(el=>el.Id);
        }

        public async Task<bool> DeleteUserReportSettingsAsync(Guid reportId, ulong? id)
        {
            var reportMetadata = _reportService.GetReport(reportId);

            if (reportMetadata == null)
            {
                return false;
            }

            var userManager = _servicesProvider.GetRequiredService<UserManager>();

            var userId = userManager.ClaimsPrincipalId();

            if (!userId.HasValue)
            {
                throw new Exception("Только авторизованный пользователь может удалять параметры отчета.");
            }

            var csManager = _servicesProvider.GetRequiredService<CustomStorageManager>();

            if (id.HasValue)
            {
                if(!(await csManager.GetValueAsync<List<ReportSavedSettings>>(
                    GetReportSavedSettingsKey(reportId),
                    nameof(User),
                    userId.Value.ToString()) is List<ReportSavedSettings> saved))
                {
                    return true;
                }

                var item = saved.SingleOrDefault(el => el.Id == id);

                if (item == null)
                {
                    return true;
                }

                saved.Remove(item);

                await csManager.SetValueAsync(
                    GetReportSavedSettingsKey(reportId),
                    nameof(User),
                    userId.Value.ToString(),
                    saved);

                return true;
            }

            await csManager.DeleteAsync(GetReportSavedSettingsKey(reportId),
                nameof(User),
                userId.Value.ToString());

            return true;
        }

        private static string GetReportSavedSettingsKey(Guid reportId)
        {
            return $"{ReportSettingsKeyPrefix}{reportId}";
        }
    }
}
