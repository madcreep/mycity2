using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Pagination;
using log4net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using Microsoft.EntityFrameworkCore.Storage;

namespace BIT.MyCity.Managers
{
    public class AttachedFileManager
    {
        private const long MaxFileLen = 128 * 1024;

        private readonly ILog _log = LogManager.GetLogger(typeof(AttachedFileManager));
        private readonly IServiceProvider _serviceProvider;
        private readonly IWebHostEnvironment _appEnvironment;
        public AttachedFileManager(IServiceProvider serviceProvider, IWebHostEnvironment appEnvironment)
        {
            _serviceProvider = serviceProvider;
            _appEnvironment = appEnvironment;
        }

        private string _fileStorageDirectory;

        public string FileStorageDirectory
        {
            get
            {
                if (_fileStorageDirectory == null)
                {
                    var rootDirectory = _appEnvironment.ContentRootPath;

                    var storageSubDirectory = _serviceProvider.GetRequiredService<IConfiguration>()["StoragePath"];

                    _fileStorageDirectory = Path.Combine(rootDirectory, storageSubDirectory);
                }

                if (!Directory.Exists(_fileStorageDirectory))
                {
                    Directory.CreateDirectory(_fileStorageDirectory);
                }

                return _fileStorageDirectory;
            }
        }

        public string CurrentStorageName => DateTime.UtcNow.ToString("yyyyMMdd", CultureInfo.InvariantCulture);

        public string TodayDirectory => Path.Combine(FileStorageDirectory,
            CurrentStorageName);

        public string GetFileFullName(AttachedFile af)
        {

            if (af.StoragePath == null || af.StorageName == null)
            {
                return null;
            }

            return Path.Combine(FileStorageDirectory, af.StoragePath, af.StorageName);
        }

        public async Task<string> GetThumbCached(long attachedFileId)
        {
            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            var af = await ctx.AttachedFiles
                .Include(el => el.Thumbs)
                .Where(el => el.Id == attachedFileId)
                .FirstOrDefaultAsync();

            _log.Debug($"GetThumbCached({attachedFileId},{af?.ContentType})");

            if (af?.StorageName == null || af.StoragePath == null)
            {
                return null;
            }

            string result = null;

            var width = settingsManager.SettingValue<int>(SettingsKeys.SystemAttachThumbnailsGqlWidth);

            var height = settingsManager.SettingValue<int>(SettingsKeys.SystemAttachThumbnailsGqlHeight);

            var hh = (IHttpContextAccessor)_serviceProvider.GetService(typeof(IHttpContextAccessor));

            hh.HttpContext.Items.TryGetValue(nameof(QueryOptions.ThumbSize), out var thumbSize);

            hh.HttpContext.Items.TryGetValue(nameof(QueryOptions.ThumbSizeTolerance), out var thumbSizeTolerance);

            hh.HttpContext.Items.TryGetValue(nameof(QueryOptions.ThumbFormat), out var thumbFormat);

            SKEncodedImageFormat? tf = null;

            if (thumbFormat != null)
            {
                tf = thumbFormat as SKEncodedImageFormat?;
            }

            if (thumbSize != null && thumbSize is string ts)
            {
                var rx = new Regex("^([0-9]+)x([0-9]+)$");
                var m = rx.Match(ts);
                if (m.Success)
                {
                    int.TryParse(m.Groups[1].Value, out int x);
                    int.TryParse(m.Groups[2].Value, out int y);
                    var minWidth = settingsManager.SettingValue<int>(SettingsKeys.SystemAttachThumbnailsGqlMinWidth);
                    var maxWidth = settingsManager.SettingValue<int>(SettingsKeys.SystemAttachThumbnailsGqlMaxWidth);
                    var minHeight = settingsManager.SettingValue<int>(SettingsKeys.SystemAttachThumbnailsGqlMinHeight);
                    var maxHeight = settingsManager.SettingValue<int>(SettingsKeys.SystemAttachThumbnailsGqlMaxHeight);
                    width = Math.Min(Math.Max(x, minWidth), maxWidth);
                    height = Math.Min(Math.Max(y, minHeight), maxHeight);
                    width = Math.Min(Math.Max(width, 16), 2048);
                    height = Math.Min(Math.Max(height, 16), 2048);
                }
            }

            if (af.Width == 0)
            {
                var fs = File.OpenRead(GetFileFullName(af));
                UpdateImageSize(af, fs);
                fs.Close();
            }

            if (af.Width > 0 && af.Height > 0)
            {
                int w;
                int h;
                w = width;
                h = af.Height * w / af.Width;
                if (h > height)
                {
                    h = height;
                    w = af.Width * h / af.Height;
                }
                width = w;
                height = h;
            }

            if (thumbSizeTolerance != null && thumbSizeTolerance is string tst)
            {
                var rx1 = new Regex("^([0-9]+)x([0-9]+)$");

                var m1 = rx1.Match(tst);

                if (m1.Success)
                {
                    int.TryParse(m1.Groups[1].Value, out int dx);
                    int.TryParse(m1.Groups[2].Value, out int dy);
                    dx = Math.Min(Math.Max(dx, 0), 50);
                    dy = Math.Min(Math.Max(dy, 0), 50);
                    result = await GetThumbCachedPxDb(width, height, af, tf, dx, dy);
                }

                var rx2 = new Regex("^([0-9]+)%x([0-9]+)%$");

                var m2 = rx2.Match(tst);

                if (m2.Success)
                {
                    int.TryParse(m2.Groups[1].Value, out int dx);
                    int.TryParse(m2.Groups[2].Value, out int dy);
                    dx = Math.Min(Math.Max(dx, 0), 50);
                    dy = Math.Min(Math.Max(dy, 0), 50);
                    result = await GetThumbCachedPercentDb(width, height, af, tf, dx, dy);
                }
            }

            return result ?? await GetThumbCachedPxDb(width, height, af, tf);
        }

        public async Task<string> GetThumbCachedPxDb(int width, int height, AttachedFile af, SKEncodedImageFormat? format = null, int dwMax = 10, int dhMax = 10)
        {
            var thumbs = af.Thumbs
                .Where(el => Math.Abs(el.Height - height) <= dhMax && Math.Abs(el.Width - width) <= dwMax);
            if (format.HasValue)
            {
                thumbs = thumbs.Where(el => el.Format == format.Value);
            }

            ThumbNail result = null;

            var thumbNails = thumbs as ThumbNail[] ?? thumbs.ToArray();

            if (thumbNails.Any())
            {
                result = thumbNails.First();
                var dwMin = int.MaxValue;
                var dhMin = int.MaxValue;
                foreach (var e in thumbNails.Skip(1))
                {
                    var dw = Math.Abs(e.Width - width);
                    if (dw < dwMin)
                    {
                        dwMin = dw;
                        result = e;
                    }
                    var dh = Math.Abs(e.Height - height);
                    if (dh >= dhMin)
                    {
                        continue;
                    }

                    dhMin = dh;

                    result = e;
                }
            }
            result = await NewMethod(width, height, af, format, result);
            return result?.Base64Data;
        }

        private async Task<ThumbNail> NewMethod(int width, int height, AttachedFile af, SKEncodedImageFormat? format, ThumbNail result)
        {
            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            if (result == null)
            {
                var fileName = GetFileFullName(af);
                result = MakeThumb3(fileName, width, height, format);
                if (result != null)
                {
                    result.AttachedFileId = af.Id;
                    af.Thumbs.Add(result);
                    ctx.ThumbNails.Add(result);

                    var toDelete = af.Thumbs.OrderBy(el => el.LastAccess).Skip(10);
                    foreach (var a in toDelete)
                    {
                        af.Thumbs.Remove(a);
                        ctx.ThumbNails.Remove(a);
                    }
                    await ctx.SaveChangesAsync();
                }
            }

            if (result == null)
            {
                return null;
            }

            result.LastAccess = DateTime.UtcNow;

            await ctx.SaveChangesAsync();

            return result;
        }

        private ThumbNail MakeThumb3(string fileName, int width, int height, SKEncodedImageFormat? format = null)
        {
            var fileStream = new FileStream(fileName, FileMode.Open);

            SKBitmap sourceBitmap = default;

            try
            {
                sourceBitmap = SKBitmap.Decode(fileStream);
            }
            catch (Exception ex)
            {
                _log.Error($"SKBitmap.Decode: {ex.Message}");
            }

            fileStream.Close();

            if (sourceBitmap == default)
            {
                return null;
            }

            var quality = SKFilterQuality.Medium;
            using SKBitmap scaledBitmap = sourceBitmap.Resize(new SKImageInfo(width, height), quality);
            using SKImage scaledImage = SKImage.FromBitmap(scaledBitmap);
            if (!format.HasValue)
            {
                format = SKEncodedImageFormat.Jpeg;
            }
            using SKData data = scaledImage.Encode(format.Value, 80);
            var ba = data?.ToArray();
            if (ba != null && ba.Length > 0)
            {
                return new ThumbNail
                {
                    Format = format.Value,
                    Height = height,
                    Width = width,
                    Base64Data = $"data:image/{format.Value.ToString().ToLower()};base64,{Convert.ToBase64String(ba)}",
                };
            }

            return null;
        }

        public async Task<string> GetThumbCachedPercentDb(int width, int height, AttachedFile af, SKEncodedImageFormat? format = null, int dwMax = 10, int dhMax = 10)
        {
            var thumbs = af.Thumbs.Where(el
                => el.Width > 0
                && el.Height > 0
                && ((double)Math.Abs(el.Width - width) * 100 / el.Width) <= dwMax
                && ((double)Math.Abs(el.Height - width) * 100 / el.Height) <= dhMax
                );
            if (format.HasValue)
            {
                thumbs = thumbs.Where(el => el.Format == format.Value);
            }
            ThumbNail result = null;
            var thumbNails = thumbs as ThumbNail[] ?? thumbs.ToArray();

            if (thumbNails.Any())
            {
                result = thumbNails.First();
                var dwMin = (double)Math.Abs(result.Width - width) / result.Width;
                var dhMin = (double)Math.Abs(result.Height - height) / result.Height;
                foreach (var e in thumbNails.Skip(1))
                {
                    var dw = (double)Math.Abs(e.Width - width) / e.Width;
                    if (dw < dwMin)
                    {
                        dwMin = dw;
                        result = e;
                    }
                    var dh = (double)Math.Abs(e.Height - height) / e.Height;
                    if (!(dh < dhMin))
                    {
                        continue;
                    }
                    dhMin = dh;
                    result = e;
                }
            }
            result = await NewMethod(width, height, af, format, result);
            return result?.Base64Data;
        }

        public async Task<FileDownload> FileDownloadAsync(FileDownloadRequest freq)
        {
            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            var af = await ctx.AttachedFiles
                .Where(el => !el.Deleted)
                .Where(el => el.Id == freq.Id)
                .FirstOrDefaultAsync();

            var allowed = await af.IsReadAllowed(userManager.ClaimsPrincipalId(), userManager.CurrentClaimsPrincipal(), ctx, settingsManager);

            if (!allowed)
            {
                throw new Exception($"File id {freq.Id} not found or not allowed for reading");
            }
            var name = Path.Combine(FileStorageDirectory, af.StoragePath, af.StorageName);

            var fileOnDisk = File.OpenRead(name);

            if (freq.Pos < 0 || freq.Pos >= fileOnDisk.Length)
            {
                throw new Exception("File Pos error");
            }

            if (freq.Len > MaxFileLen)
            {
                freq.Len = MaxFileLen;
            }

            var bytes = new byte[freq.Len];

            fileOnDisk.Seek(freq.Pos, SeekOrigin.Begin);

            var readBytes = await fileOnDisk.ReadAsync(bytes, 0, (int)freq.Len);

            var res = new FileDownload
            {
                Data = Convert.ToBase64String(bytes.Take(readBytes).ToArray()),
                Id = af.Id,
                Pos = freq.Pos
            };

            return res;
        }

        public async Task<AttachedFile> FileUploadAsync(IFormFile file, FileUploadForm form)
        {
            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            AttachedFile attachedFile;

            var user = await userManager.CurrentUser();

            if (!CheckAllowedExtension(user, file.FileName))
            {
                throw new Exception("Файл анного типа запрещен для загрузки.");
            }

            if (user == null)
            {
                throw new Exception("Authorization required");
            }
            BaseConstants.AllowedActionFlags need;
            if (form.Id <= 0)
            {
                attachedFile = new AttachedFile
                {
                    Len = file.Length,
                    Name = file.FileName,
                    ContentType = file.ContentType,
                    //ParentId = form.ParentId,
                    //ParentType = form.ParentType,
                    Sequre = form.Sequre
                };
                attachedFile = await CreateDbAsync(attachedFile);
                form.Id = attachedFile.Id;
                need = BaseConstants.AllowedActionFlags.Create;
            }
            else
            {
                need = BaseConstants.AllowedActionFlags.Edit;
            }

            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            attachedFile = await ctx.AttachedFiles
                .Where(el => el.Id == form.Id).
                FirstOrDefaultAsync();

            if (attachedFile == null)
            {
                throw new Exception($"AttachedFile {form.Id} not found");
            }

            if (attachedFile.Incomplete == false)
            {
                throw new Exception($"AttachedFile {form.Id} already uploaded");
            }

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            var aFlags = await attachedFile
                .GetAllowedActions(userManager.ClaimsPrincipalId(), userManager.CurrentClaimsPrincipal(), ctx, settingsManager);

            if (!aFlags.HasFlag(need))
            {
                throw new Exception($"Access denied to attachedFile {attachedFile.Id} for {need}");
            }

            if (string.IsNullOrWhiteSpace(attachedFile.StorageName))
            {
                attachedFile.StorageName = attachedFile.Id.ToString();
            }
            if (string.IsNullOrWhiteSpace(attachedFile.StoragePath))
            {
                attachedFile.StoragePath = CurrentStorageName;
            }

            attachedFile.UploadedBy = user;

            UpdateImageSize(attachedFile, file.OpenReadStream());

            var path = MakePath(attachedFile);

            var name = Path.Combine(path, attachedFile.StorageName);                                //  ./.../[StoragePath]/StoragePath/StorageName

            await using (var fileStream = new FileStream(name, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }

            attachedFile.Incomplete = false;

            ctx.ProtocolRecords.Add(new ProtocolRecord
            {
                User = user,
                AttachedFile = attachedFile,
                Action = "AttachedFile upload completed (POST)"
            });

            ContentTypeSetByExt(attachedFile);

            await ctx.SaveChangesAsync();

            return attachedFile;
        }

        public bool CheckAllowedExtension(User user, string fileName)
        {
            if (user is { LoginSystem: LoginSystem.Form, SelfFormRegister: false })
            {
                return true;
            }

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            var allowedExtensions = settingsManager.SettingValue(SettingsKeys.SystemAttachAllowedExtensions);

            if (string.IsNullOrWhiteSpace(allowedExtensions))
            {
                _log.Error($"Не заданы разрешенные расширения файлов.");
                return false;
            }

            var extensions = allowedExtensions.Split(',')
                .Select(el => el.Trim(' ', '.').ToUpper())
                .Where(el => !string.IsNullOrWhiteSpace(el))
                .Distinct()
                .ToArray();

            var extension = Path.GetExtension(fileName)?
                .Trim(' ', '.')
                .ToUpper();

            if (extensions.Any(el => el == extension))
            {
                return true;
            }

            _log.Error($"Файл данного типа запрещен к загрузке <{fileName}>.");

            return false;
        }

        private static void ContentTypeSetByExt(AttachedFile attachedFile)
        {
            if (string.IsNullOrEmpty(attachedFile.ContentType))
            {
                var ext = Path.GetExtension(attachedFile.Name);
                if (!string.IsNullOrEmpty(ext))
                {
                    ext = ext.ToLower();
                    var d = new Dictionary<string, string>
                    {
                        [".gif"] = "image/gif",
                        [".jpg"] = "image/jpeg",
                        [".jpeg"] = "image/jpeg",
                        [".png"] = "image/png",
                        [".ico"] = "image/vnd.microsoft.icon",
                        [".wbmp"] = "image/vnd.wap.wbmp",
                        [".webp"] = "image/webp",
                    };
                    if (d.ContainsKey(ext))
                    {
                        attachedFile.ContentType = d[ext];
                    }
                }
            }
        }

        private void UpdateImageSize(AttachedFile af, Stream file)
        {
            if (af == null || file == null)
            {
                return;
            }

            file.Seek(0, SeekOrigin.Begin);

            SKImageInfo info = default;

            try
            {
                info = SKBitmap.DecodeBounds(file);
            }
            catch (Exception ex)
            {
                _log.Error($"SKBitmap.DecodeBounds: {ex.Message}");
            }

            if (!info.IsEmpty)
            {
                af.Width = info.Width;
                af.Height = info.Height;
            }
            else
            {
                af.Width = -1;
            }
        }

        private string MakePath(AttachedFile attachedFile)
        {
            var path = Path.Combine(FileStorageDirectory, attachedFile.StoragePath);                                    //  ./.../[StoragePath]/StoragePath

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }

        public async Task<FileUploadResult> FileUploadAsync(FileUpload file)
        {
            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            var user = await userManager.CurrentUser();

            const long cluster = 4096;

            var res = new FileUploadResult();

            var buff = Convert.FromBase64String(file.Data);

            AttachedFile attachedFile = null;

            if (!file.Id.HasValue)
            {
                if (string.IsNullOrWhiteSpace(file.Name))
                {
                    throw new Exception("Не указано имя файла.");
                }

                if (!CheckAllowedExtension(user, file.Name))
                {
                    throw new Exception("Файл данного типа запрещен для загрузки.");
                }

                if (file.Len <= 0)
                {
                    throw new Exception("Длинна файла должна быть больше 0.");
                }

                if (file.Pos < 0)
                {
                    throw new Exception("Позиция части файла не может быть меньше 0.");
                }

                if (file.Pos + buff.Length > file.Len)
                {
                    throw new Exception("Превышена общая длинна файла.");
                }

                attachedFile = new AttachedFile
                {
                    Len = file.Len,
                    Name = file.Name,
                    ContentType = file.ContentType,
                    ParentId = file.ParentId ?? (default),
                    ParentType = file.ParentType,
                    Sequre = file.Sequre ?? (default)
                };
            }
            else
            {
                var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

                attachedFile = await ctx.AttachedFiles
                    .Where(el => el.Id == file.Id)
                    .FirstOrDefaultAsync();

                if (attachedFile == null)
                {
                    throw new Exception($"Файл (id = {file.Id}) не найден.");
                }

                if (!attachedFile.Incomplete)
                {
                    throw new Exception($"Файл (id = {file.Id}) уже полностью загружен.");
                }
            }

            if (buff.Length < cluster && file.Pos + buff.Length < file.Len)
            {
                throw new Exception($"Ошибка передачи части содержимого файла. Только последняя часть может содержать контент меньший {cluster} байт.");
            }

            if (!file.Id.HasValue)
            {
                attachedFile = await CreateDbAsync(attachedFile);
                file.Id ??= attachedFile.Id;
            }

            if (string.IsNullOrWhiteSpace(attachedFile.StorageName))
            {
                attachedFile.StorageName = attachedFile.Id.ToString();
            }

            if (string.IsNullOrWhiteSpace(attachedFile.StoragePath))
            {
                attachedFile.StoragePath = CurrentStorageName;
            }

            attachedFile.UploadedBy = user ?? attachedFile.UploadedBy;
            
            var name = await WriteFileChunk(file, FileStorageDirectory, attachedFile, buff, cluster);

            var chunks = (int)((attachedFile.Len + (cluster - 1)) / cluster);

            var nextChunk = await StickChunks(attachedFile.Len, name, cluster);

            if (nextChunk == -1)
            {
                res.Next = -1;
                res.Percent = 100;
                attachedFile.Incomplete = false;

                await using var fs = File.Open(name, FileMode.Open);

                UpdateImageSize(attachedFile, fs);

                //ctx.ProtocolRecords.Add(new ProtocolRecord
                //{
                //    User = user,
                //    AttachedFile = attachedFile,
                //    Action = $"AttachedFile upload completed (GQL)"
                //});
                //ContentTypeSetByExt(attachedFile);
                //await ctx.SaveChangesAsync();
            }
            else
            {
                res.Next = nextChunk * cluster;
                res.Percent = 100 * nextChunk / chunks;
            }

            res.Id = attachedFile.Id;

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            await context.SaveChangesAsync();

            return res;
        }

        private static async Task<int> StickChunks(long len, string name, long cluster)
        {
            var allChunks = true;
            var nextChunk = 0;
            for (long i = 0; i < len;)
            {
                nextChunk = (int)(i / cluster);
                var chunkName = Path.Combine(name, nextChunk.ToString());
                if (!File.Exists(chunkName))
                {
                    allChunks = false;
                    break;
                }
                var di = (new FileInfo(chunkName)).Length;
                if (di == 0)
                {
                    allChunks = false;
                    break;
                }
                i += di;
            }
            var fileName = name + ".tmp";
            if (allChunks)
            {
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
                using (var stream = File.Open(fileName, FileMode.OpenOrCreate))
                {
                    for (long i = 0; i < len;)
                    {
                        nextChunk = (int)(i / cluster);
                        var chunkName = Path.Combine(name, nextChunk.ToString());
                        await stream.WriteAsync(File.ReadAllBytes(chunkName));
                        var di = (new FileInfo(chunkName)).Length;
                        if (di == 0)
                        {
                            allChunks = false;
                            break;
                        }
                        i += di;
                    }
                }
            }
            if (allChunks)
            {
                Directory.Delete(name, true);
                File.Move(fileName, name);
                return -1;
            }
            return nextChunk;
        }

        private static async Task<string> WriteFileChunk(FileUpload file, string path, AttachedFile attachedFile,
            IReadOnlyCollection<byte> buff, long cluster)
        {
            var dirName = Path.Combine(path, attachedFile.StoragePath);
            if (!Directory.Exists(dirName))
            {
                Directory.CreateDirectory(dirName);
            }

            var name = Path.Combine(dirName, attachedFile.StorageName);
            if (!Directory.Exists(name))
            {
                Directory.CreateDirectory(name);
            }

            var beginChunk = (file.Pos + (cluster - 1)) / cluster;

            var boffset = file.Pos % cluster;

            var blen = buff.Count - boffset;

            if (beginChunk * cluster + blen != attachedFile.Len)
            {
                /*если это не завершающий неполный чанк обрезаем его лишнее справа*/
                blen -= blen % cluster;
            }

            if (blen > 0)
            {
                var chunkName = Path.Combine(name, beginChunk.ToString());

                await using var stream = File.Open(chunkName, FileMode.OpenOrCreate);

                await stream.WriteAsync(buff.Skip((int)boffset).Take((int)blen).ToArray());
            }

            return name;
        }

        private async Task<bool> WriteFile(AttachedFile attachedFile, Stream stream)
        {
            try
            {
                var dirName = Path.Combine(FileStorageDirectory, attachedFile.StoragePath);

                if (!Directory.Exists(dirName))
                {
                    Directory.CreateDirectory(dirName);
                }

                var fileFullPath = Path.Combine(dirName, attachedFile.StorageName);
                
                await using var fileStream = new FileStream(fileFullPath, FileMode.Create, FileAccess.Write);

                stream.Seek(0, SeekOrigin.Begin);

                await stream.CopyToAsync(fileStream);

                await fileStream.FlushAsync();

                fileStream.Close();

                return true;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка сохранения файла.", ex);
            }

            return false;
        }

        private async Task<bool> WriteFile(AttachedFile attachedFile, byte[] byteArray)
        {
            try
            {
                var dirName = Path.Combine(FileStorageDirectory, attachedFile.StoragePath);

                if (!Directory.Exists(dirName))
                {
                    Directory.CreateDirectory(dirName);
                }

                var fileFullPath = Path.Combine(dirName, attachedFile.StorageName);

                await using var fileStream = new FileStream(fileFullPath, FileMode.Create, FileAccess.Write);

                await fileStream.WriteAsync(byteArray, 0, byteArray.Length);

                await fileStream.FlushAsync();

                fileStream.Close();

                return true;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка сохранения файла.", ex);
            }

            return false;
        }

        public async Task<Pagination<AttachedFile>> GetAttachedFilesAsync(QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<AttachedFile>.Items).ToJsonFormat(), out var items);

            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            var obj = ctx.AttachedFiles
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FilterByReadAllowed(userManager.ClaimsPrincipalId(), userManager.CurrentClaimsPrincipal())
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                ;
            var result = new Pagination<AttachedFile>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<AttachedFile>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }

        public async Task<AttachedFile> CreateDbAsync(AttachedFile file)
        {
            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            /*
             Если указан ParentId то проверяется на валидность и в зависимости от ParentType файл
             присоединяется к Appeal или к Message или еще куда-нибудь
             */
            var user = await userManager.CurrentUser() ?? await userManager.GetAnonymousUserAsync();

            var flags = BaseConstants.AllowedActionFlags.None;

            BaseIdEntity parent = null;

            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            if (file.parentId_ > 0 && !string.IsNullOrWhiteSpace(file.parentType_))
            {
                parent = (BaseIdEntity)await BaseConstants.GetEntityAsync(file.parentId_, file.parentType_, ctx);

                if (parent == null)
                {
                    throw new Exception($"ParentId/ParentType {file.ParentId}/{file.ParentType} not found");
                }

                if (file.parentType_ == nameof(Appeal))
                {
                    var appeal = await ctx.Appeals
                        .Where(el => el.Id == file.parentId_)
                        .Include(el => el.Author)
                        .FirstOrDefaultAsync();
                    if (appeal != null && appeal.Author.UserName == "$anonymousAppealUser$")
                    {
                        flags |= BaseConstants.AllowedActionFlags.Create;
                        user ??= appeal.Author;
                    }
                }
            }
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Create))
            {
                if (user.UserName == "$anonymousAppealUser$")
                {
                    flags |= BaseConstants.AllowedActionFlags.Create;
                }
                else
                {
                    var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

                    flags |= await file.GetAllowedActions(userManager.ClaimsPrincipalId(), userManager.CurrentClaimsPrincipal(), ctx,
                        settingsManager);
                }
            }

            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Create))
            {
                throw new Exception("Access Denied");
            }

            file.UploadedBy = user ?? throw new Exception("Нужно авторизоваться");

            if (file.IsPublic)
            {
                throw new Exception("Нельзя сделать файл публичным при создании");
            }

            ctx.AttachedFiles.Add(file);

            await file.ParentSet(parent, ctx);

            //ctx.ProtocolRecords.Add(new ProtocolRecord
            //{
            //    User = user,
            //    AttachedFile = file,
            //    Action = "AttachedFile record created"
            //});

            await ctx.SaveWeightAsync(file);

            return file;
        }

        public async Task<AttachedFile> CreateDbAsync(AttachedFile aFile, byte[] byteArray)
        {
            if (aFile == null || byteArray == null || byteArray.Length == 0)
            {
                return null;
            }

            IDbContextTransaction transaction = null;

            try
            {
                var userManager = _serviceProvider.GetRequiredService<UserManager>();

                var user = await userManager.CurrentUser() ?? await userManager.GetAnonymousUserAsync();

                var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

                transaction = await ctx.Database.BeginTransactionAsync();

                aFile.StoragePath = CurrentStorageName;
                aFile.UploadedBy = user ?? throw new Exception("Нужно авторизоваться");
                aFile.ContentType ??= "application/octet-stream";

                await ctx.AttachedFiles.AddAsync(aFile);

                await ctx.SaveChangesAsync();

                aFile.StorageName = aFile.Id.ToString();

                aFile.Len = byteArray.Length;

                var writeFileResult = await WriteFile(aFile, byteArray);

                if (!writeFileResult)
                {
                    await transaction.RollbackAsync();

                    return null;
                }

                aFile.Incomplete = false;

                await ctx.SaveChangesAsync();

                await transaction.CommitAsync();

                return aFile;
            }
            catch (Exception ex)
            {
                _log.Error("", ex);

                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }
            }

            return null;
        }

        /// <summary>
        /// Только для внутреннего использования
        /// </summary>
        /// <param name="aFile"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public async Task<AttachedFile> CreateDbAsync(AttachedFile aFile, Stream stream)
        {
            if (aFile == null || stream == null || stream.Length == 0)
            {
                return null;
            }

            IDbContextTransaction transaction = null;

            try
            {
                var userManager = _serviceProvider.GetRequiredService<UserManager>();
                
                var user = await userManager.CurrentUser() ?? await userManager.GetAnonymousUserAsync();

                var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

                transaction = await ctx.Database.BeginTransactionAsync();

                aFile.StoragePath = CurrentStorageName;
                aFile.UploadedBy = user ?? throw new Exception("Нужно авторизоваться");
                aFile.ContentType ??= "application/octet-stream";

                await ctx.AttachedFiles.AddAsync(aFile);

                await ctx.SaveChangesAsync();
                
                aFile.StorageName = aFile.Id.ToString();

                aFile.Len = stream.Length;

                var writeFileResult = await WriteFile(aFile, stream);

                if (!writeFileResult)
                {
                    await transaction.RollbackAsync();

                    return null;
                }

                aFile.Incomplete = false;

                await ctx.SaveChangesAsync();

                await transaction.CommitAsync();

                return aFile;
            }
            catch (Exception ex)
            {
                _log.Error("", ex);

                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }
            }

            return null;
        }

        public async Task<AttachedFile> UpdateDbAsync(long id, AttachedFile src, GraphQlKeyDictionary fields)
        {
            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            var dest = await ctx.AttachedFiles.FirstOrDefaultAsync(el => el.Id == id);

            if (dest == null)
            {
                throw new Exception($"AttachedFile {id} not found");
            }

            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            var flags = await dest.GetAllowedActions(userManager.ClaimsPrincipalId(), userManager.CurrentClaimsPrincipal(), ctx, settingsManager);

            var arFields = fields.Select(el => el.Key).ToDictionary(el => el);
            if (arFields.Remove(nameof(AttachedFile.Private).ToJsonFormat()))
            {
                var userId = userManager.ClaimsPrincipalId();

                if (userId != dest.UploadedById)
                {
                    throw new Exception($"Флаг {nameof(AttachedFile.Private)} может менять только юзер, загрузивший файл");
                }
            }
            if (arFields.Remove(nameof(AttachedFile.IsPublic).ToJsonFormat()))
            {
                if (src.IsPublic && !flags.HasFlag(BaseConstants.AllowedActionFlags.Moderate))
                {
                    throw new Exception($"Опубликовать аттач может только модератор");
                }
            }
            if (arFields.Count > 0)
            {
                if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Edit))
                {
                    throw new Exception($"AttachedFile {id} Access Denied");
                }
            }

            var old = (AttachedFile)dest.Clone();

            BaseConstants.UpdateEntityByFieldsList(dest, src, fields);

            var diff = old.MemberwiseDiff(dest);
            if (!string.IsNullOrEmpty(diff))
            {
                ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = await userManager.CurrentUser(),
                    AttachedFile = dest,
                    Action = $"AttachedFile record changed",
                    Data = diff
                });
            }

            await ctx.SaveChangesAsync();

            return dest;
        }

        public async Task<AttachedFile> ApproveAsync(long id, AttachedFileApprove src, GraphQlKeyDictionary updateKeys)
        {
            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            var userId = userManager.ClaimsPrincipalId();

            if (userId == null)
            {
                throw new Exception($"Нужно авторизоваться");
            }

            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            var dest = await ctx.AttachedFiles
                .Where(el => el.Id == id)
                .Where(el => !el.Deleted)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"AttachedFileId {id} not found");
            }

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            if (!(await dest
                    .GetAllowedActions(userManager.ClaimsPrincipalId(), userManager.CurrentClaimsPrincipal(), ctx,
                        settingsManager))
                .HasFlag(BaseConstants.AllowedActionFlags.Moderate))
            {
                throw new Exception($"Вам это нельзя (AttachedFile)");
            }

            var old = (AttachedFile)dest.Clone();

            if (src.Public_.HasValue && dest.IsPublic != src.Public_.Value)
            {
                dest.IsPublic = src.Public_.Value;
            }

            var diff = old.MemberwiseDiff(dest);

            if (!string.IsNullOrEmpty(diff))
            {
                ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = await userManager.CurrentUser(),
                    AttachedFile = dest,
                    Action = $"AttachedFile moderated {dest.Id}",
                    Data = diff
                });
            }

            await ctx.SaveChangesAsync();

            return dest;
        }

        public bool DeleteAttach(AttachedFile aFile)
        {
            var fileName = GetFileFullName(aFile);

            _log.Info($"DeleteAttach удаление файла (Id = {aFile.Id}) => '{fileName}'");
            /*Собираемся удалить файлы, привязанные к аттачам.
             Но тут осторожничаем.*/
            if (fileName != null && Regex.IsMatch(Path.GetFileName(fileName), "^[0-9]+$"))
            {
                /*Имя файла должно состоять только из цифр*/
                var noDelete = false;

                if (aFile.Incomplete && Directory.Exists(fileName))
                {
                    /*Файл не был докачан - значит кусочки лежат в папке*/
                    var fl = Directory.GetFiles(fileName);

                    foreach (var f in fl)
                    {
                        if (!Regex.IsMatch(Path.GetFileName(f), "^[0-9]+$"))
                        {
                            /*Если вдруг в папке что-то неподходящее - не удаляем ничего!*/
                            noDelete = true;
                            break;
                        }
                    }
                    if (!noDelete)
                    {
                        _log.Info($"AttachedFile {aFile.Id} is incomplete, delete dir '{fileName}'");
                        Directory.Delete(fileName, true);
                    }
                }
                if (!aFile.Incomplete && File.Exists(fileName))
                {
                    /*Файл был докачан и существует*/
                    _log.Info($"AttachedFile {aFile.Id} '{fileName}' exist => delete");
                    File.Delete(fileName);
                }
            }

            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            foreach (var protocolRecord in ctx.ProtocolRecords.Where(el => el.AttachedFile.Id == aFile.Id))
            {
                protocolRecord.AttachedFile = null;
            }

            ctx.AttachedFiles.Remove(aFile);

            return true;
        }

        public bool DeleteDirectory(string directoryPath)
        {
            _log.Info($"DeleteDirectory удаление директории => '{directoryPath}'");

            try
            {
                if (Directory.Exists(directoryPath))
                {
                    Directory.Delete(directoryPath, true);
                }

                return true;
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка удаления директории <{directoryPath}>", ex);
            }

            return false;

        }

        public bool DeleteFile(string filePath)
        {
            try
            {
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                return true;
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка удаления файла <{filePath}>", ex);
            }

            return false;

        }

        public IEnumerable<string> GetAllDirectories()
        {
            var result = new List<string>();

            if (!Directory.Exists(FileStorageDirectory))
            {
                return result;
            }

            result.AddRange(Directory.GetDirectories(FileStorageDirectory));

            return result;
        }

        public IEnumerable<string> GetFilesInDirectory(string directoryPath)
        {
            var result = new List<string>();

            if (!Directory.Exists(directoryPath))
            {
                return result;
            }

            result.AddRange(Directory.GetFiles(directoryPath));

            return result;
        }
    }
}
