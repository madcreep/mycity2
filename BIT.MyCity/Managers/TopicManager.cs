using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Managers
{
    public class TopicManager
    {
        private readonly IServiceProvider _servicesProvider;

        public TopicManager(IServiceProvider servicesProvider)
        {
            _servicesProvider = servicesProvider;
        }

        public async Task<IdentityResult> CheckRegionsForNewUserAsync(List<UserSubsystemDirectoryId> dicIds)
        {
            if (dicIds == null || !dicIds.Any())
            {
                return IdentityResult.Success;
            }

            var context = _servicesProvider.GetRequiredService<DocumentsContext>();

            var subsystems = await context.Subsystems
                .ToDictionaryAsync(el => el.UID);

            var result = new List<IdentityError>();

            foreach (var ssItem in dicIds)
            {
                if (string.IsNullOrWhiteSpace(ssItem.SubsystemUid))
                {
                    result.Add(new IdentityError
                    {
                        Code = "TestTerritoriesError",
                        Description = "Не указана подсистема для тем"
                    });

                    continue;
                }

                if (!subsystems.ContainsKey(ssItem.SubsystemUid))
                {
                    result.Add(new IdentityError
                    {
                        Code = "TestTerritoriesError",
                        Description = $"Подсистема указанная для темы \"{ssItem.SubsystemUid}\" отсутствует"
                    });

                    continue;
                }

                var testIds = ssItem.Ids
                    .Distinct()
                    .ToArray();

                if (testIds.Length != ssItem.Ids.Length)
                {
                    result.Add(new IdentityError
                    {
                        Code = "TestTerritoriesError",
                        Description = $"Переданы дублирующиеся темы для подсистемы \"{ssItem.SubsystemUid}\""
                    });
                }

                var ssDicIds = subsystems[ssItem.SubsystemUid]
                    .TopicIds
                    ?? new List<long>();

                var contains = ssDicIds
                    .Intersect(ssItem.Ids)
                    .ToArray();

                if (contains.Length != testIds.Length)
                {
                    var idStr = string.Join(" ", testIds.Except(ssDicIds));

                    result.Add(new IdentityError
                    {
                        Code = "TestTerritoriesError",
                        Description = $"Переданы отсутствующие в подсистеме \"{ssItem.SubsystemUid}\" темы : {idStr}"
                    });
                }
            }

            return result.Any()
                ? IdentityResult.Failed(result.ToArray())
                : IdentityResult.Success;
        }
    }
}
