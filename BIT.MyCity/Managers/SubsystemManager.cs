using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using BIT.MyCity.Subsystems;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL.Utilities;
using log4net;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BIT.MyCity.Managers
{
    public class SubsystemManager
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(SubsystemManager));
        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        public SubsystemManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
        }
        public async Task<Pagination<Subsystem>> GetSubsystemsAsync(QueryOptions options)
        {
            var obj = _ctx.Subsystems
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal())
                .FiltersBy(options.Filters, options.WithDeleted);

            var result = new Pagination<Subsystem>();

            if (options.RequestedFields.ContainsKey(nameof(Pagination<Subsystem>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }

            if (options.RequestedFields.TryGetValue(nameof(Pagination<Subsystem>.Items).ToJsonFormat(), out var items))
            {
                result.Items = await obj
                    .IncludeByFields(items)
                    .IncludeByNotMappedFields(items)
                    .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                    .GetRange(options.Range?.Take, options.Range?.Skip)
                    .AsSplitQuery()
                    .ToListAsync();

                foreach (var a in result.Items)
                {
                    GetFromSettings(items, a);
                }
            }
            return result;
        }

        private void GetFromSettings(GraphQlKeyDictionary items, Subsystem a)
        {
            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();
            if (items.ContainsKey(nameof(Subsystem.Name).ToJsonFormat()))
            {
                a.Name = settingsManager.SettingValue($"subsystem.{a.UID}.name.");
            }
            if (items.ContainsKey(nameof(Subsystem.Description).ToJsonFormat()))
            {
                a.Description = settingsManager.SettingValue($"subsystem.{a.UID}.description.");
            }
        }

        private async Task<Subsystem> GetSubsystemAsync(string uid, GraphQlKeyDictionary requestedFields)
        {
            var a = await _ctx.Subsystems
                    .Where(el => el.UID == uid)
                    .IncludeByFields(requestedFields)
                    .IncludeByNotMappedFields(requestedFields)
                    .FirstOrDefaultAsync();
            GetFromSettings(requestedFields, a);
            return a;
        }

        public async Task<Subsystem> CreateDbAsync(Subsystem obj, GraphQlKeyDictionary updatedKeys, GraphQlKeyDictionary requestedFields)
        {
            if (await _ctx.Subsystems.Where(el => !el.Deleted).AnyAsync(el => el.UID == obj.UID))
            {
                throw new Exception($"Subsystem '{obj.UID}' already exist");
            }
            if (obj.ModeratorId.HasValue)
            {
                obj.Moderator = await _ctx.Users.Where(el => !el.Disconnected && el.Id == obj.ModeratorId.Value).FirstOrDefaultAsync();
                if (obj.Moderator == null)
                {
                    throw new Exception($"User {obj.ModeratorId.Value} not found");
                }
            }
            obj.Region2Subsystems = new List<Region2Subsystem>();
            obj.Rubric2Subsystems = new List<Rubric2Subsystem>();
            obj.Territory2Subsystems = new List<Territory2Subsystem>();
            obj.Topic2Subsystems = new List<Topic2Subsystem>();
            await BaseConstants.UpdateObjIdsList(
                destIds: new List<long>(),
                srcIds: obj.RegionIds,
                db: _ctx.Regions,
                funcAdd: t => obj.Region2Subsystems.Add(new Region2Subsystem { Subsystem = obj, Region = t }));
            await BaseConstants.UpdateObjIdsList(
                destIds: new List<long>(),
                srcIds: obj.RubricIds,
                db: _ctx.Rubrics,
                funcAdd: t => obj.Rubric2Subsystems.Add(new Rubric2Subsystem { Subsystem = obj, Rubric = t }));
            await BaseConstants.UpdateObjIdsList(
                destIds: new List<long>(),
                srcIds: obj.TerritoryIds,
                db: _ctx.Territories,
                funcAdd: t => obj.Territory2Subsystems.Add(new Territory2Subsystem { Subsystem = obj, Territory = t }));
            await BaseConstants.UpdateObjIdsList(
                destIds: new List<long>(),
                srcIds: obj.TopicIds,
                db: _ctx.Topics,
                funcAdd: t => obj.Topic2Subsystems.Add(new Topic2Subsystem { Subsystem = obj, Topic = t }));
            //foreach (var name in obj.AppealPropertyNames)
            //{
            //    PropertyInfo t
            //        = typeof(Appeal).GetProperty(name,
            //            BindingFlags.IgnoreCase
            //            | BindingFlags.Public
            //            | BindingFlags.Instance);
            //    if (t == null)
            //    {
            //        throw new Exception($"Prorerty:'{name}' not found in Appeal");
            //    }
            //}
            if (obj.MainEntityName != null)
            {
                NewMethod(obj.MainEntityFields, obj.MainEntityName);
            }
            _ctx.Subsystems.Add(obj);
            await _ctx.SaveChangesAsync();
            return await GetSubsystemAsync(obj.UID, requestedFields);
        }

        private void NewMethod(List<SubsystemMainEntityField> fields, string entityName)
        {
            var mainEntity = _ctx
                .GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .FirstOrDefault(el => el.PropertyType.GenericTypeArguments.Length == 1
                      && el.PropertyType.GenericTypeArguments[0].GetInterfaces().Contains(typeof(IBaseIdEntity))
                      && el.PropertyType.GenericTypeArguments[0].Name == entityName);
            if (mainEntity == null)
            {
                throw new Exception($"Entity:'{entityName}' not found");
            }
            foreach (var field in fields)
            {
                var property = mainEntity.GetType().GetProperty(field.NameAPI, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                if (property == null)
                {
                    throw new Exception($"Prorerty:'{field.NameAPI}' not found in {mainEntity.Name}");
                }
                field.NameAPI = property.Name;
                if (field.NameAPISecond != null)
                {
                    var property2 = mainEntity.GetType().GetProperty(field.NameAPISecond, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                    if (property2 == null)
                    {
                        throw new Exception($"Prorerty:'{field.NameAPI}' not found in {mainEntity.Name}");
                    }
                    field.NameAPISecond = property2.Name;
                }
                _ctx.SubsystemMainEntityFields.Add(field);
            }
        }

        public async Task<Subsystem> UpdateDbAsync(string uid, Subsystem src, GraphQlKeyDictionary updatedKeys, GraphQlKeyDictionary requestedFields)
        {
            var dest = await _ctx.Subsystems
                .Where(el => el.UID == uid)
                .Include(el => el.Region2Subsystems).ThenInclude(el => el.Region)
                .Include(el => el.Rubric2Subsystems).ThenInclude(el => el.Rubric)
                .Include(el => el.Territory2Subsystems).ThenInclude(el => el.Territory)
                .Include(el => el.Topic2Subsystems).ThenInclude(el => el.Topic)
                .Include(subsystem => subsystem.MainEntityFields)
                .FirstOrDefaultAsync();

            if (dest == null)
            {
                throw new Exception($"Subsystem '{uid}' not found");
            }
            if (src.ModeratorId.HasValue)
            {
                dest.Moderator = await _ctx.Users.Where(el => !el.Disconnected && el.Id == src.ModeratorId.Value).FirstOrDefaultAsync();
                if (dest.Moderator == null)
                {
                    throw new Exception($"User {src.ModeratorId.Value} not found");
                }
            }

            /*говнокооод говнокоод*/
            var settings = new List<Settings>();
            if (updatedKeys.ContainsKey(nameof(Subsystem.Name).ToJsonFormat()))
            {
                settings.Add(new Settings { Key = $"subsystem.{uid}.name.", Value = src.Name });
            }
            if (updatedKeys.ContainsKey(nameof(Subsystem.Description).ToJsonFormat()))
            {
                settings.Add(new Settings { Key = $"subsystem.{uid}.description.", Value = src.Description });
            }
            if (settings.Count > 0)
            {
                var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();
                var result = await settingsManager.Set(settings);
                if (!result.Success)
                {
                    throw new Exception($"{result.Errors}");
                }
            }

            if (updatedKeys.ContainsKey(nameof(Subsystem.RegionIds).ToJsonFormat()))
            {
                await BaseConstants.SetT2LinkedEntIdsAsync(
                src.RegionIds,
                dest.Region2Subsystems,
                async uid => await _ctx.Regions.Where(el => !el.Deleted && el.Id == uid).FirstOrDefaultAsync(),
                (u, uid) => new Region2Subsystem { Subsystem = dest, SubsystemId = dest.Id, Region = u, RegionId = uid },
                uid => dest.Region2Subsystems.Any(el => el.RegionId == uid)
                );
            }

            await BaseConstants.UpdateObjIdsList(
                destIds: dest.Region2Subsystems.Select(el => el.RegionId).ToList(),
                srcIds: src.RegionIdsChange,
                db: _ctx.Regions,
                funcAdd: obj => dest.Region2Subsystems.Add(new Region2Subsystem { Subsystem = dest, Region = obj }),
                funcDel: obj => dest.Region2Subsystems.Remove(dest.Region2Subsystems.Where(el => el.RegionId == obj.Id).First())
                );

            if (updatedKeys.ContainsKey(nameof(Subsystem.RubricIds).ToJsonFormat()))
            {
                await BaseConstants.SetT2LinkedEntIdsAsync(
                src.RubricIds,
                dest.Rubric2Subsystems,
                async uid => await _ctx.Rubrics
                    .Where(el => !el.Deleted && el.Id == uid)
                    .FirstOrDefaultAsync(),
                (u, uid) => new Rubric2Subsystem { Subsystem = dest, SubsystemId = dest.Id, Rubric = u, RubricId = uid },
                uid => dest.Rubric2Subsystems.Any(el => el.RubricId == uid)
                );
            }

            await BaseConstants.UpdateObjIdsList(
                destIds: dest.Rubric2Subsystems.Select(el => el.RubricId).ToList(),
                srcIds: src.RubricIdsChange,
                db: _ctx.Rubrics,
                funcAdd: obj => dest.Rubric2Subsystems
                    .Add(new Rubric2Subsystem { Subsystem = dest, Rubric = obj }),
                funcDel: obj => dest.Rubric2Subsystems
                    .Remove(dest.Rubric2Subsystems.First(el => el.RubricId == obj.Id))
                );

            if (updatedKeys.ContainsKey(nameof(Subsystem.TerritoryIds).ToJsonFormat()))
            {
                await BaseConstants.SetT2LinkedEntIdsAsync(
                src.TerritoryIds,
                dest.Territory2Subsystems,
                async uid => await _ctx.Territories.Where(el => !el.Deleted && el.Id == uid).FirstOrDefaultAsync(),
                (u, uid) => new Territory2Subsystem { Subsystem = dest, SubsystemId = dest.Id, Territory = u, TerritoryId = uid },
                uid => dest.Territory2Subsystems.Any(el => el.TerritoryId == uid)
                );
            }
            await BaseConstants.UpdateObjIdsList(
                destIds: dest.Territory2Subsystems.Select(el => el.TerritoryId).ToList(),
                srcIds: src.TerritoryIdsChange,
                db: _ctx.Territories,
                funcAdd: obj => dest.Territory2Subsystems.Add(new Territory2Subsystem { Subsystem = dest, Territory = obj }),
                funcDel: obj => dest.Territory2Subsystems.Remove(dest.Territory2Subsystems.Where(el => el.TerritoryId == obj.Id).First())
                );

            if (updatedKeys.ContainsKey(nameof(Subsystem.TopicIds).ToJsonFormat()))
            {
                await BaseConstants.SetT2LinkedEntIdsAsync(
                src.TopicIds,
                dest.Topic2Subsystems,
                async uid => await _ctx.Topics.Where(el => !el.Deleted && el.Id == uid).FirstOrDefaultAsync(),
                (u, uid) => new Topic2Subsystem { Subsystem = dest, SubsystemId = dest.Id, Topic = u, TopicId = uid },
                uid => dest.Topic2Subsystems.Any(el => el.TopicId == uid)
                );
            }
            await BaseConstants.UpdateObjIdsList(
                destIds: dest.Topic2Subsystems.Select(el => el.TopicId).ToList(),
                srcIds: src.TopicIdsChange,
                db: _ctx.Topics,
                funcAdd: obj => dest.Topic2Subsystems.Add(new Topic2Subsystem { Subsystem = dest, Topic = obj }),
                funcDel: obj => dest.Topic2Subsystems.Remove(dest.Topic2Subsystems.Where(el => el.TopicId == obj.Id).First())
                );

            List<string> l = typeof(Appeal)
                .GetProperties(
                    BindingFlags.IgnoreCase
                    | BindingFlags.Public
                    | BindingFlags.Instance)
                .Select(p => p.Name).ToList();

            //if (updatedKeys.ContainsKey(nameof(Subsystem.AppealPropertyNames).ToJsonFormat()))
            //{
            //    dest.AppealPropertyNames.Clear();
            //    foreach (var a in src.AppealPropertyNames)
            //    {
            //        var n = typeof(Appeal).GetProperty(a, BindingFlags.IgnoreCase
            //        | BindingFlags.Public
            //        | BindingFlags.Instance)?.Name;
            //        if (n == null)
            //        {
            //            throw new Exception($"Appeal property {a} not found");
            //        }
            //        if (!dest.AppealPropertyNames.Contains(n))
            //        {
            //            dest.AppealPropertyNames.Add(n);
            //        }
            //    }
            //}

            if (updatedKeys.ContainsKey(nameof(Subsystem.MainEntityFields).ToJsonFormat()) || (src.MainEntityName != null && src.MainEntityName != dest.MainEntityName))
            {
                if (dest.MainEntityFields == null)
                {
                    await dest.LoadReference(dest.MainEntityFields, _ctx);
                }
                if (dest.MainEntityFields != null)
                {
                    foreach (var a in dest.MainEntityFields)
                    {
                        _ctx.SubsystemMainEntityFields.Remove(a);
                    }
                    dest.MainEntityFields.Clear();
                }
            }
            if (updatedKeys.ContainsKey(nameof(Subsystem.MainEntityFields).ToJsonFormat()))
            {
                NewMethod(src.MainEntityFields, src.MainEntityName ?? dest.MainEntityName);
            }
            if (updatedKeys.ContainsKey(nameof(Subsystem.MainEntityFieldsUpdate).ToJsonFormat()))
            {
                foreach (var field in src.MainEntityFieldsUpdate)
                {
                    var f = dest.MainEntityFields.Where(el => el.NameAPI == field.NameAPI).FirstOrDefault();
                    if (f != null)
                    {
                        dest.MainEntityFields.Remove(f);
                        _ctx.SubsystemMainEntityFields.Remove(f);
                    }
                    dest.MainEntityFields.Add(field);
                    _ctx.SubsystemMainEntityFields.Add(field);
                }
            }

            //BaseConstants.UpdateObjNamesList(dest.AppealPropertyNames, src.AppealPropertyNamesChange, l, "AppealPropertyName");
            BaseConstants.UpdateEntityByFieldsList(dest, src, updatedKeys, new string[] { nameof(dest.MainEntityFields), nameof(dest.MainEntityFieldsUpdate) });
            await _ctx.SaveChangesAsync();
            return await GetSubsystemAsync(uid, requestedFields);
        }

        public async Task<bool> SetDefaultValues()
        {
            try
            {
                var subsystemService = _serviceProvider.GetRequiredService<SubsystemService>();

                foreach (var subsystem in subsystemService.Subsystems)
                {
                    if (_ctx.Subsystems.FirstOrDefault(el => el.UID == subsystem.SystemKey) != null)
                    {
                        continue;
                    }

                    _ctx.Subsystems.Add(new Subsystem
                    {
                        UID = subsystem.SystemKey,
                    });

                    await _ctx.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка установки значений по умолчанию таблицы подсистем", ex);

                return false;
            }
            return true;
        }

        private class DefaultMetaData : IHasSubsystemMetaData
        {
            public string SubsystemUID { get; set; }
            #region IHasSubsystemMetaData
            public string MainEntityName { get; set; }//Appeal, News, VoteRoot
            public List<SubsystemMainEntityField> MainEntityFields { get; set; } = new List<SubsystemMainEntityField>();
            public bool IsPublicDefault { get; set; }
            public bool PrivateOnly { get; set; }
            public bool UseLikesForMain { get; set; }
            public bool UseLikesForComments { get; set; }
            public bool UseDislikes { get; set; }
            public bool UseRates { get; set; }

            public bool UseComments { get; set; }
            public bool UseAttaches { get; set; }
            public bool UseMaps { get; set; }
            public bool UseRubricsDescription { get; set; }
            public bool UseTerritoriesDescription { get; set; }
            public bool UseRegionsDescription { get; set; }
            public bool UseTopicsDescription { get; set; }
            public bool UseInMobileClient { get; set; }
            #endregion
        }

        private readonly IEnumerable<DefaultMetaData> DefaultMetaDatas = new DefaultMetaData[]
        {
            new DefaultMetaData
            {
                SubsystemUID = SSUID.citizens,
                MainEntityName = nameof(Appeal),
                UseMaps = true,
                UseComments = true,
                UseAttaches = true,
                UseRates = true,
                UseLikesForMain = true,
                UseLikesForComments = true,
                UseDislikes = true,
                UseInMobileClient = false,
                MainEntityFields = new List<SubsystemMainEntityField>
                {
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.SubsystemId),
                        Required = true
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Author),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Latitude),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Longitude),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Place),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Approved),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.RejectionReason),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Topic),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.PublicGranted),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.SiteStatus),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Rubric),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Territory),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.ExtNumber),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.ExtDate),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Description),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.AttachedFiles),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Region1),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Zip),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Address),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Phone),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Email),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Platform),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.AppealType),
                    },
                }
            },
            new DefaultMetaData
            {
                SubsystemUID = SSUID.organizations,
                MainEntityName = nameof(Appeal),
                UseComments = true,
                UseAttaches = true,
                UseRates = true,
                UseLikesForMain = true,
                UseLikesForComments = true,
                UseDislikes = true,
                MainEntityFields = new List<SubsystemMainEntityField>
                {
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.SubsystemId),
                        Required = true
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Author),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = $"{nameof(Appeal.Author)}.{nameof(Appeal.Author.FullName)}",
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = $"{nameof(Appeal.Organization)}.{nameof(Appeal.Organization.Email)}",
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = $"{nameof(Appeal.Organization)}.{nameof(Appeal.Organization.FullName)}",
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = $"{nameof(Appeal.Organization)}.{nameof(Appeal.Organization.ShortName)}",
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = $"{nameof(Appeal.Organization)}.{nameof(Appeal.Organization.Ogrn)}",
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = $"{nameof(Appeal.Organization)}.{nameof(Appeal.Organization.Inn)}",
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = $"{nameof(Appeal.Organization)}.{nameof(Appeal.Organization.Addresses)}",
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Title),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Approved),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.RejectionReason),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Topic),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.SiteStatus),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Rubric),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Territory),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Addressee),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.WORegNumberRegDate),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.RegNumber),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.RegDate),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Signatory),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Post),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.ExtNumber),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.ExtDate),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Description),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.AttachedFiles),
                    },
                    new SubsystemMainEntityField
                    {
                        NameAPI = nameof(Appeal.Organization),
                    },
                }
            }
        };

        public async Task<bool> SetDefaultMetaData()
        {
            try
            {
                var subsystems = await _ctx.Subsystems.Where(el => el.MainEntityName == null || el.MainEntityName == "").Include(el => el.MainEntityFields).ToListAsync();
                foreach (var ss in subsystems)
                {
                    var d = DefaultMetaDatas.Where(el => el.SubsystemUID == ss.UID).FirstOrDefault();
                    if (d == null) continue;
                    ss.MainEntityName = d.MainEntityName;
                    ss.MainEntityFields?.Clear();
                    ss.MainEntityFields = d.MainEntityFields;
                    await _ctx.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка установки значений по умолчанию метаданных подсистем", ex);

                return false;
            }
            return true;
        }

    }
    public static class SubsystemManagerExtensions
    {
        public static IQueryable<Subsystem> IncludeByNotMappedFields(this IQueryable<Subsystem> obj, GraphQlKeyDictionary fields)
        {
            if (fields == null || obj == null)
            {
                return obj;
            }

            if (fields.ContainsKey(nameof(Subsystem.Regions).ToJsonFormat()))
            {
                obj = obj.Include(el => el.Region2Subsystems).ThenInclude(el => el.Region);
            }
            if (fields.ContainsKey(nameof(Subsystem.Rubrics).ToJsonFormat()))
            {
                obj = obj.Include(el => el.Rubric2Subsystems).ThenInclude(el => el.Rubric);
            }
            if (fields.ContainsKey(nameof(Subsystem.Territories).ToJsonFormat()))
            {
                obj = obj.Include(el => el.Territory2Subsystems).ThenInclude(el => el.Territory);
            }
            if (fields.ContainsKey(nameof(Subsystem.Topics).ToJsonFormat()))
            {
                obj = obj.Include(el => el.Topic2Subsystems).ThenInclude(el => el.Topic);
            }
            return obj;
        }
    }
}
