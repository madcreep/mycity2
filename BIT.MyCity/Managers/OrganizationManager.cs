using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL.Utilities;
using log4net;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Managers
{
    public class OrganizationManager
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(OrganizationManager));
        private IServiceProvider _serviceProvider;

        public OrganizationManager(IServiceProvider servicesProvider)
        {
            _serviceProvider = servicesProvider;
        }

        public async Task<Pagination<UserOrganization>> GetOrganizationsAsync(
            SelectionRange range,
            List<GraphQlQueryFilter> filter,
            GraphQlKeyDictionary requestedKeys)
        {
            var result = new Pagination<UserOrganization>();

            var itemsKeys = requestedKeys;

            if (requestedKeys.ContainsKey(nameof(Pagination<UserOrganization>.Items).ToJsonFormat()))
            {
                itemsKeys = requestedKeys[nameof(Pagination<UserOrganization>.Items).ToJsonFormat()];
            }

            var query = GetOrganizationsQuery(filter, itemsKeys);

            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            if (!userManager.HasClaim(ClaimName.Manage, "users")
                && !userManager.HasClaim(ClaimName.Moderate, "organizations"))
            {
                var currentUserId = userManager.ClaimsPrincipalId() ?? 0;

                query = query
                    .Where(el => el.Users2Organizations
                        .Any(x => x.UserId == currentUserId));
            }

            if (requestedKeys.ContainsKey(nameof(Pagination<User>.Total).ToJsonFormat()))
            {
                result.Total = await query.LongCountAsync();
            }

            if (itemsKeys == null)
            {
                return result;
            }

            query = query
                .OrderByFieldNames(nameof(UserOrganization.Id), range?.OrderBy)
                .GetRange(range?.Take, range?.Skip);

            result.Items = await query.ToArrayAsync();

            return result;
        }

        public async Task<Pagination<UserOrganization>> GetCurrentUserOrganizationsAsync(
            SelectionRange range,
            GraphQlKeyDictionary requestedKeys)
        {
            var result = new Pagination<UserOrganization>();

            var itemsKeys = requestedKeys;

            if (requestedKeys.ContainsKey(nameof(Pagination<UserOrganization>.Items).ToJsonFormat()))
            {
                itemsKeys = requestedKeys[nameof(Pagination<UserOrganization>.Items).ToJsonFormat()];
            }

            var query = GetOrganizationsQuery(null, itemsKeys);

            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            var currentUserId = userManager.ClaimsPrincipalId() ?? 0;

            query = query
                .Where(el => el.Users2Organizations
                    .Any(x => x.UserId == currentUserId));

            if (requestedKeys.ContainsKey(nameof(Pagination<User>.Total).ToJsonFormat()))
            {
                result.Total = await query.LongCountAsync();
            }

            if (itemsKeys == null)
            {
                return result;
            }

            query = query
                .OrderByFieldNames(nameof(UserOrganization.Id), range?.OrderBy)
                .GetRange(range?.Take, range?.Skip);

            result.Items = await query.ToArrayAsync();

            return result;
        }

        private IQueryable<UserOrganization> GetOrganizationsQuery(List<GraphQlQueryFilter> filter, GraphQlKeyDictionary itemsKeys)
        {

            var userFilter = filter?
                                 .Where(el => el.Name == nameof(UserOrganization.UserIds).ToJsonFormat())
                                 .ToArray()
                             ?? new GraphQlQueryFilter[0];

            foreach (var itemFilter in userFilter)
            {
                itemFilter.Name =
                    $"{nameof(UserOrganization.Users2Organizations).ToJsonFormat()}.{nameof(User2Organization.UserId).ToJsonFormat()}";
            }

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            var query = context.UserOrganizations
                .IncludeByKeys(itemsKeys)
                .FiltersBy(filter);

            return query;
        }
    }
}
