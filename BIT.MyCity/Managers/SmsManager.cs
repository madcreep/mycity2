using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using BIT.MyCity.Sms.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SmsMessage = BIT.MyCity.Database.SmsMessage;

namespace BIT.MyCity.Managers
{
    public class SmsManager
    {
        private readonly IServiceProvider _servicesProvider;

        public SmsManager(IServiceProvider servicesProvider)
        {
            _servicesProvider = servicesProvider;
        }

        public async Task<Balance> GetBalanceAsync()
        {
            var smsService = _servicesProvider.GetRequiredService<SmsService>();

            var result = await smsService.GetBalanceAsync();

            return result;
        }

        
    }
}
