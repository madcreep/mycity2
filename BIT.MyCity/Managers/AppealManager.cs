using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using BIT.MyCity.Subsystems;
using BIT.MyCity.Utilites;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL.Utilities;
using log4net;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BIT.MyCity.Managers
{
    public sealed class AppealManager
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(AppealManager));

        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        private readonly SettingsManager _sm;
        private readonly ElasticClient _elasticClient;
        public AppealManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
            _sm = _serviceProvider.GetRequiredService<SettingsManager>();
            try
            {
                _elasticClient = (ElasticClient)_serviceProvider.GetRequiredService<IElasticClient>();
            }
            catch
            {
                //ignore
            }
        }

        public async Task<Pagination<Appeal>> GetAppealsAsync(QueryOptions options)
        {
            var user = await _um.CurrentUser();

            var request = _ctx.Appeals
                .FiltersBy(options.Filters, options.WithDeleted)
                .FilterByReadAllowedAsync(_um, user, _sm, options.WithLinkedByOrganizations);
            //.FiltersBy(options.FiltersNew);
            
            var result = new Pagination<Appeal>();

            if (options.RequestedFields.ContainsKey(nameof(Pagination<Appeal>.Total).ToJsonFormat()))
            {
                result.Total = await request.LongCountAsync();
            }

            if (!options.RequestedFields.ContainsKey(nameof(Pagination<Appeal>.Items).ToJsonFormat()) ||
                !options.RequestedFields.TryGetValue(nameof(Pagination<Appeal>.Items).ToJsonFormat(), out var items) ||
                items == null)
            {
                return result;
            }

            var containsAttachedFiles = items.ContainsKey(nameof(Appeal.AttachedFiles).ToJsonFormat());

            if (containsAttachedFiles)
            {
                items.Remove(nameof(Appeal.AttachedFiles).ToJsonFormat());
            }

            request = request
                .IncludeByFields(items)
                .IncludeFilterByNotMappedFields(items).FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
                .GetRange(options.Range?.Take, options.Range?.Skip);


            result.Items = await request.ToListAsync();

            if (containsAttachedFiles)
            {
                var userId = user?.Id ?? -100L;
                var userIsAdminGroup = _um.HasClaim(ClaimName.Moderate,
                                           $"{SubsystemCitizenAppeals.BaseSystemKey}.{SSClaimSuffix.Md.enable}")
                                       || _um.HasClaim(ClaimName.Moderate,
                                           $"{SubsystemOrganizationAppeals.BaseSystemKey}.{SSClaimSuffix.Md.enable}")
                                       || _um.HasClaim(ClaimName.Subsystem,
                                           $"{SubsystemCitizenAppeals.BaseSystemKey}.{SSClaimSuffix.Ss.executor}")
                                       || _um.HasClaim(ClaimName.Subsystem,
                                           $"{SubsystemOrganizationAppeals.BaseSystemKey}.{SSClaimSuffix.Ss.executor}")
                                       || _um.HasClaim(ClaimName.Service);

                var appealsId = result.Items
                    .Select(el => el.Id);

                var fRequest = _ctx.AttachedFilesToAppeals
                    .Include(el => el.AttachedFile)
                    .Where(el=> appealsId.Contains(el.AppealId));


                if (userIsAdminGroup)
                {
                    //request = request.IncludeFilter(el => el.AttachedFilesToAppeal
                    //    .Select(x => x.AttachedFile));
                }
                else
                {
                    fRequest = fRequest
                        .Where(el => el.AttachedFile.IsPublic || el.Appeal.AuthorId == userId);
                    //request = request.IncludeFilter(el =>
                    //    el.AttachedFilesToAppeal
                    //        .Where(x => x.AttachedFile.IsPublic || el.AuthorId == userId)
                    //        .Select(x => x.AttachedFile));
                }

                _ = await fRequest.ToArrayAsync();

                foreach (var item in result.Items)
                {
                    item.AttachedFiles ??= new List<AttachedFile>();
                }
            }

            if (!items.ContainsKey(nameof(Appeal.UnapprovedMessages).ToJsonFormat()) &&
                !items.ContainsKey(nameof(Appeal.HasUnapprovedMessages).ToJsonFormat()) &&
                !items.ContainsKey(nameof(Appeal.UnapprovedMessagesCount).ToJsonFormat()) &&
                !items.ContainsKey(nameof(Appeal.MessagesCount).ToJsonFormat()) &&
                !items.ContainsKey(nameof(Appeal.Messages).ToJsonFormat()))
            {
                return result;
            }

            items.TryGetValue(nameof(Appeal.Messages).ToJsonFormat(), out var messageItems);
            items.TryGetValue(nameof(Appeal.UnapprovedMessages).ToJsonFormat(), out var unapprovedMessageItems);

            var messagesRequest = _ctx.Messages
                .Where(el => result.Items.Select(a => a.Id).Contains(el.Appeal.Id));

            if (!items.ContainsKey(nameof(Appeal.MessagesCount).ToJsonFormat()))
            {
                messagesRequest = messagesRequest
                    .Where(el => el.Approved == ApprovedEnum.UNDEFINED);
            }

            messagesRequest = messagesRequest
                .IncludeByFields(messageItems)
                .IncludeByNotMappedFields(messageItems)
                .IncludeByFields(unapprovedMessageItems)
                .IncludeByNotMappedFields(unapprovedMessageItems)
                .FilterByReadAllowed(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal(), _sm);

            var messages = (await messagesRequest
                .ToArrayAsync())
                .GroupBy(el => el.AppealId)
                .ToDictionary(el => el.Key, el => el.ToArray());

            if (items.ContainsKey(nameof(Appeal.UnapprovedMessages).ToJsonFormat()))
            {
                foreach (var appeal in result.Items)
                {
                    if (messages.ContainsKey(appeal.Id))
                    {
                        appeal.UnapprovedMessages.AddRange(messages[appeal.Id]
                            .Where(el => el.Approved == ApprovedEnum.UNDEFINED));
                    }
                }
            }

            if (items.ContainsKey(nameof(Appeal.HasUnapprovedMessages).ToJsonFormat()))
            {
                foreach (var appeal in result.Items)
                {
                    appeal.HasUnapprovedMessages = messages.ContainsKey(appeal.Id)
                                                   && messages[appeal.Id].Any(el => el.Approved == ApprovedEnum.UNDEFINED);
                }
            }

            if (items.ContainsKey(nameof(Appeal.UnapprovedMessagesCount).ToJsonFormat()))
            {
                foreach (var appeal in result.Items)
                {
                    appeal.UnapprovedMessagesCount = messages.TryGetValue(appeal.Id, out var message)
                        ? message.Count(el => el.Approved == ApprovedEnum.UNDEFINED)
                        : 0;
                }
            }

            if (items.ContainsKey(nameof(Appeal.MessagesCount).ToJsonFormat()))
            {
                foreach (var appeal in result.Items)
                {
                    appeal.MessagesCount = messages.TryGetValue(appeal.Id, out var message)
                        ? message.Count()
                        : 0;
                }
            }

            return result;
        }

        public async Task<Pagination<AppealsStatBy>> GetAppealsStatByGroupAsync(QueryOptions options, int offsetHours = 0)
        {
            if (!options.RequestedFields.TryGetValue(nameof(Pagination<AppealsStatBy>.Items).ToJsonFormat(),
                out var items))
            {
                return null;
            }

            var obj = _ctx.Appeals
                .FiltersBy(options.Filters, options.WithDeleted);

            if (items.ContainsKey(nameof(AppealsStatBy.Rubric).ToJsonFormat()))
            {
                obj = obj.Include(el => el.Rubric);
            }
            else if (items.ContainsKey(nameof(AppealsStatBy.Region).ToJsonFormat()))
            {
                obj = obj.Include(el => el.Region);
            }
            else if (items.ContainsKey(nameof(AppealsStatBy.Territory).ToJsonFormat()))
            {
                obj = obj.Include(el => el.Territory);
            }
            else if (items.ContainsKey(nameof(AppealsStatBy.Topic).ToJsonFormat()))
            {
                obj = obj.Include(el => el.Topic);
            }
            var result = new Pagination<AppealsStatBy>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<AppealsStatBy>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }

            if (!options.RequestedFields.ContainsKey(nameof(Pagination<AppealsStatBy>.Items).ToJsonFormat()))
            {
                return result;
            }


            result.Items = new List<AppealsStatBy>();
            var obj1 = obj
                .Select(el => new
                {
                    Rubric = items.ContainsKey(nameof(AppealsStatBy.Rubric).ToJsonFormat()) ? el.Rubric : default,
                    Region = items.ContainsKey(nameof(AppealsStatBy.Region).ToJsonFormat()) ? el.Region : default,
                    Territory = items.ContainsKey(nameof(AppealsStatBy.Territory).ToJsonFormat()) ? el.Territory : default,
                    Topic = items.ContainsKey(nameof(AppealsStatBy.Topic).ToJsonFormat()) ? el.Topic : default,

                    CreatedAtHour = items.ContainsKey(nameof(AppealsStatBy.CreatedAtHour).ToJsonFormat()) ? new int?(el.CreatedAt.AddHours(offsetHours).Hour) : default,
                    CreatedAtDay = items.ContainsKey(nameof(AppealsStatBy.CreatedAtDay).ToJsonFormat()) ? new int?(el.CreatedAt.AddHours(offsetHours).Day) : default,
                    CreatedAtMonth = items.ContainsKey(nameof(AppealsStatBy.CreatedAtMonth).ToJsonFormat()) ? new int?(el.CreatedAt.AddHours(offsetHours).Month) : default,
                    CreatedAtYear = items.ContainsKey(nameof(AppealsStatBy.CreatedAtYear).ToJsonFormat()) ? new int?(el.CreatedAt.AddHours(offsetHours).Year) : default,

                    UpdatedAtHour = items.ContainsKey(nameof(AppealsStatBy.UpdatedAtHour).ToJsonFormat()) ? (el.UpdatedAt.HasValue ? new int?(el.UpdatedAt.Value.AddHours(offsetHours).Hour) : default) : default,
                    UpdatedAtDay = items.ContainsKey(nameof(AppealsStatBy.UpdatedAtDay).ToJsonFormat()) ? (el.UpdatedAt.HasValue ? new int?(el.UpdatedAt.Value.AddHours(offsetHours).Day) : default) : default,
                    UpdatedAtMonth = items.ContainsKey(nameof(AppealsStatBy.UpdatedAtMonth).ToJsonFormat()) ? (el.UpdatedAt.HasValue ? new int?(el.UpdatedAt.Value.AddHours(offsetHours).Month) : default) : default,
                    UpdatedAtYear = items.ContainsKey(nameof(AppealsStatBy.UpdatedAtYear).ToJsonFormat()) ? (el.UpdatedAt.HasValue ? new int?(el.UpdatedAt.Value.AddHours(offsetHours).Year) : default) : default,
                })
                .AsEnumerable();
            var obj2 = obj1
                .GroupBy(el => new
                {
                    el.Rubric,
                    el.Region,
                    el.Territory,
                    el.Topic,
                    el.CreatedAtHour,
                    el.CreatedAtDay,
                    el.CreatedAtMonth,
                    el.CreatedAtYear,
                    el.UpdatedAtHour,
                    el.UpdatedAtDay,
                    el.UpdatedAtMonth,
                    el.UpdatedAtYear
                });
            var obj3 = obj2
                    .Select(el => new AppealsStatBy
                    {
                        Region = el.Key.Region,
                        Rubric = el.Key.Rubric,
                        Territory = el.Key.Territory,
                        Topic = el.Key.Topic,
                        CreatedAtHour = DateTime.SpecifyKind(new DateTime(
                            el.Key.CreatedAtYear ?? 1900,
                            el.Key.CreatedAtMonth ?? 1,
                            el.Key.CreatedAtDay ?? 1,
                            el.Key.CreatedAtHour ?? 0,
                            0,
                            0
                        ), DateTimeKind.Utc),
                        CreatedAtDay = DateTime.SpecifyKind(new DateTime(
                            el.Key.CreatedAtYear ?? 1900,
                            el.Key.CreatedAtMonth ?? 1,
                            el.Key.CreatedAtDay ?? 1
                        ), DateTimeKind.Utc),
                        CreatedAtMonth = DateTime.SpecifyKind(new DateTime(
                            el.Key.CreatedAtYear ?? 1900,
                            el.Key.CreatedAtMonth ?? 1,
                            1
                        ), DateTimeKind.Utc),
                        CreatedAtYear = DateTime.SpecifyKind(new DateTime(
                            el.Key.CreatedAtYear ?? 1900,
                            1,
                            1
                        ), DateTimeKind.Utc),
                        UpdatedAtHour = DateTime.SpecifyKind(new DateTime(
                            el.Key.UpdatedAtYear ?? 1900,
                            el.Key.UpdatedAtMonth ?? 1,
                            el.Key.UpdatedAtDay ?? 1,
                            el.Key.UpdatedAtHour ?? 0,
                            0,
                            0
                        ), DateTimeKind.Utc),
                        UpdatedAtDay = DateTime.SpecifyKind(new DateTime(
                            el.Key.UpdatedAtYear ?? 1900,
                            el.Key.UpdatedAtMonth ?? 1,
                            el.Key.UpdatedAtDay ?? 1
                        ), DateTimeKind.Utc),
                        UpdatedAtMonth = DateTime.SpecifyKind(new DateTime(
                            el.Key.UpdatedAtYear ?? 1900,
                            el.Key.UpdatedAtMonth ?? 1,
                            1
                        ), DateTimeKind.Utc),
                        UpdatedAtYear = DateTime.SpecifyKind(new DateTime(
                            el.Key.UpdatedAtYear ?? 1900,
                            1,
                            1
                        ), DateTimeKind.Utc),
                        AppealCount = el.Count(),
                    })
                ;
            result.Items = obj3.ToList();

            return result;
        }

        public async Task<Pagination<Appeal>> GetAppealsWhereModeratorAsync(long id, List<string> subsystemUidList, QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<Appeal>.Items).ToJsonFormat(), out var items);
            var user = await _um.CurrentUser();
            User moderator;
            if (id > 0)
            {
                moderator = await _ctx.Users.Where(el => el.Id == id).FirstOrDefaultAsync();
                if (moderator == null)
                {
                    throw new Exception($"Moderator {id} not found");
                }
            }
            else
            {
                moderator = user;
            }
            var obj = _ctx.Appeals
            .IncludeByFields(items)
            .IncludeByNotMappedFields(items)
            .FilterByModeratorAsync(moderator, subsystemUidList)
            .FilterByReadAllowedAsync(_um, user, _sm, options.WithLinkedByOrganizations)
            .FiltersBy(options.Filters, options.WithDeleted)
            .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
            ;
            return await BaseConstants.MakePaginationResult(obj, options);
        }

        public async Task<Pagination<Appeal>> GetAppealsWhereExecutorAsync(long id, List<string> subsystemUidList, QueryOptions options)
        {
            options.RequestedFields.TryGetValue(nameof(Pagination<Appeal>.Items).ToJsonFormat(), out var items);
            User user = await _um.CurrentUser();
            User executor;
            if (id > 0)
            {
                executor = await _ctx.Users.Where(el => el.Id == id).FirstOrDefaultAsync();
                if (executor == null)
                {
                    throw new Exception($"Executor {id} not found");
                }
            }
            else
            {
                executor = user;
            }
            var obj = _ctx.Appeals
            .IncludeByFields(items)
            .IncludeByNotMappedFields(items)
            .FilterByExecutorAsync(executor, subsystemUidList)
            .FilterByReadAllowedAsync(_um, user, _sm, options.WithLinkedByOrganizations)
            .FiltersBy(options.Filters, options.WithDeleted)
            .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy)
            ;
            return await BaseConstants.MakePaginationResult(obj, options);
        }

        public async Task<Pagination<Appeal>> GetAppealsFbAsync(QueryOptions options, string email)
        {
            var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();
            var normEmail = keyNormalizer.NormalizeEmail(email);
            options.RequestedFields.TryGetValue(nameof(Pagination<Appeal>.Items).ToJsonFormat(), out var items);
            
            var obj = _ctx.Appeals
                .Where(el => el.AppealType == AppealTypeEnum.FEEDBACK && el.Email == normEmail)
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FiltersBy(options.Filters, options.WithDeleted)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), options.Range?.OrderBy);
            return await BaseConstants.MakePaginationResult(obj, options);
        }

        public async Task<List<Appeal>> GetAllAppealsAsync(GraphQlKeyDictionary requestedFields, List<GraphQlQueryFilter> filters, SelectionRange range)
        {
            var obj = await _ctx.Appeals
                .MakeQueryEntity(requestedFields, filters, range)
                .ToListAsync();
            return obj;
        }

        public async Task<Pagination<Appeal>> SearchAppealsAsync(string query, QueryOptions options)
        {
            if (_elasticClient == null)
            {
                return null;
            }
            options.RequestedFields.TryGetValue(nameof(Pagination<Appeal>.Items).ToJsonFormat(), out var items);
            var result = new Pagination<Appeal>();

            query ??= "";
            var skip = options.Range?.Skip ?? 0;
            var take = options.Range?.Take ?? 100;
            var response = await _elasticClient.SearchAsync<ESAppeal>(
                s => s.Query(q => q.QueryString(d => d.Query(query)))
                    .From(skip)
                    .Size(take)
                    //.Analyzer("russian_morphology")
                    );
            if (response.IsValid)
            {
                result.Total = response.Total;
                if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
                {
                    var ids = response.Documents.Select(el => el.Id).ToList();
                    if (ids.Count > 0)
                    {
                        User user = await _um.CurrentUser();
                        result.Items = await _ctx.Appeals
                            .Where(el => ids.Contains(el.Id))
                            .IncludeByFields(items)
                            .IncludeByNotMappedFields(items)
                            .FilterByReadAllowedAsync(_um, user, _sm, options.WithLinkedByOrganizations)
                            .ToListAsync();
                    }
                }
            }
            return result;
        }

        public async Task<Pagination<Appeal>> FtsAppealsAsync(string query, QueryOptions options)
        {
            var user = await _um.CurrentUser();
            options.RequestedFields.TryGetValue(nameof(Pagination<Appeal>.Items).ToJsonFormat(), out var items);
            var result = new Pagination<Appeal>();

            query ??= "";
            query = query.Trim();
            query = Regex.Replace(query, "[^a-zA-Z0-9а-яёА-ЯЁ]+", " ");
            query = Regex.Replace(query, " +", "|");

            var obj = _ctx.Appeals
                .IncludeByFields(items)
                .IncludeByNotMappedFields(items)
                .FiltersBy(options.Filters, options.WithDeleted)
                .FilterByReadAllowedAsync(_um, user, _sm, options.WithLinkedByOrganizations);
            //var tot = await obj.LongCountAsync();
            //if (tot > 0)
            //{
            var obj1 = obj
                .Where(el => el.SearchVector.Matches(EF.Functions.ToTsQuery(query)))
                .Select(el => new
                {
                    Appeal = el,
                    Rank = el.SearchVector.Rank(EF.Functions.ToTsQuery(query))
                });

            var obj2 = obj1
                .Where(el => el.Rank > 0)
                .OrderByDescending(el => el.Rank)
                .ThenByDescending(el => el.Appeal.CreatedAt)
                .Select(el => el.Appeal);

            result.Total = await obj2.LongCountAsync();

            if (options.Range?.Skip != null)
                obj2 = obj2.Skip(options.Range.Skip.Value);
            if (options.Range?.Take != null)
                obj2 = obj2.Take(options.Range.Take.Value);
            result.Items = await obj2.ToListAsync();
            //}
            //else
            //{
            //    result.Items = new List<Appeal>();
            //}
            return result;
        }

        private async Task CheckAppealWriteSubsystemRights(Appeal src)
        {
            if (src == null)
            {
                return;
            }

            await src.LoadReference(src.Subsystem, _ctx);

            if (src.Subsystem == null)
            {
                return;
            }

            if (src.RegionId.HasValue)
            {
                if (src.Subsystem.Region2Subsystems == null)
                {
                    await _ctx.Entry(src.Subsystem).Collection(el => el.Region2Subsystems).Query().Where(el => el.RegionId == src.RegionId.Value && el.SubsystemId == src.Subsystem.Id).LoadAsync();
                }

                if (src.Subsystem?.Region2Subsystems != null
                    && !src.Subsystem.Region2Subsystems.Any(el => el.RegionId == src.RegionId.Value && el.SubsystemId == src.Subsystem.Id))
                {
                    throw new Exception($"Region {src.RegionId} нельзя использовать в подсистеме {src.Subsystem.UID}");
                }
            }
            if (src.RubricId.HasValue)
            {
                if (src.Subsystem?.Rubric2Subsystems == null)
                {
                    await _ctx.Entry(src.Subsystem)
                        .Collection(el => el.Rubric2Subsystems)
                        .Query()
                        .Where(el => el.RubricId == src.RubricId.Value && el.SubsystemId == src.Subsystem.Id)
                        .LoadAsync();
                }
                
                if (src.Subsystem?.Rubric2Subsystems != null
                    && !src.Subsystem.Rubric2Subsystems.Any(el => el.RubricId == src.RubricId.Value && el.SubsystemId == src.Subsystem.Id))
                {
                    throw new Exception($"Rubric {src.RubricId} нельзя использовать в подсистеме {src.Subsystem.UID}");
                }
            }
            if (src.TerritoryId.HasValue)
            {
                if (src.Subsystem?.Territory2Subsystems == null)
                {
                    await _ctx.Entry(src.Subsystem)
                        .Collection(el => el.Territory2Subsystems)
                        .Query()
                        .Where(el => el.TerritoryId == src.TerritoryId.Value && el.SubsystemId == src.Subsystem.Id)
                        .LoadAsync();
                }

                if (src.Subsystem?.Territory2Subsystems != null
                    && !src.Subsystem.Territory2Subsystems.Any(el => el.TerritoryId == src.TerritoryId.Value && el.SubsystemId == src.Subsystem.Id))
                {
                    throw new Exception($"Territory Id:{src.TerritoryId} нельзя использовать в подсистеме {src.Subsystem.UID}");
                }
            }
            if (src.TopicId.HasValue)
            {
                if (src.Subsystem?.Topic2Subsystems == null)
                {
                    await _ctx.Entry(src.Subsystem)
                        .Collection(el => el.Topic2Subsystems)
                        .Query()
                        .Where(el => el.TopicId == src.TopicId.Value && el.SubsystemId == src.Subsystem.Id)
                        .LoadAsync();
                }


                if (src.Subsystem?.Topic2Subsystems != null
                    && !src.Subsystem.Topic2Subsystems.Any(el => el.TopicId == src.TopicId.Value && el.SubsystemId == src.Subsystem.Id))
                {
                    throw new Exception($"Topic Id:{src.TopicId} нельзя использовать в подсистеме {src.Subsystem.UID}");
                }
            }
        }

        public async Task<Appeal> CreateDbFbAsync(Appeal src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            CheckFbAuthority(src);
            if (_um.ClaimsPrincipalId() == null)
            {
                src.Author = _ctx.Users
                    .FirstOrDefault(el => el.UserName == "$feedBackUser$" && !el.Disconnected);
            }
            else
            {
                src.Author = await _um.CurrentUser();
            }

            src.AppealType = AppealTypeEnum.FEEDBACK;

            return await CreateDbAsync(src, updateFields, requestedFields);
        }

        public async Task<Appeal> CreateDbAsync(Appeal src, GraphQlKeyDictionary updateFields, GraphQlKeyDictionary requestedFields)
        {
            /*AuthorId - обязателен и должен быть валиден
             RubricId,
             RegionId,
             TerritoryId,
             TopicId - необязательны, но если заданы то должны быть валидны
             AttachedFilesIds - необязательны, но если заданы то должны быть валидны
             */

            var userId = _um.ClaimsPrincipalId();

            var userRequest = userId.HasValue
                ? _ctx.Users.Where(el => el.Id == userId.Value)
                : _ctx.Users.Where(el => el.UserName == "$anonymousAppealUser$" && !el.Disconnected);

            src.Author = await userRequest
                    .Include(el => el.IdentityClaims)
                    .Include(el => el.UserRole)
                    .ThenInclude(el => el.Role)
                    .ThenInclude(el => el.IdentityClaims)
                    .FirstOrDefaultAsync();

            src.Subsystem = await _ctx.Subsystems
                .Where(el => !el.Deleted)
                .Where(el => el.UID == src.SubsystemId)
                .FirstOrDefaultAsync();

            if (src.Subsystem == null)
            {
                throw new Exception($"Подсистема '{src.SubsystemId}' не найдена");
            }

            if (src.AppealType == AppealTypeEnum.FEEDBACK)
            {
                CheckFbAuthority(src);

                if (!updateFields.ContainsKey(nameof(Appeal.SiteStatus).ToJsonFormat()))
                {
                    src.SiteStatus = await GetInitialStatus("feedbackStatus", null, (int)SiteStatus.Registered);
                }
                else
                {
                    if (!await _ctx.EnumVerbs
                        .AnyAsync(el => el.Group == "feedbackStatus"
                                        && !el.Deleted
                                        && el.Value == src.SiteStatus.ToString()))
                    {
                        throw new Exception($"Статус '{src.SiteStatus}' не является допустимым для {src.AppealType}");
                    }
                }
            }
            else
            {
                if (src.Author == null)
                {
                    throw new Exception("Нужно авторизоваться");
                }

                if (!updateFields.ContainsKey(nameof(Appeal.SiteStatus).ToJsonFormat()))
                {
                    src.SiteStatus = await GetInitialStatus("appealStatus", src.Subsystem.UID, (int)SiteStatus.Sent);
                }
                else
                {
                    if (!await _ctx.EnumVerbs
                        .AnyAsync(el => el.Group == "appealStatus"
                                        && !el.Deleted
                                        && el.SubsystemUID == src.Subsystem.UID
                                        && el.Value == src.SiteStatus.ToString()))
                    {
                        throw new Exception($"Статус '{src.SiteStatus}' не является допустимым для {src.AppealType}");
                    }
                }
            }

            src.Organization = await BaseConstants.GetObjById<UserOrganization>(src.OrganizationId, _ctx);

            if (src.SubsystemId == SubsystemOrganizationAppeals.BaseSystemKey && src.OrganizationId == null && src.AppealType != AppealTypeEnum.FEEDBACK)
            {
                throw new Exception($"Необходимо указать OrganizationId для подсистемы {src.SubsystemId}");
            }

            src.Rubric = await BaseConstants.GetObjById(src.RubricId, _ctx.Rubrics);
            src.Region = await BaseConstants.GetObjById(src.RegionId, _ctx.Regions);
            src.Territory = await BaseConstants.GetObjById(src.TerritoryId, _ctx.Territories);
            src.Topic = await BaseConstants.GetObjById(src.TopicId, _ctx.Topics);

            if (!updateFields.ContainsKey(nameof(Appeal.SubscribeToAnswers).ToJsonFormat()))
            {
                src.SubscribeToAnswers = _sm.SettingValue<SubscribeToAnswersEnum>($"subsystem.{src.SubsystemId ?? SSUID.citizens}.subscribe_to_answers.");
            }

            if (src.AppealType != AppealTypeEnum.FEEDBACK)
            {
                if (!(await src.GetAllowedActions(src.Author.Id, src.Author.AllClaims, _sm, _ctx))
                    .HasFlag(BaseConstants.AllowedActionFlags.Create))
                {
                    throw new Exception("Вам это нельзя");
                }
            }

            await CheckAppealWriteSubsystemRights(src);

            var trn = await _ctx.Database.BeginTransactionAsync();
            try
            {
                await UpdateAttachedFilesIds(src, src);

                await BaseConstants.UpdateObjIdsList(
                    destIds: new List<long>(),
                    srcIds: src.AppealExecutorIds,
                    db: _ctx.AppealExecutors,
                    funcAdd: obj => src.Appeal2Executors.Add(new Appeal2Executor { Appeal = src, AppealExecutor = obj }));

                await BaseConstants.UpdateObjIdsList(
                    destIds: new List<long>(),
                    srcIds: src.AppealPrincipalExecutorIds,
                    db: _ctx.AppealExecutors,
                    funcAdd: obj => src.Appeal2PrincipalExecutors.Add(new Appeal2PrincipalExecutor { Appeal = src, AppealPrincipalExecutor = obj }));

                src.NeedToUpdateInExt = true;
                src.Uploaded = src.AttachedFiles == null || !src.AttachedFiles.Any(el => el.Incomplete);
                _ctx.Appeals.Add(src);

                src.ViewSet(true, src.Author);

                //_ctx.ProtocolRecords.Add(new ProtocolRecord
                //{
                //    User = src.Author,
                //    Appeal = src,
                //    Subsystem = src.Subsystem,
                //    Action = $"Appeal created"
                //});
                await _ctx.SaveWeightAsync(src);
            }
            catch
            {
                await trn.RollbackAsync();
                throw;
            }
            await trn.CommitAsync();
            if (_elasticClient != null)
            {
                await _elasticClient.IndexDocumentAsync(new ESAppeal(src));
            }

            await SendEmailMessagesOnCreate(src);
            await SendPushMessagesOnCreate(src);

            return await GetAppealAsync(src.Id, requestedFields);
        }

        private async Task<int> GetInitialStatus(string group, string subsystemUid, int statusDefault)
        {
            var s = await _ctx.EnumVerbs
                .Where(el => !el.Deleted && el.Group == group && el.SubsystemUID == subsystemUid)
                .ToListAsync();

            var minInitialValue = int.MaxValue;

            foreach (var a in s)
            {
                if (
                    a.CustomSettings.TryDeserializeJson<AppealStatusSettings>(out var statusSettings) &&
                    statusSettings.StatusOptions.IsInitial &&
                    int.TryParse(a.Value, out var v) &&
                    v < minInitialValue
                    )
                {
                    minInitialValue = v;
                }
            }
            return minInitialValue < int.MaxValue ? minInitialValue : statusDefault;
        }

        private void CheckFbAuthority(Appeal src)
        {
            if (src.Author == null && src.Email == null)
            {
                throw new Exception($"Для отправки сообщени обратной связи нужно авторизоваться либо заполнить поле Email");
            }
            if (src.Email != null)
            {
                if (!EmailUtility.CheckEmail(src.Email))
                {
                    throw new Exception($"Не валидный адрес электронной почты : {src.Email}");
                }
                var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();
                src.Email = keyNormalizer.NormalizeEmail(src.Email);
            }
        }

        public async Task<Appeal> UpdateDbAsync(long id, Appeal src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var userId = _um.ClaimsPrincipalId();
            if (userId == null)
            {
                throw new Exception("Нужно авторизоваться");
            }

            var dest = await _ctx.Appeals
                .Where(el => el.Id == id)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"AppealId {id} not found");
            }
            if (!(await dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()?.Claims, _sm, _ctx)).HasFlag(BaseConstants.AllowedActionFlags.Edit))
            {
                throw new Exception($"Вам это нельзя");
            }
            await dest.LoadReference(dest.Subsystem, _ctx);
            src.Subsystem = dest.Subsystem;

            if (updateKeys.ContainsKey(nameof(Appeal.SiteStatus).ToJsonFormat()))
            {
                do
                {
                    string group, ssuid = dest.Subsystem?.UID;

                    if (dest.AppealType == AppealTypeEnum.FEEDBACK)
                    {
                        group = "feedbackStatus";
                    }
                    else
                    {
                        group = "appealStatus";
                    }

                    var statusNew = await _ctx.EnumVerbs
                        .Where(el => el.Group == group && el.SubsystemUID == ssuid && el.Value == src.SiteStatus.ToString())
                        .FirstOrDefaultAsync();

                    if (statusNew == null)
                    {
                        throw new Exception($"Попытка недопустимой смены статуса {dest.AppealType} (Статус {src.SiteStatus} не существует)");
                    }

                    var statusOld = await _ctx.EnumVerbs
                        .Where(el => el.Group == group && el.SubsystemUID == ssuid && el.Value == dest.SiteStatus.ToString())
                        .FirstOrDefaultAsync();

                    if (statusOld != null && statusOld.CustomSettings.TryDeserializeJson<AppealStatusSettings>(out var statusSettings))
                    {
                        if (dest.SiteStatus != src.SiteStatus && !statusSettings.StatusOptions.NextStatus.Contains(src.SiteStatus))
                        {
                            throw new Exception($"Попытка недопустимой смены статуса {dest.AppealType}: [{dest.SiteStatus}]=>[{src.SiteStatus}]");
                        }
                    }

                } while (false);
            }

            await CheckAppealWriteSubsystemRights(src);
            var old = (Appeal)dest.Clone();
            dest.Rubric = await BaseConstants.GetObjById(src.RubricId, _ctx.Rubrics) ?? dest.Rubric;
            dest.Region = await BaseConstants.GetObjById(src.RegionId, _ctx.Regions) ?? dest.Region;
            dest.Territory = await BaseConstants.GetObjById(src.TerritoryId, _ctx.Territories) ?? dest.Territory;
            dest.Topic = await BaseConstants.GetObjById(src.TopicId, _ctx.Topics) ?? dest.Topic;

            //await BaseConstants.ModifyIdsList(nameof(src.AttachedFiles), src, dest, _ctx.AttachedFiles, _ctx, el => el.AttachedFiles);
            await UpdateAttachedFilesIds(src, dest);

            if (updateKeys.ContainsKey(nameof(Appeal.ModeratorsIds).ToJsonFormat()))
            {
                await BaseConstants.SetT2LinkedEntIdsAsync(
                    src.ModeratorsIds,
                    dest.UserModeratorAppeal,
                    async uid => await _ctx.Users.Where(el => !el.Disconnected && el.Id == uid).FirstOrDefaultAsync(),
                    (u, uid) => new UserModeratorAppeal { Appeal = dest, User = u },
                    uid => dest.UserModeratorAppeal.Any(el => el.UserId == uid)
                    );
            }
            if (updateKeys.ContainsKey(nameof(Appeal.ModeratorsIdsChange).ToJsonFormat()))
            {
                await _ctx.Entry(dest).Collection(el => el.UserModeratorAppeal).Query().Where(el => el.AppealId == dest.Id).LoadAsync();
                await BaseConstants.UpdateT2UsersIdsAsync(
                src.ModeratorsIdsChange,
                dest.UserModeratorAppeal,
                new UserModeratorAppeal { Appeal = dest, AppealId = dest.Id },
                _ctx,
                fileId => _ctx.UserModeratorAppeals.FirstOrDefaultAsync(el => el.AppealId == dest.Id && el.UserId == fileId)
                );
            }
            if (updateKeys.ContainsKey(nameof(Appeal.AppealExecutorIds).ToJsonFormat()))
            {
                await BaseConstants.SetT2LinkedEntIdsAsync(
                src.AppealExecutorIds,
                dest.Appeal2Executors,
                async uid => await _ctx.AppealExecutors.Where(el => !el.Deleted && el.Id == uid).FirstOrDefaultAsync(),
                (u, uid) => new Appeal2Executor { Appeal = dest, AppealExecutor = u },
                uid => dest.Appeal2Executors.Any(el => el.AppealExecutorId == uid)
                );
            }
            if (updateKeys.ContainsKey(nameof(Appeal.AppealExecutorIdsChange).ToJsonFormat()))
            {
                await _ctx.Entry(dest).Collection(el => el.Appeal2Executors).Query().Where(el => el.AppealId == dest.Id).LoadAsync();
                await BaseConstants.UpdateObjIdsList(
                destIds: dest.Appeal2Executors.Select(el => el.AppealExecutorId).ToList(),
                srcIds: src.AppealExecutorIdsChange,
                db: _ctx.AppealExecutors,
                funcAdd: obj => dest.Appeal2Executors.Add(new Appeal2Executor { Appeal = src, AppealExecutor = obj }),
                funcDel: obj => dest.Appeal2Executors.Remove(dest.Appeal2Executors.Where(el => el.AppealExecutorId == obj.Id).First())
                );
            }
            if (updateKeys.ContainsKey(nameof(Appeal.AppealPrincipalExecutorIds).ToJsonFormat()))
            {
                await BaseConstants.SetT2LinkedEntIdsAsync(
                src.AppealPrincipalExecutorIds,
                dest.Appeal2PrincipalExecutors,
                async uid => await _ctx.AppealExecutors.Where(el => !el.Deleted && el.Id == uid).FirstOrDefaultAsync(),
                (u, uid) => new Appeal2PrincipalExecutor { Appeal = dest, AppealPrincipalExecutor = u },
                uid => dest.Appeal2PrincipalExecutors.Any(el => el.AppealPrincipalExecutorId == uid)
                );
            }

            if (updateKeys.ContainsKey(nameof(Appeal.AppealPrincipalExecutorIdsChange).ToJsonFormat()))
            {
                await _ctx.Entry(dest).Collection(el => el.Appeal2PrincipalExecutors).Query().Where(el => el.AppealId == dest.Id).LoadAsync();
                await BaseConstants.UpdateObjIdsList(
                    destIds: dest.Appeal2PrincipalExecutors.Select(el => el.AppealPrincipalExecutorId).ToList(),
                    srcIds: src.AppealPrincipalExecutorIdsChange,
                    db: _ctx.AppealExecutors,
                    funcAdd: obj => dest.Appeal2PrincipalExecutors.Add(new Appeal2PrincipalExecutor { Appeal = src, AppealPrincipalExecutor = obj }),
                    funcDel: obj => dest.Appeal2PrincipalExecutors.Remove(dest.Appeal2PrincipalExecutors.First(el => el.AppealPrincipalExecutorId == obj.Id))
                    );
            }

            BaseConstants.UpdateEntityByFieldsList(dest, src, updateKeys);

            await _ctx.Entry(dest)
                .Collection(el => el.AttachedFilesToAppeal)
                .Query()
                .Include(el => el.AttachedFile)
                .Where(el => el.AttachedFileId == dest.Id)
                .LoadAsync();

            dest.Uploaded = !dest.AttachedFiles.Any(el => el.Incomplete);

            var diff = old.MemberwiseDiff(dest);
            if (!string.IsNullOrEmpty(diff))
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = await _um.CurrentUser(),
                    Appeal = dest,
                    Subsystem = dest.Subsystem,
                    Action = "Appeal changed",
                    Data = diff
                });
            }

            if (dest.SiteStatus != old.SiteStatus)
            {
                var views = await _ctx.UserViewAppeals
                    .Where(el => el.AppealId == id)
                    .ToArrayAsync();

                foreach (var view in views)
                {
                    view.Viewed = false;
                }

                dest.ViewsCount = 0;
            }

            await _ctx.SaveChangesAsync();

            if (_elasticClient != null)
            {
                var esAppeal = new ESAppeal(dest);
                await _elasticClient?.UpdateAsync<ESAppeal>(esAppeal, u => u.Doc(esAppeal));
            }

            await SendEmailMessagesOnUpdate(dest, old, updateKeys);
            await SendPushMessagesOnUpdate(dest, old, updateKeys);

            return await GetAppealAsync(id, requestedFields);
        }

        private async Task UpdateAttachedFilesIds(Appeal src, Appeal dest)
        {
            if (src.AttachedFilesIds != null)
            {
                dest.AttachedFilesToAppeal ??= new List<AttachedFilesToAppeals>();

                var files = (from s in src.AttachedFilesIds
                    join d in dest.AttachedFilesToAppeal on s equals d.AttachedFileId
                        into dd
                    from ddd in dd.DefaultIfEmpty()
                    select new {src = s, dest = ddd})
                    .ToArray();

                dest.AttachedFilesToAppeal.Clear();

                dest.AttachedFilesToAppeal.AddRange(files.Where(el => el.dest != null).Select(el => el.dest));

                var addIds
                    = files.Where(x => x.dest == null).Select(x => x.src).ToArray();

                dest.AttachedFilesToAppeal.AddRange((await _ctx.AttachedFiles
                        .Where(el => addIds.Contains(el.Id))
                        .ToArrayAsync())
                    .Select(el => new AttachedFilesToAppeals {AttachedFile = el}));
            }
        }

        public async Task<Appeal> RegisterAppealAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            var userId = _um.ClaimsPrincipalId();
            if (!userId.HasValue)
            {
                throw new Exception("Вы не авторизованы.");
            }

            var claimPrincipal = _um.CurrentClaimsPrincipal();

            var user = await _um.CurrentUser();

            var dest = await _ctx.Appeals
                .Include(el => el.Subsystem)
                .FilterByReadAllowedAsync(_um, user, _sm, false)
                .Where(el => el.Id == id)
                .Distinct()
                .SingleOrDefaultAsync();

            if (dest == null)
            {
                throw new Exception($"Обращение с Id = {id} не найдено или вы не имеете прав на его просмотр.");
            }

            if (dest.Subsystem.UID != SubsystemCitizenAppeals.BaseSystemKey || dest.AppealType == AppealTypeEnum.FEEDBACK)
            {
                throw new Exception("Для данного обращения операция не предусмотрна.");
            }

            var registerParamValue = _sm.SettingValue<int>($"subsystem.{SubsystemCitizenAppeals.SettingsKey.CitizenSettingsRegisterWait}");

            if (registerParamValue == 0)
            {
                throw new Exception("Не установлен режим ручной регистрации обращений.");
            }

            if (!claimPrincipal.HasClaim(ClaimName.Subsystem, $"{dest.Subsystem.UID}.{SSClaimSuffix.Ss.available}")
            || !claimPrincipal.HasClaim(ClaimName.Moderate, $"{dest.Subsystem.UID}.{SSClaimSuffix.Md.enable}"))
            {
                throw new Exception("Для Вас эта операция запрещена. Вы не являетесь модератором подсистемы.");
            }

            if (!string.IsNullOrWhiteSpace(dest.ExtNumber))
            {
                throw new Exception($"Обращение уже зарегистрировано. Внешний номер {dest.ExtNumber}.");
            }

            if (dest.DeloStatus == "2register")
            {
                throw new Exception($"Обращение уже имеет статус <{dest.DeloStatus}>.");
            }

            dest.DeloStatus = "2register";

            await _ctx.SaveChangesAsync();

            return await GetAppealAsync(id, requestedFields);
        }

        private async Task<Appeal> GetAppealAsync(long id, GraphQlKeyDictionary requestedFields)
        {
            return await _ctx.Appeals
                .Where(el => el.Id == id)
                .IncludeFilterByFields(requestedFields)
                .IncludeFilterByNotMappedFields(requestedFields)
                .FirstOrDefaultAsync();
        }

        public async Task<Appeal> ApproveAsync(long id, AppealApprove src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var userId = _um.ClaimsPrincipalId();
            if (userId == null)
            {
                throw new Exception($"Нужно авторизоваться");
            }
            var dest = await _ctx.Appeals
                .Where(el => el.Id == id)
                .Where(el => !el.Deleted)
                .FirstOrDefaultAsync();
            if (dest == null)
            {
                throw new Exception($"AppealId {id} not found");
            }
            if (!(await dest.GetAllowedActions(_um.ClaimsPrincipalId(), _um.CurrentClaimsPrincipal()?.Claims, _sm, _ctx)).HasFlag(BaseConstants.AllowedActionFlags.Moderate))
            {
                throw new Exception($"Вам это нельзя");
            }
            Appeal old = (Appeal)dest.Clone();
            if (src.ApprovedEnum.HasValue && dest.Approved != src.ApprovedEnum.Value)
            {
                dest.Approved = src.ApprovedEnum.Value;
            }
            if (src.ModerationStage.HasValue && dest.ModerationStage != src.ModerationStage.Value)
            {
                dest.ModerationStage = src.ModerationStage.Value;
            }

            if (src.Public_.HasValue && dest.Public_ != src.Public_.Value)
            {
                if (src.Public_.Value)
                {
                    if (dest.AppealType == AppealTypeEnum.FEEDBACK)
                    {
                        throw new Exception($"Сообщение обратной связи не может быть публичным");
                    }
                    if (dest.PublicGranted)
                    {
                        dest.Public_ = src.Public_.Value;
                        dest.NeedToUpdateInExt = true;
                    }
                }
                else
                {
                    dest.Public_ = src.Public_.Value;
                    dest.NeedToUpdateInExt = true;
                }
            }
            if (src.RejectionReason != null)
            {
                dest.RejectionReason = src.RejectionReason;
            }
            var diff = old.MemberwiseDiff(dest);
            if (!string.IsNullOrEmpty(diff))
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = await _um.CurrentUser(),
                    Appeal = dest,
                    Subsystem = dest.Subsystem,
                    Action = $"Appeal moderated {dest.Id}",
                    Data = diff
                });
            }
            await _ctx.SaveChangesAsync();

            if (old.Approved != dest.Approved && dest.Approved == ApprovedEnum.REJECTED)
            {
                await SendEmailMessagesOnApproved(dest);
            }

            if (old.Approved != dest.Approved)
            {
                _ =SendPushMessagesOnApproved(dest);
            }

            return await GetAppealAsync(id, requestedFields);
        }

        public async Task<LikeReport> LikeAsync(LikeRequest like)
        {
            var user = await _um.CurrentUser();
            if (user == null && like.Liked != LikeRequestEnum.NONE)
            {
                throw new Exception($"Аноним не может лайкать");
            }
            var obj = await _ctx.Appeals
                .Where(el => !el.Deleted)
                .Where(el => el.Public_)
                .Where(el => el.Id == like.Id)
                .Include(el => el.UserLikeAppeal)
                .FirstOrDefaultAsync();
            if (obj == null)
            {
                throw new Exception($"{like.Type}:{like.Id} not found");
            }

            if (user != null && like.Liked != LikeRequestEnum.NONE && obj.AuthorId == user.Id)
            {
                throw new Exception($"Нельзя лайкать свои обращения");
            }
            var likesCount = obj.LikesCount;
            var dislikesCount = obj.DislikesCount;
            var liked = obj.LikeSet(like.Liked, user);
            if (likesCount != obj.LikesCount || dislikesCount != obj.DislikesCount)
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = user,
                    Appeal = obj,
                    Subsystem = obj.Subsystem,
                    Action = $"Appeal {obj.Id} Like status changed"
                });
                await _ctx.SaveChangesAsync();
            }
            return new LikeReport { Liked = liked, LikeCount = obj.LikesCount, DislikeCount = obj.DislikesCount };
        }

        public async Task<ViewReport> ViewAsync(ViewRequest view)
        {
            var user = await _um.CurrentUser();
            if (user == null)
            {
                throw new Exception($"Аноним не может отмечать просмотренность");
            }
            var obj = await _ctx.Appeals
                .Where(el => el.Id == view.Id)
                .FilterByReadAllowedAsync(_um, user, _sm, false)
                .Include(el => el.UserViewAppeal)
                .FirstOrDefaultAsync();
            if (obj == null)
            {
                throw new Exception($"{view.Type}:{view.Id} not found");
            }

            var viewCount = obj.ViewsCount;
            var viewed = obj.ViewSet(view.Viewed, user);
            if (viewCount != obj.ViewsCount)
            {
                _ctx.ProtocolRecords.Add(new ProtocolRecord
                {
                    User = user,
                    Appeal = obj,
                    Subsystem = obj.Subsystem,
                    Action = $"Appeal {obj.Id} View status changed"
                });
                await _ctx.SaveChangesAsync();
            }
            return new ViewReport { Viewed = viewed.Viewed, ViewCount = obj.ViewsCount, CreatedAt = viewed.CreatedAt, UpdatedAt = viewed.UpdatedAt };
        }

        public IQueryable<User> GetAllModeratorsQuery(Appeal appeal)
        {
            var subsystemUid = appeal.SubsystemId
                                     ?? appeal.Subsystem?.UID;

            var ssAvailable = $"{subsystemUid}.{SSClaimSuffix.Ss.available}";

            var mdEnable = $"{subsystemUid}.{SSClaimSuffix.Md.enable}";

            var result = _ctx.Users
                .Where(el =>
                    (el.IdentityClaims.Any(x =>
                         x.ClaimType == ClaimName.Subsystem && x.ClaimValue == ssAvailable)
                     || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y =>
                         y.ClaimType == ClaimName.Subsystem && y.ClaimValue == ssAvailable))
                    )
                    && (el.IdentityClaims.Any(x =>
                            x.ClaimType == ClaimName.Moderate && x.ClaimValue == mdEnable)
                        || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y =>
                            y.ClaimType == ClaimName.Moderate && y.ClaimValue == mdEnable)))
                );

            if (appeal.Region != null)
            {
                var mdAllRegions = $"{appeal.SubsystemId}.{SSClaimSuffix.Md.all_regions}";
                result = result.Where(
                    el => el.UserRegion.Any(x => x.Subsystem.UID == subsystemUid && x.Region.Due.StartsWith(appeal.Region.Due))
                          || el.IdentityClaims.Any(x =>
                              x.ClaimType == ClaimName.Moderate && x.ClaimValue == mdAllRegions)
                          || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y =>
                              y.ClaimType == ClaimName.Moderate && y.ClaimValue == mdAllRegions))
                );
            }

            if (appeal.Rubric != null)
            {
                var mdAllRubric = $"{appeal.SubsystemId}.{SSClaimSuffix.Md.all_rubrics}";

                result = result.Where(
                    el => el.UserRubric.Any(x => x.Subsystem.UID == subsystemUid && x.Rubric.Due.StartsWith(appeal.Rubric.Due))
                          || el.IdentityClaims.Any(x =>
                              x.ClaimType == ClaimName.Moderate && x.ClaimValue == mdAllRubric)
                          || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y =>
                              y.ClaimType == ClaimName.Moderate && y.ClaimValue == mdAllRubric))
                );
            }

            if (appeal.Territory != null)
            {
                var mdAllTerritories = $"{appeal.SubsystemId}.{SSClaimSuffix.Md.all_territories}";
                result = result.Where(
                    el => el.UserTerritory.Any(x => x.Subsystem.UID == subsystemUid && x.Territory.Due.StartsWith(appeal.Territory.Due))
                          || el.IdentityClaims.Any(x =>
                              x.ClaimType == ClaimName.Moderate && x.ClaimValue == mdAllTerritories)
                          || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y =>
                              y.ClaimType == ClaimName.Moderate && y.ClaimValue == mdAllTerritories))
                );
            }

            if (appeal.Topic != null)
            {
                var mdAllTopics = $"{appeal.SubsystemId}.{SSClaimSuffix.Md.all_topics}";
                result = result.Where(
                    el => el.UserModeratorTopic.Any(x => x.Subsystem.UID == subsystemUid && x.Topic.Due.StartsWith(appeal.Topic.Due))
                          || el.IdentityClaims.Any(x =>
                              x.ClaimType == ClaimName.Moderate && x.ClaimValue == mdAllTopics)
                          || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y =>
                              y.ClaimType == ClaimName.Moderate && y.ClaimValue == mdAllTopics))
                );
            }

            return result;
        }

        public IQueryable<User> GetAllFeedbackAdministratorQuery(Appeal appeal)
        {
            var sbsystemUid = appeal.SubsystemId
                                     ?? appeal.Subsystem?.UID;

            var ssAvailable = $"{sbsystemUid}.{SSClaimSuffix.Ss.available}";

            var feedbackEnable = $"{sbsystemUid}.{SSClaimSuffix.Ss.feedback_admin}";

            var result = _ctx.Users
                .Where(el =>
                    (el.IdentityClaims.Any(x =>
                         x.ClaimType == ClaimName.Subsystem && x.ClaimValue == ssAvailable)
                     || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y =>
                         y.ClaimType == ClaimName.Subsystem && y.ClaimValue == ssAvailable))
                    )
                    && (el.IdentityClaims.Any(x =>
                            x.ClaimType == ClaimName.Subsystem && x.ClaimValue == feedbackEnable)
                        || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y =>
                            y.ClaimType == ClaimName.Subsystem && y.ClaimValue == feedbackEnable)))
                );

            return result;
        }

        private async Task SendEmailMessagesOnApproved(Appeal appeal)
        {
            if (!appeal.Public_)
            {
                return;
            }

            var emailService = _serviceProvider.GetRequiredService<MailService>();

            if (appeal.Approved == ApprovedEnum.REJECTED)
            {
                if (appeal.Author == null)
                {
                    appeal.Author = await _ctx.Users
                        .ById(appeal.AuthorId)
                        .SingleOrDefaultAsync();
                }

                var email = string.IsNullOrWhiteSpace(appeal.Email)
                    ? appeal.Author.Email
                    : appeal.Email;

                if (!string.IsNullOrWhiteSpace(email))
                {
                    var message = new MailMessage
                    {
                        Email = email,
                        //Subject = $"Отказ в публикации сообщения (№ {appeal.Id})",
                        TemplateKey = "appealRejectedCitizen",
                        AddingEntity = new List<EmailEntityMetadata>
                        {
                            EmailEntityMetadata.Generate(_ctx, appeal)
                        }
                    };

                    await emailService.SendEmailAsync(message);
                }
            }
        }

        private async Task SendEmailMessagesOnCreate(Appeal appeal)
        {
            try
            {
                await SendAppealStatusEmail(appeal);
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка отправки сообщений о новом обращении заявителю", ex);
            }

            try
            {
                if (!appeal.Public_ && appeal.AppealType != AppealTypeEnum.FEEDBACK)
                {
                    return;
                }

                var moderatorsQuery = appeal.AppealType == AppealTypeEnum.FEEDBACK
                    ? GetAllFeedbackAdministratorQuery(appeal)
                    : GetAllModeratorsQuery(appeal);

                var toSend = await moderatorsQuery
                    .Where(el => !el.Disconnected)
                    .Where(el => el.Email != null)
                    .ToArrayAsync();

                if (!toSend.Any())
                {
                    return;
                }

                foreach (var userToSend in toSend)
                {
                    if (string.IsNullOrWhiteSpace(userToSend.Email))
                    {
                        continue;
                    }

                    var mailMessage = new MailMessage
                    {
                        Email = userToSend.Email,
                        TemplateKey = appeal.AppealType == AppealTypeEnum.FEEDBACK ? "appealFeedbackRegisteredModerator" : "appealRegisteredModerator",
                        AddingEntity = new List<EmailEntityMetadata>
                        {
                            EmailEntityMetadata.Generate(_ctx, appeal),
                            EmailEntityMetadata.Generate(_ctx, userToSend)
                        }
                    };

                    var emailService = _serviceProvider.GetRequiredService<MailService>();

                    await emailService.SendEmailAsync(mailMessage);
                }
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка отправки сообщений о новом обращении модераторам", ex);
            }
        }

        private async Task SendEmailMessagesOnUpdate(Appeal dest, Appeal old, GraphQlKeyDictionary updateKeys)
        {
            if (!updateKeys.ContainsKey(nameof(Appeal.SiteStatus).ToJsonFormat())
                && !updateKeys.ContainsKey(nameof(Appeal.Approved).ToJsonFormat()))
            {
                return;
            }

            if (dest.Author == null)
            {
                dest.Author = await _ctx.Users
                    .ById(dest.AuthorId)
                    .SingleOrDefaultAsync();
            }

            if (string.IsNullOrWhiteSpace(dest.Email) && string.IsNullOrWhiteSpace(dest.Author?.Email))
            {
                return;
            }
            
            if (dest.SiteStatus != old.SiteStatus)
            {
                await SendAppealStatusEmail(dest);
            }
        }

        private async Task<bool> SendAppealStatusEmail(Appeal appeal)
        {
            var enumVerbManager = _serviceProvider.GetRequiredService<EnumVerbManager>();

            var status = await enumVerbManager.GetEnumVerbAsync(
                appeal.SiteStatus,
                appeal.AppealType == AppealTypeEnum.NORMAL ? appeal.Subsystem?.UID : null,
                appeal.AppealType == AppealTypeEnum.NORMAL ? "appealStatus" : "feedbackStatus"
            );

            if (status == null)
            {
                return false;
            }

            if (!status.CustomSettings.TryDeserializeJson<AppealStatusSettings>(out var statusSettings))
            {
                return false;
            }

            var statusOptions = statusSettings.StatusOptions;

            if (!(statusOptions?.Email is { Send: true }))
            {
                return false;
            }

            var hasTemplate = _ctx.EmailTemplates
                .Any(el => el.Key == statusOptions.Email.Template);

            if (!hasTemplate)
            {
                _log.ErrorFormat("Ошибка отправки Email. Не найден шаблон письма '{0}'", statusOptions.Email.Template);

                return false;
            }

            if (string.IsNullOrWhiteSpace(appeal.Email) && string.IsNullOrWhiteSpace(appeal.Author?.Email))
            {
                return false;
            }

            var email = string.IsNullOrWhiteSpace(appeal.Email)
                ? appeal.Author.Email
                : appeal.Email;

            var mailMessage = new MailMessage
            {
                Email = email,
                TemplateKey = statusOptions.Email.Template,
                AddingEntity = new List<EmailEntityMetadata>
                {
                    EmailEntityMetadata.Generate(_ctx, appeal)
                },
                TemplateModel = new
                {
                    appeal.SiteStatus,
                    SiteStatusName = status.Name
                },
                WithDeliveryNotice = statusOptions.Email.WithDeliveryNotice,
                DuplicateToSender = statusOptions.Email.DuplicateToSender
            };

            if (statusOptions.Email.SendAdditional != null)
            {
                if (Enum.TryParse<MessageKindEnum>(statusOptions.Email.SendAdditional.MessageKind,
                    out var kindMessage))
                {
                    var message = await _ctx.Messages
                        .Include(el => el.AttachedFilesToMessages)
                        .Where(el =>
                            el.AppealId == appeal.Id &&
                            el.Kind == kindMessage)
                        .OrderByDescending(el => el.CreatedAt)
                        .FirstOrDefaultAsync();

                    if (message != null)
                    {
                        if (statusOptions.Email.SendAdditional.SendFiles && message.AttachedFilesToMessages.Any())
                        {
                            mailMessage.FilesIds = message.AttachedFilesToMessages
                                .Select(el => el.AttachedFileId)
                                .ToArray();
                        }

                        if (statusOptions.Email.SendAdditional.SendMessage)
                        {
                            mailMessage.AddingEntity.Add(EmailEntityMetadata.Generate(_ctx, message));
                        }
                    }
                }
            }

            var emailService = _serviceProvider.GetRequiredService<MailService>();

            var messageId = await emailService.SendEmailAsync(mailMessage);

            return messageId != 0;
        }

        private async Task SendPushMessagesOnApproved(Appeal appeal)
        {
            if (!appeal.Public_)
            {
                return;
            }

            var pushService = _serviceProvider.GetRequiredService<PushService>();

            if (appeal.Author == null)
            {
                appeal.Author = await _ctx.Users
                    .ById(appeal.AuthorId)
                    .SingleOrDefaultAsync();
            }

            if (appeal.Author == null) return;
            await pushService.SendAsync(new PushMessage
            {
                UserId = appeal.Author.Id,
                Title = appeal.Approved == ApprovedEnum.REJECTED ? "Публикация отклонена" : "Публикация подтверждена",
                Body = appeal.Approved == ApprovedEnum.REJECTED ? "Публикация отклонена" : "Публикация подтверждена",
                AppealId = appeal.Id

            });
        }

        private async Task SendPushMessagesOnCreate(Appeal appeal)
        {
            try
            {
                await SendAppealStatusPush(appeal);
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка отправки сообщений о новом обращении заявителю", ex);
            }

        }

        private async Task SendPushMessagesOnUpdate(Appeal dest, Appeal old, GraphQlKeyDictionary updateKeys)
        {
            if (!updateKeys.ContainsKey(nameof(Appeal.SiteStatus).ToJsonFormat())
                && !updateKeys.ContainsKey(nameof(Appeal.Approved).ToJsonFormat()))
            {
                return;
            }

            if (dest.Author == null)
            {
                dest.Author = await _ctx.Users
                    .ById(dest.AuthorId)
                    .SingleOrDefaultAsync();
            }

            if (dest.SiteStatus != old.SiteStatus)
            {
                await SendAppealStatusPush(dest);

            }
            if (dest.Approved != old.Approved)
            {
                await SendPushMessagesOnApproved(dest);

            }

        }

        private async Task SendAppealStatusPush(Appeal appeal)
        {
            var enumVerbManager = _serviceProvider.GetRequiredService<EnumVerbManager>();

            var status = await enumVerbManager.GetEnumVerbAsync(
                appeal.SiteStatus,
                appeal.AppealType == AppealTypeEnum.NORMAL ? appeal.Subsystem?.UID : null,
                appeal.AppealType == AppealTypeEnum.NORMAL ? "appealStatus" : "feedbackStatus"
            );

            if (status == null)
            {
                return;
            }

            if (!status.CustomSettings.TryDeserializeJson<AppealStatusSettings>(out var statusSettings))
            {
                return;
            }

            var statusOptions = statusSettings.StatusOptions;

            if (statusOptions?.Email == null || !statusOptions.Email.Send) //Вероятно, следует выделить в отдельные опции отправки. Пока достаточно отправки там, где отправляется Email
            {
                return;
            }

            if (appeal.Author == null) return;


            var pushMessage = new PushMessage
            {
                UserId = appeal.Author.Id,
                AppealId = appeal.Id,
                Title = $"Установлен новый статус {status.Name}",
                Body = $"Установлен новый статус {status.Name} для обращения {appeal.Id} от {appeal.CreatedAt.ToString("dd.MM.yyyy")}", // Имеет смысл добавить шаблоны пушей
                Status = PushMessageStatus.Created
            };

            var pushService = _serviceProvider.GetRequiredService<PushService>();

            await pushService.SendAsync(pushMessage);
        }

        public async Task<Pagination<AppealStatus>> GetAppealsStatusAsync(string number, DateTime date, SelectionRange range, GraphQlKeyDictionary requestedFields)
        {
            var result = new Pagination<AppealStatus>();

            if (string.IsNullOrWhiteSpace(number))
            {
                return result;
            }

            var request = _ctx.Appeals
                .Where(el => el.AppealType == AppealTypeEnum.NORMAL
                             && el.ExtNumber == number
                             && el.ExtDate == date
                             && !el.Deleted);

            if (requestedFields.ContainsKey(nameof(Pagination<Appeal>.Total).ToJsonFormat()))
            {
                result.Total = await request
                    .CountAsync();
            }

            if (!requestedFields.ContainsKey(nameof(Pagination<Appeal>.Items).ToJsonFormat()) ||
                !requestedFields.TryGetValue(nameof(Pagination<Appeal>.Items).ToJsonFormat(), out var items) ||
                items == null)
            {
                return result;
            }

            var request1 = request
                .IncludeByFields(items)
                .OrderByFieldNames(nameof(Appeal.Weight), range?.OrderBy)
                .GetRange(range?.Take, range?.Skip)
                .Select(el => new AppealStatus(el));

            result.Items = await request1
                .ToArrayAsync();

            return result;
        }

        public async Task<bool> ResendAppealsStatusEmailAsync(long appealId)
        {
            var appeal = await _ctx.Appeals
                .Where(el => el.Id == appealId)
                .Include(el=>el.Subsystem)
                .Include(el => el.Author)
                .Include(el => el.Rubric)
                .Include(el => el.Region)
                .Include(el => el.Topic)
                .Include(el => el.Territory)
                .Include(el=> el.AttachedFilesToAppeal)
                .SingleOrDefaultAsync();

            if (appeal == null || (string.IsNullOrWhiteSpace(appeal.Email) && string.IsNullOrWhiteSpace(appeal.Author?.Email)))
            {
                return false;
            }

            return await SendAppealStatusEmail(appeal);
        }
    }
}
