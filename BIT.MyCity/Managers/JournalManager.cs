using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Pagination;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Managers
{
    public class JournalManager
    {
        private readonly IServiceProvider _serviceProvider;

        private readonly DocumentsContext _ctx;

        public JournalManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = _serviceProvider.GetRequiredService<DocumentsContext>();
        }

        public async Task<Pagination<ChangeJournalItem>> GetAsync(SelectionRange range, List<GraphQlQueryFilter> filter,
            GraphQlKeyDictionary requestedKeys)
        {


            var result = new Pagination<ChangeJournalItem>();

            var request = _ctx.Journal
                .FiltersBy(filter);

            if (requestedKeys.ContainsKey(nameof(Pagination<Message>.Total).ToJsonFormat()))
            {
                result.Total = await request.LongCountAsync();
            }

            if (requestedKeys.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                request = request
                    .IncludeByFields(requestedKeys[nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()])
                    .OrderByFieldNames(nameof(ChangeJournalItem.Timestamp), range?.OrderBy)
                    .GetRange(range?.Take, range?.Skip);
                result.Items = await request.ToListAsync();
            }

            return result;
        }

        public JournalEntityTags[] GetTags()
        {
            return _ctx.JournalTags
                .ToArray();
        }

        public JournalOperation[] GetOperations()
        {
            return ((EntityOperation[]) Enum.GetValues(typeof(EntityOperation)))
                .Where(el=>el!=EntityOperation.UNDEFINED)
                .Select(el => new JournalOperation(el))
                .OrderBy(el=>(int)el.Operation)
                .ToArray();           
        }
    }
}
