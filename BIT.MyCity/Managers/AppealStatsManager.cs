using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL.Utilities;
using log4net;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BIT.MyCity.Managers
{
    public class AppealStatsManager
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(AppealManager));

        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        private readonly SettingsManager _sm;

        public AppealStatsManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
            _sm = _serviceProvider.GetRequiredService<SettingsManager>();
        }

        public async Task<Pagination<AppealStats>> GetAppealStatsAsync(QueryOptions options)
        {
            /*Зачем я сюда полез... LINQ такой запрос не переваривает, в БД какой-то отношение отсутствует,
             
             Ага... Заработало!
             Только .OrderByFieldNames надо нписать для IEnumerable. Но потом

            Однако не работает такой сложный квери в EF. Пришлось резать через промежуточный массив :(

             */
            options.RequestedFields.TryGetValue(nameof(Pagination<AppealStats>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.AppealStats
                    .IncludeByFields(items)
                    .IncludeByNotMappedFields(items)
                    .FiltersBy(options.Filters, options.WithDeleted)
                    .Select(st =>
                    new
                    {
                        Approved = items.ContainsKey(nameof(AppealStats.Approved).ToJsonFormat()) ? st.Approved : default,
                        Deleted = items.ContainsKey(nameof(AppealStats.Deleted).ToJsonFormat()) ? st.Deleted : default,
                        Public = items.ContainsKey(nameof(AppealStats.Public).ToJsonFormat()) && st.Public,
                        Region = items.ContainsKey(nameof(AppealStats.Region).ToJsonFormat()) ? st.Region : default,
                        Topic = items.ContainsKey(nameof(AppealStats.Topic).ToJsonFormat()) ? st.Topic : default,
                        Territory = items.ContainsKey(nameof(AppealStats.Territory).ToJsonFormat()) ? st.Territory : default,
                        Rubric = items.ContainsKey(nameof(AppealStats.Rubric).ToJsonFormat()) ? st.Rubric : default,
                        Subsystem = items.ContainsKey(nameof(AppealStats.Subsystem).ToJsonFormat()) ? st.Subsystem : default,
                        CreatedAt = items.ContainsKey(nameof(AppealStats.CreatedAt).ToJsonFormat()) ? st.CreatedAt : default,
                        SiteStatus = items.ContainsKey(nameof(AppealStats.SiteStatus).ToJsonFormat()) ? st.SiteStatus : default,
                        SentToDelo = items.ContainsKey(nameof(AppealStats.SentToDelo).ToJsonFormat()) && st.SentToDelo,
                        AppealsCount = st.AppealsCount
                    })
                    .AsEnumerable()
                    .GroupBy(
                        st => new
                        {
                            st.Approved,
                            st.Deleted,
                            st.Public,
                            st.Region,
                            st.Topic,
                            st.Territory,
                            st.Rubric,
                            st.Subsystem,
                            st.CreatedAt,
                            st.SiteStatus,
                            st.SentToDelo
                        },
                        st => st.AppealsCount
                    )
                    .Select(res => new AppealStats
                    {
                        Approved = res.Key.Approved,
                        Deleted = res.Key.Deleted,
                        Public = res.Key.Public,
                        Region = res.Key.Region,
                        Topic = res.Key.Topic,
                        Territory = res.Key.Territory,
                        Rubric = res.Key.Rubric,
                        Subsystem = res.Key.Subsystem,
                        CreatedAt = res.Key.CreatedAt,
                        SiteStatus = res.Key.SiteStatus,
                        SentToDelo = res.Key.SentToDelo,
                        AppealsCount = res.Sum()
                    })
                    ;

            if (options.Range?.OrderBy?.FirstOrDefault() == null)
            {
                obj = obj.OrderBy(el => el.Rubric?.Name);
            }
            else
            {
                obj = obj.OrderByFieldNames(nameof(AppealStats.Rubric.Name), options.Range?.OrderBy);
            }
            return await BaseConstants.MakePaginationResult(obj, options);
        }
    }
}
