using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using BIT.MyCity.Utilites;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL.Utilities;
using log4net;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.WebUtilities;
namespace BIT.MyCity.Managers
{
    public class UserManager
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(UserManager));

        private readonly IServiceProvider _serviceProvider;

        private static readonly Lazy<IEnumerable<string>> CitizenKeysForSelf = new Lazy<IEnumerable<string>>(() =>
        {
            return new string[]
            {
                nameof(User.UserName).ToJsonFormat(),
                nameof(User.FullName).ToJsonFormat(),
                nameof(User.Email).ToJsonFormat(),
                nameof(User.Gender).ToJsonFormat(),
                nameof(User.Addresses).ToJsonFormat(),
                nameof(User.Phones).ToJsonFormat()
            };
        });

        private static readonly Lazy<IEnumerable<string>> ManagerKeysForSelf = new Lazy<IEnumerable<string>>(() =>
        {
            return new string[]
            {
                nameof(User.FullName).ToJsonFormat(),
                nameof(User.Phones).ToJsonFormat(),
                nameof(User.Email).ToJsonFormat(),
                nameof(User.Gender).ToJsonFormat()
            };
        });

        private static readonly Lazy<IEnumerable<string>> UserKeysForManager = new Lazy<IEnumerable<string>>(() =>
        {
            return CitizenKeysForSelf.Value
                .Union(
                    new[]
                    {
                        nameof(User.Disconnected).ToJsonFormat(),
                        nameof(User.Claims).ToJsonFormat(),
                        nameof(User.RoleIds).ToJsonFormat(),
                        //nameof(User.Roles).ToJsonFormat(),
                        nameof(User.EmailConfirmed).ToJsonFormat(),
                        nameof(User.RubricIds).ToJsonFormat(),
                        nameof(User.TopicIds).ToJsonFormat(),
                        nameof(User.RegionIds).ToJsonFormat(),
                        nameof(User.TerritoryIds).ToJsonFormat(),
                        nameof(User.ExternalId).ToJsonFormat()
                    });
        });

        private static readonly Lazy<IEnumerable<string>> CitizensKeysForManager = new Lazy<IEnumerable<string>>(() =>
        {
            return new[]
            {
                nameof(User.Disconnected).ToJsonFormat()
            };
        });

        public UserManager(IServiceProvider servicesProvider)
        {
            _serviceProvider = servicesProvider;
        }

        public async Task<Pagination<User>> GetUsersAsync(SelectionRange range, List<GraphQlQueryFilter> filter,
            GraphQlKeyDictionary requestedKeys)
        {
            var result = new Pagination<User>();

            GraphQlKeyDictionary userKeys = null;

            if (requestedKeys.ContainsKey(nameof(Pagination<Role>.Items).ToJsonFormat()))
            {
                userKeys = requestedKeys[nameof(Pagination<Role>.Items).ToJsonFormat()];
            }

            var query = GetUsersQuery(filter, userKeys);

            if (requestedKeys.ContainsKey(nameof(Pagination<User>.Total).ToJsonFormat()))
            {
                result.Total = await query.LongCountAsync();
            }

            if (userKeys == null)
            {
                return result;
            }

            query = query
                .OrderByFieldNames(nameof(User.Id), range?.OrderBy)
                .GetRange(range?.Take, range?.Skip);

            result.Items = await query.ToArrayAsync();

            return result;
        }

        private void MappingFilter(List<GraphQlQueryFilter> filters1)
        {
            if (filters1 == null)
            {
                return;
            }

            var insertingDictionary = new Dictionary<GraphQlQueryFilter, GraphQlQueryFilter>();

            var filters = filters1.Where(el => !string.IsNullOrWhiteSpace(el.Name))
                .ToList();

            // maping allClaims.type
            var findStr = $"{nameof(User.AllClaims).ToJsonFormat()}.{nameof(Claim.Type).ToJsonFormat()}";

            var mapFilters = filters
                .Where(el => el.Name.StartsWith(findStr));

            foreach (var filter in mapFilters)
            {
                filter.Name = filter.Name.Replace(
                    findStr,
                    $"{nameof(User.IdentityClaims).ToJsonFormat()}.{nameof(IdentityUserClaim<long>.ClaimType).ToJsonFormat()}");

                var newFilter = (GraphQlQueryFilter) filter.Clone();

                newFilter.Name =
                    $"{nameof(User.UserRole).ToJsonFormat()}.{nameof(UserRole.Role).ToJsonFormat()}.{nameof(Role.IdentityClaims).ToJsonFormat()}.{nameof(IdentityRoleClaim<long>.ClaimType).ToJsonFormat()}";
                newFilter.Op = FilterNextOpEnum.OR;

                insertingDictionary.Add(filter, newFilter);
            }

            // maping allClaims.value
            findStr = $"{nameof(User.AllClaims).ToJsonFormat()}.{nameof(Claim.Value).ToJsonFormat()}";

            mapFilters = filters
                .Where(el => el.Name.StartsWith(findStr));

            foreach (var filter in mapFilters)
            {
                filter.Name = filter.Name.Replace(
                    findStr,
                    $"{nameof(User.IdentityClaims).ToJsonFormat()}.{nameof(IdentityUserClaim<long>.ClaimValue).ToJsonFormat()}");

                var newFilter = (GraphQlQueryFilter) filter.Clone();

                newFilter.Name =
                    $"{nameof(User.UserRole).ToJsonFormat()}.{nameof(UserRole.Role).ToJsonFormat()}.{nameof(Role.IdentityClaims).ToJsonFormat()}.{nameof(IdentityRoleClaim<long>.ClaimValue).ToJsonFormat()}";
                newFilter.Op = FilterNextOpEnum.OR;

                insertingDictionary.Add(filter, newFilter);
            }

            // maping claims.type
            findStr = $"{nameof(User.Claims).ToJsonFormat()}.{nameof(Claim.Type).ToJsonFormat()}";

            mapFilters = filters
                .Where(el => el.Name.StartsWith(findStr));

            foreach (var filter in mapFilters)
            {
                filter.Name = filter.Name.Replace(
                    findStr,
                    $"{nameof(User.IdentityClaims).ToJsonFormat()}.{nameof(IdentityUserClaim<long>.ClaimType).ToJsonFormat()}");
            }

            // maping claims.value
            findStr = $"{nameof(User.Claims).ToJsonFormat()}.{nameof(Claim.Value).ToJsonFormat()}";

            mapFilters = filters
                .Where(el => el.Name.StartsWith(findStr));

            foreach (var filter in mapFilters)
            {
                filter.Name = filter.Name.Replace(
                    findStr,
                    $"{nameof(User.IdentityClaims).ToJsonFormat()}.{nameof(IdentityUserClaim<long>.ClaimValue).ToJsonFormat()}");
            }

            // maping roleIds
            findStr = $"{nameof(User.RoleIds).ToJsonFormat()}";

            mapFilters = filters
                .Where(el => el.Name.StartsWith(findStr));

            foreach (var filter in mapFilters)
            {
                filter.Name = filter.Name.Replace(
                    findStr,
                    $"{nameof(User.UserRole).ToJsonFormat()}.{nameof(UserRole.Role).ToJsonFormat()}.{nameof(IdentityUserClaim<long>.Id).ToJsonFormat()}");
            }

            findStr = $"{nameof(User.Claims).ToJsonFormat()}.{nameof(Claim.Type).ToJsonFormat()}";

            mapFilters = filters
                .Where(el => el.Name.StartsWith(findStr));

            foreach (var filter in mapFilters)
            {
                filter.Name = filter.Name.Replace(
                    findStr,
                    $"{nameof(User.IdentityClaims).ToJsonFormat()}.{nameof(IdentityUserClaim<long>.ClaimType).ToJsonFormat()}");
            }

            // maping TopicIds
            findStr = $"{nameof(User.TopicIds).ToJsonFormat()}";

            mapFilters = filters
                .Where(el => el.Name.StartsWith(findStr));

            foreach (var filter in mapFilters)
            {
                var strIds = $"{findStr}.{nameof(UserSubsystemDirectoryId.Ids).ToJsonFormat()}";
                var strSubsystemId = $"{findStr}.{nameof(UserSubsystemDirectoryId.SubsystemId).ToJsonFormat()}";
                var strSubsystemUid = $"{findStr}.{nameof(UserSubsystemDirectoryId.SubsystemUid).ToJsonFormat()}";
                if (filter.Name.StartsWith(strIds))
                {
                    filter.Name = filter.Name.Replace(
                        strIds,
                        $"{nameof(User.UserModeratorTopic).ToJsonFormat()}.{nameof(UserModeratorTopic.TopicId).ToJsonFormat()}");
                }
                else if (filter.Name.StartsWith(strSubsystemId))
                {
                    filter.Name = filter.Name.Replace(
                        strSubsystemId,
                        $"{nameof(User.UserModeratorTopic).ToJsonFormat()}.{nameof(UserModeratorTopic.SubsystemId).ToJsonFormat()}");
                }
                else if (filter.Name.StartsWith(strSubsystemUid))
                {
                    filter.Name = filter.Name.Replace(
                        strSubsystemUid,
                        $"{nameof(User.UserModeratorTopic).ToJsonFormat()}.{nameof(UserModeratorTopic.Subsystem).ToJsonFormat()}.{nameof(Subsystem.UID).ToJsonFormat()}");
                }
            }

            // maping Rubrics
            findStr = $"{nameof(User.RubricIds).ToJsonFormat()}";

            mapFilters = filters
                .Where(el => el.Name.StartsWith(findStr));

            foreach (var filter in mapFilters)
            {
                var strIds = $"{findStr}.{nameof(UserSubsystemDirectoryId.Ids).ToJsonFormat()}";
                var strSubsystemId = $"{findStr}.{nameof(UserSubsystemDirectoryId.SubsystemId).ToJsonFormat()}";
                var strSubsystemUid = $"{findStr}.{nameof(UserSubsystemDirectoryId.SubsystemUid).ToJsonFormat()}";
                if (filter.Name.StartsWith(strIds))
                {
                    filter.Name = filter.Name.Replace(
                        strIds,
                        $"{nameof(User.UserRubric).ToJsonFormat()}.{nameof(UserRubric.RubricId).ToJsonFormat()}");
                }
                else if (filter.Name.StartsWith(strSubsystemId))
                {
                    filter.Name = filter.Name.Replace(
                        strSubsystemId,
                        $"{nameof(User.UserRubric).ToJsonFormat()}.{nameof(UserRubric.SubsystemId).ToJsonFormat()}");
                }
                else if (filter.Name.StartsWith(strSubsystemUid))
                {
                    filter.Name = filter.Name.Replace(
                        strSubsystemUid,
                        $"{nameof(User.UserRubric).ToJsonFormat()}.{nameof(UserRubric.Subsystem).ToJsonFormat()}.{nameof(Subsystem.UID).ToJsonFormat()}");
                }
            }

            // maping Territories
            findStr = $"{nameof(User.TerritoryIds).ToJsonFormat()}";

            mapFilters = filters
                .Where(el => el.Name.StartsWith(findStr));

            foreach (var filter in mapFilters)
            {
                var strIds = $"{findStr}.{nameof(UserSubsystemDirectoryId.Ids).ToJsonFormat()}";
                var strSubsystemId = $"{findStr}.{nameof(UserSubsystemDirectoryId.SubsystemId).ToJsonFormat()}";
                var strSubsystemUid = $"{findStr}.{nameof(UserSubsystemDirectoryId.SubsystemUid).ToJsonFormat()}";
                if (filter.Name.StartsWith(strIds))
                {
                    filter.Name = filter.Name.Replace(
                        strIds,
                        $"{nameof(User.UserTerritory).ToJsonFormat()}.{nameof(UserTerritory.TerritoryId).ToJsonFormat()}");
                }
                else if (filter.Name.StartsWith(strSubsystemId))
                {
                    filter.Name = filter.Name.Replace(
                        strSubsystemId,
                        $"{nameof(User.UserTerritory).ToJsonFormat()}.{nameof(UserTerritory.SubsystemId).ToJsonFormat()}");
                }
                else if (filter.Name.StartsWith(strSubsystemUid))
                {
                    filter.Name = filter.Name.Replace(
                        strSubsystemUid,
                        $"{nameof(User.UserTerritory).ToJsonFormat()}.{nameof(UserTerritory.Subsystem).ToJsonFormat()}.{nameof(Subsystem.UID).ToJsonFormat()}");
                }
            }

            // maping Regions
            findStr = $"{nameof(User.RegionIds).ToJsonFormat()}";

            mapFilters = filters
                .Where(el => el.Name.StartsWith(findStr));

            foreach (var filter in mapFilters)
            {
                var strIds = $"{findStr}.{nameof(UserSubsystemDirectoryId.Ids).ToJsonFormat()}";
                var strSubsystemId = $"{findStr}.{nameof(UserSubsystemDirectoryId.SubsystemId).ToJsonFormat()}";
                var strSubsystemUid = $"{findStr}.{nameof(UserSubsystemDirectoryId.SubsystemUid).ToJsonFormat()}";
                if (filter.Name.StartsWith(strIds))
                {
                    filter.Name = filter.Name.Replace(
                        strIds,
                        $"{nameof(User.UserRegion).ToJsonFormat()}.{nameof(UserRegion.RegionId).ToJsonFormat()}");
                }
                else if (filter.Name.StartsWith(strSubsystemId))
                {
                    filter.Name = filter.Name.Replace(
                        strSubsystemId,
                        $"{nameof(User.UserRegion).ToJsonFormat()}.{nameof(UserRegion.SubsystemId).ToJsonFormat()}");
                }
                else if (filter.Name.StartsWith(strSubsystemUid))
                {
                    filter.Name = filter.Name.Replace(
                        strSubsystemUid,
                        $"{nameof(User.UserRegion).ToJsonFormat()}.{nameof(UserRegion.Subsystem).ToJsonFormat()}.{nameof(Subsystem.UID).ToJsonFormat()}");
                }
            }

            foreach (var item in insertingDictionary)
            {
                filters1.Insert(filters1.IndexOf(item.Key) + 1, item.Value);
            }
        }

        internal IQueryable<User> GetUsersQuery(List<GraphQlQueryFilter> filter,
            GraphQlKeyDictionary requestedFields)
        {
            MappingFilter(filter);

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            var users = context.Users
                .IncludeByKeys(requestedFields)
                .FiltersBy(filter);

            return users;
        }

        public async Task<User> GetUserAsync(long? id, GraphQlKeyDictionary requestedFields = null)
        {
            var httpContextAccessor = (IHttpContextAccessor) _serviceProvider.GetService(typeof(IHttpContextAccessor));

            var principal = httpContextAccessor.HttpContext.User;

            var principalId = ClaimsPrincipalId(principal);

            if (id.HasValue && id.Value != principalId && !principal.HasClaim(ClaimName.Manage, "users"))
            {
                throw new Exception($"У вас нет доступа к пользователю с Id={id}");
            }

            var findId = id ?? principalId;

            if (!findId.HasValue)
            {
                return null;
            }

            return await GetUserByIdAsync(findId.Value, requestedFields);
        }

        internal async Task<User> GetUserByIdAsync(long userId, GraphQlKeyDictionary requestedFields = null)
        {
            var filter = new List<GraphQlQueryFilter>
            {
                new GraphQlQueryFilter
                {
                    Name = nameof(User.Id).ToJsonFormat(),
                    Equal = userId.ToString()
                }
            };

            var users = GetUsersQuery(filter, requestedFields ?? new GraphQlKeyDictionary());

            return await users.SingleOrDefaultAsync();
        }

        internal async Task<User> GetUserAsync(string userName, GraphQlKeyDictionary requestedFields = null)
        {
            if (userName == null)
            {
                return null;
            }

            var filter = new List<GraphQlQueryFilter>
            {
                new GraphQlQueryFilter
                {
                    Name = nameof(User.UserName).ToJsonFormat(),
                    Equal = userName
                }
            };

            var users = GetUsersQuery(filter, requestedFields ?? new GraphQlKeyDictionary());

            return await users.SingleOrDefaultAsync();
        }

        internal async Task<User> GetAnonymousUserAsync()
        {
            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            return await context.Users
                .Where(el => el.UserName == "$anonymousAppealUser$" && !el.Disconnected)
                .FirstOrDefaultAsync();
        }

        public async Task<WebApi.Types.OperationResult<User>> CreateUserAsync(RegisterUserModel registerModel,
            GraphQlKeyDictionary updateKeys,
            GraphQlKeyDictionary requestedKeys)
        {
            var errors = new List<string>(await CheckRegisterModel(registerModel));

            errors.AddRange(await CheckUserModelAsync(registerModel));

            if (errors.Any())
            {
                return new WebApi.Types.OperationResult<User>(null, errors);
            }

            var dbUser = new User();

            await UpdateUserAsync(dbUser, registerModel, updateKeys);

            dbUser.LoginSystem = LoginSystem.Form;

            if (dbUser.IdentityClaims == null)
            {
                dbUser.IdentityClaims = new List<IdentityUserClaim<long>>();
            }

            dbUser.IdentityClaims.Add(new IdentityUserClaim<long>
            {
                ClaimType = ClaimName.LoginSystem,
                ClaimValue = LoginSystem.Form.ToString()
            });

            UpdateUserNormalizeValues(dbUser);

            var context = (DocumentsContext) _serviceProvider.GetService(typeof(DocumentsContext));

            var userManager = (UserManager<User>) _serviceProvider.GetService(typeof(UserManager<User>));

            //var trn = await context.Database.BeginTransactionAsync();

            try
            {
                var result = await userManager.CreateAsync(dbUser, registerModel.Password);

                if (result.Succeeded)
                {
                    //await trn.CommitAsync();

                    if (!string.IsNullOrWhiteSpace(dbUser.Email) &&
                        !dbUser.EmailConfirmed)
                    {
                        var emailId = await SendConfirmEmailMessage(dbUser);

                        if (emailId.HasValue)
                        {
                            dbUser.ConfirmEmailMailId = emailId;

                            await context.SaveChangesAsync();
                        }
                    }
                }
                //else
                //{
                //    await trn.RollbackAsync();
                //}

                return result.Succeeded
                    ? new WebApi.Types.OperationResult<User>(await GetUserAsync(dbUser.Id, requestedKeys))
                    : new WebApi.Types.OperationResult<User>(null,
                        result.Errors.Select(el => el.Description).ToArray());
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка создания пользователя", ex);

                //await trn.RollbackAsync();

                return new WebApi.Types.OperationResult<User>(null, "Ошибка сервера.");
            }
        }

        internal async Task<long?> SendConfirmEmailMessage(User user)
        {
            try
            {
                var userManager = _serviceProvider.GetRequiredService<UserManager<User>>();

                var code = await userManager.GenerateEmailConfirmationTokenAsync(user);

                var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

                var baseUrl = user.SelfFormRegister
                    ? settingsManager.SettingValue(SettingsKeys.SystemAuthFormConfirmEmailUrl)
                    : settingsManager.SettingValue(SettingsKeys.SystemApiBaseUrl);

                var confirmUri = GenerateConfirmEmailUrl(user, baseUrl, code);

                if (string.IsNullOrWhiteSpace(confirmUri))
                {
                    return null;
                }

                //const string subject = "Подтверждение Email";

                var mailService = _serviceProvider.GetRequiredService<MailService>();

                var context = _serviceProvider.GetRequiredService<DocumentsContext>();

                var messageId = await mailService.SendEmailAsync(
                    new MailMessage
                    {
                        Email = user.Email,
                        //Subject = subject,
                        TemplateKey = "confirmEmail",
                        AddingEntity = new List<EmailEntityMetadata>
                        {
                            EmailEntityMetadata.Generate(context, user)
                        },
                            //new AddingEntity {UserId = user.Id},
                        TemplateModel = new
                        {
                            SiteName = settingsManager.SettingValue(SettingsKeys.SystemWebGeneralMainTitle),
                            SiteUrl = (string) null,
                            ConfirmUrl = confirmUri
                        }
                    });

                return messageId;
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка отправки сообщения на подверждение адреса электронной почты (UserId = {user?.Id})",
                    ex);
            }

            return null;
        }

        internal async Task<bool> HasFormCitizenWithEmail([NotNull] string email)
        {
            var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();

            var normalizeEmail = keyNormalizer.NormalizeEmail(email);

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            var query = context.Users
                .Where(el => el.SelfFormRegister && el.NormalizedEmail == normalizeEmail);

            return await query.AnyAsync();
        }

        internal async Task<bool> HasUserWithName([NotNull] string userNme)
        {
            var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();

            var normalizeName = keyNormalizer.NormalizeName(userNme);

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            var query = context.Users
                .Where(el => el.NormalizedUserName == normalizeName);

            return await query.AnyAsync();
        }

        private string GenerateConfirmEmailUrl(User user, string baseUrl, string code)
        {
            if (!Uri.IsWellFormedUriString(baseUrl, UriKind.Absolute))
            {
                _log.Error($"Для подтверждения Email передан не валидный базовый адрес \"{baseUrl}\"");

                return null;
            }

            var uri = new Uri(baseUrl, UriKind.Absolute);

            var query = QueryHelpers.ParseQuery(uri.Query);

            var items = query
                .SelectMany(x => x.Value,
                    (col, value) => new KeyValuePair<string, string>(col.Key, value))
                .ToList();


            var qb = new QueryBuilder(items);

            var baseUri = uri.GetComponents(
                UriComponents.Scheme | UriComponents.Host | UriComponents.Port | UriComponents.Path,
                UriFormat.UriEscaped);

            qb.Add("UserId", user.Id.ToString());
            qb.Add("Code", code);
            qb.Add("Sh", user.Email.GetHashCode().ToString());

            var result = baseUri + qb.ToQueryString();
            return result;
        }

        public async Task<WebApi.Types.OperationResult<User>> UpdateUserAsync(long? id, User src,
            GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestKeys)
        {
            var checkResult = (await CheckUserModelAsync(src, true))
                .ToArray();

            if (checkResult.Any())
            {
                return new WebApi.Types.OperationResult<User>(null, checkResult);
            }

            var keysList = updateKeys;

            var userId = id ?? ClaimsPrincipalId();

            if (!userId.HasValue)
            {
                return new WebApi.Types.OperationResult<User>(null, "Пользователь не найден");
            }

            var context = (DocumentsContext) _serviceProvider.GetService(typeof(DocumentsContext));

            var usersRequest = context.Users
                .ById(userId.Value)
                .IncludeByKeys(updateKeys);

            var dest = await usersRequest
                .SingleOrDefaultAsync();

            if (dest == null)
            {
                return new WebApi.Types.OperationResult<User>(null, "Пользователь не найден");
            }

            var checkHasUpdateResult = await CheckHasUpdateUser(dest, updateKeys);

            if (checkHasUpdateResult != null)
            {
                return new WebApi.Types.OperationResult<User>(null, checkHasUpdateResult);
            }

            if (keysList.ContainsKey(nameof(User.UserName).ToJsonFormat()) &&
                dest.UserName != src.UserName)
            {
                var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();

                var normalizeName = keyNormalizer.NormalizeName(src.UserName);

                var hasUserWithLogin = await context.Users
                    .AnyAsync(el =>
                        el.Id != dest.Id &&
                        el.NormalizedUserName == normalizeName);

                if (hasUserWithLogin)
                {
                    return new WebApi.Types.OperationResult<User>(null,
                        "Пользователь с таким логином уже зарегистрирован");
                }
            }

            var emailChanged = keysList.ContainsKey(nameof(User.Email).ToJsonFormat())
                               && dest.Email != src.Email;

            if (emailChanged && dest.LoginSystem == LoginSystem.Form && dest.SelfFormRegister)
            {
                var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();

                var normalizeEmail = keyNormalizer.NormalizeEmail(src.Email);

                var hasUserWithEmail = await context.Users
                    .AnyAsync(el =>
                        el.Id != dest.Id &&
                        el.LoginSystem == LoginSystem.Form &&
                        el.SelfFormRegister &&
                        el.NormalizedEmail == normalizeEmail);

                if (hasUserWithEmail)
                {
                    return new WebApi.Types.OperationResult<User>(null,
                        "Пользователь с таким Email уже зарегистрирован");
                }

            }

            var emailConfirmedChanged = keysList.ContainsKey(nameof(User.EmailConfirmed).ToJsonFormat())
                                        && dest.EmailConfirmed != src.EmailConfirmed;

            await UpdateUserAsync(dest, src, keysList);

            if (emailChanged || (emailConfirmedChanged && !dest.EmailConfirmed))
            {
                dest.ConfirmEmailMailId = null;
            }

            if (emailChanged)
            {
                if ((!keysList.ContainsKey(nameof(User.EmailConfirmed).ToJsonFormat())
                     || string.IsNullOrWhiteSpace(dest.Email))
                    && dest.EmailConfirmed)
                {
                    dest.EmailConfirmed = false;
                }

                if (!string.IsNullOrWhiteSpace(dest.Email) && !dest.EmailConfirmed)
                {
                    var emailId = await SendConfirmEmailMessage(dest);

                    dest.ConfirmEmailMailId = emailId;
                }
            }
            else
            {
                if (emailConfirmedChanged && !dest.EmailConfirmed)
                {
                    var emailId = await SendConfirmEmailMessage(dest);

                    dest.ConfirmEmailMailId = emailId;
                }
            }

            if (await HasClaimAsync(dest, ClaimName.SuperAdmin))
            {
                if (dest.Disconnected)
                {
                    dest.Disconnected = false;
                }
            }

            await context.SaveChangesAsync();

            var filter = new List<GraphQlQueryFilter>
            {
                new GraphQlQueryFilter
                {
                    Name = nameof(User.Id).ToJsonFormat(),
                    Equal = dest.Id.ToString()
                }
            };

            var usersQuery = GetUsersQuery(filter, requestKeys);

            return new WebApi.Types.OperationResult<User>(await usersQuery.SingleOrDefaultAsync());
        }

        private async Task<string> CheckHasUpdateUser(User user, GraphQlKeyDictionary updateKeys)
        {
            var claimsPrincipalId = ClaimsPrincipalId();

            var principalIsManager = PrincipalHasUserManagmentClaim();

            if (user.Id != claimsPrincipalId)
            {
                if (!principalIsManager)
                {
                    return "Вы не можете редактировать данного пользователя";
                }

                var userIsSuperAdmin = await HasClaimAsync(user, ClaimName.SuperAdmin);

                if (userIsSuperAdmin)
                {
                    return "Вы не можете редактировать данного пользователя";
                }
            }

            IEnumerable<string> allowedKeys;

            if (principalIsManager)
            {
                if (user.LoginSystem == LoginSystem.Form && !user.SelfFormRegister)
                {
                    allowedKeys = UserKeysForManager
                        .Value;
                }
                else
                {
                    allowedKeys = CitizensKeysForManager
                        .Value;
                }
            }
            else
            {
                if (user.SelfFormRegister)
                {
                    allowedKeys = CitizenKeysForSelf
                        .Value;
                }
                else
                {
                    allowedKeys = ManagerKeysForSelf
                        .Value;
                }
            }

            return CheckUpdateFields(updateKeys, allowedKeys);
        }

        private string CheckUpdateFields(GraphQlKeyDictionary updateKeys, IEnumerable<string> allowedKeys)
        {
            var requestKeys = updateKeys.Keys;

            var exceptKeys = requestKeys
                .Except(allowedKeys)
                .ToArray();

            return exceptKeys.Any()
                ? $"Вы не можете редактировать для данного пользователя следующие поля : {string.Join(" ", exceptKeys)}"
                : null;
        }

        internal async Task UpdateUserAsync(User dest, User src, GraphQlKeyDictionary keysList)
        {
            await UpdateUserClaims(dest, src, keysList);

            await UpdateRubricsAsync(dest, src, keysList);

            await UpdateTerritoriesAsync(dest, src, keysList);

            await UpdateRegionsAsync(dest, src, keysList);

            await UpdateTopicsAsync(dest, src, keysList);

            UpdateRoles(dest, src, keysList);

            UpdateAddresses(dest, src, keysList);

            UpdateDocuments(dest, src, keysList);

            await UpdateUserPhonesAsync(dest, src, keysList);

            await UpdateOrganizationsAsync(dest, src, keysList);

            UpdateUtilites.UpdateEntityByFieldsList(dest, src, keysList.Keys);

            UpdateUserNormalizeValues(dest);
        }

        private async Task<IEnumerable<string>> CheckUserModelAsync(User user, bool isUpdate = false)
        {
            var result = new List<string>();

            if (user.Email != null)
            {
                if (!EmailUtility.CheckEmail(user.Email))
                {
                    result.Add($"Не валидный адрес электронной почты : {user.Email}");
                }
            }

            if (user.IdentityClaims != null)
            {
                var claimManager = _serviceProvider.GetRequiredService<ClaimManager>();

                if (isUpdate)
                {
                    claimManager.RemoveUnusedClaims(user);
                }

                var checkClaimsResult = claimManager.CheckClaims(user.IdentityClaims);

                if (!checkClaimsResult)
                {
                    result.Add("Переданы разрешения не применимые к пользователю");
                }
            }


            if (user.UserRole != null)
            {
                var roleManager = _serviceProvider.GetRequiredService<RoleManager>();

                var checkResult = await roleManager.CheckRolesForNewUserAsync(user.UserRole.Select(el => el.RoleId));

                if (!checkResult.Succeeded)
                {
                    result.AddRange(checkResult.Errors.Select(el => el.Description));
                }
            }

            if (user.TerritoryIds != null && user.TerritoryIds.Any())
            {
                var territoryManager = _serviceProvider.GetRequiredService<TerritoryManager>();

                var checkResult = await territoryManager.CheckTerritoriesForNewUserAsync(user.TerritoryIds);

                if (!checkResult.Succeeded)
                {
                    result.AddRange(checkResult.Errors.Select(el => el.Description));
                }
            }

            if (user.RubricIds != null && user.RubricIds.Any())
            {
                var rubricManager = _serviceProvider.GetRequiredService<RubricManager>();

                var checkResult = await rubricManager.CheckRubricForNewUserAsync(user.RubricIds);

                if (!checkResult.Succeeded)
                {
                    result.AddRange(checkResult.Errors.Select(el => el.Description));
                }
            }

            if (user.RegionIds != null && user.RegionIds.Any())
            {
                var regionManager = _serviceProvider.GetRequiredService<RegionManager>();

                var checkResult = await regionManager.CheckRegionsForNewUserAsync(user.RegionIds);

                if (!checkResult.Succeeded)
                {
                    result.AddRange(checkResult.Errors.Select(el => el.Description));
                }
            }

            if (user.TopicIds != null && user.TopicIds.Any())
            {
                var topicManager = _serviceProvider.GetRequiredService<TopicManager>();

                var checkResult = await topicManager.CheckRegionsForNewUserAsync(user.TopicIds);

                if (!checkResult.Succeeded)
                {
                    result.AddRange(checkResult.Errors.Select(el => el.Description));
                }
            }

            return result;
        }

        private async Task<List<string>> CheckRegisterModel(RegisterUserModel registerModel)
        {
            var result = new List<string>();

            var userManager = _serviceProvider.GetRequiredService<UserManager<User>>();

            if (string.IsNullOrWhiteSpace(registerModel.UserName))
            {
                result.Add("Имя пользователя не может быть пустым");
            }
            else
            {
                var user = await userManager.FindByNameAsync(registerModel.UserName);

                if (user != null)
                {
                    result.Add($"Пользователь с иvенем <{registerModel.UserName}> уже существует");
                }
            }

            result.AddRange(await ValidatePassword(registerModel.Password));

            return result;
        }

        internal async Task<IEnumerable<string>> ValidatePassword([NotNull] string password)
        {
            var result = new List<string>();

            if (string.IsNullOrWhiteSpace(password))
            {
                result.Add("Пароль не может быть пустым");

                return result;
            }

            var userManager = _serviceProvider.GetRequiredService<UserManager<User>>();

            var validators = userManager.PasswordValidators;

            foreach (var validator in validators)
            {
                var validationResult = await validator.ValidateAsync(userManager, null, password);

                if (validationResult.Succeeded)
                {
                    continue;
                }

                result.AddRange(validationResult.Errors
                    .Select(error => $"Пароль не прошел валидацию : {error.Description}"));
            }

            return result;
        }

        internal async Task<User> FindSelfFormUserByEmail(string email)
        {
            var filter = new List<GraphQlQueryFilter>
            {
                new GraphQlQueryFilter
                {
                    Name = nameof(User.SelfFormRegister).ToJsonFormat(),
                    Equal = "true"
                },
                new GraphQlQueryFilter
                {
                    Name = nameof(User.Email).ToJsonFormat(),
                    Equal = email
                }
            };

            var userQuery = GetUsersQuery(filter, null);

            return await userQuery.SingleOrDefaultAsync();
        }

        private async Task UpdateUserClaims(User dest, User src, GraphQlKeyDictionary keysList)
        {
            var key = nameof(User.Claims).ToJsonFormat();

            if (!keysList.ContainsKey(key))
            {
                return;
            }

            keysList.Remove(key);

            var claimManager = _serviceProvider.GetRequiredService<ClaimManager>();

            await claimManager.UpdateUserClaims(
                dest,
                src.IdentityClaims?
                    .Select(el => new Claim(el.ClaimType, el.ClaimValue))
                ?? new Claim[0]);
        }

        private async Task UpdateRubricsAsync(User dest, User src, GraphQlKeyDictionary keysList)
        {
            var key = nameof(User.RubricIds).ToJsonFormat();

            if (!keysList.ContainsKey(key))
            {
                return;
            }

            keysList.Remove(key);

            var forDelete = (from destr in dest.UserRubric
                    join srcr in src.UserRubric
                        on new {destr.RubricId, Uid = destr.Subsystem.UID} equals new
                            {srcr.RubricId, Uid = srcr.SubsystemUid}
                        into jns
                    from jn in jns.DefaultIfEmpty()
                    where jn == null
                    select destr)
                .ToArray();

            foreach (var item in forDelete)
            {
                dest.UserRubric.Remove(item);
            }

            var forAdd = (from srcr in src.UserRubric
                    join destr in dest.UserRubric
                        on new {srcr.RubricId, Uid = srcr.SubsystemUid} equals new
                            {destr.RubricId, Uid = destr.Subsystem.UID}
                        into jns
                    from jn in jns.DefaultIfEmpty()
                    where jn == null
                    select srcr)
                .ToArray();

            if (forAdd.Any())
            {
                var context = _serviceProvider.GetRequiredService<DocumentsContext>();

                var subsystems = await context.Subsystems
                    .ToDictionaryAsync(el => el.UID);

                foreach (var topic in forAdd)
                {
                    topic.SubsystemId = subsystems[topic.SubsystemUid].Id;
                }

                dest.UserRubric.AddRange(forAdd);
            }
        }

        private async Task UpdateTerritoriesAsync(User dest, User src, GraphQlKeyDictionary keysList)
        {
            var key = nameof(User.TerritoryIds).ToJsonFormat();

            if (!keysList.ContainsKey(key))
            {
                return;
            }

            keysList.Remove(key);

            var forDelete = (from destr in dest.UserTerritory
                    join srcr in src.UserTerritory
                        on new {destr.TerritoryId, Uid = destr.Subsystem.UID} equals new
                            {srcr.TerritoryId, Uid = srcr.SubsystemUid}
                        into jns
                    from jn in jns.DefaultIfEmpty()
                    where jn == null
                    select destr)
                .ToArray();

            foreach (var item in forDelete)
            {
                dest.UserTerritory.Remove(item);
            }

            var forAdd = (from srcr in src.UserTerritory
                    join destr in dest.UserTerritory
                        on new {srcr.TerritoryId, Uid = srcr.SubsystemUid} equals new
                            {destr.TerritoryId, Uid = destr.Subsystem.UID}
                        into jns
                    from jn in jns.DefaultIfEmpty()
                    where jn == null
                    select srcr)
                .ToArray();

            if (forAdd.Any())
            {
                var context = _serviceProvider.GetRequiredService<DocumentsContext>();

                var subsystems = await context.Subsystems
                    .ToDictionaryAsync(el => el.UID);

                foreach (var topic in forAdd)
                {
                    topic.SubsystemId = subsystems[topic.SubsystemUid].Id;
                }

                dest.UserTerritory.AddRange(forAdd);
            }
        }

        private async Task UpdateRegionsAsync(User dest, User src, GraphQlKeyDictionary keysList)
        {
            var key = nameof(User.RegionIds).ToJsonFormat();

            if (!keysList.ContainsKey(key))
            {
                return;
            }

            keysList.Remove(key);

            var forDelete = (from destr in dest.UserRegion
                    join srcr in src.UserRegion
                        on new {destr.RegionId, Uid = destr.Subsystem.UID} equals new
                            {srcr.RegionId, Uid = srcr.SubsystemUid}
                        into jns
                    from jn in jns.DefaultIfEmpty()
                    where jn == null
                    select destr)
                .ToArray();

            foreach (var item in forDelete)
            {
                dest.UserRegion.Remove(item);
            }

            var forAdd = (from srcr in src.UserRegion
                    join destr in dest.UserRegion
                        on new {srcr.RegionId, Uid = srcr.SubsystemUid} equals new
                            {destr.RegionId, Uid = destr.Subsystem.UID}
                        into jns
                    from jn in jns.DefaultIfEmpty()
                    where jn == null
                    select srcr)
                .ToArray();

            if (forAdd.Any())
            {
                var context = _serviceProvider.GetRequiredService<DocumentsContext>();

                var subsystems = await context.Subsystems
                    .ToDictionaryAsync(el => el.UID);

                foreach (var topic in forAdd)
                {
                    topic.SubsystemId = subsystems[topic.SubsystemUid].Id;
                }

                dest.UserRegion.AddRange(forAdd);
            }


        }

        private async Task UpdateTopicsAsync(User dest, User src, GraphQlKeyDictionary keysList)
        {
            var key = nameof(User.TopicIds).ToJsonFormat();

            if (!keysList.ContainsKey(key))
            {
                return;
            }

            keysList.Remove(key);

            var forDelete = (from destr in dest.UserModeratorTopic
                    join srcr in src.UserModeratorTopic
                        on new {destr.TopicId, Uid = destr.Subsystem.UID} equals new
                            {srcr.TopicId, Uid = srcr.SubsystemUid}
                        into jns
                    from jn in jns.DefaultIfEmpty()
                    where jn == null
                    select destr)
                .ToArray();

            foreach (var item in forDelete)
            {
                dest.UserModeratorTopic.Remove(item);
            }

            var forAdd = (from srcr in src.UserModeratorTopic
                    join destr in dest.UserModeratorTopic
                        on new {srcr.TopicId, Uid = srcr.SubsystemUid} equals new
                            {destr.TopicId, Uid = destr.Subsystem.UID}
                        into jns
                    from jn in jns.DefaultIfEmpty()
                    where jn == null
                    select srcr)
                .ToArray();

            if (forAdd.Any())
            {
                var context = _serviceProvider.GetRequiredService<DocumentsContext>();

                var subsystems = await context.Subsystems
                    .ToDictionaryAsync(el => el.UID);

                foreach (var topic in forAdd)
                {
                    topic.SubsystemId = subsystems[topic.SubsystemUid].Id;
                }

                dest.UserModeratorTopic.AddRange(forAdd);
            }
        }

        private void UpdateRoles(User dest, User src, GraphQlKeyDictionary keysList)
        {
            var key = nameof(User.RoleIds).ToJsonFormat();

            if (!keysList.ContainsKey(key))
            {
                return;
            }

            keysList.Remove(key);

            var forDelete = (from dUr in dest.UserRole
                    join sUr in src.RoleIds on dUr.RoleId equals sUr into jSUrs
                    from jUr in jSUrs.DefaultIfEmpty()
                    where jUr == 0
                    select dUr)
                .ToArray();

            foreach (var itemRole in forDelete)
            {
                dest.UserRole.Remove(itemRole);
            }

            var foAdd = from sUr in src.RoleIds
                join dUr in dest.UserRole on sUr equals dUr.RoleId into jSUrs
                from jUr in jSUrs.DefaultIfEmpty()
                where jUr == null
                select new UserRole {RoleId = sUr};

            dest.UserRole.AddRange(foAdd);
        }

        private void UpdateAddresses(User dest, User src, GraphQlKeyDictionary keysList)
        {
            var key = nameof(User.Addresses).ToJsonFormat();

            if (!keysList.ContainsKey(key))
            {
                return;
            }

            keysList.Remove(key);

            var join = (from dAddr in dest.Addresses
                    join sAddr in src.Addresses on dAddr.AddrType equals sAddr.AddrType into joinSAddres
                    from joinSAddr in joinSAddres.DefaultIfEmpty()
                    select new {dAddr, sAddr = joinSAddr})
                .ToArray();

            var forDelete = join
                .Where(el => el.sAddr == null)
                .Select(el => el.dAddr);

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            foreach (var item in forDelete)
            {
                dest.Addresses.Remove(item);

                if (item.UserOrganizationId == null)
                {
                    context.Addresses.Remove(item);
                }
            }

            var forUpdate = join
                .Where(el => el.sAddr != null && !el.dAddr.EqualsWithoutId(el.sAddr));

            foreach (var item in forUpdate)
            {
                item.dAddr.ZipCode = item.sAddr.ZipCode;
                item.dAddr.CountryId = item.sAddr.CountryId;
                item.dAddr.Building = item.sAddr.Building;
                item.dAddr.Frame = item.sAddr.Frame;
                item.dAddr.House = item.sAddr.House;
                item.dAddr.Flat = item.sAddr.Flat;
                item.dAddr.FiasCode = item.sAddr.FiasCode;
                item.dAddr.Region = item.sAddr.Region;
                item.dAddr.City = item.sAddr.City;
                item.dAddr.District = item.sAddr.District;
                item.dAddr.Area = item.sAddr.Area;
                item.dAddr.Settlement = item.sAddr.Settlement;
                item.dAddr.AdditionArea = item.sAddr.AdditionArea;
                item.dAddr.AdditionAreaStreet = item.sAddr.AdditionAreaStreet;
                item.dAddr.Street = item.sAddr.Street;
                item.dAddr.PartialAddress = item.sAddr.PartialAddress;
            }

            var forAdd = (from sAddr in src.Addresses
                    join dAddr in dest.Addresses on sAddr.AddrType equals dAddr.AddrType into joinDAddres
                    from joinDAddr in joinDAddres.DefaultIfEmpty()
                    select new {sAddr, dAddr = joinDAddr})
                .Where(el => el.dAddr == null)
                .Select(el => el.sAddr);

            dest.Addresses.AddRange(forAdd);
        }

        private void UpdateOrganizationAddresses(UserOrganization dOrg, IReadOnlyCollection<Address> sOrgAddresses)
        {
            if (sOrgAddresses == null)
            {
                return;
            }

            if (dOrg.Addresses == null)
            {
                dOrg.Addresses = new List<Address>();
            }

            var join = (from dAddr in dOrg.Addresses
                    join sAddr in sOrgAddresses on dAddr.AddrType equals sAddr.AddrType into joinSAddres
                    from joinSAddr in joinSAddres.DefaultIfEmpty()
                    select new {dAddr, sAddr = joinSAddr})
                .ToArray();

            var forDelete = join
                .Where(el => el.sAddr == null)
                .Select(el => el.dAddr);

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            foreach (var item in forDelete)
            {
                dOrg.Addresses.Remove(item);

                if (item.UserId == null)
                {
                    context.Addresses.Remove(item);
                }
            }

            var forUpdate = join
                .Where(el => el.sAddr != null && !el.dAddr.EqualsWithoutId(el.sAddr));

            foreach (var item in forUpdate)
            {
                item.dAddr.ZipCode = item.sAddr.ZipCode;
                item.dAddr.CountryId = item.sAddr.CountryId;
                item.dAddr.Building = item.sAddr.Building;
                item.dAddr.Frame = item.sAddr.Frame;
                item.dAddr.House = item.sAddr.House;
                item.dAddr.Flat = item.sAddr.Flat;
                item.dAddr.FiasCode = item.sAddr.FiasCode;
                item.dAddr.Region = item.sAddr.Region;
                item.dAddr.City = item.sAddr.City;
                item.dAddr.District = item.sAddr.District;
                item.dAddr.Area = item.sAddr.Area;
                item.dAddr.Settlement = item.sAddr.Settlement;
                item.dAddr.AdditionArea = item.sAddr.AdditionArea;
                item.dAddr.AdditionAreaStreet = item.sAddr.AdditionAreaStreet;
                item.dAddr.Street = item.sAddr.Street;
                item.dAddr.PartialAddress = item.sAddr.PartialAddress;
            }

            var forAdd = (from sAddr in sOrgAddresses
                    join dAddr in dOrg.Addresses on sAddr.AddrType equals dAddr.AddrType into joinDAddres
                    from joinDAddr in joinDAddres.DefaultIfEmpty()
                    select new {sAddr, dAddr = joinDAddr})
                .Where(el => el.dAddr == null)
                .Select(el => el.sAddr);

            dOrg.Addresses.AddRange(forAdd);
        }

        private async Task UpdateOrganizationsAsync(User dest, User src, GraphQlKeyDictionary keysList)
        {
            var key = nameof(User.Organizations).ToJsonFormat();

            if (!keysList.ContainsKey(key))
            {
                return;
            }

            keysList.Remove(key);

            if (dest.Users2Organizations == null || src.Users2Organizations == null)
            {
                return;
            }

            var join = (from dDoc in dest.Users2Organizations
                    join sDoc in src.Users2Organizations
                        on dDoc.UserOrganization.ExtId equals sDoc.UserOrganization.ExtId
                        into joinSDocs
                    from joinSDoc in joinSDocs.DefaultIfEmpty()
                    select new {dDoc, sDoc = joinSDoc})
                .ToArray();

            var forDelete = join
                .Where(el => el.sDoc == null)
                .Select(el => el.dDoc)
                .ToArray();

            foreach (var item in forDelete)
            {
                dest.Users2Organizations.Remove(item);
            }

            var forUpdate = join
                .Where(el => el.sDoc != null);

            foreach (var item in forUpdate)
            {
                item.dDoc.Chief = item.sDoc.Chief;

                UpdateOrganization(item.dDoc.UserOrganization, item.sDoc.UserOrganization);

                UpdateOrganization(item.dDoc.UserOrganization.Parent, item.sDoc.UserOrganization.Parent);
            }

            var forAdd = (from sDoc in src.Users2Organizations
                    join dDoc in dest.Users2Organizations
                        on sDoc.UserOrganization.ExtId equals dDoc.UserOrganization.ExtId
                        into joinDDocs
                    from joinDDoc in joinDDocs.DefaultIfEmpty()
                    select new {sDoc, dDoc = joinDDoc})
                .Where(el => el.dDoc == null)
                .Select(el => el.sDoc.UserOrganization);

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            var hasOrgsExtIds = forAdd
                .Select(el => el.ExtId)
                .Union(forAdd.Select(el => el.ExtParentId))
                .Distinct();

            var hasOrgs = await context.UserOrganizations
                .Include(el => el.OrganizationPhones)
                .ThenInclude(el => el.Phone)
                .Include(el => el.Parent)
                .ThenInclude(el => el.OrganizationPhones)
                .ThenInclude(el => el.Phone)
                .Include(el => el.Addresses)
                .Include(el => el.Parent)
                .ThenInclude(el => el.Addresses)
                .Where(el => hasOrgsExtIds.Contains(el.ExtId))
                .ToDictionaryAsync(el => el.ExtId);

            foreach (var item in forAdd)
            {
                UserOrganization addOrg;

                if (item.ExtParentId != null && hasOrgs.ContainsKey(item.ExtParentId))
                {
                    var dbOrg = hasOrgs[item.ExtParentId];

                    UpdateOrganization(dbOrg, item.Parent);

                    item.Parent = dbOrg;
                }
                else
                {
                    UpdateOrganizationNormalizeValues(item.Parent);
                }

                if (hasOrgs.ContainsKey(item.ExtId))
                {
                    var dbOrg = hasOrgs[item.ExtId];

                    UpdateOrganization(dbOrg, item);

                    addOrg = dbOrg;
                }
                else
                {
                    UpdateOrganizationNormalizeValues(item);
                    addOrg = item;


                }

                dest.Users2Organizations.Add(new User2Organization
                {
                    UserOrganization = addOrg,
                    Chief = item.Chief ?? false
                });
            }
        }

        private async Task UpdateUserPhonesAsync(User dest, User src, GraphQlKeyDictionary keysList)
        {
            var phonesKey = nameof(User.Phones).ToJsonFormat();

            if (!keysList.ContainsKey(phonesKey))
            {
                return;
            }

            keysList.Remove(phonesKey);

            var joinDS = (from dp in dest.UserPhones
                    join sp in src.UserPhones
                        on new {dp.Phone.Type, dp.Phone.Number, dp.Phone.Additional, dp.UserOrganization?.ExtId}
                        equals new {sp.Phone.Type, sp.Phone.Number, sp.Phone.Additional, sp.UserOrganization?.ExtId}
                        into spjoin
                    from spj in spjoin.DefaultIfEmpty()
                    select new {dPhone = dp, sPhone = spj})
                .ToArray();

            var forDelete = joinDS
                .Where(el => el.sPhone == null)
                .Select(el => el.dPhone)
                .ToArray();

            foreach (var userPhone in forDelete)
            {
                dest.UserPhones.Remove(userPhone);
            }

            var forUpdate = joinDS
                .Where(el => el.sPhone != null
                             && (el.dPhone.Confirmed != el.sPhone.Confirmed));

            foreach (var item in forUpdate)
            {
                item.dPhone.Confirmed = item.sPhone.Confirmed;
            }

            var forAdd = (from sp in src.UserPhones
                    join dp in dest.UserPhones
                        on new {sp.Phone.Type, sp.Phone.Number, sp.Phone.Additional}
                        equals new {dp.Phone.Type, dp.Phone.Number, dp.Phone.Additional}
                        into dpjoin
                    from spj in dpjoin.DefaultIfEmpty()
                    select new {sPhone = sp, dPhone = spj})
                .Where(el => el.dPhone == null)
                .Select(el => el.sPhone)
                .ToArray();

            if (forAdd.Any())
            {
                var context = _serviceProvider.GetRequiredService<DocumentsContext>();

                var types = forAdd
                    .Select(el => el.Phone.Type)
                    .ToArray();

                var numbers = forAdd
                    .Select(el => el.Phone.Number)
                    .ToArray();

                var aditionals = forAdd
                    .Select(el => el.Phone.Additional)
                    .ToArray();

                var rawSelection = from phone in context.Phones
                    where types.Contains(phone.Type) && numbers.Contains(phone.Number) &&
                          aditionals.Contains(phone.Additional)
                    select phone;

                var dbPhones = from phone in rawSelection.AsEnumerable()
                    join pair in forAdd
                        on new {phone.Type, phone.Number, phone.Additional}
                        equals new {pair.Phone.Type, pair.Phone.Number, pair.Phone.Additional}
                    select phone;


                forAdd = (from add in forAdd
                        join dbPhone in dbPhones
                            on new {add.Phone.Type, add.Phone.Number, add.Phone.Additional}
                            equals new {dbPhone.Type, dbPhone.Number, dbPhone.Additional}
                            into joinPhones
                        from joinPhone in joinPhones.DefaultIfEmpty()
                        select new OrganizationUserPhone()
                        {
                            Phone = joinPhone ?? src.UserPhones.Select(el => el.Phone)
                                .GroupBy(el => new {el.Type, el.Number, el.Additional})
                                .Where(el => el.Key.Type == add.Phone.Type
                                             && el.Key.Number == add.Phone.Number
                                             && el.Key.Additional == add.Phone.Additional)
                                .Select(el => el.First())
                                .Single(),
                            Confirmed = add.Confirmed,
                            UserOrganization = dest.Users2Organizations?
                                .Where(el => el.UserOrganization.ExtId == add.UserOrganization?.ExtId)
                                .Select(el => el.UserOrganization)
                                .FirstOrDefault()
                        })
                    .ToArray();

                dest.UserPhones.AddRange(forAdd);
            }
        }


        private static void UpdateOrganizationPhones(UserOrganization dest, IEnumerable<Phone> needPhones)
        {
            if (needPhones == null)
            {
                return;
            }

            if (dest.OrganizationPhones == null)
            {
                dest.OrganizationPhones = new List<OrganizationUserPhone>();
            }

            var join = (from dPhone in dest.OrganizationPhones
                    join sPhone in needPhones
                        on new {dPhone.Phone.Type, dPhone.Phone.Number, dPhone.Phone.Additional} equals new
                            {sPhone.Type, sPhone.Number, sPhone.Additional}
                        into joinSPhones
                    from joinSPhone in joinSPhones.DefaultIfEmpty()
                    select new {dPhone, sPhone = joinSPhone})
                .ToArray();

            var forDelete = join
                .Where(el => el.sPhone == null)
                .Select(el => el.dPhone);

            foreach (var phone in forDelete)
            {
                dest.OrganizationPhones.Remove(phone);
            }

            var forUpdate = join.Where(el => el.sPhone != null && el.dPhone.Confirmed != el.sPhone.Confirmed)
                .Select(el => new {dPhone = el.dPhone, el.sPhone});

            foreach (var item in forUpdate)
            {
                item.dPhone.Confirmed = item.sPhone.Confirmed;
            }

            var forAdd = (from sPhone in needPhones
                    join dPhone in dest.Phones
                        on new {sPhone.Type, sPhone.Number, sPhone.Additional} equals new
                            {dPhone.Type, dPhone.Number, dPhone.Additional}
                        into joinDPhones
                    from joinDPhone in joinDPhones.DefaultIfEmpty()
                    select new {sPhone, dPhone = joinDPhone})
                .Where(el => el.dPhone == null)
                .Select(el => new OrganizationUserPhone
                {
                    Phone = el.sPhone,
                    Confirmed = el.sPhone.Confirmed
                });

            dest.OrganizationPhones.AddRange(forAdd);
        }

        private void UpdateOrganization(UserOrganization dOrg, UserOrganization sOrg)
        {
            if (dOrg == null || sOrg == null)
            {
                return;
            }

            dOrg.FullName = sOrg.FullName;
            dOrg.ShortName = sOrg.ShortName;
            dOrg.Type = sOrg.Type;
            dOrg.Inn = sOrg.Inn;
            dOrg.Kpp = sOrg.Kpp;
            dOrg.Ogrn = sOrg.Ogrn;
            dOrg.Leg = sOrg.Leg;
            dOrg.AgencyType = sOrg.AgencyType;
            dOrg.AgencyTerRang = sOrg.AgencyTerRang;
            dOrg.Email = sOrg.Email;
            dOrg.EmailVerified = sOrg.EmailVerified;

            UpdateOrganizationNormalizeValues(dOrg);

            UpdateOrganizationPhones(dOrg, sOrg.Phones);

            UpdateOrganizationAddresses(dOrg, sOrg.Addresses);
        }

        private void UpdateDocuments(User dest, User src, GraphQlKeyDictionary keysList)
        {
            var key = nameof(User.UserIdentityDocuments).ToJsonFormat();

            if (!keysList.ContainsKey(key))
            {
                return;
            }

            keysList.Remove(key);

            var join = (from dDoc in dest.UserIdentityDocuments
                    join sDoc in src.UserIdentityDocuments
                        on new {dDoc.Type, dDoc.Series, dDoc.Number} equals new {sDoc.Type, sDoc.Series, sDoc.Number}
                        into joinSDocs
                    from joinSDoc in joinSDocs.DefaultIfEmpty()
                    select new {dDoc, sDoc = joinSDoc})
                .ToArray();

            var forDelete = join
                .Where(el => el.sDoc == null)
                .Select(el => el.dDoc)
                .ToArray();

            foreach (var item in forDelete)
            {
                dest.UserIdentityDocuments.Remove(item);
            }

            var forUpdate = join
                .Where(el => el.sDoc != null
                             && (el.dDoc.ExpiryDate != el.sDoc.ExpiryDate
                                 || el.dDoc.IssueDate != el.sDoc.IssueDate
                                 || el.dDoc.IssueId != el.sDoc.IssueId
                                 || el.dDoc.IssuedBy != el.sDoc.IssuedBy
                                 || el.dDoc.Verified != el.sDoc.Verified));

            foreach (var item in forUpdate)
            {
                item.dDoc.ExpiryDate = item.sDoc.ExpiryDate;
                item.dDoc.IssueDate = item.sDoc.IssueDate;
                item.dDoc.IssueId = item.sDoc.IssueId;
                item.dDoc.IssuedBy = item.sDoc.IssuedBy;
                item.dDoc.Verified = item.sDoc.Verified;
            }

            var forAdd = (from sDoc in src.UserIdentityDocuments
                    join dDoc in dest.UserIdentityDocuments
                        on new {sDoc.Type, sDoc.Series, sDoc.Number} equals new {dDoc.Type, dDoc.Series, dDoc.Number}
                        into joinDDocs
                    from joinDDoc in joinDDocs.DefaultIfEmpty()
                    select new {sDoc, dDoc = joinDDoc})
                .Where(el => el.dDoc == null)
                .Select(el => el.sDoc);

            dest.UserIdentityDocuments.AddRange(forAdd);

        }

        internal ClaimsPrincipal CurrentClaimsPrincipal()
        {
            var httpContextAccessor = (IHttpContextAccessor) _serviceProvider.GetService(typeof(IHttpContextAccessor));

            var claimsPrincipal = httpContextAccessor?.HttpContext?.User;

            return claimsPrincipal;
        }

        public long? ClaimsPrincipalId(ClaimsPrincipal claimsPrincipal = null)
        {
            var idClaim = (claimsPrincipal ?? CurrentClaimsPrincipal())?.Claims
                .FirstOrDefault(el => el.Type == JwtRegisteredClaimNames.Jti);

            if (idClaim == null || !long.TryParse(idClaim.Value, out var id))
            {
                return null;
            }

            return id;
        }

        public bool HasClaim(string claimType, string claimValue = null, ClaimsPrincipal claimsPrincipal = null)
        {
            var principal = claimsPrincipal ?? CurrentClaimsPrincipal();

            return principal.Identity.IsAuthenticated
                   && principal.Claims.Any(el =>
                       el.Type == claimType && (claimValue == null || el.Value == claimValue));
        }

        public async Task<bool> HasClaimAsync(User user, string claimName, string claimValue = null)
        {
            var context = (DocumentsContext) _serviceProvider.GetService(typeof(DocumentsContext));

            var usersClaims = context.UserClaims
                .Where(el => el.UserId == user.Id)
                .Select(el => new
                {
                    Type = el.ClaimType,
                    Value = el.ClaimValue
                });

            var roleClaims = context.UserRoles
                .Where(el => el.UserId == user.Id)
                .Join(context.RoleClaims,
                    u => u.RoleId,
                    c => c.RoleId,
                    (u, c) => new
                    {
                        Type = c.ClaimType,
                        Value = c.ClaimValue
                    });

            var claims = usersClaims
                .Union(roleClaims);

            return await claims
                .AnyAsync(el => el.Type == claimName && (claimValue == null || el.Value == claimValue));
        }

        public async Task<User> CurrentUser()
        {
            var claimsPrincipal = CurrentClaimsPrincipal();

            if (!claimsPrincipal.Identity.IsAuthenticated)
            {
                return null;
            }

            var userId = ClaimsPrincipalId(claimsPrincipal);

            if (!userId.HasValue)
            {
                return null;
            }

            var userManager = (UserManager<User>) _serviceProvider.GetService(typeof(UserManager<User>));

            return await userManager.FindByIdAsync(userId.Value.ToString());
        }

        private bool PrincipalHasUserManagmentClaim(ClaimsPrincipal claimsPrincipal = null)
        {
            return (claimsPrincipal ?? CurrentClaimsPrincipal()).Claims.Any(el =>
                el.Type == ClaimName.Manage && el.Value == "users");
        }

        public async Task<IList<User>> GetUsersByClaim(string claimName, string claimValue = null)
        {
            var context = (DocumentsContext) _serviceProvider.GetService(typeof(DocumentsContext));

            var byUsersClaims = context.UserClaims
                .Where(el => el.ClaimType == claimName && (claimValue == null || el.ClaimValue == claimValue))
                .Select(el => el.UserId);

            var byRoleClaims = context.RoleClaims
                .Where(el => el.ClaimType == claimName && (claimValue == null || el.ClaimValue == claimValue))
                .Join(context.UserRoles,
                    c => c.RoleId,
                    u => u.RoleId,
                    (c, u) => u.UserId);

            var userIds = byUsersClaims
                .Union(byRoleClaims)
                .Distinct();

            var users = context.Users
                .Where(el => userIds.Contains(el.Id));

            return await users.ToListAsync();
        }

        private void UpdateUserNormalizeValues(User user)
        {
            var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();

            var normalize = keyNormalizer.NormalizeName(user.UserName);

            if (user.NormalizedUserName != normalize)
            {
                user.NormalizedUserName = normalize;
            }

            normalize = keyNormalizer.NormalizeName(user.FullName);

            if (user.NormalizeFullName != normalize)
            {
                user.NormalizeFullName = normalize;
            }

            normalize = keyNormalizer.NormalizeEmail(user.Email);

            if (user.NormalizedEmail != normalize)
            {
                user.NormalizedEmail = normalize;
            }
        }

        private void UpdateOrganizationNormalizeValues(UserOrganization org)
        {
            if (org == null)
            {
                return;
            }

            var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();

            var normalize = keyNormalizer.NormalizeName(org.FullName);

            if (org.NormalizeFullName != normalize)
            {
                org.NormalizeFullName = normalize;
            }

            normalize = keyNormalizer.NormalizeName(org.ShortName);

            if (org.NormalizeShortName != normalize)
            {
                org.NormalizeShortName = normalize;
            }
        }

        public async Task<Pagination<User>> GetModeratorsAsync(string[] subsystemUid,
            SelectionRange range,
            GraphQlKeyDictionary requestedKeys)
        {
            if (subsystemUid == null || !subsystemUid.Any())
            {
                var context = _serviceProvider.GetRequiredService<DocumentsContext>();

                subsystemUid = await context.Subsystems
                    .Select(el => el.UID)
                    .ToArrayAsync();
            }

            if (!subsystemUid.Any())
            {
                return new Pagination<User>();
            }

            var filter = new List<GraphQlQueryFilter>
            {
                new GraphQlQueryFilter
                {
                    Where = GenerateModeratorFilter(subsystemUid)
                }
            };

            return await GetUsersAsync(range, filter, requestedKeys);
        }
        public async Task<Pagination<User>> GetExecutorsAsync(string[] subsystemUid,
            SelectionRange range,
            GraphQlKeyDictionary requestedKeys)
        {
            if (subsystemUid == null || !subsystemUid.Any())
            {
                var context = _serviceProvider.GetRequiredService<DocumentsContext>();
                subsystemUid = await context.Subsystems.Select(el => el.UID).ToArrayAsync();
            }
            if (!subsystemUid.Any())
            {
                return new Pagination<User>();
            }
            var filter = new List<GraphQlQueryFilter>
            {
                new GraphQlQueryFilter
                {
                    Where = GenerateExecutorFilter(subsystemUid)
                }
            };
            return await GetUsersAsync(range, filter, requestedKeys);
        }

        private string GenerateModeratorFilter(params string[] subsystemUid)
        {
            const string pattern = @"((el.IdentityClaims.Any(x => x.ClaimType == ""ss""
                && x.ClaimValue == ""{0}.available"")
                || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y => y.ClaimType == ""ss"" && y.ClaimValue == ""{0}.available"")))
                && (el.IdentityClaims.Any(x => x.ClaimType == ""md"" && x.ClaimValue == ""{0}.enable"")
                    || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y => y.ClaimType == ""md"" && y.ClaimValue == ""{0}.enable""))))";

            var parts = new List<string>(subsystemUid.Length);

            parts.AddRange(subsystemUid.Select(ssUid => string.Format(pattern, ssUid)));

            return string.Format("el=>{0}", string.Join(" || ", parts));
        }
        private string GenerateExecutorFilter(params string[] subsystemUid)
        {
            const string pattern = @"((el.IdentityClaims.Any(x => x.ClaimType == ""ss""
                && x.ClaimValue == ""{0}.available"")
                || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y => y.ClaimType == ""ss"" && y.ClaimValue == ""{0}.available"")))
                && (el.IdentityClaims.Any(x => x.ClaimType == ""ss"" && x.ClaimValue == ""{0}.executor"")
                    || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y => y.ClaimType == ""ss"" && y.ClaimValue == ""{0}.executor""))))";

            var parts = new List<string>(subsystemUid.Length);

            parts.AddRange(subsystemUid.Select(ssUid => string.Format(pattern, ssUid)));

            return string.Format("el=>{0}", string.Join(" || ", parts));
        }
    }
}
