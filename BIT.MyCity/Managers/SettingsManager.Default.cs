using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Services;
using BIT.MyCity.Subsystems;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Managers
{
    public sealed partial class SettingsManager
    {
        public async Task<bool> SetDefaultValues()
        {
            try
            {
                var subsystemService = _serviceProvider.GetRequiredService<SubsystemService>();

                var defaultSettings = subsystemService.Subsystems
                    .Aggregate<IExternalSubsystem, IEnumerable<Settings>>(
                        DefaultSettings,
                        (current, subsystem) => current.Union(subsystem.DefaultSettings))
                    .ToArray();

                var hasSettings = GetSettingsList();

                var join = (from dbSet in hasSettings
                            join defSet in defaultSettings on dbSet.Key equals defSet.Key into joinDefs
                            from joinDef in joinDefs.DefaultIfEmpty()
                            select new { db = dbSet, def = joinDef })
                    .ToArray();

                var forDelete = join
                    .Where(el => el.def == null)
                    .Select(el => el.db)
                    .ToArray();

                var forUpdate = join
                    .Where(el => el.def != null
                                 && (el.db.Weight != el.def.Weight
                                     || el.db.SettingNodeType != el.def.SettingNodeType
                                     || el.db.ValueType != el.def.ValueType
                                     || el.db.Nullable != el.def.Nullable
                                     || el.db.SettingType != el.def.SettingType
                                     || el.db.ReadingRightLevel != el.def.ReadingRightLevel
                                     || el.db.Description != el.def.Description
                                     || !ValuesEqual(el.db.Values,el.def.Values)))
                    .ToArray();

                var forUpdateValue = join
                    .Where(el => el.def != null
                                 && el.def.Values != null
                                 && el.def.Values.All(x => x.Value != el.db.Value))
                    .ToArray();

                var updated = forUpdate
                    .Select(el => el.db)
                    .Union(forUpdateValue.Select(el => el.db))
                    .Distinct()
                    .ToArray();

                var forAdd = (from defSet in defaultSettings
                              join dbSet in hasSettings on defSet.Key equals dbSet.Key into joinDbs
                              from joinDb in joinDbs.DefaultIfEmpty()
                              select new { def = defSet, db = joinDb })
                    .Where(el => el.db == null)
                    .Select(el => el.def)
                    .ToArray();

                using (var scope = _scopeFactory.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                    context.Settings.AttachRange(forDelete);

                    context.Settings.RemoveRange(forDelete);

                    context.Settings.AttachRange(updated);

                    foreach (var item in forUpdate)
                    {
                        item.db.Weight = item.def.Weight;
                        item.db.SettingNodeType = item.def.SettingNodeType;
                        item.db.ValueType = item.def.ValueType;
                        item.db.Nullable = item.def.Nullable;
                        item.db.SettingType = item.def.SettingType;
                        item.db.ReadingRightLevel = item.def.ReadingRightLevel;
                        item.db.Description = item.def.Description;
                        item.db.Values = item.def.Values;
                    }

                    foreach (var item in forUpdateValue)
                    {
                        item.db.Value = item.def.Value;
                    }

                    context.Settings.AddRange(forAdd);

                    await context.SaveChangesAsync();
                }

                ReloadSettings();

                return true;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка установки значений по умолчанию настроек системы", ex);

                return false;
            }
        }

        public static bool ValuesEqual(SettingsValueItem[] x, SettingsValueItem[] y)
        {
            if (x == null && y == null)
            {
                return true;
            }

            if (x == null || y == null)
            {
                return false;
            }

            return x.SequenceEqual(y, new SettingsValueItemComparer());
        }

        private class SettingsValueItemComparer : IEqualityComparer<SettingsValueItem>
        {
            public bool Equals(SettingsValueItem x, SettingsValueItem y)
            {
                if (x == null && y == null)
                {
                    return true;
                }

                if (x == null || y == null)
                {
                    return false;
                }

                return x.Equals(y);
            }

            public int GetHashCode(SettingsValueItem obj)
            {
                return obj.GetHashCode();
            }
        }

        private Settings[] DefaultSettings
        {
            get
            {
                return DefaultSettingsInner
                    .Where(el => CheckKey(el.Key, out _))
                    .ToArray();
            }
        }

        private static readonly Settings[] DefaultSettingsInner =
        {
            #region SystemWeb
            new Settings
            {
                Key = SettingsKeys.SystemWeb,
                Description = "Web интерфейс",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.TabGroup,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebGeneral,
                Description = "Основные",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebGeneralBaseUrl,
                Description = "Базовый url web-сайта",
                Value = "http://",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(Uri).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebGeneralMainTitle,
                Description = "Заглавный титул",
                Value = "Электронная приемная граждан",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 1
            },

            new Settings
            {
                Key = SettingsKeys.SystemWebRubric,
                Description = "Справочник \"Рубрики\"",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebRubricTitleOne,
                Description = "Название (ед. число)",
                Value = "Орган Власти",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebRubricTitle,
                Description = "Название (множ. число)",
                Value = "Органы Власти",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebRubricAddEnabled,
                Description = "Добавление из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Authorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },

            new Settings
            {
                Key = SettingsKeys.SystemWebTerritory,
                Description = "Справочник \"Территории\"",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebTerritoryTitleOne,
                Description = "Название (ед. число)",
                Value = "Орган Власти",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebTerritoryTitle,
                Description = "Название (множ. число)",
                Value = "Органы Власти",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebTerritoryAddEnabled,
                Description = "Добавление из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Authorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value
            },

            new Settings
            {
                Key = SettingsKeys.SystemWebRegion,
                Description = "Справочник \"Регионы\"",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebRegionTitleOne,
                Description = "Название (ед. число)",
                Value = "Регионы",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebRegionTitle,
                Description = "Название (множ. число)",
                Value = "Регион",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebRegionAddEnabled,
                Description = "Добавление из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Authorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value
            },

            new Settings
            {
                Key = SettingsKeys.SystemWebTopic,
                Description = "Справочник \"Темы\"",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 5
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebTopicTitleOne,
                Description = "Название (ед. число)",
                Value = "Регион",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebTopicTitle,
                Description = "Название (множ. число)",
                Value = "Регионы",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemWebTopicAddEnabled,
                Description = "Добавление из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Authorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value
            },
            #endregion

            #region SystemAuth
            new Settings
            {
                Key = SettingsKeys.SystemAuth,
                Description = "Авторизация",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.TabGroup,
                Weight = 2
            },

            new Settings
            {
                Key = SettingsKeys.SystemAuthToken,
                Description = "Токены доступа",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthTokenLifetimeCitizen,
                Description = "Время жизни для токена граждан (в минутах)",
                Value = "60",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthTokenLifetimeCitizenMobile,
                Description = "Время жизни для токена граждан (для мобильных устройств) (в минутах)",
                Value = "10080",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthTokenLifetimeManager,
                Description = "Время жизни для токена администраторов/модераторов (в минутах)",
                Value = "10",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },


            new Settings
            {
                Key = SettingsKeys.SystemAuthEsia,
                Description = "Единая система авторизации (ЕСИА)",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthEsiaEnabled,
                Description = "Признак включения авторизации ЕСИА",
                Value = "true",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthEsiaTestMode,
                Description = "Тестовый режим ЕСИА",
                Value = "true",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthEsiaMnemonics,
                Description = "Мнемоника ИС ЕСИА",
                Value = "LETTERS61",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthEsiaScoupeCitizen,
                Description = "Скоупы ЕСИА (для граждан)",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthEsiaScoupeOrganization,
                Description = "Скоупы ЕСИА (для организаций)",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 5
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthEsiaOpensslPath,
                Description = "Путь для запуска openssl",
                Value = "openssl",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 6
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthEsiaCerificateEsia,
                Description = "Сертификат ЕСИА",
                Value = null,
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.SecretFile,
                Weight = 7
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthEsiaCerificateOrganization,
                Description = "Сертификат организации",
                Value = null,
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.SecretFile,
                Weight = 8
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthEsiaCerificateOrganizationKey,
                Description = "Секретный ключ сертификата организации",
                Value = null,
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.SecretFile,
                Weight = 9
            },


            new Settings
            {
                Key = SettingsKeys.SystemAuthVk,
                Description = "Аторизация \"В Контакте\"",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthVkEnabled,
                Description = "Признак включения авторизации В Контакте",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthVkAppKey,
                Description = "Ключ приложения В Контакте",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthVkSecretKey,
                Description = "Секретный ключ В Контакте",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthVkServiceKey,
                Description = "Сервисный ключ В Контакте",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 4
            },

            new Settings
            {
                Key = SettingsKeys.SystemAuthFacebook,
                Description = "Аторизации \"Facebook\"",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthFacebookEnabled,
                Description = "Признак включения авторизации Facebook",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthFacebookAppKey,
                Description = "Ключ приложения Facebook",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthFacebookSecretKey,
                Description = "Секретный ключ Facebook",
                Value = "",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },


            new Settings
            {
                Key = SettingsKeys.SystemAuthForm,
                Description = "Аторизация через формы",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 5
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthFormEnabled,
                Description = "Признак включения авторизации \"Формы\"",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthFormAuthAfterRegistration,
                Description = "Авторизовать после регистрации",
                Value = "false",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthFormConfirmEmailUrl,
                Description = "Адрес страницы подтверждения Email",
                Value = "http://",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(Uri).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthFormResetPasswordUrl,
                Description = "Адрес страницы сброса пароля",
                Value = "http://",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(Uri).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthFormDisableNotConfirmEmail,
                Description = "Не давать доступ к подсистемам при не подтвержденном Email",
                Value = "true",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 5
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthAdditional,
                Description = "Дополнительные настройки",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 6
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthAdditionalUpdatePrevious,
                Description = "Обновлять предыдущие",
                Value = "false",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            #endregion

            #region SystemApi
            new Settings
            {
                Key = SettingsKeys.SystemApi,
                Description = "API",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.TabGroup,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKeys.SystemApiBaseUrl,
                Description = "Базовый url api-сайта",
                Value = "http://",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(Uri).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemApiNotificationAppealsRevisionTime,
                Description = "Время опроса обращений (ЧЧ:MM)",
                Value = "01:00",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemApiNotificationAppealsAppealIntrval,
                Description = "Интервал просроченных обращений (в часах)",
                Value = "24",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(uint).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKeys.SystemAuthSmsConfirmCodeLifetime,
                Description = "Время жизни кода подтверждения телефона (в минутах)",
                Value = "10",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 4
            },

            #endregion

            #region SystemSmtp
            new Settings
            {
                Key = SettingsKeys.SystemSmtp,
                Description = "Электронная почта",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.TabGroup,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmtpEnable,
                Description = "Включена",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "false",
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmtpHost,
                Description = "SMTP хост",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = null,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmtpPort,
                Description = "SMTP порт",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "25",
                ValueType = typeof(int).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmtpUser,
                Description = "Логин",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = null,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmtpPassword,
                Description = "Пароль",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = null,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 5
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmtpUseSsl,
                Description = "Использовать SSL",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "true",
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 6
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmtpFrom,
                Description = "Адрес отправителя",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = null,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Weight = 7
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmtpFromName,
                Description = "Имя отправителя",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "Администрация сайта",
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 8
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmtpCheckCertificateRevocation,
                Description = "Проверять отзыв сертификата",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "true",
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 9
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmtpReSendPeriod,
                Description = "Периодичность повторной отправки (в минутах 1 - 60)",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "20",
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 10
            },
            #endregion

            #region Sms
            new Settings
            {
                Key = SettingsKeys.SystemSms,
                Description = "СМС сообщения",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.TabGroup,
                Weight = 5
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmsProvider,
                Description = "Провайдер отправки (СМС)",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "TestSms",
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value,
                Values = new SettingsValueItem[]
                {
                    new SettingsValueItem<string>("Тестовый", "Тестовый"),
                    new SettingsValueItem<string>("ЭКСПЕКТО", "ЭКСПЕКТО")
                }
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmsLogin,
                Description = "Логин (СМС)",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = null,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmsPassword,
                Description = "Пароль (СМС)",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = null,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value
            },
            new Settings
            {
                Key = SettingsKeys.SystemSmsSender,
                Description = "Подпись отправителя (СМС)",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = null,
                ValueType = typeof(string).FullName,
                Nullable = true,
                SettingType = SettingType.Value
            },
            #endregion

            #region Attaches
            new Settings
            {
                Key = SettingsKeys.SystemAttach,
                Description = "Вложения",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.TabGroup,
                Weight = 6
            },
            new Settings
            {
                Key = SettingsKeys.SystemAttachAllowedExtensions,
                Description = "Разрешенные расширения файлов",
                ReadingRightLevel = RightLevel.Unauthorized,
                SettingType = SettingType.Value,
                Value = "png, jpg, jpeg, tiff, doc, docx, xlsx, xls, pdf, sig",
                ValueType = typeof(string).FullName,
                Nullable = false,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemAttachThumbnailsGqlHeight, //"system.attach.thumbnails.gql.height.",
                Description = "Высота миниатюры для поля Thumb",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "600",
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemAttachThumbnailsGqlWidth, //"system.attach.thumbnails.gql.width.",
                Description = "Ширина миниатюры для поля Thumb",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "800",
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKeys.SystemAttachThumbnailsGqlMinHeight, //"system.attach.thumbnails.gql.minheight.",
                Description = "Минимальная высота миниатюры для поля Thumb",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "32",
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKeys.SystemAttachThumbnailsGqlMinWidth, //"system.attach.thumbnails.gql.minwidth.",
                Description = "Минимальная ширина миниатюры для поля Thumb",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "32",
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 5
            },
            new Settings
            {
                Key = SettingsKeys.SystemAttachThumbnailsGqlMaxHeight, //"system.attach.thumbnails.gql.maxheight.",
                Description = "Максимальная высота миниатюры для поля Thumb",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "1024",
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 6
            },
            new Settings
            {
                Key = SettingsKeys.SystemAttachThumbnailsGqlMaxWidth, //"system.attach.thumbnails.gql.maxwidth.",
                Description = "Максимальная ширина миниатюры для поля Thumb",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                Value = "1024",
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 7
            },
            #endregion

            #region System options

            new Settings
            {
                Key = SettingsKeys.SystemOptions,
                Description = "Системные",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.TabGroup,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemOptionsJournal,
                Description = "Журнал изменений",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemOptionsJournalSavePeriod,
                Description = "Период хранения информации (в месяцах)",
                Value = "24",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemOptionsJournalClearHour,
                Description = "Время очистки журнала (час) (0-23)",
                Value = "1",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemOptionsFileCleaner,
                Description = "Очистка неиспользуемых файлов",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                SettingNodeType = SettingNodeType.Group,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKeys.SystemOptionsFileCleanerUncheckPeriod,
                Description = "Период хранения без проверки (в минутах)",
                Value = "10",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKeys.SystemOptionsFileCleanerClearPeriod,
                Description = "Период очистки файлов (в минутах) (1-60)",
                Value = "5",
                ReadingRightLevel = RightLevel.AdministratorSettings,
                ValueType = typeof(uint).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            }

        #endregion


        #region Тестовые настройки
        //new Settings
        //{
        //    Key = "system.test.int.",
        //    Description = "uint",
        //    ReadingRightLevel = RightLevel.AdministratorSettings,
        //    Value = "10",
        //    ValueType = typeof(uint).FullName,
        //    Nullable = false,
        //    SettingType = SettingType.Value,
        //    Weight = 6,
        //    Values = new SettingsValueItem[]
        //    {
        //        new SettingsValueItem<uint>(10, "10"),
        //        new SettingsValueItem<uint>(20, "20"),
        //        new SettingsValueItem<uint>(30, "30"),
        //    }
        //},
        //new Settings
        //{
        //    Key = "system.test.decimal.",
        //    Description = "decimal",
        //    ReadingRightLevel = RightLevel.AdministratorSettings,
        //    Value = "1024.1234",
        //    ValueType = typeof(decimal?).FullName,
        //    Nullable = true,
        //    SettingType = SettingType.Value,
        //    Weight = 6,
        //    Values = new SettingsValueItem[]
        //    {
        //        new SettingsValueItem<decimal?>(null, "---------"),
        //        new SettingsValueItem<decimal?>(1024.1234m, "1024.1234"),
        //        new SettingsValueItem<decimal?>(200, "200"),
        //        new SettingsValueItem<decimal?>(300, "300"),
        //    }
        //},
        //new Settings
        //{
        //    Key = "system.test.string.",
        //    Description = "string",
        //    ReadingRightLevel = RightLevel.AdministratorSettings,
        //    Value = "1024",
        //    ValueType = typeof(string).FullName,
        //    Nullable = false,
        //    SettingType = SettingType.Value,
        //    Weight = 6,
        //    Values = new SettingsValueItem[]
        //    {
        //        new SettingsValueItem<string>("string1", "string1"),
        //        new SettingsValueItem<string>("string2", "string2"),
        //        new SettingsValueItem<string>("string3", "string3")
        //    }
        //},
        //new Settings
        //{
        //    Key = "system.test.enum.",
        //    Description = "enum",
        //    ReadingRightLevel = RightLevel.AdministratorSettings,
        //    Value = SettingNodeType.None.ToString(),
        //    ValueType = typeof(SettingNodeType).FullName,
        //    Nullable = false,
        //    SettingType = SettingType.Value,
        //    Weight = 6,
        //    Values = new SettingsValueItem[]
        //    {
        //        new SettingsValueItem<SettingNodeType>(SettingNodeType.None, SettingNodeType.None.ToString()),
        //        new SettingsValueItem<SettingNodeType>(SettingNodeType.TabGroup, SettingNodeType.TabGroup.ToString()),
        //        new SettingsValueItem<SettingNodeType>(SettingNodeType.Group, SettingNodeType.Group.ToString()),
        //        new SettingsValueItem<SettingNodeType>(SettingNodeType.Value, SettingNodeType.Value.ToString()),
        //    }
        //},
        //new Settings
        //{
        //    Key = "system.test.appeal.",
        //    Description = "AppealApprove",
        //    ReadingRightLevel = RightLevel.AdministratorSettings,
        //    Value = null,
        //    ValueType = typeof(AppealApprove).FullName,
        //    Nullable = true,
        //    SettingType = SettingType.Value,
        //    Weight = 6,
        //    Values = new SettingsValueItem[]
        //    {
        //        new SettingsValueItem<AppealApprove>(null, "--"),
        //        new SettingsValueItem<AppealApprove>(new AppealApprove{RejectionReason = "1-100"}, "1-100"),
        //        new SettingsValueItem<AppealApprove>(new AppealApprove{RejectionReason = "1-1000"}, "1-1000"),
        //        new SettingsValueItem<AppealApprove>(new AppealApprove{RejectionReason = "1-10000"}, "1-10000")
        //    }
        //}, 
        #endregion
    };
    }
}
