﻿using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Pagination;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BIT.MyCity.Managers
{
    public class PTokenManager
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        private readonly SubsystemManager _ssm;
        public PTokenManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
            _ssm = (SubsystemManager)_serviceProvider.GetService(typeof(SubsystemManager));
        }
        public async Task<Pagination<PToken>> GetTokensAsync(QueryOptions options, long? id = null)
        {
            var userId = _um.ClaimsPrincipalId();
            if (!userId.HasValue)
            {
                throw new Exception("Вы не авторизованы");
            }
            if (id.HasValue && id.Value != userId.Value)
            {
                if(_um.CurrentClaimsPrincipal()?.Claims.FirstOrDefault(el => el.Type == ClaimName.Service) == null)
                {
                    throw new Exception($"Need '{ClaimName.Service}' claim");
                }
            }
            else
            {
                id = userId;
            }

            options.RequestedFields.TryGetValue(nameof(Pagination<PToken>.Items).ToJsonFormat(), out var items);
            var obj = _ctx.Tokens
                .Where(el => el.OwnerId == id.Value)
                ;
            var result = new Pagination<PToken>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<BaseIdEntity>.Items).ToJsonFormat()))
            {
                result.Items = await obj.ToListAsync();
            }
            return result;
        }
        public async Task<PToken> CreateDbAsync(PToken src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields)
        {
            var user = await _um.CurrentUser();
            if (user == null)
            {
                throw new Exception("Вы не авторизованы");
            }
            var dest = await _ctx.Tokens.Where(el => el.OwnerId == user.Id && el.Value == src.Value).FirstOrDefaultAsync();
            if (dest == null)
            {
                src.Owner = user;
                _ctx.Tokens.Add(src);
                await _ctx.SaveChangesAsync();
                return src;
            }
            else
            {
                return dest;
            }
        }
        public async Task<PToken> DeleteDbAsync(PToken src, GraphQlKeyDictionary updateKeys, GraphQlKeyDictionary requestedFields, long? id = null)
        {
            var user = await _um.CurrentUser();
            if (user == null)
            {
                throw new Exception("Вы не авторизованы");
            }
            if (id.HasValue && id.Value != user.Id)
            {
                if (_um.CurrentClaimsPrincipal()?.Claims.FirstOrDefault(el => el.Type == ClaimName.Service && el.Value == "delo") == null)
                {
                    throw new Exception($"Need '{ClaimName.Service}' claim");
                }
            }
            else
            {
                id = user.Id;
            }
            var token = await _ctx.Tokens.FirstOrDefaultAsync(el => el.OwnerId == id.Value && el.Value == src.Value);
            if (token == null)
            {
                throw new Exception("Token not found");
            }
            _ctx.Remove(token);
            await _ctx.SaveChangesAsync();
            src.Value = "deleted";
            return src;
        }
    }
}
