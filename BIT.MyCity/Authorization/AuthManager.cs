using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization.ExtApiHelpers;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Services;
using BIT.MyCity.Utilites;
using GraphQL.Utilities;
using log4net;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.WebUtilities;
using LoginResult = BIT.MyCity.Authorization.Models.LoginResult;

namespace BIT.MyCity.Authorization
{
    public partial class AuthManager
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(AuthManager));

        private readonly IServiceProvider _serviceProvider;

        private readonly Lazy<SymmetricSecurityKey> _symmetricSecurityKey;

        public AuthManager(
          IConfiguration config,
          IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            var secret = config.GetSection("JwtConfig").GetSection("secret").Value;

            _symmetricSecurityKey = new Lazy<SymmetricSecurityKey>(() => new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret)));
        }

        public IEnumerable<LoginSystem> GetAllLoginModes()
        {
            var result = new List<LoginSystem>();

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            var isActiveFormsAuthorization = settingsManager.SettingValue<bool>(SettingsKeys.SystemAuthFormEnabled);

            if (isActiveFormsAuthorization)
            {
                result.Add(LoginSystem.Form);
            }

            var helperFabric = _serviceProvider.GetRequiredService<AuthExtApiHelperFabric>();

            var activeExtSystems = helperFabric.ActiveLoginSystems;

            result.AddRange(activeExtSystems);

            return result;
        }

        public RedirectUrl GetRedirectUrl(RedirectUrl redirectUrl)
        {
            var loginModel = new LoginModel
            {
                LoginSystem = redirectUrl.LoginSystem,
                CallbackUrl = TransformCallbackUrl(redirectUrl.Url?.Trim(), redirectUrl.LoginSystem)
            };
            
            var authFabric = (AuthExtApiHelperFabric)_serviceProvider.GetService(typeof(AuthExtApiHelperFabric));

            var apiHelper = authFabric.GetHelper(loginModel.LoginSystem);

            redirectUrl.Url = apiHelper?.RedirectUrl(loginModel);

            return redirectUrl;
        }

        private string TransformCallbackUrl(string callbackUrl, LoginSystem loginSystem)
        {
            const string pattern = "^http[s]?://.+";

            var regex = new Regex(pattern);

            if (string.IsNullOrWhiteSpace(callbackUrl) || regex.IsMatch(callbackUrl))
            {
                return callbackUrl;
            }

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            var siteUrl = settingsManager.SettingValue(SettingsKeys.SystemWebGeneralBaseUrl);

            if (!regex.IsMatch(siteUrl))
            {
                return callbackUrl;
            }
            
            var ls = $"ls={loginSystem.ToString().ToUpper()}";
            
            return $"{siteUrl.Trim('/', ' ')}/epg/auth/extcallback.php?{ls}&callback={Uri.EscapeDataString(callbackUrl)}";
        }

        public RedirectUrl GetLogoutUrl(RedirectUrl redirectUrl)
        {
            var loginModel = new LoginModel
            {
                LoginSystem = redirectUrl.LoginSystem,
                CallbackUrl = redirectUrl.Url
            };

            var authFabric = (AuthExtApiHelperFabric)_serviceProvider.GetService(typeof(AuthExtApiHelperFabric));

            var apiHelper = authFabric.GetHelper(loginModel.LoginSystem);

            redirectUrl.Url = apiHelper?.LogoutUrl(loginModel);

            return redirectUrl;
        }

        public async Task<LoginResult> LoginAsync(LoginModel model, GraphQlKeyDictionary requestedKeys, HttpRequestParams httpContext = null)
        {
            LoginResult result;

            switch (model.LoginSystem)
            {
                case LoginSystem.Form:
                    result = await LoginByForm(model, requestedKeys, httpContext);
                    break;
                case LoginSystem.Esia:
                case LoginSystem.Vk:
                case LoginSystem.Google:
                case LoginSystem.Facebook:
                    result = await LoginByExtApi(model, requestedKeys, httpContext);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (string.IsNullOrWhiteSpace(model.SubscribeKey))
            {
                return result;
            }

            result.Key = model.SubscribeKey;

            var subscriptionService = _serviceProvider.GetRequiredService<GQLSubscriptionService>();

            subscriptionService.OnAuthorizeChanged(result);

            return result;
        }

        public async Task<LoginResult> UpdateToken(GraphQlKeyDictionary requestedKeys)
        {
            var httpContextAccessor =
                (IHttpContextAccessor)_serviceProvider.GetService(typeof(IHttpContextAccessor));

            var claimUser = httpContextAccessor.HttpContext.User;

            if (!claimUser.Identity.IsAuthenticated)
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = "Для не авторизованного пользователя операция не доступна"
                };
            }

            var usrManager = _serviceProvider.GetRequiredService<UserManager>();

            var filter = new List<GraphQlQueryFilter>
            {
                new GraphQlQueryFilter
                {
                    Name = nameof(User.Id).ToJsonFormat(),
                    Equal = (usrManager.ClaimsPrincipalId() ?? 0).ToString()
                }
            };

            var userKey = nameof(User).ToJsonFormat();

            var userRequestedKeys = requestedKeys.ContainsKey(userKey)
                ? requestedKeys[userKey]
                : null;

            var userQuery = usrManager.GetUsersQuery(filter, userRequestedKeys);

            var user = await userQuery.SingleOrDefaultAsync();

            if (user == null)
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.NotFound,
                    ErrorMessage = "Пользователь не найден"
                };
            }

            if (user.Disconnected)
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = "Пользователь заблокирован"
                };
            }

            var newToken = await GetSecurityTokenAsync(user);

            return new LoginResult
            {
                LoginStatus = LoginStatus.Ok,
                Token = newToken,
                User = user
            };
        }

        private async Task<LoginResult> LoginByForm(LoginModel loginModel, GraphQlKeyDictionary requestedFields, HttpRequestParams httpContext = null)
        {
            var usrManager = _serviceProvider.GetRequiredService<UserManager>();

            var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();

            var normalizeLogin = keyNormalizer.NormalizeName(loginModel.Login);

            var filter = new List<GraphQlQueryFilter>
            {
                new GraphQlQueryFilter
                {
                    Name = nameof(User.NormalizedUserName).ToJsonFormat(),
                    Equal = normalizeLogin
                }
            };

            var needKeys = new GraphQlKeyDictionary
            {
                {nameof(User.Claims).ToJsonFormat(), new GraphQlKeyDictionary() }
            };

            var userQuery = usrManager.GetUsersQuery(filter, needKeys);

            var dbUser = await userQuery
                .SingleOrDefaultAsync();

            if (dbUser == null)
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = "Неверный логин или пароль"
                };
            }

            if (dbUser.LoginSystem != LoginSystem.Form)
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = "Этот пользователь не может быть авторизован"
                };
            }

            if (dbUser.DisconnectedTimestamp != null)
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = "Этот пользователь заблокирован"
                };
            }

            var userManager = (UserManager<User>)_serviceProvider.GetService(typeof(UserManager<User>));

            var valid = await userManager.CheckPasswordAsync(dbUser, loginModel.Password);

            if (!valid)
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = "Неверный логин или пароль"
                };
            }

            if (dbUser.SelfFormRegister)
            {
                var needClaims = GetUserAvailableClaims(dbUser);

                var claimManager = _serviceProvider.GetRequiredService<ClaimManager>();

                var updateClaimsResult = await claimManager.UpdateUserClaims(dbUser, needClaims, true);

                if (!updateClaimsResult.Succeeded)
                {
                    return new LoginResult
                    {
                        LoginStatus = LoginStatus.Unauthorized,
                        ErrorMessage = "Ошибка выдачи разрешений"
                    };
                }

                var context = _serviceProvider.GetRequiredService<DocumentsContext>();

                await context.SaveChangesAsync();
            }


            string token;

            try
            {
                token = await GetSecurityTokenAsync(dbUser, httpContext, loginModel.LoginRequestSource);
            }
            catch (UserException ex)
            {
                _log.Error("Ошибка получения токена", ex);

                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = "Ошибка сервера"
                };
            }

            //var usrManager = _serviceProvider.GetRequiredService<UserManager>();

            filter = new List<GraphQlQueryFilter>
            {
                new GraphQlQueryFilter
                {
                    Name = nameof(User.Id).ToJsonFormat(),
                    Equal =  dbUser.Id.ToString()
                }
            };

            var result = new LoginResult
            {
                LoginStatus = LoginStatus.Ok,
                Token = token
            };

            var userKey = nameof(User).ToJsonFormat();

            if (!requestedFields.ContainsKey(userKey))
            {
                return result;
            }

            userQuery = usrManager.GetUsersQuery(filter, requestedFields[userKey]);

            result.User = await userQuery.SingleOrDefaultAsync();

            return result;
        }

        private async Task<LoginResult> LoginByExtApi(LoginModel loginModel, GraphQlKeyDictionary requestedFields, HttpRequestParams httpContext = null)
        {
            try
            {
                if (loginModel == null)
                {
                    _log.Error($"Ошибка получения кода доступа от сторонней системы. Нет данных.");

                    return new LoginResult
                    {
                        LoginStatus = LoginStatus.Unauthorized,
                        ErrorMessage = "Ошибка получения кода доступа от сторонней системы."
                    };
                }

                if (!CheckLoginModel(loginModel))
                {
                    _log.Error(
                        $"Ошибка получения кода доступа от сторонней системы. Не верные данные :{Environment.NewLine}   LoginSystem={loginModel.LoginSystem};{Environment.NewLine}   Code={loginModel.Code};{Environment.NewLine}   CallbackUrl={loginModel.CallbackUrl};{Environment.NewLine}   RequestCodeError={loginModel.RequestCodeError};");

                    return new LoginResult
                    {
                        LoginStatus = LoginStatus.Unauthorized,
                        ErrorMessage = "Ошибка получения кода доступа от сторонней системы. Не верные данные"
                    };
                }

                if (!string.IsNullOrWhiteSpace(loginModel.RequestCodeError))
                {
                    _log.Error($"Ошибка получения кода доступа от сторонней системы ({loginModel.LoginSystem}) : {loginModel.RequestCodeError}");

                    return new LoginResult
                    {
                        LoginStatus = LoginStatus.Unauthorized,
                        ErrorMessage = "Ошибка получения кода доступа от сторонней системы."
                    };
                }

                var authFabric = _serviceProvider.GetRequiredService<AuthExtApiHelperFabric>();

                var extApiManager = authFabric.GetHelper(loginModel.LoginSystem);

                var newUser = await extApiManager.GetUserAsync(loginModel);

                if (newUser == null)
                {
                    return new LoginResult
                    {
                        LoginStatus = LoginStatus.Unauthorized,
                        ErrorMessage = "Не удалось получить профиль пользователя"
                    };
                }

                var requestedKeys = new GraphQlKeyDictionary
                {
                    {nameof(User.UserName).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.Email).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.EmailConfirmed).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.Phones).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.FirstName).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.MiddleName).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.LastName).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.FullName).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.LoginSystem).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.ProfileConfirmed).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.Gender).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.Inn).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.Snils).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.Addresses).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {nameof(User.Claims).ToJsonFormat(), new GraphQlKeyDictionary()},
                    {
                        nameof(User.Organizations).ToJsonFormat(), new GraphQlKeyDictionary
                        {
                            {nameof(UserOrganization.Parent).ToJsonFormat(), new GraphQlKeyDictionary()},
                            {nameof(UserOrganization.Branches).ToJsonFormat(), new GraphQlKeyDictionary()},
                            {nameof(UserOrganization.Addresses).ToJsonFormat(), new GraphQlKeyDictionary()},
                            {nameof(UserOrganization.Phones).ToJsonFormat(), new GraphQlKeyDictionary()}
                        }
                    },
                    {nameof(User.UserIdentityDocuments).ToJsonFormat(), new GraphQlKeyDictionary()}
                };

                var userMng = _serviceProvider.GetRequiredService<UserManager>();

                var context = _serviceProvider.GetRequiredService<DocumentsContext>();

                var transaction = await context.Database.BeginTransactionAsync();

                User dbUser;

                try
                {
                    var identityResult = IdentityResult.Success;

                    dbUser = await userMng.GetUserAsync(newUser.UserName, requestedKeys);

                    var userManager = (UserManager<User>)_serviceProvider.GetService(typeof(UserManager<User>));

                    if (dbUser == null)
                    {
                        //dbUser = new User();

                        var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

                        if (settingsManager.SettingValue<bool>(SettingsKeys.SystemAuthAdditionalUpdatePrevious))
                        {
                            var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();

                            var normalizeEmail = keyNormalizer.NormalizeEmail(newUser.Email);
                            var normalizeFullName = keyNormalizer.NormalizeName(newUser.FullName);

                            var roleManager = _serviceProvider.GetRequiredService<RoleManager<Role>>();

                            var role = await roleManager.FindByNameAsync("Пользователь из v1.0");

                            if (role != null)
                            {
                                var lastDbUsers = await context.Users
                                    .Join(context.UserRoles,
                                        u => u.Id,
                                        r => r.UserId,
                                        (u, r) => new { User = u, r.RoleId })
                                    .Where(el =>
                                        el.RoleId == role.Id &&
                                        el.User.NormalizedEmail == normalizeEmail &&
                                        el.User.NormalizeFullName == normalizeFullName &&
                                        el.User.SelfFormRegister)
                                    .Select(el=>el.User)
                                    .ToArrayAsync();

                                if (lastDbUsers.Length == 1)
                                {
                                    dbUser = await userMng.GetUserAsync(lastDbUsers[0].UserName, requestedKeys);
                                }
                            }
                        }

                        if (dbUser == null)
                        {
                            dbUser = new User();
                            await userMng.UpdateUserAsync(dbUser, newUser, requestedKeys);
                            identityResult = await userManager.CreateAsync(dbUser);
                        }
                        else
                        {
                            await userMng.UpdateUserAsync(dbUser, newUser, requestedKeys);
                        }
                    }
                    else
                    {
                        await userMng.UpdateUserAsync(dbUser, newUser, requestedKeys);
                    }

                    if (identityResult.Succeeded)
                    {
                        AddNewUserSubsystemClaims(dbUser, extApiManager);

                        await context.SaveChangesAsync();

                        await transaction.CommitAsync();
                    }
                    else
                    {
                        await transaction.RollbackAsync();

                        return new LoginResult
                        {
                            LoginStatus = LoginStatus.Unauthorized,
                            ErrorMessage = "Регистрация пользователя не выполнена"
                        };
                    }
                }
                catch (Exception ex)
                {
                    await transaction.RollbackAsync();

                    _log.Error("Ошибка создания/обновоения пользователя", ex);

                    return new LoginResult
                    {
                        LoginStatus = LoginStatus.Unauthorized,
                        ErrorMessage = "Ошибка сервера"
                    };

                }

                string token;

                try
                {
                    token = await GetSecurityTokenAsync(dbUser, httpContext, loginModel.LoginRequestSource);
                }
                catch (Exception ex)
                {
                    _log.Error("Ошибка получения токена", ex);

                    return new LoginResult
                    {
                        LoginStatus = LoginStatus.Unauthorized,
                        ErrorMessage = "Ошибка сервера"
                    };
                }

                if (string.IsNullOrWhiteSpace(token))
                {
                    _log.Error("Ошибка получения токена (token = null)");

                    return new LoginResult
                    {
                        LoginStatus = LoginStatus.Unauthorized,
                        ErrorMessage = "Регистрация пользователя не выполнена"
                    };
                }

                var filter = new List<GraphQlQueryFilter>
                {
                    new GraphQlQueryFilter
                    {
                        Name = nameof(User.Id).ToJsonFormat(),
                        Equal =  dbUser.Id.ToString()
                    }
                };

                var usrManager = _serviceProvider.GetRequiredService<UserManager>();

                var userQuery = usrManager.GetUsersQuery(filter, requestedFields);

                var user = await userQuery.SingleOrDefaultAsync();

                if (user == null)
                {
                    _log.Error("Ошибка получения пользователя (user = null)");

                    return new LoginResult
                    {
                        LoginStatus = LoginStatus.Unauthorized,
                        ErrorMessage = "Ошибка сервера"
                    };
                }

                _log.Info($"Авторизации через стороннюю систему (UserId={user.Id}). LoginSystem={loginModel.LoginSystem};");

                return new LoginResult
                {
                    LoginStatus = LoginStatus.Ok,
                    Token = token,
                    User = user
                };
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка авторизации через стороннюю систему.{Environment.NewLine}   LoginSystem={loginModel?.LoginSystem};{Environment.NewLine}   Code={loginModel?.Code};{Environment.NewLine}   CallbackUrl={loginModel?.CallbackUrl};   {Environment.NewLine}RequestCodeError={loginModel?.RequestCodeError};",
                    ex);

                throw;
            }
        }

        private void AddNewUserSubsystemClaims(User newUser, AuthExtApiHelper extApiManager = null)
        {
            var claims = GetUserAvailableClaims(newUser, extApiManager);

            newUser.Claims = claims;
        }

        private List<Claim> GetUserAvailableClaims(User newUser, AuthExtApiHelper extApiManager = null)
        {
            var claims = new List<Claim>
            {
                new Claim(
                    ClaimName.LoginSystem,
                    (extApiManager?.LoginSystem ?? LoginSystem.Form)
                    .ToString())
            };

            var subsystemService = _serviceProvider.GetRequiredService<SubsystemService>();

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            if (newUser.LoginSystem == LoginSystem.Form
                && newUser.SelfFormRegister
                && !newUser.EmailConfirmed
                && settingsManager.SettingValue<bool>(SettingsKeys.SystemAuthFormDisableNotConfirmEmail))
            {
                return claims;
            }

            foreach (var subsystem in subsystemService.Subsystems)
            {
                claims.AddRange(subsystem.GetCitizenSubsystemClaims(newUser));
            }

            return claims;
        }


        private bool CheckLoginModel(LoginModel loginModel)
        {
            return loginModel.LoginSystem != LoginSystem.Form
                   && (!string.IsNullOrWhiteSpace(loginModel.RequestCodeError)
                       || (!string.IsNullOrWhiteSpace(loginModel.Code)
                           && !string.IsNullOrWhiteSpace(loginModel.CallbackUrl)
                           && loginModel.LoginRequestSource != LoginRequestSource.Undefined));
        }

        private async Task<string> GetSecurityTokenAsync(User user, HttpRequestParams httpRequestParams = null, LoginRequestSource source = LoginRequestSource.Undefined)
        {
            var secretKey = _symmetricSecurityKey.Value;

            var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            var claims = await GetClaimsAsync(user);

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            int lifeTime;

            if (user.LoginSystem == LoginSystem.Form && !user.SelfFormRegister)
            {
                if (!int.TryParse(settingsManager.SettingValue(SettingsKeys.SystemAuthTokenLifetimeManager), out lifeTime))
                {
                    lifeTime = 10;
                }
            }
            else
            {
                
                if (!int.TryParse(settingsManager.SettingValue(source == LoginRequestSource.Mobile ? SettingsKeys.SystemAuthTokenLifetimeCitizenMobile : SettingsKeys.SystemAuthTokenLifetimeCitizen), out lifeTime))
                {
                    lifeTime = source == LoginRequestSource.Mobile ? 10080 : 60;
                }
            }

            if (httpRequestParams == null)
            {
                var httpContextAccessor = _serviceProvider.GetRequiredService<IHttpContextAccessor>();

                var httpContext = httpContextAccessor.HttpContext;

                httpRequestParams = new HttpRequestParams
                {
                    RequestScheme = httpContext.Request.Scheme,
                    RequestHost = httpContext.Request.Host.Host,
                    RemoteIpAddress = httpContext.Connection.RemoteIpAddress.ToString()
                };
            }

            var utcNow = DateTime.UtcNow;

            var tokenOptions = new JwtSecurityToken(
                issuer: $"{httpRequestParams.RequestScheme}://{httpRequestParams.RequestHost}",
                audience: httpRequestParams.RemoteIpAddress,
                claims: claims,
                notBefore: utcNow,
                expires: utcNow.AddMinutes(lifeTime),
                signingCredentials: signinCredentials);

            return $"Bearer {new JwtSecurityTokenHandler().WriteToken(tokenOptions)}";
        }

        private async Task<List<Claim>> GetClaimsAsync(User user)
        {
            var userManager = (UserManager<User>)_serviceProvider.GetService(typeof(UserManager<User>));

            var roleManager = (RoleManager<Role>)_serviceProvider.GetService(typeof(RoleManager<Role>));

            var claims = (await userManager.GetClaimsAsync(user))
              .ToList();

            var roleNames = await userManager.GetRolesAsync(user);

            foreach (var roleName in roleNames)
            {
                var role = await roleManager.FindByNameAsync(roleName);

                if (role.Disconnected)
                {
                    continue;
                }

                claims.AddRange(await roleManager.GetClaimsAsync(role));
            }

            claims.AddRange(new []
            {
                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString()),
                new Claim(ClaimName.Сonfirmed, user.ProfileConfirmed ? "true" : "false")
            });

            if (!claims.Exists(el => el.Type == ClaimName.LoginSystem))
            {
                claims.Add(new Claim(ClaimName.LoginSystem, user.LoginSystem.ToString()));
            }

            return claims;
        }

        public async Task<OperationResult> ConfirmEmail(ConfirmEmailModel confirmEmailModel)
        {
            var userManager = (UserManager<User>)_serviceProvider.GetService(typeof(UserManager<User>));

            var user = await userManager.FindByIdAsync(confirmEmailModel.UserId.ToString());

            if (user == null || user.Email.GetHashCode() != confirmEmailModel.Sh)
            {
                return new OperationResult("Ошибка подтверждения Email");
            }

            var result = await userManager.ConfirmEmailAsync(user, confirmEmailModel.Code);

            if (!result.Succeeded)
            {
                return new OperationResult(string.Join(Environment.NewLine,
                    result.Errors.Select(el => el.Description)));
            }

            user.ConfirmEmailMailId = null;

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            await context.SaveChangesAsync();

            return new OperationResult();
        }

        public async Task<OperationResult> ResetPassword(ResetPasswordModel model)
        {
            var httpContextAccessor = (IHttpContextAccessor)_serviceProvider.GetService(typeof(IHttpContextAccessor));

            var principal = httpContextAccessor.HttpContext.User;

            var result = model.Id.HasValue
              ? await ResetAnotherUserPassword(model, principal)
              : await ResetSelfPassword(model, principal);

            return result;
        }

        private async Task<OperationResult> ResetSelfPassword(ResetPasswordModel model, ClaimsPrincipal principal)
        {
            if (string.IsNullOrWhiteSpace(model.OldPassword)
                || string.IsNullOrWhiteSpace(model.NewPassword)
                || string.IsNullOrWhiteSpace(model.ConfirmPassword))
            {
                return new OperationResult("Переданы не все данные");
            }

            if (!principal.HasClaim(ClaimName.LoginSystem, LoginSystem.Form.ToString()))
            {
                return new OperationResult("Для данного пользователя смена пароля невозможна");
            }

            if (model.NewPassword != model.ConfirmPassword)
            {
                return new OperationResult("Не верное подтверждение нового пароля");
            }

            var userManager = (UserManager<User>)_serviceProvider.GetService(typeof(UserManager<User>));

            var user = await userManager.FindByIdAsync(principal.Claims.First(el => el.Type == JwtRegisteredClaimNames.Jti).Value);

            var checkPasswordResult = await userManager.CheckPasswordAsync(user, model.OldPassword);

            if (!checkPasswordResult)
            {
                return new OperationResult("Ошибка смены пароля. Не верный старый пароль.");
            }

            var identityResult = await userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);

            return identityResult.Succeeded
              ? new OperationResult()
              : new OperationResult(string.Join(Environment.NewLine, identityResult.Errors.Select(el => el.Description)));
        }

        private async Task<OperationResult> ResetAnotherUserPassword(ResetPasswordModel model, ClaimsPrincipal principal)
        {
            if (string.IsNullOrWhiteSpace(model.NewPassword))
            {
                return new OperationResult("Переданы не все данные");
            }

            if (!principal.HasClaim(ClaimName.Manage, "users"))
            {
                return new OperationResult("Отказ в выполнении операции");
            }

            var userManager = (UserManager<User>)_serviceProvider.GetService(typeof(UserManager<User>));

            var user = model.Id.HasValue
                    ? await userManager.FindByIdAsync(model.Id.Value.ToString())
                    : null;

            if (user == null)
            {
                return new OperationResult("Пользователь не найден");
            }

            if (user.LoginSystem != LoginSystem.Form || user.SelfFormRegister)
            {
                return new OperationResult("Для данного пользователя смена пароля невозможна");
            }

            var usrManager = _serviceProvider.GetRequiredService<UserManager>();

            if (await usrManager.HasClaimAsync(user, ClaimName.SuperAdmin) && user.Id != usrManager.ClaimsPrincipalId())
            {
                return new OperationResult("Вы не имеете права изменить пароль для данного пользователя");
            }

            var context = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));

            var transaction = await context.Database.BeginTransactionAsync();

            try
            {
                var identityResult = await userManager.RemovePasswordAsync(user);

                if (identityResult.Succeeded)
                {
                    identityResult = await userManager.AddPasswordAsync(user, model.NewPassword);
                }

                if (identityResult.Succeeded)
                {
                    await transaction.CommitAsync();
                }
                else
                {
                    await transaction.RollbackAsync();
                }

                return identityResult.Succeeded
                  ? new OperationResult()
                  : new OperationResult(string.Join(Environment.NewLine, identityResult.Errors.Select(el => el.Description)));
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка смены пароля", ex);

                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                return new OperationResult("Ошибка смены пароля");
            }
        }

        public async Task<LoginResult> RegisterUserAsync(RegisterUserModel model, GraphQlKeyDictionary updateKeys,
            GraphQlKeyDictionary requestedKeys)
        {
            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            if (settingsManager.SettingValue(SettingsKeys.SystemAuthFormEnabled).ToLower() != "true")
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = "Авторизация через формы отключена"
                };
            }

            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            var principal = userManager.CurrentClaimsPrincipal();

            if (principal.Identity.IsAuthenticated)
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = "Регистрироваться могут только не авторизованные пользователи"
                };
            }

            var checkResult = (await CheckRegisterModel(model))
                .ToList();

            if (checkResult.Any())
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = string.Join("; ", checkResult)
                };
            }

            var dbUser = new User();

            await userManager.UpdateUserAsync(dbUser, model, updateKeys);

            dbUser.LoginSystem = LoginSystem.Form;
            dbUser.SelfFormRegister = true;

            AddNewUserSubsystemClaims(dbUser);

            var usrManager = _serviceProvider.GetRequiredService<UserManager<User>>();

            var identityResult = await usrManager.CreateAsync(dbUser, model.Password);

            if (!identityResult.Succeeded)
            {
                return new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized,
                    ErrorMessage = identityResult.ErrorToString("; ")
                };
            }

            var emailId = await userManager.SendConfirmEmailMessage(dbUser);

            dbUser.ConfirmEmailMailId = emailId;

            await usrManager.UpdateAsync(dbUser);

            LoginResult result;


            if (settingsManager.SettingValue<bool>(SettingsKeys.SystemAuthFormAuthAfterRegistration))
            {
                try
                {
                    var token = await GetSecurityTokenAsync(dbUser);

                    result = new LoginResult
                    {
                        LoginStatus = LoginStatus.Ok,
                        Token = token
                    };
                }
                catch (UserException ex)
                {
                    _log.Error("Ошибка получения токена", ex);

                    return new LoginResult
                    {
                        LoginStatus = LoginStatus.Unauthorized,
                        ErrorMessage = "Ошибка сервера"
                    };
                }
            }
            else
            {
                result = new LoginResult
                {
                    LoginStatus = LoginStatus.Unauthorized
                };
            }

            if (!requestedKeys.ContainsKey(nameof(User).ToJsonFormat()))
            {
                return result;
            }

            var filter = new List<GraphQlQueryFilter>
            {
                new GraphQlQueryFilter
                {
                    Name = nameof(User.Id).ToJsonFormat(),
                    Equal = dbUser.Id.ToString()
                }
            };

            var requestedFields = requestedKeys[nameof(User).ToJsonFormat()];

            var userQuery = userManager.GetUsersQuery(filter, requestedFields ?? new GraphQlKeyDictionary());

            result.User = await userQuery.SingleOrDefaultAsync();

            return result;
        }

        private async Task<IEnumerable<string>> CheckRegisterModel(RegisterUserModel model)
        {
            var result = new List<string>();

            var usrManager = _serviceProvider.GetRequiredService<UserManager>();

            if (string.IsNullOrWhiteSpace(model.UserName))
            {
                result.Add("Не указан логин пользователя");
            }
            else if (await usrManager.HasUserWithName(model.UserName))
            {
                result.Add("Пользователь с таким логином уже зарегистрирован");
            }


            if (string.IsNullOrWhiteSpace(model.Email))
            {
                result.Add("Отсутствует адрес электронной почты");
            }
            else
            {
                if (!EmailUtility.CheckEmail(model.Email))
                {
                    result.Add("Не верный формат адреса электронной почты");
                }
                else
                {
                    if (await usrManager.HasFormCitizenWithEmail(model.Email))
                    {
                        result.Add("Пользователь с таким адресом электронной почты уже зарегистрирован");
                    }
                }
            }

            result.AddRange(await usrManager.ValidatePassword(model.Password));

            if (model.Password != model.ConfirmPassword)
            {
                result.Add("Не верно введен пароль подтверждения");
            }

            if (string.IsNullOrWhiteSpace(model.FullName))
            {
                result.Add("Не указано полное имя пользователя");
            }

            if (!model.Gender.HasValue)
            {
                result.Add("Не указан пол пользователя");
            }

            return result;
        }

        public async Task<OperationResult> ForgotPasswordAsync(ForgotPasswordModel model)
        {
            if (model == null)
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "Отсутствуют данные для восстановления пароля"
                    }
                };
            }

            var userManager = _serviceProvider.GetRequiredService<UserManager<User>>();

            var usrManager = _serviceProvider.GetRequiredService<UserManager>();

            User user = null;

            if (!string.IsNullOrWhiteSpace(model.Login))
            {
                user = await userManager.FindByNameAsync(model.Login);

                if (!string.IsNullOrWhiteSpace(model.Email))
                {
                    var keyNormalizer = _serviceProvider.GetRequiredService<ILookupNormalizer>();

                    var normalizeEmail = keyNormalizer.NormalizeEmail(model.Email);

                    if (user.NormalizedEmail != normalizeEmail)
                    {
                        user = null;
                    }
                }
            }

            if (user == null && string.IsNullOrWhiteSpace(model.Login) && !string.IsNullOrWhiteSpace(model.Email))
            {
                user = await usrManager.FindSelfFormUserByEmail(model.Email);
            }

            if (user == null || user.Disconnected)
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "Пользователь не найден"
                    }
                };
            }

            if (user.LoginSystem != LoginSystem.Form || !user.SelfFormRegister)
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "Вы не можете сбросить пароль таким способом. Обратитесь к администратору системы."
                    }
                };
            }

            if (string.IsNullOrWhiteSpace(user.Email))
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "У Вас отсутствует адрес электронной почты"
                    }
                };
            }

            var mailService = _serviceProvider.GetRequiredService<MailService>();

            if (user.RestorePasswordMailId.HasValue)
            {
                var message = await mailService.GetMessageByIdAsync(user.RestorePasswordMailId.Value);

                if (message != null)
                {
                    var sendMinutes = (DateTime.Now - message.CreatedAt.ToLocalTime()).TotalMinutes;

                    if (sendMinutes < 60)
                    {
                        var nextMessageMinutes = 60 - (int)sendMinutes + 1;

                        return new OperationResult($"Сообщение сброса пароля уже отправлено. Повторное сообщение можно отправить через {nextMessageMinutes} минут.");
                    }
                }
            }

            var code = await userManager.GeneratePasswordResetTokenAsync(user);

            var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

            var confirmUri = GenerateForgotPasswordUrl(user, settingsManager.SettingValue(SettingsKeys.SystemAuthFormResetPasswordUrl), code);

            if (confirmUri == null)
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "Не валидный адрес страницы восстановления пароля"
                    }
                };
            }

            const string subject = "Восстановление пароля";

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            var emailId = await mailService.SendEmailAsync(
                new MailMessage
                {
                    Email = user.Email,
                    Subject = subject,
                    TemplateKey = "restorePassword",
                    AddingEntity = new List<EmailEntityMetadata>
                    {
                        EmailEntityMetadata.Generate(context, user)
                    },
                    TemplateModel = new
                    {
                        ConfirmUrl = confirmUri
                    }
                });

            if (emailId == 0)
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[] {"Ошибка отправки сообщения электронной почты"}
                };
            }

            user.RestorePasswordMailId = emailId;

            await context.SaveChangesAsync();

            return new OperationResult
            {
                Success = true
            };
        }

        private string GenerateForgotPasswordUrl(User user, string baseUrl, string code)
        {
            if (!Uri.IsWellFormedUriString(baseUrl, UriKind.Absolute))
            {
                _log.Error($"Для подтверждения Email передан не валидный базовый адрес \"{baseUrl}\"");

                return null;
            }

            var uri = new Uri(baseUrl, UriKind.Absolute);

            var query = QueryHelpers.ParseQuery(uri.Query);

            var items = query
                .SelectMany(x => x.Value,
                    (col, value) => new KeyValuePair<string, string>(col.Key, value))
                .ToList();


            var qb = new QueryBuilder(items);

            var baseUri = uri.GetComponents(UriComponents.Scheme | UriComponents.Host | UriComponents.Port | UriComponents.Path,
                UriFormat.UriEscaped);

            qb.Add("UserId", user.Id.ToString());
            qb.Add("Code", code);

            var result = baseUri + qb.ToQueryString();

            return result;
        }

        public async Task<OperationResult> RestorePasswordAsync(RestorePasswordModel model)
        {
            if (model == null)
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "Нет данных восстановления пароля"
                    }
                };
            }

            if (model.UserId <= 0)
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "Пользователь не найден"
                    }
                };
            }

            if (string.IsNullOrWhiteSpace(model.Code))
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "Отсутствует код восстановления"
                    }
                };
            }

            if (string.IsNullOrWhiteSpace(model.Password))
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "Пароль не может быть пустым"
                    }
                };
            }

            var usrManager = _serviceProvider.GetRequiredService<UserManager>();

            var validatePasswordResult = (await usrManager.ValidatePassword(model.Password))
                .ToArray();

            if (validatePasswordResult.Any())
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        $"Не валидный пароль ({string.Join("; ", validatePasswordResult)})"
                    }
                };
            }

            if (model.Password != model.ConfirmPassword)
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "Не верно введено подтверждение пароля"
                    }
                };
            }

            var userManager = _serviceProvider.GetRequiredService<UserManager<User>>();

            var user = await userManager.FindByIdAsync(model.UserId.ToString());

            if (user == null)
            {
                return new OperationResult
                {
                    Success = false,
                    ErrorMessages = new[]
                    {
                        "Пользователь не найден"
                    }
                };
            }

            var result = await userManager.ResetPasswordAsync(user, model.Code, model.Password);

            return new OperationResult
            {
                Success = result.Succeeded,
                ErrorMessages = result.Succeeded
                    ? null
                    : result.Errors?
                        .Select(el => el.Description)
                        .ToArray()
            };
        }

        public async Task<OperationResult> SendEmailConfirmAsync(long? userId)
        {
            User user;

            var userManager = _serviceProvider.GetRequiredService<UserManager>();

            var currentUserId = userManager.ClaimsPrincipalId();

            if (userId.HasValue)
            {
                if (userId != currentUserId)
                {
                    if (!userManager.HasClaim(ClaimName.Manage, "users"))
                    {
                        return new OperationResult("У Вас нет прав на выполнение этой операции");
                    }

                    user = await userManager.GetUserByIdAsync(userId.Value);
                }
                else
                {
                    user = await userManager.CurrentUser();
                }
            }
            else
            {
                user = await userManager.CurrentUser();
            }

            if (user == null)
            {
                return new OperationResult("Пользователь не найден");
            }

            if (user.LoginSystem != LoginSystem.Form)
            {
                return new OperationResult("Для данного пользователя подтверждение электронной почты недоступно");
            }

            if (string.IsNullOrEmpty(user.Email))
            {
                return new OperationResult("Отсутствует адрес электронной почты");
            }

            if (user.EmailConfirmed)
            {
                return new OperationResult("Адрес электронной почты уже подтвержден");
            }

            if (user.ConfirmEmailMailId.HasValue)
            {
                var emailService = _serviceProvider.GetRequiredService<MailService>();

                var message = await emailService.GetMessageByIdAsync(user.ConfirmEmailMailId.Value);

                if (message != null)
                {
                    var sendMinutes = (DateTime.Now - message.CreatedAt.ToLocalTime()).TotalMinutes;

                    if (sendMinutes < 60)
                    {
                        var nextMessageMinutes = 60 - (int)sendMinutes + 1;

                        return new OperationResult($"Сообщение подтверждения адреса электронной почты уже отправлено. Повторное сообщение можно отправить через {nextMessageMinutes} минут.");
                    }
                }
            }

            var emailId = await userManager.SendConfirmEmailMessage(user);

            if (!emailId.HasValue)
            {
                return new OperationResult("Ошибка отправки сообщения подтверждения адреса электронной почты");
            }

            user.ConfirmEmailMailId = emailId;

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            await context.SaveChangesAsync();

            return new OperationResult();
        }
    }
}
