using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization.ExtApiHelpers;

namespace BIT.MyCity.Authorization.AuthSubsystems
{
    public interface IAuthSubsystem
    {
        LoginSystem LoginSystem { get; }

        bool Enable { get; }

        AuthExtApiHelper Helper { get; }
    }
}
