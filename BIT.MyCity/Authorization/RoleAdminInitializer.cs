using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Services;
using GraphQL.Utilities;
using log4net;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.MyCity.Extensions;
using Microsoft.EntityFrameworkCore.DynamicLinq;
using Microsoft.EntityFrameworkCore;
using BIT.MyCity.Subsystems;

namespace BIT.MyCity.Authorization
{
    public class RoleAdminInitializer
    {
        private readonly ILog Log = LogManager.GetLogger(typeof(RoleAdminInitializer));

        private readonly IServiceProvider _serviceProvider;

        public RoleAdminInitializer(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<bool> Initialize()
        {
            IDbContextTransaction transaction = null;

            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            try
            {
                transaction = await context.Database.BeginTransactionAsync();

                var identityResult = await AddSuperAdminAsync();
                if (identityResult.Succeeded)
                {
                    identityResult = await AddFeedBackUserAsync();
                }
                if (identityResult.Succeeded)
                {
                    identityResult = await AddAnonymousAppealUserAsync();
                }
                
                if (identityResult.Succeeded)
                {
                    identityResult = await AddRolesAsync();
                }

                if (identityResult.Succeeded)
                {
                    var settingsManager = _serviceProvider.GetRequiredService<SettingsManager>();

                    var opResult = await settingsManager.SetDefaultValues();

                    if (!opResult)
                    {
                        identityResult = IdentityResult.Failed(new IdentityError
                        {
                            Description = "Ошибка установки начальных значений параметров приложения",
                            Code = "SetDefaultSettings"
                        });
                    }
                }

                if (identityResult.Succeeded)
                {
                    var ssm = _serviceProvider.GetRequiredService<SubsystemManager>();

                    var opResult = await ssm.SetDefaultValues();

                    if (!opResult)
                    {
                        identityResult = IdentityResult.Failed(new IdentityError
                        {
                            Description = "Ошибка установки начальных значений таблицы подсистем",
                            Code = "SetDefaultSettings"
                        });
                    }
                }

                if (identityResult.Succeeded)
                {
                    var ssm = _serviceProvider.GetRequiredService<SubsystemManager>();

                    var opResult = await ssm.SetDefaultMetaData();

                    if (!opResult)
                    {
                        identityResult = IdentityResult.Failed(new IdentityError
                        {
                            Description = "Ошибка установки начальных значений метаданных подсистем",
                            Code = "SetDefaultMetaData"
                        });
                    }
                }

                if (identityResult.Succeeded)
                {
                    var ssm = _serviceProvider.GetRequiredService<EnumVerbManager>();

                    var opResult = await ssm.SetDefaultValues();

                    if (!opResult)
                    {
                        identityResult = IdentityResult.Failed(new IdentityError
                        {
                            Description = "Ошибка установки начальных значений таблицы EnumVerb",
                            Code = "SetDefaultSettings"
                        });
                    }
                }

                if (identityResult.Succeeded)
                {
                    var mailService = _serviceProvider.GetRequiredService<MailService>();

                    var opResult = await mailService.InitializeDbAsync();

                    if (!opResult)
                    {
                        identityResult = IdentityResult.Failed(new IdentityError
                        {
                            Description = "Ошибка инициализации значений EmailTemplate",
                            Code = "SetDefaultSettings"
                        });
                    }
                }

                if (identityResult.Succeeded)
                {
                    await transaction.CommitAsync();
                }
                else
                {
                    await transaction.RollbackAsync();
                }

                return identityResult.Succeeded;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка начальной инициализации БД", ex);

                if (transaction != null)
                {
                    await transaction.RollbackAsync();
                }

                throw;
            }

        }

        private async Task<IdentityResult> AddRolesAsync()
        {
            var keys = new GraphQlKeyDictionary
            {
                {nameof(Role.Claims).ToJsonFormat(), new GraphQlKeyDictionary() }
            };

            WebApi.Types.OperationResult<Role> result = null;

            var roleManager = _serviceProvider.GetRequiredService<RoleManager>();

            var roleName = "Администратор";

            if (!await roleManager.HasRoleWidthName(roleName))
            {
                var role = new Role
                {
                    Name = roleName,
                    IsAdminRegister = false,
                    Editable = false,
                    Claims = new List<Claim>
                    {
                        new Claim(ClaimName.Manage, "users")
                    }
                };

                result = await roleManager.AddRoleAsync(role, keys, null);
            }

            if (!(result?.Success ?? true))
            {
                return IdentityResult.Failed(new IdentityError
                {
                    Description = $"Ошибка создания роли <{roleName}>"
                });
            }

            //roleName = "Главный модератор";

            //if (!await roleManager.HasRoleWidthName(roleName))
            //{
            //    var role = new Role
            //    {
            //        Name = roleName,
            //        IsAdminRegister = false,
            //        Editable = false,
            //        Claims = new List<Claim>
            //        {
            //            new Claim(ClaimName.Moderate, "organizations.enable"),
            //            new Claim(ClaimName.Moderate, "organizations.super"),
            //            new Claim(ClaimName.Moderate, "organizations.all_rubrics"),
            //            new Claim(ClaimName.Moderate, "organizations.all_territories"),
            //            new Claim(ClaimName.Moderate, "organizations.all_regions"),
            //            new Claim(ClaimName.Moderate, "organizations.all_topics"),

            //            new Claim(ClaimName.Manage, "organizations.territories"),
            //            new Claim(ClaimName.Manage, "organizations.regions"),
            //            new Claim(ClaimName.Manage, "organizations.topics"),
            //            new Claim(ClaimName.Manage, "organizations.rubrics"),
            //            new Claim(ClaimName.Manage, "rubrics"),
            //            new Claim(ClaimName.Manage, "territories"),
            //            new Claim(ClaimName.Manage, "regions"),
            //            new Claim(ClaimName.Manage, "topics"),

            //            new Claim(ClaimName.Subsystem, "organizations.available")
            //        }
            //    };

            //    result = await roleManager.AddRoleAsync(role, keys, null);
            //}


            //if (!(result?.Success ?? true))
            //{
            //    return IdentityResult.Failed(new IdentityError
            //    {
            //        Description = $"Ошибка создания роли <{roleName}>"
            //    });
            //}

            //roleName = "Модератор";

            //if (!await roleManager.HasRoleWidthName(roleName))
            //{
            //    var role = new Role
            //    {
            //        Name = roleName,
            //        IsAdminRegister = false,
            //        Editable = false,
            //        Claims = new List<Claim>
            //        {
            //            new Claim(ClaimName.Subsystem, "organizations.available"),
            //            new Claim(ClaimName.Moderate, "organizations.enable"),
            //            new Claim(ClaimName.Moderate, "organizations.all_territories"),
            //            new Claim(ClaimName.Moderate, "organizations.all_regions"),
            //            new Claim(ClaimName.Moderate, "organizations.all_topics")
            //        }
            //    };

            //    result = await roleManager.AddRoleAsync(role, keys, null);
            //}

            //if (!(result?.Success ?? true))
            //{
            //    return IdentityResult.Failed(new IdentityError
            //    {
            //        Description = $"Ошибка создания роли <{roleName}>"
            //    });
            //}

            //roleName = "Главный модератор ОГ";

            //if (!await roleManager.HasRoleWidthName(roleName))
            //{
            //    var role = new Role
            //    {
            //        Name = roleName,
            //        IsAdminRegister = false,
            //        Editable = false,
            //        Claims = new List<Claim>
            //        {
            //            new Claim(ClaimName.Moderate, "citizens.enable"),
            //            new Claim(ClaimName.Moderate, "citizens.super"),
            //            new Claim(ClaimName.Moderate, "citizens.all_rubrics"),
            //            // new Claim(ClaimName.Moderate, "citizens.all_territories"),
            //            // new Claim(ClaimName.Moderate, "citizens.all_regions"),
            //            new Claim(ClaimName.Moderate, "citizens.all_topics"),

            //            // new Claim(ClaimName.Manage, "citizens.territories"),
            //            // new Claim(ClaimName.Manage, "citizens.regions"),
            //            new Claim(ClaimName.Manage, "citizens.topics"),
            //            new Claim(ClaimName.Manage, "citizens.rubrics"),
            //            new Claim(ClaimName.Manage, "rubrics"),
            //            // new Claim(ClaimName.Manage, "territories"),
            //            // new Claim(ClaimName.Manage, "regions"),
            //            new Claim(ClaimName.Manage, "topics"),
            //            new Claim(ClaimName.Subsystem, "citizens.available"),
            //            new Claim(ClaimName.Subsystem, "citizens.create_appeal"),
            //            new Claim(ClaimName.Subsystem, "citizens.create_message"),
            //        }
            //    };

            //    result = await roleManager.AddRoleAsync(role, keys, null);
            //}


            //if (!(result?.Success ?? true))
            //{
            //    return IdentityResult.Failed(new IdentityError
            //    {
            //        Description = $"Ошибка создания роли <{roleName}>"
            //    });
            //}

            //roleName = "Модератор ОГ";

            //if (!await roleManager.HasRoleWidthName(roleName))
            //{
            //    var role = new Role
            //    {
            //        Name = roleName,
            //        IsAdminRegister = false,
            //        Editable = false,
            //        Claims = new List<Claim>
            //        {
            //            new Claim(ClaimName.Subsystem, "citizens.available"),
            //            new Claim(ClaimName.Subsystem, "citizens.create_message"),
            //            new Claim(ClaimName.Moderate, "citizens.enable"),
            //            new Claim(ClaimName.Moderate, "citizens.all_territories"),
            //            new Claim(ClaimName.Moderate, "citizens.all_regions"),
            //            new Claim(ClaimName.Moderate, "citizens.all_topics")
            //        }
            //    };

            //    result = await roleManager.AddRoleAsync(role, keys, null);
            //}

            //if (!(result?.Success ?? true))
            //{
            //    return IdentityResult.Failed(new IdentityError
            //    {
            //        Description = $"Ошибка создания роли <{roleName}>"
            //    });
            //}


            roleName = "Администратор сообщений обратной связи";

            if (!await roleManager.HasRoleWidthName(roleName))
            {
                var role = new Role
                {
                    Name = roleName,
                    IsAdminRegister = false,
                    Editable = false,
                    Claims = new List<Claim>
                    {
                        new Claim(ClaimName.Subsystem, "citizens.available"),
                        new Claim(ClaimName.Subsystem, "citizens.fbadm"),
                    }
                };

                result = await roleManager.AddRoleAsync(role, keys, null);
            }

            if (!(result?.Success ?? true))
            {
                return IdentityResult.Failed(new IdentityError
                {
                    Description = $"Ошибка создания роли <{roleName}>"
                });
            }

            //roleName = "Редактор новостей";

            //if (!await roleManager.HasRoleWidthName(roleName))
            //{
            //    var role = new Role
            //    {
            //        Name = roleName,
            //        IsAdminRegister = false,
            //        Editable = false,
            //        Claims = new List<Claim>
            //        {
            //            new Claim(ClaimName.Subsystem, "news.available"),
            //            new Claim(ClaimName.Moderate, "news.create_news"),
            //            new Claim(ClaimName.Moderate, "news.all_territories"),
            //            new Claim(ClaimName.Moderate, "news.all_regions"),
            //            new Claim(ClaimName.Moderate, "news.all_topics")
            //        }
            //    };

            //    result = await roleManager.AddRoleAsync(role, keys, null);
            //}

            //if (!(result?.Success ?? true))
            //{
            //    return IdentityResult.Failed(new IdentityError
            //    {
            //        Description = $"Ошибка создания роли <{roleName}>"
            //    });
            //}

            roleName = "Дело клиент";

            if (!await roleManager.HasRoleWidthName(roleName))
            {
                var role = new Role
                {
                    Name = roleName,
                    IsAdminRegister = false,
                    Editable = false,
                    Claims = new List<Claim>
                    {
                        new Claim(ClaimName.Manage, "rubrics"),
                        new Claim(ClaimName.Manage, "territories"),
                        new Claim(ClaimName.Manage, "regions"),
                        new Claim(ClaimName.Manage, "topics"),
                        new Claim(ClaimName.Manage, "topics"),
                        new Claim(ClaimName.Manage, "organizations.territories"),
                        new Claim(ClaimName.Manage, "organizations.regions"),
                        new Claim(ClaimName.Manage, "organizations.topics"),
                        new Claim(ClaimName.Manage, "organizations.rubrics"),

                        new Claim(ClaimName.Subsystem, "organizations.available"),
                        new Claim(ClaimName.Subsystem, "organizations.create_message"),

                        new Claim(ClaimName.Moderate, "organizations.enable"),
                        new Claim(ClaimName.Moderate, "organizations.super"),
                        new Claim(ClaimName.Moderate, "organizations.all_rubrics"),
                        new Claim(ClaimName.Moderate, "organizations.all_territories"),
                        new Claim(ClaimName.Moderate, "organizations.all_regions"),
                        new Claim(ClaimName.Moderate, "organizations.all_topics")
                    }
                };

                result = await roleManager.AddRoleAsync(role, keys, null);
            }

            return (result?.Success ?? true)
                ? IdentityResult.Success
                : IdentityResult.Failed(new IdentityError
                {
                    Description = $"Ошибка создания роли <{roleName}>"
                });
        }

        private async Task<IdentityResult> AddSuperAdminAsync()
        {
            var usrManager = _serviceProvider.GetRequiredService<UserManager>();

            var userManager = _serviceProvider.GetRequiredService<UserManager<User>>();

            var superAdmin = (await usrManager.GetUsersByClaim(ClaimName.SuperAdmin))
                .SingleOrDefault();

            if (superAdmin != null)
            {
                return IdentityResult.Success;
            }

            superAdmin = new User
            {
                UserName = "Администратор",
                LoginSystem = LoginSystem.Form,
                FullName = "Главный администратор"
            };

            var result = await userManager.CreateAsync(superAdmin, "Qwerty123~");

            if (!result.Succeeded)
            {
                return result;
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimName.SuperAdmin, string.Empty),
                new Claim(ClaimName.Manage, "users"),
                new Claim(ClaimName.Manage, "settings"),
                new Claim(ClaimName.LoginSystem, LoginSystem.Form.ToString())
            };

            //claims.AddRange(DefaultRoleName.Claims(DefaultRoleName.Administrator));

            result = await userManager.AddClaimsAsync(superAdmin, claims);

            return result;
        }

        private async Task<IdentityResult> AddFeedBackUserAsync()
        {
            const string userName = "$feedBackUser$";
            const string userPass = "qQaA1234rRfdsF%!";
            var ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            var userManager = _serviceProvider.GetRequiredService<UserManager<User>>();
            var feedBackUser = ctx.Users.ByName(userName).FirstOrDefault();
            if (feedBackUser != null)
            {
                return IdentityResult.Success;
            }
            feedBackUser = new User
            {
                UserName = userName,
            };
            var result = await userManager.CreateAsync(feedBackUser, userPass);
            return result;
        }
        private async Task<IdentityResult> AddAnonymousAppealUserAsync()
        {
            const string userName = "$anonymousAppealUser$";
            const string userPass = "qQaA1234rRfdsF%!";
            var ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            var userManager = _serviceProvider.GetRequiredService<UserManager<User>>();
            var feedBackUser = ctx.Users.ByName(userName).FirstOrDefault();
            if (feedBackUser != null)
            {
                return IdentityResult.Success;
            }
            feedBackUser = new User
            {
                UserName = userName,
                Claims = new List<Claim> 
                { 
                    new Claim(ClaimName.Subsystem, $"{SSUID.citizens}.{SSClaimSuffix.Ss.available}"), 
                    new Claim(ClaimName.Subsystem, $"{SSUID.organizations}.{SSClaimSuffix.Ss.available}"),
                    new Claim(ClaimName.Subsystem, $"{SSUID.citizens}.{SSClaimSuffix.Ss.create_appeal}"),
                    new Claim(ClaimName.Subsystem, $"{SSUID.organizations}.{SSClaimSuffix.Ss.create_appeal}")
                }
            };
            var result = await userManager.CreateAsync(feedBackUser, userPass);
            return result;
        }
        //private async Task<IdentityResult> AddDeloService()
        //{
        //    var userManager = _serviceProvider.GetRequiredService<UserManager<User>>();

        //    var user = (await userManager.GetUsersInRoleAsync(DefaultRoleName.DeloService))
        //        .SingleOrDefault();

        //    var hasInRole = user != null;

        //    if (hasInRole)
        //    {
        //        return IdentityResult.Success;
        //    }

        //    user = await userManager.FindByNameAsync("DeloService")
        //    ?? new User
        //    {
        //        UserName = "DeloService",
        //        LoginSystem = LoginSystem.Form,
        //        FullName = "Сервис связи с системой \"Дело\""
        //    };

        //    IdentityResult result;

        //    if (user.Id == 0)
        //    {
        //        result = await userManager.CreateAsync(user, "Qwerty123~");

        //        if (!result.Succeeded)
        //        {
        //            return result;
        //        }
        //    }

        //    result = await userManager.AddToRoleAsync(user, DefaultRoleName.DeloService);

        //    return result;
        //}

        //private async Task<IdentityResult> AddRoles()
        //{
        //    var result = IdentityResult.Success;

        //    var roleManager = (RoleManager<Role>)_serviceProvider.GetService(typeof(RoleManager<Role>));

        //    var hasRoles = await roleManager.Roles.ToArrayAsync();

        //    var needRoles = DefaultRoleName.AllRoles();

        //    foreach (var role in needRoles)
        //    {
        //        Role claimRole;
        //        bool createdNow;

        //        if (hasRoles.Any() && role.Editable)
        //        {
        //            continue;
        //        }

        //        if (hasRoles.All(el => el.Name != role.Name))
        //        {
        //            result = await roleManager.CreateAsync(role);

        //            if (!result.Succeeded)
        //            {
        //                return result;
        //            }

        //            claimRole = role;

        //            createdNow = true;
        //        }
        //        else
        //        {
        //            claimRole = hasRoles.First(el => el.Name == role.Name);

        //            createdNow = false;
        //        }

        //        var claimManager = (ClaimManager)_serviceProvider.GetService(typeof(ClaimManager));

        //        if (!role.Editable || createdNow)
        //        {
        //            result = await claimManager.UpdateRoleClaimsAsync(claimRole, DefaultRoleName.Claims(claimRole.Name), true);
        //        }
        //    }

        //    return result;
        //}
    }
}
