namespace BIT.MyCity.Authorization.Models
{
    public class RestorePasswordModel
    {
        public long UserId { get; set; }

        public string Code { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
