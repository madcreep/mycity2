namespace BIT.MyCity.Authorization.Models
{
    public enum Gender
    {
        Male = 1,
        Female = 2
    }
}
