using System.Collections.Generic;
using System.Security.Claims;
using BIT.MyCity.Database;

namespace BIT.MyCity.Authorization.Models
{
    public class DefaultRoleName
    {
        public const string Administrator = "Администратор";
        //public const string Moderator = "Модератор";
        //public const string Citizen = "Граждане";
        //public const string Organization = "Оганизации";
        //public const string DeloService = "Сервис дело";

        public static IEnumerable<Role> AllRoles()
        {
            return new[]
            {
                new Role
                {
                    Name = Administrator,
                    Editable = true,
                    IsAdminRegister = true,
                    Description = "Администраторы системы",
                    Weight = 1
                },
                //new Role
                //{
                //    Name = Moderator,
                //    Editable = true,
                //    IsAdminRegister = true,
                //    Description = "Модераторы системы",
                //    Weight = 2
                //},
                //new Role
                //{
                //    Name = DeloService,
                //    Editable = false,
                //    IsAdminRegister = false,
                //    Description = "Сервис ДЕЛО",
                //    Weight = 3
                //},
                //new Role
                //{
                //    Name = Citizen,
                //    Editable = false,
                //    IsAdminRegister = false,
                //    Description = "Граждане системы",
                //    Weight = 4
                //},
                //new Role
                //{
                //    Name = Organization,
                //    Editable = false,
                //    IsAdminRegister = false,
                //    Description = "Организации системы",
                //    Weight = 5
                //}
            };
        }

        public static IEnumerable<Claim> Claims(string needRole)
        {
            switch (needRole)
            {
                case Administrator:
                    return new[]
                    {
                        new Claim(ClaimName.Manage, "roles"),
                        new Claim(ClaimName.Manage, "users"),
                        new Claim(ClaimName.Manage, "rubrics"),
                        new Claim(ClaimName.Manage, "territories"),
                        new Claim(ClaimName.Manage, "regions"),
                        new Claim(ClaimName.Manage, "topics"),
                        new Claim(ClaimName.Manage, "settings")
                    };
                //case Moderator:
                //    return new[]
                //    {
                //        new Claim(ClaimName.Moderate, "citizens"),
                //        new Claim(ClaimName.Moderate, "organizations"),
                //    };
                //case Citizen:
                //    return new Claim[]
                //    {
                //        new Claim(ClaimName.Subsystem, "citizen")
                //    };
                //case Organization:
                //    return new Claim[]
                //    {
                //        new Claim(ClaimName.Subsystem, "organization")
                //    };
                //case DeloService:
                //    return new Claim[]
                //    {
                //        new Claim(ClaimName.Service, "delo")
                //    };
                default:
                    return new Claim[0];
            }
        }
    }
}
