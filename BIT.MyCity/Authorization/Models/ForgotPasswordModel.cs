namespace BIT.MyCity.Authorization.Models
{
    public class ForgotPasswordModel
    {
        public string Login { get; set; }

        public string Email { get; set; }
    }
}
