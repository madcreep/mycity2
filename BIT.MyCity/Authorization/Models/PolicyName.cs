namespace BIT.MyCity.Authorization.Models
{
    public static class PolicyName
    {
        public const string Authorized = "Authorized";

        public const string ManageUser = "Manage";
        public const string ManageRoles = "ManageRoles";
        public const string ManageUsersOrRoles = "ManageUsersOrRoles";

        public const string ManageRubrics = "ManageRubrics";
        public const string ManageTerritories = "ManageTerritories";
        public const string ManageRegions = "ManageRegions";
        public const string ManageTopics = "ManageTopics";

        public const string ManageSettings = "ManageSettings";
        public const string ManageJournal = "ManageJournal";
        public const string ManageCustomEmail = "ManageCustomEmail";
        public const string ManageReports = "ManageReports";
        
        public const string LoginSystemForm = "LoginSystemForm";
        public const string ManageUsersOrModerate = "ManageUsersOrModerate";
        public const string ManageUsersOrModerateOrganizations = "ManageUsersOrModerateOrganizations";
        public const string ManageUsersOrModerateOrganizationsOrSubsystemOrganization = "ManageUsersOrModerateOrganizationsOrSubsystemOrganization";
        public const string Moderate = "Moderate";
        public const string SubsystemOrganizations = "SubsystemOrganizations";


        public const string CreateAppeals = "CreateAppeals";
        public const string CreateMessages = "CreateMessages";

        public const string CreateNews = "CreateNews";
        public const string ManageNews = "ManageNews";
        public const string ModerateNews = "ModerateNews";

        public const string Vote = "Vote";
        public const string CreateVoting = "CreateVoting";
        public const string ManageVoting = "ManageVoting";
        public const string ModerateVoting = "ModerateVoting";

        public const string ManageRubricsSubsystem = "ManageRubricsSubsystem";
        public const string ManageTerritoriesSubsystem = "ManageTerritoriesSubsystem";
        public const string ManageRegionsSubsystem = "ManageRegionsSubsystem";
        public const string ManageTopicsSubsystem = "ManageTopicsSubsystem";

        public const string ManageRubricsWithSubsystem = "ManageRubricsWithSubsystem";
        public const string ManageTerritoriesWithSubsystem = "ManageTerritoriesWithSubsystem";
        public const string ManageRegionsWithSubsystem = "ManageRegionsWithSubsystem";
        public const string ManageTopicsWithSubsystem = "ManageTopicsWithSubsystem";
    }
}
