namespace BIT.MyCity.Authorization.Models
{
    public enum RightLevel
    {
        Undefined = 0,
        Unauthorized = 1,
        Authorized = 2,
        Manager = 3,
        AdministratorSettings = 4,
    }
}
