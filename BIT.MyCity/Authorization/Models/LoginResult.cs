using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Database;

namespace BIT.MyCity.Authorization.Models
{
    public class LoginResult
    {
        public string Key { get; set; }

        public string Token { get; set; }

        public LoginStatus LoginStatus { get; set; }

        public string ErrorMessage { get; set; }

        public User User { get; set; }
    }
}
