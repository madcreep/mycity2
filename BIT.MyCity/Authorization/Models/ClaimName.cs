namespace BIT.MyCity.Authorization.Models
{
    public static class ClaimName
    {
        public const string SuperAdmin = "sa";
        public const string Сonfirmed = "cf";
        public const string Manage = "mu";
        public const string Moderate = "md";
        public const string LoginSystem = "ls";
        public const string Subsystem = "ss";
        public const string SubsystemRights = "ssr";
        public const string Service = "svc";
        
    }
}
