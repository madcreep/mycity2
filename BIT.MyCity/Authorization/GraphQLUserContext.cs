using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;

namespace BIT.MyCity.Authorization
{
  public class GraphQLUserContext : Dictionary<string, object>, IProvideClaimsPrincipal
  {
    public ClaimsPrincipal User { get; set; }
  }
}
