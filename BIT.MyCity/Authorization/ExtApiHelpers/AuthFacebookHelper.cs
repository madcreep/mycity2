using System;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace BIT.MyCity.Authorization.ExtApiHelpers
{
    public sealed class AuthFacebookHelper : AuthExtApiHelper
    {
        private readonly string _appKey;
        private readonly string _secretKey;

        public AuthFacebookHelper(IConfiguration configuration, IServiceProvider servicesProvider)
            : base(configuration, servicesProvider)
        {
            try
            {
                var settingsManager = _servicesProvider.GetRequiredService<SettingsManager>();
                var isEnabled = settingsManager.SettingValue<bool>(SettingsKeys.SystemAuthFacebookEnabled);
                if (!isEnabled)
                {
                    Active = false;

                    return;
                }

                _appKey = settingsManager.SettingValue<string>(SettingsKeys.SystemAuthFacebookAppKey);
                _secretKey = settingsManager.SettingValue<string>(SettingsKeys.SystemAuthFacebookSecretKey);

                Active = !string.IsNullOrWhiteSpace(_appKey)
                         && !string.IsNullOrWhiteSpace(_secretKey);
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка инициализации {nameof(AuthFacebookHelper)}", ex);

                Active = false;
            }
        }

        internal override LoginSystem LoginSystem => LoginSystem.Facebook;
        internal override bool Active { get; }
        protected override string GetRedirectUrl(LoginModel loginModel)
        {
            var state = "{ls=Facebook}";

            return $"https://www.facebook.com/v7.0/dialog/oauth?client_id={_appKey}&redirect_uri={loginModel.CallbackUrl}&response_type=code&scope=email&state={state}";
        }

        protected override string GetLogoutUrl(LoginModel loginModel)
        {
            return null;
        }

        protected override async Task<User> GetUserProfileInfoAsync(LoginModel loginModel)
        {
            var token = await GetAccessTokenAsync(loginModel);

            if (token == null || token.Error != null)
            {
                return null;
            }

            var fbUser = await GetUserInfoAsync(token);

            return new User
            {
                LoginSystem = LoginSystem,
                UserName = fbUser.Id,
                FullName = fbUser.Name,
                FirstName = fbUser.FirstName,
                LastName = fbUser.LastName,
                MiddleName = fbUser.MiddleName,
                Gender = string.IsNullOrWhiteSpace(fbUser.Gender) ? (Gender?) null : fbUser.Gender == "male" ? Gender.Male : Gender.Female,
                Email = fbUser.Email,
                EmailConfirmed = !string.IsNullOrWhiteSpace(fbUser.Email),
                ProfileConfirmed = !string.IsNullOrWhiteSpace(fbUser.Email)
            };
        }

        private async Task<Token> GetAccessTokenAsync(LoginModel loginModel)
        {

            var url = $"https://graph.facebook.com/v7.0/oauth/access_token?client_id={_appKey}&redirect_uri={loginModel.CallbackUrl}&client_secret={_secretKey}&code={loginModel.Code}";

            return await GetResponse<Token>(url);
        }

        private async Task<FacebookUser> GetUserInfoAsync(Token token)
        {
            var url = $"https://graph.facebook.com/me?fields=id,name,first_name,last_name,middle_name,name_format,short_name,email,picture,gender&access_token={token.AccessToken}";

            var result = await GetResponse<FacebookUser>(url);

            return result;
        }
    }

    public class FacebookUser
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("middle_name")]
        public string MiddleName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("name_format")]
        public string NameFormat { get; set; }

        [JsonProperty("picture")]
        public FacebookPhoto Picture { get; set; }

        [JsonProperty("short_name")]
        public string ShortName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }
    }

    public class FacebookPhoto
    {
        [JsonProperty("data")]
        public FacebookData Data { get; set; }
    }

    public class FacebookData
    {
        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("width")]
        public decimal Width { get; set; }

        [JsonProperty("height")]
        public decimal Height { get; set; }

        /// <summary>
        /// Это силуэт
        /// </summary>
        [JsonProperty("is_silhouette")]
        public bool IsSilhouette { get; set; }
    }

    internal class MessageError
    {
        /// <summary>
        /// Сообщение ошибки
        /// </summary>
        [JsonProperty("message")]
        public string Ьessage { get; set; }

        /// <summary>
        /// Тип ошибки
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// Код ошибки
        /// </summary>
        [JsonProperty("code")]
        public int? Code { get; set; }

        /// <summary>
        /// Субкод ошибки
        /// </summary>
        [JsonProperty("error_subcode")]
        public int? ErrorSubcode { get; set; }


        /// <summary>
        /// Идентификатор трассировки
        /// </summary>
        [JsonProperty("fbtrace_id")]
        public string FbTraceId { get; set; }
        
    }

    internal class Token
    {
        /// <summary>
        /// Токен доступа
        /// </summary>
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        ///  Время жизни токена
        /// </summary>
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        /// <summary>
        /// Тип токена
        /// </summary>
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        /// <summary>
        /// Ошибка
        /// </summary>
        [JsonProperty("error")]
        public MessageError Error { get; set; }
    }
}
