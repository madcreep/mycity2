using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.EsiaNETCore;
using BIT.MyCity.Authorization.Models;
using Microsoft.Extensions.Configuration;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using log4net;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Authorization.ExtApiHelpers
{
    public class AuthEsiaHelper : AuthExtApiHelper
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(AuthEsiaHelper));

        private readonly string[] _scopes;

        private readonly string[] _orgScopes;

        private readonly EsiaClient _esiaClient;

        internal override LoginSystem LoginSystem => LoginSystem.Esia;

        internal override bool Active { get; }

        protected override string GetRedirectUrl(LoginModel loginModel)
        {
            if (!string.IsNullOrWhiteSpace(loginModel?.CallbackUrl))
            {
                return _esiaClient.BuildRedirectUri(loginModel.CallbackUrl);
            }

            Log.Error("Ошибка формирования RedirectUrl ЕСИА - передан пустой Url возврата");

            return null;
        }
        
        protected override string GetLogoutUrl(LoginModel loginModel)
        {
            return GetLogouttUrl(loginModel);
        }

        public AuthEsiaHelper(IConfiguration configuration, IServiceProvider servicesProvider)
            : base(configuration, servicesProvider)
        {
            try
            {
                var settingsManager = _servicesProvider.GetRequiredService<SettingsManager>();

                var isEnabled = settingsManager.SettingValue<bool>(SettingsKeys.SystemAuthEsiaEnabled);

                if (!isEnabled)
                {
                    Active = false;

                    return;
                }

                _esiaClient = new EsiaClient(GetOptions(configuration), _log);

                _scopes = _esiaClient.Options.Scope?
                              .Split(' ')
                              .Where(el => !string.IsNullOrWhiteSpace(el))
                              .ToArray()
                          ?? new string[0];

                _orgScopes = _esiaClient.Options.OrgScope?
                                 .Split(' ')
                                 .Where(el => !string.IsNullOrWhiteSpace(el))
                                 .ToArray()
                             ?? new string[0];

                Active = true;
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка инициализации {nameof(AuthEsiaHelper)}", ex);

                Active = false;
            }
        }

        protected override async Task<User> GetUserProfileInfoAsync(LoginModel loginModel)
        {
            if (!await InstallToken(loginModel))
            {
                return null;
            }

            var personInfo = await GetUserInfoAsync();

            if (personInfo == null)
            {
                return null;
            }

            var personContacts = (await GetUserContactsAsync())?
                .ToArray();

            var addresses = await GetUserAddressesAsync();

            var personDocInfo = await GetUserIdentityDocumentsAsync();

            var personOrganizationsRoles = await GetUserRolesAsync();

            var eMailValue = ExtractEmail(personContacts);

            var userPhones = ExtractPhones(personContacts);

            var userDocs = ExtractDocuments(personDocInfo);

            var result = new User
            {
                UserName = _esiaClient.Token.SbjId,
                FirstName = personInfo?.FirstName,
                MiddleName = personInfo?.MiddleName,
                LastName = personInfo?.LastName,
                FullName = personInfo?.Name ?? GetFullName(personInfo),
                Email = eMailValue?.Value,
                EmailConfirmed = eMailValue?.Verified ?? false,
                ProfileConfirmed = personInfo?.Trusted ?? false,
                UserPhones = userPhones ?? new List<OrganizationUserPhone>(),
                LoginSystem = LoginSystem,
                Inn = personInfo?.Inn,
                Snils = personInfo?.Snils,
                UserIdentityDocuments = userDocs,
                Gender = ParseGender(personInfo?.Gender),
                Users2Organizations = new List<User2Organization>(),
                Addresses = addresses
            };

            await AddOrganizations(loginModel, result, personOrganizationsRoles);

            return result;
        }

        private static Gender? ParseGender(string infoStr)
        {
            return infoStr?.ToUpper() switch
            {
                "M" => Gender.Male,
                "Ж" => Gender.Female,
                _ => null
            };
        }

        private async Task AddOrganizations(LoginModel loginModel, User user, IEnumerable<RoleInfo> personOrganizationsRoles)
        {
            if (personOrganizationsRoles == null)
            {
                return;
            }

            var result = new List<UserOrganization>();

            foreach (var role in personOrganizationsRoles)
            {
                if (!role.Active)
                {
                    continue;
                }

                var stResult = await InstallOrganizationTokenAsync(loginModel, role.Id);

                PersonOrganization orgInfo = null;

                List<Address> orgAddresses = null;

                ContactInfo eMailValue = null;

                var orgPhones = new List<OrganizationUserPhone>();

                if (stResult)
                {
                    orgInfo = await GetOrganizationInfoAsync(role.Id);

                    orgAddresses = (await GetOrganizationAddressesAsync(role.Id))
                        .ToList();

                    var orgContacts = (await GetOrganizationContactsAsync(role.Id))
                        .ToArray();

                    eMailValue = ExtractEmail(orgContacts);

                    orgPhones = ExtractPhones(orgContacts);
                }

                UserOrganization userOrganization;

                userOrganization = new UserOrganization
                {
                    ExtId = AddPrefix(orgInfo?.Oid ?? role.Id),
                    IsBrunch = false,
                    Type = orgInfo?.Type ?? role.Type,
                    ShortName = orgInfo?.ShortName ?? role.ShortName,
                    FullName = orgInfo?.FullName ?? role.FullName,
                    Inn = orgInfo?.Inn,
                    Kpp = orgInfo?.Kpp,
                    Ogrn = orgInfo?.Ogrn ?? role.Ogrn,
                    Leg = orgInfo?.Leg,
                    AgencyType = orgInfo?.AgencyType,
                    AgencyTerRang = orgInfo?.AgencyTerRang,
                    Email = eMailValue?.Value,
                    EmailVerified = eMailValue?.Verified,
                    OrganizationPhones = orgPhones,
                    Addresses = orgAddresses
                };

                if (!string.IsNullOrWhiteSpace(role.BranсhOid))
                {
                    var brunchInfo = await GetBranchInfoAsync(role.Id, role.BranсhOid);

                    var brunchAddresses = (await GetBrunchAddressesAsync(role.Id, role.BranсhOid))?
                        .ToList();

                    var brunchContacts = (await GetBrunchContactsAsync(role.Id, role.BranсhOid))?
                        .ToArray();

                    var brunchEMailValue = ExtractEmail(brunchContacts);

                    var brunchPhones = ExtractPhones(brunchContacts); ;

                    userOrganization = new UserOrganization
                    {
                        ExtId = AddPrefix(role.BranсhOid),
                        ExtParentId = AddPrefix(role.Id),
                        IsBrunch = true,
                        ShortName = brunchInfo?.Name ?? role.BranсhName,
                        FullName = brunchInfo?.Name ?? role.BranсhName,
                        Kpp = brunchInfo?.Kpp,
                        Leg = brunchInfo?.Leg,
                        Email = brunchEMailValue?.Value,
                        EmailVerified = brunchEMailValue?.Verified,
                        OrganizationPhones = brunchPhones,
                        Addresses = brunchAddresses,
                        Parent = userOrganization
                    };
                }

                userOrganization.Chief = role.Chief;

                if (string.IsNullOrWhiteSpace(role.Phone))
                {
                    var phone = string.IsNullOrWhiteSpace(role.Phone)
                        ? null
                        : new OrganizationUserPhone
                        {
                            Confirmed = true,
                            Phone = new Phone
                            {
                                Type = TypePhone.Work,
                                Number = role.Phone
                            },
                            UserOrganization = userOrganization
                        };

                    if (phone != null)
                    {
                        user.UserPhones.Add(phone);
                    }
                }

                result.Add(userOrganization);
            }

            user.Users2Organizations = result
                .Select(el => new User2Organization
                {
                    UserOrganization = el,
                    Chief = el.Chief ?? false
                })
                .ToList();
        }

        private List<UserIdentityDocument> ExtractDocuments(IEnumerable<DocInfo> personDocInfoList)
        {
            return personDocInfoList?
                       .Select(el => new UserIdentityDocument
                       {
                           Type = el.DocType,
                           Series = el.Series,
                           Number = el.Number,
                           IssuedBy = el.IssuedBy,
                           IssueId = el.IssueId,
                           IssueDate = string.IsNullOrWhiteSpace(el.IssueDate)
                               ? DateTime.TryParseExact(el.IssueDate,
                                   "dd.MM.yyyy",
                                   System.Globalization.CultureInfo.InvariantCulture,
                                   System.Globalization.DateTimeStyles.None,
                                   out var tmpDate) ? tmpDate : null as DateTime?
                               : null as DateTime?,
                           ExpiryDate = string.IsNullOrWhiteSpace(el.ExpiryDate)
                               ? DateTime.TryParseExact(el.IssueDate,
                                   "dd.MM.yyyy",
                                   System.Globalization.CultureInfo.InvariantCulture,
                                   System.Globalization.DateTimeStyles.None,
                                   out var tmpExpDate) ? tmpExpDate : null as DateTime?
                               : null as DateTime?,
                           Verified = el.Verified
                       })
                       .ToList()
                   ?? new List<UserIdentityDocument>();
        }

        private static ContactInfo ExtractEmail(ContactInfo[] contacts)
        {
            if (contacts == null)
            {
                return null;
            }

            return contacts
                .OrderByDescending(el => el.Verified)
                .FirstOrDefault(el => el.ContactType == ContactType.Email
                                      || el.ContactType == ContactType.OrgEmail
                                      || el.ContactType == ContactType.WorkEmail);
        }

        private static List<OrganizationUserPhone> ExtractPhones(ContactInfo[] contacts)
        {
            if (contacts == null)
            {
                return null;
            }

            var homePhones = contacts
                                 .Where(el => el.ContactType == ContactType.Phone)
                                 .Select(el => new OrganizationUserPhone
                                 {
                                     Confirmed = el.Verified,
                                     Phone = new Phone
                                     {
                                         Type = TypePhone.Phone,
                                         Number = el.Value
                                     }
                                 })
                             ?? new OrganizationUserPhone[0];

            var faxes = contacts
                                 .Where(el => el.ContactType == ContactType.Fax)
                                 .Select(el => new OrganizationUserPhone
                                 {
                                     Confirmed = el.Verified,
                                     Phone = new Phone
                                     {
                                         Type = TypePhone.Fax,
                                         Number = el.Value
                                     }
                                 })
                             ?? new OrganizationUserPhone[0];

            var mobilePhones = contacts
                                   .Where(el => el.ContactType == ContactType.Mobile)
                                   .Select(el => new OrganizationUserPhone
                                   {
                                       Confirmed = el.Verified,
                                       Phone = new Phone
                                       {
                                           Type = TypePhone.Mobile,
                                           Number = el.Value,
                                       }


                                   })
                               ?? new OrganizationUserPhone[0];

            return homePhones
                .Union(faxes)
                .Union(mobilePhones)
                .ToList();
        }

        private string GetLogouttUrl(LoginModel loginModel)
        {
            return _esiaClient.BuildLogoutUri(loginModel?.CallbackUrl);
        }

        private async Task<bool> InstallToken(LoginModel loginModel)
        {
            if (loginModel == null)
            {
                Log.Error($"Ошибка запроса токена доступа - LoginModel == null");

                return false;
            }

            if (string.IsNullOrWhiteSpace(loginModel.Code)
                || string.IsNullOrWhiteSpace(loginModel.CallbackUrl))
            {
                Log.Error($"Ошибка запроса токена доступа - не верные параметры (Code: {loginModel.Code}; CallbackUrl: {loginModel.CallbackUrl})");

                return false;
            }

            EsiaTokenResponse tokenResponse;

            try
            {
                tokenResponse = await _esiaClient.GetOAuthTokenAsync(loginModel.Code, loginModel.CallbackUrl);
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка получения токена ЕСИА (Code: {loginModel.Code}; CallbackUrl: {loginModel.CallbackUrl};)",
                    ex);

                return false;
            }

            if (!_esiaClient.VerifyToken(tokenResponse.AccessToken))
            {
                Log.Error($"Ошибка получения токена ЕСИА - Токен не пргошел верификацию");

                return false;
            }

            _esiaClient.Token = EsiaClient.CreateToken(tokenResponse);

            return true;
        }

        private static string GetFullName(PersonInfo personInfo)
        {
            var fullName = string.IsNullOrWhiteSpace(personInfo.LastName)
              ? string.Empty
              : personInfo.LastName.Trim();

            if (!string.IsNullOrWhiteSpace(personInfo.FirstName))
            {
                fullName += $"{(fullName.Length > 0 ? " " : string.Empty)}{personInfo.FirstName.Trim()}";
            }

            return fullName;
        }

        private EsiaOptions GetOptions(IConfiguration configuration)
        {
            var settingsManager = _servicesProvider.GetRequiredService<SettingsManager>();

            var isTest = settingsManager.SettingValue<bool>(SettingsKeys.SystemAuthEsiaTestMode);

            return new EsiaOptions
            {
                ClientId = settingsManager.SettingValue<string>(SettingsKeys.SystemAuthEsiaMnemonics),
                RedirectUri = isTest ? EsiaConsts.EsiaAuthTestUrl : EsiaConsts.EsiaAuthUrl,
                TokenUri = isTest ? EsiaConsts.EsiaTokenTestUrl : EsiaConsts.EsiaTokenUrl,
                RestUri = isTest ? EsiaConsts.EsiaRestTestUrl : EsiaConsts.EsiaRestUrl,
                LogoutUrl = isTest ? EsiaConsts.EsiaLogoutTestUrl : EsiaConsts.EsiaLogoutUrl,
                Scope = settingsManager.SettingValue<string>(SettingsKeys.SystemAuthEsiaScoupeCitizen),
                OrgScope = settingsManager.SettingValue<string>(SettingsKeys.SystemAuthEsiaScoupeOrganization),
                SignProvider = new EsiaGostSigner(new EsiaGostSgnerOptions
                {
                    OrganizationCertificateFile = configuration["AuthApiSettings:Esia:OrganizationCertificateFile"],
                    OrganizationCertificateKeyFile = configuration["AuthApiSettings:Esia:OrganizationCertificateKeyFile"],
                    EsiaCertificateFile = isTest ? configuration["AuthApiSettings:Esia:EsiaCertificateFileTest"] : configuration["AuthApiSettings:Esia:EsiaCertificateFile"],
                    OpenSslPath = settingsManager.SettingValue<string>(SettingsKeys.SystemAuthEsiaOpensslPath),
                    Log = Log
                })
            };
        }

        private async Task<PersonInfo> GetUserInfoAsync()
        {
            var needScopes = new[]
            {
                "fullname",
                "birthdate",
                "gender",
                "snils",
                "inn",
                "email"
            };

            if (!CheckPersonNeedScope(needScopes))
            {
                return null;
            }

            try
            {
                var result = await _esiaClient.GetPersonInfoAsync();

                return result;
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка запроса контактов для пользователя ЕСИА (id = {_esiaClient.Token.SbjId})", ex);

                return null;
            }
        }

        private async Task<IEnumerable<ContactInfo>> GetUserContactsAsync()
        {
            var needScopes = new[]
            {
                "contacts",
                "mobile"
            };

            if (!CheckPersonNeedScope(needScopes))
            {
                return new List<ContactInfo>();
            }

            try
            {
                var result = await _esiaClient.GetPersonContactsAsync();

                return result;
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка запроса контактов для пользователя ЕСИА (id = {_esiaClient.Token.SbjId})", ex);

                return null;
            }
        }

        private async Task<List<Address>> GetUserAddressesAsync()
        {
            var needScopes = new[]
            {
                "contacts",
                "addresses"
            };

            if (!CheckPersonNeedScope(needScopes))
            {
                return new List<Address>();
            }

            try
            {
                var esiaAddresses = await _esiaClient.GetPersonAddrsAsync();

                var result = esiaAddresses?
                                 .Select(el => new Address
                                 {
                                     AddrType = el.AddrType,
                                     ZipCode = el.ZipCode,
                                     CountryId = el.CountryId,
                                     Building = el.Building,
                                     Frame = el.Frame,
                                     House = el.House,
                                     Flat = el.Flat,
                                     FiasCode = el.FiasCode,
                                     Region = el.Region,
                                     City = el.City,
                                     District = el.District,
                                     Area = el.Area,
                                     Settlement = el.Settlement,
                                     AdditionArea = el.AdditionArea,
                                     AdditionAreaStreet = el.AdditionAreaStreet,
                                     Street = el.Street,
                                     PartialAddress = el.AddressStr
                                 })
                                 .ToList()
                             ?? new List<Address>();

                return result;
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка запроса контактов для пользователя ЕСИА (id = {_esiaClient.Token.SbjId})", ex);

                return null;
            }
        }

        private async Task<IEnumerable<DocInfo>> GetUserIdentityDocumentsAsync()
        {
            var needScopes = new[]
            {
                "id_doc"
            };

            if (!CheckPersonNeedScope(needScopes))
            {
                return null;
            }

            try
            {
                var result = await _esiaClient.GetPersonDocsAsync();

                return result;
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка запроса документов для пользователя ЕСИА (id = {_esiaClient.Token.SbjId})", ex);

                return null;
            }
        }

        private async Task<IEnumerable<RoleInfo>> GetUserRolesAsync()
        {
            var needScopes = new[]
            {
                "usr_org"
            };

            if (!CheckPersonNeedScope(needScopes))
            {
                return null;
            }

            var result = await _esiaClient.GetPersonRolesAsync();

            return result;
        }

        private async Task<bool> InstallOrganizationTokenAsync(LoginModel loginModel, string org_oid)
        {
            try
            {
                if (!_orgScopes.All(el => el.StartsWith("org_")))
                {
                    return false;
                }

                var baseOrgScopeUrl = "http://esia.gosuslugi.ru";

                var tmpScope = _orgScopes
                    .Select(el => $"{baseOrgScopeUrl}/{el}?org_oid={org_oid}");

                _esiaClient.Options.Scope = string.Join(" ", tmpScope);

                var tokenResponse = await _esiaClient.GetOAuthTokenByOrganizationAsync(loginModel.Code,
                    loginModel.CallbackUrl);

                if (string.IsNullOrWhiteSpace(tokenResponse.AccessToken))
                {
                    return false;
                }

                _esiaClient.Token = EsiaClient.CreateToken(tokenResponse);

                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка получения токена доступа системы", ex);

                return false;
            }
        }

        private async Task<PersonOrganization> GetOrganizationInfoAsync(string orgOid)
        {
            var needScopes = new[]
            {
                "org_shortname",
                "org_fullname",
                "org_type",
                "org_ogrn",
                "org_inn",
                "org_leg",
                "org_kpp",
                "org_agencyterrange",
                "org_agencytype",
                "org_oktmo"
            };

            if (!CheckOrganizationNeedScope(needScopes))
            {
                return null;
            }

            var result = await _esiaClient.GetOrganizationAsync(orgOid);

            return result;
        }

        private async Task<IEnumerable<Address>> GetOrganizationAddressesAsync(string orgOid)
        {
            var needScopes = new[]
            {
                "org_addrs"
            };

            if (!CheckOrganizationNeedScope(needScopes))
            {
                return null;
            }

            try
            {
                var addrInfoList = await _esiaClient.GetOrganizationAddressesAsync(orgOid);

                var result = addrInfoList?
                                 .Select(el => new Address
                                 {
                                     AddrType = el.AddrType,
                                     ZipCode = el.ZipCode,
                                     CountryId = el.CountryId,
                                     Building = el.Building,
                                     Frame = el.Frame,
                                     House = el.House,
                                     Flat = el.Flat,
                                     FiasCode = el.FiasCode,
                                     Region = el.Region,
                                     City = el.City,
                                     District = el.District,
                                     Area = el.Area,
                                     Settlement = el.Settlement,
                                     AdditionArea = el.AdditionArea,
                                     AdditionAreaStreet = el.AdditionAreaStreet,
                                     Street = el.Street,
                                     PartialAddress = el.AddressStr
                                 })
                                 .ToList()
                             ?? new List<Address>();

                return result;
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка запроса контактов для пользователя ЕСИА (id = {_esiaClient.Token.SbjId})", ex);

                return null;
            }
        }

        private async Task<IEnumerable<ContactInfo>> GetOrganizationContactsAsync(string orgOid)
        {
            var needScopes = new[]
            {
                "org_ctts"
            };

            if (!CheckOrganizationNeedScope(needScopes))
            {
                return null;
            }

            var result = await _esiaClient.GetOrganizationContactsAsync(orgOid);

            return result;
        }

        private async Task<BrunchInfo> GetBranchInfoAsync(string orgId, string brunchId)
        {
            if (orgId == null)
            {
                return null;
            }

            var needScopes = new[]
            {
                "org_brhs"
            };

            if (!CheckOrganizationNeedScope(needScopes))
            {
                return null;
            }

            var result = await _esiaClient.GetBranchAsync(orgId, brunchId);

            return result;
        }

        private async Task<IEnumerable<Address>> GetBrunchAddressesAsync(string orgOid, string brunchId)
        {
            var needScopes = new[]
            {
                "org_brhs_addrs"
            };

            if (!CheckOrganizationNeedScope(needScopes))
            {
                return null;
            }

            try
            {
                var addrInfoList = await _esiaClient.GetBrunchAddressesAsync(orgOid, brunchId);

                var result = addrInfoList?
                                 .Select(el => new Address
                                 {
                                     AddrType = el.AddrType,
                                     ZipCode = el.ZipCode,
                                     CountryId = el.CountryId,
                                     Building = el.Building,
                                     Frame = el.Frame,
                                     House = el.House,
                                     Flat = el.Flat,
                                     FiasCode = el.FiasCode,
                                     Region = el.Region,
                                     City = el.City,
                                     District = el.District,
                                     Area = el.Area,
                                     Settlement = el.Settlement,
                                     AdditionArea = el.AdditionArea,
                                     AdditionAreaStreet = el.AdditionAreaStreet,
                                     Street = el.Street,
                                     PartialAddress = el.AddressStr
                                 })
                                 .ToList()
                             ?? new List<Address>();

                return result;
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка запроса контактов для филиала ЕСИА (orgOid = {orgOid}; brunchOid = {brunchId};)", ex);

                return null;
            }
        }

        private async Task<IEnumerable<ContactInfo>> GetBrunchContactsAsync(string orgOid, string brunchId)
        {
            var needScopes = new[]
            {
                "org_brhs_ctts"
            };

            if (!CheckOrganizationNeedScope(needScopes))
            {
                return null;
            }

            var result = await _esiaClient.GetBrunchContactsAsync(orgOid, brunchId);

            return result;
        }

        private bool CheckPersonNeedScope(string[] needScopes)
        {
            return CheckNeedScope(needScopes, _scopes);
        }

        private bool CheckOrganizationNeedScope(string[] needScopes)
        {
            return CheckNeedScope(needScopes, _orgScopes);
        }

        private bool CheckNeedScope(string[] needScopes, string[] hasCopes)
        {
            return hasCopes.Any(el => needScopes.Contains(el));
        }
    }
}
