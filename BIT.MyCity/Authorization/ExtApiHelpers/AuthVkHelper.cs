using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace BIT.MyCity.Authorization.ExtApiHelpers
{
    internal sealed class AuthVkHelper : AuthExtApiHelper
    {
        private readonly string _appKey;
        private readonly string _secretKey;
        private readonly string _serviceKey;

        internal AuthVkHelper(IConfiguration configuration, IServiceProvider servicesProvider)
            : base(configuration, servicesProvider)
        {
            try
            {
                var settingsManager = _servicesProvider.GetRequiredService<SettingsManager>();

                var isEnabled = settingsManager.SettingValue<bool>(SettingsKeys.SystemAuthVkEnabled);

                if (!isEnabled)
                {
                    Active = false;

                    return;
                }

                _appKey = settingsManager.SettingValue<string>(SettingsKeys.SystemAuthVkAppKey);
                _secretKey = settingsManager.SettingValue<string>(SettingsKeys.SystemAuthVkSecretKey);
                _serviceKey = settingsManager.SettingValue<string>(SettingsKeys.SystemAuthVkServiceKey);

                Active = !string.IsNullOrWhiteSpace(_appKey)
                    && !string.IsNullOrWhiteSpace(_secretKey)
                    && !string.IsNullOrWhiteSpace(_serviceKey);
            }
            catch (Exception ex)
            {
                Log.Error($"Ошибка инициализации {nameof(AuthVkHelper)}", ex);

                Active = false;
            }
        }

        internal override LoginSystem LoginSystem => LoginSystem.Vk;
        internal override bool Active { get; }

        protected override string GetRedirectUrl(LoginModel loginModel)
        {
            return
                $"https://oauth.vk.com/authorize?client_id={_appKey}&display=page&redirect_uri={loginModel.CallbackUrl}&scope=email&response_type=code&v=5.103";
        }

        protected override string GetLogoutUrl(LoginModel loginModel)
        {
            return null;
        }

        protected override async Task<User> GetUserProfileInfoAsync(LoginModel loginModel)
        {
            var token = await GetAccessToken(loginModel);

            if (token == null || token.Error != null)
            {
                return null;
            }

            var profileInfo = await GetUserInfo(token.UserId);

            if (profileInfo == null)
            {
                return null;
            }

            return new User
            {
                LoginSystem = LoginSystem,
                UserName = token.UserId.ToString(),
                FullName = GetFullName(profileInfo),
                Email = token.Email,
                EmailConfirmed = !string.IsNullOrWhiteSpace(token.Email),
                ProfileConfirmed = !string.IsNullOrWhiteSpace(token.Email)
            };
        }

        #region PrivateMethods
        private async Task<Token> GetAccessToken(LoginModel loginModel)
        {
            var url =
              $"https://oauth.vk.com/access_token?client_id={_appKey}&client_secret={_secretKey}&redirect_uri={loginModel.CallbackUrl}&code={loginModel.Code}";

            return await GetResponse<Token>(url);
        }

        private static async Task<ProfileInfo> GetProfileInfo(Token token)
        {
            var url = $"https://api.vk.com/method/account.getProfileInfo?access_token={token.AccessToken}&v=5.62";



            var profileInfo = await GetResponse<ProfileInfo>(url);

            return profileInfo;
        }

        private async Task<VcUserInfo> GetUserInfo(long userId)
        {
            var url = $"https://api.vk.com/method/users.get?access_token={_serviceKey}&user_ids={userId}&name_case=nom&lang=ru&v=5.103";

            var usersInfo = await GetResponse<VcUserInfoResponse>(url);

            if (usersInfo.VcUsersInfo == null)
            {
                return null;
            }

            return usersInfo.VcUsersInfo
              .FirstOrDefault(el => el.Id == userId);
        }

        private static string GetFullName(VcUserInfo profileInfo)
        {
            var fullName = string.IsNullOrWhiteSpace(profileInfo.LastName)
              ? string.Empty
              : profileInfo.LastName.Trim();

            if (!string.IsNullOrWhiteSpace(profileInfo.FirstName))
            {
                fullName += $"{(fullName.Length > 0 ? " " : string.Empty)}{profileInfo.FirstName.Trim()}";
            }

            return fullName;
        }
        #endregion

        #region Classes
        internal class MessageError
        {
            /// <summary>
            /// Код ошибки
            /// </summary>
            [JsonProperty("error")]
            public string Error { get; set; }

            /// <summary>
            /// Описание ошибки
            /// </summary>
            [JsonProperty("error_description")]
            public string ErrorDescription { get; set; }
        }

        internal class Token : MessageError
        {
            /// <summary>
            /// Токен доступа
            /// </summary>
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }

            /// <summary>
            ///  Время жизни ключа в секундах
            /// </summary>
            [JsonProperty("expires_in")]
            public int ExpiresIn { get; set; }

            /// <summary>
            /// Идентификатор пользователя
            /// </summary>
            [JsonProperty("user_id")]
            public long UserId { get; set; }

            /// <summary>
            /// Адрес электронной почты
            /// </summary>
            [JsonProperty("email")]
            public string Email { get; set; }
        }

        internal class ProfileInfo
        {
            /// <summary>
            /// Имя пользователя
            /// </summary>
            [JsonProperty("first_name")]
            public string FirstName { get; set; }

            /// <summary>
            /// Фамилия пользователя
            /// </summary>
            [JsonProperty("last_name")]
            public string LastName { get; set; }

            /// <summary>
            /// Девичья фамилия пользователя (только для женского пола)
            /// </summary>
            [JsonProperty("maiden_name")]
            public string MaidenName { get; set; }

            /// <summary>
            /// Короткое имя пользователя(если есть)
            /// </summary>
            [JsonProperty("screen_name")]
            public string ScreenName { get; set; }

            /// <summary>
            /// Короткое имя пользователя(если есть)
            /// </summary>
            [JsonProperty("sex")]
            public int? Sex { get; set; }

            /// <summary>
            /// Cемейное положение
            /// </summary>
            [JsonProperty("relation")]
            public int? Relation { get; set; }

            /// <summary>
            /// Дата рождения пользователя, возвращается в формате D.M.YYYY
            /// </summary>
            [JsonProperty("bdate")]
            public string BDate { get; set; }

            /// <summary>
            /// Название родного города
            /// </summary>
            [JsonProperty("home_town")]
            public string HomeTown { get; set; }

            /// <summary>
            /// Страна
            /// </summary>
            [JsonProperty("country")]
            public IdTitle Country { get; set; }

            /// <summary>
            /// Город
            /// </summary>
            [JsonProperty("city")]
            public IdTitle City { get; set; }

            /// <summary>
            /// Ошибка
            /// </summary>
            [JsonProperty("error")]
            public ErrorMsg Error { get; set; }
        }

        internal class ErrorMsg
        {
            /// <summary>
            /// Код ошибки
            /// </summary>
            [JsonProperty("error_code")]
            public int? ErrorCode { get; set; }

            /// <summary>
            /// Описание ошибки
            /// </summary>
            [JsonProperty("error_msg")]
            public string ErrorMessage { get; set; }
        }

        internal class IdTitle
        {
            /// <summary>
            /// Идентификатор
            /// </summary>
            [JsonProperty("id")]
            public int? Id { get; set; }

            /// <summary>
            /// Название
            /// </summary>
            [JsonProperty("title")]
            public string Title { get; set; }
        }

        internal class VcUserInfoResponse
        {
            [JsonProperty("response")]
            public VcUserInfo[] VcUsersInfo { get; set; }
        }

        internal class VcUserInfo
        {
            [JsonProperty("id")]
            public long Id { get; set; }

            [JsonProperty("first_name")]
            public string FirstName { get; set; }

            [JsonProperty("last_name")]
            public string LastName { get; set; }

            [JsonProperty("is_closed")]
            public bool IsClosed { get; set; }

            [JsonProperty("can_access_closed")]
            public bool CanAccessClosed { get; set; }
        }
        #endregion
    }
}
