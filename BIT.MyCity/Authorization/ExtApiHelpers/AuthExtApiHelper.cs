using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Database;
using log4net;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace BIT.MyCity.Authorization.ExtApiHelpers
{
    public abstract class AuthExtApiHelper
    {
        protected readonly IConfiguration _configuration;
        protected readonly IServiceProvider _servicesProvider;

        protected readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        internal abstract LoginSystem LoginSystem { get; }

        internal abstract bool Active { get; }

        protected AuthExtApiHelper(IConfiguration configuration, IServiceProvider servicesProvider)
        {
            _configuration = configuration;
            _servicesProvider = servicesProvider;
        }

        internal async Task<User> GetUserAsync(LoginModel loginModel)
        {
            if (!Active)
            {
                return null;
            }

            var user = await GetUserProfileInfoAsync(loginModel);

            if (user == null)
            {
                return null;
            }

            user.UserName = AddPrefix(user.UserName);

            return user;
        }

        protected string AddPrefix(string value)
        {
            return value == null
                ? null
                : $"{LoginSystem}_{value}";
        }

        internal string RedirectUrl(LoginModel loginModel)
        {
            return !Active ? null : GetRedirectUrl(loginModel);
        }

        protected abstract string GetRedirectUrl(LoginModel loginModel);

        internal string LogoutUrl(LoginModel loginModel)
        {
            return !Active ? null : GetLogoutUrl(loginModel);
        }

        protected abstract string GetLogoutUrl(LoginModel loginModel);

        protected abstract Task<User> GetUserProfileInfoAsync(LoginModel loginModel);

        protected static async Task<T> GetResponse<T>(string url)
        {
            var request = WebRequest.Create(url);

            request.Method = "GET";

            var json = await GetResponseString(request);

            return DeserializeAnswer<T>(json);
        }

        private static async Task<string> GetResponseString(WebRequest request)
        {
            string result;

            try
            {
                var response = await request.GetResponseAsync();

                result = ReadResult(response);
            }
            catch (WebException ex)
            {
                result = ReadResult(ex.Response);
            }
            catch (Exception ex)
            {
                result = null;
            }

            return result;
        }

        private static string ReadResult(WebResponse response)
        {
            string result;
            using (var stream = response.GetResponseStream())
            {
                if (stream == null)
                {
                    result = null;
                }
                else
                {
                    var reader = new StreamReader(stream);

                    result = reader.ReadToEnd();
                }
            }

            return result;
        }

        private static T DeserializeAnswer<T>(string json)
        {
            T result;

            try
            {
                result = JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception ex)
            {
                result = default(T);
            }

            return result;
        }
    }
}
