using System;
using System.Collections.Generic;
using System.Linq;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization.AuthSubsystems;
using BIT.MyCity.Managers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Authorization.ExtApiHelpers
{
    public class AuthExtApiHelperFabric
    {
        private readonly LoginSystem[] _subsystemsKeys =
            new []
            {
                LoginSystem.Form,
                LoginSystem.Esia,
                LoginSystem.Vk,
                LoginSystem.Facebook
            };

        private readonly IConfiguration _config;
        private readonly IServiceProvider _servicesProvider;

        public AuthExtApiHelperFabric([FromServices] IConfiguration config, [FromServices]IServiceProvider servicesProvider)
        {
            _config = config;
            _servicesProvider = servicesProvider;
        }

        public IEnumerable<LoginSystem> ActiveLoginSystems =>
            (from key in _subsystemsKeys
                let helper = GetHelper(key)
                where helper != null && helper.Active
                select key)
            .ToList();


        public AuthExtApiHelper GetHelper(LoginSystem loginSubsystem)
        {
            return loginSubsystem switch
            {
                LoginSystem.Form => null,
                LoginSystem.Esia => GetEsiaManager(),
                LoginSystem.Vk => GetVkManager(),
                LoginSystem.Google => null,
                LoginSystem.Facebook => GetFacebookManager(),
                _ => null
            };
        }

        private AuthExtApiHelper GetVkManager()
        {
            return new AuthVkHelper(_config, _servicesProvider);
        }

        private AuthExtApiHelper GetEsiaManager()
        {
            return new AuthEsiaHelper(_config, _servicesProvider);
        }

        private AuthExtApiHelper GetFacebookManager()
        {
            return new AuthFacebookHelper(_config, _servicesProvider);
        }
    }
}
