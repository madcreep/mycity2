using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using GraphQL;
using GraphQL.Types;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types.Roles;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddRolesFields()
        {
            Field<RoleType>(
                    "create" + nameof(Role),
                    arguments: new QueryArguments(new QueryArgument<NonNullGraphType<RoleInputType>>
                    {
                        Name = nameof(Role).ToJsonFormat(),
                        Description = "Параметры создаваемой роли"
                    }),
                    description:"Создание роли",
                    resolve: context => CreateRoleAsync(context))
                .AuthorizeWith(PolicyName.ManageRoles);

            Field<RoleType>(
                    "update" + nameof(Role),
                    arguments: new QueryArguments(
                        new QueryArgument<NonNullGraphType<IdGraphType>>
                        {
                            Name = nameof(Role.Id).ToJsonFormat(),
                            Description = "Идентификатор роли"
                        },
                        new QueryArgument<NonNullGraphType<RoleInputType>>
                        {
                            Name = nameof(Role).ToJsonFormat(),
                            Description = "Параметры редактирования роли"
                        }),
                    description:"Редактирование роли",
                    resolve: context => UpdateRoleAsync(context))
                .AuthorizeWith(PolicyName.ManageRoles);

            Field<BooleanGraphType>(
                    "remove" + nameof(Role),
                    arguments: new QueryArguments(
                        new QueryArgument<NonNullGraphType<IdGraphType>>
                        {
                            Name = nameof(Role.Id).ToJsonFormat(),
                            Description = "Идентификатор роли"
                        }),
                    description: "Удаление роли",
                    resolve: context => RemoveRoleAsync(context))
                .AuthorizeWith(PolicyName.ManageRoles);

            Field<ListGraphType<RoleSortedType>>(
                "sort" + nameof(Role),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ListGraphType<RoleSortingType>>>
                    {
                        Name = nameof(Role).ToJsonFormat(),
                        Description = "Роли для сортировки"
                    }),
                description: "Сортировка ролей",
                resolve: context => SortRoleAsync(context))
                .AuthorizeWith(PolicyName.ManageRoles);
        }

        private async Task<IEnumerable<Role>> SortRoleAsync(IResolveFieldContext context)
        {
            try
            {
                var model = context.GetArgument<List<Role>>("role");

                using var scope = _scopeFactory.CreateScope();

                var roleManager = (RoleManager)scope.ServiceProvider.GetService(typeof(RoleManager));

                var result = await roleManager.SortRolesAsync(model);

                if (result.Success)
                {
                    return result.Result;
                }

                var errors = result.Errors
                    .Select(el => new ExecutionError(el));

                context.Errors.AddRange(errors);

                return result.Result;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка создания роли", ex);

                context.Errors.Add(new ExecutionError("Ошибка создания роли"));

                return null;
            }
        }

        private async Task<Role> CreateRoleAsync(IResolveFieldContext context)
        {
            try
            {
                var keys = context.Keys("role");

                var requestedKeys = context.SubFieldsKeys();

                var model = context.GetArgument<Role>("role");

                using var scope = _scopeFactory.CreateScope();

                var roleManager = (RoleManager)scope.ServiceProvider.GetService(typeof(RoleManager));

                var result = await roleManager.AddRoleAsync(model, keys, requestedKeys);

                if (result.Success)
                {
                    return result.Result;
                }

                var errors = result.Errors
                    .Select(el => new ExecutionError(el));

                context.Errors.AddRange(errors);

                return result.Result;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка создания роли", ex);

                context.Errors.Add(new ExecutionError("Ошибка создания роли"));

                return null;
            }
        }

        private async Task<Role> UpdateRoleAsync(IResolveFieldContext context)
        {
            try
            {
                var keys = context.Keys("role");

                var requestedKeys = context.SubFieldsKeys();

                var id = context.GetArgument<long>("id");

                var model = context.GetArgument<Role>("role");

                using var scope = _scopeFactory.CreateScope();

                var roleManager = (RoleManager)scope.ServiceProvider.GetService(typeof(RoleManager));

                var result = await roleManager.UpdateRoleAsync(id, model, keys, requestedKeys);

                if (result.Success)
                {
                    return result.Result;
                }

                var errors = result.Errors
                    .Select(el => new ExecutionError(el));

                context.Errors.AddRange(errors);

                return result.Result;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка редактирования роли", ex);

                context.Errors.Add(new ExecutionError("Ошибка редактирования роли"));

                return null;
            }
        }

        private async Task<bool> RemoveRoleAsync(IResolveFieldContext context)
        {
            try
            {
                var id = context.GetArgument<long>(nameof(Role.Id).ToJsonFormat());

                using var scope = _scopeFactory.CreateScope();

                var roleManager = (RoleManager)scope.ServiceProvider.GetService(typeof(RoleManager));

                return await roleManager.RemoveRole(id);
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка удаления роли", ex);

                context.Errors.Add(new ExecutionError("Ошибка удаления роли"));

                return false;
            }
        }
    }
}
