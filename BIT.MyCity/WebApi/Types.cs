

using System.Collections.Generic;

namespace BIT.MyCity.WebApi.Types
{
    public abstract class WebApiEntity
    {
        public string name;
        public List<string> fields;
    }

    public class USERWebApiEntity: WebApiEntity {
        public USERWebApiEntity () {
            name = "Users";
            fields = new List<string>() { "id", "name" };
        }
    }

    public static class WebApiTypes
    {
        public static List<WebApiEntity> list = new List<WebApiEntity>() {
            new USERWebApiEntity(),            
        };
    }

}