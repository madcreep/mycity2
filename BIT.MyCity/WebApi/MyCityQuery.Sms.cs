using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Managers;
using BIT.MyCity.Sms.Models;
using BIT.MyCity.WebApi.Types.Sms;
using GraphQL;
using GraphQL.Types;
using GraphQL.Utilities;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddSmsFields()
        {
            Field<SmsBalanceType>(
                    "smsBalance",
                    description: "Запрос баланса СМС сервиса",
                    resolve: context => GetSmsBalanceAsync(context))
                .AuthorizeWith(PolicyName.ManageSettings);
        }

        private async Task<Balance> GetSmsBalanceAsync(IResolveFieldContext context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var smsManager = scope.ServiceProvider.GetRequiredService<SmsManager>();

                var balance = await smsManager.GetBalanceAsync();

                if (balance.Success)
                {
                    return balance;
                }

                context.Errors.Add(new ExecutionError(balance.ErrorMessage));

                return null;
            }
            catch (Exception ex)
            {
                context.Errors.Add(new ExecutionError(ex.Message));

                return null;
            }
        }
    }
}
