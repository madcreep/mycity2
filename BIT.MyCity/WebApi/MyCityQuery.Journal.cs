using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Journal;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddJournalFields()
        {
            Field<ListGraphType<JournalOperationsType>>(
                    "JournalOperations".ToJsonFormat(),
                    description: "Операции журнала изменений",
                    resolve: context => GetJournalOperationsAsync(context))
                .AuthorizeWith(PolicyName.ManageJournal);

            Field<ListGraphType<JournalEntityTagsType>>(
                    "JournalTags".ToJsonFormat(),
                    description: "Тэги журнала изменений",
                    resolve: context => GetJournalTagsAsync(context))
                .AuthorizeWith(PolicyName.ManageJournal);

            Field<PaginationType<JournalItemType, ChangeJournalItem>>(
                    "Journal".ToJsonFormat(),
                    arguments: new QueryArguments(
                        new QueryArgument<SelectionRangeType> { Name = "range", Description = "Сортировка и пагинация" },
                        new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                    description: "Запрос журнала изменений",
                    resolve: context => GetJournalAsync(context))
                .AuthorizeWith(PolicyName.ManageJournal);
        }

        private async Task<JournalEntityTags[]> GetJournalTagsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var manager = scope.ServiceProvider.GetRequiredService<JournalManager>();

                return manager.GetTags();
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка запроса тэгов журнала изменений", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<JournalOperation[]> GetJournalOperationsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var manager = scope.ServiceProvider.GetRequiredService<JournalManager>();

                return manager.GetOperations();
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка запроса типов операций журнала изменений", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<Pagination<ChangeJournalItem>> GetJournalAsync(IResolveFieldContext<object> context)
        {
            try
            {
                var range = context.GetArgument<SelectionRange>("range");

                var filter = context.GetArgument<List<GraphQlQueryFilter>>("filter");

                var keys = context.SubFieldsKeys();

                using var scope = _scopeFactory.CreateScope();

                var manager = scope.ServiceProvider.GetRequiredService<JournalManager>();

                return await manager.GetAsync(range, filter, keys);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка запроса журнала изменений", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }
    }
}
