using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Email;
using GraphQL;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddEmailFields()
        {
            Field<BooleanGraphType>(
                    "createEmail",
                    arguments: new QueryArguments(new QueryArgument<NonNullGraphType<CustomEmailType>>
                    {
                        Name = nameof(CustomEmail).ToJsonFormat(),
                        Description = "Параметры сообщения электронной почты"
                    }),
                    description: "Создание сообщения электронной почты",
                    resolve: context => CreateEmailAsync(context))
                .AuthorizeWith(PolicyName.ManageCustomEmail);
        }

        private async Task<bool> CreateEmailAsync(IResolveFieldContext context)
        {
            try
            {
                var model = context.GetArgument<CustomEmail>(nameof(CustomEmail).ToJsonFormat());

                using var scope = _scopeFactory.CreateScope();

                var emailManager = scope.ServiceProvider.GetRequiredService<EmailManager>();

                var errors = await emailManager.CreateCustomMessage(model);

                if (!errors.Any())
                {
                    return true;
                }

                context.Errors.AddRange(errors.Select(el=>new ExecutionError(el)));

                return false;

            }
            catch (Exception ex)
            {
                _log.Error("Ошибка создания пользовательского сообщения электронной почты", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return false;
            }
        }
    }
}
