using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddRubricatorFields()
        {
            Field<RegionType>(
                name: "create" + nameof(Region),
                description: "создание нового Региона",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<RegionCreateType>>
                { Name = nameof(Region) }),
                resolve: context => CreateRegionAsync(context))
                .AuthorizeWith(PolicyName.ManageRegions);

            Field<RubricType>(
                name: "create" + nameof(Rubric),
                description: "создание новой Рубрики",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<RubricCreateType>>
                { Name = nameof(Rubric) }),
                resolve: context => CreateRubricAsync(context))
                .AuthorizeWith(PolicyName.ManageRubrics);

            Field<TerritoryType>(
                name: "create" + nameof(Territory),
                description: "создание новой Территории",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<TerritoryCreateType>>
                { Name = nameof(Territory) }),
                resolve: context => CreateTerritoryAsync(context))
                .AuthorizeWith(PolicyName.ManageTerritories);

            Field<TopicType>(
                name: "create" + nameof(Topic),
                description: "создание нового Топика",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<TopicCreateType>>
                { Name = nameof(Topic) }),
                resolve: context => CreateTopicAsync(context))
                .AuthorizeWith(PolicyName.ManageTopics);

            Field<RegionType>(
                name: "update" + nameof(Region),
                description: "изменение существующего Региона",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<RegionUpdateType>> { Name = nameof(Region) }),
                resolve: context => UpdateRegionAsync(context))
                //.AuthorizeWith(PolicyName.ManageRegionsWithSubsystem)
                ;

            Field<RubricType>(
                name: "update" + nameof(Rubric),
                description: "изменение существующей Рубрики",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<RubricUpdateType>> { Name = nameof(Rubric) }),
                resolve: context => UpdateRubricAsync(context))
                //.AuthorizeWith(PolicyName.ManageRubricsWithSubsystem)
                ;

            Field<TerritoryType>(
                name: "update" + nameof(Territory),
                description: "изменение существующей Территории",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<TerritoryUpdateType>> { Name = nameof(Territory) }),
                resolve: context => UpdateTerritoryAsync(context))
                //.AuthorizeWith(PolicyName.ManageTerritoriesWithSubsystem)
                ;

            Field<TopicType>(
                name: "update" + nameof(Topic),
                description: "изменение существующего Топика",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<TopicUpdateType>> { Name = nameof(Topic) }),
                resolve: context => UpdateTopicAsync(context))
                //.AuthorizeWith(PolicyName.ManageTopicsWithSubsystem)
                ;
        }
        private async Task<Region> CreateRegionAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var entityName = nameof(Region).ToJsonFormat();
                var updateKeys = context.Keys(entityName);
                var requestedFields = context.SubFieldsKeys();
                IRubricator obj = context.GetArgument<Region>(nameof(Region));
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return (await m.CreateAsync(obj, updateKeys, requestedFields)) as Region;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Region> UpdateRegionAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var updateKeys = context.Keys(nameof(Region).ToJsonFormat());
                var requestedFields = context.SubFieldsKeys();
                var id = context.GetArgument<long>("id");
                var obj = context.GetArgument<Region>(nameof(Region));
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return (await m.UpdateAsync(id, obj, updateKeys, requestedFields)) as Region;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Rubric> CreateRubricAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var entityName = nameof(Rubric).ToJsonFormat();
                var updateKeys = context.Keys(entityName);
                var requestedFields = context.SubFieldsKeys();
                IRubricator obj = context.GetArgument<Rubric>(nameof(Rubric));
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return (await m.CreateAsync(obj, updateKeys, requestedFields)) as Rubric;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Rubric> UpdateRubricAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var updateKeys = context.Keys(nameof(Rubric).ToJsonFormat());
                var requestedFields = context.SubFieldsKeys();
                var id = context.GetArgument<long>("id");
                var obj = context.GetArgument<Rubric>(nameof(Rubric));
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return (await m.UpdateAsync(id, obj, updateKeys, requestedFields)) as Rubric;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Territory> CreateTerritoryAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var entityName = nameof(Territory).ToJsonFormat();
                var updateKeys = context.Keys(entityName);
                var requestedFields = context.SubFieldsKeys();
                IRubricator obj = context.GetArgument<Territory>(nameof(Territory));
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return (await m.CreateAsync(obj, updateKeys, requestedFields)) as Territory;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Territory> UpdateTerritoryAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var requestedFields = context.SubFieldsKeys();
                var updateKeys = context.Keys(nameof(Territory).ToJsonFormat());
                var id = context.GetArgument<long>("id");
                var obj = context.GetArgument<Territory>(nameof(Territory));
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return (await m.UpdateAsync(id, obj, updateKeys, requestedFields)) as Territory;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Topic> CreateTopicAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var entityName = nameof(Topic).ToJsonFormat();
                var updateKeys = context.Keys(entityName);
                var requestedFields = context.SubFieldsKeys();
                IRubricator obj = context.GetArgument<Topic>(nameof(Topic));
                var manager = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return (await manager.CreateAsync(obj, updateKeys, requestedFields)) as Topic;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Topic> UpdateTopicAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var fields = context.Keys(nameof(Topic).ToJsonFormat());
                var requestedFields = context.SubFieldsKeys();
                var id = context.GetArgument<long>("id");
                var obj = context.GetArgument<Topic>(nameof(Topic));
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                //return await m.UpdateDbAsync(id, obj, fields, requestedFields);
                return await m.UpdateAsync(id, obj, fields, requestedFields) as Topic;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
    public sealed partial class MyCityQuery
    {
        private void AddRubricatorFields()
        {
            Field<PaginationType<RegionType, Region>>(
                name: nameof(Region),
                description: "получение списка Регионов",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetRegionsPgAsync(context));

            Field<PaginationType<RubricType, Rubric>>(
                name: nameof(Rubric),
                description: "получение списка Рубрик",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetRubricsPgAsync(context));

            Field<PaginationType<TerritoryType, Territory>>(
                name: nameof(Territory),
                description: "получение списка Территорий",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetTerritoriesPgAsync(context));

            Field<PaginationType<TopicType, Topic>>(
                name: nameof(Topic),
                description: "получение списка Топиков",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetTopicsPgAsync(context));
        }
        private async Task<Pagination<Region>> GetRegionsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return await m.GetRubricatorsAsync<Region>(QueryOptionsParse(context, scope));
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Pagination<Rubric>> GetRubricsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return await m.GetRubricatorsAsync<Rubric>(QueryOptionsParse(context, scope));
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Pagination<Territory>> GetTerritoriesPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return await m.GetRubricatorsAsync<Territory>(QueryOptionsParse(context, scope));
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Pagination<Topic>> GetTopicsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var m = (RubricatorManager)scope.ServiceProvider.GetService(typeof(RubricatorManager));
                return await m.GetRubricatorsAsync<Topic>(QueryOptionsParse(context, scope));
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
}
