using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.GraphQL.Authorization;
using GraphQL;
using GraphQL.Types;
using BIT.MyCity.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using LoginResult = BIT.MyCity.Authorization.Models.LoginResult;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddAuthorizationFields()
        {
            Field<ListGraphType<LoginSystemType>>("loginModes",
                description: "Запрос мнемоник доступных внешних систем авторизации",
                resolve: context => this.GetLoginSystemsAsync(context));

            Field<RedirectUrlType>("loginRedirectUrl",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<RedirectUrlInputType>>
                    {
                        Name = "redirectUrl",
                        Description = "Адрес перернаправления"
                    }
                ),
                description: "Запрос адреса страницы авторизации",
                resolve: context => GetRedirectUrlAsync(context));

            Field<RedirectUrlType>("logoutRedirectUrl",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<RedirectUrlInputType>>
                    {
                        Name = "redirectUrl",
                        Description = "Адрес перернаправления"
                    }
                ),
                description: "Запрос адреса страницы авторизации",
                resolve: context => GetLogoutUrlAsync(context));

            Field<LoginResultType>(
                "login",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<LoginType>>
                    {
                        Name = "login",
                        Description = "Параметры авторизации"
                    }
                ),
                description: "Получение токена авторизации",
                resolve: context => LoginAsync(context));

            Field<LoginResultType>(
                    "updateToken",
                    description: "Обновление токена авторизации",
                    resolve: context => UpdateTokenAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<OperationResultType>(
                "confirmEmail",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ConfirmEmailType>>
                    {
                        Name = "confirmEmail",
                        Description = "Параметры подтверждения электронной почты"
                    }
                ),
                description: "Подтверждение электронной почты",
                resolve: context => ConfirmEmailAsync(context));

            Field<OperationResultType>(
                "restorePassword",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ForgotPasswordType>>
                    {
                        Name = "restorePassword",
                        Description = "Параметры восстановления пароля для пользователей зарегистрированных через формы"
                    }
                ),
                description: "Восстановление забытого пароля",
                resolve: context => ForgotPasswordAsync(context));

            Field<OperationResultType>(
                "sendConfirmEmail",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType>
                    {
                        Name = "id",
                        Description = "Идентификатор пользователя"
                    }
                ),
                description: "Послать электронное письмо подтверждения Email",
                resolve: context => SendEmailConfirmAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
        }

        private async Task<IEnumerable<LoginSystem>> GetLoginSystemsAsync(IResolveFieldContext context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                var modes = authManager.GetAllLoginModes();

                return modes;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса списка систем авторизации", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<RedirectUrl> GetRedirectUrlAsync(IResolveFieldContext context)
        {
            try
            {
                var redirectUrl = context.GetArgument<RedirectUrl>("redirectUrl");

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                redirectUrl = authManager.GetRedirectUrl(redirectUrl);
                    
                return redirectUrl;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса адреса перенаправления", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<RedirectUrl> GetLogoutUrlAsync(IResolveFieldContext context)
        {
            try
            {
                var redirectUrl = context.GetArgument<RedirectUrl>("redirectUrl");

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                redirectUrl = authManager.GetLogoutUrl(redirectUrl);

                return redirectUrl;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса адреса перенаправления", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<LoginResult> LoginAsync(IResolveFieldContext context)
        {
            var loginModel = context.GetArgument<LoginModel>("login");

            if (string.IsNullOrWhiteSpace(loginModel.SubscribeKey))
            {
                return await GetLoginResult(context);
            }

            using var scope = _scopeFactory.CreateScope();

            var httpContextAccessor = scope.ServiceProvider.GetRequiredService<IHttpContextAccessor>();

            var httpContext = httpContextAccessor.HttpContext;

            var httpParam = new HttpRequestParams
            {
                RequestScheme = httpContext.Request.Scheme,
                RequestHost = httpContext.Request.Host.Host,
                RemoteIpAddress = httpContext.Connection.RemoteIpAddress.ToString()
            };

            var param = new object[] { context, httpParam };

            ThreadPool.QueueUserWorkItem((s) =>
            {
                var args = s as object[];

                var task = GetLoginResult(args[0] as IResolveFieldContext, args[1] as HttpRequestParams);

                task.Wait();

            }, param);

            return new LoginResult
            {
                Key = loginModel.SubscribeKey,
                LoginStatus = LoginStatus.InSubscribe
            };
        }

        private async Task<LoginResult> GetLoginResult(IResolveFieldContext context, HttpRequestParams httpContext = null)
        {
            try
            {
                var loginModel = context.GetArgument<LoginModel>("login");

                var requestedFields = context.SubFieldsKeys();

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager) scope.ServiceProvider.GetService(typeof(AuthManager));

                var loginResult = await authManager.LoginAsync(loginModel, requestedFields, httpContext);

                if (!string.IsNullOrWhiteSpace(loginResult.ErrorMessage))
                {
                    context.Errors.Add(new ExecutionError(loginResult.ErrorMessage));
                }

                return loginResult;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса авторизации", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<LoginResult> UpdateTokenAsync(IResolveFieldContext context)
        {
            try
            {
                var requestedFields = context.SubFieldsKeys();

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                var loginResult = await authManager.UpdateToken(requestedFields);

                if (!string.IsNullOrWhiteSpace(loginResult.ErrorMessage))
                {
                    context.Errors.Add(new ExecutionError(loginResult.ErrorMessage));
                }

                return loginResult;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса обновления токена", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<OperationResult> ConfirmEmailAsync(IResolveFieldContext context)
        {
            try
            {
                var confirmEmailModel = context.GetArgument<ConfirmEmailModel>("confirmEmail");

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                var operationResult = await authManager.ConfirmEmail(confirmEmailModel);

                if (operationResult.Success || operationResult.ErrorMessages == null)
                {
                    return operationResult;
                }

                foreach (var errorMessage in operationResult.ErrorMessages)
                {
                    context.Errors.Add(new ExecutionError(errorMessage));
                }

                return operationResult;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса подтверждения электронной почты", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<OperationResult> ForgotPasswordAsync(IResolveFieldContext context)
        {
            try
            {
                var model = context.GetArgument<ForgotPasswordModel>("restorePassword");

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                var operationResult = await authManager.ForgotPasswordAsync(model);

                if (operationResult.Success || operationResult.ErrorMessages == null)
                {
                    return operationResult;
                }

                foreach (var errorMessage in operationResult.ErrorMessages)
                {
                    context.Errors.Add(new ExecutionError(errorMessage));
                }

                return operationResult;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса забытого пароля", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<OperationResult> SendEmailConfirmAsync(IResolveFieldContext context)
        {
            try
            {
                var userId = context.GetArgument<long?>("id");

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                var operationResult = await authManager.SendEmailConfirmAsync(userId);

                if (operationResult.Success || operationResult.ErrorMessages == null)
                {
                    return operationResult;
                }

                foreach (var errorMessage in operationResult.ErrorMessages)
                {
                    context.Errors.Add(new ExecutionError(errorMessage));
                }

                return operationResult;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса забытого пароля", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }
    }
}
