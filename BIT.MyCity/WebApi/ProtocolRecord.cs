using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddProtocolRecordFields()
        {
        }
    }
    public sealed partial class MyCityQuery
    {
        private void AddProtocolRecordFields()
        {
            Field<PaginationType<ProtocolRecordType, ProtocolRecord>>(
                name: nameof(ProtocolRecord),
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => GetProtocolRecordPgAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

        }
        private async Task<Pagination<ProtocolRecord>> GetProtocolRecordPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (ProtocolRecordManager)scope.ServiceProvider.GetService(typeof(ProtocolRecordManager));
                    return await m.GetProtocolRecordAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
}
