using System;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.GraphQL.Authorization;
using GraphQL;
using GraphQL.Types;
using BIT.MyCity.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Authorization;
using LoginResult = BIT.MyCity.Authorization.Models.LoginResult;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddAuthorizationFields()
        {
            Field<OperationResultType>(
                "resetPassword",
                arguments: new QueryArguments(
                  new QueryArgument<NonNullGraphType<ResetPasswordInputType>> { Name = "resetPassword" }
                ),
                description: "Смена пароля",
                resolve: context => ResetPassword(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<OperationResultType>(
                    "restorePassword",
                    arguments: new QueryArguments(
                        new QueryArgument<NonNullGraphType<RestorePasswordType>> { Name = "restorePassword" }
                    ),
                    description: "Восстановление пароля",
                    resolve: context => RestorePasswordAsync(context));

            Field<LoginResultType>(
                name: "register" + nameof(User),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<RegisterModelType>>
                    {
                        Name = nameof(User).ToJsonFormat(),
                        Description = "Параметры регистрируемого пользователя"
                    }
                ),
                description: "Регистрация пользователя",
                resolve: context => RegisterSelfUserAsync(context));
        }

        private async Task<LoginResult> RegisterSelfUserAsync(IResolveFieldContext context)
        {
            try
            {
                var entityName = nameof(User).ToJsonFormat();

                var model = context.GetArgument<RegisterUserModel>(entityName);

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                var loginResult = await authManager.RegisterUserAsync(model, context.Keys(entityName), context.SubFieldsKeys());

                if (!string.IsNullOrWhiteSpace(loginResult.ErrorMessage))
                {
                    context.Errors.Add(new ExecutionError(loginResult.ErrorMessage));
                }

                return loginResult;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка регистрации пользователя", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<OperationResult> ResetPassword(IResolveFieldContext context)
        {
            try
            {
                var loginType = context.GetArgument<ResetPasswordModel>("resetPassword");

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                var operationResult = await authManager.ResetPassword(loginType);

                if (operationResult.ErrorMessages == null)
                {
                    return operationResult;
                }

                foreach (var errorMessage in operationResult.ErrorMessages)
                {
                    context.Errors.Add(new ExecutionError(errorMessage));
                }

                return operationResult;
            }
            catch (Exception ex)
            {
                context.Errors.Add(new ExecutionError(ex.Message));

                return null;
            }
        }

        private async Task<OperationResult> RestorePasswordAsync(IResolveFieldContext context)
        {
            try
            {
                var model = context.GetArgument<RestorePasswordModel>("restorePassword");

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                var operationResult = await authManager.RestorePasswordAsync(model);

                if (operationResult.ErrorMessages == null)
                {
                    return operationResult;
                }

                foreach (var errorMessage in operationResult.ErrorMessages)
                {
                    context.Errors.Add(new ExecutionError(errorMessage));
                }

                return operationResult;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка обработки запроса сброса пароля", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }
    }
}
