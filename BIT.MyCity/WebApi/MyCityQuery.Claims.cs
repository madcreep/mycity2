using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.ApiInterface;
using GraphQL.Types;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types.Claims;
using GraphQL;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddClaimsFields()
        {
            Field<ListGraphType<ClaimInfoType>>(
                    "claimsInfo",
                    description: "Получение информации о доступных разрешениях",
                    resolve: context => GetClaimInfoAsync(context))
                .AuthorizeWith(PolicyName.ManageUsersOrRoles);
        }

        private Task<IEnumerable<ClaimInfoModel>> GetClaimInfoAsync(IResolveFieldContext context)
        {

            return Task.Run(() =>
            {
                try
                {
                    using var scope = _scopeFactory.CreateScope();

                    var claimManager = (ClaimManager)scope.ServiceProvider.GetService(typeof(ClaimManager));

                    return claimManager.ClaimInfos;
                }
                catch (Exception ex)
                {
                    Log.Error("Ошибка запроса информации о клаймах", ex);

                    context.Errors.Add(new ExecutionError("Ошибка запроса информации о клаймах"));

                    return null;
                }
            });
        }
    }
}
