using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using GraphQL;
using GraphQL.Types;
using log4net;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using BIT.MyCity.Services;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery : ObjectGraphType<object>
    {
        public static readonly ILog Log = LogManager.GetLogger(typeof(MyCityQuery));

        private readonly IServiceScopeFactory _scopeFactory;

        public MyCityQuery(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
            Name = "Query";
            AddAuthorizationFields();

            AddUsersFields();

            AddClaimsFields();

            AddRolesFields();

            AddOrganizationsFields();

            AddAppealFields();
            AddAppealExecutorFields();
            AddMessageFields();
            AddRubricatorFields();
            AddLikeFields();
            AddVoteFields();
            AddAppointmentFields();
            AddSubSystemFields();
            AddAttachedFileFields();
            AddSettingsFields();
            AddProtocolRecordFields();
            AddEnumVerbFields();
            AddNewsFields();
            AddVersionFields();

            AddSmsFields();
            AddEmailFields();

            AddAppealStatsFields();
            AddPTokenFields();

            AddJournalFields();

            AddReportsFields();
            AddExtDbAppealFields();

            AddExternalSubsystemFields();
        }

        private void AddExternalSubsystemFields()
        {
            var subsystemService =_scopeFactory.CreateScope().ServiceProvider.GetRequiredService<SubsystemService>();

            foreach (var subsystem in subsystemService.Subsystems)
            {
                subsystem.AddGqlQueryFields(this);
            }
        }

        public static QueryOptions QueryOptionsParse(IResolveFieldContext<object> context, IServiceScope scope)
        {
            var options = context.GetArgument<QueryOptions>("options") ?? new QueryOptions();
            options.RequestedFields = context.SubFieldsKeys();
            options.Range = context.GetArgument<SelectionRange>("range") ?? options.Range;
            options.Filters = context.GetArgument<List<GraphQlQueryFilter>>("filter") ?? options.Filters;
            //options.FiltersNew = context.GetArgument<List<FilterItem>>("filters") ?? options.FiltersNew;
            options.WithDeleted |= (options.Filters?.Where(el => el.WithDeleted.HasValue && el.WithDeleted.Value).FirstOrDefault() != null);
            if (options.ThumbSize != null)
            {
                var hh = (IHttpContextAccessor)scope.ServiceProvider.GetService(typeof(IHttpContextAccessor));
                if (options.ThumbSize != null && !hh.HttpContext.Items.ContainsKey(nameof(QueryOptions.ThumbSize)))
                {
                    hh.HttpContext.Items.Add(nameof(QueryOptions.ThumbSize), $"{options.ThumbSize}");
                }
                if (options.ThumbFormat.HasValue && !hh.HttpContext.Items.ContainsKey(nameof(QueryOptions.ThumbFormat)))
                {
                    hh.HttpContext.Items.Add(nameof(QueryOptions.ThumbFormat), options.ThumbFormat);
                }
                if (options.ThumbSizeTolerance != null && !hh.HttpContext.Items.ContainsKey(nameof(QueryOptions.ThumbSizeTolerance)))
                {
                    hh.HttpContext.Items.Add(nameof(QueryOptions.ThumbSizeTolerance), $"{options.ThumbSizeTolerance}");
                }
            }

            return options;
        }

        public static void ReportErrorMessage(IResolveFieldContext<object> context, Exception ex)
        {
            var r = (new Random()).Next();
            Log.Error($"#{r}:{ex.Message},\n\tInnerException:{ex.InnerException?.Message ?? "No Inner Exception"},\n\tStackTrace:{ex.StackTrace ?? "No Stack Trace"}");
            context.Errors.Add(new ExecutionError($"Ошибка сервера #{r}"));
        }
    }

    public static class ResolveFieldContextExtensions
    {
        public static void ReportErrorMessage(this IResolveFieldContext<object> context, Exception ex, ILog log)
        {
            var r = (new Random()).Next();
            log.Error($"#{r}:{ex.Message},\n\tInnerException:{ex.InnerException?.Message ?? "No Inner Exception"},\n\tStackTrace:{ex.StackTrace ?? "No Stack Trace"}");
            context.Errors.Add(new ExecutionError($"Ошибка сервера #{r}"));
        }
    }
}
