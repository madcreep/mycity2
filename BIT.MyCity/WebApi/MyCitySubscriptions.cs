using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Services;
using BIT.MyCity.WebApi.Types;
using GraphQL;
using GraphQL.Subscription;
using GraphQL.Types;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Authorization;
using Microsoft.AspNetCore.Http;
using LoginResult = BIT.MyCity.Authorization.Models.LoginResult;

namespace BIT.MyCity.WebApi
{
    public class MyCitySubscriptions : ObjectGraphType<object>
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IServiceScopeFactory _scopeFactory;

        public MyCitySubscriptions(IServiceScopeFactory scopeFactory, IServiceProvider servicesProvider)
        {
            _scopeFactory = scopeFactory;
            var gqlSubscriptionService = servicesProvider.GetRequiredService<GQLSubscriptionService>();
            gqlSubscriptionService.SettingChanged += OnObjectChangedEvent;
            gqlSubscriptionService.MessageChanged += OnObjectChangedEvent;
            gqlSubscriptionService.AuthorizeChanged += OnObjectChangedEvent;

            Name = "Subscriptions";
            Description = "Подписки на всякие события";

            FieldSubscribe<SettingsType>(
                name: "new" + nameof(Settings),
                description: "Подписка на обновление настроек",
                arguments: new QueryArguments(
                    new QueryArgument<ListGraphType<StringGraphType>> { Name = "startsWith", Description = "Должно начинаться с" },
                    new QueryArgument<ListGraphType<StringGraphType>> { Name = "in", Description = "Должно совпадать с" }
                ),
                resolve: context => ResolveSettings(context),
                subscribe: context => SubscribeSettings(context)
                );

            FieldSubscribe<MessageType>(
                name: "new" + nameof(Message),
                description: "Подписка на обновление комментариев",
                arguments: new QueryArguments(
                    new QueryArgument<MessageSubscribeSetType> { Name = "subscribe" }
                ),
                resolve: context => ResolveMessage(context),
                subscribe: context => SubscribeMessage(context)
                );

            FieldSubscribe<LoginResultType>(
                name: "login",
                description: "Подписка на авторизацию",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<LoginType>>
                    {
                        Name = "login",
                        Description = "Параметры авторизации"
                    }
                ),
                resolve: context => ResolveAuthorization(context),
                subscribe: context => SubscribeLogin(context)
            );
        }

        private Settings ResolveSettings(IResolveFieldContext context)
        {
            var obj = context.Source as Settings;
            return obj;
        }

        private Message ResolveMessage(IResolveFieldContext context)
        {
            var obj = context.Source as Message;
            return obj;
        }

        private bool SettingsMask(string str, IReadOnlyCollection<string> startWith, ICollection<string> @in)
        {
            var res = true;

            if (startWith != null && startWith.Count > 0)
            {
                res = false;

                if (startWith.Any(str.StartsWith))
                {
                    return true;
                }
            }

            if (@in == null || @in.Count <= 0)
            {
                return res;
            }

            return @in.Contains(str);
        }

        private IObservable<Settings> SubscribeSettings(IResolveEventStreamContext context)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var startsWith = context.GetArgument<List<string>>("startsWith");
                var @in = context.GetArgument<List<string>>("in");
                var um = (UserManager)scope.ServiceProvider.GetService(typeof(UserManager));
                var principal = um.CurrentClaimsPrincipal();
                var rightLevel = SettingsManager.GetRightLevel(principal);
                IObservable<Settings> res = _settingsStream.AsObservable()
                    .Where(el => el.ReadingRightLevel <= rightLevel)
                    .Where(el => SettingsMask(el.Key, startsWith, @in));
                return res;
            }
        }

        private IObservable<Message> SubscribeMessage(IResolveEventStreamContext context)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var obj = context.GetArgument<MessageSubscribeSet>("subscribe");
                if (string.IsNullOrEmpty(obj.EntityType))
                {
                    obj.EntityType = nameof(Message);
                }
                if (obj.EntityType == nameof(Message))
                {
                    var ctx = scope.ServiceProvider.GetRequiredService<DocumentsContext>();
                    var rm = ctx?.Messages
                        .FirstOrDefault(el => el.Id == obj.EntityId);
                    if (rm == null)
                    {
                        throw new Exception($"{obj.EntityType}:{obj.EntityId} not found");
                    }
                    IObservable<Message> res = _messagesStream.AsObservable()
                        .Where(el => MessFilter(el, BaseConstants.GetMessageRootType(rm), rm.Due, obj.Level))
                        ;
                    return res;
                }
                else
                {
                    IObservable<Message> res = _messagesStream.AsObservable()
                        .Where(el => MessFilter(el, obj.EntityType, obj.EntityId, obj.Level))
                        ;
                    return res;
                }
            }
        }
        private bool MessFilter(Message message, string rootType, string due, int level)
        {
            if (BaseConstants.GetMessageRootType(message) != rootType)
                return false;
            if (!message.Due.StartsWith(due))
                return false;
            if (BaseConstants.GetDueLevel(due) + level < BaseConstants.GetDueLevel(message.Due))
                return false;
            return true;
        }

        private bool MessFilter(Message message, string rootType, long rootId, int level)
        {
            if (rootType != BaseConstants.GetMessageRootType(message))
                return false;
            if (rootId != BaseConstants.GetMessageRootId(message))
                return false;
            if (level + 2 < BaseConstants.GetDueLevel(message.Due))
                return false;
            return true;
        }

        private readonly ISubject<Settings> _settingsStream = new ReplaySubject<Settings>(1);

        private ConcurrentStack<Settings> SettingsStack { get; } = new ConcurrentStack<Settings>();

        private readonly ISubject<Message> _messagesStream = new ReplaySubject<Message>(1);

        private ConcurrentStack<Message> MessagesStack { get; } = new ConcurrentStack<Message>();

        private readonly ISubject<LoginResult> _authorizeStream = new ReplaySubject<LoginResult>(1);

        private ConcurrentStack<LoginResult> AuthorizeStack { get; } = new ConcurrentStack<LoginResult>();

        private LoginResult ResolveAuthorization(IResolveFieldContext context)
        {
            var obj = context.Source as LoginResult;
            return obj;
        }

        private IObservable<LoginResult> SubscribeLogin(IResolveEventStreamContext context)
        {
            using var scope = _scopeFactory.CreateScope();

            var loginModel = context.GetArgument<LoginModel>("login");

            if (loginModel == null || string.IsNullOrEmpty(loginModel.SubscribeKey))
            {
                throw new Exception("Подписка невозможна без указания SubscribeKey");
            }

            Log.DebugFormat("Запрос подписки на авторизацию ({0} : {1})", loginModel.LoginSystem.ToString(), loginModel.Code);

            var httpContextAccessor = scope.ServiceProvider.GetRequiredService<IHttpContextAccessor>();

            var httpContext = httpContextAccessor.HttpContext;

            var httpParam = new HttpRequestParams
            {
                RequestScheme = httpContext.Request.Scheme,
                RequestHost = httpContext.Request.Host.Host,
                RemoteIpAddress = httpContext.Connection.RemoteIpAddress.ToString()
            };

            var res = _authorizeStream.AsObservable()
                .Where(el => el.Key == loginModel.SubscribeKey);

            var param = new object[] { context, httpParam };

            ThreadPool.QueueUserWorkItem((s) =>
            {
                Thread.Sleep(100);

                if (!(s is object[] args))
                {
                    return;
                }

                var task = GetLoginResult(args[0] as IResolveFieldContext, args[1] as HttpRequestParams);

                task.Wait();
            }, param);

            return res;
        }

        private async Task<LoginResult> GetLoginResult(IResolveFieldContext context, HttpRequestParams httpContext = null)
        {
            try
            {
                var loginModel = context.GetArgument<LoginModel>("login");

                var requestedFields = context.SubFieldsKeys();

                using var scope = _scopeFactory.CreateScope();

                var authManager = (AuthManager)scope.ServiceProvider.GetService(typeof(AuthManager));

                var loginResult = await authManager.LoginAsync(loginModel, requestedFields, httpContext);

                if (!string.IsNullOrWhiteSpace(loginResult.ErrorMessage))
                {
                    context.Errors.Add(new ExecutionError(loginResult.ErrorMessage));
                }

                return loginResult;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса авторизации", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }


        public void OnObjectChangedEvent(object sender, ObjectChangedArgs args)
        {
            switch (args?.ChangedObject)
            {
                case Settings setting:
                    SettingsStack.Push(setting);
                    _settingsStream.OnNext(setting);
                    break;
                case Message message:
                    MessagesStack.Push(message);
                    _messagesStream.OnNext(message);
                    break;
                case LoginResult loginResult:
                    AuthorizeStack.Push(loginResult);
                    _authorizeStream.OnNext(loginResult);
                    break;
            }
        }
    }
}
