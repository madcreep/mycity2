using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using GraphQL;
using GraphQL.Types;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Roles;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddRolesFields()
        {
            Field<PaginationType<RoleType, Role>>(
                    nameof(Role).ToJsonFormat(),
                    arguments: new QueryArguments(
                        new QueryArgument<SelectionRangeType> {Name = "range", Description = "Сортировка и пагинация"},
                        new QueryArgument<ListGraphType<FilterType>> {Name = "filter", Description = "Фильтр"}),
                    description: "Запрос ролей",
                    resolve: context => GetRolesAsync(context))
                .AuthorizeWith(PolicyName.ManageUsersOrRoles);
        }

        private async Task<Pagination<Role>> GetRolesAsync(IResolveFieldContext<object> context)
        {
            try
            {
                var range = context.GetArgument<SelectionRange>("range");

                var filter = context.GetArgument<List<GraphQlQueryFilter>>("filter");

                var keys = context.SubFieldsKeys();

                using var scope = _scopeFactory.CreateScope();

                var roleManager = (RoleManager)scope.ServiceProvider.GetService(typeof(RoleManager));

                return await roleManager.GetRolesAsync(range, filter, keys);
            }
            catch (Exception ex)
            {
                context.Errors.Add(new ExecutionError(ex.Message));

                return null;
            }
        }
    }
}
