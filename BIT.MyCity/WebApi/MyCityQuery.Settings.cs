using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using GraphQL;
using GraphQL.Types;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddSettingsFields()
        {
            Field<ListGraphType<SettingsType>>(
                name: nameof(Settings),
                arguments: new QueryArguments(
                    new QueryArgument<ListGraphType<StringGraphType>>
                    {
                        Name = "key",
                        Description = "Запрашиваемые ключи настроек",
                        DefaultValue = new List<string>()
                    }
                ),
                resolve: context => GetSettingsAsync(context));

        }
        private async Task<List<Settings>> GetSettingsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var settingsKeyList = context.GetArgument<List<string>>("key");

                var settingsManager = (SettingsManager)scope.ServiceProvider.GetService(typeof(SettingsManager));

                var result = await settingsManager.GetSettingsAsync(settingsKeyList);

                if (result.Success)
                {
                    return result.Result;
                }

                var errors = result.Errors
                    .Select(el => new ExecutionError(el));

                context.Errors.AddRange(errors);

                return result.Result;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса настроек", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }
    }
}
