using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddOrganizationsFields()
        {
            Field<PaginationType<OrganizationType, UserOrganization>>(
                    "current" + nameof(UserOrganization),
                    arguments: new QueryArguments(
                        new QueryArgument<SelectionRangeType> { Name = "range", Description = "Сортировка и пагинация" }),
                    description: "Запрос организаций текущего пользователя",
                    resolve: context => GetCurentUserOrganizationsAsync(context))
                .AuthorizeWith(PolicyName.SubsystemOrganizations);

            Field<PaginationType<OrganizationType, UserOrganization>>(
                    nameof(UserOrganization),
                    arguments: new QueryArguments(
                        new QueryArgument<SelectionRangeType> { Name = "range", Description = "Сортировка и пагинация" },
                        new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                    description: "Запрос организаций текущего пользователя",
                    resolve: context => GetOrganizationsAsync(context))
                .AuthorizeWith(PolicyName.ManageUsersOrModerateOrganizations);
        }



        private async Task<Pagination<UserOrganization>> GetCurentUserOrganizationsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                var range = context.GetArgument<SelectionRange>("range");

                var keys = context.SubFieldsKeys();

                using (var scope = _scopeFactory.CreateScope())
                {
                    var organizationManager = scope.ServiceProvider.GetRequiredService<OrganizationManager>();

                    return await organizationManager.GetCurrentUserOrganizationsAsync(range, keys);
                }
            }
            catch (Exception ex)
            {
                context.Errors.Add(new ExecutionError(ex.Message));

                return null;
            }
        }

        private async Task<Pagination<UserOrganization>> GetOrganizationsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                var range = context.GetArgument<SelectionRange>("range");

                var filter = context.GetArgument<List<GraphQlQueryFilter>>("filter");

                var keys = context.SubFieldsKeys();

                using (var scope = _scopeFactory.CreateScope())
                {
                    var organizationManager = scope.ServiceProvider.GetRequiredService<OrganizationManager>();

                    return await organizationManager.GetOrganizationsAsync(range, filter, keys);
                }
            }
            catch (Exception ex)
            {
                context.Errors.Add(new ExecutionError(ex.Message));

                return null;
            }
        }
    }
}
