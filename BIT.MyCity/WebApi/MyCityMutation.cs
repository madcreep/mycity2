using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using GraphQL;
using GraphQL.Types;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation : ObjectGraphType
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IServiceScopeFactory _scopeFactory;

        public MyCityMutation(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
            Name = "Mutation";
            AddAuthorizationFields();

            AddUsersFields();

            AddRolesFields();

            AddAppealFields();
            AddAppealExecutorFields();
            AddMessageFields();
            AddRubricatorFields();
            AddLikeFields();
            AddVoteFields();
            AddAppointmentFields();
            AddSubSystemFields();
            AddAttachedFileFields();
            AddSettingsFields();
            AddProtocolRecordFields();
            AddEnumVerbFields();
            AddNewsFields();
            AddVersionFields();
            AddPTokenFields();
            AddEmailFields();

            AddReportsFields();

            Field<NonNullGraphType<StringGraphType>>(
                name: "delete",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<EntitySelectorType>> { Name = "entity" }),
                resolve: context => DeleteDbAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
        }

        private async Task<string> DeleteDbAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (IServiceScope scope = _scopeFactory.CreateScope())
                {
                    var _um = (UserManager)scope.ServiceProvider.GetService(typeof(UserManager));
                    if (_um.CurrentClaimsPrincipal().Claims.Any(el => el.Type == ClaimName.SuperAdmin || el.Type == ClaimName.Service))
                    {
                        var ctx = scope.ServiceProvider.GetRequiredService<DocumentsContext>();
                        var entity = context.GetArgument<EntitySelector>("entity");
                        return await BaseConstants.DeleteDbAsync(entity.Name, entity.Id, ctx, scope);
                    }
                    else
                    {
                        return "Access denied";
                    }
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        public static void ReportErrorMessage(IResolveFieldContext<object> context, Exception ex)
        {
            var r = (new Random()).Next();
            _log.Error($"#{r}:{ex.Message},\n\tInnerException:{ex.InnerException?.Message ?? "No Inner Exception"},\n\tStackTrace:{ex.StackTrace ?? "No Stack Trace"}");
            context.Errors.Add(new ExecutionError($"Ошибка сервера #{r} '{ex.Message}'"));
        }

    }
}
