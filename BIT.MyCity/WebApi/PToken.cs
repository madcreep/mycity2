﻿using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddPTokenFields()
        {
            Field<PTokenType>(
                name: "create" + nameof(PToken),
                description: "Регистрация нового токена",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<PTokenCreateType>>
                { Name = nameof(PToken), Description = "Токен" }),
                resolve: context => CreateTokenAsync(context)).AuthorizeWith(PolicyName.Authorized);
            Field<PTokenType>(
                name: "delete" + nameof(PToken),
                description: "Удаление токена",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> { Name = "userId", Description = "Id пользователя" },
                    new QueryArgument<NonNullGraphType<PTokenDeleteType>> { Name = nameof(PToken), Description = "Токен" }
                    ),
                resolve: context => DeleteTokenAsync(context)).AuthorizeWith(PolicyName.Authorized);
        }
        private async Task<PToken> CreateTokenAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(PToken).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<PToken>(entityName);
                    var m = (PTokenManager)scope.ServiceProvider.GetService(typeof(PTokenManager));
                    return await m.CreateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
        private async Task<PToken> DeleteTokenAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(PToken).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<PToken>(entityName);
                    var userId = context.GetArgument<long?>("userId");
                    var m = (PTokenManager)scope.ServiceProvider.GetService(typeof(PTokenManager));
                    return await m.DeleteDbAsync(obj, updateKeys, requestedFields, userId);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
    }
    public sealed partial class MyCityQuery
    {
        private void AddPTokenFields()
        {
            Field<PaginationType<PTokenType, PToken>>(
                name: nameof(PToken),
                description: "Получение списка токенов",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> { Name = "userId", Description = "Id пользователя" },
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetTokensPgAsync(context)).AuthorizeWith(PolicyName.Authorized);
        }
        private async Task<Pagination<PToken>> GetTokensPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var userId = context.GetArgument<long?>("userId");
                    var m = (PTokenManager)scope.ServiceProvider.GetService(typeof(PTokenManager));
                    return await m.GetTokensAsync(QueryOptionsParse(context, scope), userId);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
    }
}
