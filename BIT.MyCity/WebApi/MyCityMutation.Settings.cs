using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using GraphQL;
using GraphQL.Types;
using GraphQL.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddSettingsFields()
        {
            Field<ListGraphType<SettingsType>>(
                name: "set" + nameof(Settings),
                arguments: new QueryArguments(
                    new QueryArgument<ListGraphType<SettingsUpdateType>> { Name = nameof(Settings), Description = "Установка настроек" }),
                resolve: context => UpdateSettingsAsync(context))
                .AuthorizeWith(PolicyName.ManageSettings);

            //Field<BooleanGraphType>(
            //        name: "delete" + nameof(Settings),
            //        arguments: new QueryArguments(
            //            new QueryArgument<ListGraphType<StringGraphType>> { Name = "key", Description = "Ключи удаляемых настроек" }),
            //        resolve: context => DeleteSettingsAsync(context))
            //    .AuthorizeWith(PolicyName.ManageSettings);
        }
        private async Task<List<Settings>> UpdateSettingsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var obj = context.GetArgument<List<Settings>>(nameof(Settings));

                var settingsManager = scope.ServiceProvider.GetRequiredService<SettingsManager>();

                var result = await settingsManager.Set(obj);

                if (result.Success)
                {
                    return result.Result;
                }

                var errors = result.Errors
                    .Select(el => new ExecutionError(el));

                context.Errors.AddRange(errors);

                return result.Result;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка обработки запроса установки настроек", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        //private async Task<bool> DeleteSettingsAsync(IResolveFieldContext<object> context)
        //{
        //    try
        //    {
        //        using var scope = _scopeFactory.CreateScope();

        //        var obj = context.GetArgument<List<string>>("key");

        //        var settingsManager = scope.ServiceProvider.GetRequiredService<SettingsManager>();

        //        var result = await settingsManager.Delete(obj);

        //        if (result.Success)
        //        {
        //            return result.Result;
        //        }

        //        var errors = result.Errors
        //            .Select(el => new ExecutionError(el));

        //        context.Errors.AddRange(errors);

        //        return result.Result;
        //    }
        //    catch (Exception ex)
        //    {
        //        _log.Error("Ошибка обработки запроса удаления настроек", ex);

        //        context.Errors.Add(new ExecutionError("Ошибка сервера"));

        //        return false;
        //    }
        //}
    }
}
