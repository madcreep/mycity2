﻿using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddAppointmentFields()
        {
            Field<AppointmentType>(
                name: "update" + nameof(Appointment),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<AppointmentUpdateType>> { Name = nameof(Appointment) }),
                resolve: context => UpdateAppointmentAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<AppointmentNotificationType>(
                name: "update" + nameof(AppointmentNotification),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<AppointmentNotificationUpdateType>> { Name = nameof(AppointmentNotification) }),
                resolve: context => UpdateAppointmentNotificationAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<AppointmentRangeType>(
                name: "update" + nameof(AppointmentRange),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<AppointmentRangeUpdateType>> { Name = nameof(AppointmentRange) }),
                resolve: context => UpdateAppointmentRangeAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<OfficerType>(
                name: "update" + nameof(Officer),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<OfficerUpdateType>> { Name = nameof(Officer) }),
                resolve: context => UpdateOfficerAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<AppointmentType>(
                name: "create" + nameof(Appointment),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<AppointmentCreateType>> { Name = nameof(Appointment) }),
                resolve: context => CreateAppointmentAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<AppointmentNotificationType>(
                name: "create" + nameof(AppointmentNotification),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<AppointmentNotificationCreateType>> { Name = nameof(AppointmentNotification) }),
                resolve: context => CreateAppointmentNotificationAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<AppointmentRangeType>(
                name: "create" + nameof(AppointmentRange),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<AppointmentRangeCreateType>> { Name = nameof(AppointmentRange) }),
                resolve: context => CreateAppointmentRangeAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<OfficerType>(
                name: "create" + nameof(Officer),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<OfficerCreateType>> { Name = nameof(Officer) }),
                resolve: context => CreateOfficerAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
        }
        private async Task<Appointment> CreateAppointmentAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(Appointment).ToJsonFormat();
                    var obj = context.GetArgument<Appointment>(entityName);
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.CreateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Appointment> UpdateAppointmentAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(Appointment).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<Appointment>(entityName);
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.UpdateDbAsync(id, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<AppointmentNotification> CreateAppointmentNotificationAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(AppointmentNotification).ToJsonFormat();
                    var obj = context.GetArgument<AppointmentNotification>(entityName);
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.CreateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<AppointmentNotification> UpdateAppointmentNotificationAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(AppointmentNotification).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<AppointmentNotification>(entityName);
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.UpdateDbAsync(id, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<AppointmentRange> CreateAppointmentRangeAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(AppointmentRange).ToJsonFormat();
                    var obj = context.GetArgument<AppointmentRange>(entityName);
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.CreateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<AppointmentRange> UpdateAppointmentRangeAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(AppointmentRange).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<AppointmentRange>(entityName);
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.UpdateDbAsync(id, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Officer> CreateOfficerAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(Officer).ToJsonFormat();
                    var obj = context.GetArgument<Officer>(entityName);
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.CreateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Officer> UpdateOfficerAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(Officer).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<Officer>(entityName);
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.UpdateDbAsync(id, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
    public sealed partial class MyCityQuery
    {
        private void AddAppointmentFields()
        {
            Field<PaginationType<AppointmentType, Appointment>>(
                name: nameof(Appointment),
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetAppointmentsPgAsync(context));

            Field<PaginationType<AppointmentNotificationType, AppointmentNotification>>(
                name: nameof(AppointmentNotification),
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetAppointmentNotificationsPgAsync(context));

            Field<PaginationType<AppointmentRangeType, AppointmentRange>>(
                name: nameof(AppointmentRange),
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetAppointmentRangesPgAsync(context));

            Field<PaginationType<OfficerType, Officer>>(
                name: nameof(Officer),
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetOfficersPgAsync(context));
        }
        private async Task<Pagination<Appointment>> GetAppointmentsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.GetAppointmentsAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Pagination<AppointmentNotification>> GetAppointmentNotificationsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.GetAppointmentNotificationsAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Pagination<AppointmentRange>> GetAppointmentRangesPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.GetAppointmentRangesAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Pagination<Officer>> GetOfficersPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (AppointmentManager)scope.ServiceProvider.GetService(typeof(AppointmentManager));
                    return await m.GetOfficersAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
}
