using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BIT.MyCity.WebApi
{
    public class MyCityShema : Schema
    {
        public MyCityShema(IServiceScopeFactory scopeFactory, IServiceProvider servicesProvider)
            : base(servicesProvider)
        {
            Query = new MyCityQuery(scopeFactory);
            Mutation = new MyCityMutation(scopeFactory);
            Subscription = new MyCitySubscriptions(scopeFactory, servicesProvider);
            // Query = new StarWarsQuery( new StarWarsData());
            // Query = resolver.Resolve<TestQuery1>();
            // Mutation = new StarWarsMutation (new StarWarsData());
        }
    }
}
