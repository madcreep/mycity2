﻿using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddNewsFields()
        {
            Field<NewsType>(
                name: "create" + nameof(News),
                description: "Создание новой новости",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<NewsCreateType>>
                { Name = nameof(News), Description = "Новость" }),
                resolve: context => CreateNewsAsync(context))
                .AuthorizeWith(PolicyName.CreateNews);
            Field<NewsType>(
                name: "update" + nameof(News),
                description: "Изменение существующей новости",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "Id новости" },
                    new QueryArgument<NonNullGraphType<NewsUpdateType>> { Name = nameof(News), Description = "Поля новости для изменения" }),
                resolve: context => UpdateNewsAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<NewsType>(
                name: "approve" + nameof(News),
                description: "Принятие к публикации или отклонение новости",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "Id новости" },
                    new QueryArgument<NonNullGraphType<NewsApproveType>> { Name = nameof(NewsApprove), Description = "Поля новости для изменения" }),
                resolve: context => ApproveNewsAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
        }
        private async Task<News> CreateNewsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(News).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<News>(entityName);
                    var m = (NewsManager)scope.ServiceProvider.GetService(typeof(NewsManager));
                    return await m.CreateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

        private async Task<News> UpdateNewsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(News).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<News>(entityName);
                    var m = (NewsManager)scope.ServiceProvider.GetService(typeof(NewsManager));
                    var result = await m.UpdateDbAsync(id, obj, updateKeys, requestedFields);
                    return result;
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
        private async Task<News> ApproveNewsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(NewsApprove).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<NewsApprove>(entityName);
                    var m = (NewsManager)scope.ServiceProvider.GetService(typeof(NewsManager));
                    return await m.ApproveAsync(id, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }

    public sealed partial class MyCityQuery
    {
        private void AddNewsFields()
        {
            Field<PaginationType<NewsType, News>>(
                name: nameof(News),
                description: "Получение списка новостей",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetNewsPgAsync(context));

            Field<PaginationType<NewsType, News>>(
                name: "search" + nameof(News),
                description: "Поиск новостей",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<RangeSearchType> { Name = "range", Description = "Пагинация" },
                    new QueryArgument<StringGraphType> { Name = "query", Description = "Поисковый запрос" }),
                resolve: context => this.SearchNewsPgAsync(context));

        }
        private async Task<Pagination<News>> GetNewsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (NewsManager)scope.ServiceProvider.GetService(typeof(NewsManager));
                    return await m.GetNewsAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Pagination<News>> SearchNewsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var query = context.GetArgument<string>("query");
                    var m = (NewsManager)scope.ServiceProvider.GetService(typeof(NewsManager));
                    return await m.SearchNewsAsync(query, QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
}
