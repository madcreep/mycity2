﻿using BIT.MyCity.Database;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using GraphQL;
using System;
using System.Reflection;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddVersionFields()
        {
        }

    }

    public sealed partial class MyCityQuery
    {
        private void AddVersionFields()
        {
            Field<ApiInformationType>(
                name: nameof(ApiInformation),
                description: "Системная информация серверной части. Версии.",
                resolve: context => GetAPIInformation(context));
        }
        private ApiInformation GetAPIInformation(IResolveFieldContext<object> context)
        {
            try
            {
                var result = new ApiInformation
                {
                    OsDescription = System.Runtime.InteropServices.RuntimeInformation.OSDescription,
                    AspDotnetVersion = Assembly
                        .GetEntryAssembly()?
                        .GetCustomAttribute<System.Runtime.Versioning.TargetFrameworkAttribute>()?
                        .FrameworkName,
                    AppVersion = Assembly.GetEntryAssembly()?.GetCustomAttribute<AssemblyFileVersionAttribute>()?.Version,
                    AppConfiguration = Assembly.GetEntryAssembly()?.GetCustomAttribute<AssemblyConfigurationAttribute>()?.Configuration,
                    AppBuildTime = DateTime.SpecifyKind(BaseConstants.GetBuildDate(Assembly.GetEntryAssembly()), DateTimeKind.Utc),
                    ASPNETCORE_ENVIRONMENT = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"),
                    DbVersion = BaseConstants.Db_Version
                };
                return result;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
    }
}
