using System;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddAppealStatsFields()
        {
            Field<PaginationType<AppealStatsType, AppealStats>>(
                name: nameof(AppealStats),
                description: "Получение статистики",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetAppealsStatsAsync(context));
        }
        private async Task<Pagination<AppealStats>> GetAppealsStatsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (AppealStatsManager)scope.ServiceProvider.GetService(typeof(AppealStatsManager));
                    return await m.GetAppealStatsAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

    }
}
