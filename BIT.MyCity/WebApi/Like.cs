using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddLikeFields()
        {
            Field<ViewReportType>(
                name: "view",
                description: "Увидеть/развидеть",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ViewRequestType>> { Name = "view" }
                    ),
                resolve: context => ViewEntityAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<RegardReportType>(
                name: "regard",
                description: "Поставить оценку",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<RegardRequestType>> { Name = "regard" }
                    ),
                resolve: context => RegardEntityAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<LikeReportType>(
                name: "like",
                description: "Поставить лайк",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<LikeRequestType>> { Name = "like" }
                    ),
                resolve: context => LikeEntityAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<LikeReportType>(
                name: "likeAppeal",
                description: "Поставить лайк Обращению",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<LikeRequestType>> { Name = "like" }
                    ),
                resolve: context => LikeAppealAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<LikeReportType>(
                name: "likeMessage",
                description: "Поставить лайк Сообщению",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<LikeRequestType>> { Name = "like" }
                    ),
                resolve: context => LikeMessageAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
        }
        private async Task<ViewReport> ViewEntityAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var view = context.GetArgument<ViewRequest>("view");
                var ctx = (DocumentsContext)scope.ServiceProvider.GetService(typeof(DocumentsContext));
                var e = await BaseConstants.GetEntityAsync(view.Id, view.Type, ctx);
                if (e == null)
                {
                    throw new Exception($"{view.Type}:{view.Id} not found");
                }
                ViewReport report;
                switch (e)
                {
                    case Appeal _:
                    {
                        var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                        report = await m.ViewAsync(view);
                        break;
                    }
                    case Message _:
                    {
                        var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                        report = await m.ViewAsync(view);
                        break;
                    }
                    case News _:
                    {
                        var m = (NewsManager)scope.ServiceProvider.GetService(typeof(NewsManager));
                        report = await m.ViewAsync(view);
                        break;
                    }
                    default:
                        throw new Exception($"View is not implemented for {view.Type}");
                }
                return report;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

        private async Task<RegardReport> RegardEntityAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var regard = context.GetArgument<RegardRequest>("regard");
                var ctx = (DocumentsContext)scope.ServiceProvider.GetService(typeof(DocumentsContext));
                var e = await BaseConstants.GetEntityAsync(regard.Id, regard.Type, ctx);
                if (e == null)
                {
                    throw new Exception($"{regard.Type}:{regard.Id} not found");
                }
                RegardReport report;
                if (e is Message)
                {
                    var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                    report = await m.RegardAsync(regard);
                }
                else
                {
                    throw new Exception($"Regard is not implemented for {regard.Type}");
                }
                return report;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

        private async Task<LikeReport> LikeEntityAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var like = context.GetArgument<LikeRequest>("like");
                var ctx = (DocumentsContext)scope.ServiceProvider.GetService(typeof(DocumentsContext));
                var e = await BaseConstants.GetEntityAsync(like.Id, like.Type, ctx);
                if (e == null)
                {
                    throw new Exception($"{like.Type}:{like.Id} not found");
                }
                LikeReport report;
                switch (e)
                {
                    case Appeal _:
                    {
                        var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                        report = await m.LikeAsync(like);
                        break;
                    }
                    case Message _:
                    {
                        var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                        report = await m.LikeAsync(like);
                        break;
                    }
                    case News _:
                    {
                        var m = (NewsManager)scope.ServiceProvider.GetService(typeof(NewsManager));
                        report = await m.LikeAsync(like);
                        break;
                    }
                    default:
                        throw new Exception($"Like is not implemented for {like.Type}");
                }
                return report;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

        private async Task<LikeReport> LikeAppealAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var like = context.GetArgument<LikeRequest>("like");
                var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                return await m.LikeAsync(like);
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

        private async Task<LikeReport> LikeMessageAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var like = context.GetArgument<LikeRequest>("like");
                var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                return await m.LikeAsync(like);
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
    public sealed partial class MyCityQuery
    {
        private void AddLikeFields()
        {
        }
    }
}
