using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Reports;
using BIT.MyCity.WebApi.Types.Reports;
using GraphQL;
using GraphQL.Types;
using GraphQL.Utilities;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddReportsFields()
        {
            Field<ListGraphType<ReportSettingsType>>(
                nameof(ReportSettings).ToJsonFormat(),
                arguments: new QueryArguments(
                    new QueryArgument<GuidGraphType> { Name = "reportId", Description = "Идентификатор отчета" }),
                description: "Настройки отчета",
                resolve: GetReportSettingsAsync)
                .AuthorizeWith(PolicyName.ManageReports);

            Field<ExecuteReportResultType>(
                "ExecuteReport".ToJsonFormat(),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<ReportSettingsInputType>> { Name = nameof(ReportSettings), Description = "Настройки отчета" }),
                description: "Получить отчет",
                resolve: ExecuteReportAsync)
                .AuthorizeWith(PolicyName.ManageReports);

            Field<ListGraphType<ReportSavedSettingsType>>(
                    "ReportSavedSettings".ToJsonFormat(),
                    arguments: new QueryArguments(
                        new QueryArgument<NonNullGraphType<GuidGraphType>> { Name = "reportId", Description = "Идентификатор отчета" }),
                    description: "Пользовательские сохраненные параметры",
                    resolve: GetReportSavedSettingAsync)
                .AuthorizeWith(PolicyName.ManageReports);
        }
        
        private async Task<IEnumerable<ReportSettings>> GetReportSettingsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var manager = scope.ServiceProvider.GetRequiredService<ReportManager>();

                var id = context.GetArgument<Guid?>("reportId");

                var requestedFields = context.SubFieldsKeys();

                return await manager.GetReportSettingsAsync(id, requestedFields);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка запроса параметров отчета", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<ExecuteReportResult> ExecuteReportAsync(IResolveFieldContext<object> context)
        {
            var name = "Unknown field";

            try
            {
                using var scope = _scopeFactory.CreateScope();

                var manager = scope.ServiceProvider.GetRequiredService<ReportManager>();

                var reportSettings = context.GetArgument<ReportSettings>(nameof(ReportSettings));
                
                name = reportSettings.Name;

                return await manager.ExecuteReportAsync(reportSettings);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка запроса параметров отчета", ex);

                // context.Errors.Add(new ExecutionError($"Ошибка сервера: {name}: {ex.Message}"));

                return null;
            }
        }

        private async Task<IEnumerable<ReportSavedSettings>> GetReportSavedSettingAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var manager = scope.ServiceProvider.GetRequiredService<ReportManager>();

                var id = context.GetArgument<Guid>("reportId");

                return await manager.GetUserReportSettingsAsync(id);
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка запроса параметров отчета", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }
    }
}
