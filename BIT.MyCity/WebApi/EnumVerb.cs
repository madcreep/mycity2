﻿using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddEnumVerbFields()
        {
            Field<EnumVerbType>(
                name: "create" + nameof(EnumVerb),
                arguments: new QueryArguments(
                    new QueryArgument<EnumVerbCreateType> { Name = nameof(EnumVerb) }),
                resolve: context => CreateEnumVerbAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<EnumVerbType>(
                name: "update" + nameof(EnumVerb),
                description: "Если в теле объекта задан id, то объект ищется по id и остальные поля редактируются. " +
                "Если id не задан, то должна быть задана тройка полей {value, group, subsystemUID} и по ней ищется объект. " +
                "",
                arguments: new QueryArguments(
                    new QueryArgument<EnumVerbUpdateType> { Name = nameof(EnumVerb) }),
                resolve: context => UpdateEnumVerbAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<EnumVerbType>(
                name: "updateByID" + nameof(EnumVerb),
                description: "Обновление по ID",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> { Name = nameof(EnumVerb.Id) },
                    new QueryArgument<EnumVerbUpdateType> { Name = nameof(EnumVerb) }
                    ),
                resolve: context => UpdateEnumVerbByIdAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<EnumVerbType>(
                name: "updateByKey" + nameof(EnumVerb),
                description: "Обновление по тройке полей {value, group, subsystemUID}",
                arguments: new QueryArguments(
                    new QueryArgument<StringGraphType> { Name = nameof(EnumVerb.Value) },
                    new QueryArgument<StringGraphType> { Name = nameof(EnumVerb.Group) },
                    new QueryArgument<StringGraphType> { Name = nameof(EnumVerb.SubsystemUID) },
                    new QueryArgument<EnumVerbUpdateType> { Name = nameof(EnumVerb) }
                    ),
                resolve: context => UpdateEnumVerbByKeyAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<EnumVerbType>(
                name: "write" + nameof(EnumVerb),
                arguments: new QueryArguments(
                    new QueryArgument<EnumVerbCreateType> { Name = nameof(EnumVerb) }),
                resolve: context => WriteEnumVerbAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
        }
        private async Task<EnumVerb> CreateEnumVerbAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(EnumVerb).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<EnumVerb>(entityName);
                    var m = (EnumVerbManager)scope.ServiceProvider.GetService(typeof(EnumVerbManager));
                    return await m.CreateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<EnumVerb> UpdateEnumVerbAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(EnumVerb).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<EnumVerb>(entityName);
                    var m = (EnumVerbManager)scope.ServiceProvider.GetService(typeof(EnumVerbManager));
                    return await m.UpdateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<EnumVerb> UpdateEnumVerbByIdAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(EnumVerb).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>(nameof(EnumVerb.Id).ToJsonFormat());
                    var obj = context.GetArgument<EnumVerb>(entityName);
                    var m = (EnumVerbManager)scope.ServiceProvider.GetService(typeof(EnumVerbManager));
                    return await m.UpdateDbAsync(id, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<EnumVerb> UpdateEnumVerbByKeyAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(EnumVerb).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var value = context.GetArgument<string>(nameof(EnumVerb.Value).ToJsonFormat());
                    var group = context.GetArgument<string>(nameof(EnumVerb.Group).ToJsonFormat());
                    var subsystemUID = context.GetArgument<string>(nameof(EnumVerb.SubsystemUID).ToJsonFormat());
                    var obj = context.GetArgument<EnumVerb>(entityName);
                    var m = (EnumVerbManager)scope.ServiceProvider.GetService(typeof(EnumVerbManager));
                    return await m.UpdateDbAsync(value, group, subsystemUID, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<EnumVerb> WriteEnumVerbAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(EnumVerb).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<EnumVerb>(entityName);
                    var m = (EnumVerbManager)scope.ServiceProvider.GetService(typeof(EnumVerbManager));
                    return await m.WriteDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }

    public sealed partial class MyCityQuery
    {
        private void AddEnumVerbFields()
        {
            Field<PaginationType<EnumVerbType, EnumVerb>>(
                name: nameof(EnumVerb),
                description: "Получение списка записей",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetEnumVerbsPgAsync(context));

        }
        private async Task<Pagination<EnumVerb>> GetEnumVerbsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (EnumVerbManager)scope.ServiceProvider.GetService(typeof(EnumVerbManager));
                    return await m.GetEnumVerbsAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
}
