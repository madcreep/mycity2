using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddMessageFields()
        {
            Field<MessageType>(
                name: "create" + nameof(Message),
                description: "Создать новое сообщение/комментарий. " +
                "Сообщение должно быть привязано (parentId/parentType) " +
                "к обращению (Appeal), сообщению (Message), новости (News) либо к голосованию (Vote)",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<MessageCreateType>>
                { Name = nameof(Message) }),
                resolve: context => CreateMessageAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<MessageType>(
                name: "update" + nameof(Message),
                description: "Изменить существующее сообщение",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<MessageUpdateType>> { Name = nameof(Message) }),
                resolve: context => UpdateMessageAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<PaginationType<MessageType, Message>>(
                name: "approve" + nameof(Message),
                description: "Принятие к публикации или отклонение комментария",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "Id комментария" },
                    new QueryArgument<NonNullGraphType<MessageApproveType>> { Name = nameof(MessageApprove), Description = "Поля комментария для изменения" }),
                resolve: context => ApproveMessageAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<MessageType>(
                name: "delete" + nameof(Message) + "WithComment",
                description: "Удаление сообщения с описанием причины",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "Id комментария" },
                    new QueryArgument<NonNullGraphType<MessageDeleteType>> { Name = nameof(MessageDelete) }),
                resolve: context => DeleteMessageAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<MessageSubscribeGetType>(
                name: nameof(Message) + "Subscribe",
                arguments: new QueryArguments(
                    new QueryArgument<MessageSubscribeSetType> { Name = "subscribe" }),
                resolve: context => MessageSubscribeAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<NonNullGraphType<StringGraphType>>(
                name: nameof(Message) + "UnSubscribe",
                arguments: new QueryArguments(
                    new QueryArgument<MessageSubscribeSetType> { Name = "unsubscribe_mask" }),
                resolve: context => MessageUnSubscribeAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

        }
        private async Task<Message> CreateMessageAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(Message).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<Message>(entityName);
                    var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                    return await m.CreateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Message> UpdateMessageAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(Message).ToJsonFormat();
                    var updatedKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<Message>(entityName);
                    var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                    return await m.UpdateDbAsync(id, obj, updatedKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Pagination<Message>> ApproveMessageAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(MessageApprove).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<MessageApprove>(entityName);
                    var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                    return await m.ApproveAsync(id, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Message> DeleteMessageAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(MessageDelete).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<MessageDelete>(entityName);
                    var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                    return await m.DeleteAsync(id, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<MessageSubscribeGet> MessageSubscribeAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = "subscribe".ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<MessageSubscribeSet>(entityName);
                    var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                    return await m.MessageSubscribeAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<string> MessageUnSubscribeAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = "unsubscribe_mask".ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<MessageUnSubscribe>(entityName);
                    var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                    return await m.MessageUnSubscribeAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }

    public sealed partial class MyCityQuery
    {
        private void AddMessageFields()
        {
            Field<PaginationType<MessageType, Message>>(
                name: nameof(Message),
                description: "Получение списка сообщений/комментариев",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: GetMessagesPgAsync);

            Field<PaginationType<MessageType, Message>>(
                name: "search" + nameof(Message),
                description: "Поиск сообщений/комментариев",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<RangeSearchType> { Name = "range", Description = "Пагинация" },
                    new QueryArgument<StringGraphType> { Name = "query", Description = "Поисковый запрос" }),
                resolve: SearchMessagesPgAsync);

            Field<PaginationType<MessageType, Message>>(
                name: nameof(Message) + "List",
                description: "Получение выбранного сообщения и ответов на него в виде массива",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<NonNullGraphType<LongGraphType>> { Name = "id", Description = "Id объекта" },
                    new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "type", Description = "Тип объекта" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }
                    ),
                resolve: GetMessageListAsync);

            Field<PaginationType<MessageType, Message>>(
                name: nameof(Appeal) + nameof(Message) + "List",
                description: "Получение комментариев к обращению в виде массива",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<NonNullGraphType<LongGraphType>> { Name = "id", Description = "Id обращения (Appeal)" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }
                    ),
                deprecationReason: "Используйте обобщенный метод messageList",
                resolve: GetAppealMessageListAsync);

            Field<PaginationType<MessageType, Message>>(
                name: nameof(Vote) + nameof(Message) + "List",
                description: "Получение комментариев к голосованию в виде массива",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<NonNullGraphType<LongGraphType>> { Name = "id", Description = "Id голосования (Vote)" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }
                    ),
                deprecationReason: "Используйте обобщенный метод messageList",
                resolve: GetVoteMessageListAsync);
            Field<PaginationType<Subs2DueMessagesType, Subs2DueMessages>>(
                name: nameof(Message) + "Subscribes",
                description: "Получение всех подписок на комментарии пользователя",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" }
                    ),
                resolve: GetMessageSubscribesAsync);
        }
        private async Task<Pagination<Message>> GetMessagesPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                return await m.GetMessagesAsync(QueryOptionsParse(context, scope));
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Pagination<Message>> SearchMessagesPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                //var requestFields = context.SubFieldsKeys();
                //var range = context.GetArgument<RangeSearch>("range");
                var query = context.GetArgument<string>("query");
                var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                return await m.SearchMessagesAsync(query, QueryOptionsParse(context, scope));
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Pagination<Message>> GetMessageListAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var id = context.GetArgument<long>("id");
                var type = context.GetArgument<string>("type");
                var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                return await m.GetMessageListAsync(id, type, QueryOptionsParse(context, scope));
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Pagination<Message>> GetAppealMessageListAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var id = context.GetArgument<long>("id");
                var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                var ctx = (DocumentsContext)scope.ServiceProvider.GetService(typeof(DocumentsContext));
                return await m.GetEntityMessageListAsync(id, QueryOptionsParse(context, scope), ctx.Appeals);
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Pagination<Subs2DueMessages>> GetMessageSubscribesAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                return await m.GetMessageSubscribesAsync(QueryOptionsParse(context, scope));
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Pagination<Message>> GetVoteMessageListAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var id = context.GetArgument<long>("id");
                var m = (MessageManager)scope.ServiceProvider.GetService(typeof(MessageManager));
                var ctx = (DocumentsContext)scope.ServiceProvider.GetService(typeof(DocumentsContext));
                return await m.GetEntityMessageListAsync(id, QueryOptionsParse(context, scope), ctx.Votes);
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
}
