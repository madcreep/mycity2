﻿using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddAppealExecutorFields()
        {
            Field<AppealExecutorType>(
                name: "create" + nameof(AppealExecutor),
                description: "Создание нового исполнителя",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<AppealExecutorCreateType>>
                { Name = nameof(AppealExecutor), Description = "Исполнитель" }),
                resolve: context => CreateAppealExecutorAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<AppealExecutorType>(
                name: "update" + nameof(AppealExecutor),
                description: "Изменение существующего исполнителя",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "Id исполнителя" },
                    new QueryArgument<NonNullGraphType<AppealExecutorUpdateType>> { Name = nameof(AppealExecutor), Description = "Поля исполнителя для изменения" }),
                resolve: context => UpdateAppealExecutorAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
        }
        private async Task<AppealExecutor> CreateAppealExecutorAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(AppealExecutor).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<AppealExecutor>(nameof(AppealExecutor));
                    var m = (AppealExecutorManager)scope.ServiceProvider.GetService(typeof(AppealExecutorManager));
                    return await m.CreateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<AppealExecutor> UpdateAppealExecutorAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(AppealExecutor).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<AppealExecutor>(entityName);
                    var m = (AppealExecutorManager)scope.ServiceProvider.GetService(typeof(AppealExecutorManager));
                    return await m.UpdateDbAsync(id, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }

    public sealed partial class MyCityQuery
    {
        private void AddAppealExecutorFields()
        {
            Field<PaginationType<AppealExecutorType, AppealExecutor>>(
                name: nameof(AppealExecutor),
                description: "Получение списка исполнителей",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetAppealExecutorsPgAsync(context));

        }
        private async Task<Pagination<AppealExecutor>> GetAppealExecutorsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (AppealExecutorManager)scope.ServiceProvider.GetService(typeof(AppealExecutorManager));
                    return await m.GetAppealExecutorsAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
    }
}
