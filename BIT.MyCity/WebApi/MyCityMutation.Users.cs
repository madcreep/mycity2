using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.ApiInterface.Models;
using GraphQL;
using GraphQL.Types;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Authorization;
using BIT.MyCity.WebApi.Types.Users;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddUsersFields()
        {
            Field<UserType>(
                    name: "create" + nameof(User),
                    arguments: new QueryArguments(
                        new QueryArgument<NonNullGraphType<RegisterModelType>>
                        {
                            Name = nameof(User).ToJsonFormat(),
                            Description = "Параметры создаваемого пользователя"
                        }
                    ),
                    description:"Создание пользователя",
                    resolve: context => RegisterUserAsync(context))
                .AuthorizeWith(PolicyName.ManageUser);

            Field<UserType>(
                    name: "update" + nameof(User),
                    arguments: new QueryArguments(
                        new QueryArgument<IdGraphType>
                        {
                            Name = nameof(User.Id).ToJsonFormat(),
                            Description = "Идентификатор редактируемого пользователя"
                        },
                        new QueryArgument<NonNullGraphType<UserInputType>>
                        {
                            Name = nameof(User).ToJsonFormat(),
                            Description = "Редактируемые значения"
                        }),
                    description: "Редактирование пользователя",
                    resolve: context => UpdateUserAsync(context))
                .AuthorizeWith(PolicyName.LoginSystemForm);

            Field<OperationResultType>(
                    name: "confirmPhone",
                    description: "Отправлка СМС подтверждения телефонного номера",
                    arguments: new QueryArguments(
                        new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "phoneId", Description = "Идентификатор номера телефона" },
                        new QueryArgument<NonNullGraphType<StringGraphType>> { Name = "code", Description = "Код подтверждения номера телефона" }),
                    resolve: context => ConfirmPhoneAsync(context))
                .AuthorizeWith(PolicyName.LoginSystemForm);
        }

        private async Task<User> RegisterUserAsync(IResolveFieldContext context)
        {
            try
            {
                var entityName = nameof(User).ToJsonFormat();

                var model = context.GetArgument<RegisterUserModel>(nameof(User).ToJsonFormat());

                using var scope = _scopeFactory.CreateScope();

                var userManager = (UserManager)scope.ServiceProvider.GetService(typeof(UserManager));

                var result = await userManager.CreateUserAsync(model, context.Keys(entityName), context.SubFieldsKeys());

                if (result.Success)
                {
                    return result.Result;
                }

                var errors = result.Errors
                    .Select(el => new ExecutionError(el));

                context.Errors.AddRange(errors);

                return result.Result;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка обработки запроса установки настроек", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<User> UpdateUserAsync(IResolveFieldContext context)
        {
            try
            {
                var entityName = nameof(User).ToJsonFormat();

                var id = context.GetArgument<long?>(nameof(User.Id).ToJsonFormat());

                var model = context.GetArgument<User>(entityName);

                var updateKeys = context.Keys(entityName);

                var requestKeys = context.SubFieldsKeys();

                using var scope = _scopeFactory.CreateScope();

                var userManager = (UserManager)scope.ServiceProvider.GetService(typeof(UserManager));

                var result = await userManager.UpdateUserAsync(id, model, updateKeys, requestKeys);

                if (result.Success)
                {
                    return result.Result;
                }

                var errors = result.Errors
                    .Select(el => new ExecutionError(el));

                context.Errors.AddRange(errors);

                return result.Result;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка обработки запроса установки настроек", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<OperationResult> ConfirmPhoneAsync(IResolveFieldContext context)
        {
            try
            {
                var phoneId = context.GetArgument<long>("phoneId");

                var code = context.GetArgument<string>("code");

                using var scope = _scopeFactory.CreateScope();

                var phoneManager = scope.ServiceProvider.GetRequiredService<PhoneManager>();

                var result = await phoneManager.ConfirmPhone(phoneId, code);

                if (!result.Success)
                {
                    context.Errors.AddRange(result.ErrorMessages
                        .Select(el => new ExecutionError(el)));
                }

                return result;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка подтверждения телефонного номера", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }
    }
}
