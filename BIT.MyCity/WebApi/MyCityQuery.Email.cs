using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Email;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddEmailFields()
        {
            Field<PaginationType<EmailTemplateType, EmailTemplate>>(
                    name: nameof(EmailTemplate).ToJsonFormat(),
                    description: "Получение списка шаблонов Email",
                    arguments: new QueryArguments(
                        new QueryArgument<SelectionRangeType> { Name = "range", Description = "Сортировка и пагинация" },
                        new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                    resolve: context => GetEmailTemplateAsync(context))
                .AuthorizeWith(PolicyName.ManageSettings);
        }

        private async Task<Pagination<EmailTemplate>> GetEmailTemplateAsync(IResolveFieldContext context)
        {
            try
            {
                var range = context.GetArgument<SelectionRange>("range");

                var filter = context.GetArgument<List<GraphQlQueryFilter>>("filter");

                var requestedFields = context.SubFieldsKeys();

                using var scope = _scopeFactory.CreateScope();

                var emailManager = scope.ServiceProvider.GetRequiredService<EmailManager>();

                var result = await emailManager.GetTemplates(range, filter, requestedFields);

                return result;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса шаблонов Email", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }
    }
}
