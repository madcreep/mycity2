using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddVoteFields()
        {
            Field<VoteRootType>(
                name: "create" + nameof(VoteRoot),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<VoteRootCreateType>> { Name = nameof(VoteRoot) }),
                resolve: context => CreateVoteRootAsync(context))
                .AuthorizeWith(PolicyName.CreateVoting);

            Field<VoteType>(
                name: "create" + nameof(Vote),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<VoteCreateType>> { Name = nameof(Vote) }),
                resolve: context => CreateVoteAsync(context))
                .AuthorizeWith(PolicyName.CreateVoting);

            Field<VoteItemType>(
                name: "create" + nameof(VoteItem),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<VoteItemCreateType>> { Name = nameof(VoteItem) }),
                resolve: context => CreateVoteItemAsync(context))
                .AuthorizeWith(PolicyName.ModerateVoting);

            Field<VoteType>(
                name: "voteAction",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<VoteActionType>> { Name = nameof(VoteAction) }
                    ),
                resolve: context => VoteActionAsync(context))
                .AuthorizeWith(PolicyName.Vote);

            Field<PaginationType<VoteType, Vote>>(
                name: "votesAction",
                arguments: new QueryArguments(
                    new QueryArgument<ListGraphType<VoteActionType>> { Name = nameof(VoteAction) }
                    ),
                resolve: context => VotesActionAsync(context))
                .AuthorizeWith(PolicyName.Vote);

            Field<VoteRootType>(
                name: "update" + nameof(VoteRoot),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<VoteRootUpdateType>> { Name = nameof(VoteRoot) }),
                resolve: context => UpdateVoteRootAsync(context))
                .AuthorizeWith(PolicyName.ModerateVoting);

            Field<VoteType>(
                name: "update" + nameof(Vote),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<VoteUpdateType>> { Name = nameof(Vote) }),
                resolve: context => UpdateVoteAsync(context))
                .AuthorizeWith(PolicyName.ModerateVoting);

            Field<VoteItemType>(
                name: "update" + nameof(VoteItem),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" },
                    new QueryArgument<NonNullGraphType<VoteItemUpdateType>> { Name = nameof(VoteItem) }),
                resolve: context => UpdateVoteItemAsync(context))
                .AuthorizeWith(PolicyName.ModerateVoting);

            Field<VoteRootType>(
                name: "voteStart",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" }),
                resolve: context => VoteStartAsync(context))
                .AuthorizeWith(PolicyName.ManageVoting);

            Field<VoteRootType>(
                name: "voteStop",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id" }),
                resolve: context => VoteStopAsync(context))
                .AuthorizeWith(PolicyName.ManageVoting);

        }
        private async Task<VoteRoot> CreateVoteRootAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var updatedFields = context.Keys(nameof(VoteRoot).ToJsonFormat());
                var requestedFields = context.SubFieldsKeys();
                var obj = context.GetArgument<VoteRoot>(nameof(VoteRoot));
                var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                return await m.CreateDbAsync(obj, updatedFields, requestedFields);
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Vote> CreateVoteAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedFields = context.Keys(nameof(Vote).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<Vote>(nameof(Vote));
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.CreateDbAsync(obj, updatedFields, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<VoteItem> CreateVoteItemAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedFields = context.Keys(nameof(VoteItem).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<VoteItem>(nameof(VoteItem));
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.CreateDbAsync(obj, updatedFields, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<VoteRoot> UpdateVoteRootAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedFields = context.Keys(nameof(VoteRoot).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<VoteRoot>(nameof(VoteRoot));
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.UpdateDbAsync(id, obj, updatedFields, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Vote> UpdateVoteAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedFields = context.Keys(nameof(Vote).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<Vote>(nameof(Vote));
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.UpdateDbAsync(id, obj, updatedFields, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<VoteItem> UpdateVoteItemAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedFields = context.Keys(nameof(VoteItem).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<VoteItem>(nameof(VoteItem));
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.UpdateDbAsync(id, obj, updatedFields, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<VoteRoot> VoteStartAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedFields = context.Keys(nameof(Vote).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.VoteStartAsync(id, updatedFields, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
        private async Task<VoteRoot> VoteStopAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedFields = context.Keys(nameof(Vote).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.VoteStopAsync(id, updatedFields, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Vote> VoteActionAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedFields = context.Keys(nameof(Vote).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<VoteAction>(nameof(VoteAction).ToJsonFormat());
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.VoteActionAsync(obj, updatedFields, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Pagination<Vote>> VotesActionAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedFields = context.Keys(nameof(Vote).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<List<VoteAction>>(nameof(VoteAction).ToJsonFormat());
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.VotesActionAsync(obj, updatedFields, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
    public sealed partial class MyCityQuery
    {
        private void AddVoteFields()
        {
            Field<PaginationType<VoteRootType, VoteRoot>>(
                name: nameof(VoteRoot),
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetVoteRootsPgAsync(context)
                );

            Field<PaginationType<VoteType, Vote>>(
                name: nameof(Vote),
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetVotesPgAsync(context)
                );

            Field<PaginationType<VoteItemType, VoteItem>>(
                name: nameof(VoteItem),
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => this.GetVoteItemsPgAsync(context)
                );
        }
        private async Task<Pagination<VoteRoot>> GetVoteRootsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.GetVoteRootsAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Pagination<Vote>> GetVotesPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.GetVotesAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Pagination<VoteItem>> GetVoteItemsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (VoteManager)scope.ServiceProvider.GetService(typeof(VoteManager));
                    return await m.GetVoteItemsAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

    }
}
