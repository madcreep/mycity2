using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BIT.MyCity.WebApi.Types.Filter;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddAppealFields()
        {
            Field<AppealType>(
                name: "create" + nameof(Appeal),
                description: "Создание нового обращения",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<AppealCreateType>>
                { Name = nameof(Appeal), Description = "Обращение" }),
                resolve: context => CreateAppealAsync(context));
            Field<AppealType>(
                name: "createFB" + nameof(Appeal),
                description: "Создание нового сообщения обратной связи",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<AppealFBCreateType>>
                { Name = nameof(Appeal), Description = "Сообщение обратной связи" }),
                resolve: context => CreateAppealFBAsync(context));
            Field<AppealType>(
                name: "update" + nameof(Appeal),
                description: "Изменение существующего обращения",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "Id обращения" },
                    new QueryArgument<NonNullGraphType<AppealUpdateType>> { Name = nameof(Appeal), Description = "Поля обращения для изменения" }),
                resolve: context => UpdateAppealAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<AppealType>(
                    name: "register" + nameof(Appeal),
                    description: "Изменение существующего обращения",
                    arguments: new QueryArguments(
                        new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "Id обращения" }),
                    resolve: context => RegisterAppealAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<AppealType>(
                name: "approve" + nameof(Appeal),
                description: "Принятие к публикации или отклонение обращения",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "Id обращения" },
                    new QueryArgument<NonNullGraphType<AppealApproveType>> { Name = nameof(AppealApprove), Description = "Поля обращения для изменения" }),
                resolve: context => ApproveAppealAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
        }
        private async Task<Appeal> CreateAppealAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(Appeal).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<Appeal>(entityName);
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    return await m.CreateDbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

        private async Task<Appeal> CreateAppealFBAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(Appeal).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<Appeal>(entityName);
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    return await m.CreateDbFbAsync(obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

        private async Task<Appeal> UpdateAppealAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(Appeal).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<Appeal>(entityName);
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    var result = await m.UpdateDbAsync(id, obj, updateKeys, requestedFields);
                    return result;
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

        private async Task<Appeal> RegisterAppealAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(Appeal).ToJsonFormat();
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    var result = await m.RegisterAppealAsync(id, requestedFields);
                    return result;
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

        private async Task<Appeal> ApproveAppealAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var entityName = nameof(AppealApprove).ToJsonFormat();
                    var updateKeys = context.Keys(entityName);
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<AppealApprove>(entityName);
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    return await m.ApproveAsync(id, obj, updateKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }

    public sealed partial class MyCityQuery
    {
        private void AddAppealFields()
        {
            Field<PaginationType<AppealType, Appeal>>(
                name: nameof(Appeal),
                description: "Получение списка обращений",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => GetAppealsPgAsync(context));
            Field<PaginationType<AppealsStatByGroupType, AppealsStatBy>>(
                name: nameof(AppealsStatBy),
                description: "Получение статистики по рубрикаторам и датам",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" },
                    new QueryArgument<IntGraphType> { Name = "offsetHours", Description = "Сдвиг временных меток" }
                    ),
                resolve: context => GetAppealsStatByGroupPgAsync(context));
            Field<PaginationType<AppealType, Appeal>>(
                name: $"{nameof(Appeal)}WhereModerator",
                description: "Получение списка обращений с указанным модератором",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> { Name = "id", Description = "ID модератора" },
                    new QueryArgument<ListGraphType<StringGraphType>> { Name = "subsystemUIDs", Description = "Подсистемы" },
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => GetAppealsWhereModeratorPgAsync(context));
            Field<PaginationType<AppealType, Appeal>>(
                name: $"{nameof(Appeal)}WhereExecutor",
                description: "Получение списка обращений с указанным исполнителем",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> { Name = "id", Description = "ID исполнителя" },
                    new QueryArgument<ListGraphType<StringGraphType>> { Name = "subsystemUIDs", Description = "Подсистемы" },
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: context => GetAppealsWhereExecutorPgAsync(context));
            Field<PaginationType<AppealType, Appeal>>(
                name: nameof(Appeal) + "FB",
                description: "Получение списка сообщений обратной связи",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" },
                    new QueryArgument<StringGraphType> { Name = "email", Description = "Email" }
                    ),
                resolve: context => GetAppealsFbPgAsync(context));

            Field<PaginationType<AppealType, Appeal>>(
                name: "search" + nameof(Appeal),
                description: "Поиск обращений",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<RangeSearchType> { Name = "range", Description = "Пагинация" },
                    new QueryArgument<StringGraphType> { Name = "query", Description = "Поисковый запрос" }),
                resolve: context => SearchAppealsPgAsync(context));
            Field<PaginationType<AppealType, Appeal>>(
                name: "fts" + nameof(Appeal),
                description: "Поиск обращений",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<RangeSearchType> { Name = "range", Description = "Пагинация" },
                    new QueryArgument<StringGraphType> { Name = "query", Description = "Поисковый запрос" }),
                resolve: context => FtsAppealsPgAsync(context));

            Field<PaginationType<AppealStatusType, AppealStatus>>(
                name: $"check{nameof(AppealStatus)}",
                description: "Получение статуса обращения по номеру и дате",
                arguments: new QueryArguments(
                    new QueryArgument<StringGraphType> { Name = "number", Description = "Номер обращения" },
                    new QueryArgument<DateGraphType> { Name = "date", Description = "Дата обращения" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Пагинация" }),
                resolve: GetAppealsStatusAsync);

            Field<BooleanGraphType>(
                name: $"resend{nameof(AppealStatus)}Email",
                description: "Отправка статуса обращения на Email",
                arguments: new QueryArguments(
                    new QueryArgument<IdGraphType> { Name = "id", Description = "Идентификатор обращения" }),
                resolve: ResendAppealsStatusEmailAsync);


        }

        private async Task<bool> ResendAppealsStatusEmailAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var appealId = context.GetArgument<long>("id");
                var requestedFields = context.SubFieldsKeys();
                var manager = scope.ServiceProvider.GetRequiredService<AppealManager>();
                return await manager.ResendAppealsStatusEmailAsync(appealId);
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return false;
            }
        }

        private async Task<Pagination<AppealStatus>> GetAppealsStatusAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var number = context.GetArgument<string>("number");
                var date = context.GetArgument<DateTime>("date");
                var range = context.GetArgument<SelectionRange>("range");
                var requestedFields = context.SubFieldsKeys();
                var manager = scope.ServiceProvider.GetRequiredService<AppealManager>();
                return await manager.GetAppealsStatusAsync(number, date, range, requestedFields);
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }

        private async Task<Pagination<Appeal>> GetAppealsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                var appealsAsync = await m.GetAppealsAsync(QueryOptionsParse(context, scope));
                return appealsAsync;
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
        private async Task<Pagination<AppealsStatBy>> GetAppealsStatByGroupPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var offsetHours = context.GetArgument<int>("offsetHours");
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    return await m.GetAppealsStatByGroupAsync(QueryOptionsParse(context, scope), offsetHours);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
        private async Task<Pagination<Appeal>> GetAppealsWhereModeratorPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    var id = context.GetArgument<long>("id");
                    var subsystemUIDs = context.GetArgument<List<string>>("subsystemUIDs");
                    return await m.GetAppealsWhereModeratorAsync(id, subsystemUIDs, QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
        private async Task<Pagination<Appeal>> GetAppealsWhereExecutorPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    var id = context.GetArgument<long>("id");
                    var subsystemUIDs = context.GetArgument<List<string>>("subsystemUIDs");
                    return await m.GetAppealsWhereExecutorAsync(id, subsystemUIDs, QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
        private async Task<Pagination<Appeal>> GetAppealsFbPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    var email = context.GetArgument<string>("email");
                    return await m.GetAppealsFbAsync(QueryOptionsParse(context, scope), email);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Pagination<Appeal>> SearchAppealsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var query = context.GetArgument<string>("query");
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    return await m.SearchAppealsAsync(query, QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<Pagination<Appeal>> FtsAppealsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var query = context.GetArgument<string>("query");
                    var m = (AppealManager)scope.ServiceProvider.GetService(typeof(AppealManager));
                    return await m.FtsAppealsAsync(query, QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        
    }
}
