using BIT.MyCity.Model;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Journal
{
    public class JournalEntityTagsType : ObjectGraphType<JournalEntityTags>
    {
        public JournalEntityTagsType()
        {
            Name = nameof(JournalEntityTagsType);
            Field(d => d.Tag)
                .Description("Имя сущности");
            Field(d => d.Name)
                .Description("Назване сущности");
            Field(d => d.PropertyTags, type: typeof(ListGraphType<JournalTagsType>))
                .Description("Тэги свойств");
        }
    }
}
