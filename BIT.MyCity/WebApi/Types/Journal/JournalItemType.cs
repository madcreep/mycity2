using BIT.MyCity.Database;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Journal
{
    public class JournalItemType : ObjectGraphType<ChangeJournalItem>
    {
        public JournalItemType()
        {
            Name = nameof(JournalItemType);
            Field(d => d.Timestamp, type: typeof(DateTimeGraphType))
                .Description("Штамп времени");
            Field(d => d.User, true, typeof(UserType))
                .Description("Пользователь");
            Field(d => d.Operation, type: typeof(EntityOperationEnumType))
                .Description("Тип операции");
            Field(d => d.OperationName)
                .Description("Название операции");
            Field(d => d.EntityName)
                .Description("Имя сущности");
            Field(d => d.FriendlyName)
                .Description("Название сущности");
            Field(d => d.EntityKey)
                .Description("Ключ сущности");
            Field(d => d.Properties, type: typeof(ListGraphType<JournalPropertiesType>))
                .Description("Измененные свойства");
        }
    }
}
