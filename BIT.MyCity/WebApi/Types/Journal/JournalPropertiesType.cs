using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Journal
{
    public class JournalPropertiesType : ObjectGraphType<ChangeJournalProperties>
    {
        public JournalPropertiesType()
        {
            Name = nameof(JournalPropertiesType);
            Field(d => d.Tag)
                .Description("Имя поля");
            Field(d => d.FriendlyName)
                .Description("Назване свойства");
            Field(d => d.OldValue, true)
                .Description("Старое значение свойства");
            Field(d => d.NewValue, true)
                .Description("Новое значение свойства");
            Field(d => d.NewValueFriendly, true)
                .Description("Новое значение свойства (дружественное)");
        }
    }
}
