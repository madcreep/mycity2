using BIT.MyCity.Model;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Journal
{
    public class EntityOperationEnumType : EnumerationGraphType<EntityOperation>
    {
    }
}
