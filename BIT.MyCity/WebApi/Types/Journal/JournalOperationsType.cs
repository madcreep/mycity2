using BIT.MyCity.Model;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Journal
{
    public class JournalOperationsType : ObjectGraphType<JournalOperation>
    {
        public JournalOperationsType()
        {
            Name = nameof(JournalOperationsType);
            Field(d => d.Operation, type: typeof(EntityOperationEnumType))
                .Description("Тип операции");
            Field(d => d.OperationName)
                .Description("Назване операции");
        }
    }
}
