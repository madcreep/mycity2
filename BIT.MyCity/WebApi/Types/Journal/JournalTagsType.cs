using BIT.MyCity.Model;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Journal
{
    public class JournalTagsType : ObjectGraphType<JournalTags>
    {
        public JournalTagsType()
        {
            Name = nameof(JournalTagsType);
            Field(d => d.Tag)
                .Description("Имя поля");
            Field(d => d.Name)
                .Description("Назване свойства");
        }
    }
}
