﻿using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class PTokenType : ObjectGraphType<PToken>
    {
        public PTokenType()
        {
            Name = nameof(PToken);
            Field(d => d.Id, type: typeof(IdGraphType)).Description("Id объекта");
            Field(d => d.CreatedAt, type: typeof(DateTimeGraphType)).Description("Дата создания объекта");
            Field(d => d.UpdatedAt, type: typeof(DateTimeGraphType)).Description("Дата изменения объекта");
            Field(d => d.ExpiredAt, type: typeof(DateTimeGraphType)).Description("Дата протухания объекта");
            Field(d => d.DeviceType).Description("");
            Field(d => d.Value).Description("");
        }
    }

    public class PTokenCreateType : InputObjectGraphType<PToken>
    {
        public PTokenCreateType()
        {
            Name = "create" + nameof(PToken);
            Field(d => d.DeviceType, nullable: false).Description("");
            Field(d => d.Value, nullable: false).Description("");
            Field(d => d.ExpiredAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Дата протухания объекта");
        }
    }
    public class PTokenDeleteType : InputObjectGraphType<PToken>
    {
        public PTokenDeleteType()
        {
            Name = "delete" + nameof(PToken);
            Field(d => d.Value, nullable: false).Description("");
        }
    }
}
