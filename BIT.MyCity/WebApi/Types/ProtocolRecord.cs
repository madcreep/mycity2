﻿using BIT.MyCity.Database;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class ProtocolRecordType : BaseReadType<ProtocolRecord>
    {
        internal static void InitCommonFields(ComplexGraphType<ProtocolRecord> obj)
        {
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<ProtocolRecord> obj)
        {
        }
        internal static void InitReadOnlyFields(ComplexGraphType<ProtocolRecord> obj)
        {
            obj.Field(d => d.User, nullable: true, type: typeof(UserType));
            obj.Field(d => d.Appeal, nullable: true, type: typeof(AppealType));
            obj.Field(d => d.AppealExecutor, nullable: true, type: typeof(AppealExecutorType));
            obj.Field(d => d.Appointment, nullable: true, type: typeof(AppointmentType));
            obj.Field(d => d.AppointmentNotification, nullable: true, type: typeof(AppointmentNotificationType));
            obj.Field(d => d.AppointmentRange, nullable: true, type: typeof(AppointmentRangeType));
            obj.Field(d => d.AttachedFile, nullable: true, type: typeof(AttachedFileType));
            obj.Field(d => d.Message, nullable: true, type: typeof(MessageType));
            obj.Field(d => d.Officer, nullable: true, type: typeof(OfficerType));
            obj.Field(d => d.Region, nullable: true, type: typeof(RegionType));
            obj.Field(d => d.Rubric, nullable: true, type: typeof(RubricType));
            obj.Field(d => d.Territory, nullable: true, type: typeof(TerritoryType));
            obj.Field(d => d.Topic, nullable: true, type: typeof(TopicType));
            obj.Field(d => d.Subsystem, nullable: true, type: typeof(SubsystemType));
            obj.Field(d => d.Vote, nullable: true, type: typeof(VoteType));
            obj.Field(d => d.VoteItem, nullable: true, type: typeof(VoteItemType));
            obj.Field(d => d.Action, nullable: true);
            obj.Field(d => d.Data, nullable: true);
        }
        public ProtocolRecordType()
        {
            Name = nameof(ProtocolRecord);
        }
        protected override void InitFields()
        {
            base.InitFields();
            ProtocolRecordType.InitCommonFields(this);
            ProtocolRecordType.InitReadOnlyFields(this);
        }
    }
    public class ProtocolRecordUpdateType : BaseMutationType<ProtocolRecord>
    {
        public ProtocolRecordUpdateType()
        {
            Name = "update" + nameof(ProtocolRecord);
        }
        protected override void InitFields()
        {
            base.InitFields();
            ProtocolRecordType.InitCommonFields(this);
            ProtocolRecordType.InitWriteOnlyFields(this);
        }
    }
    public class ProtocolRecordCreateType : BaseMutationType<ProtocolRecord>
    {
        public ProtocolRecordCreateType()
        {
            Name = "create" + nameof(ProtocolRecord);
        }
        protected override void InitFields()
        {
            base.InitFields();
            ProtocolRecordType.InitCommonFields(this);
            ProtocolRecordType.InitWriteOnlyFields(this);
        }
    }
}
