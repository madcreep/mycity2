using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using GraphQL.Types;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.WebApi.Types.Users;

namespace BIT.MyCity.WebApi.Types
{
    public class RubricType : HierarchyReadType<Rubric>
    {
        internal static void InitMainFields(ComplexGraphType<Rubric> obj)
        {
            obj.Field(d => d.Name, nullable: true).Description("Наименование");
            obj.Field(d => d.ShortName, nullable: true).Description("Короткое наименование");
            obj.Field(d => d.ExtId, nullable: true).Description("Идентификатор во внешней системе");
            obj.Field(d => d.ExtType, nullable: true).Description("---");
            obj.Field(d => d.Description, nullable: true).Description("Описание");
            obj.Field(d => d.Docgroup, nullable: true).Description("Группа документов");
            obj.Field(d => d.Receivers, nullable: true).Description("---");
            obj.Field(d => d.UseForPA, nullable: true).Description("Запись на прием");
            obj.Field(d => d.DocGroupForUL, nullable: true).Description("Группа документов для ЮЛ");
        }
        public RubricType()
        {
            Name = nameof(Rubric);
        }
        protected override void InitFields()
        {
            base.InitFields();
            RubricType.InitMainFields(this);
            Field(d => d.Subsystems, nullable: true, type: typeof(ListGraphType<SubsystemType>)).Description("Подсистемы");
            Field(d => d.Moderators, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemType>)).Description("Модераторы");
            //Field(d => d.ModeratorsOfSubsystems, nullable: true, type: typeof(ListGraphType<UserOfSubsystemType>)).Description("Модераторы по подсистемам");

            Field(d => d.Executors, nullable: true, type: typeof(ListGraphType<UserType>)).Description("Исполнители");
            Field(d => d.ExecutorsOfSubsystems, nullable: true, type: typeof(ListGraphType<UserOfSubsystemType>)).Description("Исполнители по подсистемам");
        }
    }
    public class RubricCreateType : HierarchyCreateType<Rubric>
    {
        public RubricCreateType()
        {
            Name = $"create{nameof(Rubric)}";
        }
        protected override void InitFields()
        {
            base.InitFields();
            RubricType.InitMainFields(this);

            Fields.AddPolicy(PolicyName.ManageRubrics);

            Field(d => d.ModeratorsSet, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemInputType>))
                .Name(nameof(Rubric.Moderators))
                .Description("Mодераторы")
                .AuthorizeWith(PolicyName.ManageRubricsSubsystem);

            Field(d => d.ExecutorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей")
                .AuthorizeWith(PolicyName.ManageRubricsSubsystem);
            Field(d => d.ExecutorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем для задания исполнителей")
                .AuthorizeWith(PolicyName.ManageRubricsSubsystem);

            

            Field(d => d.SubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем")
                .AuthorizeWith(PolicyName.ManageRubricsSubsystem);

        }
    }
    public class RubricUpdateType : HierarchyMutationType<Rubric>
    {
        public RubricUpdateType()
        {
            Name = $"update{nameof(Rubric)}";
        }
        protected override void InitFields()
        {
            base.InitFields();
            RubricType.InitMainFields(this);
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор родителя");

            Fields.AddPolicy(PolicyName.ManageRubrics);

            Field(d => d.ModeratorsSet, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemInputType>))
                .Name(nameof(Rubric.Moderators))
                .Description("Mодераторы")
                .AuthorizeWith(PolicyName.ManageRubricsSubsystem);

            Field(d => d.ExecutorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей")
                .AuthorizeWith(PolicyName.ManageRubricsSubsystem);
            
            Field(d => d.ExecutorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем для задания исполнителей")
                .AuthorizeWith(PolicyName.ManageRubricsSubsystem);

            Field(d => d.SubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("UID подсистем")
            .AuthorizeWith(PolicyName.ManageRubricsSubsystem);

            Field(d => d.SubSystemUIDsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("UID подсистем (+/-)")
            .AuthorizeWith(PolicyName.ManageRubricsSubsystem);

        }
    }
}
