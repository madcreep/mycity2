using BIT.MyCity.Sms.Models;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Sms
{
    public class SmsBalanceType : ObjectGraphType<Balance>
    {
        public SmsBalanceType()
        {
            Name = nameof(Balance);
            Field(d => d.Currency, true)
                .Description("Валюта");
            Field(d => d.Amount, true)
                .Description("Сумма");
            Field(d => d.SmsCount, true)
                .Description("Количество доступных отправок СМС");
        }
    }
}
