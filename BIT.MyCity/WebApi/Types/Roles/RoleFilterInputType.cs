using BIT.MyCity.Model;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Roles
{
    public class RoleFilterInputType : InputObjectGraphType<RoleFilter>
    {
        public RoleFilterInputType()
        {
            Name = nameof(RoleFilterInputType);
            Field(d => d.Disconnected, true)
                .Description("Признак отключенной роли");
            Field(d => d.Editable, true)
                .Description("Признак редактируемой роли");
            Field(d => d.UserId, true)
                .Description("Идентификатор пользователя");
            Field(d => d.Id, true, typeof(ListGraphType<BigIntGraphType>))
                .Description("Идентификаторы ролей");
        }
    }
}
