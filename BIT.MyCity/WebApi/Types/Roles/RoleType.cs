using BIT.MyCity.Database;
using BIT.MyCity.WebApi.Types.Claims;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Roles
{
    public class RoleType : ObjectGraphType<Role>
    {
        public RoleType()
        {
            Name = nameof(RoleType);
            Field(d => d.Id, true, type: typeof(IdGraphType))
                .Description("Идентификатор роли");
            Field(d => d.Name)
                .Description("Название роли");
            Field(d => d.Description, true)
                .Description("Описание роли");
            Field(d => d.Claims, type: typeof(ListGraphType<ClaimType>))
                .Description("Разрешения");
            Field(d => d.Editable)
                .Description("Возможность редактирования");
            Field(d => d.IsAdminRegister)
                .Description("Возможность назначать создаваемым пользователям");
            Field(d => d.CreatedAt,type: typeof(DateTimeGraphType))
                .Description("Дата создания (UTC)");
            Field(d => d.UpdatedAt, type: typeof(DateTimeGraphType))
                .Description("Дата редактирования (UTC)");
            Field(d => d.DisconnectedTimestamp, true, typeof(DateTimeGraphType))
                .Description("Дата блокирования (UTC)");
            Field(d => d.Disconnected)
                .Description("Признак блокированной роли");
            Field(d => d.Users, true, typeof(ListGraphType<UserType>))
                .Description("Пользователи включенные в роль");
            Field(d => d.Weight)
                .Description("Признак сортироваки");
        }
    }

    public class RoleInputType : InputObjectGraphType<Role>
    {
        public RoleInputType()
        {
            Name =nameof(RoleInputType);
            Field(d => d.Name, true)
                .Description("Название роли");
            Field(d => d.Description, true)
                .Description("Описание роли");
            Field(d => d.Disconnected, true)
                .Description("Признак отключения роли");
            Field(d => d.Claims, true, typeof(ListGraphType<ClaimInputType>))
                .Description("Разрешения");
        }
    }


    public class RoleSortedType : ObjectGraphType<Role>
    {
        public RoleSortedType()
        {
            Name = nameof(RoleSortedType);
            Field(d => d.Id, type: typeof(IdGraphType))
                .Description("Идентификатор роли");
            Field(d => d.Weight)
                .Description("Признак сортироваки");
        }
    }

    public class RoleSortingType : InputObjectGraphType<Role>
    {
        public RoleSortingType()
        {
            Name = nameof(RoleSortingType);
            Field(d => d.Id, type: typeof(IdGraphType))
                .Description("Идентификатор роли");
            Field(d => d.Weight)
                .Description("Признак сортироваки");
        }
    }
}
