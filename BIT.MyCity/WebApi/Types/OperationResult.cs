using System;
using System.Collections.Generic;
using System.Linq;

namespace BIT.MyCity.WebApi.Types
{
    public class OperationResult<T>
    {
        public bool Success => !Errors.Any();

        public T Result { get; }

        public string[] Errors { get; }

        public OperationResult(T result, IEnumerable<string> errors = null)
        :this(result, errors?.ToArray())
        { }

        public OperationResult(T result, params string[] errors)
        {
            Result = result;
            Errors = errors ?? Array.Empty<string>();
        }
    }
}
