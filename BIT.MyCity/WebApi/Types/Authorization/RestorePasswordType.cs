using BIT.MyCity.Authorization.Models;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Authorization
{
    public class ForgotPasswordType : InputObjectGraphType<ForgotPasswordModel>
    {
        public ForgotPasswordType()
        {
            Name = nameof(ForgotPasswordType);
            Field(d => d.Login, true)
                .Description("Логин");
            Field(d => d.Email, true)
                .Description("Адрес электронной почты");
        }
    }

    public class RestorePasswordType : InputObjectGraphType<RestorePasswordModel>
    {
        public RestorePasswordType()
        {
            Name = nameof(RestorePasswordType);
            Field(d => d.UserId)
                .Description("Идентификатор пользователя");
            Field(d => d.Code)
                .Description("Код восстановления пароля");
            Field(d => d.Password)
                .Description("Новый пароль");
            Field(d => d.ConfirmPassword)
                .Description("Подтверждение пароля");
        }
    }
}
