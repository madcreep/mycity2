using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Claims;
using GraphQL.Types;
using BIT.MyCity.WebApi.Types.Users;

namespace BIT.MyCity.WebApi.Types.Authorization
{
    public class RegisterModelType : InputObjectGraphType<RegisterUserModel>
    {
        public RegisterModelType()
        {
            Name = nameof(RegisterModelType);
            Field(d => d.UserName)
                .Description("Логин");

            Field(d => d.Password)
                .Description("Пароль");

            Field(d => d.ConfirmPassword, true)
                .Description("Подтверждение пароля");

            Field(d => d.FullName, nullable: true)
                .Description("Полное Имя");

            Field(d => d.Gender, nullable: true, typeof(GenderType))
                .Description("Пол");

            Field(d => d.Email, nullable: true)
                .Description("Адрес электронной почты");

            Field(d => d.EmailConfirmed, nullable: true)
                .Description("Признак подтвержденности электронной почты");

            Field(d => d.Disconnected, nullable: true)
                .Description("Признак отключенного пользователя");

            Field(d => d.Phones, nullable: true, typeof(ListGraphType<PhoneInputType>))
                .Description("Телефоны");

            Field(d => d.Claims, nullable: true, typeof(ListGraphType<ClaimInputType>))
                .Description("Клаймы");

            Field(d => d.RoleIds, nullable: true, typeof(ListGraphType<BigIntGraphType>))
                .Description("Идентификаторы ролей");

            //Field(d => d.RubricIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsInputType>))
            //    .Description("Идентификаторы рубрик по подсистемам");

            //Field(d => d.TerritoryIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsInputType>))
            //    .Description("Идентификаторы территорий по подсистемам");

            //Field(d => d.RegionIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsInputType>))
            //    .Description("Идентификаторы регионов по подсистемам");

            //Field(d => d.TopicIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsInputType>))
            //    .Description("Идентификаторы тем по подсистемам");
        }
    }

}
