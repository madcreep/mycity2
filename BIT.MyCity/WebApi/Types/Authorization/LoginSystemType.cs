using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization.Models;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Authorization
{
  public class LoginSystemType : EnumerationGraphType<LoginSystem>
  { }

  public class LoginRequestSourceType : EnumerationGraphType<LoginRequestSource>
  { }

  public class GenderType : EnumerationGraphType<Gender>
  { }

}
