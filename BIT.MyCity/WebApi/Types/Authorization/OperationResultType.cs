using BIT.MyCity.ApiInterface.Models;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Authorization
{
    public class OperationResultType : ObjectGraphType<OperationResult>
    {
        public OperationResultType()
        {
            Name = nameof(OperationResultType);

            Field(d => d.Success, true)
                .Description("Результат операции");

            Field(d => d.ErrorMessages, true)
                .Description("Сообщение об ошибке");
        }
    }
}
