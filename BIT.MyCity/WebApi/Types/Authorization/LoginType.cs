using BIT.MyCity.ApiInterface.Models;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Authorization
{
    public class LoginType : InputObjectGraphType<LoginModel>
    {
        public LoginType()
        {
            Name = nameof(LoginType);
            Field(d => d.SubscribeKey, true)
                .Description("Ключ подписки");
            Field(d => d.Login, true)
                .Description("Логин");
            Field(d => d.Password, true)
                .Description("Пароль");
            Field(d => d.LoginSystem, true, typeof(LoginSystemType))
                .Description("Система авторизации");
            Field(d => d.Code, true)
                .Description("Код доступа");
            Field(d => d.CallbackUrl, true)
                .Description("URL-обратного вызова");
            Field(d => d.RequestCodeError, true)
                .Description("Описание ошибки запроса кода доступа через сторонние системы авторизации");
            Field(d => d.LoginRequestSource, true, typeof(LoginRequestSourceType))
                .Description("Тип системы через которую запорсили авторизацию (Возможные значения SITE или MOBILE)");
        }
    }
}
