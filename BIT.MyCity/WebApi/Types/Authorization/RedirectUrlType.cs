using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Authorization
{
  public class RedirectUrlType : ObjectGraphType<RedirectUrl>
  {
    public RedirectUrlType()
    {
      Name = "RedirectUrl";
      Field<StringGraphType>("loginSystem",
          resolve: context => context.Source.LoginSystem.ToString(),
          description: "Мнемоника системы авторизации");
      Field(d => d.Url)
          .Description("URL перенаправления");
    }
  }

  public class RedirectUrlInputType : InputObjectGraphType<RedirectUrl>
  {
    public RedirectUrlInputType()
    {
      Name = "RedirectInputUrl";
      Field<StringGraphType>("loginSystem",
          resolve: context => context.Source.LoginSystem.ToString(),
          description: "Мнемоника системы авторизации");
      Field(d => d.Url)
          .Description("URL перенаправления");
    }
  }
}
