using BIT.MyCity.ApiInterface.Models;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Authorization
{
  public class ResetPasswordInputType : InputObjectGraphType<ResetPasswordModel>
  {
    public ResetPasswordInputType()
    {
      Name = nameof(ResetPasswordInputType);
      Field(d => d.Id, true, typeof(IdGraphType))
          .Description("Идентификатор пользователя");
      Field(d => d.OldPassword, true)
          .Description("Старый пароль");
      Field(d => d.NewPassword)
          .Description("Новый пароль");
      Field(d => d.ConfirmPassword, true)
          .Description("Повторение нового пароля");
    }
  }
}
