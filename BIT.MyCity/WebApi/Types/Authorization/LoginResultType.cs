using BIT.MyCity.Authorization.Models;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Authorization
{
    public class LoginResultType : ObjectGraphType<LoginResult>
    {
        public LoginResultType()
        {
            Name = nameof(LoginResultType);

            Field(d => d.Key, true)
                .Description("Ключ подписки");

            Field(d => d.Token, true)
                .Description("Токен");

            Field<StringGraphType>("loginStatus",
                resolve: context => context.Source.LoginStatus.ToString(),
                description: "Статус операции авторизации");

            Field(d => d.ErrorMessage, true)
                .Description("Сообщение об ошибке");

            Field(d => d.User, true, typeof(UserType))
                .Description("Пользователь");
        }
    }
}
