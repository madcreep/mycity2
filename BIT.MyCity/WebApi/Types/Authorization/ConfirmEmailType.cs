using BIT.MyCity.ApiInterface.Models;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Authorization
{
  public class ConfirmEmailType : InputObjectGraphType<ConfirmEmailModel>
  {
    public ConfirmEmailType()
    {
      Name = nameof(ConfirmEmailType);
      Field(d => d.UserId, true)
          .Description("Идентификатор пользователя");
      Field(d => d.Code, true)
          .Description("Код подтверждения");
      Field(d => d.Sh, true)
          .Description("Хэш код Email");
    }
  }
}
