using BIT.MyCity.Database;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class AttachedFileType : BaseReadType<AttachedFile>
    {
        internal static void InitMainFields(ComplexGraphType<AttachedFile> obj)
        {
            obj.Field(d => d.Name, nullable: true).Description("Имя файла");
            obj.Field(d => d.Path, nullable: true).Description("Путь");
            obj.Field(d => d.ExtId, nullable: true).Description("Внешний идентификатор");
            obj.Field(d => d.Private, nullable: true).Description("Файл приватный");
            obj.Field(d => d.IsPublic, nullable: true).Description("Файл публичный");
            obj.Field(d => d.ContentType, nullable: true).Description("Content-Type");
        }

        public AttachedFileType()
        {
            Name = nameof(AttachedFile);
        }
        protected override void InitFields()
        {
            base.InitFields();
            InitMainFields(this);
            Field(d => d.UploadedBy, nullable: true, type: typeof(UserType)).Description("Кем загружен на сервер");
            Field(d => d.Sequre).Description("Без доступа по чтению");
            Field(d => d.Len).Description("Длина");
            Field(d => d.Incomplete).Description("Не докачан");
            Field(d => d.Thumb, nullable: true).Description("Миниатюра");
        }
    }
    public class AttachedFileCreateType : BaseMutationType<AttachedFile>
    {
        public AttachedFileCreateType()
        {
            Name = "create" + nameof(AttachedFile);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AttachedFileType.InitMainFields(this);
            Field(d => d.Sequre, nullable: true).Description("Без доступа по чтению");
        }
    }

    public class AttachedFileUpdateType : WPMutationType<AttachedFile>
    {
        public AttachedFileUpdateType()
        {
            Name = "update" + nameof(AttachedFile);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AttachedFileType.InitMainFields(this);
        }
    }
    public class AttachedFileApproveType : InputObjectGraphType<AttachedFileApprove>
    {
        public AttachedFileApproveType()
        {
            Name = "approve" + nameof(AttachedFile);
            Description = "Принятие к публикации или отклонение приложенного файла";
            Field(d => d.Public_, nullable: true).Description("Публичный");
        }
    }

}
