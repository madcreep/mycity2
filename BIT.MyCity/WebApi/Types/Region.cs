using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class UserOfSubsystemType : ObjectGraphType<UserOfSubsystem>
    {
        public UserOfSubsystemType()
        {
            Name = nameof(UserOfSubsystem);
            Field(d => d.User, nullable: true, type: typeof(UserType)).Description("Пользователь");
            Field(d => d.Subsystem, nullable: true, type: typeof(SubsystemType)).Description("Подсистема");
        }
    }

    public class RegionType : HierarchyReadType<Region>
    {
        internal static void InitMainFields(ComplexGraphType<Region> obj)
        {
            obj.Field(d => d.Name, nullable: true).Description("Наименование");
            obj.Field(d => d.ShortName, nullable: true).Description("Короткое наименование");
            obj.Field(d => d.ExtId, nullable: true).Description("Идентификатор во внешней системе");
        }

        public RegionType()
        {
            Name = nameof(Region);
        }

        protected override void InitFields()
        {
            base.InitFields();
            RegionType.InitMainFields(this);
            Field(d => d.Subsystems, nullable: true, type: typeof(ListGraphType<SubsystemType>)).Description("Подсистемы");
            Field(d => d.Moderators, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemType>)).Description("Модераторы");
            //Field(d => d.ModeratorsOfSubsystems, nullable: true, type: typeof(ListGraphType<UserOfSubsystemType>)).Description("Модераторы по подсистемам");
            Field(d => d.Executors, nullable: true, type: typeof(ListGraphType<UserType>)).Description("Исполнители");
            Field(d => d.ExecutorsOfSubsystems, nullable: true, type: typeof(ListGraphType<UserOfSubsystemType>)).Description("Исполнители по подсистемам");
        }
    }

    public class RegionCreateType : HierarchyCreateType<Region>
    {
        public RegionCreateType()
        {
            Name = $"create{nameof(Region)}";
        }

        protected override void InitFields()
        {
            base.InitFields();
            RegionType.InitMainFields(this);
            Fields.AddPolicy(PolicyName.ManageRegions);
            //Field(d => d.ModeratorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("Id модераторов");
            //Field(d => d.ModeratorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("UID подсистем для задания модераторов");
            Field(d => d.ModeratorsSet, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemInputType>))
                .Name(nameof(Rubric.Moderators))
                .Description("Mодераторы")
                .AuthorizeWith(PolicyName.ManageRegionsSubsystem);

            Field(d => d.ExecutorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей")
                .AuthorizeWith(PolicyName.ManageRegionsSubsystem);
            Field(d => d.ExecutorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем для задания исполнителей")
                .AuthorizeWith(PolicyName.ManageRegionsSubsystem);
            
            Field(d => d.SubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем")
                .AuthorizeWith(PolicyName.ManageRegionsSubsystem);
        }
    }

    public class RegionUpdateType : HierarchyMutationType<Region>
    {
        public RegionUpdateType()
        {
            Name = $"update{nameof(Region)}";
        }

        protected override void InitFields()
        {
            base.InitFields();
            RegionType.InitMainFields(this);
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType))
                .Description("Идентификатор родителя");

            Fields.AddPolicy(PolicyName.ManageRegions);

            //Field(d => d.ModeratorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("Id модераторов");
            //Field(d => d.ModeratorIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("Id модераторов (+/-)");
            //Field(d => d.ModeratorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("UID подсистем для задания модераторов");


            Field(d => d.ModeratorsSet, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemInputType>))
                .Name(nameof(Rubric.Moderators))
                .Description("Mодераторы")
                .AuthorizeWith(PolicyName.ManageRegionsSubsystem); ;

            Field(d => d.ExecutorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей")
                .AuthorizeWith(PolicyName.ManageRegionsSubsystem); ;
            Field(d => d.ExecutorIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей (+/-)")
                .AuthorizeWith(PolicyName.ManageRegionsSubsystem); ;
            Field(d => d.ExecutorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем для задания исполнителей")
                .AuthorizeWith(PolicyName.ManageRegionsSubsystem); ;
            
            Field(d => d.SubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем")
                .AuthorizeWith(PolicyName.ManageRegionsSubsystem);
            Field(d => d.SubSystemUIDsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем (+/-)")
                .AuthorizeWith(PolicyName.ManageRegionsSubsystem);

        }
    }
}
