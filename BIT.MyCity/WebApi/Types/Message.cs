using GraphQL.Types;
using BIT.MyCity.Database;
using BIT.MyCity.WebApi.Types.Users;
using BIT.MyCity.Model;

namespace BIT.MyCity.WebApi.Types
{
    public class MessageKindEnumType : EnumerationGraphType<MessageKindEnum>
    { }
    public class SubscribeToAnswersEnumType : EnumerationGraphType<SubscribeToAnswersEnum>
    { }

    public class MessageType : TreeReadType<Message>
    {
        internal static void InitMainFields(ComplexGraphType<Message> obj)
        {
            obj.Field(d => d.Text, nullable: true).Description("Текст сообщения");
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<Message> obj)
        {
            obj.Field(d => d.AttachedFilesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id присоединенных файлов");
            //obj.Field(d => d.AttachedFilesIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id присоединенных файлов (+/-)");
        }
        internal static void InitReadOnlyFields(ComplexGraphType<Message> obj)
        {
            obj.Field(d => d.Author, nullable: true, type: typeof(UserType)).Description("Автор сообщения: Пользователь (внутренний)");
            obj.Field(d => d.AppealExecutor, nullable: true, type: typeof(AppealExecutorType)).Description("Автор сообщения: Исполнитель (может быть внешним)");
            obj.Field(d => d.LikesCount).Description("Счетчик лайков");
            obj.Field(d => d.DislikesCount).Description("Счетчик дизлайков");
            obj.Field(d => d.ViewsCount).Description("Счетчик просмотров");
            obj.Field(d => d.RegardsCount).Description("Счетчик оценок");
            obj.Field(d => d.RegardAverage).Description("Средняя оценка");
            obj.Field(d => d.Appeal, nullable: true, type: typeof(AppealType)).Description("Корневое обращение");
            obj.Field(d => d.VoteRoot, nullable: true, type: typeof(VoteRootType)).Description("Корневое голосование");
            obj.Field(d => d.LikeWIL, nullable: true, type: typeof(LikeWILType)).Description("Как я лайкнул");
            obj.Field(d => d.ViewWIV, nullable: true, type: typeof(ViewWIVType)).Description("Как я посмотрел");
            obj.Field(d => d.RegardWIR, nullable: true, type: typeof(RegardWIRType)).Description("Как я оценил");
        }
        public MessageType()
        {
            Name = nameof(Message);
            Description = "Сообщение/комментарий";
        }
        protected override void InitFields()
        {
            base.InitFields();
            MessageType.InitMainFields(this);
            MessageType.InitReadOnlyFields(this);
            Field(d => d.AttachedFiles, nullable: true, type: typeof(ListGraphType<AttachedFileType>)).Description("присоединенные файлы");
            Field(d => d.UsersViewed, nullable: true, type: typeof(ListGraphType<UserType>)).Description("Просмотры");
            Field(d => d.UsersLiked, nullable: true, type: typeof(ListGraphType<UserType>)).Description("Лайки");
            Field(d => d.UsersRegard, nullable: true, type: typeof(ListGraphType<UserType>)).Description("Оценки");
            Field(d => d.Kind, nullable: true, type: typeof(MessageKindEnumType)).Description("Вид сообщения");
            Field(d => d.ModerationStage, nullable: true, type: typeof(ModerationStageEnumType)).Description("Стадии модерации");
            Field(d => d.Approved, nullable: true, type: typeof(ApprovedEnumType)).Description("Принято к публикации/отказано");
            Field(d => d.SubscribeToAnswers, nullable: true, type: typeof(SubscribeToAnswersEnumType)).Description("Уведомлять об ответах на данное сообщение");
        }
    }
    public class MessageCreateType : TreeCreateType<Message>
    {
        public MessageCreateType()
        {
            Name = "create" + nameof(Message);
            Description = "Сообщение/комментарий";
        }
        protected override void InitFields()
        {
            base.InitFields();
            MessageType.InitWriteOnlyFields(this);
            Field(d => d.Text, nullable: true).Description("Текст сообщения");
            Field(d => d.Kind, nullable: true, type: typeof(MessageKindEnumType)).Description("Вид сообщения");
            Field(d => d.AppealExecutorId, nullable: true, type: typeof(IdGraphType)).Description("Исполнитель");
            Field(d => d.SubscribeToAnswers, nullable: true, type: typeof(SubscribeToAnswersEnumType)).Description("Уведомлять об ответах на данное сообщение");
            Field(d => d.Approved, nullable: true, type: typeof(ApprovedEnumType)).Description("Принято к публикации/отказано");
        }
    }
    public class MessageUpdateType : TreeMutationType<Message>
    {
        public MessageUpdateType()
        {
            Name = "update" + nameof(Message);
            Description = "Сообщение/комментарий";
        }
        protected override void InitFields()
        {
            base.InitFields();
            MessageType.InitWriteOnlyFields(this);
            Field(d => d.Text, nullable: true).Description("Текст сообщения");
            Field(d => d.SubscribeToAnswers, nullable: true, type: typeof(SubscribeToAnswersEnumType)).Description("Уведомлять об ответах на данное сообщение");
        }
    }
    public class MessageApproveType : InputObjectGraphType<MessageApprove>
    {
        public MessageApproveType()
        {
            Name = "approve" + nameof(Message);
            Description = "Принятие к публикации или отклонение комментария";
            Field(d => d.ApprovedEnum, nullable: true, type: typeof(ApprovedEnumType)).Description("Принято к публикации/отказано");
            Field(d => d.ModerationStage, nullable: true, type: typeof(ModerationStageEnumType)).Description("Стадии модерации");
            Field(d => d.Public_, nullable: true).Description("Публичное");
            Field(d => d.Reason, nullable: true).Description("Создать комментарий с описанием причины");
        }
    }
    public class MessageDeleteType : InputObjectGraphType<MessageDelete>
    {
        public MessageDeleteType()
        {
            Name = "delete" + nameof(Message);
            Description = "Удаление комментария";
            Field(d => d.Reason, nullable: true).Description("Причина удаления");
        }
    }

    public class MessageSubscribeSet
    {
        public string EntityType { get; set; }
        public long EntityId { get; set; }
        public int Level { get; set; }
    }
    public class MessageSubscribeGet
    {
        public string RootType { get; set; }
        public string Due { get; set; }
        public int Level { get; set; }
    }
    public class MessageUnSubscribe
    {
        public string RootType { get; set; }
        public string EntityType { get; set; }
        public long EntityId { get; set; }
        public string Due { get; set; }
    }

    public class MessageSubscribeSetType : InputObjectGraphType<MessageSubscribeSet>
    {
        public MessageSubscribeSetType()
        {
            Name = $"{nameof(Message)}SubscribeSet";
            Description = "Подписка на комментарии";
            Field(d => d.EntityType, nullable: false).Description("Имя объекта");
            Field(d => d.EntityId, nullable: false, type: typeof(IdGraphType)).Description("ID объекта");
            Field(d => d.Level, nullable: true).Description("Уровень вложенности");
        }
    }
    public class MessageSubscribeGetType : ObjectGraphType<MessageSubscribeGet>
    {
        public MessageSubscribeGetType()
        {
            Name = $"{nameof(Message)}SubscribeGet";
            Description = "Подписка на комментарии";
            Field(d => d.RootType).Description("Корневой объект");
            Field(d => d.Due).Description("DUE");
            Field(d => d.Level).Description("Уровень вложенности");
        }
    }
    public class Subs2DueMessagesType : ObjectGraphType<Subs2DueMessages>
    {
        public Subs2DueMessagesType()
        {
            Name = $"{nameof(Message)}SubscribeGet_";
            Description = "Подписка на комментарии";
            Field(d => d.RootType).Description("Корневой объект");
            Field(d => d.Due).Description("DUE");
            Field(d => d.Level).Description("Уровень вложенности");
        }
    }
    public class MessageUnSubscribeType : InputObjectGraphType<MessageUnSubscribe>
    {
        public MessageUnSubscribeType()
        {
            Name = $"{nameof(Message)}UnSubscribe";
            Description = "Отписка";
            Field(d => d.EntityType, nullable: true).Description("Имя объекта");
            Field(d => d.EntityId, nullable: true, type: typeof(IdGraphType)).Description("ID объекта");
            Field(d => d.RootType, nullable: true).Description("Корневой объект");
            Field(d => d.Due, nullable: true).Description("DUE");
        }
    }
}
