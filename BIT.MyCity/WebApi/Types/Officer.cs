﻿using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class OfficerType : BaseReadType<Officer>
    {
        internal static void InitMainFields(ComplexGraphType<Officer> obj)
        {
            obj.Field(d => d.Name, nullable: true);
            obj.Field(d => d.ExtId, nullable: true);
            obj.Field(d => d.Position, nullable: true);
            obj.Field(d => d.Authority, nullable: true);
            obj.Field(d => d.Place, nullable: true);
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<Officer> obj)
        {
            obj.Field(d => d.RubricId, nullable: true, type: typeof(IdGraphType));
            obj.Field(d => d.RangesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>));
            obj.Field(d => d.RangesIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>));
        }
        public OfficerType()
        {
            Name = nameof(Officer);
        }
        protected override void InitFields()
        {
            base.InitFields();
            OfficerType.InitMainFields(this);
            Field(d => d.Rubric, nullable: true, type: typeof(RubricType));
            Field(d => d.Ranges, nullable: true, type: typeof(ListGraphType<AppointmentRangeType>));
        }
    }
    public class OfficerUpdateType : BaseMutationType<Officer>
    {
        public OfficerUpdateType()
        {
            Name = "update" + nameof(Officer);
        }
        protected override void InitFields()
        {
            base.InitFields();
            OfficerType.InitMainFields(this);
            OfficerType.InitWriteOnlyFields(this);
        }
    }
    public class OfficerCreateType : BaseMutationType<Officer>
    {
        public OfficerCreateType()
        {
            Name = "create" + nameof(Officer);
        }
        protected override void InitFields()
        {
            base.InitFields();
            OfficerType.InitMainFields(this);
            OfficerType.InitWriteOnlyFields(this);
        }
    }
}
