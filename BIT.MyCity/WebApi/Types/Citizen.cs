using System;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using GraphQL.Types;
using Mycity2.Authorization.Models;
using Mycity2.Database;

namespace Mycity2.WebApi.Types
{
    public class Citizen : BaseTbl
    {
        public string[] Arr { get; set; }
        public string Migrate { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Otchestvo { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

    }

    public class CitizenType : ObjectGraphType<Citizen>
    {
        public CitizenType()
        {
            Name = "CITIZEN";
            Field(d => d.Id).Description("Идентификатор объекта");//.AuthorizeWith(PolicyName.OnlyAdmin);
            Field(d => d.CreatedAt, true).Description("Дата создания объекта");
            Field(d => d.UpdatedAt, true).Description("Дата изменения объекта");
            Field(d => d.SortingOrder).Description("Для особой сортировки");
            Field(d => d.Deleted).Description("Удален");
            Field(d => d.Hidden).Description("Скрыт");
            Field(d => d.IsNode).Description("Является папкой");
            Field(d => d.Type, nullable: true).Description("Наименование типа");
            Field(d => d.Due, nullable: true).Description("ДУЕ");
            Field(d => d.ParentId).Description("Идентификатор родителя");
            Field(d => d.ParentType, nullable: true).Description("Наименование типа родителя");

            Field(d => d.Migrate, nullable: true).Description("---");
            Field(d => d.Name, nullable: true).Description("Имя гражданина");
            Field(d => d.Surname, nullable: true).Description("Фамилия");
            Field(d => d.Otchestvo, nullable: true).Description("Отчество");
            Field(d => d.Address, nullable: true).Description("---");
            Field(d => d.Email, nullable: true).Description("---");
            Field(d => d.Phone, nullable: true).Description("---");
//            Interface<BaseTblObjectInterface>();
        }

    }

    public class CitizenInputType : InputObjectGraphType<Citizen>
    {
        public CitizenInputType()
        {
            Name = "CitizenInput";
            Field(x => x.Name);            
        }
    }

}
