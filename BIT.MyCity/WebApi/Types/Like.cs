﻿using BIT.MyCity.Model;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class LikeRequestEnumType : EnumerationGraphType<LikeRequestEnum>
    {
    }
    public class LikeStateEnumType : EnumerationGraphType<LikeStateEnum>
    {
    }
    public class LikeRequestType : InputObjectGraphType<LikeRequest>
    {
        public LikeRequestType()
        {
            Name = nameof(LikeRequest);
            Field(d => d.Id, type: typeof(IdGraphType)).Description("Id объекта");
            Field(d => d.Type, nullable: true).Description("Тип объекта");
            Field(d => d.Liked, type: typeof(LikeRequestEnumType)).Description("" +
                "NONE - ничего не делать," +
                "LIKE - поставить лайк," +
                "LIKE_OFF - снять лайк," +
                "DISLIKE - поставить дизлайк," +
                "DISLIKE_OFF - снять дизлайк");
        }
    }
    public class LikeReportType : ObjectGraphType<LikeReport>
    {
        public LikeReportType()
        {
            Name = nameof(LikeReport);
            Field(d => d.Liked, type: typeof(LikeStateEnumType)).Description("" +
                "NONE - ничего," +
                "LIKED - стоит лайк, " +
                "DISLIKED - cтоит дизлайк");
            Field(d => d.LikeCount).Description("Счетчик лайков");
            Field(d => d.DislikeCount).Description("Счетчик дизлайков");
        }
    }
    public class LikeWILType : ObjectGraphType<LikeWIL>
    {
        public LikeWILType()
        {
            Name = nameof(LikeWIL);
            Field(d => d.Liked, type: typeof(LikeStateEnumType)).Description("" +
                "NONE - ничего," +
                "LIKED - стоит лайк, " +
                "DISLIKED - cтоит дизлайк");
        }
    }
    public class ViewRequestType : InputObjectGraphType<ViewRequest>
    {
        public ViewRequestType()
        {
            Name = nameof(ViewRequest);
            Field(d => d.Id, type: typeof(IdGraphType)).Description("Id объекта");
            Field(d => d.Type, nullable: true).Description("Тип объекта");
            Field(d => d.Viewed).Description("Просмотрено");
        }
    }
    public class ViewReportType : ObjectGraphType<ViewReport>
    {
        public ViewReportType()
        {
            Name = nameof(ViewReport);
            Field(d => d.Viewed).Description("Просмотрено");
            Field(d => d.ViewCount).Description("Сколько юзеров просмотрели");
            Field(d => d.CreatedAt, type: typeof(DateTimeGraphType)).Description("Дата создания объекта");
            Field(d => d.UpdatedAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Дата изменения объекта");
        }
    }
    public class RegardRequestType : InputObjectGraphType<RegardRequest>
    {
        public RegardRequestType()
        {
            Name = nameof(RegardRequest);
            Field(d => d.Id, type: typeof(IdGraphType)).Description("Id объекта");
            Field(d => d.Type, nullable: true).Description("Тип объекта");
            Field(d => d.Regard).Description("Оценка");
        }
    }
    public class RegardReportType : ObjectGraphType<RegardReport>
    {
        public RegardReportType()
        {
            Name = nameof(RegardReport);
            Field(d => d.Regard).Description("Оценка");
            Field(d => d.RegardCount).Description("Сколько юзеров оценили");
        }
    }
    public class ViewWIVType : ObjectGraphType<ViewWIV>
    {
        public ViewWIVType()
        {
            Name = nameof(ViewWIV);
            Field(d => d.Viewed).Description("Я видел");
            Field(d => d.CreatedAt, type: typeof(DateTimeGraphType)).Description("Дата создания объекта");
            Field(d => d.UpdatedAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Дата изменения объекта");
        }
    }
    public class RegardWIRType : ObjectGraphType<RegardWIR>
    {
        public RegardWIRType()
        {
            Name = nameof(RegardWIR);
            Field(d => d.Regard).Description("Оценка");
        }
    }
}
