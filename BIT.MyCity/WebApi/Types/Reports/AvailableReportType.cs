using BIT.MyCity.Reports;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Reports
{
    public class AvailableReportType : ObjectGraphType<IReport>
    {
        public AvailableReportType()
        {
            Name = "AvailableReport";
            Field(d => d.Id, type: typeof(GuidGraphType))
                .Description("Идентификатор отчета");
            Field(d => d.Name)
                .Description("Название отчета");
            Field(d => d.Description, true)
                .Description("Описание отчета");
        }
    }
}
