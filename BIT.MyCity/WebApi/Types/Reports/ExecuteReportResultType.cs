using BIT.MyCity.Reports;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Reports
{
    public class ExecuteReportResultType : ObjectGraphType<ExecuteReportResult>
    {
        public ExecuteReportResultType()
        {
            Name = nameof(ExecuteReportResult);
            Field(d => d.Id, type: typeof(GuidGraphType))
                .Description("Идентификатор отчета");
            Field(d => d.Success, type: typeof(BooleanGraphType))
                .Description("Признау успешного создания отчета");
            Field(d => d.Errors, true, typeof(ListGraphType<StringGraphType>))
                .Description("Ошибки формирования отчета");
            Field(d => d.Template, true, type: typeof(StringGraphType))
                .Description("Отображаемая HTML разметка отчета");
            Field(d => d.File, true, type: typeof(AttachedFileType))
                .Description("Файл отчета");
        }
    }
}
