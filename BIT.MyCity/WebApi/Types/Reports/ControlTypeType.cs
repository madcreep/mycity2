using BIT.MyCity.Reports;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Reports
{
    public class ControlTypeType : EnumerationGraphType<ReportSettingsControlType>
    {
    }
}
