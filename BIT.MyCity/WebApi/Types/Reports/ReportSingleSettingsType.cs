using BIT.MyCity.Reports;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Reports
{
    public class ReportSingleSettingsType : ObjectGraphType<ReportSingleSettings>
    {
        public ReportSingleSettingsType()
        {
            Name = nameof(ReportSingleSettings);
            Field(d => d.Name)
                .Description("Имя параметра");
            Field(d => d.Title)
                .Description("Отображаемое имя параметра");
            Field(d => d.ControlType, type: typeof(ControlTypeType))
                .Description("Тип элемента управления");
            Field(d => d.Required, type: typeof(BooleanGraphType))
                .Description("Признак обязательного параметра");
            Field(d => d.Value, true)
                .Description("Установленное начение");
            Field(d => d.Values, true)
                .Description("Возможные значения параметра");
            Field(d => d.AllEnable, true, typeof(BooleanGraphType))
                .Description("Отображать чекбокс \"Все\"");
            Field(d => d.AllSelected, true, typeof(BooleanGraphType))
                .Description("Состояние чекбокса \"Все\"");
            Field(d => d.Options, true, typeof(StringGraphType))
                .Description("Дополнительные параметры");
        }
    }
    
    public class ReportSingleSettingsInputType : InputObjectGraphType<ReportSingleSettings>
    {
        public ReportSingleSettingsInputType()
        {
            Name = $"{nameof(ReportSingleSettings)}Input";
            Field(d => d.Name, type: typeof(StringGraphType))
                .Description("Имя параметра");
            Field(d => d.Value, true, typeof(StringGraphType))
                .Description("Установленное начение");
            Field(d => d.AllEnable, true, typeof(BooleanGraphType))
                .Description("Отображать чекбокс \"Все\"");
            Field(d => d.AllSelected, true, typeof(BooleanGraphType))
                .Description("Состояние чекбокса \"Все\"");
            Field(d => d.Options, true, typeof(StringGraphType))
                .Description("Дополнительные параметры");
        }
    }
}
