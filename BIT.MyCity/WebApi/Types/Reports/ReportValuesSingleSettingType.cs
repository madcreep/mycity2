using BIT.MyCity.Reports;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Reports
{
    public class ReportValuesSingleSettingType : ObjectGraphType<ReportValuesSingleSetting>
    {
        public ReportValuesSingleSettingType()
        {
            Name = Name = $"{nameof(ReportValuesSingleSetting)}";
            Field(d => d.Name)
                .Description("Имя параметра");
            Field(d => d.Value, true)
                .Description("Установленное начение");
            Field(d => d.AllSelected, true, typeof(BooleanGraphType))
                .Description("Состояние чекбокса \"Все\"");
            Field(d => d.Options, true, typeof(StringGraphType))
                .Description("Дополнительные параметры");
        }
    }

    public class ReportValuesSingleSettingInputType : InputObjectGraphType<ReportValuesSingleSetting>
    {
        public ReportValuesSingleSettingInputType()
        {
            Name = Name = $"{nameof(ReportValuesSingleSetting)}Input";
            Field(d => d.Name)
                .Description("Имя параметра");
            Field(d => d.Value, true)
                .Description("Установленное начение");
            Field(d => d.AllSelected, true, typeof(BooleanGraphType))
                .Description("Состояние чекбокса \"Все\"");
            Field(d => d.Options, true, typeof(StringGraphType))
                .Description("Дополнительные параметры");
        }
    }
}
