using BIT.MyCity.Reports;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Reports
{
    public class ReportSettingsType : ObjectGraphType<ReportSettings>
    {
        public ReportSettingsType()
        {
            Name = nameof(ReportSettings);
            Field(d => d.ReportId, type: typeof(GuidGraphType))
                .Description("Идентификатор отчета");
            Field(d => d.Name, type: typeof(StringGraphType))
                .Description("Название отчета");
            Field(d => d.Description, true, typeof(StringGraphType))
                .Description("Описание отчета");
            Field(d => d.Parameters, type: typeof(ListGraphType<ReportSingleSettingsType>))
                .Description("Параметры отчета");
        }
    }

    public class ReportSettingsInputType : InputObjectGraphType<ReportSettings>
    {
        public ReportSettingsInputType()
        {
            Name = $"{nameof(ReportSettings)}Input";
            Field(d => d.ReportId, type: typeof(GuidGraphType))
                .Description("Идентификатор отчета");
            Field(d => d.TimeOffset, type: typeof(IntGraphType))
                .Description("Пользовательское смещение времени");
            Field(d => d.Parameters, type: typeof(ListGraphType<ReportSingleSettingsInputType>))
                .Description("Параметры отчета");
        }
    }
    
}
