using BIT.MyCity.Reports;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Reports
{
    public class ReportSavedSettingsType : ObjectGraphType<ReportSavedSettings>
    {
        public ReportSavedSettingsType()
        {
            Name = nameof(ReportSavedSettings);
            Field(d => d.Id, type: typeof(IdGraphType))
                .Description("Идентификатор набора");
            Field(d => d.Name)
                .Description("Название набора");
            Field(d => d.Parameters, true, typeof(ListGraphType<ReportValuesSingleSettingType>))
                .Description("Параметры");
        }
    }

    public class ReportSavedSettingsInputType : InputObjectGraphType<ReportSavedSettings>
    {
        public ReportSavedSettingsInputType()
        {
            Name = $"{nameof(ReportSavedSettings)}Input";
            Field(d => d.Id, true, typeof(IdGraphType))
                .Description("Идентификатор набора");
            Field(d => d.Name)
                .Description("Название набора");
            Field(d => d.Parameters, true, typeof(ListGraphType<ReportValuesSingleSettingInputType>))
                .Description("Параметры");
        }
    }
}
