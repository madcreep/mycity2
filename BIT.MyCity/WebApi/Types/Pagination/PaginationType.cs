using System.Collections.Generic;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Pagination
{
    public class PaginationType<P, T> : ObjectGraphType<Pagination<T>> where P: IGraphType
    {
        public PaginationType()
        {
            var typeName = typeof(T).Name;

            Name = $"{typeName}PageType";
            Field(d => d.Total)
                .Description("Размер выборки");

            Field(d => d.Items, type: typeof(ListGraphType<P>))
                .Description("Элементы коллекции");
        }
    }

    public class Pagination<T>
    {
        public long Total { get; set; }

        public IEnumerable<T> Items { get; set; }
    }
}
