using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using GraphQL.Types;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.WebApi.Types.Users;

namespace BIT.MyCity.WebApi.Types
{
    public class TerritoryType : HierarchyReadType<Territory>
    {
        internal static void InitMainFields(ComplexGraphType<Territory> obj)
        {
            obj.Field(d => d.Name, nullable: true).Description("Наименование");
            obj.Field(d => d.ShortName, nullable: true).Description("Короткое наименование");
            obj.Field(d => d.ExtId, nullable: true).Description("Идентификатор во внешней системе");
        }

        public TerritoryType()
        {
            Name = nameof(Territory);
        }

        protected override void InitFields()
        {
            base.InitFields();
            TerritoryType.InitMainFields(this);
            Field(d => d.Subsystems, nullable: true, type: typeof(ListGraphType<SubsystemType>)).Description("Подсистемы");
            Field(d => d.Moderators, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemType>)).Description("Модераторы");
            //Field(d => d.ModeratorsOfSubsystems, nullable: true, type: typeof(ListGraphType<UserOfSubsystemType>)).Description("Модераторы по подсистемам");
            Field(d => d.Executors, nullable: true, type: typeof(ListGraphType<UserType>)).Description("Исполнители");
            Field(d => d.ExecutorsOfSubsystems, nullable: true, type: typeof(ListGraphType<UserOfSubsystemType>)).Description("Исполнители по подсистемам");
        }
    }

    public class TerritoryCreateType : HierarchyCreateType<Territory>
    {
        public TerritoryCreateType()
        {
            Name = $"create{nameof(Territory)}";
        }

        protected override void InitFields()
        {
            base.InitFields();
            TerritoryType.InitMainFields(this);

            Fields.AddPolicy(PolicyName.ManageTerritories);
            //Field(d => d.ModeratorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("Id модераторов");
            //Field(d => d.ModeratorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("UID подсистем для задания модераторов");
            Field(d => d.ModeratorsSet, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemInputType>))
                .Name(nameof(Rubric.Moderators))
                .Description("Mодераторы")
                .AuthorizeWith(PolicyName.ManageTerritoriesSubsystem);

            Field(d => d.ExecutorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей")
                .AuthorizeWith(PolicyName.ManageTerritoriesSubsystem);
            Field(d => d.ExecutorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем для задания исполнителей")
                .AuthorizeWith(PolicyName.ManageTerritoriesSubsystem);

            

            Field(d => d.SubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем")
                .AuthorizeWith(PolicyName.ManageTerritoriesSubsystem);

        }
    }

    public class TerritoryUpdateType : HierarchyMutationType<Territory>
    {
        public TerritoryUpdateType()
        {
            Name = $"update{nameof(Territory)}";
        }

        protected override void InitFields()
        {
            base.InitFields();
            TerritoryType.InitMainFields(this);
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор родителя");

            Fields.AddPolicy(PolicyName.ManageTerritories);

            //Field(d => d.ModeratorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("Id модераторов");
            //Field(d => d.ModeratorIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("Id модераторов (+/-)");
            //Field(d => d.ModeratorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("UID подсистем для задания модераторов");

            Field(d => d.ModeratorsSet, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemInputType>))
                .Name(nameof(Rubric.Moderators))
                .Description("Mодераторы")
                .AuthorizeWith(PolicyName.ManageTerritoriesSubsystem);

            Field(d => d.ExecutorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей")
                .AuthorizeWith(PolicyName.ManageTerritoriesSubsystem);
            Field(d => d.ExecutorIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей (+/-)")
                .AuthorizeWith(PolicyName.ManageTerritoriesSubsystem);
            Field(d => d.ExecutorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем для задания исполнителей")
                .AuthorizeWith(PolicyName.ManageTerritoriesSubsystem);
            
            Field(d => d.SubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем")
                .AuthorizeWith(PolicyName.ManageTerritoriesSubsystem);
            Field(d => d.SubSystemUIDsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем (+/-)")
                .AuthorizeWith(PolicyName.ManageTerritoriesSubsystem);

        }
    }
}
