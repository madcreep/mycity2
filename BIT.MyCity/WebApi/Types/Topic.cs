using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using GraphQL.Types;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.WebApi.Types.Users;

namespace BIT.MyCity.WebApi.Types
{
    public class TopicType : HierarchyReadType<Topic>
    {
        internal static void InitMainFields(ComplexGraphType<Topic> obj)
        {
            obj.Field(d => d.Name, nullable: true).Description("Наименование");
            obj.Field(d => d.ShortName, nullable: true).Description("Короткое наименование");
            obj.Field(d => d.ExtId, nullable: true).Description("Идентификатор во внешней системе");
        }

        public TopicType()
        {
            Name = nameof(Topic);
        }

        protected override void InitFields()
        {
            base.InitFields();
            TopicType.InitMainFields(this);
            Field(d => d.Subsystem, nullable: true, type: typeof(SubsystemType)).Description("Подсистема").DeprecationReason($"Теперь в '{nameof(Topic.Subsystems)}'");
            Field(d => d.Subsystems, nullable: true, type: typeof(ListGraphType<SubsystemType>)).Description("Подсистемы");
            Field(d => d.Moderators, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemType>)).Description("Модераторы");
            //Field(d => d.ModeratorsOfSubsystems, nullable: true, type: typeof(ListGraphType<UserOfSubsystemType>)).Description("Модераторы по подсистемам");

            Field(d => d.Executors, nullable: true, type: typeof(ListGraphType<UserType>)).Description("Исполнители");
            Field(d => d.ExecutorsOfSubsystems, nullable: true, type: typeof(ListGraphType<UserOfSubsystemType>)).Description("Исполнители по подсистемам");
        }
    }

    public class TopicCreateType : HierarchyCreateType<Topic>
    {
        public TopicCreateType()
        {
            Name = $"create{nameof(Topic)}";
        }

        protected override void InitFields()
        {
            base.InitFields();
            TopicType.InitMainFields(this);
            //Field(d => d.ModeratorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("Id модераторов");
            //Field(d => d.ModeratorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("UID подсистем для задания модераторов");

            Fields.AddPolicy(PolicyName.ManageTopics);

            Field(d => d.ModeratorsSet, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemInputType>))
                .Name(nameof(Rubric.Moderators))
                .Description("Mодераторы")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);

            Field(d => d.ExecutorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);
            Field(d => d.ExecutorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем для задания исполнителей")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);
            
            Field(d => d.SubsystemId, nullable: true, type: typeof(IdGraphType))
                .Description("Уникальное наименование подсистемы")
                .DeprecationReason($"Теперь в '{nameof(Topic.SubSystemUIDs)}'")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);

            Field(d => d.SubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);

        }
    }

    public class TopicUpdateType : HierarchyMutationType<Topic>
    {
        public TopicUpdateType()
        {
            Name = $"update{nameof(Topic)}";
        }

        protected override void InitFields()
        {
            base.InitFields();
            TopicType.InitMainFields(this);
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор родителя");
            //Field(d => d.ModeratorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("Id модераторов");
            //Field(d => d.ModeratorIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("Id модераторов (+/-)");
            //Field(d => d.ModeratorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
            //    .Description("UID подсистем для задания модераторов");

            Fields.AddPolicy(PolicyName.ManageTopics);

            Field(d => d.ModeratorsSet, nullable: true, type: typeof(ListGraphType<ClerkInSubsystemInputType>))
                .Name(nameof(Rubric.Moderators))
                .Description("Mодераторы")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);

            Field(d => d.ExecutorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);
            Field(d => d.ExecutorIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("Id исполнителей (+/-)")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);
            Field(d => d.ExecutorIdsSubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем для задания исполнителей")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);

            Field(d => d.SubSystemUIDs, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);
            Field(d => d.SubSystemUIDsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>))
                .Description("UID подсистем (+/-)")
                .AuthorizeWith(PolicyName.ManageTopicsSubsystem);

        }
    }
}
