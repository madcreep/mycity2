using GraphQL.Types;
using BIT.MyCity.Database;

namespace BIT.MyCity.WebApi.Types
{
    public class BaseReadType<T> : ObjectGraphType<T> where T : BaseEntity
    {
        public BaseReadType()
        {
            InitFields();
        }
        protected virtual void InitFields() 
        {
            Field(d => d.Id, nullable: false, type: typeof(IdGraphType)).Description("Идентификатор объекта");
            Field(d => d.CreatedAt, type: typeof(DateTimeGraphType)).Description("Дата создания объекта");
            Field(d => d.UpdatedAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Дата изменения объекта");
            Field(d => d.Weight, nullable: true).Description("Для особой сортировки");
            Field(d => d.Deleted, nullable: true).Description("Удален");
        }
    }
    public class BaseMutationType<T> : InputObjectGraphType<T> where T : BaseEntity
    {
        public BaseMutationType()
        {
            InitFields();
        }
        protected virtual void InitFields()
        {
            Field(d => d.Weight, nullable: true).Description("Для особой сортировки");
            Field(d => d.Deleted, nullable: true).Description("Удален");
        }
    }
    public class TreeReadType<T> : BaseReadType<T> where T : TreeEntity
    {
        public TreeReadType()
        {
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор родителя");
            Field(d => d.ParentType, nullable: true).Description("Наименование типа родителя");
            Field(d => d.IsNode, nullable: true).Description("Является папкой");
            Field(d => d.Due, nullable: true).Description("Ключ для упрощения доступа к иерархическим объектам");
        }
    }
    public class TreeMutationType<T> : BaseMutationType<T> where T : TreeEntity
    {
        public TreeMutationType()
        {
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.IsNode, nullable: true).Description("Является папкой");
        }
    }
    public class TreeCreateType<T> : BaseMutationType<T> where T : TreeEntity
    {
        public TreeCreateType()
        {
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор родителя");
            Field(d => d.ParentType, nullable: true).Description("Наименование типа родителя");
            Field(d => d.IsNode, nullable: true).Description("Является папкой");
        }
    }

    public class HierarchyReadType<T> : BaseReadType<T> where T : HierarchyEntity
    {
        public HierarchyReadType()
        {
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор родителя");
            Field(d => d.ParentType, nullable: true).Description("Наименование типа родителя");
            Field(d => d.IsNode, nullable: true).Description("Является папкой");
            Field(d => d.Due, nullable: true).Description("Ключ для упрощения доступа к иерархическим объектам");
            Field(d => d.Layer, nullable: false).Description("Уровень");
        }
    }
    public class HierarchyMutationType<T> : BaseMutationType<T> where T : HierarchyEntity
    {
        public HierarchyMutationType()
        {
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.IsNode, nullable: true).Description("Является папкой");
        }
    }
    public class HierarchyCreateType<T> : BaseMutationType<T> where T : HierarchyEntity
    {
        public HierarchyCreateType()
        {
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор родителя");
            Field(d => d.ParentType, nullable: true).Description("Наименование типа родителя");
            Field(d => d.IsNode, nullable: true).Description("Является папкой");
        }
    }

    public class WPReadType<T> : BaseReadType<T> where T : BaseEntity, IHasParentEntity
    {
        public WPReadType()
        {
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор родителя");
            Field(d => d.ParentType, nullable: true).Description("Наименование типа родителя");
        }
    }
    public class WPMutationType<T> : BaseMutationType<T> where T : BaseEntity
    {
        public WPMutationType()
        {
        }
        protected override void InitFields()
        {
            base.InitFields();
        }
    }
    public class WPCreateType<T> : BaseMutationType<T> where T : BaseEntity, IHasParentEntity
    {
        public WPCreateType()
        {
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор родителя");
            Field(d => d.ParentType, nullable: true).Description("Наименование типа родителя");
        }
    }
}
