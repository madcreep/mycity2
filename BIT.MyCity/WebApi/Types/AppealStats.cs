using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class AppealStatsType : ObjectGraphType<AppealStats>
    {
        internal static void InitReadOnlyFields(ComplexGraphType<AppealStats> obj)
        {
            obj.Field(d => d.Approved, nullable: true, type: typeof(ApprovedEnumType)).Description("Одобренные");
            obj.Field(d => d.Deleted, nullable: true).Description("Удаленные");
            obj.Field(d => d.Public, nullable: true).Description("Публичные");
            obj.Field(d => d.Region, nullable: true, type: typeof(RegionType)).Description("Регион");
            obj.Field(d => d.Rubric, nullable: true, type: typeof(RubricType)).Description("Рубрика");
            obj.Field(d => d.Subsystem, nullable: true, type: typeof(SubsystemType)).Description("Подсистема");
            obj.Field(d => d.Territory, nullable: true, type: typeof(TerritoryType)).Description("Территория");
            obj.Field(d => d.Topic, nullable: true, type: typeof(TopicType)).Description("Тема");
            obj.Field(d => d.AppealsCount, nullable: true).Description("Количество обращений");
            obj.Field(d => d.CreatedAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Дата создания");
            obj.Field(d => d.SiteStatus, nullable: true).Description("Статус");
            obj.Field(d => d.SentToDelo, nullable: true).Description("Зарегистрировано в Деле");
            
        }

        //internal static void InitWriteOnlyFields(ComplexGraphType<AppealStats> obj)
        //{
        //    obj.Field(d => d.RubricId, nullable: true, type: typeof(IdGraphType)).Description("Id Рубрики");
        //    obj.Field(d => d.RegionId, nullable: true, type: typeof(IdGraphType)).Description("Id региона");
        //    obj.Field(d => d.TopicId, nullable: true, type: typeof(IdGraphType)).Description("Id темы");
        //    obj.Field(d => d.TerritoryId, nullable: true, type: typeof(IdGraphType)).Description("Id территории");
        //}
        
        public AppealStatsType()
        {
            Name = nameof(AppealStats);
            Description = "Статистика обращений";
            InitReadOnlyFields(this);
        }
        //protected override void InitFields()
        //{
        //    base.InitFields();
        //    InitReadOnlyFields(this);
        //}
    }
}