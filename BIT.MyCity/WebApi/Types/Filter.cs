using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Filter;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;
using SkiaSharp;

namespace BIT.MyCity.WebApi.Types
{
    public class SelectionRangeType : InputObjectGraphType<SelectionRange>
    {
        public SelectionRangeType()
        {                 
            Name = "Range"; // nameof(SelectionRange);
            Field(d => d.OrderBy, nullable: true, type: typeof(ListGraphType<OrderByItemType>)).Description("Массив полей для сортировки");
            Field(d => d.Skip, nullable: true).Description("Сколько элементов пропустить с начала");
            Field(d => d.Take, nullable: true).Description("Сколько элементов вернуть");
        }
    }

    public class RangeSearchType : InputObjectGraphType<RangeSearch>
    {
        public RangeSearchType()
        {
            Name = nameof(RangeSearch);
            Field(d => d.Skip, nullable: true).Description("Сколько элементов пропустить с начала");
            Field(d => d.Take, nullable: true).Description("Сколько элементов вернуть");
        }
    }

    public class FilterOpEnumType : EnumerationGraphType<FilterNextOpEnum>
    {
    }
    public class FilterType : InputObjectGraphType<GraphQlQueryFilter>
    {
        public FilterType()
        {
            Name = "Filter";
            Field(d => d.Name, nullable: true).Description("Имя поля");
            Field(d => d.Like, nullable: true).Description("Поиск подстроки (%ищу%), начала (ищу%) или конца (%ищу) строки в любом регистре");
            Field(d => d.Not, nullable: true).Description("Инверсия условия");
            Field(d => d.Equal, nullable: true).Description("Равенство. Для строк с учетом регистра.");
            Field(d => d.More, nullable: true).Description("Больше");
            Field(d => d.Less, nullable: true).Description("Меньше");
            Field(d => d.GreaterOrEqual, nullable: true).Description("Больше или равно");
            Field(d => d.LessOrEqual, nullable: true).Description("Меньше или равно");
            Field(d => d.Op, nullable: true, type: typeof(FilterOpEnumType)).Description("Объединение с накопленным результатом");
            Field(d => d.In, nullable: true, type: typeof(ListGraphType<StringGraphType>)).Description("Есть в списке");
            Field(d => d.WithDeleted, nullable: true).Description("Включить удаленные элементы");
            Field(d => d.Stacked, nullable: true).Description("Использование стека");
            Field(d => d.Where, nullable: true).Description("Условия фильтрации через DynamicLinq");
        }
    }

    public class SKEncodedImageFormatType : EnumerationGraphType<SKEncodedImageFormat>
    {
    }
    public class QueryOptionsType : InputObjectGraphType<QueryOptions>
    {
        public QueryOptionsType()
        {
            Name = nameof(QueryOptions);
            Field(d => d.Filters, nullable: true, type: typeof(ListGraphType<FilterType>)).Description("Фильтр");
            //Field(d => d.FiltersNew, nullable: true, type: typeof(ListGraphType<FilterItemType>)).Description("Фильтр (Новый)");
            Field(d => d.Range, nullable: true, type: typeof(SelectionRangeType)).Description("Сортировка и пагинация");
            Field(d => d.WithDeleted, nullable: true).Description("Включить удаленные");
            Field(d => d.WithLinkedByOrganizations, nullable: true).Description("Включить связанные через организации пользователя");
            Field(d => d.ThumbSize, nullable: true).Description("Желаемый размер миниатюры в аттачах, например 100x100");
            Field(d => d.ThumbSizeTolerance, nullable: true).Description("Допустимое отклонение размера миниатюры в аттачах, например 20%x15%, 10x10");
            Field(d => d.ThumbFormat, nullable: true, type: typeof(SKEncodedImageFormatType)).Description("Формат миниатюры");
        }
    }

    public class FilterSearchType : InputObjectGraphType<FilterSearch>
    {
        public FilterSearchType()
        {
            Name = nameof(FilterSearch);
            Field(d => d.Name).Description("Имя поля");
            Field(d => d.Match).Description("Строка поиска");
        }
    }

    public class EntitySelector
    {
        public string Name { get; set; }
        public long Id { get; set; }
    }

    public class EntitySelectorType : InputObjectGraphType<EntitySelector>
    {
        public EntitySelectorType()
        {
            Name = nameof(EntitySelector);
            Field(d => d.Name).Description("Имя объекта");
            Field(d => d.Id).Description("id");
        }
    }
}
