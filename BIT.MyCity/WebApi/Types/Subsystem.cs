﻿using GraphQL.Types;
using BIT.MyCity.Database;
using BIT.MyCity.WebApi.Types.Users;

namespace BIT.MyCity.WebApi.Types
{
    public class SubsystemType : BaseReadType<Subsystem>
    {
        public SubsystemType()
        {
            Name = nameof(Subsystem);
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.UID, nullable: false, type: typeof(IdGraphType)).Description("Уникальное наименование подсистемы");
            Field(d => d.Name, nullable: true).Description("Печатное наименование подсистемы");
            Field(d => d.Description, nullable: true).Description("Описание подсистемы");
            Field(d => d.Regions, nullable: true, type: typeof(ListGraphType<RegionType>)).Description("Список регионов подсистемы");
            Field(d => d.Rubrics, nullable: true, type: typeof(ListGraphType<RubricType>)).Description("Список рубрик подсистемы");
            Field(d => d.Territories, nullable: true, type: typeof(ListGraphType<TerritoryType>)).Description("Список территорий подсистемы");
            Field(d => d.Topics, nullable: true, type: typeof(ListGraphType<TopicType>)).Description("Список топиков подсистемы");
            //Field(d => d.AppealPropertyNames, nullable: true).Description("Список доступных полей объекта Appeal");
            Field(d => d.Moderator, nullable: true, type: typeof(UserType)).Description("Модератор подсистемы");

            Field(d => d.MainEntityName, nullable: true).Description("Имя API основного объекта подсистемы");
            Field(d => d.MainEntityFields, nullable: true, type: typeof(ListGraphType<SubsystemMainEntityFieldType>)).Description("Поля основного объекта подсистемы");
            Field(d => d.IsPublicDefault, nullable: false).Description("?");
            Field(d => d.PrivateOnly, nullable: false).Description("?");
            Field(d => d.UseLikesForMain, nullable: false).Description("?");
            Field(d => d.UseLikesForComments, nullable: false).Description("?");
            Field(d => d.UseDislikes, nullable: false).Description("?");
            Field(d => d.UseRates, nullable: false).Description("?");
            Field(d => d.UseComments, nullable: false).Description("?");
            Field(d => d.UseAttaches, nullable: false).Description("?");
            Field(d => d.UseMaps, nullable: false).Description("?");
            Field(d => d.UseRubricsDescription, nullable: false).Description("?");
            Field(d => d.UseTerritoriesDescription, nullable: false).Description("?");
            Field(d => d.UseRegionsDescription, nullable: false).Description("?");
            Field(d => d.UseTopicsDescription, nullable: true).Description("?");
            Field(d => d.UseInMobileClient, nullable: true).Description("?");
        }
    }
    public class SubsystemCreateType : BaseMutationType<Subsystem>
    {
        public SubsystemCreateType()
        {
            Name = "create" + nameof(Subsystem);
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.UID, nullable: false, type: typeof(IdGraphType)).Description("Уникальное наименование подсистемы");
            Field(d => d.Name, nullable: true).Description("Печатное наименование подсистемы");
            Field(d => d.Description, nullable: true).Description("Описание подсистемы");
            Field(d => d.RegionIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id регионов");
            Field(d => d.RubricIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id рубрик");
            Field(d => d.TerritoryIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id территорий");
            Field(d => d.TopicIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id топиков");
            //Field(d => d.AppealPropertyNames, nullable: true).Description("Список доступных полей объекта Appeal");
            Field(d => d.ModeratorId, nullable: true, type: typeof(IdGraphType)).Description("Id модератора подсистемы");

            Field(d => d.MainEntityName, nullable: true).Description("Имя API основного объекта подсистемы");
            Field(d => d.MainEntityFields, nullable: true, type: typeof(ListGraphType<SubsystemMainEntityFieldCreateType>)).Description("Поля основного объекта подсистемы");
            Field(d => d.IsPublicDefault, nullable: true).Description("?");
            Field(d => d.PrivateOnly, nullable: true).Description("?");
            Field(d => d.UseLikesForMain, nullable: true).Description("?");
            Field(d => d.UseLikesForComments, nullable: true).Description("?");
            Field(d => d.UseDislikes, nullable: true).Description("?");
            Field(d => d.UseRates, nullable: true).Description("?");
            Field(d => d.UseComments, nullable: true).Description("?");
            Field(d => d.UseAttaches, nullable: true).Description("?");
            Field(d => d.UseMaps, nullable: true).Description("?");
            Field(d => d.UseRubricsDescription, nullable: true).Description("?");
            Field(d => d.UseTerritoriesDescription, nullable: true).Description("?");
            Field(d => d.UseRegionsDescription, nullable: true).Description("?");
            Field(d => d.UseTopicsDescription, nullable: true).Description("?");
            Field(d => d.UseInMobileClient, nullable: true).Description("?");
        }
    }
    public class SubsystemUpdateType : BaseMutationType<Subsystem>
    {
        public SubsystemUpdateType()
        {
            Name = "update" + nameof(Subsystem);
        }
        protected override void InitFields()
        {
            base.InitFields();
            Field(d => d.Name, nullable: true).Description("Печатное наименование подсистемы");
            Field(d => d.Description, nullable: true).Description("Описание подсистемы");
            Field(d => d.RegionIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id регионов");
            Field(d => d.RubricIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id рубрик");
            Field(d => d.TerritoryIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id территорий");
            Field(d => d.TopicIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id топиков");
            //Field(d => d.AppealPropertyNames, nullable: true).Description("Список доступных полей объекта Appeal");
            Field(d => d.ModeratorId, nullable: true, type: typeof(IdGraphType)).Description("Id модератора подсистемы");
            Field(d => d.RegionIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id регионов (+/-)");
            Field(d => d.RubricIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id рубрик (+/-)");
            Field(d => d.TerritoryIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id территорий (+/-)");
            Field(d => d.TopicIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id топиков (+/-)");
            //Field(d => d.AppealPropertyNamesChange, nullable: true).Description("Список доступных полей объекта Appeal (+/-)");

            Field(d => d.MainEntityName, nullable: true).Description("Имя API основного объекта подсистемы");
            Field(d => d.MainEntityFields, nullable: true, type: typeof(ListGraphType<SubsystemMainEntityFieldCreateType>)).Description("Поля основного объекта подсистемы");
            Field(d => d.MainEntityFieldsUpdate, nullable: true, type: typeof(ListGraphType<SubsystemMainEntityFieldCreateType>)).Description("Поля основного объекта подсистемы");
            Field(d => d.IsPublicDefault, nullable: true).Description("?");
            Field(d => d.PrivateOnly, nullable: true).Description("?");
            Field(d => d.UseLikesForMain, nullable: true).Description("?");
            Field(d => d.UseLikesForComments, nullable: true).Description("?");
            Field(d => d.UseDislikes, nullable: true).Description("?");
            Field(d => d.UseRates, nullable: true).Description("?");
            Field(d => d.UseComments, nullable: true).Description("?");
            Field(d => d.UseAttaches, nullable: true).Description("?");
            Field(d => d.UseMaps, nullable: true).Description("?");
            Field(d => d.UseRubricsDescription, nullable: true).Description("?");
            Field(d => d.UseTerritoriesDescription, nullable: true).Description("?");
            Field(d => d.UseRegionsDescription, nullable: true).Description("?");
            Field(d => d.UseTopicsDescription, nullable: true).Description("?");
        }
    }


    public class SubsystemMainEntityFieldType : ObjectGraphType<SubsystemMainEntityField>
    {
        public SubsystemMainEntityFieldType()
        {
            Name = nameof(SubsystemMainEntityField);
            Field(d => d.NameAPI, nullable: false).Description("Имя поля объекта в подсистеме");
            Field(d => d.NameDisplayed, nullable: true).Description("Имя поля объекта в для пользователя");
            Field(d => d.Description, nullable: true).Description("Описание поля для пользователя");
            Field(d => d.NameAPISecond, nullable: true).Description("имя другого поля, содержимое которого выводим если основное не заполнено");
            Field(d => d.Required, nullable: true).Description("обязательно к заполнению при создании объекта");
            Field(d => d.Weight, nullable: true).Description("вес для сортировки");
            Field(d => d.Placeholder, nullable: true).Description("выводим это если поле не заполнено");
            Field(d => d.Subsystem, nullable: true, type: typeof(SubsystemType)).Description("подсистема");
        }
    }
    public class SubsystemMainEntityFieldCreateType : InputObjectGraphType<SubsystemMainEntityField>
    {
        public SubsystemMainEntityFieldCreateType()
        {
            Name = "create" + nameof(SubsystemMainEntityField);
            Field(d => d.NameAPI, nullable: false).Description("Имя поля объекта в подсистеме");
            Field(d => d.NameDisplayed, nullable: true).Description("Имя поля объекта в для пользователя");
            Field(d => d.Description, nullable: true).Description("Описание поля для пользователя");
            Field(d => d.NameAPISecond, nullable: true).Description("имя другого поля, содержимое которого выводим если основное не заполнено");
            Field(d => d.Required, nullable: true).Description("обязательно к заполнению при создании объекта");
            Field(d => d.Weight, nullable: true).Description("вес для сортировки");
            Field(d => d.Placeholder, nullable: true).Description("выводим это если поле не заполнено");
        }
    }
    public class SubsystemMainEntityFieldUpdateType : InputObjectGraphType<SubsystemMainEntityField>
    {
        public SubsystemMainEntityFieldUpdateType()
        {
            Name = "update" + nameof(SubsystemMainEntityField);
            Field(d => d.NameAPI, nullable: false).Description("Имя поля объекта в подсистеме");
            Field(d => d.NameDisplayed, nullable: true).Description("Имя поля объекта в для пользователя");
            Field(d => d.Description, nullable: true).Description("Описание поля для пользователя");
            Field(d => d.NameAPISecond, nullable: true).Description("имя другого поля, содержимое которого выводим если основное не заполнено");
            Field(d => d.Required, nullable: true).Description("обязательно к заполнению при создании объекта");
            Field(d => d.Weight, nullable: true).Description("вес для сортировки");
            Field(d => d.Placeholder, nullable: true).Description("выводим это если поле не заполнено");
        }
    }
}
