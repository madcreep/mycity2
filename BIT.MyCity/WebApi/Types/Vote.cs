using BIT.MyCity.Database;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    #region VoteWAV
    public class VoteWIVType : ObjectGraphType<VoteWIV>
    {
        public VoteWIVType()
        {
            Name = nameof(VoteWIV);
            Field(d => d.Voted).Description("Голосование имело место быть");
            Field(d => d.Text, nullable: true).Description("Текст индивидуального предложения");
            Field(d => d.VoteItemsIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Список Id пунктов голосования");
        }
    }
    #endregion
    #region VoteOffer
    public class VoteOfferType : ObjectGraphType<VoteOffer>
    {
        public VoteOfferType()
        {
            Name = nameof(VoteOffer);
            Field(d => d.Text, nullable: true).Description("Текст индивидуального предложения");
            Field(d => d.User, nullable: true, type: typeof(UserType)).Description("Кто внёс предложение");
        }
    }
    #endregion
    #region VoteAction
    public class VoteActionType : InputObjectGraphType<VoteAction>
    {
        public VoteActionType()
        {
            Name = nameof(VoteAction);
            Field(d => d.VoteId, type: typeof(IdGraphType)).Description("Id голосования");
            Field(d => d.VoteItemIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Массив Id выбранных пунктов голосования");
            Field(d => d.VoteItemNumbers, nullable: true, type: typeof(ListGraphType<IntGraphType>)).Description("Массив номеров выбранных пунктов голосования");
            Field(d => d.Comment, nullable: true).Description("Текст индивидуального предложения");
        }
    }
    #endregion
    #region VoteRoot
    public class VoteStatusEnumType : EnumerationGraphType<VoteRoot.StatusEnum>
    {
    }
    public class VoteRootType : BaseReadType<VoteRoot>
    {
        internal static void InitMainFields(ComplexGraphType<VoteRoot> obj)
        {
            obj.Field(d => d.Name, nullable: true).Description("Наименование");
            obj.Field(d => d.ShortName, nullable: true).Description("Короткое наименование");
            obj.Field(d => d.StartAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Когда начнется");
            obj.Field(d => d.EndAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Когда завершится");
            obj.Field(d => d.ArchiveAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Когда архивируется");
            obj.Field(d => d.Text, nullable: true).Description("Описание голосования");
            obj.Field(d => d.TextResult, nullable: true).Description("Описание итогов голосования");
            obj.Field(d => d.CommentsAllowed, nullable: true).Description("Пользователи могут оставлять комментарии");
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<VoteRoot> obj)
        {
            obj.Field(d => d.VotesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Список Id узлов голосования");
            obj.Field(d => d.AttachedFilesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id присоединенных файлов");
            obj.Field(d => d.VoteEntryId, nullable: true, type: typeof(IdGraphType)).Description("Id узла входа");
            obj.Field(d => d.RegionId, nullable: true, type: typeof(IdGraphType)).Description("Id Region");
            obj.Field(d => d.RubricId, nullable: true, type: typeof(IdGraphType)).Description("Id Rubric");
            obj.Field(d => d.TerritoryId, nullable: true, type: typeof(IdGraphType)).Description("Id Territory");
            obj.Field(d => d.TopicId, nullable: true, type: typeof(IdGraphType)).Description("Id Topic");
        }
        internal static void InitReadOnlyFields(ComplexGraphType<VoteRoot> obj)
        {
            obj.Field(d => d.StartedAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Когда началось");
            obj.Field(d => d.EndedAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Когда завершилось");
            obj.Field(d => d.ArchivedAt, nullable: true, type: typeof(DateTimeGraphType)).Description("Когда заархивировалось");
            obj.Field(d => d.State, nullable: true, type: typeof(VoteStatusEnumType)).Description("Текущее состояние (БУДЕТ/ИДЕТ/ЗАКОНЧИЛОСЬ)");
            obj.Field(d => d.AttachedFiles, nullable: true, type: typeof(ListGraphType<AttachedFileType>)).Description("Присоединенные файлы");
            obj.Field(d => d.Votes, nullable: true, type: typeof(ListGraphType<VoteType>)).Description("Узлы голосования");
            obj.Field(d => d.Region, nullable: true, type: typeof(RegionType)).Description("Id Region");
            obj.Field(d => d.Rubric, nullable: true, type: typeof(RubricType)).Description("Id Rubric");
            obj.Field(d => d.Territory, nullable: true, type: typeof(TerritoryType)).Description("Id Territory");
            obj.Field(d => d.Topic, nullable: true, type: typeof(TopicType)).Description("Id Topic");
            obj.Field(d => d.UsersCount).Description("Сколько пользователей голосовало");
            obj.Field(d => d.Messages, type: typeof(ListGraphType<MessageType>)).Description("Ответы");
        }
        public VoteRootType()
        {
            Name = nameof(VoteRoot);
        }
        protected override void InitFields()
        {
            base.InitFields();
            VoteRootType.InitMainFields(this);
            VoteRootType.InitReadOnlyFields(this);
        }
    }
    public class VoteRootUpdateType : BaseMutationType<VoteRoot>
    {
        public VoteRootUpdateType()
        {
            Name = "update" + nameof(VoteRoot);
        }
        protected override void InitFields()
        {
            base.InitFields();
            VoteRootType.InitMainFields(this);
            VoteRootType.InitWriteOnlyFields(this);
            Field(d => d.VotesIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Список Id узлов голосования (+/-)");
            //Field(d => d.AttachedFilesIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id присоединенных файлов (+/-)");
        }
    }
    public class VoteRootCreateType : BaseMutationType<VoteRoot>
    {
        public VoteRootCreateType()
        {
            Name = "create" + nameof(VoteRoot);
        }
        protected override void InitFields()
        {
            base.InitFields();
            VoteRootType.InitMainFields(this);
            VoteRootType.InitWriteOnlyFields(this);
        }
    }
    #endregion
    #region Vote
    public class VoteModeEnumType : EnumerationGraphType<Vote.ModeEnum>
    {
    }
    public class VoteType : BaseReadType<Vote>
    {
        internal static void InitMainFields(ComplexGraphType<Vote> obj)
        {
            obj.Field(d => d.Name, nullable: true).Description("Наименование");
            obj.Field(d => d.ShortName, nullable: true).Description("Короткое наименование");
            obj.Field(d => d.Text, nullable: true).Description("Описание голосования");
            obj.Field(d => d.TextResult, nullable: true).Description("Описание итогов голосования");
            obj.Field(d => d.Mode, nullable: true, type: typeof(VoteModeEnumType)).Description("Режим голосования ONE_SELECT: можно голосовать за 1 пункт, MANY_SELECT - за несколько пунктов");
            obj.Field(d => d.OfferEnabled, nullable: true).Description("Можно предложить свой вариант");
            obj.Field(d => d.ModeLimitValue, nullable: true).Description("Максимальное число пунктов в режиме MANY_SELECT");
            obj.Field(d => d.CommentsAllowed, nullable: true).Description("Пользователи могут оставлять комментарии");
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<Vote> obj)
        {
            obj.Field(d => d.VoteItemsIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Список Id пунктов голосования");
            obj.Field(d => d.ParentVoteItemsIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Родительские пункты многоузлового голосования");
            obj.Field(d => d.AttachedFilesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id присоединенных файлов");
            obj.Field(d => d.VoteRootId, nullable: true, type: typeof(IdGraphType)).Description("Id корня");
        }
        internal static void InitReadOnlyFields(ComplexGraphType<Vote> obj)
        {
            obj.Field(d => d.VoteRoot, nullable: true, type: typeof(VoteRootType)).Description("Корень опроса");
            obj.Field(d => d.Messages, type: typeof(ListGraphType<MessageType>)).Description("Ответы");
        }
        public VoteType()
        {
            Name = nameof(Vote);
        }
        protected override void InitFields()
        {
            base.InitFields();
            VoteType.InitMainFields(this);
            VoteType.InitReadOnlyFields(this);
            Field(d => d.VoteItems, nullable: true, type: typeof(ListGraphType<VoteItemType>)).Description("Пункты голосования");
            Field(d => d.ParentVoteItems, nullable: true, type: typeof(ListGraphType<VoteItemType>)).Description("Пункты голосования");
            Field(d => d.Users, nullable: true, type: typeof(ListGraphType<UserType>)).Description("Голосовавшие пользователи");
            Field(d => d.Offers, nullable: true, type: typeof(ListGraphType<VoteOfferType>)).Description("Индивидуальные предложения");
            Field(d => d.OffersCount).Description("Сколько пользователей внесли свои предложения");
            Field(d => d.AttachedFiles, nullable: true, type: typeof(ListGraphType<AttachedFileType>)).Description("Присоединенные файлы");
            Field(d => d.VoteWIV, nullable: true, type: typeof(VoteWIVType)).Description("Как я проголосовал");
            Field(d => d.UsersCount).Description("Сколько пользователей голосовало");
        }
    }
    public class VoteUpdateType : BaseMutationType<Vote>
    {
        public VoteUpdateType()
        {
            Name = "update" + nameof(Vote);
        }
        protected override void InitFields()
        {
            base.InitFields();
            VoteType.InitMainFields(this);
            VoteType.InitWriteOnlyFields(this);
            Field(d => d.VoteItems, nullable: true, type: typeof(ListGraphType<VoteItemUpdateType>)).Description("Пункты голосования");
            Field(d => d.VoteItemsIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Список Id пунктов голосования (+/-)");
            Field(d => d.ParentVoteItemsIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Родительские пункты многоузлового голосования (+/-)");
            //Field(d => d.AttachedFilesIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Присоединенных файлов (+/-)");
        }
    }
    public class VoteCreateType : BaseMutationType<Vote>
    {
        public VoteCreateType()
        {
            Name = "create" + nameof(Vote);
        }
        protected override void InitFields()
        {
            base.InitFields();
            VoteType.InitMainFields(this);
            VoteType.InitWriteOnlyFields(this);
            Field(d => d.VoteItems, nullable: true, type: typeof(ListGraphType<VoteItemCreateType>)).Description("Пункты голосования");
        }
    }
    #endregion
    #region VoteItem
    public class VoteItemType : BaseReadType<VoteItem>
    {
        internal static void InitMainFields(ComplexGraphType<VoteItem> obj)
        {
            obj.Field(d => d.Name, nullable: true).Description("Наименование");
            obj.Field(d => d.ShortName, nullable: true).Description("Короткое наименование");
            obj.Field(d => d.Description, nullable: true).Description("Пространное описание");
            obj.Field(d => d.Number, nullable: true).Description("Порядковый номер пункта");
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<VoteItem> obj)
        {
            obj.Field(d => d.VoteId, nullable: true, type: typeof(IdGraphType)).Description("Id голосования, (текущий узел)");
            obj.Field(d => d.NextVoteId, nullable: true, type: typeof(IdGraphType)).Description("Id голосования, (следующего узла)");
            obj.Field(d => d.AttachedFilesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id присоединенных файлов");
        }
        public VoteItemType()
        {
            Name = nameof(VoteItem);
        }
        protected override void InitFields()
        {
            base.InitFields();
            VoteItemType.InitMainFields(this);
            Field(d => d.Vote, nullable: true, type: typeof(VoteType)).Description("Голосование, (текущий узел)");
            Field(d => d.NextVote, nullable: true, type: typeof(VoteType)).Description("Голосование, (следующий узел)");
            Field(d => d.AttachedFiles, nullable: true, type: typeof(ListGraphType<AttachedFileType>)).Description("Присоединенные файлы");
            Field(d => d.Counter).Description("Сколько пользователей проголосовало за данный пункт");
        }
    }
    public class VoteItemUpdateType : BaseMutationType<VoteItem>
    {
        public VoteItemUpdateType()
        {
            Name = "update" + nameof(VoteItem);
        }
        protected override void InitFields()
        {
            base.InitFields();
            VoteItemType.InitMainFields(this);
            VoteItemType.InitWriteOnlyFields(this);
            //Field(d => d.AttachedFilesIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Присоединенных файлов (+/-)");
        }
    }
    public class VoteItemCreateType : BaseMutationType<VoteItem>
    {
        public VoteItemCreateType()
        {
            Name = "create" + nameof(VoteItem);
        }
        protected override void InitFields()
        {
            base.InitFields();
            VoteItemType.InitMainFields(this);
            VoteItemType.InitWriteOnlyFields(this);
        }
    }
    #endregion
}
