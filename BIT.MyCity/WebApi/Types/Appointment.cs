using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class AppointmentType : BaseReadType<Appointment>
    {
        internal static void InitMainFields(ComplexGraphType<Appointment> obj)
        {
            obj.Field(d => d.Number, nullable: true);
            obj.Field(d => d.Date, nullable: true, type: typeof(DateTimeGraphType));
            obj.Field(d => d.Description, nullable: true);
            obj.Field(d => d.Status, nullable: true);
            obj.Field(d => d.RejectionReason, nullable: true);
            obj.Field(d => d.AuthorName, nullable: true);
            obj.Field(d => d.AuthorNameUpperCase, nullable: true);
            obj.Field(d => d.Region, nullable: true);
            obj.Field(d => d.Municipality, nullable: true);
            obj.Field(d => d.Address, nullable: true);
            obj.Field(d => d.Zip, nullable: true);
            obj.Field(d => d.Phone, nullable: true);
            obj.Field(d => d.Email, nullable: true);
            obj.Field(d => d.Admin, nullable: true);
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<Appointment> obj)
        {
            obj.Field(d => d.AttachedFilesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>));
            obj.Field(d => d.AnswersIds, nullable: true, type: typeof(ListGraphType<IdGraphType>));
            //obj.Field(d => d.AttachedFilesIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>));
            obj.Field(d => d.AnswersIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>));
            obj.Field(d => d.RubricId, nullable: true, type: typeof(IdGraphType));
            obj.Field(d => d.OfficerId, nullable: true, type: typeof(IdGraphType));
            obj.Field(d => d.AppointmentRangeId, nullable: true, type: typeof(IdGraphType));
            obj.Field(d => d.TopicId, nullable: true, type: typeof(IdGraphType));
        }
        public AppointmentType()
        {
            Name = nameof(Appointment);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppointmentType.InitMainFields(this);
            Field(d => d.Rubric, nullable: true, type: typeof(RubricType));
            Field(d => d.Officer, nullable: true, type: typeof(OfficerType));
            Field(d => d.AppointmentRange, nullable: true, type: typeof(AppointmentRangeType));
            Field(d => d.Topic, nullable: true, type: typeof(TopicType));
            Field(d => d.AttachedFiles, nullable: true, type: typeof(ListGraphType<AttachedFileType>));
            Field(d => d.Answers, nullable: true, type: typeof(ListGraphType<AppointmentNotificationType>));
        }
    }
    public class AppointmentUpdateType : BaseMutationType<Appointment>
    {
        public AppointmentUpdateType()
        {
            Name = "update" + nameof(Appointment);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppointmentType.InitMainFields(this);
            AppointmentType.InitWriteOnlyFields(this);
        }
    }
    public class AppointmentCreateType : BaseMutationType<Appointment>
    {
        public AppointmentCreateType()
        {
            Name = "create" + nameof(Appointment);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppointmentType.InitMainFields(this);
            AppointmentType.InitWriteOnlyFields(this);
        }
    }
}
