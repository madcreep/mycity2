using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class EnumVerbType : BaseReadType<EnumVerb>
    {
        public EnumVerbType()
        {
            Name = nameof(EnumVerb);
            Field(d => d.Value, nullable: true);
            Field(d => d.ShortName, nullable: true);
            Field(d => d.Name, nullable: true);
            Field(d => d.Description, nullable: true);
            Field(d => d.Group, nullable: true);
            Field(d => d.SubsystemUID, nullable: true);
            Field(d => d.CustomSettings, nullable: true);
        }
    }
    public class EnumVerbUpdateType : BaseMutationType<EnumVerb>
    {
        public EnumVerbUpdateType()
        {
            Name = "update" + nameof(EnumVerb);
            Field(d => d.Id, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор объекта");
            Field(d => d.Value, nullable: true);
            Field(d => d.ShortName, nullable: true);
            Field(d => d.Name, nullable: true);
            Field(d => d.Description, nullable: true);
            Field(d => d.Group, nullable: true);
            Field(d => d.SubsystemUID, nullable: true);
            Field(d => d.CustomSettings, nullable: true);
        }
    }
    public class EnumVerbCreateType : BaseMutationType<EnumVerb>
    {
        public EnumVerbCreateType()
        {
            Name = "create" + nameof(EnumVerb);
            Field(d => d.Value, nullable: true);
            Field(d => d.ShortName, nullable: true);
            Field(d => d.Name, nullable: true);
            Field(d => d.Description, nullable: true);
            Field(d => d.Group, nullable: true);
            Field(d => d.SubsystemUID, nullable: true);
            Field(d => d.CustomSettings, nullable: true);
        }
    }
}
