using BIT.MyCity.Model;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Email
{
    public class EmailPersonType : InputObjectGraphType<EmailPerson>
    {
        public EmailPersonType()
        {
            Name = nameof(EmailPerson);
            
            Field(d => d.Email, true)
                .Description("Адрес электронной почты");

            Field(d => d.Name, true)
                .Description("Имя отправителя/получателя");
        }
    }

    public class CustomEmailType : InputObjectGraphType<CustomEmail>
    {
        public CustomEmailType()
        {
            Name = nameof(CustomEmail);

            Field(d => d.From, true, typeof(EmailPersonType))
                .Description("Отправитель");
            Field(d => d.To, type: typeof(ListGraphType<EmailPersonType>))
                .Description("Получатели");
            Field(d => d.Subject)
                .Description("Тема");
            Field(d => d.Message)
                .Description("Сообщение");
            Field(d => d.FilesId, true, typeof(ListGraphType<IdGraphType>))
                .Description("Идентификаторы прикрепляемых файлов");
            Field(d => d.WithDeliveryNotice, true)
                .Description("С уведомлением о доставке");
            Field(d => d.DuplicateToSender, true)
                .Description("Дублировать отправителю");
        }
    }
}
