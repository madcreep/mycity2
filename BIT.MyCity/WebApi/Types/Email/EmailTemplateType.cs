using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Email
{
    public class EmailTemplateType : ObjectGraphType<EmailTemplate>
    {
        public EmailTemplateType()
        {
            Field(d => d.Key, false, type: typeof(IdGraphType))
                .Description("Ключ");
            Field(d => d.Template)
                .Description("Шаблон");
            Field(d => d.CreatedAt, type: typeof(DateTimeGraphType))
                .Description("Дата создания");
            Field(d => d.UpdatedAt, true, typeof(DateTimeGraphType))
                .Description("Дата обновления");
        }
    }
}
