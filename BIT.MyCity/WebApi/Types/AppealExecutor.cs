﻿using BIT.MyCity.Database;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class AppealExecutorType : BaseReadType<AppealExecutor>
    {
        internal static void InitMainFields(ComplexGraphType<AppealExecutor> obj)
        {
            obj.Field(d => d.Name, nullable: true).Description("Наименование");
            obj.Field(d => d.ShortName, nullable: true).Description("Короткое наименование");
            obj.Field(d => d.Department, nullable: true).Description("---");
            obj.Field(d => d.DepartmentId, nullable: true).Description("---");
            obj.Field(d => d.Position, nullable: true).Description("---");
            obj.Field(d => d.Organization, nullable: true).Description("---");
            obj.Field(d => d.ExtId, nullable: true).Description("Идентификатор во внешней системе");
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<AppealExecutor> obj)
        {
            obj.Field(d => d.AppealIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id обращений, в которых есть такой исполнитель");
            obj.Field(d => d.AppealPrincipalIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id обращений, в которых есть такой ответственный исполнитель");
            obj.Field(d => d.AppealIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id обращений, в которых есть такой исполнитель (+/-)");
            obj.Field(d => d.AppealPrincipalIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id обращений, в которых есть такой ответственный исполнитель (+/-)");
            obj.Field(d => d.UserId, nullable: true, type: typeof(IdGraphType)).Description("Id пользователя");
        }
        internal static void InitReadOnlyFields(ComplexGraphType<AppealExecutor> obj)
        {
            obj.Field(d => d.Appeals, type: typeof(ListGraphType<AppealType>)).Description("Обращения, в которых есть такой исполнитель");
            obj.Field(d => d.AppealsPrincipal, type: typeof(ListGraphType<AppealType>)).Description("Обращения, в которых есть такой ответствнный исполнитель");
            obj.Field(d => d.User, nullable: true, type: typeof(UserType)).Description("Пользователь");
        }
        public AppealExecutorType()
        {
            Name = nameof(AppealExecutor);
            Description = "Исполнитель";
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppealExecutorType.InitMainFields(this);
            AppealExecutorType.InitReadOnlyFields(this);
        }
    }
    public class AppealExecutorUpdateType : BaseMutationType<AppealExecutor>
    {
        public AppealExecutorUpdateType()
        {
            Name = "update" + nameof(AppealExecutor);
            Description = "Обращение";
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppealExecutorType.InitMainFields(this);
            AppealExecutorType.InitWriteOnlyFields(this);
        }
    }
    public class AppealExecutorCreateType : BaseMutationType<AppealExecutor>
    {
        public AppealExecutorCreateType()
        {
            Name = "create" + nameof(AppealExecutor);
            Description = "Обращение";
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppealExecutorType.InitMainFields(this);
            AppealExecutorType.InitWriteOnlyFields(this);
        }
    }
}
