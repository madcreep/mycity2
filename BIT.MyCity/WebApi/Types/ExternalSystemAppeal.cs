using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class ExternalSystemAppeal : ObjectGraphType<Model.ExternalSystemAppeal>
    {
        public ExternalSystemAppeal()
        {
            Name = nameof(ExternalSystemAppeal);
            Field(d => d.Id, nullable: false, type: typeof(IdGraphType)).Description("Идентификатор обращения");
            Field(d => d.Number, nullable: false, type: typeof(StringGraphType)).Description("Номер обращения");
            Field(d => d.Date, nullable: false, type: typeof(DateGraphType)).Description("Дата обращения");
            Field(d => d.CitizenName, nullable: true, type: typeof(StringGraphType)).Description("Имя автора обращения");
            Field(d => d.Rubrics, nullable: false, type: typeof(ListGraphType<ExternalSystemAppealRubric>)).Description("Рубрики");
        }
    }

    public class ExternalSystemAppealRubric : ObjectGraphType<Model.ExternalSystemAppealRubric>
    {
        public ExternalSystemAppealRubric()
        {
            Name = nameof(ExternalSystemAppealRubric);
            Field(d => d.Name, nullable: false, type: typeof(StringGraphType)).Description("Название");
            Field(d => d.Status, nullable: false, type: typeof(StringGraphType)).Description("Статус");
        }
    }
}
