using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using GraphQL.Types;
using BIT.MyCity.Database;

namespace BIT.MyCity.WebApi.Types.Users
{
    public class PhoneType : ObjectGraphType<Phone>
    {
        public PhoneType()
        {
            Name = "PhoneType";
            Field(d => d.Id)
                .Description("Идентификатор объекта");
            Field(d => d.Type, type: typeof(TypePhoneType))
                .Description("Тип телефона");
            Field(d => d.Number)
                .Description("Номер телефона");
            Field(d => d.Additional, true)
                .Description("Добавочный номер");
            Field(d => d.Confirmed)
                .Description("Признак подтвержденного номера");
        }
    }

    public class PhoneInputType : InputObjectGraphType<Phone>
    {
        public PhoneInputType()
        {
            Name = "PhoneInputType";
            Field(d => d.Id, true)
                .Description("Идентификатор объекта")
                .AuthorizeWith(PolicyName.LoginSystemForm);
            Field(d => d.Type, type: typeof(TypePhoneType))
                .Description("Тип телефона")
                .AuthorizeWith(PolicyName.LoginSystemForm);
            Field(d => d.Number)
                .Description("Номер телефона")
                .AuthorizeWith(PolicyName.LoginSystemForm);
            Field(d => d.Additional, true)
                .Description("Добавочный номер")
                .AuthorizeWith(PolicyName.LoginSystemForm);
            Field(d => d.Confirmed, true)
                .Description("Признак подтвержденного номера")
                .AuthorizeWith(PolicyName.ManageUser);
        }
    }
}
