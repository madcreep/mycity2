using BIT.EsiaNETCore;
using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Users
{
    public class OrganizationType : ObjectGraphType<UserOrganization>
    {
        public OrganizationType()
        {
            Name = "OrganizationType";
            Field(d => d.Id).Description("Идентификатор объекта");
            Field(d => d.IsBrunch).Description("Признак филиала");
            Field(d => d.ShortName, true).Description("Краткое наименование");
            Field(d => d.FullName, true).Description("Полное наименование");
            Field(d => d.Inn, true).Description("ИНН");
            Field(d => d.Kpp, true).Description("КПП");
            Field(d => d.Ogrn, true).Description("ОГРН");
            Field(d => d.Email, true).Description("Email");
            Field(d => d.EmailVerified, true).Description("Email подтвержден");
            Field(d => d.Leg, true).Description("Код организационно-правовой формы по общероссийскому классификатору организационно-правовых форм");
            Field(d => d.AgencyTerRang, true).Description("Территориальная принадлежность ОГВ (только для государственных организаций, код по справочнику «Субъекты Российской федерации» (ССРФ)");
            Field(d => d.AgencyType, true, typeof(AgencyTypeType)).Description("тип ОГВ");
            Field(d => d.Addresses, true, typeof(ListGraphType<AddressType>)).Description("Адреса");
            Field(d => d.Phones, true, typeof(ListGraphType<PhoneType>)).Description("Телефоны");

            Field(d => d.Parent, true, typeof(ListGraphType<OrganizationType>))
                .Description("Головная организация")
                .AuthorizeWith(PolicyName.ManageUsersOrModerateOrganizations);
            Field(d => d.UserIds, true)
                .Description("Идентификаторы сотрудников организации")
                .AuthorizeWith(PolicyName.ManageUsersOrModerateOrganizations); ;
            Field(d => d.ChiefIds, true)
                .Description("Идентификаторы руководителей организации")
                .AuthorizeWith(PolicyName.ManageUsersOrModerateOrganizations); ;
        }
    }

    public class AgencyTypeType : EnumerationGraphType<AgencyType>
    { }
}
