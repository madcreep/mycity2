using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Model;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Users
{
    public class UserDirectoryIdsType : ObjectGraphType<UserSubsystemDirectoryId>
    {
        public UserDirectoryIdsType()
        {
            Name = nameof(UserDirectoryIdsType);
            Field(d => d.Ids)
                .Description("Идентификатор элемента справочника");
            Field(d => d.SubsystemUid)
                .Description("Ключ подсистемы");
        }
    }

    public class UserDirectoryIdsInputType : InputObjectGraphType<UserSubsystemDirectoryId>
    {
        public UserDirectoryIdsInputType()
        {
            Name = nameof(UserDirectoryIdsInputType);
            Field(d => d.Ids, true)
                .Description("Идентификатор элемента справочника");
            Field(d => d.SubsystemUid)
                .Description("Ключ подсистемы");
        }
    }
}
