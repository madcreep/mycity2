using BIT.EsiaNETCore;
using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Users
{
    public class AddressType : ObjectGraphType<Address>
    {
        public AddressType()
        {
            Name = nameof(AddressType);
            Field(d => d.Id)
                .Description("Идентификатор объекта");
            Field(d => d.AddrType, type: typeof(AddressTypeType))
                .Description("Тип адреса");
            Field(d => d.ZipCode, true)
                .Description("Индекс");
            Field(d => d.CountryId, true)
                .Description("Идентификатор страны");
            Field(d => d.Building, true)
                .Description("Строение");
            Field(d => d.Frame, true)
                .Description("Корпус");
            Field(d => d.House, true)
                .Description("Дом");
            Field(d => d.Flat, true)
                .Description("Квартира");
            Field(d => d.FiasCode, true)
                .Description("Код КЛАДР");
            Field(d => d.Region, true)
                .Description("Регион");
            Field(d => d.City, true)
                .Description("Город");
            Field(d => d.District, true)
                .Description("Внутригородской район");
            Field(d => d.Area, true)
                .Description("Район");
            Field(d => d.Settlement, true)
                .Description("Поселение");
            Field(d => d.AdditionArea, true)
                .Description("Доп. территория");
            Field(d => d.AdditionAreaStreet, true)
                .Description("Улица на доп. территории");
            Field(d => d.Street, true)
                .Description("Улица");
            Field(d => d.PartialAddress, true)
                .Description("Строка адреса (не включая дом, строение, корпус, номер квартиры)");
            Field(d => d.FullAddress, true)
                .Description("Строка полного адреса");
        }
    }

    public class AddressTypeType : EnumerationGraphType<AddrType>
    { }
}
