using BIT.GraphQL.Authorization;
using GraphQL.Types;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.WebApi.Types.Authorization;
using BIT.MyCity.WebApi.Types.Claims;
using BIT.MyCity.WebApi.Types.Roles;

namespace BIT.MyCity.WebApi.Types.Users
{
    public class UserType : ObjectGraphType<User>
    {
        public UserType()
        {
            Name = nameof(User);

            Field(d => d.Id)
                .Description("Идентификатор объекта");

            Field(d => d.LastName, nullable: true)
                .Description("Фамилия");

            Field(d => d.FirstName, nullable: true)
                .Description("Имя");

            Field(d => d.MiddleName, nullable: true)
                .Description("Отчество");

            Field(d => d.FullName, nullable: true)
                .Description("Полное Имя");

            Field(d => d.Gender, nullable: true, typeof(GenderType))
                .Description("Пол");

            Field(d => d.Inn, nullable: true)
                .Description("ИНН");

            Field(d => d.Snils, nullable: true)
                .Description("СНИЛС");

            Field(d => d.Email, nullable: true)
                .Description("Адрес электронной почты");

            Field(d => d.EmailConfirmed, nullable: true)
                .Description("Признак подтвержденного адреса электронной почты");

            Field(d => d.ProfileConfirmed, nullable: true)
                .Description("Признак подтвержденного профиля");

            Field(d => d.CreatedAt, false, typeof(DateTimeGraphType))
                .Description("Дата создания объекта");

            Field(d => d.UpdatedAt, nullable: true, typeof(DateTimeGraphType))
                .Description("Дата изменения объекта")
                .AuthorizeWith(PolicyName.ManageUser);

            Field(d => d.Disconnected)
                .Description("Признак блокировки")
                //.AuthorizeWith(PolicyName.ManageUsersOrModerate)
                ;

            Field(d => d.DisconnectedTimestamp, nullable: true, typeof(DateTimeGraphType))
                .Description("Штамп времени блокировки")
                .AuthorizeWith(PolicyName.ManageUsersOrModerate);
            
            Field(d => d.UserName, nullable: true)
                .Description("Логин");

            Field(d => d.LoginSystem, type: typeof(LoginSystemType))
                .Description("Система логирования")
                .AuthorizeWith(PolicyName.ManageUsersOrModerate);

            Field(d => d.Phones, nullable: false, typeof(ListGraphType<PhoneType>))
                .Description("Телефоны");

            Field(d => d.Addresses, nullable: false, typeof(ListGraphType<AddressType>))
                .Description("Телефоны");

            Field(d => d.Roles, nullable: true, typeof(ListGraphType<RoleType>))
                .Description("Роли")
                .AuthorizeWith(PolicyName.ManageUser);

            Field(d => d.Claims, nullable: true, typeof(ListGraphType<ClaimType>))
                .Description("Разрешения")
                .AuthorizeWith(PolicyName.ManageUser);

            Field(d => d.ExternalId, nullable: true)
                .Description("Идентификатор пользователя во внешней системе")
                .AuthorizeWith(PolicyName.ManageUser);

            Field(d => d.OldSystemId, nullable: true)
                .Description("Идентификатор пользователя в системе из которой осуществен перенос")
                .AuthorizeWith(PolicyName.ManageUser);

            //Field(d => d.RubricIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsType>))
            //    .Description("Идентификаторы рубрик по подсистемам")
            //    .AuthorizeWith(PolicyName.ManageUsersOrModerate);

            //Field(d => d.TerritoryIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsType>))
            //    .Description("Идентификаторы территорий по подсистемам")
            //    .AuthorizeWith(PolicyName.ManageUsersOrModerate);

            //Field(d => d.RegionIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsType>))
            //    .Description("Идентификаторы регионов по подсистемам")
            //    .AuthorizeWith(PolicyName.ManageUsersOrModerate);

            //Field(d => d.TopicIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsType>))
            //    .Description("Идентификаторы тем по подсистемам")
            //    .AuthorizeWith(PolicyName.ManageUsersOrModerate);

            Field(d => d.Organizations, nullable: true, typeof(ListGraphType<OrganizationType>))
                .Description("Организации")
                .AuthorizeWith(PolicyName.ManageUsersOrModerateOrganizationsOrSubsystemOrganization);
            
            Field(d => d.UserIdentityDocuments, nullable: true, typeof(ListGraphType<UserIdentityDocumentsType>))
                .Description("Документы");

            Field(d => d.AllClaims, nullable: true, typeof(ListGraphType<ClaimType>))
                .Description("Сводный список разрешений");

            Field(d => d.SelfFormRegister, nullable: false, typeof(BooleanGraphType))
                .Description("Признак пользователя зарегистрировавшегося через формы");
        }
    }

    public class UserInputType : InputObjectGraphType<User>
    {
        public UserInputType()
        {
            Name = nameof(UserInputType);
            Field(d => d.FullName, nullable: true)
                .Description("Полное Имя")
                .AuthorizeWith(PolicyName.LoginSystemForm);

            Field(d => d.UserName, nullable: true)
                .Description("Логин")
                .AuthorizeWith(PolicyName.LoginSystemForm);

            Field(d => d.Gender, nullable: true, typeof(GenderType))
                .Description("Пол")
                .AuthorizeWith(PolicyName.LoginSystemForm);

            Field(d => d.Email, nullable: true)
                .Description("Адрес электронной почты")
                .AuthorizeWith(PolicyName.LoginSystemForm);

            Field(d => d.EmailConfirmed, nullable: true, typeof(BooleanGraphType))
                .Description("Признак подтвержденности электронной почты")
                .AuthorizeWith(PolicyName.ManageUser);

            Field(d => d.Disconnected, nullable: true)
                .Description("Признак отключенного пользователя")
                .AuthorizeWith(PolicyName.ManageUser);

            Field(d => d.Phones, nullable: true, typeof(ListGraphType<PhoneInputType>))
                .Description("Телефоны")
                .AuthorizeWith(PolicyName.LoginSystemForm);

            Field(d => d.Claims, nullable: true, typeof(ListGraphType<ClaimInputType>))
                .Description("Клаймы")
                .AuthorizeWith(PolicyName.ManageUser);

            Field(d => d.RoleIds, nullable: true, typeof(ListGraphType<BigIntGraphType>))
                .Description("Идентификаторы ролей")
                .AuthorizeWith(PolicyName.ManageUser);

            Field(d => d.ExternalId, nullable: true)
                .Description("Идентификатор пользователя во внешней системе")
                .AuthorizeWith(PolicyName.ManageUser);

            Field(d => d.OldSystemId, nullable: true)
                .Description("Идентификатор пользователя в системе из которой осуществен перенос")
                .AuthorizeWith(PolicyName.ManageUser);

            //Field(d => d.RubricIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsInputType>))
            //    .Description("Идентификаторы рубрик по подсистемам")
            //    .AuthorizeWith(PolicyName.ManageUser);

            //Field(d => d.TerritoryIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsInputType>))
            //    .Description("Идентификаторы территорий по подсистемам")
            //    .AuthorizeWith(PolicyName.ManageUser);

            //Field(d => d.RegionIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsInputType>))
            //    .Description("Идентификаторы регионов по подсистемам")
            //    .AuthorizeWith(PolicyName.ManageUser);

            //Field(d => d.TopicIds, nullable: true, typeof(ListGraphType<UserDirectoryIdsInputType>))
            //    .Description("Идентификаторы тем по подсистемам")
            //    .AuthorizeWith(PolicyName.ManageUser);
        }
    }
}
