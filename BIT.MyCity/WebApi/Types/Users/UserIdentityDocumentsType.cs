using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.EsiaNETCore;
using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Users
{
    public class UserIdentityDocumentsType : ObjectGraphType<UserIdentityDocument>
    {
        public UserIdentityDocumentsType()
        {
            Name = nameof(UserIdentityDocumentsType);
            Field(d => d.Id).Description("Идентификатор объекта");
            Field(d => d.Type, false, typeof(DocTypeType)).Description("Тип документа");
            Field(d => d.Verified).Description("Подтвержден");
            Field(d => d.Series, true).Description("Серия документа");
            Field(d => d.Number, true).Description("Номер документа");
            Field(d => d.IssueDate, true).Description("Дата выдачи");
            Field(d => d.IssueId, true).Description("Код подразделения");
            Field(d => d.IssuedBy, true).Description("Кем выдан");
            Field(d => d.ExpiryDate, true).Description("Cрок действия документа");
        }
    }

    public class DocTypeType : EnumerationGraphType<DocType>
    { }
}
