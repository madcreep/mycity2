using BIT.MyCity.ApiInterface.Models;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Users
{
    public class OrderByItemType : InputObjectGraphType<OrderByItem>
    {
        public OrderByItemType()
        {
            Name = nameof(OrderByItemType);
            Field(d => d.Field)
                .Description("Имя поля для сортировки");
            Field(d => d.Direction, true, typeof(SearchDirectionType))
                .Description("Направление сортировки");
        }
    }

    public class SearchDirectionType : EnumerationGraphType<SearchDirection>
    {
    }
}
