﻿using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class AppointmentNotificationType : BaseReadType<AppointmentNotification>
    {
        internal static void InitMainFields(ComplexGraphType<AppointmentNotification> obj)
        {
            obj.Field(d => d.Date, nullable: true, type: typeof(DateTimeGraphType));
            obj.Field(d => d.Status, nullable: true);
            obj.Field(d => d.Html, nullable: true);
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<AppointmentNotification> obj)
        {
            obj.Field(d => d.AppointmentId, nullable: true);
        }
        public AppointmentNotificationType()
        {
            Name = nameof(AppointmentNotification);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppointmentNotificationType.InitMainFields(this);
            Field(d => d.Appointment, nullable: true, type: typeof(AppointmentType));
        }
    }
    public class AppointmentNotificationUpdateType : BaseMutationType<AppointmentNotification>
    {
        public AppointmentNotificationUpdateType()
        {
            Name = "update" + nameof(AppointmentNotification);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppointmentNotificationType.InitMainFields(this);
            AppointmentNotificationType.InitWriteOnlyFields(this);
        }
    }
    public class AppointmentNotificationCreateType : BaseMutationType<AppointmentNotification>
    {
        public AppointmentNotificationCreateType()
        {
            Name = "create" + nameof(AppointmentNotification);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppointmentNotificationType.InitMainFields(this);
            AppointmentNotificationType.InitWriteOnlyFields(this);
        }
    }
}
