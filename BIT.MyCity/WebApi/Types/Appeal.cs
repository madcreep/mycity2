using GraphQL.Types;
using BIT.MyCity.Database;
using BIT.MyCity.WebApi.Types.Users;
using BIT.MyCity.Model;

namespace BIT.MyCity.WebApi.Types
{
    public class ModerationStageEnumType : EnumerationGraphType<ModerationStageEnum>
    {
    }
    public class ApprovedEnumType : EnumerationGraphType<ApprovedEnum>
    {
    }
    public class RegardModeEnumType : EnumerationGraphType<RegardModeEnum>
    {
    }
    public class AppealTypeEnumType : EnumerationGraphType<AppealTypeEnum>
    {
    }
    public class AppealType : BaseReadType<Appeal>
    {
        internal static void InitMainFields(ComplexGraphType<Appeal> obj)
        {
            obj.Field(d => d.ExtId, nullable: true).Description("---");
            obj.Field(d => d.ExtDate, nullable: true, type: typeof(DateTimeGraphType)).Description("Дата регистрации");
            obj.Field(d => d.ExtNumber, nullable: true).Description("Рег №");
            obj.Field(d => d.Addressee, nullable: true).Description("Должностное лицо - адресат сообщения");
            obj.Field(d => d.AuthorName, nullable: true).Description("---");
            obj.Field(d => d.FirstName, nullable: true).Description("---");
            obj.Field(d => d.LastName, nullable: true).Description("---");
            obj.Field(d => d.Patronim, nullable: true).Description("---");
            obj.Field(d => d.Title, nullable: true).Description("---");
            obj.Field(d => d.Description, nullable: true).Description("---");
            obj.Field(d => d.TopicUpdated, nullable: true).Description("---");
            obj.Field(d => d.SiteStatus, nullable: true).Description("---");
            obj.Field(d => d.MarkSend, nullable: true).Description("---");
            obj.Field(d => d.StatusConfirmed, nullable: true).Description("---");
            obj.Field(d => d.PushesSent, nullable: true).Description("---");
            obj.Field(d => d.Number, nullable: true).Description("---");
            obj.Field(d => d.Fulltext, nullable: true).Description("---").DeprecationReason("Не пригодилась");
            obj.Field(d => d.Platform, nullable: true).Description("---");
            obj.Field(d => d.SentToDelo, nullable: true).Description("---");
            obj.Field(d => d.AnswerByPost, nullable: true).Description("---");
            obj.Field(d => d.ExternalExec, nullable: true).Description("---");
            obj.Field(d => d.Address, nullable: true).Description("---");
            obj.Field(d => d.Zip, nullable: true).Description("---");
            obj.Field(d => d.Region1, nullable: true).Description("---");
            obj.Field(d => d.Phone, nullable: true).Description("---");
            obj.Field(d => d.Municipality, nullable: true).Description("---");
            obj.Field(d => d.Views, nullable: true).Description("---");
            obj.Field(d => d.Uploaded, nullable: true).Description("---");
            obj.Field(d => d.ClientType, nullable: true).Description("---");
            obj.Field(d => d.Inn, nullable: true).Description("---");
            obj.Field(d => d.ResDate, nullable: true, type: typeof(DateTimeGraphType)).Description("---");
            obj.Field(d => d.PlanDate, nullable: true, type: typeof(DateTimeGraphType)).Description("---");
            obj.Field(d => d.FactDate, nullable: true, type: typeof(DateTimeGraphType)).Description("---");
            obj.Field(d => d.DeloText, nullable: true).Description("---");
            obj.Field(d => d.DeloStatus, nullable: true).Description("---");
            obj.Field(d => d.Collective, nullable: true).Description("---");
            obj.Field(d => d.FilesPush, nullable: true, type: typeof(DateTimeGraphType)).Description("---");

            obj.Field(d => d.PublicGranted, nullable: true).Description("Публичность запрошена");
            obj.Field(d => d.Signatory, nullable: true).Description("Подписал");
            obj.Field(d => d.Post, nullable: true).Description("Должность");
            obj.Field(d => d.RegNumber, nullable: true).Description("Регистрационный номер документа в организации/ИП");
            obj.Field(d => d.RegDate, nullable: true, type: typeof(DateTimeGraphType)).Description("Дата регистрации документа");
            obj.Field(d => d.WORegNumberRegDate, nullable: true).Description("Без номера");
            obj.Field(d => d.Latitude, nullable: true).Description("Место на карте: широта");
            obj.Field(d => d.Longitude, nullable: true).Description("Место на карте: долгота");
            obj.Field(d => d.Place, nullable: true).Description("Место на карте: описание");
            obj.Field(d => d.RegardMode, nullable: true, type: typeof(RegardModeEnumType)).Description("Режим оценки отчетов исполнителей");

            obj.Field(d => d.Email, nullable: true).Description("---");
            obj.Field(d => d.SubscribeToAnswers, nullable: true, type: typeof(SubscribeToAnswersEnumType)).Description("Уведомлять об ответах на данное обращение");
            obj.Field(d => d.OldSystemId, nullable: true).Description("Идентификатор обращения в системе из которой осуществен перенос");
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<Appeal> obj)
        {
            obj.Field(d => d.RubricId, nullable: true, type: typeof(IdGraphType)).Description("Id Рубрики");
            obj.Field(d => d.RegionId, nullable: true, type: typeof(IdGraphType)).Description("Id Региона");
            obj.Field(d => d.TopicId, nullable: true, type: typeof(IdGraphType)).Description("Id Топика");
            obj.Field(d => d.TerritoryId, nullable: true, type: typeof(IdGraphType)).Description("Id Территории");
            obj.Field(d => d.AttachedFilesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Присоединенных файлов");
            //obj.Field(d => d.AttachedFilesIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Присоединенных файлов (+/-)");
            obj.Field(d => d.ModeratorsIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Модераторов");
            obj.Field(d => d.ModeratorsIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Модераторов (+/-)");
            obj.Field(d => d.OrganizationId, nullable: true, type: typeof(IdGraphType)).Description("Id Организации");
            obj.Field(d => d.AppealExecutorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Исполнителей");
            obj.Field(d => d.AppealExecutorIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Исполнителей (+/-)");
            obj.Field(d => d.AppealPrincipalExecutorIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Ответственных исполнителей");
            obj.Field(d => d.AppealPrincipalExecutorIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Ответственных исполнителей (+/-)");
        }
        internal static void InitReadOnlyFields(ComplexGraphType<Appeal> obj)
        {
            obj.Field(d => d.LikesCount).Description("Счетчик лайков");
            obj.Field(d => d.ViewsCount).Description("Счетчик просмотров");

            obj.Field(d => d.Rubric, nullable: true, type: typeof(RubricType)).Description("Рубрика");
            obj.Field(d => d.Region, nullable: true, type: typeof(RegionType)).Description("Регион");
            obj.Field(d => d.Topic, nullable: true, type: typeof(TopicType)).Description("Топик");
            obj.Field(d => d.Territory, nullable: true, type: typeof(TerritoryType)).Description("Территория");
            obj.Field(d => d.Author, nullable: true, type: typeof(UserType)).Description("Автор");
            obj.Field(d => d.Organization, nullable: true, type: typeof(OrganizationType)).Description("Организация");

            obj.Field(d => d.AttachedFiles, type: typeof(ListGraphType<AttachedFileType>)).Description("Присоединенные файлы");
            obj.Field(d => d.Moderators, type: typeof(ListGraphType<UserType>)).Description("Модераторы");
            obj.Field(d => d.UsersLiked, type: typeof(ListGraphType<UserType>)).Description("Кто поставил лайк или дизлайк");

            obj.Field(d => d.Approved, type: typeof(ApprovedEnumType)).Description("Принято к публикации/отказано");
            obj.Field(d => d.RejectionReason, nullable: true).Description("Причина отказа в публикации");
            obj.Field(d => d.Public_).Description("Публичность подтверждена");
            obj.Field(d => d.ModerationStage, type: typeof(ModerationStageEnumType)).Description("Стадии модерации");
            obj.Field(d => d.AppealExecutors, type: typeof(ListGraphType<AppealExecutorType>)).Description("Исполнители");
            obj.Field(d => d.AppealPrincipalExecutors, type: typeof(ListGraphType<AppealExecutorType>)).Description("Ответственные исполнители");
            obj.Field(d => d.NeedToUpdateInExt, nullable: true).Description("Нужно обновить во внешней системе");
            obj.Field(d => d.Subsystem, nullable: true, type: typeof(SubsystemType)).Description("Подсистема");
            obj.Field(d => d.LikeWIL, nullable: true, type: typeof(LikeWILType)).Description("Как я лайкнул");
            obj.Field(d => d.ViewWIV, nullable: true, type: typeof(ViewWIVType)).Description("Как я посмотрел");
            obj.Field(d => d.AppealType, nullable: false, type: typeof(AppealTypeEnumType)).Description("Тип обращения (обычное/сообщение обратной связи)");
            obj.Field(d => d.Messages, type: typeof(ListGraphType<MessageType>)).Description("Ответы");
            obj.Field(d => d.HasUnapprovedMessages).Description("Есть неотмодерированные комментарии");
            obj.Field(d => d.UnapprovedMessages, type: typeof(ListGraphType<MessageType>)).Description("неотмодерированные комментарии");
            obj.Field(d => d.UnapprovedMessagesCount).Description("---");
            obj.Field(d => d.MessagesCount).Description("Всего комментариев");
        }
        public AppealType()
        {
            Name = nameof(Appeal);
            Description = "Обращение";
        }
        protected override void InitFields()
        {
            base.InitFields();
            InitMainFields(this);
            InitReadOnlyFields(this);
        }
    }

    public class AppealStatusType : ObjectGraphType<AppealStatus>
    {
        public AppealStatusType()
        {
            Name = $"{nameof(Appeal)}ByNumberAndDateStatus";
            Description = "Статус Обращения";
            Field(d => d.Id, nullable: false, type: typeof(IdGraphType)).Description("Идентификатор обращения");
            Field(d => d.Weight, nullable: true).Description("Вес сортировки");
            Field(d => d.RubricId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор рубрики");
            Field(d => d.Rubric, nullable: true, type: typeof(RubricType)).Description("Рубрика");
            Field(d => d.RegionId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор региона");
            Field(d => d.Region, nullable: true, type: typeof(RegionType)).Description("Регион");
            Field(d => d.TopicId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор топика");
            Field(d => d.Topic, nullable: true, type: typeof(TopicType)).Description("Топик");
            Field(d => d.TerritoryId, nullable: true, type: typeof(IdGraphType)).Description("Идентификатор территории");
            Field(d => d.Territory, nullable: true, type: typeof(TerritoryType)).Description("Территория");
            Field(d => d.SiteStatus, nullable: false).Description("Идентификатор статуса");
            Field(d => d.ExtNumber, nullable: true).Description("Регистрационный номер во внешней системе");
            Field(d => d.ExtDate, nullable: true, type: typeof(DateTimeGraphType)).Description("Дата регистрации во внешней системе");
        }
    }



    public class AppealUpdateType : BaseMutationType<Appeal>
    {
        public AppealUpdateType()
        {
            Name = "update" + nameof(Appeal);
            Description = "Обращение";
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppealType.InitMainFields(this);
            AppealType.InitWriteOnlyFields(this);
            Field(d => d.NeedToUpdateInExt, nullable: true).Description("Нужно обновить во внешней системе");
        }
    }
    public class AppealCreateType : BaseMutationType<Appeal>
    {
        public AppealCreateType()
        {
            Name = "create" + nameof(Appeal);
            Description = "Обращение";
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppealType.InitMainFields(this);
            AppealType.InitWriteOnlyFields(this);
            Field(d => d.SubsystemId, nullable: false, type: typeof(NonNullGraphType<IdGraphType>)).Description("Уникальное наименование подсистемы");
            Field(d => d.AppealType, nullable: true, type: typeof(AppealTypeEnumType)).Description("Тип обращения (обычное/сообщение обратной связи)");
        }
    }
    public class AppealFBCreateType : BaseMutationType<Appeal>
    {
        public AppealFBCreateType()
        {
            Name = "createFB" + nameof(Appeal);
            Description = "Сообщение обратной связи";
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppealType.InitMainFields(this);
            AppealType.InitWriteOnlyFields(this);
            Field(d => d.SubsystemId, nullable: false, type: typeof(NonNullGraphType<IdGraphType>)).Description("Уникальное наименование подсистемы");
        }
    }
    public class AppealApproveType : InputObjectGraphType<AppealApprove>
    {
        public AppealApproveType()
        {
            Name = "approve" + nameof(Appeal);
            Description = "Принятие к публикации или отклонение обращения";
            Field(d => d.ApprovedEnum, nullable: true, type: typeof(ApprovedEnumType)).Description("Принято к публикации/отказано");
            Field(d => d.RejectionReason, nullable: true).Description("Причина отказа в публикации");
            Field(d => d.ModerationStage, nullable: true, type: typeof(ModerationStageEnumType)).Description("Стадии модерации");
            Field(d => d.Public_, nullable: true).Description("Публичное");
        }
    }

    public class AppealsStatByGroupType : ObjectGraphType<AppealsStatBy>
    {
        public AppealsStatByGroupType()
        {
            Name = nameof(AppealsStatBy);
            Field(d => d.Rubric, nullable: true, typeof(RubricType)).Description("");
            Field(d => d.Region, nullable: true, typeof(RegionType)).Description("");
            Field(d => d.Territory, nullable: true, typeof(TerritoryType)).Description("");
            Field(d => d.Topic, nullable: true, typeof(TopicType)).Description("");
            Field(d => d.CreatedAtYear, nullable: true, type: typeof(DateTimeGraphType)).Description("");
            Field(d => d.UpdatedAtYear, nullable: true, type: typeof(DateTimeGraphType)).Description("");
            Field(d => d.CreatedAtMonth, nullable: true, type: typeof(DateTimeGraphType)).Description("");
            Field(d => d.UpdatedAtMonth, nullable: true, type: typeof(DateTimeGraphType)).Description("");
            Field(d => d.CreatedAtDay, nullable: true, type: typeof(DateTimeGraphType)).Description("");
            Field(d => d.UpdatedAtDay, nullable: true, type: typeof(DateTimeGraphType)).Description("");
            Field(d => d.CreatedAtHour, nullable: true, type: typeof(DateTimeGraphType)).Description("");
            Field(d => d.UpdatedAtHour, nullable: true, type: typeof(DateTimeGraphType)).Description("");
            //Field(d => d.CreatedAtYear, nullable: true).Description("");
            //Field(d => d.UpdatedAtYear, nullable: true).Description("");
            //Field(d => d.CreatedAtMonth, nullable: true).Description("");
            //Field(d => d.UpdatedAtMonth, nullable: true).Description("");
            //Field(d => d.CreatedAtDay, nullable: true).Description("");
            //Field(d => d.UpdatedAtDay, nullable: true).Description("");
            //Field(d => d.CreatedAtHour, nullable: true).Description("");
            //Field(d => d.UpdatedAtHour, nullable: true).Description("");
            Field(d => d.AppealCount, nullable: false).Description("");
        }
    }
}
