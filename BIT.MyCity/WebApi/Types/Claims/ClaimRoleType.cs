using System.Security.Claims;
using GraphQL.Types;
using Microsoft.AspNetCore.Identity;

namespace BIT.MyCity.WebApi.Types.Claims
{
    public class ClaimType : ObjectGraphType<Claim>
    {
        public ClaimType()
        {
            Name = nameof(ClaimType);
            Field(d => d.Type)
                .Description("Тип разрешения");
            Field(d => d.Value)
                .Description("Значение разрешения");
        }
    }

    public class ClaimInputType : InputObjectGraphType<Claim>
    {
        public ClaimInputType()
        {
            Name = nameof(ClaimInputType);
            Field(d => d.Type)
                .Description("Тип разрешения");
            Field(d => d.Value)
                .Description("Значение разрешения");
        }
    }







    //public class ClaimUserType : ObjectGraphType<IdentityUserClaim<long>>
    //{
    //    public ClaimUserType()
    //    {
    //        Name = nameof(ClaimUserType);
    //        //Field(d => d.Id)
    //        //    .Description("Идентификатор");
    //        Field(d => d.ClaimType)
    //            .Description("Тип разрешения");
    //        Field(d => d.ClaimValue)
    //            .Description("Значение разрешения");
    //    }
    //}

    //public class ClaimRoleType : ObjectGraphType<IdentityRoleClaim<long>>
    //{
    //    public ClaimRoleType()
    //    {
    //        Name = nameof(ClaimRoleType);
    //        //Field(d => d.Id)
    //        //    .Description("Идентификатор");
    //        Field(d => d.ClaimType)
    //            .Description("Тип разрешения");
    //        Field(d => d.ClaimValue)
    //            .Description("Значение разрешения");
    //    }
    //}

    //public class ClaimUserInputType : InputObjectGraphType<IdentityUserClaim<long>>
    //{
    //    public ClaimUserInputType()
    //    {
    //        Name = nameof(ClaimUserInputType);
    //        //Field(d => d.Id, true)
    //        //    .Description("Иднетификатор");
    //        Field(d => d.ClaimType)
    //            .Description("Тип разрешения");
    //        Field(d => d.ClaimValue)
    //            .Description("Значение разрешения");
    //    }
    //}

    //public class ClaimRoleInputType : InputObjectGraphType<IdentityRoleClaim<long>>
    //{
    //    public ClaimRoleInputType()
    //    {
    //        Name = nameof(ClaimRoleInputType);
    //        //Field(d => d.Id, true)
    //        //    .Description("Иднетификатор");
    //        Field(d => d.ClaimType)
    //            .Description("Тип разрешения");
    //        Field(d => d.ClaimValue)
    //            .Description("Значение разрешения");
    //    }
    //}
}
