using BIT.MyCity.ApiInterface;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Claims
{
    public class ClaimInfoType : ObjectGraphType<ClaimInfoModel>
    {
        public ClaimInfoType()
        {
            Name = nameof(ClaimInfoType);
            Field(d => d.ClaimType)
                .Description("Тип разрешения");
            Field(d => d.Description)
                .Description("Описание разрешения");
            Field(d => d.ClaimEntity, type: typeof(ListGraphType<ClaimEntityType>))
                .Description("Сущности к которым может применяться разрешение");
            Field(d => d.Values, true, typeof(ListGraphType<ClaimInfoValueType>))
                .Description("Возможные значения");
        }
    }

    public class ClaimInfoValueType : ObjectGraphType<ClaimValueModel>
    {
        public ClaimInfoValueType()
        {
            Name = nameof(ClaimInfoValueType);
            Field(d => d.Value)
                .Description("Значение");
            Field(d => d.Description)
                .Description("Описание");
        }
    }

    public class ClaimEntityType : EnumerationGraphType<ClaimEntity>
    { }
}
