﻿using BIT.MyCity.Model;
using GraphQL.Types;
using System;

namespace BIT.MyCity.WebApi.Types
{
    public class ApiInformationType : ObjectGraphType<ApiInformation>
    {
        public ApiInformationType()
        {
            Name = nameof(ApiInformation);
            Field(d => d.OsDescription, nullable: true).Description("Host OS version");
            Field(d => d.AppBuildTime, nullable: true, typeof(DateTimeGraphType)).Description("Дата сборки");
            Field(d => d.AspDotnetVersion, nullable: true).Description("Framework version");
            Field(d => d.AppVersion, nullable: true).Description("Версия сборки");
            Field(d => d.AppConfiguration, nullable: true).Description("Конфигурация сборки");
            Field(d => d.ASPNETCORE_ENVIRONMENT, nullable: true).Description("ASPNETCORE_ENVIRONMENT");
            Field(d => d.DbVersion, nullable: true).Description("DB Info");
        }
    }
}
