using System;
using GraphQL.Types;

namespace Mycity2.WebApi.Types
{
    public abstract class MyCityObject
    {
        //         У всех объектов моделей данных присутствуют обязательные поля:
        // id - идентификатор объекта,
        // createdAt - дата создания объекта
        // updatedAt - дата изменения объекта
        public uint Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        
    }
    //public class MyCityObjectInterface : InterfaceGraphType<MyCityObject>
    //{


    //    public MyCityObjectInterface()
    //    {
    //        Name = "MyCityObject";

    //        Field(d => d.Id).Description("Идентификатор объекта");
    //        Field(d => d.CreatedAt).Description("Дата создания объекта");
    //        Field(d => d.UpdatedAt).Description("Дата изменения объекта");
    //    }
    //}
}
