﻿using BIT.MyCity.Database;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class AppointmentRangeType : BaseReadType<AppointmentRange>
    {
        internal static void InitMainFields(ComplexGraphType<AppointmentRange> obj)
        {
            obj.Field(d => d.Date, nullable: true, type: typeof(DateTimeGraphType));
            obj.Field(d => d.Start, nullable: true, type: typeof(DateTimeGraphType));
            obj.Field(d => d.End, nullable: true, type: typeof(DateTimeGraphType));
            obj.Field(d => d.Appointments, nullable: true);
            obj.Field(d => d.OfficerName, nullable: true);
            obj.Field(d => d.RubricOrder, nullable: true);
            obj.Field(d => d.RubricName, nullable: true);
            obj.Field(d => d.DeleteMark, nullable: true);
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<AppointmentRange> obj)
        {
            obj.Field(d => d.OfficerId, nullable: true, type: typeof(IdGraphType));
            obj.Field(d => d.RegisteredAppointmentsIds, nullable: true, type: typeof(ListGraphType<IdGraphType>));
            obj.Field(d => d.RegisteredAppointmentsIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>));
        }
        public AppointmentRangeType()
        {
            Name = nameof(AppointmentRange);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppointmentRangeType.InitMainFields(this);
            Field(d => d.Officer, nullable: true, type: typeof(OfficerType));
            Field(d => d.RegisteredAppointments, nullable: true, type: typeof(ListGraphType<AppointmentType>));
        }
    }
    public class AppointmentRangeUpdateType : BaseMutationType<AppointmentRange>
    {
        public AppointmentRangeUpdateType()
        {
            Name = "update" + nameof(AppointmentRange);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppointmentRangeType.InitMainFields(this);
            AppointmentRangeType.InitWriteOnlyFields(this);
        }
    }
    public class AppointmentRangeCreateType : BaseMutationType<AppointmentRange>
    {
        public AppointmentRangeCreateType()
        {
            Name = "create" + nameof(AppointmentRange);
        }
        protected override void InitFields()
        {
            base.InitFields();
            AppointmentRangeType.InitMainFields(this);
            AppointmentRangeType.InitWriteOnlyFields(this);
        }
    }
}
