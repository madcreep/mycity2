using BIT.GraphQL.Extensions.Filters;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types.Filter
{
    public class FilterOperationEnumType : EnumerationGraphType<Operation>
    {
    }

    public class FilterStatementConnectorEnumType : EnumerationGraphType<FilterStatementConnector>
    {
    }

    public class FilterItemType : InputObjectGraphType<FilterItem>
    {
        public FilterItemType()
        {
            Name = nameof(FilterItem);
            Field(d => d.Operation, nullable: true, typeof(FilterOperationEnumType))
                .Description("Тип операции");
            Field(d => d.PropertyPath, nullable: true)
                .Description("Путь к свойству");
            Field(d => d.Value, nullable: true, typeof(ListGraphType<StringGraphType>))
                .Description("Значения (константы)");
            Field(d => d.Connector, nullable: true, typeof(FilterStatementConnectorEnumType))
                .Description("Тип подключения к предыдущему элементу.");
            Field(d => d.IsNegative, nullable: true, typeof(BooleanGraphType))
                .Description("Инверсия операции");
            Field(d => d.SubProperty, nullable: true, typeof(ListGraphType<FilterItemType>))
                .Description("Уточнения");
            Field(d => d.ParenthesizedExpression, nullable: true, typeof(ListGraphType<FilterItemType>))
                .Description("Выражение в скобках");
        }
    }
}
