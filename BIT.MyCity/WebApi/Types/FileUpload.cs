﻿using BIT.MyCity.Model;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class FileDownloadRequestType : InputObjectGraphType<FileDownloadRequest>
    {
        public FileDownloadRequestType()
        {
            Name = nameof(FileDownloadRequest);
            Field(d => d.Id, type: typeof(IdGraphType)).Description("Id файла");
            Field(d => d.Len).Description("Запрашиваемая длина");
            Field(d => d.Pos).Description("Запрашиваемая позиция в файле");
        }
    }

    public class FileDownloadType : ObjectGraphType<FileDownload>
    {
        public FileDownloadType()
        {
            Name = nameof(FileDownload);
            Field(d => d.Id, type: typeof(IdGraphType)).Description("Id файла");
            Field(d => d.Pos).Description("Позиция в файле");
            Field(d => d.Data).Description("Данные в base64");
        }
    }
    public class FileUploadType : InputObjectGraphType<FileUpload>
    {
        public FileUploadType()
        {
            Name = nameof(FileUpload);
            Field(d => d.Name, nullable: true).Description("Имя файла. Передается в первом кусочке");
            Field(d => d.Id, nullable: true, type: typeof(IdGraphType)).Description("Id файла. Передается в последующих кусочках");
            Field(d => d.Len, nullable: true).Description("Общая длина файла. Передается в первом кусочке");
            Field(d => d.Pos).Description("Позиция передаваемых данных");
            Field(d => d.Data).Description("Данные в base64");
            Field(d => d.ParentId, nullable: true, type: typeof(IdGraphType)).Description("Id объекта, куда аттачить файл");
            Field(d => d.ParentType, nullable: true).Description("Тип объекта");
            Field(d => d.ContentType, nullable: true).Description("Content-Type");
            Field(d => d.Sequre, nullable: true).Description("Без доступа по чтению");
        }
    }

    public class FileUploadResultType : ObjectGraphType<FileUploadResult>
    {
        public FileUploadResultType()
        {
            Name = nameof(FileUploadResult);
            Field(d => d.Id, nullable: true, type: typeof(IdGraphType)).Description("Id файла");
            Field(d => d.Next, nullable: true).Description("Ближайшая к началу позиция файла где данные еще не загружены");
            Field(d => d.Percent, nullable: true).Description("Процент загруженности (0-100)");
        }
    }
}
