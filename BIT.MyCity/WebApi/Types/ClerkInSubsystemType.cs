using BIT.MyCity.Database;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class ClerkInSubsystemType : ObjectGraphType<ClerkInSubsystem>
    {
        public ClerkInSubsystemType()
        {
            Name = nameof(ClerkInSubsystemType);
            Field(d => d.SubsystemUid,false)
                .Description("Подсистема");
            Field(d => d.Users, nullable: false, type: typeof(ListGraphType<UserType>))
                .Description("Клерки");
        }
    }

    public class ClerkInSubsystemInputType : InputObjectGraphType<ClerkInSubsystemSet>
    {
        public ClerkInSubsystemInputType()
        {
            Name = nameof(ClerkInSubsystemInputType);
            Field(d => d.SubsystemUid, false, typeof(IdGraphType))
                .Description("Подсистема");
            Field(d => d.UserIds, nullable: false, type: typeof(ListGraphType<IdGraphType>))
                .Description("Клерки");
        }
    }
}
