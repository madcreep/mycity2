using BIT.MyCity.Database;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Users;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class NewsStateEnumType : EnumerationGraphType<NewsStateEnum>
    {
    }
    public class NewsType : BaseReadType<News>
    {
        internal static void InitMainFields(ComplexGraphType<News> obj)
        {
            obj.Field(d => d.Name, nullable: true).Description("Наименование");
            obj.Field(d => d.ShortName, nullable: true).Description("Короткое наименование");
            obj.Field(d => d.Text, nullable: true).Description("Содержимое новости");
            obj.Field(d => d.ShortText, nullable: true).Description("Краткий текст новости / начало текста новости");
            obj.Field(d => d.CommentsAllowed, nullable: true).Description("Пользователи могут оставлять комментарии");
        }
        internal static void InitWriteOnlyFields(ComplexGraphType<News> obj)
        {
            obj.Field(d => d.RubricId, nullable: true, type: typeof(IdGraphType)).Description("Id Рубрики");
            obj.Field(d => d.RegionId, nullable: true, type: typeof(IdGraphType)).Description("Id Региона");
            obj.Field(d => d.TopicId, nullable: true, type: typeof(IdGraphType)).Description("Id Топика");
            obj.Field(d => d.TerritoryId, nullable: true, type: typeof(IdGraphType)).Description("Id Территории");
            obj.Field(d => d.AttachedFilesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Присоединенных файлов");
            //obj.Field(d => d.AttachedFilesIdsChange, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id Присоединенных файлов (+/-)");
        }
        internal static void InitReadOnlyFields(ComplexGraphType<News> obj)
        {
            obj.Field(d => d.LikesCount).Description("Счетчик лайков");
            obj.Field(d => d.ViewsCount).Description("Счетчик просмотров");

            obj.Field(d => d.Rubric, nullable: true, type: typeof(RubricType)).Description("Рубрика");
            obj.Field(d => d.Region, nullable: true, type: typeof(RegionType)).Description("Регион");
            obj.Field(d => d.Topic, nullable: true, type: typeof(TopicType)).Description("Топик");
            obj.Field(d => d.Territory, nullable: true, type: typeof(TerritoryType)).Description("Территория");
            obj.Field(d => d.Author, nullable: true, type: typeof(UserType)).Description("Автор");
            obj.Field(d => d.AttachedFiles, type: typeof(ListGraphType<AttachedFileType>)).Description("Присоединенные файлы");
            obj.Field(d => d.UsersLiked, type: typeof(ListGraphType<UserType>)).Description("Кто поставил лайк или дизлайк");

            obj.Field(d => d.State, type: typeof(NewsStateEnumType)).Description("Состояние");
            obj.Field(d => d.LikeWIL, nullable: true, type: typeof(LikeWILType)).Description("Как я лайкнул");
            obj.Field(d => d.ViewWIV, nullable: true, type: typeof(ViewWIVType)).Description("Как я посмотрел");
            obj.Field(d => d.Messages, type: typeof(ListGraphType<MessageType>)).Description("Ответы");
        }
        public NewsType()
        {
            Name = nameof(News);
            Description = "Новость";
        }
        protected override void InitFields()
        {
            base.InitFields();
            NewsType.InitMainFields(this);
            NewsType.InitReadOnlyFields(this);
        }
    }
    public class NewsUpdateType : BaseMutationType<News>
    {
        public NewsUpdateType()
        {
            Name = "update" + nameof(News);
            Description = "Новость";
        }
        protected override void InitFields()
        {
            base.InitFields();
            NewsType.InitMainFields(this);
            NewsType.InitWriteOnlyFields(this);
        }
    }
    public class NewsCreateType : BaseMutationType<News>
    {
        public NewsCreateType()
        {
            Name = "create" + nameof(News);
            Description = "Обращение";
        }
        protected override void InitFields()
        {
            base.InitFields();
            NewsType.InitMainFields(this);
            NewsType.InitWriteOnlyFields(this);
        }
    }
    public class NewsApproveType : InputObjectGraphType<NewsApprove>
    {
        public NewsApproveType()
        {
            Name = "approve" + nameof(News);
            Description = "Публикация или снятие с публикации новости";
            Field(d => d.State, nullable: false, type: typeof(NewsStateEnumType)).Description("Состояние");
        }
    }
}
