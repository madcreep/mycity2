using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using GraphQL.Types;

namespace BIT.MyCity.WebApi.Types
{
    public class RightLevelEnumType : EnumerationGraphType<RightLevel>
    {
    }
    public class SettingsType : ObjectGraphType<Settings>
    {
        internal static void InitMainFields(ComplexGraphType<Settings> obj)
        {
            obj.Field(d => d.Key, type: typeof(IdGraphType)).Description("Ключ");
            obj.Field(d => d.Value, nullable: true).Description("Значение");
        }
        public SettingsType()
        {
            Name = nameof(Settings);
            Description = "Получение настроек";
            InitMainFields(this);
            Field(d => d.ReadingRightLevel, nullable: true, type: typeof(RightLevelEnumType)).Description("Требуемый уровень доступа по чтению");
            Field(d => d.CreatedAt, false, typeof(DateTimeGraphType)).Description("Дата создания объекта");
            Field(d => d.UpdatedAt, nullable: true, typeof(DateTimeGraphType)).Description("Дата изменения объекта");
            Field(d => d.SettingNodeType, nullable: false, typeof(SettingNodeTypeType)).Description("Тип узла настройки");
            Field(d => d.Weight).Description("Вес (признак сортировки)");
            Field(d => d.ValueType, true).Description("Тип значения настройки");
            Field(d => d.SettingType, nullable: true, typeof(SettingTypeType)).Description("Тип настройки");
            Field(d => d.Description, nullable: true).Description("Описание настройки");
            Field(d => d.Values, nullable: true, typeof(ListGraphType<SettingsValueItemType>)).Description("Возможные значения");
            Field(d => d.AttachedFiles, nullable: true, type: typeof(ListGraphType<AttachedFileType>)).Description("Присоединенные файлы");
        }
    }

    public class SettingsCreateType : InputObjectGraphType<Settings>
    {
        public SettingsCreateType()
        {
            Name = "create" + nameof(Settings);
            SettingsType.InitMainFields(this);
            Field(d => d.AttachedFilesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id присоединенных файлов");
        }
    }

    public class SettingsUpdateType : InputObjectGraphType<Settings>
    {
        public SettingsUpdateType()
        {
            Name = "update" + nameof(Settings);
            SettingsType.InitMainFields(this);
            Field(d => d.AttachedFilesIds, nullable: true, type: typeof(ListGraphType<IdGraphType>)).Description("Id присоединенных файлов");
        }
    }

    public class SettingTypeType : EnumerationGraphType<SettingType>
    { }

    public class SettingNodeTypeType : EnumerationGraphType<SettingNodeType>
    { }

    public class SettingsValueItemType : ObjectGraphType<SettingsValueItem>
    {
        public SettingsValueItemType()
        {
            Name = "create" + nameof(SettingsValueItem);
            Field(d => d.Value, true).Description("Значение параметра");
            Field(d => d.DisplayText).Description("Отображаемый текст");
        }
    }
}
