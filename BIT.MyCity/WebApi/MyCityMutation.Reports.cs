using System;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Reports;
using BIT.MyCity.WebApi.Types.Reports;
using GraphQL;
using GraphQL.Types;
using GraphQL.Utilities;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddReportsFields()
        {
            Field<ReportSavedSettingsType>(
                    "saveReportSettings",
                    arguments: new QueryArguments(
                        new QueryArgument<NonNullGraphType<GuidGraphType>> { Name = "reportId", Description = "Идентификатор отчета" },
                        new QueryArgument<NonNullGraphType<ReportSavedSettingsInputType>>
                    {
                        Name = $"{nameof(ReportSavedSettings)}Input".ToJsonFormat(),
                        Description = "Сохраняемые параметры"
                    }),
                    description: "Сохранить пользовательские параметры отчета",
                    resolve: context => SaveReportSavingSettingsAsync(context))
                .AuthorizeWith(PolicyName.ManageReports);

            Field<BooleanGraphType>(
                    "deleteReportSettings",
                    arguments: new QueryArguments(
                        new QueryArgument<NonNullGraphType<GuidGraphType>> { Name = "reportId", Description = "Идентификатор отчета" },
                        new QueryArgument<IdGraphType> { Name = "id", Description = "Идентификатор набора параметров" }),
                    description: "Удалить пользовательские параметры отчета",
                    resolve: context => DeleteReportSavingSettingsAsync(context))
                .AuthorizeWith(PolicyName.ManageReports);
        }

        private async Task<ReportSavedSettings> SaveReportSavingSettingsAsync(IResolveFieldContext<object> context)
        {
            var name = $"{nameof(ReportSavedSettings)}Input".ToJsonFormat();
            
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var manager = scope.ServiceProvider.GetRequiredService<ReportManager>();

                var id = context.GetArgument<Guid>("reportId");

                var settings = context.GetArgument<ReportSavedSettings>(name);

                return await manager.SaveUserReportSettingsAsync(id, settings);
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка запроса параметров отчета", ex);

                context.Errors.Add(new ExecutionError($"Ошибка сервера: {name}: {ex.Message}"));

                return null;
            }
        }

        private async Task<bool> DeleteReportSavingSettingsAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var manager = scope.ServiceProvider.GetRequiredService<ReportManager>();

                var reportIid = context.GetArgument<Guid>("reportId");

                var id = context.GetArgument<uint?>("id");
                
                return await manager.DeleteUserReportSettingsAsync(reportIid, id);
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка запроса параметров отчета", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return false;
            }
        }
    }
}
