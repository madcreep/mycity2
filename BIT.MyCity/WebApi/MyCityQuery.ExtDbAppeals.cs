using System;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddExtDbAppealFields()
        {
            //Field<PaginationType<AppealType, Appeal>>(
            //    name: $"externalSystem{nameof(Appeal)}",
            //    description: "Поиск обращений во внешней системе",
            //    arguments: new QueryArguments(
            //        new QueryArgument<StringGraphType> { Name = "appealNumber", Description = "Номер обращения" },
            //        new QueryArgument<DateGraphType> { Name = "appealDate", Description = "Дата обращения" }),
            //    resolve: context => GetExtDbAppealsPgAsync(context));
        }

        //private async Task<Pagination<Appeal>> GetExtDbAppealsPgAsync(IResolveFieldContext<object> context)
        //{
        //    try
        //    {
        //        var number = context.GetArgument<string>("appealNumber");
        //        var date = context.GetArgument<DateTime>("appealDate");

        //        using var scope = _scopeFactory.CreateScope();
        //        var manager = scope.ServiceProvider.GetRequiredService<AppealNotDbManager>();
        //        var result = await manager.GetExternalSystemAppeal(number, date);
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        ReportErrorMessage(context, ex);
        //        return null;
        //    }
        //}
    }
}
