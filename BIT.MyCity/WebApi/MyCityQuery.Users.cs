using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using BIT.MyCity.ApiInterface.Models;
using GraphQL;
using GraphQL.Types;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Authorization;
using BIT.MyCity.WebApi.Types.Pagination;
using BIT.MyCity.WebApi.Types.Users;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityQuery
    {
        private void AddUsersFields()
        {
            Field<PaginationType<UserType, User>>(
                    name: nameof(User).ToJsonFormat(),
                    description: "Получение списка пользователей",
                    arguments: new QueryArguments(
                        new QueryArgument<SelectionRangeType> { Name = "range", Description = "Сортировка и пагинация" },
                        new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                    resolve: context => GetUsersAsync(context))
                .AuthorizeWith(PolicyName.ManageUsersOrModerate);

            Field<UserType>(
                    name: "current" + nameof(User),
                    description: "Получение текущего пользователя",
                    resolve: context => GetUserAsync(context))
                .AuthorizeWith(PolicyName.Authorized);

            Field<PaginationType<UserType, User>>(
                    name: "availableModerators",
                    description: "Получение списка модераторов по подсистемам",
                    arguments: new QueryArguments(
                        new QueryArgument<SelectionRangeType> { Name = "range", Description = "Сортировка и пагинация" },
                        new QueryArgument<ListGraphType<StringGraphType>> { Name = "subsystemUid", Description = "UID подсистем" }),
                    resolve: context => GetModeratorsAsync(context))
                .AuthorizeWith(PolicyName.ManageUsersOrModerate);
            Field<PaginationType<UserType, User>>(
                    name: "availableExecutors",
                    description: "Получение списка исполнителей по подсистемам",
                    arguments: new QueryArguments(
                        new QueryArgument<SelectionRangeType> { Name = "range", Description = "Сортировка и пагинация" },
                        new QueryArgument<ListGraphType<StringGraphType>> { Name = "subsystemUid", Description = "UID подсистем" }),
                    resolve: context => GetExecutorsAsync(context))
                .AuthorizeWith(PolicyName.ManageUsersOrModerate);

            Field<OperationResultType>(
                    name: "confirmPhone",
                    description: "Отправлка СМС подтверждения телефонного номера",
                    arguments: new QueryArguments(
                        new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "phoneId", Description = "Идентификатор номера телефона" }),
                    resolve: context => ConfirmPhoneAsync(context))
                .AuthorizeWith(PolicyName.LoginSystemForm);
        }

        private async Task<Pagination<User>> GetUsersAsync(IResolveFieldContext context)
        {
            try
            {
                var range = context.GetArgument<SelectionRange>("range");

                var filter = context.GetArgument<List<GraphQlQueryFilter>>("filter");

                var requestedFields = context.SubFieldsKeys();

                using (var scope = _scopeFactory.CreateScope())
                {
                    var userManager = (UserManager) scope.ServiceProvider.GetService(typeof(UserManager));

                    var result = await userManager.GetUsersAsync(range, filter, requestedFields);

                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса пользователей", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<User> GetUserAsync(IResolveFieldContext context)
        {
            try
            {
                var requestedFields = context.SubFieldsKeys();
                using (var scope = _scopeFactory.CreateScope())
                {
                    var userManager = (UserManager) scope.ServiceProvider.GetService(typeof(UserManager));
                    var result = await userManager.GetUserAsync(null as long?, requestedFields);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса пользователей с пагинацией", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }

        private async Task<Pagination<User>> GetModeratorsAsync(IResolveFieldContext context)
        {
            try
            {
                var subsystemUid = context.GetArgument<string[]>("subsystemUid");

                var range = context.GetArgument<SelectionRange>("range");

                var requestedFields = context.SubFieldsKeys();

                using var scope = _scopeFactory.CreateScope();

                var userManager = (UserManager)scope.ServiceProvider.GetService(typeof(UserManager));

                var result = await userManager.GetModeratorsAsync(subsystemUid, range, requestedFields);

                return result;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса модераторов", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }
        private async Task<Pagination<User>> GetExecutorsAsync(IResolveFieldContext context)
        {
            try
            {
                var subsystemUid = context.GetArgument<string[]>("subsystemUid");
                var range = context.GetArgument<SelectionRange>("range");
                var requestedFields = context.SubFieldsKeys();
                using var scope = _scopeFactory.CreateScope();
                var userManager = (UserManager)scope.ServiceProvider.GetService(typeof(UserManager));
                var result = await userManager.GetExecutorsAsync(subsystemUid, range, requestedFields);
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка обработки запроса исполнителей", ex);
                context.Errors.Add(new ExecutionError("Ошибка сервера"));
                return null;
            }
        }

        private async Task<OperationResult> ConfirmPhoneAsync(IResolveFieldContext context)
        {
            try
            {
                var phoneId = context.GetArgument<long>("phoneId");

                using var scope = _scopeFactory.CreateScope();

                var phoneManager = scope.ServiceProvider.GetRequiredService<PhoneManager>();

                var result = await phoneManager.SendConfirmPhoneSms(phoneId);

                if (!result.Success)
                {
                    context.Errors.AddRange(result.ErrorMessages
                        .Select(el=> new ExecutionError(el)));
                }

                return result;
            }
            catch (Exception ex)
            {
                Log.Error("Ошибка запроса подтверждения телефонного номера", ex);

                context.Errors.Add(new ExecutionError("Ошибка сервера"));

                return null;
            }
        }
    }
}
