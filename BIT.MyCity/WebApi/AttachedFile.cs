using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddAttachedFileFields()
        {
            Field<FileUploadResultType>(
                name: "fileUpload",
                description: "Загрузка файла на сервер",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<FileUploadType>> { Name = "file", Description = "Передаваемый файл или его часть" }
                    ),
                resolve: context => FileUploadAsync(context));
            Field<AttachedFileType>(
                name: "create" + nameof(AttachedFile),
                description: "Создание файла (ссылки)",
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<AttachedFileCreateType>>
                { Name = nameof(AttachedFile), Description = "Присоединенный файл" }),
                resolve: context => CreateAttachedFileAsync(context));
            Field<AttachedFileType>(
                name: "update" + nameof(AttachedFile),
                description: "Изменение ссылки на файл",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "Id присоединенного файла" },
                    new QueryArgument<NonNullGraphType<AttachedFileUpdateType>> { Name = nameof(AttachedFile) }),
                resolve: context => UpdateAttachedFileAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<AttachedFileType>(
                name: "approve" + nameof(AttachedFile),
                description: "Публикация файла",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "id", Description = "Id присоединенного файла" },
                    new QueryArgument<NonNullGraphType<AttachedFileApproveType>> { Name = nameof(AttachedFileApprove) }),
                resolve: context => ApproveAttachedFileAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
        }
        private async Task<FileUploadResult> FileUploadAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var file = context.GetArgument<FileUpload>("file");
                var m = (AttachedFileManager)scope.ServiceProvider.GetService(typeof(AttachedFileManager));
                return await m.FileUploadAsync(file);
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
        private async Task<AttachedFile> CreateAttachedFileAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var obj = context.GetArgument<AttachedFile>(nameof(AttachedFile));
                var m = (AttachedFileManager)scope.ServiceProvider.GetService(typeof(AttachedFileManager));
                return await m.CreateDbAsync(obj);
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<AttachedFile> UpdateAttachedFileAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var fields = context.Keys(nameof(AttachedFile).ToJsonFormat());
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<AttachedFile>(nameof(AttachedFile));
                    var m = (AttachedFileManager)scope.ServiceProvider.GetService(typeof(AttachedFileManager));
                    return await m.UpdateDbAsync(id, obj, fields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
        private async Task<AttachedFile> ApproveAttachedFileAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var fields = context.Keys(nameof(AttachedFileApprove).ToJsonFormat());
                    var id = context.GetArgument<long>("id");
                    var obj = context.GetArgument<AttachedFileApprove>(nameof(AttachedFileApprove));
                    var m = (AttachedFileManager)scope.ServiceProvider.GetService(typeof(AttachedFileManager));
                    return await m.ApproveAsync(id, obj, fields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
    public sealed partial class MyCityQuery
    {
        private void AddAttachedFileFields()
        {
            Field<FileDownloadType>(
                name: "fileDownload",
                description: "Скачивание файла с сервера",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<FileDownloadRequestType>> { Name = "file", Description = "Запрос на скачивание" }),
                resolve: context => this.FileDownloadAsync(context));
            Field<PaginationType<AttachedFileType, AttachedFile>>(
                name: nameof(AttachedFile),
                description: "Получение списка присоединенных файлов",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }
                    ),
                resolve: context => this.GetAttachedFilesPgAsync(context));
        }
        private async Task<Pagination<AttachedFile>> GetAttachedFilesPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var m = (AttachedFileManager)scope.ServiceProvider.GetService(typeof(AttachedFileManager));
                    return await m.GetAttachedFilesAsync(QueryOptionsParse(context, scope));
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<FileDownload> FileDownloadAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var freq = context.GetArgument<FileDownloadRequest>("file");
                    var m = (AttachedFileManager)scope.ServiceProvider.GetService(typeof(AttachedFileManager));
                    return await m.FileDownloadAsync(freq);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }
        }
    }
}
