namespace BIT.MyCity.WebApi.Types
{
    public class TestType
    {
        public uint Id { get; set; }
        public string Name { get; set; }
        public string[] Arr { get; set; }
    }

}