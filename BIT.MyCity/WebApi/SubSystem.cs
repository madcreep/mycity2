using BIT.GraphQL.Authorization;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.WebApi.Types;
using BIT.MyCity.WebApi.Types.Pagination;
using GraphQL;
using GraphQL.Types;
using System;
using System.Threading.Tasks;

namespace BIT.MyCity.WebApi
{
    public sealed partial class MyCityMutation
    {
        private void AddSubSystemFields()
        {
            //пока обработка клаймов захардкожена под каждую подсистему нет смысла в этом мутаторе
            Field<SubsystemType>(
                name: "create" + nameof(Subsystem),
                arguments: new QueryArguments(new QueryArgument<NonNullGraphType<SubsystemCreateType>>
                { Name = nameof(Subsystem) }),
                resolve: context => CreateSubsystemAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
            Field<SubsystemType>(
                name: "update" + nameof(Subsystem),
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "uid" },
                    new QueryArgument<NonNullGraphType<SubsystemUpdateType>> { Name = nameof(Subsystem) }),
                resolve: context => UpdateSubsystemAsync(context))
                .AuthorizeWith(PolicyName.Authorized);
        }
        private async Task<Subsystem> CreateSubsystemAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedKeys = context.Keys(nameof(Subsystem).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var obj = context.GetArgument<Subsystem>(nameof(Subsystem));
                    var m = (SubsystemManager)scope.ServiceProvider.GetService(typeof(SubsystemManager));
                    return await m.CreateDbAsync(obj, updatedKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }

        private async Task<Subsystem> UpdateSubsystemAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var updatedKeys = context.Keys(nameof(Subsystem).ToJsonFormat());
                    var requestedFields = context.SubFieldsKeys();
                    var id = context.GetArgument<string>("uid");
                    var obj = context.GetArgument<Subsystem>(nameof(Subsystem));
                    var m = (SubsystemManager)scope.ServiceProvider.GetService(typeof(SubsystemManager));
                    return await m.UpdateDbAsync(id, obj, updatedKeys, requestedFields);
                }
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
    public sealed partial class MyCityQuery
    {
        private void AddSubSystemFields()
        {
            Field<PaginationType<SubsystemType, Subsystem>>(
                name: nameof(Subsystem),
                description: "Получение списка и параметров подсистем",
                arguments: new QueryArguments(
                    new QueryArgument<QueryOptionsType> { Name = "options", Description = "Параметры" },
                    new QueryArgument<SelectionRangeType> { Name = "range", Description = "Объект для сортировки и пагинации" },
                    new QueryArgument<ListGraphType<FilterType>> { Name = "filter", Description = "Фильтр" }),
                resolve: GetSubsystemsPgAsync);
        }
        private async Task<Pagination<Subsystem>> GetSubsystemsPgAsync(IResolveFieldContext<object> context)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var m = (SubsystemManager)scope.ServiceProvider.GetService(typeof(SubsystemManager));
                return await m.GetSubsystemsAsync(QueryOptionsParse(context, scope));
            }
            catch (Exception ex)
            {
                ReportErrorMessage(context, ex);
                return null;
            }

        }
    }
}
