using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BIT.MyCity
{
    public static class ErrorHandler
    {

        public static IActionResult HandleErrors(this IdentityResult result, ErrorResult receiver, string message = null) {
            var errors = result.Errors.ToDictionary(error => error.Code, error => error.Description);
            return receiver(new { Message = message, Errors = errors});

        }

    }

    public delegate IActionResult ErrorResult(object data);  
}
