using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using BIT.MyCity.Database;
using BIT.MyCity.Model;


namespace BIT.MyCity
{
    public static class AuthService
    {
        //public static string GetSecurityToken(UserForm user)
        //{
        //    var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("doYouWantToKnowTheSecret@98762"));
        //    var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
        //    var claims = GetClaims(user);

        //    var tokenOptions = new JwtSecurityToken(
        //        issuer: GlobalProperties.AppUrl,
        //        audience: GlobalProperties.AppUrl,
        //        //                issuer: "http://www.expert-doc.ru",
        //        //                audience: "http://www.expert-doc.ru",
        //        claims: claims,
        //        expires: DateTime.Now.AddMinutes(10),
        //        signingCredentials: signinCredentials);

        //    return new JwtSecurityTokenHandler().WriteToken(tokenOptions);
        //}

//        public static List<Claim> GetClaims(UserForm user)
//        {
//            var specificRight = false;
//            foreach (var roleName in user.UserRole)
//            {
//                var role = user.AllRoles.FirstOrDefault(r => r.DefaultRoleName == roleName)?.SpecificRight;
//                specificRight |= role.HasValue && role.Value;
//                if (specificRight)
//                    break;
//            }
//            var claims = new List<Claim>()
//            {
//                new Claim(JwtRegisteredClaimNames.Email, user.Email),
////                new Claim(JwtRegisteredClaimNames.Jti, (user.Id == Guid.Empty ? new Guid() : user.Id).ToString()),
////TODO
//                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString()),
//                new Claim(JwtRegisteredClaimNames.Birthdate, DateTime.Now.AddMonths(-10).ToString("yyyy-MM-dd")),
//                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),

//                new Claim("SpecificRight", specificRight.ToString(),"boolean", "http://www.biz-it.ru")
//            };

//            //            foreach (var role in user.UserRole)
//            //            {
//            //                claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));
//            //            }
//            return claims;
//        }

        public static async Task<User> GetCurrentUser(HttpContext httpContext, UserManager<User> userManager)
        {
            var currentUser = httpContext.User;
            var principal = currentUser.Identity as ClaimsIdentity;
            if (principal == null)
            {
                throw new UserException("Вход не выполнен");
            }

            var claims = principal.Claims.ToList();
            var nameClaim = claims.FirstOrDefault(c =>
                c.Type == principal.NameClaimType);
            if (nameClaim == null)
            {
                nameClaim = claims.FirstOrDefault(c => c.Type == JwtRegisteredClaimNames.Email);
            }

            if (nameClaim == null)
            {
                throw new UserException("Вход не выполнен");
            }

            var email = nameClaim.Value;
            if (string.IsNullOrEmpty(email))
            {
                throw new UserException("Вход не выполнен");
            }

            var user = await userManager.FindByEmailAsync(email);
            if (user == null)
                throw new UserException("Пользователь не найден");
            return user;
        }


        public static async Task<UserForm> UserToUserForm(User user, UserManager<User> userManager, RoleManager<Role> roleManager)
        {
            return new UserForm
            {
                Id = user.Id,
                Email = user.Email,
                FullName = user.FullName,
                AllRoles = roleManager.Roles.Select(r => RoleToRoleForm(r)).ToList(),
                UserRoles = await userManager.GetRolesAsync(user)
            };
        }

        public static RoleForm RoleToRoleForm(Role role)
        {
            return new RoleForm
            {
                Id = role.Id,
                RoleName = role.Name,
                Description = role.Description
            };
        }

        //public static ClaimsPrincipal ValidateToken(string jwtToken)
        //{
        //    IdentityModelEventSource.ShowPII = true;

        //    TokenValidationParameters validationParameters = new TokenValidationParameters();

        //    validationParameters.ValidateLifetime = true;

        //    validationParameters.ValidAudience = GlobalProperties.AppUrl;
        //    validationParameters.ValidIssuer = GlobalProperties.AppUrl;
        //    validationParameters.IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("doYouWantToKnowTheSecret@98762"));

        //    ClaimsPrincipal principal = new JwtSecurityTokenHandler().ValidateToken(jwtToken, validationParameters, out _);


        //    return principal;
        //}

    }

}
