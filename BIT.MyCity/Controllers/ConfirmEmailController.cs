using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using Microsoft.AspNetCore.Mvc;

namespace BIT.MyCity.Controllers
{
    //[Route("ConfirmEmail")]
    public class ConfirmEmailController : Controller
    {
        [HttpGet]
        //[Route("ConfirmEmail/Index")]
        public async Task<IActionResult> Index(
            [FromServices] AuthManager authManager,
            //[FromServices] UserManager userManager,
            [FromQuery] ConfirmEmailModel model)
        {
            var confirmResult = await authManager.ConfirmEmail(model);

            var outModel = new ConfirmEmailResultApi
            {
                Succsess = confirmResult.Success,
                ErrorMessage = string.Join("; ", confirmResult.ErrorMessages)
            };
            
            //if (confirmResult.Succsess)
            //{
            //    outModel.User = await userManager.GetUserAsync(model.UserId);
            //}

            return View(outModel);
        }
    }

    public class ConfirmEmailResultApi : ConfirmEmailResult
    {
        public User User { get; set; }
    }
}
