using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using BIT.MyCity.Database;
using BIT.MyCity.Model;

namespace BIT.MyCity.Controllers
{
  [ApiController, Route("api/service"), Authorize(Roles = "service")]
  public class ServiceController : ControllerBase
  {
    private readonly DocumentsContext _context;

    public ServiceController([FromServices] DocumentsContext context)
    {
      _context = context;
    }

    [HttpPost, Route("sendDocument")]
    public async Task<IActionResult> PostDocument(DocumentModel model)
    {
      DocumentModel result = null;
      //using (var context = new DocumentsContext())
      //{
        var document = await _context.Documents.FirstOrDefaultAsync(d => d.Id == model.Id);
        if (document == null)
        {
          document = model.ToDocument();
          _context.Documents.Add(document);
        }
        else
        {
          document.AnticorruptionExpertise = model.AnticorruptionExpertise;
          document.Content = model.Content;
          document.Date = model.Date;
          document.ExpertiseStart = model.ExpertiseStart;
          document.ExpertiseEnd = model.ExpertiseEnd;
          document.Number = model.Number;
          document.ProfileCommittee = model.ProfileCommittee;
          document.PublishDate = model.PublishDate;
          document.UploadDate = model.UploadDate;
          document.Сorrespondent = model.Сorrespondent;
        }

        if (document.Rubric.Due != model.Rubric?.Due)
        {
          if (model.Rubric == null)
          {
            document.Rubric = null;
          }
          else
          {
            var rubric = await _context.Rubrics.FirstOrDefaultAsync(r => r.Due == model.Rubric.Due);
            if (rubric == null)
            {
              rubric = model.Rubric.ToRubric();
              _context.Rubrics.Add(rubric);
            }

            document.Rubric = rubric;
          }
        }
        _context.SaveChanges();
        var dbDocument = await _context.Documents.FirstOrDefaultAsync(d => d.Id == model.Id);
        result = dbDocument.ToDocumentModel();

      //}
      return Ok(result);
    }

    [HttpPost, Route("sendFile/{id}")]
    public async Task<IActionResult> SendFiles([FromServices] IConfiguration config, [FromBody]DocumentFileModel[] model, int id)
    {
      if (model == null) return BadRequest(new { Message = "Required request object is missing" });
      if (id == 0) return BadRequest(new { Message = "Document UUID is missing" });
      
        var document = await _context.Documents.FirstOrDefaultAsync(d => d.Id == id);
        if (document == null)
        {
          return NotFound(new { Message = $"Document with id {id} not found" });
        }
        var localPath = Path.Combine(config["StoragePath"], document.Uuid.ToString());
        if (!Directory.Exists(localPath))
        {
          Directory.CreateDirectory(localPath);
        }
        if (Request.Form.Files != null && Request.Form.Files.Count > 0)
        {
          foreach (var formFile in Request.Form.Files)
          {
            var fileModel = model.FirstOrDefault(m => m.FileName == formFile.FileName);
            if (fileModel == null) continue;
            var file = fileModel.ToDocumentFile(null);
            var dbFile = document.Files.FirstOrDefault(f => f.Id == file.Id);
            var fileId = Guid.Empty;
            if (dbFile == null)
            {
              file.Document = document;
              _context.DocumentFiles.Add(file);
              document.Files.Add(file);
              fileId = new Guid();
              file.Uuid = fileId;
            }
            else
            {
              fileId = dbFile.Uuid;
            }

            var filePath = Path.Combine(localPath, fileId.ToString());
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
              await formFile.CopyToAsync(stream);
            }

          }
          await _context.SaveChangesAsync();
        }
      return Ok();
    }

    [HttpPost, Route("sendStage/{id}")]
    public async Task<IActionResult> SendStage([FromBody]ReviewStageModel model, int id)
    {
      if (id == 0)
      {
        return BadRequest(new { Message = "Missing required id parameter" });
      }

      ReviewStageModel result;
        var document = await _context.Documents.FirstOrDefaultAsync(d => d.Id == id);
        if (document == null)
        {
          return NotFound(new { Message = $"Document with id {id} not found" });
        }

        var dbStage = await _context.ReviewStages.FirstOrDefaultAsync(s =>
            s.Document.Id == id && s.StageType == model.StageType);
        if (dbStage == null)
        {
          dbStage = model.ToReviewStage(null);
          dbStage.Document = document;
          document.ReviewStages.Add(dbStage);
        }
        else
        {
          dbStage.Date = model.Date;
          dbStage.Number = model.Number;
          dbStage.ProfileCommittee = model.ProfileCommittee;
          dbStage.SendDate = model.SendDate;
          dbStage.StageType = model.StageType;
        }
        _context.SaveChanges();
        result = dbStage.ToStageModel();
      return Ok(result);
    }
  }
}
