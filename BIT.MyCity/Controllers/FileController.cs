using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using GraphQL.Utilities;
using log4net;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Extensions;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BIT.MyCity.Controllers
{
    [Route("api/[controller]")]
    public class FileController : Controller
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly DocumentsContext _ctx;
        private readonly UserManager _um;
        private readonly AttachedFileManager _afm;
        private readonly ILog _log = LogManager.GetLogger(typeof(FileController));
        private readonly SettingsManager _sm;
        public FileController(IServiceProvider serviceProvider, IWebHostEnvironment appEnvironment)
        {
            _serviceProvider = serviceProvider;
            _appEnvironment = appEnvironment;
            _ctx = (DocumentsContext)_serviceProvider.GetService(typeof(DocumentsContext));
            _um = (UserManager)_serviceProvider.GetService(typeof(UserManager));
            _sm = _serviceProvider.GetRequiredService<SettingsManager>();
            _afm = (AttachedFileManager)_serviceProvider.GetService(typeof(AttachedFileManager));
        }
        // GET: api/<controller>
        [HttpGet, Route("{id:long}")]
        public async Task<IActionResult> Get(long id)
        {
            _log.Debug($"Запрос файла (id:{id})");

            var principal = HttpContext.User;
            var principalId = _um.ClaimsPrincipalId(principal);
            var file = _ctx.AttachedFiles
                .Where(el => !el.Deleted && !el.Sequre)
                .FirstOrDefault(el => el.Id == id);
            if (file == null)
            {
                _log.Warn($"Не найдена запись о файле (id:{id})");
                return NotFound("file not found");
            }

            var allowed = await file.IsReadAllowed(principalId, principal, _ctx, _sm);

            if (!allowed)
            {
                _log.Warn($"Чтение файла запрещено (id:{id})");
                return Unauthorized();
            }

            var path = _serviceProvider.GetRequiredService<IConfiguration>()["StoragePath"];

            path = Path.Combine(_appEnvironment.ContentRootPath, path);

            var physicalPath = Path.Combine(path, file.StoragePath, file.StorageName);

            if (!System.IO.File.Exists(physicalPath))
            {
                _log.Warn($"Не найден физический файл (id:{id}) <{physicalPath}>");
                return NotFound("File not found");
            }

            _log.Debug($"Отправка файла (id:{id}) <{physicalPath}> <{file.Name}>");

            //return File(System.IO.File.ReadAllBytes(physicalPath), "application/octet-stream");
            var physicalFile = PhysicalFile(physicalPath, "application/octet-stream", file.Name, true);
            
            return physicalFile;

        }

        // POST api/<controller>
        [HttpPost]
        public async Task<IActionResult> Post([FromForm] IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                try
                {
                    var nfo = new FileUploadForm();
                    foreach (var key in HttpContext.Request.Form.Keys)
                    {
                        var v = HttpContext.Request.Form[key][0];
                        var p = nfo.GetType()
                            .GetProperty(key, System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
                        if (p != null)
                        {
                            if (p.PropertyType == typeof(long))
                            {
                                p.SetValue(nfo, long.Parse(v));
                            }
                            else if (p.PropertyType == typeof(string))
                            {
                                p.SetValue(nfo, v);
                            }
                            else if (p.PropertyType == typeof(bool))
                            {
                                p.SetValue(nfo, bool.Parse(v));
                            }
                        }
                    }

                    var af = await _afm.FileUploadAsync(uploadedFile, nfo);

                    if (af != null)
                    {
                        return Ok($"id={af.Id}");
                    }

                    _log.Error($"FilePOST rejected");
                    return Unauthorized();
                }
                catch (Exception ex)
                {
                    _log.Error($"FilePOST: {ex.Message}");
                    throw;
                }
            }
            return Ok("No file");
        }
        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
    public class FileModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
