using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BIT.MyCity.Controllers
{
    //[Authorize(Policy = "Authorized")]
    public class ResetPasswordController : Controller
    {
        [HttpPost]
        //[Route("ResetPassword/Index")]
        public async Task<IActionResult> Index(
            [FromServices] AuthManager authManager,
            //[FromServices] UserManager userManager,
            [FromQuery] ResetPasswordModel model)
        {
            var confirmResult = await authManager.ResetPassword(model);

            var outModel = new ConfirmEmailResultApi
            {
                Succsess = confirmResult.Success,
                ErrorMessage = string.Join("; ", confirmResult.ErrorMessages)
            };
            
            return View(outModel);
        }
    }

}
