using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BIT.MyCity.Database
{
    public class Appointment : BaseWPEntity, IHasAttachedFiles
    {
        public int? Number { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public string RejectionReason { get; set; }
        public string AuthorName { get; set; }
        public string AuthorNameUpperCase { get; set; }
        public string Region { get; set; }
        public string Municipality { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool? Admin { get; set; }

        public List<AttachedFilesToAppointments> AttachedFilesToAppointments { get; set; }

        [NotMapped]
        public List<AttachedFile> AttachedFiles { get; set; }
        [NotMapped]
        public List<long> AttachedFilesIds { get; set; }
        //[NotMapped]
        //public List<long> AttachedFilesIdsChange { get; set; }

        public void AddFile(AttachedFile file)
        {
            throw new NotImplementedException();
        }

        public Rubric Rubric { get; set; }
        public long? RubricId { get; set; }

        public Officer Officer { get; set; }
        public long? OfficerId { get; set; }

        public AppointmentRange AppointmentRange { get; set; }
        public long? AppointmentRangeId { get; set; }

        public Topic Topic { get; set; }
        public long? TopicId { get; set; }

        public List<AppointmentNotification> Answers { get; set; }
        [NotMapped]
        public List<long> AnswersIds { get; set; }
        [NotMapped]
        public List<long> AnswersIdsChange { get; set; }

        public Appointment()
        {
            Type = nameof(Appointment);
        }
    }
}
