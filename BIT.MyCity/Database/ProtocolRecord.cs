﻿using System;

namespace BIT.MyCity.Database
{
    public class ProtocolRecord : BaseEntity
    {
        /*keep*/
        public User User { get; set; }
        public Appeal Appeal { get; set; }
        public Appointment Appointment { get; set; }
        public Officer Officer { get; set; }
        public AppointmentRange AppointmentRange { get; set; }
        public Rubric Rubric { get; set; }
        public Region Region { get; set; }
        public Territory Territory { get; set; }
        public Topic Topic { get; set; }
        public string Action { get; set; }
        public string Data { get; set; }

        /*add*/
        public AppealExecutor AppealExecutor { get; set; }
        public AppointmentNotification AppointmentNotification { get; set; }
        public AttachedFile AttachedFile { get; set; }
        public Message Message { get; set; }
        public Subsystem Subsystem { get; set; }
        public Vote Vote { get; set; }
        public VoteItem VoteItem { get; set; }


        /*remove*/
        //public long? UserId { get; set; }
        //public long? AppealId { get; set; }
        //public long? AppointmentId { get; set; }
        //public long? OfficerId { get; set; }
        //public long? AppointmentRangeId { get; set; }
        //public long? RubricId { get; set; }
        //public long? RegionId { get; set; }
        //public long? TerritoryId { get; set; }
        //public long? TopicId { get; set; }

        //public string ExtId { get; set; }
        //public DateTime? ExtDate { get; set; }
        //public string ExtNumber { get; set; }
        //public int? Status { get; set; }
        //public int? SiteStatus { get; set; }
        //public bool? Public { get; set; }
        //public string Source { get; set; }
        //public bool? Rejected { get; set; }
        //public string RejectionReason { get; set; }

        public ProtocolRecord()
        {
            Type = nameof(ProtocolRecord);
        }
    }
}
