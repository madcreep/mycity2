using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using BIT.MyCity.Model;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BIT.MyCity.Database
{
    public class Email :  IUpdatedEntity
    {
        private readonly ILazyLoader _lazyLoader;

        private readonly object _syncRoot = new object();

        public long Id { get; set; }
        
        public string Message { get; set; }

        private MailMessage _mailMessage;

        [NotMapped]
        public MailMessage MailMessage
        {
            get
            {
                if (_mailMessage != null)
                {
                    return _mailMessage;
                }

                lock (_syncRoot)
                {
                    if (_mailMessage == null)
                    {
                        _mailMessage = JsonConvert.DeserializeObject<MailMessage>(Message);
                    }
                }

                return _mailMessage;
            }
            set => Message = JsonConvert.SerializeObject(value,
                Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
        }
        
        [NotMapped]
        public dynamic DynamicMessage
        {
            get
            {
                var converter = new ExpandoObjectConverter();
                return JsonConvert.DeserializeObject<ExpandoObject>(Message, converter);
            }
        }

        public string Template { get; set; }

        private List<AttachedFilesToEmails> _attachedFilesToEmails = new List<AttachedFilesToEmails>();

        public List<AttachedFilesToEmails> AttachedFilesToEmails
        {
            get => _attachedFilesToEmails ?? _lazyLoader?.Load(this, ref _attachedFilesToEmails);
            set => _attachedFilesToEmails = value;
        }

        private List<AttachedFile> _attachedFiles;

        [NotMapped]
        public List<AttachedFile> AttachedFiles
        {
            get => _attachedFiles
                   ?? AttachedFilesToEmails.Select(el => el.AttachedFile).ToList();

            set => _attachedFiles = value;
        }

        public DateTime? SendingTimestamp { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public Email(ILazyLoader lazyLoader)
            : this()
        {
            _lazyLoader = lazyLoader;
        }

        public Email()
        {}
    }
}
