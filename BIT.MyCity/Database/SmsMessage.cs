using System;
using System.ComponentModel.DataAnnotations;
using BIT.MyCity.Model;
using BIT.MyCity.Sms.Models;

namespace BIT.MyCity.Database
{
    public class SmsMessage : IUpdatedEntity
    {
        [Key]
        public long Id { get; set; }

        public long UserId { get; set; }

        public virtual User User { get; set; }

        public SmsType SmsType { get; set; } = SmsType.Custom;

        public string Sender { get; set; }

        public string PhoneNumber { get; set; }

        public string Message { get; set; }

        /// <summary>
        /// Требуемое время доставки
        /// </summary>
        public DateTime? ScheduleTime { get; set; }

        public SmsMessageStatus Status { get; set; } = SmsMessageStatus.Created;

        public string ErrorMessage { get; set; }

        public string Service { get; set; }

        public string ServiceId { get; set; }

        /// <summary>
        /// Время жизни (позже указанной даты сообщения не отправляются)
        /// </summary>
        public DateTime? LifeTime { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }
    }
}
