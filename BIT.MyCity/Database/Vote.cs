using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace BIT.MyCity.Database
{
    public class Vote : BaseEntity, IHasAttachedFiles, IHasMessages
    {
        public enum ModeEnum
        {
            ONE_SELECT,
            MANY_SELECT,
        }

        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Text { get; set; }
        public string TextResult { get; set; }

        public ModeEnum Mode { get; set; } = ModeEnum.ONE_SELECT;
        public int ModeLimitValue { get; set; } = 1;
        public bool OfferEnabled { get; set; }
        public bool? CommentsAllowed { get; set; }

        public int OffersCount { get; set; }
        [NotMapped]
        public List<VoteOffer> Offers => UserVote?.Where(el => !string.IsNullOrWhiteSpace(el.Comment)).Select(el => new VoteOffer { Text = el.Comment, User = el.User }).ToList() ?? new List<VoteOffer>();

        public List<VoteItem> VoteItems { get; set; }
        [NotMapped]
        public List<long> VoteItemsIds { get; set; }
        [NotMapped]
        public List<long> VoteItemsIdsChange { get; set; }

        public List<UserVote> UserVote { get; set; } = new List<UserVote>();
        [NotMapped]
        public List<User> Users => UserVote?.Select(el => el.User).ToList() ?? new List<User>();
        [NotMapped]
        public int UsersCount { get { return UserVote != null ? UserVote.Select(el => el.UserId).Distinct().Count() : 0; } }

        public List<VoteItem> ParentVoteItems { get; set; }
        [NotMapped]
        public List<long> ParentVoteItemsIds { get; set; }
        [NotMapped]
        public List<long> ParentVoteItemsIdsChange { get; set; }

        public List<AttachedFilesToVotes> AttachedFilesToVotes { get; set; }

        [NotMapped]
        public List<AttachedFile> AttachedFiles { get; set; }
        [NotMapped]
        public List<long> AttachedFilesIds { get; set; }
        //[NotMapped]
        //public List<long> AttachedFilesIdsChange { get; set; }

        public void AddFile(AttachedFile file)
        {
            throw new System.NotImplementedException();
        }

        public long? VoteRootId { get; set; }
        public VoteRoot VoteRoot { get; set; }
        public List<Message> Messages { get; set; }

        [NotMapped]
        public VoteWIV VoteWIV {
            get {
                using var serviceScope = ServiceActivator.GetScope();
                var m = (VoteManager)serviceScope.ServiceProvider.GetService(typeof(VoteManager));
                return m.GetWIV(this);
            }
        }
        public Vote()
        {
            Type = nameof(Vote);
        }
    }
}
