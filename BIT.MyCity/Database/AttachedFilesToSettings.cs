using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace BIT.MyCity.Database
{
    public class AttachedFilesToSettings
    {
        private readonly ILazyLoader _lazyLoader;

        public AttachedFilesToSettings()
        { }

        public AttachedFilesToSettings(ILazyLoader lazyLoader)
        {
            _lazyLoader = lazyLoader;
        }

        [ForeignKey(nameof(Settings))]
        [NotNull]
        public string SettingsKey { get; set; }

        private Settings _settings;

        public Settings Settings
        {
            get => _settings ?? _lazyLoader.Load(this, ref _settings);
            set => _settings = value;
        }

        public long AttachedFileId { get; set; }

        private AttachedFile _attachedFile;

        public AttachedFile AttachedFile
        {
            get => _attachedFile ?? _lazyLoader.Load(this, ref _attachedFile);
            set => _attachedFile = value;
        }
    }
}
