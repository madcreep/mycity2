using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BIT.MyCity.Database
{
  public class User2Organization
  {
    public long UserId { get; set; }

    public long UserOrganizationId { get; set; }

    [ForeignKey("UserId")]
    public User User { get; set; }

    [ForeignKey("UserOrganizationId")]
    public UserOrganization UserOrganization { get; set; }

    public bool Chief { get; set; }
  }
}
