using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace BIT.MyCity.Database
{
    public class Subsystem : BaseEntity, IHasSubsystemMetaData
    {
        [Required]
        public string UID { get; set; }
        [NotMapped]
        public string Name { get; set; }
        [NotMapped]
        public string Description { get; set; }
        [NotMapped]
        public List<long> RegionIds { get; set; } = new List<long>();
        [NotMapped]
        public List<long> RegionIdsChange { get; set; } = new List<long>();
        [NotMapped]
        public List<Region> Regions => Region2Subsystems?.Select(el => el.Region).ToList() ?? new List<Region>();
        public List<Region2Subsystem> Region2Subsystems { get; set; }

        [NotMapped]
        public List<long> RubricIds { get; set; } = new List<long>();
        [NotMapped]
        public List<long> RubricIdsChange { get; set; } = new List<long>();
        [NotMapped]
        public List<Rubric> Rubrics => Rubric2Subsystems?.Select(el => el.Rubric).ToList() ?? new List<Rubric>();
        public List<Rubric2Subsystem> Rubric2Subsystems { get; set; }

        [NotMapped]
        public List<long> TerritoryIds { get; set; } = new List<long>();
        [NotMapped]
        public List<long> TerritoryIdsChange { get; set; } = new List<long>();
        [NotMapped]
        public List<Territory> Territories => Territory2Subsystems?.Select(el => el.Territory).ToList() ?? new List<Territory>();
        public List<Territory2Subsystem> Territory2Subsystems { get; set; }

        [NotMapped]
        public List<long> TopicIds { get; set; } = new List<long>();
        [NotMapped]
        public List<long> TopicIdsChange { get; set; } = new List<long>();
        [NotMapped]
        public List<Topic> Topics => Topic2Subsystems?.Select(el => el.Topic).ToList() ?? new List<Topic>();
        public List<Topic2Subsystem> Topic2Subsystems { get; set; }

        public long? ModeratorId { get; set; }
        public User Moderator { get; set; }

        [NotMapped]
        public List<SubsystemMainEntityField> MainEntityFieldsUpdate { get; set; } = new List<SubsystemMainEntityField>();
        #region IHasSubsystemMetaData
        public string MainEntityName { get; set; }//Appeal, News, VoteRoot
        public List<SubsystemMainEntityField> MainEntityFields { get; set; }
        public bool IsPublicDefault { get; set; }
        public bool PrivateOnly { get; set; }
        public bool UseLikesForMain { get; set; }
        public bool UseLikesForComments { get; set; }
        public bool UseDislikes { get; set; }
        public bool UseRates { get; set; }

        public bool UseComments { get; set; }
        public bool UseAttaches { get; set; }
        public bool UseMaps { get; set; }
        public bool UseRubricsDescription { get; set; }
        public bool UseTerritoriesDescription { get; set; }
        public bool UseRegionsDescription { get; set; }
        public bool UseTopicsDescription { get; set; }
        public bool UseInMobileClient { get; set; }
        #endregion
        public Subsystem()
        {
            Type = nameof(Subsystem);
        }
    }

    public class SubsystemMainEntityField
    {
        public string NameAPI { get; set; }//имя поля в системе
        public string NameDisplayed { get; set; }//имя поля для пользователя
        public string Description { get; set; }//описание поля для пользователя
        public string NameAPISecond { get; set; }//имя другого поля, содержимое которого выводим если основное не заполнено
        public bool Required { get; set; }//обязательно к заполнению при создании объекта
        public int Weight { get; set; }//вес для сортировки
        public string Placeholder { get; set; }//выводим это если поле не заполнено

        public long SubsystemId { get; set; }
        public Subsystem Subsystem { get; set; }
    }
}
