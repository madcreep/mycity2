using SkiaSharp;
using System;

namespace BIT.MyCity.Database
{
    public class ThumbNail
    {
        public long Id { get; set; }

        public SKEncodedImageFormat Format { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public string Base64Data { get; set; }

        public DateTime LastAccess { get; set; }

        public long AttachedFileId { get; set; }
    }
}
