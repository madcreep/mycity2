using System.Collections.Generic;

namespace BIT.MyCity.Database
{
    public abstract class BaseLinkUser
    {
        public long UserId { get; set; }
        public User User { get; set; }
    }

    public class UserOfSubsystem
    {
        public User User { get; set; }
        public Subsystem Subsystem { get; set; }
    }

    public class ClerkInSubsystem
    {
        public string SubsystemUid { get; set; }
        
        public List<User> Users { get; set; }
        
    }

    public class ClerkInSubsystemSet
    {
        public string SubsystemUid { get; set; }

        public List<long> UserIds { get; set; }

    }

}
