using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace BIT.MyCity.Database
{
    public class VoteRoot : BaseEntity, IHasAttachedFiles, IHasRubricators, IHasMessages
    {
        public enum StatusEnum
        {
            DRAFT,
            IS_PLANNED,
            IN_PROGRESS,
            IS_DONE,
            IS_ARCHIVED
        }

        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Text { get; set; }
        public string TextResult { get; set; }

        public DateTime? StartAt { get; set; }
        public DateTime? EndAt { get; set; }
        public DateTime? ArchiveAt { get; set; }
        public DateTime? StartedAt { get; set; }
        public DateTime? EndedAt { get; set; }
        public DateTime? ArchivedAt { get; set; }

        public StatusEnum State { get; set; } = StatusEnum.DRAFT;
        public SubscribeToAnswersEnum SubscribeToAnswers { get; set; } = SubscribeToAnswersEnum.Not_Subscribed;
        public bool CommentsAllowed { get; set; }
        public int OpenLevel { get; set; }

        public long? VoteEntryId { get; set; }
        public Vote VoteEntry { get; set; }

        public List<Vote> Votes { get; set; }
        [NotMapped]
        public List<long> VotesIds { get; set; }
        [NotMapped]
        public List<long> VotesIdsChange { get; set; }

        public List<AttachedFilesToVoteRoots> AttachedFilesToVoteRoots { get; set; }

        [NotMapped]
        public List<AttachedFile> AttachedFiles { get; set; }
        [NotMapped]
        public List<long> AttachedFilesIds { get; set; }
        //[NotMapped]
        //public List<long> AttachedFilesIdsChange { get; set; }

        public void AddFile(AttachedFile file)
        {
            throw new NotImplementedException();
        }

        public long? RegionId { get; set; }
        public Region Region { get; set; }
        public long? RubricId { get; set; }
        public Rubric Rubric { get; set; }
        public long? TerritoryId { get; set; }
        public Territory Territory { get; set; }
        public long? TopicId { get; set; }
        public Topic Topic { get; set; }

        [NotMapped]
        public int UsersCount {
            get 
                { 
                List<long> ul = new List<long>();
                if(Votes != null)
                {
                    foreach(var a in Votes)
                    {
                        if(a.UserVote != null)
                        {
                            ul.AddRange(a.UserVote.Select(el => el.UserId));
                        }
                    }
                }
                return ul.Distinct().Count();
            } 
        }

        public List<Message> Messages { get; set; }

        public VoteRoot()
        {
            Type = nameof(VoteRoot);
        }
    }
}
