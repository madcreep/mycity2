using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using BIT.MyCity.Attributes;
using BIT.MyCity.Helpers;
using BIT.MyCity.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace BIT.MyCity.Database
{
    public class ChangeJournalItem
    {
        public ChangeJournalItem()
        {
            Timestamp = DateTime.UtcNow;
        }

        public ChangeJournalItem(EntityEntry entity, long? userId)
            : this()
        {
            var entityAttribute = (ChangeJournalEntityAttribute) Attribute.GetCustomAttribute(entity.Entity.GetType(),
                typeof(ChangeJournalEntityAttribute));

            UserId = userId;
            Entity = entity;
            EntityName = Entity.Entity.GetType().Name;
            FriendlyName = entityAttribute?.FriendlyName;

            Operation = MapOperation(Entity.State);

            SetFields();
        }

        [NotMapped] public EntityEntry Entity { get; }

        public long Id { get; set; }

        public DateTime Timestamp { get; set; }

        public string EntityName { get; set; }

        public string FriendlyName { get; set; }

        public string EntityKey { get; set; }

        public EntityOperation Operation { get; set; }

        [NotMapped] public string OperationName => Operation.MapName();

        public long? UserId { get; set; }

        public User User { get; set; }

        public List<ChangeJournalProperties> Properties { get; set; } = new List<ChangeJournalProperties>();

        private EntityOperation MapOperation(EntityState eState)
        {
            switch (eState)
            {
                case EntityState.Added:
                    return EntityOperation.CREATING;
                case EntityState.Modified:
                    return EntityOperation.CHANGING;
                case EntityState.Deleted:
                case EntityState.Detached:
                    return EntityOperation.DELETING;
                case EntityState.Unchanged:
                    return EntityOperation.UNDEFINED;
                default:
                    throw new ArgumentOutOfRangeException(nameof(eState), eState, null);
            }
        }

        private void SetFields()
        {
            switch (Operation)
            {
                case EntityOperation.CREATING:
                    Properties.AddRange(Entity.Properties
                        .Where(el =>
                            Attribute.IsDefined(el.Metadata.PropertyInfo, typeof(ChangeJournalPropertyAttribute)))
                        .Select(el => new ChangeJournalProperties
                        {
                            Tag = el.Metadata.Name,
                            FriendlyName = ((ChangeJournalPropertyAttribute) Attribute.GetCustomAttribute(
                                el.Metadata.PropertyInfo,
                                typeof(ChangeJournalPropertyAttribute)))?.FriendlyName,
                            NewValue = GetValues(el, false),
                            NewValueFriendly = GetReferenceFriendlyName(el)
                        }));
                    break;
                case EntityOperation.CHANGING:
                    Properties.AddRange(Entity.Properties
                        .Where(el => el.IsModified
                                     && Attribute.IsDefined(el.Metadata.PropertyInfo,
                                         typeof(ChangeJournalPropertyAttribute)))
                        .Select(el => new ChangeJournalProperties
                        {
                            Tag = el.Metadata.Name,
                            FriendlyName = ((ChangeJournalPropertyAttribute) Attribute.GetCustomAttribute(
                                el.Metadata.PropertyInfo,
                                typeof(ChangeJournalPropertyAttribute)))?.FriendlyName,
                            OldValue = GetValues(el, true),
                            NewValue = GetValues(el, false),
                            NewValueFriendly = GetReferenceFriendlyName(el)
                        }));

                    break;
                case EntityOperation.DELETING:
                    Properties.AddRange(Entity.Properties
                        .Where(el =>
                            Attribute.IsDefined(el.Metadata.PropertyInfo, typeof(ChangeJournalPropertyAttribute)))
                        .Select(el => new ChangeJournalProperties
                        {
                            Tag = el.Metadata.Name,
                            FriendlyName = ((ChangeJournalPropertyAttribute) Attribute.GetCustomAttribute(
                                el.Metadata.PropertyInfo,
                                typeof(ChangeJournalPropertyAttribute)))?.FriendlyName,
                            OldValue = GetValues(el, true)
                        }));
                    break;
                case EntityOperation.UNDEFINED:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static string GetValues(PropertyEntry el, bool isOriginal)
        {
            var value = isOriginal
                ? el.OriginalValue
                : el.CurrentValue;

            if (value is ICollection<object> values)
            {
                return string.Join(";", values);
            }

            return value?.ToString();
        }

        public void InitEntityKey()
        {
            if (Entity == null)
            {
                return;
            }

            EntityKey = string.Join(",", Entity.Metadata
                .FindPrimaryKey()
                .Properties
                .Select(p => Entity.Property(p.Name).CurrentValue));
        }

        private static string GetReferenceFriendlyName(PropertyEntry el)
        {
            var attribute = (ChangeJournalPropertyAttribute) Attribute.GetCustomAttribute(el.Metadata.PropertyInfo,
                typeof(ChangeJournalPropertyAttribute));

            return attribute?.GetReferencePropertyCurrentValue(el);
        }
    }
}
