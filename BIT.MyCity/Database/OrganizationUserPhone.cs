using System;
using System.ComponentModel.DataAnnotations;

namespace BIT.MyCity.Database
{
    public class OrganizationUserPhone
    {
        [Key]
        public long Id { get; set; }

        public long? UserId { get; set; }

        public User User { get; set; }

        public long? OrganizationId { get; set; }

        public UserOrganization UserOrganization { get; set; }

        public long PhoneId { get; set; }

        public Phone Phone { get; set; }

        /// <summary>
        /// Подтвержден
        /// </summary>
        public bool Confirmed { get; set; }

        public string ConfirmCode { get; set; }

        public DateTime? ConfirmCodeLifeTime { get; set; }
    }
}
