﻿namespace BIT.MyCity.Database
{
    public class Region2Subsystem
    {
        public long RegionId { get; set; }
        public Region Region { get; set; }
        public long SubsystemId { get; set; }
        public Subsystem Subsystem { get; set; }
    }
    public class Rubric2Subsystem
    {
        public long RubricId { get; set; }
        public Rubric Rubric { get; set; }
        public long SubsystemId { get; set; }
        public Subsystem Subsystem { get; set; }
    }
    public class Territory2Subsystem
    {
        public long TerritoryId { get; set; }
        public Territory Territory { get; set; }
        public long SubsystemId { get; set; }
        public Subsystem Subsystem { get; set; }
    }
    public class Topic2Subsystem
    {
        public long TopicId { get; set; }
        public Topic Topic { get; set; }
        public long SubsystemId { get; set; }
        public Subsystem Subsystem { get; set; }
    }
}
