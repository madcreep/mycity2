using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Migrations.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BIT.MyCity.Attributes;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using log4net;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Database
{
    public class PostgreHistoryRepository : NpgsqlHistoryRepository
    {
        public PostgreHistoryRepository(HistoryRepositoryDependencies dependencies)
            : base(dependencies)
        {
        }

        protected override void ConfigureTable(EntityTypeBuilder<HistoryRow> history)
        {
            base.ConfigureTable(history);
            history.Property(h => h.MigrationId).HasColumnName("migration_id");
            history.Property(h => h.ProductVersion).HasColumnName("product_version");
        }
    }

    public class DocumentsContext : IdentityDbContext<User, Role, long,
        IdentityUserClaim<long>, UserRole,
        IdentityUserLogin<long>, IdentityRoleClaim<long>,
        IdentityUserToken<long>>
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(DocumentsContext));

        private readonly IServiceProvider _services;

        public DocumentsContext(DbContextOptions<DocumentsContext> options, IServiceProvider services)
          : base(options)
        {
            _services = services;
        }
        
        public new DbSet<User> Users { get; set; }

        public DbSet<CustomStorageItem> CustomStorage { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<UserOrganization> UserOrganizations { get; set; }

        public DbSet<Phone> Phones { get; set; }

        public DbSet<OrganizationUserPhone> OrganizationUserPhones { get; set; }

        public DbSet<UserIdentityDocument> UserIdentityDocuments { get; set; }

        public DbSet<Subsystem> Subsystems { get; set; }

        public DbSet<Region> Regions { get; set; }
        public DbSet<Territory> Territories { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<Rubric> Rubrics { get; set; }

        public DbSet<Appeal> Appeals { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<AttachedFile> AttachedFiles { get; set; }
        public DbSet<AttachedFilesToAppeals> AttachedFilesToAppeals { get; set; }
        public DbSet<AttachedFilesToMessages> AttachedFilesToMessages { get; set; }
        public DbSet<AttachedFilesToAppointments> AttachedFilesToAppointments { get; set; }
        public DbSet<AttachedFilesToEmails> AttachedFilesToEmails { get; set; }
        public DbSet<AttachedFilesToNews> AttachedFilesToNews { get; set; }
        public DbSet<AttachedFilesToSettings> AttachedFilesToSettings { get; set; }
        public DbSet<AttachedFilesToVoteItems> AttachedFilesToVoteItems { get; set; }
        public DbSet<AttachedFilesToVoteRoots> AttachedFilesToVoteRoots { get; set; }
        public DbSet<AttachedFilesToVotes> AttachedFilesToVotes { get; set; }


        public DbSet<UserRegion> UserRegions { get; set; }
        public DbSet<UserRubric> UserRubrics { get; set; }
        public DbSet<UserTerritory> UserTerritories { get; set; }
        public DbSet<UserModeratorTopic> UserModeratorTopics { get; set; }

        public DbSet<ExecutorRegion> ExecutorRegions { get; set; }
        public DbSet<ExecutorRubric> ExecutorRubrics { get; set; }
        public DbSet<ExecutorTerritory> ExecutorTerritories { get; set; }
        public DbSet<ExecutorTopic> ExecutorTopics { get; set; }

        public DbSet<UserModeratorAppeal> UserModeratorAppeals { get; set; }
        public DbSet<UserViewAppeal> UserViewAppeals { get; set; }
        public DbSet<UserViewMessage> UserViewMessages { get; set; }
        public DbSet<UserLikeAppeal> UserLikeAppeals { get; set; }
        public DbSet<UserLikeMessage> UserLikeMessage { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }

        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<AppointmentNotification> AppointmentNotifications { get; set; }
        public DbSet<AppointmentRange> AppointmentRanges { get; set; }
        public DbSet<Officer> Officers { get; set; }
        public DbSet<VoteRoot> VoteRoots { get; set; }
        public DbSet<Vote> Votes { get; set; }
        public DbSet<VoteItem> VoteItems { get; set; }
        public DbSet<UserVote> UserVotes { get; set; }
        public DbSet<ProtocolRecord> ProtocolRecords { get; set; }
        public DbSet<AppealExecutor> AppealExecutors { get; set; }
        public DbSet<Appeal2Executor> Appeal2Executor { get; set; }
        public DbSet<Appeal2PrincipalExecutor> Appeal2PrincipalExecutor { get; set; }
        public DbSet<Region2Subsystem> Region2Subsystems { get; set; }
        public DbSet<Rubric2Subsystem> Rubric2Subsystems { get; set; }
        public DbSet<Territory2Subsystem> Territory2Subsystems { get; set; }
        public DbSet<Topic2Subsystem> Topic2Subsystems { get; set; }
        public DbSet<Email> Emails { get; set; }
        public DbSet<SmsMessage> SmsMessages { get; set; }
        public DbSet<PushMessage> PushMessages { get; set; }
        public DbSet<EnumVerb> EnumVerbs { get; set; }
        public DbSet<ThumbNail> ThumbNails { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<UserLikeNews> UserLikeNews { get; set; }
        public DbSet<UserViewNews> UserViewNews { get; set; }
        public DbSet<UserRegardMessage> MessageRegards { get; set; }
        public DbSet<Subs2DueMessages> Subs2DueMessages { get; set; }
        public DbSet<SubsystemMainEntityField> SubsystemMainEntityFields { get; set; }
        public DbSet<AppealStats> AppealStats { get; set; }
        public DbSet<PToken> Tokens { get; set; }

        public DbSet<ChangeJournalItem> Journal { get; set; }
        public DbSet<ChangeJournalProperties> JournalProperties { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasDefaultSchema("public");

            #region UserRole

            modelBuilder.Entity<UserRole>()
                .HasOne(el => el.User)
                .WithMany(el => el.UserRole)
                .HasForeignKey(el => el.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<UserRole>()
                .HasOne(el => el.Role)
                .WithMany(el => el.UserRole)
                .HasForeignKey(el => el.RoleId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region User

            modelBuilder.Entity<User>()
                .HasIndex(b => b.UserName)
                .IsUnique();

            modelBuilder.Entity<User>()
                .HasIndex(b => b.NormalizeFullName);

            modelBuilder.Entity<User>()
                .HasMany(el => el.IdentityClaims)
                .WithOne()
                .HasForeignKey(uc => uc.UserId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(el => el.Addresses)
                .WithOne()
                .HasForeignKey(el => el.UserId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<User>()
                .HasIndex(b => b.OldSystemId)
                .IsUnique(false);

            modelBuilder.Entity<User>()
                .HasIndex(b => b.ExternalId)
                .IsUnique(false);

            #endregion

            #region Role

            modelBuilder.Entity<Role>()
                .HasMany(el => el.IdentityClaims)
                .WithOne()
                .HasForeignKey(uc => uc.RoleId)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Role>()
                .HasIndex(b => b.Weight)
                .IsUnique();

            #endregion

            #region UsersToOrganizations Linc

            modelBuilder.Entity<User2Organization>()
                .HasKey(t => new { t.UserId, t.UserOrganizationId });

            modelBuilder.Entity<User2Organization>()
                .HasOne(pt => pt.User)
                .WithMany(p => p.Users2Organizations)
                .HasForeignKey(pt => pt.UserId);

            modelBuilder.Entity<User2Organization>()
                .HasOne(el => el.UserOrganization)
                .WithMany(el => el.Users2Organizations)
                .HasForeignKey(el => el.UserOrganizationId);

            #endregion

            #region Organization

            modelBuilder.Entity<UserOrganization>()
                .HasIndex(b => b.Inn)
                .IsUnique(false);

            modelBuilder.Entity<UserOrganization>()
                .HasIndex(b => b.Kpp)
                .IsUnique(false);

            modelBuilder.Entity<UserOrganization>()
                .HasIndex(b => b.Ogrn)
                .IsUnique(false);

            modelBuilder.Entity<UserOrganization>()
                .HasIndex(b => b.NormalizeShortName)
                .IsUnique(false);

            modelBuilder.Entity<UserOrganization>()
                .HasIndex(b => b.NormalizeFullName)
                .IsUnique(false);

            modelBuilder.Entity<UserOrganization>()
                .HasOne(x => x.Parent)
                .WithMany(x => x.Branches)
                .HasForeignKey(x => x.ParentId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<UserOrganization>()
                .HasMany(el => el.Addresses)
                .WithOne()
                .HasForeignKey(el => el.UserOrganizationId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            #endregion

            #region Phones

            modelBuilder.Entity<OrganizationUserPhone>()
                .HasOne(x => x.User)
                .WithMany(x => x.UserPhones)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<OrganizationUserPhone>()
                .HasOne(x => x.Phone)
                .WithMany(x => x.OrganizationUserPhones)
                .HasForeignKey(x => x.PhoneId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<OrganizationUserPhone>()
                .HasOne(x => x.UserOrganization)
                .WithMany(x => x.OrganizationPhones)
                .HasForeignKey(x => x.OrganizationId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<OrganizationUserPhone>()
                .HasOne(x => x.Phone)
                .WithMany(x => x.OrganizationUserPhones)
                .HasForeignKey(x => x.PhoneId)
                .OnDelete(DeleteBehavior.Cascade);

            #endregion

            #region Rubric
            modelBuilder.Entity<Rubric>().HasIndex(r => r.Due);
            modelBuilder.Entity<Rubric>().HasIndex(r => r.Id);

            modelBuilder.Entity<UserRubric>().HasKey(t => new { t.UserId, t.RubricId, t.SubsystemId });
            modelBuilder.Entity<UserRubric>().HasOne(u => u.User).WithMany(g => g.UserRubric).HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserRubric>().HasOne(u => u.Rubric).WithMany(g => g.UserModerator).HasForeignKey(u => u.RubricId);

            modelBuilder.Entity<ExecutorRubric>().HasKey(t => new { t.UserId, t.RubricId, t.SubsystemId });
            modelBuilder.Entity<ExecutorRubric>().HasOne(u => u.User).WithMany(g => g.ExecutorRubric).HasForeignKey(u => u.UserId);
            modelBuilder.Entity<ExecutorRubric>().HasOne(u => u.Rubric).WithMany(g => g.UserExecutor).HasForeignKey(u => u.RubricId);

            #endregion
            #region Region
            modelBuilder.Entity<Region>().HasIndex(r => r.Due);
            modelBuilder.Entity<Region>().HasIndex(r => r.Id);

            modelBuilder.Entity<UserRegion>()
                .HasKey(t => new { t.UserId, t.RegionId, t.SubsystemId });
            modelBuilder.Entity<UserRegion>()
                .HasOne(u => u.User)
                .WithMany(g => g.UserRegion)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserRegion>()
                .HasOne(u => u.Region)
                .WithMany(g => g.UserModerator)
                .HasForeignKey(u => u.RegionId);

            modelBuilder.Entity<ExecutorRegion>().HasKey(t => new { t.UserId, t.RegionId, t.SubsystemId });
            modelBuilder.Entity<ExecutorRegion>().HasOne(u => u.User).WithMany(g => g.ExecutorRegion).HasForeignKey(u => u.UserId);
            modelBuilder.Entity<ExecutorRegion>().HasOne(u => u.Region).WithMany(g => g.UserExecutor).HasForeignKey(u => u.RegionId);
            #endregion
            
            #region Territory
            modelBuilder.Entity<Territory>().HasIndex(r => r.Due);
            modelBuilder.Entity<Territory>().HasIndex(r => r.Id);

            modelBuilder.Entity<UserTerritory>().HasKey(t => new { t.UserId, t.TerritoryId, t.SubsystemId });
            modelBuilder.Entity<UserTerritory>().HasOne(u => u.User).WithMany(g => g.UserTerritory).HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserTerritory>().HasOne(u => u.Territory).WithMany(g => g.UserModerator).HasForeignKey(u => u.TerritoryId);

            modelBuilder.Entity<ExecutorTerritory>().HasKey(t => new { t.UserId, t.TerritoryId, t.SubsystemId });
            modelBuilder.Entity<ExecutorTerritory>().HasOne(u => u.User).WithMany(g => g.ExecutorTerritory).HasForeignKey(u => u.UserId);
            modelBuilder.Entity<ExecutorTerritory>().HasOne(u => u.Territory).WithMany(g => g.UserExecutor).HasForeignKey(u => u.TerritoryId);
            #endregion
            #region Topic
            modelBuilder.Entity<Topic>().HasIndex(r => r.Due);
            modelBuilder.Entity<Topic>().HasIndex(r => r.Id);

            modelBuilder.Entity<UserModeratorTopic>().HasKey(t => new { t.UserId, t.TopicId, t.SubsystemId });
            modelBuilder.Entity<UserModeratorTopic>().HasOne(u => u.User).WithMany(g => g.UserModeratorTopic).HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserModeratorTopic>().HasOne(u => u.Topic).WithMany(g => g.UserModerator).HasForeignKey(u => u.TopicId);

            modelBuilder.Entity<ExecutorTopic>().HasKey(t => new { t.UserId, t.TopicId, t.SubsystemId });
            modelBuilder.Entity<ExecutorTopic>().HasOne(u => u.User).WithMany(g => g.ExecutorTopic).HasForeignKey(u => u.UserId);
            modelBuilder.Entity<ExecutorTopic>().HasOne(u => u.Topic).WithMany(g => g.UserExecutor).HasForeignKey(u => u.TopicId);
            #endregion

            #region Subsystem

            modelBuilder.Entity<Subsystem>().HasIndex(r => r.UID);

            modelBuilder.Entity<Topic>()
                .HasOne(p => p.Subsystem)
                .WithMany(t => t.Topics)
                .HasForeignKey(p => p.SubsystemId_);

            modelBuilder.Entity<Subsystem>()
                .HasOne(p => p.Moderator)
                .WithMany(p => p.SSModeratorDefault);

            #endregion

            #region Settings

            modelBuilder.Entity<Settings>()
                .Property(e => e.Values)
                .HasConversion(
                    v =>
                        v == null || !v.Any()
                            ? null
                            : v.SerializeJson(false),
                    v =>
                        v == null
                            ? null
                            : v.TryDeserializeJson<SettingsValueItem[]>());

            #endregion

            modelBuilder.Entity<UserModeratorAppeal>()
                .HasKey(t => new { t.UserId, t.AppealId });
            modelBuilder.Entity<UserModeratorAppeal>()
                .HasOne(u => u.User)
                .WithMany(g => g.UserModeratorAppeal)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserModeratorAppeal>()
                .HasOne(u => u.Appeal)
                .WithMany(g => g.UserModeratorAppeal)
                .HasForeignKey(u => u.AppealId);

            #region News
            modelBuilder.Entity<News>()
                .HasOne(p => p.Author)
                .WithMany(p => p.AuthorInNews);

            modelBuilder.Entity<UserViewNews>()
                .HasKey(t => new { t.UserId, t.NewsId });
            modelBuilder.Entity<UserViewNews>()
                .HasOne(u => u.User)
                .WithMany(g => g.ViewNewsLink)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserViewNews>()
                .HasOne(u => u.News)
                .WithMany(g => g.UserView)
                .HasForeignKey(u => u.NewsId);

            modelBuilder.Entity<UserLikeNews>()
                .HasKey(t => new { t.UserId, t.NewsId });
            modelBuilder.Entity<UserLikeNews>()
                .HasOne(u => u.User)
                .WithMany(g => g.LikeNewsLink)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserLikeNews>()
                .HasOne(u => u.News)
                .WithMany(g => g.UserLike)
                .HasForeignKey(u => u.NewsId);
            #endregion

            modelBuilder.Entity<UserRegardMessage>()
                .HasKey(t => new { t.UserId, t.MessageId });
            modelBuilder.Entity<UserRegardMessage>()
                .HasOne(u => u.User)
                .WithMany(g => g.RegardMessagesLink)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserRegardMessage>()
                .HasOne(u => u.Message)
                .WithMany(g => g.UserRegardMessage)
                .HasForeignKey(u => u.MessageId);

            modelBuilder.Entity<UserViewMessage>()
                .HasKey(t => new { t.UserId, t.MessageId });
            modelBuilder.Entity<UserViewMessage>()
                .HasOne(u => u.User)
                .WithMany(g => g.ViewMessagesLink)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserViewMessage>()
                .HasOne(u => u.Message)
                .WithMany(g => g.UserViewMessage)
                .HasForeignKey(u => u.MessageId);

            modelBuilder.Entity<UserLikeMessage>()
                .HasKey(t => new { t.UserId, t.MessageId });
            modelBuilder.Entity<UserLikeMessage>()
                .HasOne(u => u.User)
                .WithMany(g => g.LikeMessagesLink)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserLikeMessage>()
                .HasOne(u => u.Message)
                .WithMany(g => g.UserLikeMessage)
                .HasForeignKey(u => u.MessageId);

            modelBuilder.Entity<UserViewAppeal>()
                .HasKey(t => new { t.UserId, t.AppealId });
            modelBuilder.Entity<UserViewAppeal>()
                .HasOne(u => u.User)
                .WithMany(g => g.ViewAppealsLink)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserViewAppeal>()
                .HasOne(u => u.Appeal)
                .WithMany(g => g.UserViewAppeal)
                .HasForeignKey(u => u.AppealId);

            modelBuilder.Entity<UserLikeAppeal>()
                .HasKey(t => new { t.UserId, t.AppealId });
            modelBuilder.Entity<UserLikeAppeal>()
                .HasOne(u => u.User)
                .WithMany(g => g.UserLikeAppeal)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserLikeAppeal>()
                .HasOne(u => u.Appeal)
                .WithMany(g => g.UserLikeAppeal)
                .HasForeignKey(u => u.AppealId);

            modelBuilder.Entity<Appeal>()
                .HasOne(p => p.Author)
                .WithMany(p => p.AuthorInAppeals);

            modelBuilder.Entity<PToken>()
                .HasOne(p => p.Owner)
                .WithMany(p => p.PTokens);

            modelBuilder.Entity<Appeal>()
                .HasIndex(p => p.OldSystemId)
                .IsUnique(false);

            #region message
            modelBuilder.Entity<Message>()
                .HasOne(p => p.Author)
                .WithMany(p => p.Messages);
            modelBuilder.Entity<Message>()
                .HasOne(p => p.Appeal)
                .WithMany(p => p.Messages);
            modelBuilder.Entity<Message>()
                .HasOne(p => p.Vote)
                .WithMany(p => p.Messages);
            modelBuilder.Entity<Message>()
                .HasOne(p => p.VoteRoot)
                .WithMany(p => p.Messages);
            modelBuilder.Entity<Message>()
                .HasOne(p => p.News)
                .WithMany(p => p.Messages);
            modelBuilder.Entity<Message>()
                .HasOne(p => p.AppealExecutor)
                .WithMany(p => p.Messages);
            #endregion

            #region Appointment
            modelBuilder.Entity<AppointmentNotification>()
                .HasOne(p => p.Appointment)
                .WithMany(p => p.Answers);

            modelBuilder.Entity<Appointment>()
                .HasOne(p => p.AppointmentRange)
                .WithMany(p => p.RegisteredAppointments);

            modelBuilder.Entity<AppointmentRange>()
                .HasOne(p => p.Officer)
                .WithMany(p => p.Ranges);
            #endregion
            #region Votes

            modelBuilder.Entity<VoteItem>()
                .HasOne(p => p.Vote)
                .WithMany(p => p.VoteItems)
                .HasForeignKey(u => u.VoteId);

            modelBuilder.Entity<UserVote>()
                .HasKey(t => new { t.UserId, t.VoteId });
            modelBuilder.Entity<UserVote>()
                .HasOne(u => u.User)
                .WithMany(g => g.UserVote)
                .HasForeignKey(u => u.UserId);
            modelBuilder.Entity<UserVote>()
                .HasOne(u => u.Vote)
                .WithMany(g => g.UserVote)
                .HasForeignKey(u => u.VoteId);

            modelBuilder.Entity<Vote>()
                .HasOne(p => p.VoteRoot)
                .WithMany(t => t.Votes)
                .HasForeignKey(p => p.VoteRootId);

            modelBuilder.Entity<VoteItem>()
                .HasOne(p => p.NextVote)
                .WithMany(t => t.ParentVoteItems)
                .HasForeignKey(p => p.NextVoteId);
            #endregion
            #region Settings
            modelBuilder.Entity<Settings>().HasIndex(r => r.Key);
            //modelBuilder.Entity<Settings>()
            //    .HasMany(el => el.AttachedFiles)
            //    .WithOne()
            //    .OnDelete(DeleteBehavior.SetNull);

            #endregion
            #region AppealExecutor
            modelBuilder.Entity<Appeal2Executor>()
                .HasKey(t => new { t.AppealExecutorId, t.AppealId });
            modelBuilder.Entity<Appeal2Executor>()
                .HasOne(u => u.AppealExecutor)
                .WithMany(g => g.Appeal2Executors)
                .HasForeignKey(u => u.AppealExecutorId);
            modelBuilder.Entity<Appeal2Executor>()
                .HasOne(u => u.Appeal)
                .WithMany(g => g.Appeal2Executors)
                .HasForeignKey(u => u.AppealId);

            modelBuilder.Entity<Appeal2PrincipalExecutor>()
                .HasKey(t => new { t.AppealPrincipalExecutorId, t.AppealId });
            modelBuilder.Entity<Appeal2PrincipalExecutor>()
                .HasOne(u => u.AppealPrincipalExecutor)
                .WithMany(g => g.Appeal2PrincipalExecutors)
                .HasForeignKey(u => u.AppealPrincipalExecutorId);
            modelBuilder.Entity<Appeal2PrincipalExecutor>()
                .HasOne(u => u.Appeal)
                .WithMany(g => g.Appeal2PrincipalExecutors)
                .HasForeignKey(u => u.AppealId);
            #endregion

            modelBuilder.Entity<EnumVerb>()
                .HasIndex(t => new { t.Value, t.Group, t.SubsystemUID })
                .IsUnique();

            modelBuilder.Entity<Subs2DueMessages>()
                .HasIndex(t => t.Due);
            modelBuilder.Entity<Subs2DueMessages>()
                .HasKey(t => new { t.RootType, t.Due, t.UserId });
            modelBuilder.Entity<Subs2DueMessages>()
                .HasOne(t => t.User)
                .WithMany(t => t.Subs2DueMessages)
                .HasForeignKey(t => t.UserId);

            modelBuilder.Entity<SubsystemMainEntityField>()
                .HasKey(t => new { t.SubsystemId, t.NameAPI });
            modelBuilder.Entity<SubsystemMainEntityField>()
                .HasOne(t => t.Subsystem)
                .WithMany(t => t.MainEntityFields)
                .HasForeignKey(t => t.SubsystemId);

            #region Rubricator2Subsystem
            modelBuilder.Entity<Region2Subsystem>()
                .HasKey(t => new { t.RegionId, t.SubsystemId });
            modelBuilder.Entity<Region2Subsystem>()
                .HasOne(u => u.Region)
                .WithMany(g => g.Rubricator2Subsystems)
                .HasForeignKey(u => u.RegionId);
            modelBuilder.Entity<Region2Subsystem>()
                .HasOne(u => u.Subsystem)
                .WithMany(g => g.Region2Subsystems)
                .HasForeignKey(u => u.SubsystemId);

            modelBuilder.Entity<Rubric2Subsystem>()
                .HasKey(t => new { t.RubricId, t.SubsystemId });
            modelBuilder.Entity<Rubric2Subsystem>()
                .HasOne(u => u.Rubric)
                .WithMany(g => g.Rubricator2Subsystems)
                .HasForeignKey(u => u.RubricId);
            modelBuilder.Entity<Rubric2Subsystem>()
                .HasOne(u => u.Subsystem)
                .WithMany(g => g.Rubric2Subsystems)
                .HasForeignKey(u => u.SubsystemId);

            modelBuilder.Entity<Territory2Subsystem>()
                .HasKey(t => new { t.TerritoryId, t.SubsystemId });
            modelBuilder.Entity<Territory2Subsystem>()
                .HasOne(u => u.Territory)
                .WithMany(g => g.Rubricator2Subsystems)
                .HasForeignKey(u => u.TerritoryId);
            modelBuilder.Entity<Territory2Subsystem>()
                .HasOne(u => u.Subsystem)
                .WithMany(g => g.Territory2Subsystems)
                .HasForeignKey(u => u.SubsystemId);

            modelBuilder.Entity<Topic2Subsystem>()
                .HasKey(t => new { t.TopicId, t.SubsystemId });
            modelBuilder.Entity<Topic2Subsystem>()
                .HasOne(u => u.Topic)
                .WithMany(g => g.Rubricator2Subsystems)
                .HasForeignKey(u => u.TopicId);
            modelBuilder.Entity<Topic2Subsystem>()
                .HasOne(u => u.Subsystem)
                .WithMany(g => g.Topic2Subsystems)
                .HasForeignKey(u => u.SubsystemId);
            #endregion
            
            #region AppealStats
            modelBuilder.Entity<AppealStats>()
                .HasNoKey()
                .ToView("AppealsView");
            modelBuilder.Entity<AppealStats>()
                .HasOne(u => u.Subsystem)
                .WithMany()
                .HasForeignKey(u => u.SubsystemId);
            modelBuilder.Entity<AppealStats>()
                .HasOne(u => u.Rubric)
                .WithMany()
                .HasForeignKey(u => u.RubricId);
            modelBuilder.Entity<AppealStats>()
                .HasOne(u => u.Territory)
                .WithMany()
                .HasForeignKey(u => u.TerritoryId);
            modelBuilder.Entity<AppealStats>()
                .HasOne(u => u.Topic)
                .WithMany()
                .HasForeignKey(u => u.TopicId);
            modelBuilder.Entity<AppealStats>()
                .HasOne(u => u.Region)
                .WithMany()
                .HasForeignKey(u => u.RegionId);
            #endregion

            #region Journal

            modelBuilder.Entity<ChangeJournalItem>()
                .HasMany(g => g.Properties)
                .WithOne()
                .HasForeignKey(s => s.ChangeJournalItemId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ChangeJournalItem>()
                .HasIndex(el => new
                {
                    el.Timestamp,
                    el.Operation,
                    el.EntityKey
                });

            modelBuilder.Entity<ChangeJournalProperties>()
                .HasIndex(el => el.Tag);

            #endregion
            
            modelBuilder.Entity<Appeal>()
                .HasGeneratedTsVectorColumn(
                    p => p.SearchVector,
                    "russian",  // Text search config
                    p => new { p.Title, p.Description })  // Included properties
                .HasIndex(p => p.SearchVector)
                .HasMethod("GIN");

            #region AttachedFilesTo

            modelBuilder.Entity<AttachedFilesToAppeals>()
                .HasKey(sc => new {sc.AppealId, sc.AttachedFileId});
            modelBuilder.Entity<AttachedFilesToAppointments>()
                .HasKey(sc => new { sc.AppointmentId, sc.AttachedFileId });
            modelBuilder.Entity<AttachedFilesToMessages>()
                .HasKey(sc => new { sc.MessageId, sc.AttachedFileId });
            modelBuilder.Entity<AttachedFilesToNews>()
                .HasKey(sc => new { sc.NewsId, sc.AttachedFileId });
            modelBuilder.Entity<AttachedFilesToEmails>()
                .HasKey(sc => new { sc.EmailId, sc.AttachedFileId });
            modelBuilder.Entity<AttachedFilesToVotes>()
                .HasKey(sc => new { sc.VoteId, sc.AttachedFileId });
            modelBuilder.Entity<AttachedFilesToVoteRoots>()
                .HasKey(sc => new { sc.VoteRootId, sc.AttachedFileId });
            modelBuilder.Entity<AttachedFilesToVoteItems>()
                .HasKey(sc => new { sc.VoteItemId, sc.AttachedFileId });
            modelBuilder.Entity<AttachedFilesToSettings>()
                .HasKey(sc => new { sc.SettingsKey, sc.AttachedFileId });
            #endregion

            modelBuilder.Entity<CustomStorageItem>()
                .HasKey(el => new {el.Key, el.OwnerName, el.OwnerId, el.Weight});

            /*в БД не сохраняется DateTime.kind, при чтении получается Unspecified, принудительно ставим всюду Utc
             отсюда: https://github.com/dotnet/efcore/issues/4711*/
            var dateTimeConverter = new ValueConverter<DateTime, DateTime>(v => v, v => DateTime.SpecifyKind(v, DateTimeKind.Utc));
            var nullableDateTimeConverter = new ValueConverter<DateTime?, DateTime?>(v => v, v => v.HasValue ? DateTime.SpecifyKind(v.Value, DateTimeKind.Utc) : v);
            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                foreach (var property in entityType.GetProperties())
                {
                    if (property.ClrType == typeof(DateTime))
                    {
                        property.SetValueConverter(dateTimeConverter);
                    }
                    else if (property.ClrType == typeof(DateTime?))
                    {
                        property.SetValueConverter(nullableDateTimeConverter);
                    }
                }
            }
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            var timestamp = DateTime.UtcNow;

            var logItems = GetChangeLogItems();

            SetTimestamp(timestamp);

            var saveResult = base.SaveChanges(acceptAllChangesOnSuccess);

            try
            {
                foreach (var item in logItems)
                {
                    item.Timestamp = timestamp;
                    item.InitEntityKey();
                }

                Journal.AddRange(logItems);

                base.SaveChanges(acceptAllChangesOnSuccess);
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка записи лога изменений.", ex);
            }

            return saveResult;
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            var timestamp = DateTime.UtcNow;

            var logItems = GetChangeLogItems()                           
                .ToArray();

            SetTimestamp(timestamp);

            var saveResult = base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);

            saveResult.Wait(cancellationToken);

            try
            {
                foreach (var item in logItems)
                {
                    item.Timestamp = timestamp;
                    item.InitEntityKey();
                }

                Journal.AddRange(logItems);

                var saveResult2 = base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);

                saveResult2.Wait(cancellationToken);
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка записи лога изменений.", ex);
            }

            return saveResult;
        }

        private void SetTimestamp(DateTime timestamp)
        {
            var entries = ChangeTracker
                .Entries()
                .Where(e => e.Entity is IUpdatedEntity && (
                    e.State == EntityState.Added ||
                    e.State == EntityState.Modified));

            foreach (var entityEntry in entries)
            {
                if (entityEntry.State == EntityState.Modified)
                {
                    ((IUpdatedEntity)entityEntry.Entity).UpdatedAt = timestamp;
                }

                if (entityEntry.State == EntityState.Added)
                {
                    ((IUpdatedEntity)entityEntry.Entity).CreatedAt = timestamp;
                    ((IUpdatedEntity)entityEntry.Entity).UpdatedAt = timestamp;
                }
            }
        }

        private IEnumerable<ChangeJournalItem> GetChangeLogItems()
        {
            try
            {
                var userManager = _services.GetRequiredService<UserManager>();
                var userId = userManager.ClaimsPrincipalId();

                return ChangeTracker.Entries()
                    .Where(el => Attribute.IsDefined(el.Entity.GetType(), typeof(ChangeJournalEntityAttribute))
                                 && (el.State == EntityState.Added || el.State == EntityState.Deleted
                                 || el.State == EntityState.Modified
                                    && el.Properties.Any(p=> p.IsModified && Attribute.IsDefined(p.Metadata.PropertyInfo, typeof(ChangeJournalPropertyAttribute)))))
                    .Select(t => new ChangeJournalItem(t, userId))
                    .ToArray();
            }
            catch (Exception ex)
            {
                _log.Warn("Ошибка получения измененных сущностей", ex);
            }

            return new ChangeJournalItem[0];
        }
        
        public IEnumerable<JournalEntityTags> JournalTags
        {
            get
            {
                return Model.GetEntityTypes()
                    .Where(el => Attribute.IsDefined(el.ClrType, typeof(ChangeJournalEntityAttribute)))
                    .Select(el => new JournalEntityTags
                    {
                        Tag = el.ClrType.Name,
                        Name = ((ChangeJournalEntityAttribute) Attribute.GetCustomAttribute(el.ClrType,
                            typeof(ChangeJournalEntityAttribute)))?.FriendlyName,
                        PropertyTags = el.ClrType.GetProperties()
                            .Where(x => Attribute.IsDefined(x, typeof(ChangeJournalPropertyAttribute)))
                            .Select(x => new JournalTags
                            {
                                Tag = x.Name,
                                Name = ((ChangeJournalPropertyAttribute) Attribute.GetCustomAttribute(x,
                                    typeof(ChangeJournalPropertyAttribute)))?.FriendlyName
                            })
                            .OrderBy(x => x.Name)
                            .ToArray()
                    })
                    .OrderBy(el => el.Name)
                    .ToArray();
            }
        }
    }
}
