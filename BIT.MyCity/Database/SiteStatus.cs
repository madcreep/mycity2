namespace BIT.MyCity.Database
{
    public enum SiteStatus
    {
        /// <summary>
        /// Отправлено
        /// </summary>
        Sent = 0,
        /// <summary>
        /// Зарегистрировано
        /// </summary>
        Registered = 1,
        /// <summary>
        /// На рассмотрении
        /// </summary>
        UnderСonsideration = 2,
        /// <summary>
        /// Направлено по компетенции
        /// </summary>
        DirectedByCompetence = 3,
        /// <summary>
        /// На рассмотрении 2
        /// </summary>
        UnderСonsideration2 = 4,
        /// <summary>
        /// На рассмотрении 2
        /// </summary>
        Answered = 5,
        /// <summary>
        /// Не является обращением
        /// </summary>
        NotAppeal = 6,
        /// <summary>
        /// Продлено
        /// </summary>
        Extended = 7,
        /// <summary>
        /// Передано депутату
        /// </summary>
        TransferredToDeputy = 8,
        /// <summary>
        /// Рассмотрение прекращено по заявлению гражданина
        /// </summary>
        ConsiderationTerminated = 9
    }
}
