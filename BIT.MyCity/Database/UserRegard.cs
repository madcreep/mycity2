﻿using System;

namespace BIT.MyCity.Database
{
    public abstract class UserRegard : IUpdatedEntity
    {
        public User User { get; set; }
        public long? UserId { get; set; }
        public int Regard { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
    public class UserRegardMessage : UserRegard
    {
        public Message Message { get; set; }
        public long? MessageId { get; set; }
    }
}
