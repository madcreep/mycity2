using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using BIT.EsiaNETCore;

namespace BIT.MyCity.Database
{
    public class Address
    {
        [Key]
        public long Id { get; set; }

        public long? UserId { get; set; }

        public long? UserOrganizationId { get; set; }

        public AddrType AddrType { get; set; }

        /// <summary>
        /// Индекс
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Идентификатор страны
        /// </summary>
        public string CountryId { get; set; }

        /// <summary>
        /// Строение
        /// </summary>
        public string Building { get; set; }

        /// <summary>
        /// Корпус
        /// </summary>
        public string Frame { get; set; }

        /// <summary>
        /// Дом
        /// </summary>
        public string House { get; set; }

        /// <summary>
        /// Квартира
        /// </summary>
        public string Flat { get; set; }

        /// <summary>
        /// Код КЛАДР
        /// </summary>
        public string FiasCode { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Внутригородской район
        /// </summary>
        public string District { get; set; }

        /// <summary>
        /// Район
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// Поселение
        /// </summary>
        public string Settlement { get; set; }

        /// <summary>
        /// Доп. территория
        /// </summary>
        public string AdditionArea { get; set; }

        /// <summary>
        /// Улица на доп. территории
        /// </summary>
        public string AdditionAreaStreet { get; set; }

        /// <summary>
        /// Улица
        /// </summary>
        public string Street { get; set; }

        public bool EqualsWithoutId(Address addr)
        {
            return ZipCode == addr.ZipCode
                   && CountryId == addr.CountryId
                   && Building == addr.Building
                   && Frame == addr.Frame
                   && House == addr.House
                   && Flat == addr.Flat
                   && FiasCode == addr.FiasCode
                   && Region == addr.Region
                   && City == addr.City
                   && District == addr.District
                   && Area == addr.Area
                   && Settlement == addr.Settlement
                   && AdditionArea == addr.AdditionArea
                   && AdditionAreaStreet == addr.AdditionAreaStreet
                   && Street == addr.Street
                   && PartialAddress == addr.PartialAddress;
        }

        /// <summary>
        /// Строка адреса (не включая дом, строение, корпус, номер квартиры)
        /// </summary>
        public string PartialAddress { get; set; }

        /// <summary>
        /// Строка полного адреса
        /// </summary>
        [NotMapped]
        public string FullAddress
        {
            get
            {
                var sb = new StringBuilder(256);
                sb.Append(ZipCode);
                if (CountryId != null) sb.Append($", {CountryId}");
                if(PartialAddress != null) sb.Append($", {PartialAddress}");
                if (House != null) sb.Append($", д. {House}");
                if (Building != null) sb.Append($", стр. {Building}");
                if (Frame != null) sb.Append($", корп. {Frame}");
                if (Flat != null) sb.Append($", кв. {Flat}");
                return sb.ToString();
            }
        }
    }
}
