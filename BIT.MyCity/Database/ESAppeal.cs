namespace BIT.MyCity.Database
{
    public class ESAppeal
    {
        public long Id { get; set; }
        public string RejectionReason { get; set; }
        public string Fulltext { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Place { get; set; }
        public string Platform { get; set; }
        public string Address { get; set; }
        public string Region1 { get; set; }
        public string DeloText { get; set; }
        public ESAppeal(Appeal obj)
        {
            Id = obj.Id;
            RejectionReason = obj.RejectionReason;
            Fulltext = obj.Fulltext;
            Title = obj.Title;
            Description = obj.Description;
            Place = obj.Place;
            Platform = obj.Platform;
            Address = obj.Address;
            Region1 = obj.Region1;
            DeloText = obj.DeloText;
        }
    }
}
