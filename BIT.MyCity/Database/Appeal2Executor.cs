﻿namespace BIT.MyCity.Database
{
    public class Appeal2Executor
    {
        public long AppealId { get; set; }
        public Appeal Appeal { get; set; }
        public long AppealExecutorId { get; set; }
        public AppealExecutor AppealExecutor { get; set; }
    }

    public class Appeal2PrincipalExecutor
    {
        public long AppealId { get; set; }
        public Appeal Appeal { get; set; }
        public long AppealPrincipalExecutorId { get; set; }
        public AppealExecutor AppealPrincipalExecutor { get; set; }
    }
}
