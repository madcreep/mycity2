﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BIT.MyCity.Database
{
    public abstract class UserExecutorSubsystem : BaseLinkUser
    {
        public long SubsystemId { get; set; }
        public Subsystem Subsystem { get; set; }
        [NotMapped]
        public string SubsystemUid { get; set; }
    }
    public class ExecutorTopic : UserExecutorSubsystem
    {
        public long TopicId { get; set; }
        public Topic Topic { get; set; }
    }
    public class ExecutorRegion : UserExecutorSubsystem
    {
        public long RegionId { get; set; }
        public Region Region { get; set; }
    }
    public class ExecutorRubric : UserExecutorSubsystem
    {
        public long RubricId { get; set; }
        public Rubric Rubric { get; set; }
    }
    public class ExecutorTerritory : UserExecutorSubsystem
    {
        public long TerritoryId { get; set; }
        public Territory Territory { get; set; }
    }
}
