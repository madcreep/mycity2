﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BIT.MyCity.Database
{
    public class Officer : BaseEntity
    {
        public string Name { get; set; }
        public string ExtId { get; set; }
        public string Position { get; set; }
        public string Authority { get; set; }
        public string Place { get; set; }
        public Rubric Rubric { get; set; }
        public long? RubricId { get; set; }
        public List<AppointmentRange> Ranges { get; set; }
        [NotMapped]
        public List<long> RangesIds { get; set; }
        [NotMapped]
        public List<long> RangesIdsChange { get; set; }
        public Officer()
        {
            Type = nameof(Officer);
        }
    }
}
