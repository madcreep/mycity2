namespace BIT.MyCity.Database
{
    public class AttachedFilesToAppointments
    {
        public long AppointmentId { get; set; }

        public Appointment Appointment { get; set; }

        public long AttachedFileId { get; set; }

        public AttachedFile AttachedFile { get; set; }
    }
}
