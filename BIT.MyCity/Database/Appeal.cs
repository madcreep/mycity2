using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using BIT.MyCity.Attributes;
using NpgsqlTypes;

namespace BIT.MyCity.Database
{
    [ChangeJournalEntity("Обращение")]
    public class Appeal : BaseEntity, ICloneable, IHasAttachedFiles, IHasRubricators, IHasSubsystem, IHasMessages
    {
        //private readonly ILazyLoader _lazyLoader;

        /// <summary>
        /// Идентификатор обращения в системе
        /// из которой осуществен перенос
        /// </summary>
        [StringLength(256)]
        public string OldSystemId { get; set; }

        /*links*/
        #region IHasRubricators
        public Rubric Rubric { get; set; }
        [ChangeJournalProperty("Рубрика", "Name")]
        public long? RubricId { get; set; }

        public Territory Territory { get; set; }
        [ChangeJournalProperty("Территория", "Name")]
        public long? TerritoryId { get; set; }

        public Topic Topic { get; set; }
        [ChangeJournalProperty("Тема", "Name")]
        public long? TopicId { get; set; }

        public Region Region { get; set; }
        [ChangeJournalProperty("Регион", "Name")]
        public long? RegionId { get; set; }
        #endregion

        public UserOrganization Organization { get; set; }
        public long? OrganizationId { get; set; }

        public User Author { get; set; }
        public long AuthorId { get; set; }

        #region IHasAttachedFiles

        //private List<AttachedFilesToAppeals> _attachedFilesToAppeal;

        public List<AttachedFilesToAppeals> AttachedFilesToAppeal { get; set; }
        //{
        //    get => _attachedFilesToAppeal ?? _lazyLoader?.Load(this, ref _attachedFilesToAppeal);
        //    set => _attachedFilesToAppeal = value;
        //}

        private List<AttachedFile> _attachedFiles;

        [NotMapped]
        public List<AttachedFile> AttachedFiles
        {
            get => _attachedFiles
                   ?? AttachedFilesToAppeal?.Select(el => el.AttachedFile).ToList();

            set => _attachedFiles = value;
        }
        
        [NotMapped]
        public List<long> AttachedFilesIds { get; set; }
        //[NotMapped]
        //public List<long> AttachedFilesIdsChange { get; set; }

        public void AddFile(AttachedFile file)
        {
            AttachedFilesToAppeal.Add(new AttachedFilesToAppeals
            {
                AttachedFile = file
            });
        }

        #endregion
        #region moderators
        [NotMapped]
        public List<long> ModeratorsIds { get; set; } = new List<long>();
        [NotMapped]
        public List<long> ModeratorsIdsChange { get; set; } = new List<long>();
        [NotMapped]
        public List<User> Moderators => UserModeratorAppeal?.Select(el => el.User).ToList() ?? new List<User>();
        public List<UserModeratorAppeal> UserModeratorAppeal { get; set; }
        #endregion
        #region like
        public List<UserLikeAppeal> UserLikeAppeal { get; set; } = new List<UserLikeAppeal>();
        [NotMapped]
        public List<User> UsersLiked => UserLikeAppeal?.Select(el => el.User).ToList() ?? new List<User>();
        public int LikesCount { get; set; }
        public int DislikesCount { get; set; }
        [NotMapped]
        public LikeWIL LikeWIL {
            get {
                using var serviceScope = ServiceActivator.GetScope();
                var _um = (UserManager)serviceScope.ServiceProvider.GetService(typeof(UserManager));
                var result = new LikeWIL();
                var userId = _um.ClaimsPrincipalId();
                if (userId.HasValue)
                {
                    result.Liked = LikeGet(userId.Value);
                }
                return result;
            }
        }
        public LikeStateEnum LikeGet(long userId)
        {
            var a = UserLikeAppeal.Where(el => el.UserId == userId).FirstOrDefault();
            return a != null ? a.Liked : LikeStateEnum.NONE;
        }
        public LikeStateEnum LikeSet(LikeRequestEnum liked, User user)
        {
            var a = UserLikeAppeal.Where(el => el.UserId == user.Id).FirstOrDefault();
            if (a == null)
            {
                a = new UserLikeAppeal { Appeal = this, AppealId = Id, User = user, UserId = user.Id };
                UserLikeAppeal.Add(a);
            }
            a.Liked = BaseConstants.LikeProcess(liked, a.Liked);
            LikesCount = UserLikeAppeal.Count(el => el.Liked == LikeStateEnum.LIKED);
            DislikesCount = UserLikeAppeal.Count(el => el.Liked == LikeStateEnum.DISLIKED);
            return a.Liked;
        }
        #endregion
        #region view
        public List<UserViewAppeal> UserViewAppeal { get; set; } = new List<UserViewAppeal>();
        [NotMapped]
        public List<User> UsersViewed => UserViewAppeal?.Select(el => el.User).ToList() ?? new List<User>();

        public int ViewsCount { get; set; }
        [NotMapped]
        public ViewWIV ViewWIV {
            get {
                using var serviceScope = ServiceActivator.GetScope();
                var _um = (UserManager)serviceScope.ServiceProvider.GetService(typeof(UserManager));
                var result = new ViewWIV();
                var userId = _um.ClaimsPrincipalId();
                if (userId == null)
                {
                    return result;
                }
                var userView = UserViewAppeal.FirstOrDefault(el => el.UserId == userId);

                if (userView == null)
                {
                    return result;
                }

                result.Viewed = userView.Viewed;
                result.CreatedAt = userView.CreatedAt;
                result.UpdatedAt = userView.UpdatedAt;
                return result;
            }
        }
        public bool ViewGet(long userId)
        {
            var a = UserViewAppeal.FirstOrDefault(el => el.UserId == userId);
            return a is { Viewed: true };
        }
        public UserViewAppeal ViewSet(bool viewed, User user)
        {
            var viewAppeal = UserViewAppeal.FirstOrDefault(el => el.UserId == user.Id);
            if (viewAppeal == null)
            {
                viewAppeal = new UserViewAppeal { Appeal = this, AppealId = Id, User = user, UserId = user.Id };
                UserViewAppeal.Add(viewAppeal);
            }
            viewAppeal.Viewed = viewed;
            ViewsCount = UserViewAppeal.Count(el => el.Viewed);
            return viewAppeal;
        }
        #endregion
        #region appealExecutors
        [NotMapped]
        public List<long> AppealExecutorIds { get; set; } = new List<long>();
        [NotMapped]
        public List<long> AppealExecutorIdsChange { get; set; } = new List<long>();
        [NotMapped]
        public List<AppealExecutor> AppealExecutors => Appeal2Executors?.Select(el => el.AppealExecutor).ToList() ?? new List<AppealExecutor>();
        public List<Appeal2Executor> Appeal2Executors { get; set; } = new List<Appeal2Executor>();
        #endregion
        #region AppealPrincipalExecutors
        [NotMapped]
        public List<long> AppealPrincipalExecutorIds { get; set; } = new List<long>();
        [NotMapped]
        public List<long> AppealPrincipalExecutorIdsChange { get; set; } = new List<long>();
        [NotMapped]
        public List<AppealExecutor> AppealPrincipalExecutors => Appeal2PrincipalExecutors?.Select(el => el.AppealPrincipalExecutor).ToList() ?? new List<AppealExecutor>();
        public List<Appeal2PrincipalExecutor> Appeal2PrincipalExecutors { get; set; } = new List<Appeal2PrincipalExecutor>();
        #endregion
        
        public List<Message> Messages { get; set; }
        #region IHasSubsystem
        public Subsystem Subsystem { get; set; }

        [ForeignKey("Subsystem")]
        public long? SubsystemId1 { get; set; }
        #endregion
        [NotMapped]
        public string SubsystemId { get; set; }

        /*fields*/
        public RegardModeEnum RegardMode { get; set; } = RegardModeEnum.DISABLED;
        public bool Public_ { get; set; }

        [ChangeJournalProperty("Публикация")]
        public ApprovedEnum Approved { get; set; } = ApprovedEnum.UNDEFINED;

        [ChangeJournalProperty("Отказ в публикации")]
        public string RejectionReason { get; set; }
        public ModerationStageEnum ModerationStage { get; set; } = ModerationStageEnum.WAIT;
        public bool PublicGranted { get; set; }
        public string Signatory { get; set; }
        public string Post { get; set; }
        public string RegNumber { get; set; }

        public DateTime? RegDate { get; set; }
        public DateTime? ExtDate { get; set; }
        public DateTime? ResDate { get; set; }
        public DateTime? PlanDate { get; set; }
        public DateTime? FactDate { get; set; }
        public DateTime? FilesPush { get; set; }
        public bool WORegNumberRegDate { get; set; }
        
        #region place
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Place { get; set; }
        #endregion
        public AppealTypeEnum AppealType { get; set; }
        public SubscribeToAnswersEnum SubscribeToAnswers { get; set; } = SubscribeToAnswersEnum.Not_Subscribed;

        public string Fulltext { get; set; }
        public string Title { get; set; }
        public string ExtId { get; set; }
        public bool NeedToUpdateInExt { get; set; }
        public string ExtNumber { get; set; }
        public string Addressee { get; set; }
        public string AuthorName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Patronim { get; set; }
        public string Description { get; set; }
        public bool TopicUpdated { get; set; }

        [ChangeJournalAppealStatus("Статус")]
        public int SiteStatus { get; set; }
        public bool MarkSend { get; set; }
        public bool StatusConfirmed { get; set; }
        public bool PushesSent { get; set; }
        public int Number { get; set; }
        public string Platform { get; set; } = "Web";
        public bool SentToDelo { get; set; }
        public bool? AnswerByPost { get; set; }
        public bool? ExternalExec { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string Region1 { get; set; }
        public string Phone { get; set; }
        public string Municipality { get; set; }
        public string Email { get; set; }
        public int Views { get; set; }
        public int UnapprovedComments { get; set; }
        public bool? Uploaded { get; set; }
        public int ClientType { get; set; }
        public string Inn { get; set; }
        public string DeloText { get; set; }
        public string DeloStatus { get; set; }
        public bool? Collective { get; set; }

        [NotMapped]
        public bool HasUnapprovedMessages { get; set; }
        [NotMapped]
        public List<Message> UnapprovedMessages { get; set; } = new List<Message>();
        [NotMapped]
        public int UnapprovedMessagesCount { get; set; }
        [NotMapped]
        public int MessagesCount { get; set; }

        public NpgsqlTsVector SearchVector { get; set; }

        //public Appeal(ILazyLoader lazyLoader)
        //    : this()
        //{
        //    _lazyLoader = lazyLoader;
        //}

        public Appeal()
        {
            Type = nameof(Appeal);
        }
        public object Clone()
        {
            var clone = (Appeal)MemberwiseClone();
            if (AttachedFiles != null)
            {
                clone.AttachedFiles = new List<AttachedFile>();
                clone.AttachedFiles.AddRange(AttachedFiles);
            }
            if (UserModeratorAppeal!=null)
            {
                clone.UserModeratorAppeal = new List<UserModeratorAppeal>();
                clone.UserModeratorAppeal.AddRange(UserModeratorAppeal);
            }
            if (UserLikeAppeal != null)
            {
                clone.UserLikeAppeal = new List<UserLikeAppeal>();
                clone.UserLikeAppeal.AddRange(UserLikeAppeal);
            }
            if (Appeal2Executors != null)
            {
                clone.Appeal2Executors = new List<Appeal2Executor>();
                clone.Appeal2Executors.AddRange(Appeal2Executors);
            }
            if (Appeal2PrincipalExecutors != null)
            {
                clone.Appeal2PrincipalExecutors = new List<Appeal2PrincipalExecutor>();
                clone.Appeal2PrincipalExecutors.AddRange(Appeal2PrincipalExecutors);
            }
            return clone;
        }
    }
}
