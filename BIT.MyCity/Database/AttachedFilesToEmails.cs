using Microsoft.EntityFrameworkCore.Infrastructure;

namespace BIT.MyCity.Database
{
    public class AttachedFilesToEmails
    {
        private readonly ILazyLoader _lazyLoader;

        public AttachedFilesToEmails()
        { }

        public AttachedFilesToEmails(ILazyLoader lazyLoader)
        {
            _lazyLoader = lazyLoader;
        }

        public long EmailId { get; set; }

        private Email _email;

        public Email Email
        {
            get => _email ?? _lazyLoader.Load(this, ref _email);
            set => _email = value;
        }

        public long AttachedFileId { get; set; }

        private AttachedFile _attachedFile;

        public AttachedFile AttachedFile
        {
            get => _attachedFile ?? _lazyLoader.Load(this, ref _attachedFile);
            set => _attachedFile = value;
        }
    }
}
