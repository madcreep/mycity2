﻿using BIT.MyCity.Model;
using System;

namespace BIT.MyCity.Database
{
    public abstract class UserLike : IHasLikeUserList, IUpdatedEntity
    {
        public LikeStateEnum Liked { get; set; } = LikeStateEnum.NONE;
        public long UserId { get; set; }
        public User User { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
    public class UserLikeAppeal : UserLike
    {
        public long AppealId { get; set; }
        public Appeal Appeal { get; set; }
    }
    public class UserLikeMessage : UserLike
    {
        public long MessageId { get; set; }
        public Message Message { get; set; }
    }
    public class UserLikeNews : UserLike
    {
        public long NewsId { get; set; }
        public News News { get; set; }
    }
}
