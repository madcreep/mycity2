namespace BIT.MyCity.Database
{
    public enum ModerationStageEnum
    {
        WAIT = 0,
        INPROGRESS = 1,
        DONE = 2
    }
}
