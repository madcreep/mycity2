using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Model;

namespace BIT.MyCity.Database
{
    public class Phone
    {
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public TypePhone Type { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Добавочный номер
        /// </summary>
        public string Additional { get; set; }

        public List<OrganizationUserPhone> OrganizationUserPhones { get; set; }

        [NotMapped]
        public List<User> Users => OrganizationUserPhones?
            .Where(el => el.User != null)
            .Select(el => el.User)
            .ToList();

        [NotMapped]
        public List<UserOrganization> UserOrganizations => OrganizationUserPhones?
            .Where(el => el.UserOrganization != null)
            .Select(el => el.UserOrganization)
            .ToList();

        [NotMapped]
        public bool Confirmed { get; set; }
    }
}
