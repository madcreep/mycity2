using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;

namespace BIT.MyCity.Database
{
    [Table("roles")]
    public class Role : IdentityRole<long>, IUpdatedEntity, IDisconnectableEntity
    {
        [StringLength(255)]
        public string Description { get; set; }

        public bool Editable { get; set; } = true;

        public bool IsAdminRegister { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public DateTime? DisconnectedTimestamp { get; set; }

        public bool Disconnected
        {
            get => DisconnectedTimestamp != null;
            set
            {
                if (!value)
                {
                    DisconnectedTimestamp = null;

                    return;
                }

                if (!DisconnectedTimestamp.HasValue)
                {
                    DisconnectedTimestamp = DateTime.UtcNow;
                }
            }
        }

        public virtual List<UserRole> UserRole { get; set; }

        [NotMapped] public List<User> Users => UserRole?.Select(el => el.User).ToList() ?? new List<User>();

        public List<IdentityRoleClaim<long>> IdentityClaims { get; set; }

        [NotMapped]
        public List<Claim> Claims
        {
            get => IdentityClaims?.Select(el => new Claim(el.ClaimType, el.ClaimValue))
                .ToList();
            set
            {
                IdentityClaims = value?.Select(el => new IdentityRoleClaim<long>
                                     {
                                         ClaimType = el.Type,
                                         ClaimValue = el.Value
                                     })
                                     .ToList()
                                 ?? new List<IdentityRoleClaim<long>>();
            }
        }

        public int Weight { get; set; }
    }
}
