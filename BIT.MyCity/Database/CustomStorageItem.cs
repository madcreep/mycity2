using System.ComponentModel.DataAnnotations;

namespace BIT.MyCity.Database
{
    public class CustomStorageItem
    {
        [StringLength(256)]
        public  string Key { get; set; }
        
        [StringLength(256)]
        public string OwnerName { get; set; }

        [StringLength(256)]
        public string OwnerId { get; set; }

        public int Weight { get; set; }

        [StringLength(2048)]
        public string Value { get; set; }
    }
}
