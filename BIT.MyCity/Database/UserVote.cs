﻿using System.Collections.Generic;

namespace BIT.MyCity.Database
{
    public class UserVote : BaseLinkUser
    {
        public long VoteId { get; set; }
        public Vote Vote { get; set; }
        public List<long> VotepunktIds { get; set; } = new List<long>();
        public string Comment { get; set; }
    }
}
