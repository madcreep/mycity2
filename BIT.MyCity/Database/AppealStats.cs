using System;

namespace BIT.MyCity.Database
{
    public class AppealStats : IHasRubricators, IHasSubsystem
    {
        public int AppealsCount { get; set; }
        public long? SubsystemId { get; set; }
        public Subsystem Subsystem { get; set; }
        public Rubric Rubric { get; set; }
        public long? RubricId { get; set; }
        public Territory Territory { get; set; }
        public long? TerritoryId { get; set; }
        public Topic Topic { get; set; }
        public long? TopicId { get; set; }
        public Region Region { get; set; }
        public long? RegionId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public bool Public { get; set; }
        public bool Deleted { get; set; }
        public int SiteStatus { get; set; }
        public bool SentToDelo { get; set; }
        public ApprovedEnum Approved { get; set; }
    }
}
