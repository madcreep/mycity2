﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BIT.MyCity.Database
{
    public class AppointmentRange : BaseWPEntity
    {
        public DateTime? Date { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public int? Appointments { get; set; }
        public string OfficerName { get; set; }
        public int? RubricOrder { get; set; }
        public string RubricName { get; set; }
        public Officer Officer { get; set; }
        public long? OfficerId { get; set; }
        public List<Appointment> RegisteredAppointments { get; set; }
        [NotMapped]
        public List<long> RegisteredAppointmentsIds { get; set; }
        [NotMapped]
        public List<long> RegisteredAppointmentsIdsChange { get; set; }
        public bool? DeleteMark { get; set; }

        public AppointmentRange()
        {
            Type = nameof(AppointmentRange);
        }
    }
}
