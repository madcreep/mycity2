using BIT.MyCity.Managers;
using BIT.MyCity.Services;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Database
{
    public class AttachedFile : BaseEntity, ICloneable
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string ExtId { get; set; }
        public bool Private { get; set; }
        public bool IsPublic { get; set; } = false;
        public string StorageName { get; set; }
        public string StoragePath { get; set; }
        public long Len { get; set; }
        public bool Incomplete { get; set; } = true;
        public long? UploadedById { get; set; }
        public User UploadedBy { get; set; }
        public bool Sequre { get; set; }
        public string ContentType { get; set; }

        public SKEncodedImageFormat Format { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        [NotMapped]
        public string Thumb
        {
            get
            {
                if (ContentType == null || !ContentType.StartsWith("image"))
                {
                    return null;
                }

                using var serviceScope = ServiceActivator.GetScope();
                var m = (AttachedFileManager)serviceScope.ServiceProvider.GetService(typeof(AttachedFileManager));
                return m.GetThumbCached(Id).Result;
            }
        }
        public List<ThumbNail> Thumbs { get; set; } = new List<ThumbNail>();
        
        #region parent
        //public long? NewsId { get; set; }
        //public News News { get; set; }
        //public long? VoteRootId { get; set; }
        //public VoteRoot VoteRoot { get; set; }
        ////public long? VoteId { get; set; }
        //public Vote Vote { get; set; }
        //public long? VoteItemId { get; set; }
        //public VoteItem VoteItem { get; set; }
        //public long? MessageId { get; set; }
        //public Message Message { get; set; }
        //public long? AppointmentId { get; set; }
        //public Appointment Appointment { get; set; }
        //public long? AppealId { get; set; }
        //public Appeal Appeal { get; set; }

        [NotMapped]
        public long parentId_;
        [NotMapped]
        public long ParentId
        {
            get
            {
                return parentId_;
                //var a = NewsId ?? VoteId ?? VoteItemId ?? VoteRootId ?? MessageId ?? AppointmentId ?? AppealId;
                //return a ?? 0;
            }
            set => parentId_ = value;
        }

        [NotMapped]
        public string parentType_;
        [NotMapped]
        public string ParentType
        {
            get
            {
                return parentType_;
                //if (NewsId.HasValue) return nameof(News);
                //else if (VoteRootId.HasValue) return nameof(VoteRoot);
                //else if (VoteId.HasValue) return nameof(Vote);
                //else if (VoteItemId.HasValue) return nameof(VoteItem);
                //else if (MessageId.HasValue) return nameof(Message);
                //else if (AppointmentId.HasValue) return nameof(Appointment);
                //else if (AppealId.HasValue) return nameof(Appeal);
                //else return null;
            }
            set => parentType_ = value;
        }

        //public async Task ParentClear(DocumentsContext ctx)
        //{
        //    if (AttachedFilesToEntity == null)
        //    {
        //        await ctx.Entry(this)
        //            .Reference(el => el.AttachedFilesToEntity)
        //            .LoadAsync();
        //    }

        //    var ent = (IHasAttachedFiles)ctx.Entry()


        //    if (NewsId.HasValue)
        //    {
        //        if (News == null)
        //        {
        //            await ctx.Entry(this)
        //             .Reference(el => el.News)
        //             .Query()
        //             .Include(el => el.AttachedFiles)
        //             .LoadAsync();
        //        }

        //        News?.AttachedFiles?.RemoveAll(el => el == this);
        //    }
        //    //if (VoteRootId.HasValue)
        //    //{
        //    //    if (VoteRoot == null) await ctx.Entry(this).Reference(el => el.VoteRoot).Query().Include(el => el.AttachedFiles).LoadAsync();
        //    //    VoteRoot?.AttachedFiles?.RemoveAll(el => el == this);
        //    //}
        //    //if (VoteId.HasValue)
        //    //{
        //    //    if (Vote == null) await ctx.Entry(this).Reference(el => el.Vote).Query().Include(el => el.AttachedFiles).LoadAsync();
        //    //    Vote?.AttachedFiles?.RemoveAll(el => el == this);
        //    //}
        //    //if (VoteItemId.HasValue)
        //    //{
        //    //    if (VoteItem == null) await ctx.Entry(this).Reference(el => el.VoteItem).Query().Include(el => el.AttachedFiles).LoadAsync();
        //    //    VoteItem?.AttachedFiles?.RemoveAll(el => el == this);
        //    //}
        //    //if (MessageId.HasValue)
        //    //{
        //    //    if (Message == null) await ctx.Entry(this).Reference(el => el.Message).Query().Include(el => el.AttachedFiles).LoadAsync();
        //    //    Message?.AttachedFiles?.RemoveAll(el => el == this);
        //    //}
        //    //if (AppointmentId.HasValue)
        //    //{
        //    //    if (Appointment == null) await ctx.Entry(this).Reference(el => el.Appointment).Query().Include(el => el.AttachedFiles).LoadAsync();
        //    //    Appointment?.AttachedFiles?.RemoveAll(el => el == this);
        //    //}
        //    //if (AppealId.HasValue)
        //    //{
        //    //    if (Appeal == null) await ctx.Entry(this).Reference(el => el.Appeal).Query().Include(el => el.AttachedFiles).LoadAsync();
        //    //    Appeal?.AttachedFiles?.RemoveAll(el => el == this);
        //    //}
        //}

        public async Task ParentSet(IBaseIdEntity parent, DocumentsContext ctx)
        {
            if (parent != null)
            {
                await ParentSet(parent.Id, parent.Type, ctx);
            }
        }
        public async Task ParentSet(long id, string type, DocumentsContext ctx)
        {
            var propertyInfo = ctx
                .GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .FirstOrDefault(el => el.PropertyType.GenericTypeArguments.Length == 1
                                      && el.PropertyType.GenericTypeArguments[0].GetInterfaces().Contains(typeof(IHasAttachedFiles))
                                      && el.PropertyType.GenericTypeArguments[0].Name == type);

            var q = (IQueryable<IHasAttachedFiles>)propertyInfo?.GetValue(ctx);

            if (q != null)
            {
                var obj = await q?
                    .Where(el => ((IBaseIdEntity)el).Id == id)
                    .FirstOrDefaultAsync();
                
                obj?.AddFile(this);
            }
        }
        #endregion
        public AttachedFile()
        {
            Type = nameof(AttachedFile);
        }
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
