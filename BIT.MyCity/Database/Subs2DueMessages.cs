﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BIT.MyCity.Database
{
    public class Subs2DueMessages
    {
        public string RootType { get; set; }
        public string Due { get; set; }
        public int Level { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
    }
}
