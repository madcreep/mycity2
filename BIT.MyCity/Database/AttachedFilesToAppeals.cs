using Microsoft.EntityFrameworkCore.Infrastructure;

namespace BIT.MyCity.Database
{
    public class AttachedFilesToAppeals
    {
        //private readonly ILazyLoader _lazyLoader;

        public AttachedFilesToAppeals()
        {}

        //public AttachedFilesToAppeals(ILazyLoader lazyLoader)
        //{
        //    _lazyLoader = lazyLoader;
        //}

        public long AppealId { get; set; }

        //private Appeal _appeal;

        public Appeal Appeal { get; set; }
        //{
        //    get => _appeal ?? _lazyLoader.Load(this, ref _appeal);
        //    set => _appeal = value;
        //}
        
        public long AttachedFileId { get; set; }

        //private AttachedFile _attachedFile;

        public AttachedFile AttachedFile { get; set; }
        //{
        //    get => _attachedFile ?? _lazyLoader.Load(this, ref _attachedFile);
        //    set => _attachedFile = value;
        //}
    }
}
