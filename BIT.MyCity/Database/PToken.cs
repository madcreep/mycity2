﻿using System;

namespace BIT.MyCity.Database
{
    public class PToken : IUpdatedEntity
    {
        public long Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? ExpiredAt { get; set; }
        public string Value { get; set; }
        public string DeviceType { get; set; }
        public long OwnerId { get; set; }
        public User Owner { get; set; }
    }
}
