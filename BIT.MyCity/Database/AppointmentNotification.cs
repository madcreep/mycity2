﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BIT.MyCity.Database
{
    public class AppointmentNotification : BaseWPEntity
    {
        public long? AppointmentId { get; set; }
        public Appointment Appointment { get; set; }
        public DateTime? Date { get; set; }
        public int? Status { get; set; }
        public string Html { get; set; }

        public AppointmentNotification()
        {
            Type = nameof(AppointmentNotification);
        }
    }
}
