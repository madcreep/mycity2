﻿using System;

namespace BIT.MyCity.Database
{
    public abstract class UserView : IHasViewUserList, IUpdatedEntity
    {
        public bool Viewed { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
    public class UserViewAppeal : UserView
    {
        public long AppealId { get; set; }
        public Appeal Appeal { get; set; }
    }
    public class UserViewMessage : UserView
    {
        public long MessageId { get; set; }
        public Message Message { get; set; }
    }
    public class UserViewNews : UserView
    {
        public long NewsId { get; set; }
        public News News { get; set; }
    }
}
