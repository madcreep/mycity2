using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace BIT.MyCity.Database
{
    public enum NewsStateEnum
    {
        DRAFT = 0,
        MODERATE = 1,
        PUBLISHED = 2,
        UNPUBLISHED = 3,
    }

    public class News : BaseEntity, ICloneable, IHasAttachedFiles, IHasRubricators, IHasMessages
    {
        #region IHasAttachedFiles
        public List<AttachedFilesToNews> AttachedFilesToNews { get; set; }

        [NotMapped]
        public List<AttachedFile> AttachedFiles { get; set; } = new List<AttachedFile>();
        [NotMapped]
        public List<long> AttachedFilesIds { get; set; } = new List<long>();
        //[NotMapped]
        //public List<long> AttachedFilesIdsChange { get; set; } = new List<long>();

        public void AddFile(AttachedFile file)
        {
            AttachedFilesToNews.Add(new AttachedFilesToNews
            {
                AttachedFile = file
            });
        }

        #endregion
        #region IHasRubricators
        public Rubric Rubric { get; set; }
        public long? RubricId { get; set; }
        public Territory Territory { get; set; }
        public long? TerritoryId { get; set; }
        public Topic Topic { get; set; }
        public long? TopicId { get; set; }
        public Region Region { get; set; }
        public long? RegionId { get; set; }
        #endregion
        #region like
        public List<UserLikeNews> UserLike { get; set; } = new List<UserLikeNews>();
        [NotMapped]
        public List<User> UsersLiked => UserLike?.Select(el => el.User).ToList() ?? new List<User>();
        public int LikesCount { get; set; }
        public int DislikesCount { get; set; }
        [NotMapped]
        public LikeWIL LikeWIL {
            get {
                using var serviceScope = ServiceActivator.GetScope();
                var _um = (UserManager)serviceScope.ServiceProvider.GetService(typeof(UserManager));
                LikeWIL result = new LikeWIL();
                var userId = _um.ClaimsPrincipalId();
                if (userId.HasValue)
                {
                    result.Liked = LikeGet(userId.Value);
                }
                return result;
            }
        }
        public LikeStateEnum LikeGet(long userId)
        {
            var a = UserLike.Where(el => el.UserId == userId).FirstOrDefault();
            return a != null ? a.Liked : LikeStateEnum.NONE;
        }
        public LikeStateEnum LikeSet(LikeRequestEnum liked, User user)
        {
            var a = UserLike.Where(el => el.UserId == user.Id).FirstOrDefault();
            if (a == null)
            {
                a = new UserLikeNews { News = this, NewsId = Id, User = user, UserId = user.Id };
                UserLike.Add(a);
            }
            a.Liked = BaseConstants.LikeProcess(liked, a.Liked);
            LikesCount = UserLike.Where(el => el.Liked == LikeStateEnum.LIKED).Count();
            DislikesCount = UserLike.Where(el => el.Liked == LikeStateEnum.DISLIKED).Count();
            return a.Liked;
        }
        #endregion
        #region view
        public List<UserViewNews> UserView { get; set; } = new List<UserViewNews>();
        [NotMapped]
        public List<User> UsersViewed => UserView?.Select(el => el.User).ToList() ?? new List<User>();

        public int ViewsCount { get; set; }
        [NotMapped]
        public ViewWIV ViewWIV {
            get {
                using var serviceScope = ServiceActivator.GetScope();
                var _um = (UserManager)serviceScope.ServiceProvider.GetService(typeof(UserManager));
                ViewWIV result = new ViewWIV();
                var userId = _um.ClaimsPrincipalId();
                if (userId != null)
                {
                    var userView = UserView.Where(el => el.UserId == userId).FirstOrDefault();
                    if (userView != null)
                    {
                        result.Viewed = userView.Viewed;
                        result.CreatedAt = userView.CreatedAt;
                        result.UpdatedAt = userView.UpdatedAt;
                    }
                }
                return result;
            }
        }
        public bool ViewGet(long userId)
        {
            var a = UserView.Where(el => el.UserId == userId).FirstOrDefault();
            return a != null ? a.Viewed : false;
        }
        public UserViewNews ViewSet(bool viewed, User user)
        {
            var a = UserView.Where(el => el.UserId == user.Id).FirstOrDefault();
            if (a == null)
            {
                a = new UserViewNews { News = this, NewsId = Id, User = user, UserId = user.Id };
                UserView.Add(a);
            }
            a.Viewed = viewed;
            ViewsCount = UserView.Where(el => el.Viewed).Count();
            return a;
        }
        #endregion

        public User Author { get; set; }

        public long AuthorId { get; set; }

        public bool CommentsAllowed { get; set; }

        public NewsStateEnum State { get; set; } = NewsStateEnum.DRAFT;

        public SubscribeToAnswersEnum SubscribeToAnswers { get; set; } = SubscribeToAnswersEnum.Not_Subscribed;

        public string Name { get; set; }

        public string ShortName { get; set; }

        public string Text { get; set; }

        public string ShortText { get; set; }

        public List<Message> Messages { get; set; }

        public News()
        {
            Type = nameof(News);
        }

        public object Clone()
        {
            var clone = (News)MemberwiseClone();

            clone.AttachedFiles = new List<AttachedFile>();

            clone.AttachedFiles.AddRange(AttachedFiles);

            clone.UserLike = new List<UserLikeNews>();

            clone.UserLike.AddRange(UserLike);

            return clone;
        }
    }
    public class ESNews
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Text { get; set; }
        public ESNews(News obj)
        {
            Id = obj.Id;
            Name = obj.Name;
            ShortName = obj.ShortName;
            Text = obj.Text;
        }
    }
}
