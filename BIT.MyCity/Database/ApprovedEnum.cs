using BIT.MyCity.Attributes;

namespace BIT.MyCity.Database
{
    public enum ApprovedEnum
    {
        [MapNameEnum("Не рассмотрено")]
        UNDEFINED = 0,
        [MapNameEnum("Одобрено")]
        ACCEPTED = 1,
        [MapNameEnum("Отклонено")]
        REJECTED = 2
    }
}
