using System;
using System.ComponentModel.DataAnnotations.Schema;
using BIT.MyCity.ApiInterface.Models;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Model;

namespace BIT.MyCity.Database
{
    public class User : IdentityUser<long>, IDisconnectableEntity, IUpdatedEntity
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public string NormalizeFullName { get; set; }

        public LoginSystem LoginSystem { get; set; }

        /// <summary>
        /// Признак пользователя самостоятельно зарегистрированного через формы
        /// </summary>
        public bool SelfFormRegister { get; set; }

        /// <summary>
        /// Идентификатор сообщения подтверждения Email
        /// </summary>
        public long? ConfirmEmailMailId { get; set; }

        /// <summary>
        /// Идентификатор сообщения на сброс пароля
        /// </summary>
        public long? RestorePasswordMailId { get; set; }

        public bool ProfileConfirmed { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public DateTime? DisconnectedTimestamp { get; set; }

        public bool Disconnected
        {
            get => DisconnectedTimestamp != null;
            set
            {
                if (value)
                {
                    if (!DisconnectedTimestamp.HasValue)
                    {
                        DisconnectedTimestamp = DateTime.UtcNow;
                    }

                    return;
                }

                DisconnectedTimestamp = null;
            }
        }

        public Gender? Gender { get; set; }

        public string Inn { get; set; }

        public string Snils { get; set; }

        /// <summary>
        /// Идентификатор пользователя во внешней системе
        /// </summary>
        [StringLength(256)]
        public string ExternalId { get; set; }

        /// <summary>
        /// Идентификатор пользователя в системе
        /// из которой осуществен перенос
        /// </summary>
        [StringLength(256)]
        public string OldSystemId { get; set; }

        #region Masked Fields

        private new string PhoneNumber { get; set; }

        private new bool PhoneNumberConfirmed { get; set; }

        private new int AccessFailedCount { get; set; }

        private new bool LockoutEnabled { get; set; }

        private new DateTimeOffset? LockoutEnd { get; set; }

        private new bool TwoFactorEnabled { get; set; }

        #endregion

        #region Addresses

        public List<Address> Addresses { get; set; } = new List<Address>();

        #endregion

        #region Rubrics

        public List<UserRubric> UserRubric { get; set; } = new List<UserRubric>();
        public List<ExecutorRubric> ExecutorRubric { get; set; } = new List<ExecutorRubric>();
        [NotMapped]
        public List<UserSubsystemDirectoryId> RubricIds 
        {    
            get 
            {
                if (UserRubric == null || !UserRubric.Any())
                {
                    return new List<UserSubsystemDirectoryId>();
                }

                UserRubric
                    .ForEach(el => el.SubsystemUid = el.Subsystem?.UID ?? el.SubsystemUid);

                return UserRubric
                    .GroupBy(el => el.SubsystemUid)
                    .Select(el => new UserSubsystemDirectoryId
                    {
                        SubsystemId = el.FirstOrDefault(el => el.Subsystem != null)?.Subsystem.Id ?? 0,
                        SubsystemUid = el.Key,
                        Ids = el.Select(x => x.RubricId).ToArray()
                    })
                    .ToList();
            }
            set =>
                UserRubric = value == null
                    ? new List<UserRubric>()
                    :(from item in value
                       from id in item.Ids ?? new long[0]
                       select new UserRubric
                       {
                           RubricId = id,
                           SubsystemId = item.SubsystemId ?? 0,
                           SubsystemUid = item.SubsystemUid
                       }).ToList();
        }
        //[NotMapped]
        //public List<Rubric> Rubrics => UserRubric?.Select(el => el.Rubric).ToList() ?? new List<Rubric>(); 
        #endregion

        #region Territories
        public List<UserTerritory> UserTerritory { get; set; } = new List<UserTerritory>();
        public List<ExecutorTerritory> ExecutorTerritory { get; set; } = new List<ExecutorTerritory>();
        [NotMapped]
        public List<UserSubsystemDirectoryId> TerritoryIds
        {
            get
            {
                if (UserTerritory == null || !UserTerritory.Any())
                {
                    return new List<UserSubsystemDirectoryId>();
                }

                UserTerritory
                    .ForEach(el => el.SubsystemUid = el.Subsystem?.UID ?? el.SubsystemUid);

                return UserTerritory
                    .GroupBy(el => el.SubsystemUid)
                    .Select(el => new UserSubsystemDirectoryId
                    {
                        SubsystemId = el.FirstOrDefault(el => el.Subsystem != null)?.Subsystem.Id ?? 0,
                        SubsystemUid = el.Key,
                        Ids = el.Select(x => x.TerritoryId).ToArray()
                    })
                    .ToList();
            }
            set =>
                UserTerritory = value == null
                    ? new List<UserTerritory>()
                    : (from item in value
                        from id in item.Ids ?? new long[0]
                       select new UserTerritory
                        {
                            TerritoryId = id,
                            SubsystemId = item.SubsystemId ?? 0,
                            SubsystemUid = item.SubsystemUid
                        }).ToList();
        }
        //[NotMapped]
        //public List<Territory> Territories => UserTerritory?.Select(el => el.Territory).ToList() ?? new List<Territory>(); 
        #endregion

        #region Regions
        public List<UserRegion> UserRegion { get; set; } = new List<UserRegion>();
        public List<ExecutorRegion> ExecutorRegion { get; set; } = new List<ExecutorRegion>();
        [NotMapped]
        public List<UserSubsystemDirectoryId> RegionIds
        {
            get
            {
                if (UserRegion == null || !UserRegion.Any())
                {
                    return new List<UserSubsystemDirectoryId>();
                }

                UserRegion
                    .ForEach(el => el.SubsystemUid = el.Subsystem?.UID ?? el.SubsystemUid);

                return UserRegion
                    .GroupBy(el => el.SubsystemUid)
                    .Select(el => new UserSubsystemDirectoryId
                    {
                        SubsystemId = el.FirstOrDefault(el => el.Subsystem != null)?.Subsystem.Id ?? 0,
                        SubsystemUid = el.Key,
                        Ids = el.Select(x => x.RegionId).ToArray()
                    })
                    .ToList();
            }
            set =>
                UserRegion = value == null
                    ? new List<UserRegion>()
                    : (from item in value
                        from id in item.Ids ?? new long[0]
                       select new UserRegion
                        {
                            RegionId = id,
                            SubsystemId = item.SubsystemId ?? 0,
                            SubsystemUid = item.SubsystemUid
                        }).ToList();
        }
        //[NotMapped]
        //public List<Region> Regions => UserRegion?.Select(el => el.Region).ToList() ?? new List<Region>(); 
        #endregion

        #region Topics
        public List<UserModeratorTopic> UserModeratorTopic { get; set; } = new List<UserModeratorTopic>();
        public List<ExecutorTopic> ExecutorTopic { get; set; } = new List<ExecutorTopic>();
        [NotMapped]
        public List<UserSubsystemDirectoryId> TopicIds
        {
            get
            {
                if (UserModeratorTopic == null || !UserModeratorTopic.Any())
                {
                    return new List<UserSubsystemDirectoryId>();
                }

                UserModeratorTopic
                    .ForEach(el => el.SubsystemUid = el.Subsystem?.UID ?? el.SubsystemUid);

                return UserModeratorTopic
                        .GroupBy(el => el.SubsystemUid)
                        .Select(el => new UserSubsystemDirectoryId
                        {
                            SubsystemId = el.FirstOrDefault(el=>el.Subsystem != null)?.Subsystem.Id ?? 0,
                            SubsystemUid = el.Key,
                            Ids = el.Select(x => x.TopicId).ToArray()
                        })
                        .ToList();
            }
            set => UserModeratorTopic = value == null
                ? new List<UserModeratorTopic>()
                : (from item in value
                    from id in item.Ids ?? new long[0]
                   select new UserModeratorTopic
                    {
                        TopicId = id,
                        SubsystemId = item.SubsystemId ?? 0,
                        SubsystemUid = item.SubsystemUid
                    })
                .ToList();
        }

        #endregion

        #region Messages
        public List<Message> Messages { get; set; }
        //[NotMapped]
        //public List<Message> Messages => Messages?.Select(el => el.Message).ToList() ?? new List<Message>();

        public List<UserRegardMessage> RegardMessagesLink { get; set; }
        public List<UserViewMessage> ViewMessagesLink { get; set; }
        //[NotMapped]
        //public List<Message> ViewMessages => ViewMessagesLink?.Select(el => el.Message).ToList();


        public List<UserLikeMessage> LikeMessagesLink { get; set; }
        //[NotMapped]
        //public List<Message> LikeMessages => LikeMessagesLink?.Select(el => el.Message).ToList(); 
        #endregion

        #region Appeals
        public List<Appeal> AuthorInAppeals { get; set; }

        public List<UserLikeAppeal> UserLikeAppeal { get; set; }
        //[NotMapped]
        //public List<Appeal> LikeAppeals => UserLikeAppeal?.Select(el => el.Appeal).ToList() ?? new List<Appeal>();

        public List<UserModeratorAppeal> UserModeratorAppeal { get; set; }
        //[NotMapped]
        //public List<Appeal> Appeals => UserModeratorAppeal?.Select(el => el.Appeal).ToList() ?? new List<Appeal>();
        public List<UserViewAppeal> ViewAppealsLink { get; set; }
        #endregion

        #region Organizations
        public virtual List<User2Organization> Users2Organizations { get; set; } = new List<User2Organization>();
        [NotMapped]
        public List<UserOrganization> Organizations
        {
            get
            {
                if (Users2Organizations == null)
                {
                    return null;
                }

                var result = new List<UserOrganization>(Users2Organizations.Count);

                foreach (var user2Organization in Users2Organizations)
                {
                    user2Organization.UserOrganization.Chief = user2Organization.Chief;

                    result.Add(user2Organization.UserOrganization);
                }

                return result;
            }
        }
        #endregion

        #region Roles

        public List<UserRole> UserRole { get; set; } = new List<UserRole>();

        [NotMapped]
        public IEnumerable<Role> Roles
        {
            get { return UserRole?.Select(el => el.Role).ToList() ?? new List<Role>(); }
        }

        [NotMapped]
        public IEnumerable<long> RoleIds
        {
            get => UserRole.Select(el => el.RoleId);
            set
            {
                UserRole = value?.Select(el => new UserRole {RoleId = el}).ToList() ?? new List<UserRole>();
            }
        }

        #endregion

        #region Claims

        public virtual List<IdentityUserClaim<long>> IdentityClaims { get; set; } = new List<IdentityUserClaim<long>>();

        [NotMapped]
        public List<Claim> Claims
        {
            get => IdentityClaims?.Select(el => new Claim(el.ClaimType, el.ClaimValue))
                .ToList();
            set
            {
                IdentityClaims = value?.Select(el => new IdentityUserClaim<long>
                                     {
                                         ClaimType = el.Type,
                                         ClaimValue = el.Value
                                     })
                                     .ToList()
                                 ?? new List<IdentityUserClaim<long>>();
            }
        }

        [NotMapped]
        public ICollection<Claim> AllClaims
        {
            get
            {
                var claims = Claims ?? new List<Claim>();

                var rolesClaims = Roles
                    .Where(el=> !el.Disconnected && el.IdentityClaims != null)
                    .Select(el => el.Claims);

                foreach (var rolesClaim in rolesClaims)
                {
                    claims.AddRange(rolesClaim);
                }
                
                var result = claims
                    .GroupBy(el=>new{el.Type, el.Value})
                    .Select(el =>new Claim(el.Key.Type, el.Key.Value))
                    .ToArray();

                return result;
            }
        }

        public bool HasClaim(string type, string value = null)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }
            
            return AllClaims.Any(el => el.Type == type && (value == null || el.Value == value));
        }

        #endregion

        #region Phones

        public virtual List<OrganizationUserPhone> UserPhones { get; set; } = new List<OrganizationUserPhone>();

        [NotMapped]
        public List<Phone> Phones
        {
            get => UserPhones?.Select(el => new Phone
                {
                    Id = el.Phone.Id,
                    Type = el.Phone.Type,
                    Number = el.Phone.Number,
                    Additional = el.Phone.Additional,
                    Confirmed = el.Confirmed
            })
                .ToList();
            set
            {
                UserPhones = value?
                    .Select(el => new OrganizationUserPhone
                    {
                        Phone = el,
                        Confirmed = el.Confirmed
                    })
                    .ToList();
            }
        }

        #endregion

        #region Votes
        public List<UserVote> UserVote { get; set; }
        [NotMapped]
        public List<long> VoteIds {
            get => UserVote?.Select(el => el.VoteId).ToList() ?? new List<long>();

            set {
                UserVote = value?
                    .Select(el => new UserVote
                    {
                        VoteId = el
                    })
                    .ToList()
                    ?? new List<UserVote>();
            }
        }
        [NotMapped]
        public List<Vote> Votes => UserVote?.Select(el => el.Vote).ToList() ?? new List<Vote>();
        #endregion

        #region IdentityDocuments

        public virtual List<UserIdentityDocument> UserIdentityDocuments { get; set; } = new List<UserIdentityDocument>();

        #endregion

        #region News
        public List<News> AuthorInNews { get; set; }
        public List<UserViewNews> ViewNewsLink { get; set; }
        public List<UserLikeNews> LikeNewsLink { get; set; }

        #endregion
        #region Subsystem
        public List<Subsystem> SSModeratorDefault { get; set; }
        #endregion
        #region Subs2Messages
        public List<Subs2DueMessages> Subs2DueMessages { get; set; }
        #endregion
        #region PToken
        public List<PToken> PTokens { get; set; }
        #endregion
    }
}
