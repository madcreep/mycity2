using System;
using BIT.EsiaNETCore;

namespace BIT.MyCity.Database
{
    public class UserIdentityDocument
    {
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// Тип документа
        /// </summary>
        public DocType Type { get; set; }

        /// <summary>
        /// Подтвержден
        /// </summary>
        public bool Verified { get; set; }

        /// <summary>
        /// Серия документа
        /// </summary>
        public string Series { get; set; }

        /// <summary>
        /// Номер документа
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public DateTime? IssueDate { get; set; }

        /// <summary>
        /// Код подразделения
        /// </summary>
        public string IssueId { get; set; }

        /// <summary>
        /// Кем выдан
        /// </summary>
        public string IssuedBy { get; set; }

        /// <summary>
        /// Cрок действия документа
        /// </summary>
        public DateTime? ExpiryDate { get; set; }


        public virtual User User { get; set; }
    }
}
