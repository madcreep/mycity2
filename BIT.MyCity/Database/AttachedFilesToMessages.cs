using Microsoft.EntityFrameworkCore.Infrastructure;

namespace BIT.MyCity.Database
{
    public class AttachedFilesToMessages
    {
        //private readonly ILazyLoader _lazyLoader;

        public AttachedFilesToMessages()
        { }

        //public AttachedFilesToMessages(ILazyLoader lazyLoader)
        //    : this()
        //{
        //    _lazyLoader = lazyLoader;
        //}

        public long MessageId { get; set; }

        //private Message _message;

        public virtual Message Message { get; set; }
        //{
        //    get => _message ?? _lazyLoader?.Load(this, ref _message);
        //    set => _message = value;
        //}

        public long AttachedFileId { get; set; }

        //private AttachedFile _attachedFile;

        public virtual AttachedFile AttachedFile { get; set; }
        //{
        //    get => _attachedFile ?? _lazyLoader?.Load(this, ref _attachedFile);
        //    set => _attachedFile = value;
        //}
    }
}
