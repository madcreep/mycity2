namespace BIT.MyCity.Database
{
    public class AttachedFilesToVoteItems
    {
        public long VoteItemId { get; set; }

        public VoteItem VoteItem { get; set; }

        public long AttachedFileId { get; set; }

        public AttachedFile AttachedFile { get; set; }
    }
}
