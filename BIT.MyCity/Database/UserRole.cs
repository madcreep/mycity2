using Microsoft.AspNetCore.Identity;

namespace BIT.MyCity.Database
{
    public class UserRole : IdentityUserRole<long>
    {
        public User User { get; set; }

        public  Role Role { get; set; }
    }
}
