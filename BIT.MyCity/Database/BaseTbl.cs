using BIT.MyCity.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BIT.MyCity.Attributes;

namespace BIT.MyCity.Database
{
    public interface IKey
    {
        long Id { get; set; }
    }

    public interface ISortable : IKey
    {
        long Weight { get; set; }
    }

    public interface IUpdatedEntity
    {
        DateTime CreatedAt { get; set; }
        DateTime? UpdatedAt { get; set; }
    }

    public interface IHasParentEntity
    {
        long ParentId { get; set; }
        string ParentType { get; set; }
    }

    public interface IHasVParentEntity
    {
        Task ParentClear(DocumentsContext ctx);
        Task ParentSet(IBaseIdEntity parent, DocumentsContext ctx);
        Task ParentSet(long id, string type, DocumentsContext ctx);
    }

    public interface IHasParent
    {
        long? ParentId { get; set; }
        string ParentType { get; set; }
    }

    public interface IHierarchical : IHasParent, ISortable
    {
        bool IsNode { get; set; }
        int Layer { get; set; }
        string Due { get; set; }
    }
    
    public interface ITreeHierarchicalEntity
    {
        bool IsNode { get; set; }
        string Due { get; set; }
    }

    public interface IDeletable
    {
        DateTime? DeleteTimestamp { get; set; }
        bool Deleted { get; set; }
    }

    public interface IDisconnectableEntity
    {
        DateTime? DisconnectedTimestamp { get; set; }
        bool Disconnected { get; set; }
    }

    public interface IBaseIdEntity : ISortable
    {
        string Type { get; set; }
    }

    public abstract class BaseIdEntity : IBaseIdEntity
    {
        public long Id { get; set; }
        public long Weight { get; set; }
        public string Type { get; set; }
    }

    public abstract class BaseEntity : BaseIdEntity, IUpdatedEntity, IDeletable
    {
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? DeleteTimestamp { get; set; }

        [ChangeJournalProperty("Признак логического удаления")]
        public bool Deleted { get; set; }
    }

    public abstract class BaseWPEntity : BaseEntity, IHasParentEntity
    {
        public long ParentId { get; set; }
        public string ParentType { get; set; }

        public void ParentSet(long id, string type)
        {
            ParentId = id;
            ParentType = type;
        }

        public void ParentSet(IBaseIdEntity parent)
        {
            ParentId = parent.Id;
            ParentType = parent.Type;
        }

        public void ParentClear()
        {
            ParentSet(0, null);
        }
    }

    public abstract class TreeEntity : BaseWPEntity, ITreeHierarchicalEntity
    {
        public bool IsNode { get; set; }
        public string Due { get; set; }
    }

    public abstract class HierarchyEntity : BaseEntity, IHierarchical
    {
        public long? ParentId { get; set; }
        public string ParentType { get; set; }
        public bool IsNode { get; set; }
        public int Layer { get; set; }
        public string Due { get; set; }
    }

    public abstract class BaseTreeEntity : BaseEntity, ITreeHierarchicalEntity
    {
        public bool IsNode { get; set; }
        public string Due { get; set; }
    }

    public interface IRubricatorFields 
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string ExtId { get; set; }
        public List<ClerkInSubsystem> Moderators { get; }
        public List<ClerkInSubsystemSet> ModeratorsSet { get; }
        public void UserModeratorClear();
        public void UserModeratorAdd(User user, Subsystem subsystem);
        public void UserModeratorRemove(User user, Subsystem subsystem);
        public List<long> ExecutorIds { get; set; }
        public List<long> ExecutorIdsChange { get; set; }
        public List<string> ExecutorIdsSubSystemUIDs { get; set; }
        public List<User> Executors { get; }
        public List<UserOfSubsystem> ExecutorsOfSubsystems { get; }
        public void UserExecutorClear();
        public void UserExecutorAdd(User user, Subsystem subsystem);
        public void UserExecutorRemove(User user, Subsystem subsystem);
        public List<string> SubSystemUIDs { get; set; }
        public List<string> SubSystemUIDsChange { get; set; }
        public List<Subsystem> Subsystems { get; }
        public void SubsystemsClear();
        public void SubsystemsAdd(Subsystem subsystem);
        public void SubsystemsRemove(Subsystem subsystem);
    }

    public interface IRubricator : IHierarchical, IRubricatorFields, IDeletable, ISortable
    { }
    
    public interface IHasAttachedFiles
    {
        public List<AttachedFile> AttachedFiles { get; set; }
        public List<long> AttachedFilesIds { get; set; }
        //public List<long> AttachedFilesIdsChange { get; set; }

        public void AddFile(AttachedFile file);
    }

    public interface IHasRubricators
    {
        public Rubric Rubric { get; set; }
        public long? RubricId { get; set; }
        public Territory Territory { get; set; }
        public long? TerritoryId { get; set; }
        public Topic Topic { get; set; }
        public long? TopicId { get; set; }
        public Region Region { get; set; }
        public long? RegionId { get; set; }
    }

    public interface IHasLikeUserList
    {
        public LikeStateEnum Liked { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
    }

    public interface IHasViewUserList
    {
        public bool Viewed { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
    }

    public interface IHasSubsystem
    {
        public Subsystem Subsystem { get; set; }
    }

    public interface IHasMessages
    {
        public List<Message> Messages { get; set; }
    }

    public interface IHasSubsystemMetaData
    {
        public string MainEntityName { get; set; }//Appeal, News, VoteRoot
        public List<SubsystemMainEntityField> MainEntityFields { get; set; }
        public bool IsPublicDefault { get; set; }
        public bool PrivateOnly { get; set; }
        public bool UseLikesForMain { get; set; }
        public bool UseLikesForComments { get; set; }
        public bool UseDislikes { get; set; }
        public bool UseRates { get; set; }

        public bool UseComments { get; set; }
        public bool UseAttaches { get; set; }
        public bool UseMaps { get; set; }
        public bool UseRubricsDescription { get; set; }
        public bool UseTerritoriesDescription { get; set; }
        public bool UseRegionsDescription { get; set; }
        public bool UseTopicsDescription { get; set; }
        public bool UseInMobileClient { get; set; }
    }
}
