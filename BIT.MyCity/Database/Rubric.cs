using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Subsystems;

namespace BIT.MyCity.Database
{
    public class Rubric : HierarchyEntity, IRubricator
    {
        #region IRubricator
        [Column("name")]
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string ExtId { get; set; }
        
        [NotMapped]
        public List<ClerkInSubsystem> Moderators =>
                UserModerator?
                    .Where(el => Subsystems.Select(x => x.UID).Contains(el.Subsystem.UID))
                    .GroupBy(el => el.Subsystem)
                    .Select(el => new ClerkInSubsystem
                    {
                        SubsystemUid = el.Key.UID,
                        Users = el
                            .Select(x => x.User)
                            .Where(x => !x.Disconnected && x.AllClaims
                                .Any(y => y.Type == ClaimName.Moderate && y.Value == $"{el.Key.UID}.{SSClaimSuffix.Md.enable}"))
                            .ToList()
                    })
                    .ToList()
                ?? new List<ClerkInSubsystem>();

        [NotMapped]
        public List<ClerkInSubsystemSet> ModeratorsSet { get; set; } = new List<ClerkInSubsystemSet>();

        public void UserModeratorClear()
        {
            UserModerator.Clear();
        }
        public void UserModeratorAdd(User user, Subsystem subsystem)
        {
            if (!UserModerator.Any(el => el.User == user && el.Subsystem == subsystem))
            {
                UserModerator.Add(new UserRubric { Rubric = this, User = user, Subsystem = subsystem });
            }
        }
        public void UserModeratorRemove(User user, Subsystem subsystem)
        {
            UserModerator.RemoveAll(el => el.User == user && el.Subsystem == subsystem);
        }

        [NotMapped]
        public List<long> ExecutorIds { get; set; } = new List<long>();
        [NotMapped]
        public List<long> ExecutorIdsChange { get; set; } = new List<long>();
        [NotMapped]
        public List<string> ExecutorIdsSubSystemUIDs { get; set; } = new List<string>();
        [NotMapped]
        public List<User> Executors => UserExecutor?.Select(el => el.User).ToList() ?? new List<User>();
        [NotMapped]
        public List<UserOfSubsystem> ExecutorsOfSubsystems
            => UserExecutor?.Select(el => new UserOfSubsystem { User = el.User, Subsystem = el.Subsystem }).ToList()
            ?? new List<UserOfSubsystem>();
        public void UserExecutorClear()
        {
            UserExecutor.Clear();
        }
        public void UserExecutorAdd(User user, Subsystem subsystem)
        {
            if (!UserExecutor.Any(el => el.User == user && el.Subsystem == subsystem))
            {
                UserExecutor.Add(new ExecutorRubric { Rubric = this, User = user, Subsystem = subsystem });
            }
        }
        public void UserExecutorRemove(User user, Subsystem subsystem)
        {
            UserExecutor.RemoveAll(el => el.User == user && (subsystem == null || el.Subsystem == subsystem));
        }

        [NotMapped]
        public List<string> SubSystemUIDs { get; set; } = new List<string>();
        [NotMapped]
        public List<string> SubSystemUIDsChange { get; set; } = new List<string>();
        [NotMapped]
        public List<Subsystem> Subsystems => Rubricator2Subsystems?.Select(el => el.Subsystem).ToList() ?? new List<Subsystem>();
        public void SubsystemsClear()
        {
            Rubricator2Subsystems.Clear();
        }
        public void SubsystemsAdd(Subsystem subsystem)
        {
            if (!Rubricator2Subsystems.Any(el => el.Subsystem == subsystem))
            {
                Rubricator2Subsystems.Add(new Rubric2Subsystem { Rubric = this, Subsystem = subsystem });
            }
        }
        public void SubsystemsRemove(Subsystem subsystem)
        {
            Rubricator2Subsystems.RemoveAll(el => el.Subsystem == subsystem);
        }
        #endregion

        public List<UserRubric> UserModerator { get; set; } = new List<UserRubric>();
        public List<ExecutorRubric> UserExecutor { get; set; } = new List<ExecutorRubric>();
        public List<Rubric2Subsystem> Rubricator2Subsystems { get; set; } = new List<Rubric2Subsystem>();

        public string ExtType { get; set; }
        public string Description { get; set; }
        public string Docgroup { get; set; }
        public string Receivers { get; set; }
        public bool UseForPA { get; set; }

        public string DocGroupForUL { get; set; }

        public Rubric()
        {
            Type = nameof(Rubric);
            ParentType = nameof(Rubric);
        }
    }
}
