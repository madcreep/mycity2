using System.ComponentModel.DataAnnotations;
using System;
using BIT.MyCity.Model;

namespace BIT.MyCity.Database
{
    public class PushMessage : IUpdatedEntity
    {
        [Key]
        public long Id { get; set; }

        public long UserId { get; set; }

        public virtual User User { get; set; }

        //public string Token { get; set; }
        
        public string Sender { get; set; }


        public string Body { get; set; }
        public string Title { get; set; }
        
        public long AppealId { get; set; }
        
        public PushMessageStatus Status { get; set; } = PushMessageStatus.Created;

        public string ErrorMessage { get; set; }

        
        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }
        
    }
}