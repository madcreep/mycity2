namespace BIT.MyCity.Database
{
    public class AttachedFilesToVoteRoots
    {
        public long VoteRootId { get; set; }

        public VoteRoot VoteRoot { get; set; }

        public long AttachedFileId { get; set; }

        public AttachedFile AttachedFile { get; set; }
    }
}
