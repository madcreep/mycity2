using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.WebApi.Types.Pagination;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace BIT.MyCity.Database
{
    public static class BaseConstants
    {
        public static string Int2Due(long i, string parentDue = null, string a = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        {
            var due = string.Empty;
            while (i > 0)
            {
                due += a[(int)(i % a.Length)];
                i /= a.Length;
            }
            if (string.IsNullOrEmpty(due))
            {
                due = "0";
            }
            return (parentDue ?? "") + due + ".";
        }

        public static async Task SaveDueAsync1<T>(this DocumentsContext ctx, T obj, object parent = null) where T : IHierarchical
        {
            var due = GetDue(parent);
            if (obj.Id == 0)
            {
                await ctx.SaveChangesAsync();
            }
            obj.Due = Int2Due(obj.Id, due);
            await ctx.SaveChangesAsync();
        }

        public static async Task SaveDueAsync<T>(this DocumentsContext ctx, T obj, object parent = null) where T : class, IBaseIdEntity, ITreeHierarchicalEntity
        {
            var due = GetDue(parent);
            if (obj.Id == 0)
            {
                await ctx.SaveChangesAsync();
            }
            obj.Due = Int2Due(obj.Id, due);
            await ctx.SaveChangesAsync();
        }

        public static string GetDue(object parent)
        {
            string due = null;
            if (parent != null)
            {
                due = parent switch
                {
                    BaseTreeEntity p1 => p1.Due,
                    TreeEntity p2 => p2.Due,
                    BaseIdEntity p3 => Int2Due(p3.Id),
                    _ => due
                };
            }
            return due;
        }

        public static async Task RebaseDueHierarchical<T>(T dest, T src, DocumentsContext ctx) where T : IHierarchical
        {
            var duePrefixOldLen = 0;

            if ((src.ParentId != 0 && src.ParentId != dest.ParentId) || (!string.IsNullOrWhiteSpace(src.ParentType) && src.ParentType != dest.ParentType))
            {
                var parentNew = await src.GetParentAsyncHierarchical(ctx);

                if (parentNew == null)
                {
                    throw new Exception($"Parent {src.ParentType}:{src.ParentId} not found");
                }
                var duePrefixNew = GetDue(parentNew);
                if (duePrefixNew == null)
                {
                    throw new Exception($"Не удалось вычислить новый префикс DUE для перемещения ветви");
                }
                var parentOld = await dest.GetParentAsyncHierarchical(ctx);
                string duePrefixOld = null;
                if (parentOld != null)
                {
                    duePrefixOld = GetDue(parentOld);
                    if (duePrefixOld == null)
                    {
                        throw new Exception($"Не удалось вычислить старый префикс DUE для перемещения ветви");
                    }
                }
                if (duePrefixOld != null)
                {
                    duePrefixOldLen = duePrefixOld.Length;
                }
                if (duePrefixNew != null)
                {
                    var dbset = ctx.GetType().GetProperty(dest.GetType().Name).GetValue(ctx) as IQueryable<IHierarchical>;
                    if (dbset == null)
                    {
                        return;
                    }
                    if (dbset.Where(el => el.Due.StartsWith(dest.Due)).Any(el => el.Id == src.ParentId))
                    {
                        throw new Exception($"Попытка перемещения ветви в самое себя");
                    }
                    var subTree = await dbset.Where(el => el.Due.StartsWith(dest.Due)).ToListAsync();
                    foreach (var el in subTree)
                    {
                        el.Due = duePrefixNew + el.Due.Substring(duePrefixOldLen);
                    }
                }
            }
        }

        //public static async Task RebaseDue<T>(T o, T n, DocumentsContext ctx, DbSet<T> db) where T : class, IBaseIdEntity, ITreeHierarchicalEntity, IHasParentEntity
        //{
        //    string duePrefixNew = null;
        //    int duePrefixOldLen = 0;
        //    if ((n.ParentId != 0 && n.ParentId != o.ParentId) || (!string.IsNullOrWhiteSpace(n.ParentType) && n.ParentType != o.ParentType))
        //    {
        //        var parentNew = await n.GetParentAsync(ctx);
        //        if (parentNew == null)
        //        {
        //            throw new Exception($"Parent {n.ParentType}:{n.ParentId} not found");
        //        }
        //        duePrefixNew = GetDue(parentNew);
        //        if (duePrefixNew == null)
        //        {
        //            throw new Exception($"Не удалось вычислить новый префикс DUE для перемещения ветви");
        //        }
        //        var parentOld = await o.GetParentAsync(ctx);
        //        string duePrefixOld = null;
        //        if (parentOld != null)
        //        {
        //            duePrefixOld = GetDue(parentOld);
        //            if (duePrefixOld == null)
        //            {
        //                throw new Exception($"Не удалось вычислить старый префикс DUE для перемещения ветви");
        //            }
        //        }
        //        if (duePrefixOld != null)
        //        {
        //            duePrefixOldLen = duePrefixOld.Length;
        //        }
        //        if (duePrefixNew != null)
        //        {
        //            if (db.Where(el => el.Due.StartsWith(o.Due)).Where(el => el.Id == n.ParentId).Any())
        //            {
        //                throw new Exception($"Попытка перемещения ветви в самое себя");
        //            }
        //            var subTree = await db.Where(el => el.Due.StartsWith(o.Due)).ToListAsync();
        //            foreach (var el in subTree)
        //            {
        //                el.Due = duePrefixNew + el.Due.Substring(duePrefixOldLen);
        //            }
        //        }
        //    }
        //}
        
        public static async Task<string> DeleteDbAsync(string entityType, long entityId, DocumentsContext ctx, IServiceScope scope)
        {
            var f = ctx
                .GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(
                    el => el.PropertyType.GenericTypeArguments.Length == 1
                    && el.PropertyType.GenericTypeArguments[0].GetInterfaces().Contains(typeof(IBaseIdEntity))
                    && el.PropertyType.GenericTypeArguments[0].Name == entityType)
                .FirstOrDefault();
            var q = (IQueryable<IBaseIdEntity>)f?.GetValue(ctx);
            if (q != null)
            {
                var obj = await q?.Where(el => !((IDeletable)el).Deleted && el.Id == entityId).FirstOrDefaultAsync();
                if (obj != null)
                {
                    await DeleteDbAsync(obj, ctx, scope);
                }
                else
                {
                    throw new Exception($"Entity:'{entityType}' with id:{entityId} not found");
                }
                return "OK";
            }
            else
            {
                throw new Exception($"Entity:'{entityType}' not found");
            }
        }

        public static async Task DeleteDbAsync(IBaseIdEntity obj, DocumentsContext ctx, IServiceScope scope)
        {
            if (obj.GetType().GetInterfaces().Contains(typeof(IHasAttachedFiles)))
            {
                var o = (IHasAttachedFiles)obj;
                if (o.AttachedFiles == null)
                {
                    await ctx.Entry(o).Collection(el => el.AttachedFiles).LoadAsync();
                }
                o.AttachedFiles?.Clear();
            }
            if (obj.GetType() == typeof(AttachedFile))
            {
                var m = (AttachedFileManager)scope.ServiceProvider.GetService(typeof(AttachedFileManager));
                m.DeleteAttach((AttachedFile)obj);
            }
            if (obj.GetType().GetInterfaces().Contains(typeof(IDeletable)))
            {
                if (!((IDeletable)obj).Deleted)
                {
                    ((IDeletable)obj).DeleteTimestamp = DateTime.UtcNow;
                    ((IDeletable)obj).Deleted = true;
                }
            }
            else if (obj.GetType().GetInterfaces().Contains(typeof(IDisconnectableEntity)))
            {
                if (!((IDisconnectableEntity)obj).Disconnected)
                {
                    ((IDisconnectableEntity)obj).DisconnectedTimestamp = DateTime.UtcNow;
                    ((IDisconnectableEntity)obj).Disconnected = true;
                }
            }
            //else
            //{
            //    if (obj.GetType().Name == nameof(EnumVerb))
            //    {
            //        ctx.EnumVerbs.Remove((EnumVerb)obj);
            //    }
            //}
            await ctx.SaveChangesAsync();
        }

        //public static async Task AddAttachedFileToParent(AttachedFile obj, DocumentsContext ctx)
        //{
        //    await DeleteAttachedFileFromParentAsync(obj, ctx, true);
        //}

        public static async Task SetT2LinkedEntIdsAsync<T, T2>(
            List<long> srcIds,
            List<T> destObjs,
            Func<long, Task<T2>> find,
            Func<T2, long, T> func,
            Func<long, bool> exist
            ) where T : class where T2 : class
        {
            destObjs.Clear();
            foreach (var fileId in srcIds)
            {
                if (fileId < 0)
                {
                    throw new Exception($"For partial modify lists use *Change property, id '{fileId}' is not valid for Set");
                }
                else if (!exist(fileId))
                {
                    var file = await find(fileId);
                    if (file != null)
                    {
                        destObjs.Add(func(file, fileId));
                    }
                    else
                    {
                        throw new Exception($"Id:{fileId} not found on ADD");
                    }
                }
            }
        }

        //public static async Task SetT2LinkedEntIdsAsync<T, T2>(
        //    List<string> srcUIDs,
        //    List<T2> destObjs,
        //    Func<string, Task<T>> find,
        //    Func<T, T2> func,
        //    Func<long, bool> exist
        //    ) where T : IBaseIdEntity where T2 : class
        //{
        //    destObjs.Clear();
        //    foreach (string srcUID in srcUIDs)
        //    {
        //        if (srcUID.StartsWith("-"))
        //        {
        //            throw new Exception($"For partial modify lists use *Change property, id '{srcUID}' is not valid for Set");
        //        }
        //        T file = await find(srcUID);
        //        if (file == null)
        //        {
        //            throw new Exception($"UID '{srcUID}' not found on ADD");
        //        }
        //        if (!exist(file.Id))
        //        {
        //            destObjs.Add(func(file));
        //        }
        //    }
        //}

        /*изменение списка юзеров через список их Id (+id add, -id remove)*/
        public static async Task UpdateT2UsersIdsAsync<T>(
            List<long> srcIds,/*список изменений + и -*/
            List<T> destObjs,/*старый список*/
            T elem,/*объект связки (new UserModeratorAppeal { Appeal = dest, AppealId = dest.Id },)*/
            DocumentsContext ctx,/*контекст БД*/
            Func<long, Task<T>> func/*функция поиска объекта связки (fileId => _ctx.UserModeratorAppeals.FirstOrDefaultAsync(el => el.AppealId == dest.Id && el.UserId == fileId))*/
            ) where T : BaseLinkUser
        {
            var destIds = destObjs.Select(el => el.UserId).ToList();
            foreach (var fileId in srcIds)
            {
                if (fileId > 0 && !destIds.Contains(fileId))
                {
                    var file = await ctx.Users
                        .Where(el => el.Id == fileId)
                        .Where(el => !el.Disconnected)
                        .FirstOrDefaultAsync();
                    if (file != null)
                    {
                        elem.User = file;
                        elem.UserId = file.Id;
                        destObjs.Add(elem);
                        destIds.Add(file.Id);
                    }
                    else
                    {
                        throw new Exception($"UserId:{fileId} not found on ADD");
                    }
                }
                else if (fileId < 0 && destIds.Contains(-fileId))
                {
                    destIds.Remove(-fileId);
                    var file = await func(-fileId);
                    if (file != null)
                    {
                        destObjs.Remove(file);
                    }
                }
            }
        }
        //public static async Task CreateT2UsersIdsAsync<T>(
        //    List<long> srcIds,
        //    List<T> destObjs,
        //    DocumentsContext ctx
        //    ) where T : BaseLinkUser, new()
        //{
        //    foreach (var fileId in srcIds)
        //    {
        //        var file = await ctx.Users
        //            .Where(el => el.Id == fileId)
        //            .Where(el => !el.Disconnected)
        //            .FirstOrDefaultAsync();
        //        if (file != null)
        //        {
        //            destObjs.Add(new T { User = file, UserId = file.Id });
        //        }
        //        else
        //        {
        //            throw new Exception($"UserId:{fileId} not found on ADD");
        //        }
        //    }
        //}
        //public static async Task UpdateUser2TsIdsAsync<T, T1>(
        //    List<long> destIds,
        //    List<long> srcIds,
        //    List<T> destObjs,
        //    Func<T1, T> elem,
        //    Func<long, Task<T>> func,
        //    Func<long, Task<T1>> func2,
        //    Func<T, long> func3
        //    ) where T : BaseLinkUser where T1 : BaseEntity
        //{
        //    destIds.Clear();
        //    foreach (var file in destObjs)
        //    {
        //        destIds.Add(func3(file));
        //    }
        //    foreach (var fileId in srcIds)
        //    {
        //        if (fileId > 0 && !destIds.Contains(fileId))
        //        {
        //            var file = await func2(fileId);
        //            if (file != null)
        //            {
        //                destObjs.Add(elem(file));
        //                destIds.Add(file.Id);
        //            }
        //            else
        //            {
        //                throw new Exception($"Id:{fileId} not found on ADD");
        //            }
        //        }
        //        else if (fileId < 0 && destIds.Contains(-fileId))
        //        {
        //            destIds.Remove(-fileId);
        //            var file = await func(-fileId);
        //            if (file != null)
        //            {
        //                destObjs.Remove(file);
        //            }
        //        }
        //    }
        //}
        public static void UpdateObjNamesList(List<string> destNames, List<string> srcNames, List<string> avail, string ex = "Name")
        {
            foreach (var srcName in srcNames)
            {
                if (!srcName.StartsWith("-") && !destNames.Any(s => s.Equals(srcName, StringComparison.OrdinalIgnoreCase)))
                {
                    int i;
                    for (i = 0; i < avail.Count; i++)
                    {
                        if (avail[i].Equals(srcName, StringComparison.OrdinalIgnoreCase))
                            break;
                    }
                    if (i < avail.Count)
                    {
                        destNames.Add(avail[i]);
                    }
                    else
                    {
                        throw new Exception($"{ex}:{srcName} not found on ADD");
                    }
                }
                else if (srcName.StartsWith("-") && destNames.Any(s => s.Equals(srcName.Substring(1), StringComparison.OrdinalIgnoreCase)))
                {
                    int i;
                    var s = srcName.Substring(1);
                    for (i = 0; i < avail.Count; i++)
                    {
                        if (avail[i].Equals(s, StringComparison.OrdinalIgnoreCase))
                            break;
                    }
                    if (i < avail.Count)
                    {
                        destNames.RemoveAt(i);
                    }
                }
            }
        }
        public static async Task UpdateObjIdsList<T>(
            List<long> destIds,         /*список назначения*/
            List<long> srcIds,          /*список пришел*/
            DbSet<T> db,                /*БД где хранится T*/
            string ex = "ObjId",        /*часть сообщения об ошибке*/
            Action<T> funcAdd = null,   /*вызывается при добавлении*/
            Action<T> funcDel = null    /*вызывается при удалении*/
            ) where T : BaseEntity
        {
            foreach (long srcId in srcIds)
            {
                if (srcId > 0 && !destIds.Contains(srcId))
                {
                    T dbObj = await db
                        .Where(el => el.Id == srcId)
                        .Where(el => !el.Deleted)
                        .FirstOrDefaultAsync();
                    if (dbObj != null)
                    {
                        destIds.Add(srcId);
                        funcAdd?.Invoke(dbObj);
                    }
                    else
                    {
                        throw new Exception($"{ex}:{srcId} not found on ADD");
                    }
                }
                else if (srcId < 0 && destIds.Contains(-srcId))
                {
                    destIds.Remove(-srcId);
                    T dbObj = await db
                        .Where(el => el.Id == -srcId)
                        .Where(el => !el.Deleted)
                        .FirstOrDefaultAsync();
                    if (dbObj != null)
                    {
                        funcDel?.Invoke(dbObj);
                    }
                }
            }
        }

        public static async Task UpdateObjIdsList<T>(
            List<string> destUIDs,         /*список назначения*/
            List<string> srcUIDs,          /*список пришел*/
            DbSet<T> db,                /*БД где хранится T*/
            string ex = "ObjId",        /*часть сообщения об ошибке*/
            Action<T> funcAdd = null,   /*вызывается при добавлении*/
            Action<T> funcDel = null    /*вызывается при удалении*/
            ) where T : Subsystem
        {
            foreach (var srcUID in srcUIDs)
            {
                if (!srcUID.StartsWith("-") && !destUIDs.Contains(srcUID))
                {
                    T dbObj = await db
                        .Where(el => el.UID == srcUID)
                        .Where(el => !el.Deleted)
                        .FirstOrDefaultAsync();
                    if (dbObj != null)
                    {
                        destUIDs.Add(srcUID);
                        funcAdd?.Invoke(dbObj);
                    }
                    else
                    {
                        throw new Exception($"{ex}:{srcUID} not found on ADD");
                    }
                }
                else if (srcUID.StartsWith("-") && destUIDs.Contains(srcUID.Substring(1)))
                {
                    destUIDs.Remove(srcUID.Substring(1));
                    T dbObj = await db
                        .Where(el => el.UID == srcUID.Substring(1))
                        .Where(el => !el.Deleted)
                        .FirstOrDefaultAsync();
                    if (dbObj != null)
                    {
                        funcDel?.Invoke(dbObj);
                    }
                }
            }
        }

        public static async Task SetObjIdsList<T>(List<long> srcList, List<long> destList, DbSet<T> db, string comment = "ObjectId") where T : class, IBaseIdEntity, IDeletable
        {
            destList.Clear();
            foreach (var a in srcList.Distinct())
            {
                if (!(await db.AnyAsync(el => !el.Deleted && el.Id == a)))
                {
                    throw new Exception($"{comment} {a} not found");
                }
                else
                {
                    destList.Add(a);
                }
            }
        }

        public static async Task SetAttachedFilesIdsList<T1, T2>(
            List<long> srcIds,
            List<T1> destObjs,
            DbSet<T1> db,
            T2 dest) where T1 : BaseWPEntity where T2 : BaseEntity
        {
            foreach (var file in destObjs)
            {
                file.ParentClear();
            }
            destObjs.Clear();
            List<long> destIds = new List<long>();
            foreach (var fileId in srcIds)
            {
                if (!destIds.Contains(fileId))
                {
                    var file = await db
                        .Where(el => el.Id == fileId)
                        .Where(el => !el.Deleted)
                        .FirstOrDefaultAsync();

                    if (file != null)
                    {
                        destObjs.Add(file);
                        destIds.Add(fileId);
                        file.ParentSet(dest);
                    }
                    else
                    {
                        throw new Exception($"{typeof(T1)}:{fileId} not found on ADD");
                    }
                }
            }
        }

        /*Функция проверяет существование объектов на добавление
         и дополнительно проверяет предикат у добавляемых объектов.
         При истине предиката вызыдвается action
         При отсутствии объекта кидается Exception*/
        public static async Task CheckAddIdsList<T1, T2>(
            string name,
            T2 src,
            DbSet<T1> db,
            Func<T1, bool> predicate = null,
            Action<T1> action = null
            ) where T1 : BaseEntity where T2 : BaseEntity
        {
            var fieldIds = src.GetType().GetProperty($"{name}Ids", BindingFlags.Public | BindingFlags.Instance);
            var fieldIdsChange = src.GetType().GetProperty($"{name}IdsChange", BindingFlags.Public | BindingFlags.Instance);
            var srcIds = (List<long>)fieldIds.GetValue(src);
            var srcIdsChange = (List<long>)fieldIdsChange.GetValue(src);
            var ids = new List<long>();
            if (srcIds != null)
            {
                ids.AddRange(srcIds);
            }
            if (srcIdsChange != null)
            {
                ids.AddRange(srcIdsChange.Where(el => el > 0));
            }
            if (predicate != null)
            {
                var existInDB = await db.Where(el => ids.Contains(el.Id)).ToListAsync();
                var notExistInDB = ids.Except(existInDB.Select(el => el.Id)).FirstOrDefault();
                if (notExistInDB > 0)
                {
                    throw new Exception($"'{typeof(T1).Name}':{notExistInDB} not found");
                }
                var err = existInDB.Where(predicate).FirstOrDefault();
                if (err != null)
                {
                    action?.Invoke(err);
                }
            }
        }
        public static async Task ModifyIdsList<T1, T2>(
            string name,
            T2 src,
            T2 dest,
            DbSet<T1> db,
            DocumentsContext ctx,
            Expression<Func<T2, IEnumerable<T1>>> propertyExpression = null
            ) where T1 : BaseEntity where T2 : BaseEntity
        {
            var fieldObjs = dest.GetType().GetProperty($"{name}", BindingFlags.Public | BindingFlags.Instance);
            var fieldIds = src.GetType().GetProperty($"{name}Ids", BindingFlags.Public | BindingFlags.Instance);
            var fieldIdsChange = src.GetType().GetProperty($"{name}IdsChange", BindingFlags.Public | BindingFlags.Instance);
            var destObjs = (List<T1>)fieldObjs.GetValue(dest);
            var srcIds = (List<long>)fieldIds.GetValue(src);
            var srcIdsChange = (List<long>)fieldIdsChange.GetValue(src);
            if (destObjs == null && (srcIds != null || (srcIdsChange != null && srcIdsChange.Count > 0)))
            {
                if (propertyExpression != null && (ctx.Entry(dest).State == EntityState.Modified || ctx.Entry(dest).State == EntityState.Unchanged))
                {
                    await ctx.Entry(dest).Collection(propertyExpression).LoadAsync();
                    destObjs = (List<T1>)fieldObjs.GetValue(dest);
                }
                else
                {
                    destObjs = new List<T1>();
                    fieldObjs.SetValue(dest, destObjs);
                }
            }
            await SetAttachedFilesIdsList(srcIds, destObjs, db, ctx);
            await UpdateAttachedFilesIdsList(srcIdsChange, destObjs, db, ctx);
        }

        public static async Task SetAttachedFilesIdsList<T>(
            List<long> srcIds,
            List<T> destObjs,
            DbSet<T> db,
            DocumentsContext ctx
            ) where T : BaseEntity
        {
            if (srcIds != null)
            {
                if (typeof(T).GetInterfaces().Contains(typeof(IHasVParentEntity)))
                {
                    var l = destObjs.Select(el => el).ToList();
                    foreach (T file in l)
                    {
                        await ((IHasVParentEntity)file).ParentClear(ctx);
                    }
                }
                destObjs.Clear();
                foreach (var fileId in srcIds)
                {
                    if (!destObjs.Any(el => el.Id == fileId))
                    {
                        T file = await db
                            .Where(el => el.Id == fileId)
                            .Where(el => !el.Deleted)
                            .FirstOrDefaultAsync();
                        if (file != null)
                        {
                            if (typeof(T).GetInterfaces().Contains(typeof(IHasVParentEntity)))
                            {
                                await ((IHasVParentEntity)file).ParentClear(ctx);
                            }
                            destObjs.Add(file);
                        }
                        else
                        {
                            throw new Exception($"{typeof(T)}:{fileId} not found on ADD");
                        }
                    }
                }
            }
        }

        //public static async Task UpdateAttachedFilesIdsList<T1, T2>(
        //    List<long> srcIds,
        //    List<T1> destObjs,
        //    DbSet<T1> db,
        //    T2 dest) where T1 : BaseWPEntity where T2 : BaseEntity
        //{
        //    var destIds = destObjs.Select(el => el.Id).ToList();
        //    foreach (var fileId in srcIds)
        //    {
        //        if (fileId > 0 && !destIds.Contains(fileId))
        //        {
        //            var file = await db
        //                .Where(el => el.Id == fileId)
        //                .Where(el => !el.Deleted)
        //                .FirstOrDefaultAsync();

        //            if (file != null)
        //            {
        //                destObjs.Add(file);
        //                destIds.Add(file.Id);
        //                file.ParentSet(dest);
        //            }
        //            else
        //            {
        //                throw new Exception($"{typeof(T1)}:{fileId} not found on ADD");
        //            }
        //        }
        //        else if (fileId < 0 && destIds.Contains(-fileId))
        //        {
        //            destIds.Remove(-fileId);
        //            var file = await db
        //                .Where(el => el.Id == -fileId)
        //                .FirstOrDefaultAsync();
        //            if (file != null)
        //            {
        //                destObjs.Remove(file);
        //                file.ParentClear();
        //            }
        //        }
        //    }
        //}
        public static async Task UpdateAttachedFilesIdsList<T>(
            List<long> srcIds,
            List<T> destObjs,
            DbSet<T> db,
            DocumentsContext ctx
            ) where T : BaseEntity
        {
            if (srcIds != null)
            {
                foreach (var fileId in srcIds)
                {
                    if (fileId > 0 && !destObjs.Any(el => el.Id == fileId))
                    {
                        var file = await db
                            .Where(el => el.Id == fileId)
                            .Where(el => !el.Deleted)
                            .FirstOrDefaultAsync();

                        if (file != null)
                        {
                            if (typeof(T).GetInterfaces().Contains(typeof(IHasVParentEntity)))
                            {
                                await ((IHasVParentEntity)file).ParentClear(ctx);
                            }
                            destObjs.Add(file);
                        }
                        else
                        {
                            throw new Exception($"{typeof(T)}:{fileId} not found on ADD");
                        }
                    }
                    else if (fileId < 0)
                    {
                        var file = destObjs.Where(el => el.Id == (-fileId)).FirstOrDefault();
                        if (file != null)
                        {
                            destObjs.Remove(file);
                        }
                    }
                }
            }
        }

        public static void UpdateEntityByFieldsList<T>(T dest, T src, GraphQlKeyDictionary fields, string[] exclude = null)
        {
            if (dest == null || src == null)
            {
                return;
            }

            if (fields == null)
            {
                foreach (string field in dest
                    .GetType()
                    .GetProperties()
                    .Select(el => el.Name)
                    .Where(el => el != nameof(BaseEntity.Id))
                    .Where(el => (exclude == null || !exclude.Contains(el, StringComparer.OrdinalIgnoreCase)))
                )
                {
                    NewMethod(dest, src, field);
                }
            }
            else
            {
                foreach (var field in fields.Keys)
                {
                    if (exclude == null || !exclude.Contains(field, StringComparer.OrdinalIgnoreCase))
                    {
                        NewMethod(dest, src, field);
                    }
                }
            }
        }

        private static void NewMethod<T>(T dest, T src, string field)
        {
            var f = dest.GetType()
                .GetProperty(field, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

            if (f == null)
            {
                return;
            }

            if (f.PropertyType != typeof(List<long>))
            {
                f.SetValue(dest, f.GetValue(src));
            }
        }

        public static async Task<object> GetEntityAsync(long entityId, string entityType, DocumentsContext ctx)
        {
            if (entityType == null)
            {
                return null;
            }

            var f = ctx
                .GetType()
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .FirstOrDefault(el => el.PropertyType.GenericTypeArguments.Length == 1
                                      && el.PropertyType.GenericTypeArguments[0].GetInterfaces().Contains(typeof(IBaseIdEntity))
                                      && el.PropertyType.GenericTypeArguments[0].Name == entityType);

            var q = (IQueryable<IBaseIdEntity>)f?
                .GetValue(ctx);

            if (q == null)
            {
                return null;
            }

            var e = await q?
                .Where(el => el.Id == entityId)
                .FirstOrDefaultAsync();

            return e;
        }

        public static async Task<object> GetParentAsync1<T>(this T obj, DocumentsContext ctx, List<string> parentTypes = null)
            where T : IHierarchical
        {
            object parent = null;

            if (obj.ParentId != 0 && obj.ParentId.HasValue
                                  && !string.IsNullOrEmpty(obj.ParentType)
                                  && (parentTypes == null || parentTypes.Contains(obj.ParentType)))
            {
                parent = await GetEntityAsync(obj.ParentId.Value, obj.ParentType, ctx);
            }

            return parent;
        }

        public static async Task<object> GetParentAsyncHierarchical<T>(this T obj, DocumentsContext ctx, List<string> parentTypes = null)
            where T : IHierarchical
        {
            object parent = null;

            if (obj.ParentId != 0 && obj.ParentId.HasValue
                                  && !string.IsNullOrEmpty(obj.ParentType)
                                  && (parentTypes == null || parentTypes.Contains(obj.ParentType)))
            {
                parent = await GetEntityAsync(obj.ParentId.Value, obj.ParentType, ctx);
            }

            return parent;
        }


        public static async Task<object> GetParentAsync<T>(this T obj, DocumentsContext ctx, List<string> parentTypes = null)
            where T : IHasParentEntity
        {
            object parent = null;

            if (obj.ParentId != 0
                && !string.IsNullOrEmpty(obj.ParentType)
                && (parentTypes == null || parentTypes.Contains(obj.ParentType)))
            {
                parent = await GetEntityAsync(obj.ParentId, obj.ParentType, ctx);
            }

            return parent;
        }

        public static PropertyInfo GetPropInfo(this Type src, string propertyName)
        {
            var propertyInfo = src.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            if (propertyInfo == null)
            {
                return null;
            }
            if (propertyInfo.DeclaringType != src)
            {
                propertyInfo = propertyInfo.DeclaringType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            }
            if (propertyInfo == null)
            {
                return null;
            }
            var notMappedAttribute = propertyInfo
                .GetCustomAttribute<NotMappedAttribute>();
            if (notMappedAttribute != null)
            {
                return null;
            }
            return propertyInfo;
        }

        //public static PropertyInfo GetPropInfo1<TSource>(this TSource src, string propertyName)
        //{
        //    var entityType = src.GetType();
        //    var propertyInfo = entityType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
        //    if (propertyInfo == null)
        //    {
        //        return null;
        //    }
        //    if (propertyInfo.DeclaringType != entityType)
        //    {
        //        propertyInfo = propertyInfo.DeclaringType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
        //    }
        //    if (propertyInfo == null)
        //    {
        //        return null;
        //    }
        //    var notMappedAttribute = propertyInfo
        //        .GetCustomAttribute<NotMappedAttribute>();
        //    if (notMappedAttribute != null)
        //    {
        //        return null;
        //    }
        //    return propertyInfo;
        //}

        //public static IQueryable<TSource> IncludeByPath<TSource>(this IQueryable<TSource> query,
        //    string propertyPath) where TSource : class
        //{
        //    var entityType = typeof(TSource);
        //    var nn = propertyPath.Split('.');
        //    var ct = entityType;
        //    string ol = "";
        //    for (int i = 0; i < nn.Length - 1; i++)
        //    {
        //        var pi = ct.GetPropInfo(nn[i]);
        //        if (pi == null)
        //        {
        //            break;
        //        }
        //        if (!string.IsNullOrEmpty(ol))
        //        {
        //            ol += ".";
        //        }
        //        ol += pi.Name;
        //        ct = pi.PropertyType;
        //    }
        //    if (!string.IsNullOrEmpty(ol))
        //    {
        //        query = query.Include(ol);
        //    }
        //    return query;
        //}

        //public static IQueryable<TSource> IncludeByName<TSource>(this IQueryable<TSource> query,
        //    string propertyName) where TSource : class
        //{
        //    var entityType = typeof(TSource);
        //    PropertyInfo propertyInfo = entityType.GetPropInfo(propertyName);
        //    if (propertyInfo == null)
        //    {
        //        return query;
        //    }
        //    var res = query.Include(propertyInfo.Name);
        //    return res;
        //}

        //public static Expression<Func<TSource, bool>> MakeExpr1<TSource>(Type entityType, string[] pn, string template, int level = 0)
        //{
        //    Type ct = entityType;
        //    ParameterExpression entity = Expression.Parameter(entityType, "x" + level.ToString());
        //    bool isFirst = true;
        //    MemberExpression property = null;
        //    for (int i = 0; i < pn.Length; i++)
        //    {
        //        var propertyName = pn[i];
        //        var pi = ct.GetPropInfo(propertyName);
        //        if (pi == null)
        //        {
        //            return null;
        //        }
        //        if (isFirst)
        //        {
        //            property = Expression.MakeMemberAccess(entity, pi);
        //            isFirst = false;
        //        }
        //        else
        //        {
        //            property = Expression.MakeMemberAccess(property, pi);
        //        }
        //        ct = pi.PropertyType;

        //        var d0 = ct.GenericTypeArguments;
        //        if (d0 != null && d0.Length == 1 && i < pn.Length - 1)
        //        {
        //            Type d1 = d0[0];
        //            var laex = MakeExpr(d1, pn.Skip(i + 1).ToArray(), template, level + 1);
        //            MethodInfo mw = typeof(Enumerable).GetMethods()
        //                .Where(m => m.Name == "Any")
        //                .Where(m => m.GetParameters().ToList().Count == 2)
        //                .FirstOrDefault();
        //            var mwg = mw.MakeGenericMethod(d1);
        //            MethodCallExpression wcall = Expression.Call(mwg, property, laex);
        //            ParameterExpression e1 = Expression.Parameter(d1, "y");
        //            return Expression.Lambda<Func<TSource, bool>>(wcall, new ParameterExpression[] { entity });
        //        }
        //    }
        //    if (property == null)
        //    {
        //        return null;
        //    }
        //    MethodInfo method1 = typeof(string).GetMethods()
        //        .Where(m => m.Name == "ToLower")
        //        .Where(m => m.GetParameters().ToList().Count == 0)
        //        .SingleOrDefault();
        //    MethodInfo method2 = typeof(string).GetMethods()
        //        .Where(m => m.Name == "Contains")
        //        .Where(m => m.GetParameters().ToList().Count == 1)
        //        .Where(m => m.GetParameters()[0].ParameterType == typeof(string))
        //        .SingleOrDefault();
        //    MethodCallExpression call1 = Expression.Call(property, method1);
        //    MethodCallExpression call2 = Expression.Call(call1, method2, new Expression[] { Expression.Constant(template.ToLower()) });
        //    var la = Expression.Lambda<Func<TSource, bool>>(call2, new ParameterExpression[] { entity });
        //    return la;
        //}

        public static LambdaExpression MakeExpr(Type entityType, string[] pn, string template, int level = 0)
        {
            Type ct = entityType;
            ParameterExpression entity = Expression.Parameter(entityType, "x" + level.ToString());
            bool isFirst = true;
            MemberExpression property = null;
            for (int i = 0; i < pn.Length; i++)
            {
                var propertyName = pn[i];
                var pi = ct.GetPropInfo(propertyName);
                if (pi == null)
                {
                    return null;
                }
                if (isFirst)
                {
                    property = Expression.MakeMemberAccess(entity, pi);
                    isFirst = false;
                }
                else
                {
                    property = Expression.MakeMemberAccess(property, pi);
                }
                ct = pi.PropertyType;

                var d0 = ct.GenericTypeArguments;
                if (d0 != null && d0.Length == 1 && i < pn.Length - 1)
                {
                    Type d1 = d0[0];
                    var laex = MakeExpr(d1, pn.Skip(i + 1).ToArray(), template, level + 1);
                    var laex10 = MakeExpr(d1, pn.Skip(i + 1).ToArray(), template, level + 10);
                    //                    IEnumerable<UserVote> l0 = new List<UserVote>();

                    //                    var tt2 = Type.GetType("System.Func`2[BIT.MyCity.Database.UserVote,System.Boolean]");

                    //var ddd = l0.GetType().GetMethods();
                    //var d5 = typeof(Enumerable).GetMethod("Where");
                    //var dde = ddd.Where(m => m.Name == "Where").FirstOrDefault();


                    //                    var ddf = dde.GetParameters();
                    MethodInfo mw = typeof(Enumerable).GetMethods()
                        .Where(m => m.Name == "Any")
                        .Where(m => m.GetParameters().ToList().Count == 2)
                        .FirstOrDefault();
                    //var nn = typeof(UserVote).AssemblyQualifiedName;
                    //var tt = Type.GetType("System.Collections.Generic.IEnumerable`1[BIT.MyCity.Database.UserVote]");
                    var mwg = mw.MakeGenericMethod(d1);

                    //var laex1 = MakeExpr(typeof(UserVote), new string[] { "Comment" }, template);
                    //var l0t = l0.GetType();
                    //var l0p = Expression.Parameter(l0t, "x");

                    //var wcall1 = Expression.Call(mw, new Expression[] { l0p, laex1 });

                    MethodCallExpression wcall = Expression.Call(mwg, property, laex);

                    //MethodInfo any = typeof(Enumerable).GetMethods()
                    //    .Where(m => m.Name == "Count")
                    //    .Where(m => m.GetParameters().ToList().Count == 1)
                    //    .FirstOrDefault();

                    //MethodInfo anyg = any.MakeGenericMethod(ct);

                    //var anycall = Expression.Call(wcall, anyg);

                    ParameterExpression e1 = Expression.Parameter(d1, "y");

                    return Expression.Lambda(wcall, new ParameterExpression[] { e1 });
                }
            }
            if (property == null)
            {
                return null;
            }
            MethodInfo method1 = typeof(string).GetMethods()
                .Where(m => m.Name == "ToLower")
                .Where(m => m.GetParameters().ToList().Count == 0)
                .SingleOrDefault();
            MethodInfo method2 = typeof(string).GetMethods()
                .Where(m => m.Name == "Contains")
                .Where(m => m.GetParameters().ToList().Count == 1)
                .Where(m => m.GetParameters()[0].ParameterType == typeof(string))
                .SingleOrDefault();
            MethodCallExpression call1 = Expression.Call(property, method1);
            MethodCallExpression call2 = Expression.Call(call1, method2, new Expression[] { Expression.Constant(template.ToLower()) });
            var la = Expression.Lambda(call2, new ParameterExpression[] { entity });
            return la;
        }

        //private static IQueryable<TSource> ContainsIgnoreCasePath<TSource>(
        //    this IQueryable<TSource> query,
        //    string propertyPath,
        //    string template) where TSource : class
        //{
        //    query = query.IncludeByPath(propertyPath);
        //    var entityType = typeof(TSource);
        //    string[] pn = propertyPath.Split('.');
        //    var la = MakeExpr1<TSource>(typeof(TSource), pn, template);
        //    return query.Where(la);
        //}

        //public static IQueryable<TSource> ContainsIgnoreCase<TSource>(this IQueryable<TSource> query,
        //    string propertyName,
        //    string template)
        //{
        //    var entityType = typeof(TSource);
        //    var propertyInfo = entityType.GetPropInfo(propertyName);
        //    if (propertyInfo == null)
        //    {
        //        return query;
        //    }
        //    ParameterExpression entity = Expression.Parameter(entityType, "x");
        //    MemberExpression property = Expression.MakeMemberAccess(entity, propertyInfo);
        //    MethodInfo method1 = typeof(string).GetMethods()
        //        .Where(m => m.Name == "ToLower")
        //        .Where(m => m.GetParameters().ToList().Count == 0)
        //        .SingleOrDefault();
        //    MethodInfo method2 = typeof(string).GetMethods()
        //        .Where(m => m.Name == "Contains")
        //        .Where(m => m.GetParameters().ToList().Count == 1)
        //        .Where(m => m.GetParameters()[0].ParameterType == typeof(string))
        //        .SingleOrDefault();
        //    MethodCallExpression call1 = Expression.Call(property, method1);
        //    MethodCallExpression call2 = Expression.Call(call1, method2, new Expression[] { Expression.Constant(template.ToLower()) });
        //    var res = query.Where(Expression.Lambda<Func<TSource, bool>>(call2, entity));
        //    return res;
        //}

        //public static IQueryable<TSource> ContainsIn<TSource>(this IQueryable<TSource> query,
        //    string propertyName,
        //    string template)
        //{
        //    var entityType = typeof(TSource);

        //    // Create x=>x.PropName
        //    var propertyInfo = entityType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

        //    if (propertyInfo == null)
        //    {
        //        return query;
        //    }

        //    if (propertyInfo.DeclaringType != entityType)
        //    {
        //        propertyInfo = propertyInfo.DeclaringType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
        //    }

        //    // If we try to order by a property that does not exist in the object return the list
        //    if (propertyInfo == null)
        //    {
        //        return query;
        //    }

        //    var notMappedAttribute = propertyInfo
        //        .GetCustomAttribute<NotMappedAttribute>();

        //    if (notMappedAttribute != null)
        //    {
        //        return query;
        //    }

        //    // .Where(x => x.PropertyName.Contains(template)
        //    ParameterExpression arg = Expression.Parameter(entityType, "x");// x =>
        //                                                                    //            ParameterExpression arg1 = Expression.Parameter(typeof(string), "y");
        //    MemberExpression property = Expression.MakeMemberAccess(arg, propertyInfo); // x.PropertyName
        //    MethodInfo method1 = typeof(string).GetMethods()
        //        .Where(m => m.Name == "Contains")
        //        .Where(m => m.GetParameters().ToList().Count == 1)
        //        .Where(m => m.GetParameters()[0].ParameterType == typeof(string))
        //        //                .Where(m => m.GetParameters()[1].ParameterType == typeof(StringComparison))
        //        .SingleOrDefault();
        //    MethodInfo method2 = typeof(string).GetMethods()
        //        .Where(m => m.Name == "ToLower")
        //        .Where(m => m.GetParameters().ToList().Count == 0)
        //        .SingleOrDefault();
        //    //var d2 = d1.Where(m => m.Name == "Contains");
        //    //var d3 = d2.Where(m => m.GetParameters().ToList().Count == 2);
        //    //foreach(var d31 in d3)
        //    //{
        //    //    var d32 = d31.GetParameters()[0].ParameterType;
        //    //    var d35 = typeof(string);
        //    //    var d444 = d32 == d35;
        //    //    var d33 = d31.GetParameters()[1];
        //    //}
        //    //var d4 = d3.SingleOrDefault();
        //    //            template.Contains("123", StringComparison.OrdinalIgnoreCase);

        //    var m2Call = Expression.Call(property, method2);
        //    //MethodInfo method1 = typeof(string).GetMethods()
        //    //    .Where(m => m.Name == "Contains" && m.IsGenericMethodDefinition)
        //    //    .Where(m => m.GetParameters().ToList().Count == 2) // ensure selecting the right overload
        //    //    .Single();// x.PropertyName.Contains()
        //    //MethodInfo method1g = method1.MakeGenericMethod(new Type[]{ typeof(string), typeof(StringComparison)});  // x.PropertyName.Contains(string)
        //    //            var ex = Expression.Equal(method1g, Expression.Constant(true));
        //    MethodCallExpression method1gCall = Expression.Call(m2Call, method1, new Expression[] { Expression.Constant(template) });
        //    //Expression<Func<string, bool>> lambda = Expression.Lambda<Func<string, bool>>(method1gCall, arg1);
        //    //            Expression expr = query.Expression;
        //    //            MethodCallExpression method1gCall = Expression.Call(expr, method1g, arg1);


        //    //            var property = Expression.MakeMemberAccess(arg, propertyInfo);
        //    //LambdaExpression selector = Expression.Lambda(property, new ParameterExpression[] { arg1 });

        //    //            var methodName = "Where";

        //    //MethodInfo method = typeof(Queryable).GetMethods()
        //    //    .Where(m => m.Name == methodName && m.IsGenericMethodDefinition)
        //    //    .Where(m => m.GetParameters().ToList().Count == 2) // ensure selecting the right overload
        //    //    .Single();

        //    //The linq's OrderBy<TSource, TKey> has two generic types, which provided here
        //    //            MethodInfo genericMethod = method.MakeGenericMethod(entityType, propertyInfo.PropertyType);

        //    /* Call query.OrderBy(selector), with query and selector: x=> x.PropName
        //      Note that we pass the selector as Expression to the method and we don't compile it.
        //      By doing so EF can extract "order by" columns and generate SQL for it. */
        //    //            return (IOrderedQueryable<TSource>)genericMethod.Invoke(genericMethod, new object[] { query, selector });
        //    var res = query.Where(Expression.Lambda<Func<TSource, bool>>(method1gCall, arg));
        //    return res;
        //}


        public static IQueryable<T> MakeIQueryableBySQLFilter<T>(DbSet<T> db, T objIns, string dbName, List<GraphQlQueryFilter> filters) where T : class
        {
            IQueryable<T> obj;
            if (filters != null && filters.Count > 0)
            {
                obj = db.FromSqlRaw(MakeSQLFilter(objIns, dbName, filters));
            }
            else
            {
                obj = db;
            }
            return obj;
        }
        public static string MakeSQLFilter<T>(T objIns, string dbName, List<GraphQlQueryFilter> filters)
        {
            if (filters != null && filters.Count > 0)
            {
                string sql = "";
                foreach (var f in filters)
                {
                    System.Reflection.PropertyInfo t = objIns
                        .GetType()
                        .GetProperty(f.Name, System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
                    if (t == null)
                    {
                        throw new Exception($"Prorerty:'{f.Name}' not found in {typeof(T).Name}");
                    }
                    string not = "";
                    string q = "";
                    string name = t.Name;
                    if (t.PropertyType.Name == "String" || t.PropertyType.Name == "DateTime")
                    {
                        q = "'";
                    }
                    if (f.Not.HasValue && f.Not.Value)
                    {
                        not = "NOT ";
                    }
                    object[] attr = t.GetCustomAttributes(true);
                    foreach (var at in attr)
                    {
                        if (at.GetType().Name == "ColumnAttribute")
                        {
                            name = ((System.ComponentModel.DataAnnotations.Schema.ColumnAttribute)at).Name;
                            break;
                        }
                    }

                    if (f.Like != null)
                    {
                        if (sql.Length > 0)
                        {
                            sql += " AND";
                        }
                        sql += $" \"{name}\" {not}ILIKE '{f.Like}'";
                    }
                    if (f.In.Count > 0)
                    {
                        if (sql.Length > 0)
                        {
                            sql += " AND";
                        }
                        sql += $" \"{name}\" {not}IN (";
                        for (int i = 0; i < f.In.Count; i++)
                        {
                            sql += $"{q}{f.In[i]}{q}";
                            if (i < f.In.Count - 1)
                            {
                                sql += ", ";
                            }
                        }
                        sql += ")";
                    }
                    if (f.More != null && f.Less != null)
                    {
                        if (sql.Length > 0)
                        {
                            sql += " AND";
                        }
                        sql += $" \"{name}\" {not}BETWEEN {q}{f.More}{q} AND {q}{f.Less}{q}";
                    }
                    else if (f.More != null)
                    {
                        if (sql.Length > 0)
                        {
                            sql += " AND";
                        }
                        sql += $" \"{name}\" > {q}{f.More}{q}";
                    }
                    else if (f.Less != null)
                    {
                        if (sql.Length > 0)
                        {
                            sql += " AND";
                        }
                        sql += $" \"{name}\" < {q}{f.Less}{q}";
                    }
                    else if (f.Equal != null)
                    {
                        if (sql.Length > 0)
                        {
                            sql += " AND";
                        }
                        if (t.PropertyType.Name == "DateTime")
                        {
                            sql += $" \"{name}\" {not}BETWEEN {q}{f.Equal}{q} AND {q}{f.Equal} 23:59:59{q}";
                        }
                        else
                        {
                            sql += $" {not}\"{name}\" = {q}{f.Equal}{q}";
                        }
                    }
                    else if (f.Like == null && f.In.Count == 0)
                    {
                        if (sql.Length > 0)
                        {
                            sql += " AND";
                        }
                        sql += $" {not}\"{name}\"";
                    }
                }
                return $"SELECT * From public.\"{dbName}\" WHERE{sql}";
            }
            else
            {
                return "";
            }
        }
        public static IQueryable<T> GetRange<T>(this IQueryable<T> obj, int? take, int? skip)
        {
            if (skip.HasValue)
            {
                obj = obj.Skip(skip.Value);
            }
            if (take.HasValue)
            {
                obj = obj.Take(take.Value);
            }
            return obj;
        }
        public static IEnumerable<T> GetRange<T>(this IEnumerable<T> obj, int? take, int? skip)
        {
            if (skip.HasValue)
            {
                obj = obj.Skip(skip.Value);
            }
            if (take.HasValue)
            {
                obj = obj.Take(take.Value);
            }
            return obj;
        }
        public static async Task<List<T>> GetObjListByIds<T>(IEnumerable<long> ids, DbSet<T> db, bool noex = false) where T : BaseEntity
        {
            var res = new List<T>();
            foreach (var id in ids)
            {
                var elem = await db
                    .Where(el => el.Id == id)
                    .Where(el => !el.Deleted)
                    .FirstOrDefaultAsync();
                if (elem == null)
                {
                    if (noex == false)
                    {
                        throw new Exception($"{typeof(T).Name} {id} not found!");
                    }
                }
                else
                {
                    res.Add(elem);
                }
            }
            return res;
        }

        public static async Task<T> GetObjById<T>(long? id, DbSet<T> db) where T : BaseEntity
        {
            if (!id.HasValue)
            {
                return null;
            }
            var elem = await db
                .Where(el => el.Id == id)
                .Where(el => !el.Deleted)
                .FirstOrDefaultAsync();
            if (elem == null)
            {
                throw new Exception($"{typeof(T).Name} {id} not found!");
            }
            else
            {
                return elem;
            }
        }
        public static async Task<Database.UserOrganization> GetObjById<UserOrganization>(long? id, DocumentsContext ctx) where UserOrganization : class
        {
            if (!id.HasValue)
            {
                return null;
            }
            var elem = await ctx.UserOrganizations
                .Where(el => el.Id == id)
                .FirstOrDefaultAsync();
            if (elem == null)
            {
                throw new Exception($"{nameof(UserOrganization)} {id} not found!");
            }
            else
            {
                return elem;
            }
        }

        [Flags]
        public enum AllowedActionFlags
        {
            None = 0x00,
            Read = 0x01,
            Edit = 0x02,
            Moderate = 0x04,
            Create = 0x08,
            Tune = 0x10,
            VoteAllowed = 0x20,
            All = 0xff,
            AuthorOf = 0x100
        }

        public static List<string> ToPropertyPathList(this GraphQlKeyDictionary fields, string path)
        {
            var pl = new List<string>();
            if (fields != null)
            {
                foreach (var field in fields)
                {
                    var s = string.IsNullOrWhiteSpace(path) ? field.Key : path + "." + field.Key;
                    pl.Add(s);
                    if (field.Value != null)
                    {
                        pl.AddRange(field.Value.ToPropertyPathList(s));
                    }
                }
            }
            return pl;
        }

        public static string MemberwiseDiff<T>(this T a, T b)
        {
            string report = string.Empty;
            foreach (var prop in
                typeof(T)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(el => el.GetCustomAttribute<NotMappedAttribute>() == null)
                )
            {
                if (prop.PropertyType == typeof(string))
                {
                    var va = (string)prop.GetValue(a);
                    var vb = (string)prop.GetValue(b);
                    if ((va != null && vb != null && string.Compare(va, vb) != 0) || (va == null && vb != null) || (va != null && vb == null))
                    {
                        report += $"{prop.Name}:'{va}'=>'{vb}',";
                    }
                }
                else if (prop.PropertyType == typeof(bool)
                    || prop.PropertyType == typeof(short)
                    || prop.PropertyType == typeof(ushort)
                    || prop.PropertyType == typeof(int)
                    || prop.PropertyType == typeof(uint)
                    || prop.PropertyType == typeof(long)
                    || prop.PropertyType == typeof(ulong)
                    || prop.PropertyType == typeof(DateTime)
                    )
                {
                    var va = prop.GetValue(a);
                    var vb = prop.GetValue(b);
                    if (!va.Equals(vb))
                    {
                        report += $"{prop.Name}:'{va}'=>'{vb}',";
                    }
                }
                else if (prop.PropertyType == typeof(bool?)
                    || prop.PropertyType == typeof(short?)
                    || prop.PropertyType == typeof(ushort?)
                    || prop.PropertyType == typeof(int?)
                    || prop.PropertyType == typeof(uint?)
                    || prop.PropertyType == typeof(long?)
                    || prop.PropertyType == typeof(ulong?)
                    || prop.PropertyType == typeof(DateTime?)
                    )
                {
                    var va = prop.GetValue(a);
                    var vb = prop.GetValue(b);
                    if (va != null && vb != null)
                    {
                        if (!va.Equals(vb))
                        {
                            report += $"{prop.Name}:'{va}'=>'{vb}',";
                        }
                    }
                    else if (va != vb)
                    {
                        report += $"{prop.Name}:'{va}'=>'{vb}',";
                    }
                }
                else if (prop.PropertyType.GetInterface(nameof(IList)) != null)
                {
                    if(((IList)prop.GetValue(a)) != null && ((IList)prop.GetValue(b)) != null)
                    {
                        var ac = ((IList)prop.GetValue(a)).Count;
                        var bc = ((IList)prop.GetValue(b)).Count;
                        if (ac != bc)
                        {
                            report += $"{prop.Name}.Count:'{ac}'=>'{bc}',";
                        }
                    }
                }
                else
                {
                    var va = prop.GetValue(a);
                    var vb = prop.GetValue(b);
                    if (va != null && vb != null)
                    {
                        if (!va.Equals(vb))
                        {
                            report += $"{prop.Name}:'{va}'=>'{vb}',";
                        }
                    }
                    else if (va != vb)
                    {
                        report += $"{prop.Name}:'{va}'=>'{vb}',";
                    }
                }
            }
            return report;
        }

        public static T MemberwiseClone<T>(this T a) where T : class, new()
        {
            var b = new T();
            foreach (var prop in
                typeof(T)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(el => el.GetCustomAttribute<NotMappedAttribute>() == null)
                )
            {
                prop.SetValue(b, prop.GetValue(a));
            }
            return b;
        }

        public static bool SubsystemIsEnabledInSettings(SettingsManager sm, string ss_uid)
        {
            var v = sm.SettingValue<bool>($"subsystem.{ss_uid}.enable.");
            return v;
        }
        public static bool VoteAlwaysEditableInSettings(SettingsManager sm)
        {
            /*временный костылик для фронтэнда*/
            var v = sm.SettingValue<bool>($"subsystem.vote.always_editable");
            return v;
        }

        public static List<long> AddIfNotExist(this List<long> a, long? el)
        {
            if (a != null && el.HasValue)
            {
                if (!a.Contains(el.Value))
                {
                    a.Add(el.Value);
                }
                else
                {
                    a = new List<long> { el.Value };
                }
            }
            return a;
        }

        public static LikeStateEnum LikeProcess(LikeRequestEnum like, LikeStateEnum liked)
        {
            switch (like)
            {
                case LikeRequestEnum.DISLIKE:
                    if (liked != LikeStateEnum.DISLIKED)
                    {
                        liked = LikeStateEnum.DISLIKED;
                    }
                    break;
                case LikeRequestEnum.DISLIKE_OFF:
                    if (liked == LikeStateEnum.DISLIKED)
                    {
                        liked = LikeStateEnum.NONE;
                    }
                    break;
                case LikeRequestEnum.LIKE:
                    if (liked != LikeStateEnum.LIKED)
                    {
                        liked = LikeStateEnum.LIKED;
                    }
                    break;
                case LikeRequestEnum.LIKE_OFF:
                    if (liked == LikeStateEnum.LIKED)
                    {
                        liked = LikeStateEnum.NONE;
                    }
                    break;
            }
            return liked;
        }

        public static async Task LoadReference<TEntity, TProperty>(this TEntity obj, TProperty prop, DocumentsContext ctx)
        {
            if (obj != null)
            {
                if (prop == null && ctx.Entry(obj).State != EntityState.Detached)
                {
                    await ctx.Entry(obj)
                        .Reference(typeof(TProperty).Name)
                        .LoadAsync();
                }
            }
        }
        public static DateTime GetBuildDate(Assembly assembly)
        {
            const string BuildVersionMetadataPrefix = "+build";
            var attribute = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>();
            if (attribute?.InformationalVersion != null)
            {
                var value = attribute.InformationalVersion;
                var index = value.IndexOf(BuildVersionMetadataPrefix);
                if (index > 0)
                {
                    value = value.Substring(index + BuildVersionMetadataPrefix.Length);
                    if (DateTime.TryParseExact(value, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out var result))
                    {
                        return result;
                    }
                }
            }

            return default;
        }

        public static string Db_Version;

        public static async Task<Pagination<T>> MakePaginationResult<T>(IQueryable<T> obj, QueryOptions options)
        {
            var result = new Pagination<T>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<T>.Total).ToJsonFormat()))
            {
                result.Total = await obj.LongCountAsync();
            }
            
            if (options.RequestedFields.ContainsKey(nameof(Pagination<T>.Items).ToJsonFormat()))
            {
                obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
                result.Items = await obj.ToListAsync();
            }
            return result;
        }
        public static async Task<Pagination<T>> MakePaginationResult<T>(IEnumerable<T> obj, QueryOptions options)
        {
            var result = new Pagination<T>();
            if (options.RequestedFields.ContainsKey(nameof(Pagination<T>.Total).ToJsonFormat()))
            {
                result.Total = obj.LongCount();
            }
            obj = obj.GetRange(options.Range?.Take, options.Range?.Skip);
            if (options.RequestedFields.ContainsKey(nameof(Pagination<T>.Items).ToJsonFormat()))
            {
                result.Items = obj.ToList();
            }
            return result;
        }
        public static async Task<AllowedActionFlags> GetAllowedActionsAsync(object e, UserManager um, SettingsManager sm, DocumentsContext ctx)
        {
            AllowedActionFlags flags = AllowedActionFlags.None;
            if (e is Appeal)
            {
                flags = await ((Appeal)e).GetAllowedActions(um.ClaimsPrincipalId(), um.CurrentClaimsPrincipal()?.Claims, sm, ctx);
            }
            else if (e is Message)
            {
                flags = await ((Message)e).GetAllowedActions(um.ClaimsPrincipalId(), um.CurrentClaimsPrincipal()?.Claims, sm, ctx);
            }
            else if (e is News)
            {
                flags = await ((News)e).GetAllowedActions(um.ClaimsPrincipalId(), um.CurrentClaimsPrincipal(), sm, ctx);
            }
            else if (e is Vote)
            {
                flags = ((Vote)e).GetAllowedActions(um.ClaimsPrincipalId(), um.CurrentClaimsPrincipal());
            }
            else if (e is VoteRoot)
            {
                flags = ((VoteRoot)e).GetAllowedActions(um.ClaimsPrincipalId(), um.CurrentClaimsPrincipal());
            }

            return flags;
        }
        public static int GetDueLevel(string due)
        {
            int level = 0;
            if(due != null)
            {
                level = due.Where(el => el == '.').Count();
            }
            return level;
        }
        public static string GetMessageRootType(Message mess)
        {
            if (mess.AppealId.HasValue)
            {
                return nameof(Appeal);
            }
            else if (mess.NewsId.HasValue)
            {
                return nameof(News);
            }
            else if (mess.VoteId.HasValue)
            {
                return nameof(Vote);
            }
            else if (mess.VoteRootId.HasValue)
            {
                return nameof(VoteRoot);
            }
            else
            {
                return String.Empty;
            }
        }
        public static long GetMessageRootId(Message mess)
        {
            return mess.AppealId ?? mess.NewsId ?? mess.VoteId ?? mess.VoteRootId ?? 0;
        }
    }
}
