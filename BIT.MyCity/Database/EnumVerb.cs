namespace BIT.MyCity.Database
{
    public class EnumVerb : BaseEntity
    {
        public string Value { get; set; }
        public string ShortName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Group { get; set; }
        public string SubsystemUID { get; set; }
        public string CustomSettings { get; set; }
        public EnumVerb()
        {
            Type = nameof(EnumVerb);
        }
    }
}
