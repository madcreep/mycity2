using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using BIT.EsiaNETCore;

namespace BIT.MyCity.Database
{
    public class UserOrganization
    {
        /// <summary>
        /// Идентификатор организации
        /// </summary>
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// Идентификатор головной организации
        /// </summary>
        public long? ParentId { get; set; }

        /// <summary>
        /// Идентификатор организации во внешней системе
        /// </summary>
        public string ExtId { get; set; }

        /// <summary>
        /// Идентификатор головной организации во внешней системе
        /// </summary>
        public string ExtParentId { get; set; }

        /// <summary>
        /// Сокращенное наименование организации
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Нормализованное сокращенное наименование организации
        /// </summary>
        public string NormalizeShortName { get; set; }

        /// <summary>
        /// Полное наименование организации
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Нормализованное полное наименование организации
        /// </summary>
        public string NormalizeFullName { get; set; }

        public string Email { get; set; }

        public bool? EmailVerified { get; set; }

        /// <summary>
        /// Тип организации.
        /// </summary>
        public OrganizationType Type { get; set; }

        /// <summary>
        /// ОГРН организации
        /// </summary>
        public string Ogrn { get; set; }

        /// <summary>
        /// ИНН организации
        /// </summary>
        public string Inn { get; set; }

        /// <summary>
        /// Код организационно-правовой формы по общероссийскому классификатору
        /// организационно-правовых форм
        /// </summary>
        public string Leg { get; set; }

        /// <summary>
        /// КПП организации
        /// </summary>
        public string Kpp { get; set; }

        /// <summary>
        /// территориальная принадлежность ОГВ
        /// (только для государственных организаций,
        /// код по справочнику «Субъекты Российской федерации» (ССРФ),
        /// для Российской Федерации используется код 00
        /// </summary>
        public string AgencyTerRang { get; set; }

        /// <summary>
        /// Тип ОГВ (только для государственных организаций)
        /// </summary>
        public AgencyType? AgencyType { get; set; }

        public List<Address> Addresses { get; set; }

        public virtual List<OrganizationUserPhone> OrganizationPhones { get; set; }

        [NotMapped]
        public List<Phone> Phones {
            get => OrganizationPhones?
                .Where(el => el.UserId == null)
                .Select(el => el.Phone)
                .ToList();
        }

        public List<User2Organization> Users2Organizations { get; set; }

        public bool EqualsWithoutId(UserOrganization org)
        {
            return ShortName.Equals(org.ShortName)
                   && FullName.Equals(org.FullName)
                   && Type.Equals(org.Type)
                   && Ogrn.Equals(org.Ogrn)
                   && Inn.Equals(org.Inn)
                   && Leg.Equals(org.Leg)
                   && Kpp.Equals(org.Kpp)
                   && AgencyTerRang.Equals(org.AgencyTerRang)
                   && AgencyType.Equals(org.AgencyType);
        }

        /// <summary>
        /// Список филиалов
        /// </summary>
        public virtual List<UserOrganization> Branches { get; set; }

        /// <summary>
        /// Головная организация
        /// </summary>
        public virtual UserOrganization Parent { get; set; }

        public bool IsBrunch { get; set; }

        [NotMapped]
        public bool? Chief { get; set; }

        [NotMapped]
        public IEnumerable<long> ChiefIds => Users2Organizations?
            .Where(el => el.Chief)
            .Select(el => el.UserId)
            .ToArray();

        [NotMapped]
        public IEnumerable<long> UserIds => Users2Organizations?
            .Select(el => el.UserId)
            .ToArray();
    }
}
