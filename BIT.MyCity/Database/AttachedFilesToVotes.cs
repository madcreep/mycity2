namespace BIT.MyCity.Database
{
    public class AttachedFilesToVotes
    {
        public long VoteId { get; set; }

        public Vote Vote { get; set; }

        public long AttachedFileId { get; set; }

        public AttachedFile AttachedFile { get; set; }
    }
}
