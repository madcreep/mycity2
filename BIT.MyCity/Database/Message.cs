using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace BIT.MyCity.Database
{
    public enum MessageKindEnum
    {
        Normal = 0,
        ReasonToParentMessage = 1,
        SystemUserMessage = 2
    }
    public enum SubscribeToAnswersEnum
    {
        Not_Subscribed = 0,
        Subscribed = 1,
    }
    public class Message : TreeEntity, ICloneable, IHasAttachedFiles
    {
        //private readonly ILazyLoader _lazyLoader;

        public string Text { get; set; }
        public ApprovedEnum Approved { get; set; } = ApprovedEnum.UNDEFINED;
        public ModerationStageEnum ModerationStage { get; set; } = ModerationStageEnum.WAIT;
        public bool Public_ { get; set; }
        public bool PublicGranted { get; set; }
        public MessageKindEnum Kind { get; set; } = MessageKindEnum.Normal;
        public SubscribeToAnswersEnum SubscribeToAnswers { get; set; } = SubscribeToAnswersEnum.Not_Subscribed;

        public long? AuthorId { get; set; }
        public User Author { get; set; }
        public long? AppealExecutorId { get; set; }
        public AppealExecutor AppealExecutor { get; set; }

        public long? AppealId { get; set; }
        public Appeal Appeal { get; set; }
        public long? VoteId { get; set; }
        public Vote Vote { get; set; }
        public long? VoteRootId { get; set; }
        public VoteRoot VoteRoot { get; set; }
        public long? NewsId { get; set; }
        public News News { get; set; }

        public virtual List<AttachedFilesToMessages> AttachedFilesToMessages { get; set; } =
            new List<AttachedFilesToMessages>();

        private List<AttachedFile> _attachedFiles;

        [NotMapped]
        public List<AttachedFile> AttachedFiles
        {
            get => _attachedFiles
                   ?? AttachedFilesToMessages.Select(el => el.AttachedFile).ToList();

            set => _attachedFiles = value;
        }
        [NotMapped]
        public List<long> AttachedFilesIds { get; set; }
        //[NotMapped]
        //public List<long> AttachedFilesIdsChange { get; set; }

        public void AddFile(AttachedFile file)
        {
            if (file.Id > 0)
            {
                if (AttachedFilesToMessages.Any(el => el.AttachedFileId == file.Id))
                {
                    return;
                }
            }

            AttachedFilesToMessages.Add(new AttachedFilesToMessages
            {
                AttachedFile = file
            });
        }

        #region regard
        public List<UserRegardMessage> UserRegardMessage { get; set; } = new List<UserRegardMessage>();
        [NotMapped]
        public List<User> UsersRegard => UserRegardMessage?.Select(el => el.User).ToList() ?? new List<User>();

        public int RegardsCount { get; set; }
        public double RegardAverage { get; set; }
        [NotMapped]
        public RegardWIR RegardWIR {
            get {
                using var serviceScope = ServiceActivator.GetScope();
                var _um = (UserManager)serviceScope.ServiceProvider.GetService(typeof(UserManager));
                var userId = _um.ClaimsPrincipalId();
                if (userId != null)
                {
                    var x = UserRegardMessage.Where(el => el.UserId == userId).FirstOrDefault();
                    if (x != null)
                    {
                        return new RegardWIR { Regard = x.Regard };
                    }
                }
                return null;
            }
        }
        public int RegardSet(int regard, User user)
        {
            var a = UserRegardMessage.Where(el => el.UserId == user.Id).FirstOrDefault();
            if (a == null)
            {
                a = new UserRegardMessage { Message = this, MessageId = Id, User = user, UserId = user.Id};
                UserRegardMessage.Add(a);
            }
            a.Regard = regard;
            RegardsCount = UserRegardMessage.Where(el => el.UserId == user.Id).Count();
            RegardAverage = UserRegardMessage.Where(el => el.UserId == user.Id).Select(el => el.Regard).Average();
            return a.Regard;
        }
        #endregion
        #region view
        public List<UserViewMessage> UserViewMessage { get; set; } = new List<UserViewMessage>();
        [NotMapped]
        public List<User> UsersViewed => UserViewMessage?.Select(el => el.User).ToList() ?? new List<User>();

        public int ViewsCount { get; set; }
        [NotMapped]
        public ViewWIV ViewWIV {
            get {
                using var serviceScope = ServiceActivator.GetScope();
                var _um = (UserManager)serviceScope.ServiceProvider.GetService(typeof(UserManager));
                ViewWIV result = new ViewWIV();
                var userId = _um.ClaimsPrincipalId();
                if (userId != null)
                {
                    var userView = this.UserViewMessage.Where(el => el.UserId == userId).FirstOrDefault();
                    if (userView != null)
                    {
                        result.Viewed = userView.Viewed;
                        result.CreatedAt = userView.CreatedAt;
                        result.UpdatedAt = userView.UpdatedAt;
                    }
                }
                return result;
            }
        }
        public bool ViewGet(long userId)
        {
            var a = UserViewMessage.Where(el => el.UserId == userId).FirstOrDefault();
            return a != null ? a.Viewed : false;
        }
        public UserViewMessage ViewSet(bool viewed, User user)
        {
            var a = UserViewMessage.Where(el => el.UserId == user.Id).FirstOrDefault();
            if (a == null)
            {
                a = new UserViewMessage { Message = this, MessageId = Id, User = user, UserId = user.Id };
                UserViewMessage.Add(a);
            }
            a.Viewed = viewed;
            ViewsCount = UserViewMessage.Where(el => el.Viewed).Count();
            return a;
        }
        #endregion
        #region like
        public List<UserLikeMessage> UserLikeMessage { get; set; } = new List<UserLikeMessage>();
        [NotMapped]
        public List<User> UsersLiked => UserLikeMessage?.Select(el => el.User).ToList() ?? new List<User>();

        public int LikesCount { get; set; }
        public int DislikesCount { get; set; }
        [NotMapped]
        public LikeWIL LikeWIL {
            get {
                using var serviceScope = ServiceActivator.GetScope();
                var _um = (UserManager)serviceScope.ServiceProvider.GetService(typeof(UserManager));
                LikeWIL result = new LikeWIL();
                var userId = _um.ClaimsPrincipalId();
                if (userId != null)
                {
                    var userLike = this.UserLikeMessage.Where(el => el.UserId == userId).FirstOrDefault();
                    if (userLike != null)
                    {
                        result.Liked = userLike.Liked;
                    }
                }
                return result;
            }
        }
        public LikeStateEnum LikeGet(long userId)
        {
            var a = UserLikeMessage.Where(el => el.UserId == userId).FirstOrDefault();
            return a != null ? a.Liked : LikeStateEnum.NONE;
        }
        public LikeStateEnum LikeSet(LikeRequestEnum liked, User user)
        {
            var a = UserLikeMessage.Where(el => el.UserId == user.Id).FirstOrDefault();
            if (a == null)
            {
                a = new UserLikeMessage { Message = this, MessageId = Id, User = user, UserId = user.Id };
                UserLikeMessage.Add(a);
            }
            a.Liked = BaseConstants.LikeProcess(liked, a.Liked);
            LikesCount = UserLikeMessage.Where(el => el.Liked == LikeStateEnum.LIKED).Count();
            DislikesCount = UserLikeMessage.Where(el => el.Liked == LikeStateEnum.DISLIKED).Count();
            return a.Liked;
        }
        #endregion

        //public Message(ILazyLoader lazyLoader)
        //    : this()
        //{
        //    _lazyLoader = lazyLoader;
        //}
        public Message()
        {
            Type = nameof(Message);
        }

        public object Clone()
        {
            var clone = (Message)this.MemberwiseClone();
            if (AttachedFiles != null)
            {
                clone.AttachedFiles = new List<AttachedFile>();
                clone.AttachedFiles.AddRange(this.AttachedFiles);
            }
            if (UserLikeMessage != null)
            {
                clone.UserLikeMessage = new List<UserLikeMessage>();
                clone.UserLikeMessage.AddRange(this.UserLikeMessage);
            }
            return clone;
        }

        public async Task<Message> MakeChild(DocumentsContext ctx)
        {
            await this.LoadReference(Appeal, ctx);
            await this.LoadReference(VoteRoot, ctx);
            await this.LoadReference(Vote, ctx);
            await this.LoadReference(News, ctx);
            var child = new Message
            {
                ParentId = Id,
                ParentType = Type,
                Appeal = Appeal,
                VoteRoot = VoteRoot,
                Vote = Vote,
                News = News,
            };
            return child;
        }
    }
    public class ESMessage
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public ESMessage(Message obj)
        {
            Text = obj.Text;
            Id = obj.Id;
        }
    }
}
