using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using BIT.MyCity.Extensions;
using GraphQL;
using Newtonsoft.Json;

namespace BIT.MyCity.Database
{
    public class Settings : IUpdatedEntity, IHasAttachedFiles
    {
        [Key]
        public string Key { get; set; }
        
        public string Value { get; set; }

        /// <summary>
        /// Тип элемента (вкладка/группа/значение)
        /// </summary>
        public SettingNodeType SettingNodeType { get; set; } = SettingNodeType.Value;

        /// <summary>
        /// Вес в пределах уровня иерархии
        /// </summary>
        public int Weight { get; set; }

        public RightLevel ReadingRightLevel { get; set; }

        public string ValueType { get; set; }

        public bool Nullable { get; set; }

        public SettingType SettingType { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// Допустимые значения
        /// </summary>
        public SettingsValueItem[] Values { get; set; }

        public List<AttachedFilesToSettings> AttachedFilesToSettings { get; set; }

        private List<AttachedFile> _attachedFiles;

        [NotMapped]
        public List<AttachedFile> AttachedFiles
        {
            get => _attachedFiles
                   ?? AttachedFilesToSettings?.Select(el => el.AttachedFile).ToList();

            set => _attachedFiles = value;
        }
        [NotMapped]
        public List<long> AttachedFilesIds { get; set; } = new List<long>();
        //[NotMapped]
        //public List<long> AttachedFilesIdsChange { get; set; } = new List<long>();

        public void AddFile(AttachedFile file)
        {
            AttachedFilesToSettings.Add(new AttachedFilesToSettings
            {
                AttachedFile = file
            });
        }

        public DateTime CreatedAt { get; set; }

        public DateTime? UpdatedAt { get; set; }
    }

    public class SettingsValueItem : IEquatable<SettingsValueItem>
    {
        public string Value { get; set; }

        public Type ValueType { get; set; }

        private object ValueObject(Type type)
        {
            object result;

            if (type.IsPrimitive || type.IsEnum || type == typeof(string))
            {
                if (type.IsEnum)
                {
                    result = Enum.Parse(type, Value);
                }
                else
                {
                    result = Convert.ChangeType(Value, type);
                }
            }
            else
            {
                result = (Value == null ? null : JsonConvert.DeserializeObject(Value, type));
            }

            return result;
        }

        public string DisplayText { get; set; }

        public SettingsValueItem()
        {
        }

        protected SettingsValueItem(object value, Type valueType, string displayText)
        {
            if (value == null)
            {
                Value = null;
            }
            else
            {
                var type = value.GetType();

                if (type.IsPrimitive || type.IsEnum || type == typeof(string))
                {
                    Value = value?.ToString();
                }
                else
                {
                    Value = value?.SerializeJson(false);
                }
            }

            DisplayText = displayText;

            ValueType = valueType;
        }

        public bool Equals(SettingsValueItem other)
        {
            if (ReferenceEquals(this, other)) return true;

            if (ReferenceEquals(null, other)) return false;

            if (ValueType != other.ValueType) return false;

            var thisValueObject = Convert.ChangeType(ValueObject(ValueType), ValueType);

            var otherValueObject = Convert.ChangeType(other.ValueObject(ValueType), ValueType);

            Console.WriteLine($"SettingsValueItem.Equals : {thisValueObject?.GetType().Name} | {otherValueObject?.GetType().Name}");

            return ((thisValueObject == null && otherValueObject == null)
                    || (thisValueObject != null && thisValueObject.Equals(otherValueObject)))
                    && DisplayText == other.DisplayText;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((SettingsValueItem)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Value != null ? Value.GetHashCode() : 0) * 397) ^
                       (DisplayText != null ? DisplayText.GetHashCode() : 0);
            }
        }
    }

    public class SettingsValueItem<T> : SettingsValueItem
    {
        public SettingsValueItem(T value, string displayText)
            : base(value, typeof(T), displayText)
        {
            if (ValueType.IsNullable())
            {
                var innerType = Nullable.GetUnderlyingType(typeof(T));

                if (innerType != null)
                {

                    var @interface = (innerType?.GetInterfaces())
                        .FirstOrDefault(el =>
                            el.IsGenericType && el.GetGenericTypeDefinition() != typeof(IEquatable<>));

                    if (@interface?.GetGenericArguments().First() != innerType)
                    {
                        throw new ArgumentException($"\"{innerType.FullName}\" must be an equatable type");
                    }

                    return;
                }
            }

            {
                if (ValueType.IsEnum)
                {
                    return;
                }

                var interfaces = ValueType.GetInterfaces();

                if (interfaces.All(el => el != typeof(IEquatable<T>)))
                {
                    throw new ArgumentException($"\"{ValueType.FullName}\" must be an equatable type");
                }
            }
        }
    }
}
