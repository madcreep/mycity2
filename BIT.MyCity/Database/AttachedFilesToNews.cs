namespace BIT.MyCity.Database
{
    public class AttachedFilesToNews
    {
        public long NewsId { get; set; }

        public News News { get; set; }

        public long AttachedFileId { get; set; }

        public AttachedFile AttachedFile { get; set; }
    }
}
