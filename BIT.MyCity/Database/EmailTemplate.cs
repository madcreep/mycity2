using System;
using System.ComponentModel.DataAnnotations;

namespace BIT.MyCity.Database
{
    public class EmailTemplate : IUpdatedEntity
    {
        [Key]
        public string Key { get; set; }

        public string Template { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
}
