﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace BIT.MyCity.Database
{
    public class AppealExecutor : TreeEntity
    {
        [NotMapped]
        public List<long> AppealIds { get; set; } = new List<long>();
        [NotMapped]
        public List<long> AppealIdsChange { get; set; } = new List<long>();
        [NotMapped]
        public List<Appeal> Appeals => Appeal2Executors?.Select(el => el.Appeal).ToList() ?? new List<Appeal>();
        public List<Appeal2Executor> Appeal2Executors { get; set; } = new List<Appeal2Executor>();

        [NotMapped]
        public List<long> AppealPrincipalIds { get; set; } = new List<long>();
        [NotMapped]
        public List<long> AppealPrincipalIdsChange { get; set; } = new List<long>();
        [NotMapped]
        public List<Appeal> AppealsPrincipal => Appeal2PrincipalExecutors?.Select(el => el.Appeal).ToList() ?? new List<Appeal>();
        public List<Appeal2PrincipalExecutor> Appeal2PrincipalExecutors { get; set; } = new List<Appeal2PrincipalExecutor>();

        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Department { get; set; }
        public string DepartmentId { get; set; }
        public string Position { get; set; }
        public string Organization { get; set; }
        public string ExtId { get; set; }
        public User User { get; set; }
        public long? UserId { get; set; }
        public List<Message> Messages { get; set; }
        public AppealExecutor()
        {
            Type = nameof(AppealExecutor);
        }
    }
}
