using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BIT.MyCity.Database
{
    public class UserModeratorAppeal : BaseLinkUser
    {
        public long AppealId { get; set; }
        public Appeal Appeal { get; set; }
    }


    public abstract class UserModeratorSubsystem : BaseLinkUser
    {
        public long SubsystemId { get; set; }
        public Subsystem Subsystem { get; set; }
        [NotMapped]
        public string SubsystemUid { get; set; }
    }
    public class UserModeratorTopic : UserModeratorSubsystem
    {
        public long TopicId { get; set; }
        public Topic Topic { get; set; }
    }
    public class UserRegion : UserModeratorSubsystem
    {
        public long RegionId { get; set; }
        public Region Region { get; set; }
    }
    public class UserRubric : UserModeratorSubsystem
    {
        public long RubricId { get; set; }
        public Rubric Rubric { get; set; }
    }
    public class UserTerritory : UserModeratorSubsystem
    {
        public long TerritoryId { get; set; }
        public Territory Territory { get; set; }
    }
}
