using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BIT.MyCity.Database
{
    public class VoteItem : BaseEntity, IHasAttachedFiles
    {
        public int? Number { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }

        public long? VoteId { get; set; }
        public Vote Vote { get; set; }

        public long? NextVoteId { get; set; }
        public Vote NextVote { get; set; }

        public int Counter { get; set; }

        public List<AttachedFilesToVoteItems> AttachedFilesToVoteItems { get; set; }

        [NotMapped]
        public List<AttachedFile> AttachedFiles { get; set; }
        [NotMapped]
        public List<long> AttachedFilesIds { get; set; }
        //[NotMapped]
        //public List<long> AttachedFilesIdsChange { get; set; }

        public void AddFile(AttachedFile file)
        {
            throw new System.NotImplementedException();
        }

        public VoteItem()
        {
            Type = nameof(VoteItem);
        }
    }
}
