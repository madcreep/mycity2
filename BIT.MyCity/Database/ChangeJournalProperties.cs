using System.ComponentModel.DataAnnotations.Schema;

namespace BIT.MyCity.Database
{
    public class ChangeJournalProperties
    {
        public long Id { get; set; }

        public long ChangeJournalItemId { get; set; }

        public string Tag { get; set; }

        public string FriendlyName { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }
        
        public string NewValueFriendly { get; set; }
    }
}
