using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface;
using BIT.MyCity.Database;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Subsystems
{
    public interface IExternalSubsystem
    {
        string SystemKey { get; }
        bool IsEnable { get; }
        IEnumerable<Settings> DefaultSettings { get; }
        IEnumerable<ClaimInfoModel> ClaimInfos { get; }
        IEnumerable<Claim> GetCitizenSubsystemClaims(User user);
        Task SendNewMessageEmail(Message message);
        void AddGqlQueryFields(ObjectGraphType<object> gql);
        void AddGqlMutationFields(ObjectGraphType gql);
        void RegisterServices(IServiceCollection services);
    }
}
