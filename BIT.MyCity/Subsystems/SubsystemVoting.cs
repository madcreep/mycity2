using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Subsystems
{
    public class SubsystemVoting : SubsystemBase
    {
        internal static readonly string DefaultName = "Проведение голосований";
        internal static readonly string DefaultDescription = "Проведение голосований онлайн";

        internal const string BaseSystemKey = "vote";

        public override bool IsEnable
        {
            get
            {
                var settingsManager = ServiceProviderServiceExtensions.GetRequiredService<SettingsManager>(ServiceProvider);

                return settingsManager.SettingValue<bool>($"subsystem.{SettingsKey.Enable}");
            }
        }

        public SubsystemVoting(IServiceProvider serviceProvider, IServiceScopeFactory scopeFactory)
            : base(serviceProvider, scopeFactory)
        { }

        public static class SettingsKey
        {
            public static readonly string VotingSettings = $"{BaseSystemKey}.";
            public static readonly string Enable = $"{VotingSettings}enable.";
            public static readonly string Name = $"{VotingSettings}name.";
            public static readonly string Description = $"{VotingSettings}description.";
            public static readonly string AlwaysEditable = $"{VotingSettings}always_editable.";
            public static readonly string SubscribeToAnswers = $"{VotingSettings}subscribe_to_answers.";
            public static readonly string SubscribeToAnswersToAnswers = $"{VotingSettings}subscribe_to_answers_to_answers.";
        }

        public override string SystemKey => BaseSystemKey;
        protected override IEnumerable<Settings> SubsystemDefaultSettings => new[]
        {
            new Settings
            {
                Key = SettingsKey.Enable,
                Description = "Активирована",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKey.Name,
                Description = "Название",
                Value = DefaultName,
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKey.Description,
                Description = "Описание",
                Value = DefaultDescription,
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKey.AlwaysEditable,
                Description = "Редактирование разрешено всегда",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKey.SubscribeToAnswers,
                Description = "Уведомления о комментариях на новости",
                Value = SubscribeToAnswersEnum.Not_Subscribed.ToString(),
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(SubscribeToAnswersEnum).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 5,
                Values = new SettingsValueItem[]
                {
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Not_Subscribed, "Не отправлять"),
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Subscribed, "Отправлять")
                }
            },
            new Settings
            {
                Key = SettingsKey.SubscribeToAnswersToAnswers,
                Description = "Уведомления об ответах на комментарии на новости",
                Value = SubscribeToAnswersEnum.Not_Subscribed.ToString(),
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(SubscribeToAnswersEnum).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 6,
                Values = new SettingsValueItem[]
                {
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Not_Subscribed, "Не отправлять"),
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Subscribed, "Отправлять")
                }
            },
        };

        public override IEnumerable<ClaimInfoModel> ClaimInfos
        {
            get
            {
                var subsystemName = SettingValue($"subsystem.{SettingsKey.Name}");

                var settingsManager = ServiceProvider.GetRequiredService<SettingsManager>();

                return new[]
                {
                    new ClaimInfoModel(ClaimName.Subsystem,
                        new[]
                        {
                            new ClaimValueModel(1, $"{BaseSystemKey}.{SSClaimSuffix.Ss.available}",
                                "Доступ разрешен"),
                            new ClaimValueModel(2, $"{BaseSystemKey}.{SSClaimSuffix.Ss.allowed}",
                                "Разрешено голосовать"),
                            new ClaimValueModel(2, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_voting}",
                                "Создание голосований"),
                            new ClaimValueModel(3, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_message}",
                                $"Создание сообщений")
                        },
                        $"Доступ к подсистеме \"{subsystemName}\"",
                        ClaimEntity.Role, ClaimEntity.User),

                    new ClaimInfoModel(ClaimName.Manage,
                        new[]
                        {
                            new ClaimValueModel(1, $"{BaseSystemKey}.{SSClaimSuffix.Mu.control}",
                                $"Планирование и запуск голосований"),
                            new ClaimValueModel(1, $"{BaseSystemKey}.{SSClaimSuffix.Mu.rubrics}",
                                $"Настройка справочника \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitle)}\""),
                            new ClaimValueModel(2, $"{BaseSystemKey}.{SSClaimSuffix.Mu.territories}",
                                $"Настройка справочника \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitle)}\""),
                            new ClaimValueModel(3, $"{BaseSystemKey}.{SSClaimSuffix.Mu.regions}",
                                $"Настройка справочника  \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitle)}\""),
                            new ClaimValueModel(4, $"{BaseSystemKey}.{SSClaimSuffix.Mu.topics}",
                                $"Настройка справочника \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitle)}\"")
                        },
                        $"Разрешения управления для подсистемы \"{subsystemName}\"",
                        ClaimEntity.Role, ClaimEntity.User),

                    new ClaimInfoModel(ClaimName.Moderate,
                        new[]
                        {
                            new ClaimValueModel(1, $"{BaseSystemKey}.{SSClaimSuffix.Md.enable}",
                                "Модерирование голосований")
                        },
                        $"Разрешения модерирования для подсистемы \"{subsystemName}\"",
                        ClaimEntity.Role, ClaimEntity.User)
                };
            }
        }

        public override IEnumerable<Claim> GetCitizenSubsystemClaims(User user)
        {
            var result = new List<Claim>();

            result.AddRange(new List<Claim>
            {
                new Claim(ClaimName.Subsystem, $"{BaseSystemKey}.{SSClaimSuffix.Ss.available}"),
                new Claim(ClaimName.Subsystem, $"{BaseSystemKey}.{SSClaimSuffix.Ss.allowed}"),
                new Claim(ClaimName.Subsystem, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_message}")
            });


            if (user.ProfileConfirmed && user.Users2Organizations.Any())
            {
                result.Add(new Claim(ClaimName.Subsystem, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_voting}"));
            }

            return result;
        }

        public override async Task SendNewMessageEmail(Message message)
        {
            if (message.VoteId.HasValue)
            {
                using var scope = ScopeFactory.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                var vote = await context.Votes
                    .Include(el => el.VoteRoot)
                    .Where(el => el.Id == message.VoteId.Value)
                    .FirstOrDefaultAsync();

                if (vote?.VoteRoot == null || vote.VoteRoot.SubscribeToAnswers == SubscribeToAnswersEnum.Not_Subscribed)
                {
                    return;
                }

                var moderators = (await GetModerators())
                    .Where(el => !string.IsNullOrWhiteSpace(el.Email))
                    .ToArray();

                if (moderators.Any())
                {
                    var mailMessage = new MailMessage
                    {
                        Subject = $"Голосование от {vote.CreatedAt:dd.MM.yyyy}",
                        TemplateKey = "messageToVote",
                        FilesIds = message.AttachedFiles?
                            .Select(el => el.Id)
                            .ToArray()
                    };

                    var emailService = ServiceProvider.GetRequiredService<MailService>();

                    foreach (var moderator in moderators)
                    {
                        mailMessage.Email = moderator.Email;

                        mailMessage.AddingEntity = new List<EmailEntityMetadata>
                        {
                            EmailEntityMetadata.Generate(context, message),
                            EmailEntityMetadata.Generate(context, vote),
                            EmailEntityMetadata.Generate(context, moderator)
                        };

                        await emailService.SendEmailAsync(mailMessage);
                    }
                }
            }
        }

        private async Task<IEnumerable<User>> GetModerators()
        {
            using var scope = ScopeFactory.CreateScope();

            var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

            var ssAvailable = $"{BaseSystemKey}.{SSClaimSuffix.Ss.available}";

            var mdEnable = $"{BaseSystemKey}.{SSClaimSuffix.Md.enable}";

            var result = await context.Users
                .Where(el =>
                    (el.IdentityClaims.Any(x => x.ClaimType == ClaimName.Subsystem && x.ClaimValue == ssAvailable)
                     || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y => y.ClaimType == ClaimName.Subsystem && y.ClaimValue == ssAvailable)))
                    && (el.IdentityClaims.Any(x => x.ClaimType == ClaimName.Moderate && x.ClaimValue == mdEnable)
                        || el.UserRole.Any(x => x.Role.IdentityClaims.Any(y => y.ClaimType == ClaimName.Moderate && y.ClaimValue == mdEnable))))
                .ToArrayAsync();

            return result;
        }
    }
}
