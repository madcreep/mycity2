using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using GraphQL.Types;
using GraphQL.Utilities;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Subsystems
{
    public interface ISubsystem
    {
        string SystemKey { get; }

        bool IsEnable { get; }

        IEnumerable<Settings> DefaultSettings { get; }

        IEnumerable<ClaimInfoModel> ClaimInfos { get; }

        IEnumerable<Claim> GetCitizenSubsystemClaims(User user);

        Task SendNewMessageEmail(Message message);
    }

    public abstract class SubsystemBase : IExternalSubsystem
    {
        public abstract string SystemKey { get; }

        public IEnumerable<Settings> DefaultSettings
        {
            get
            {
                var claimInfoModels = SubsystemDefaultSettings;

                var result = claimInfoModels as Settings[] ?? claimInfoModels.ToArray();

                foreach (var claimInfo in result)
                {
                    claimInfo.Key = $"subsystem.{claimInfo.Key}";
                }

                return result;
            }
        }

        protected abstract IEnumerable<Settings> SubsystemDefaultSettings { get; }

        public abstract IEnumerable<ClaimInfoModel> ClaimInfos { get; }

        protected readonly IServiceProvider ServiceProvider;

        protected readonly IServiceScopeFactory ScopeFactory;

        internal SubsystemBase(IServiceProvider serviceProvider, IServiceScopeFactory scopeFactory)
        {
            ServiceProvider = serviceProvider;
            ScopeFactory = scopeFactory;
        }

        internal string SettingValue(string key)
        {
            var settingsManager = ServiceProviderExtensions.GetRequiredService<SettingsManager>(ServiceProvider);

            return settingsManager.SettingValue(key);
        }

        public abstract IEnumerable<Claim> GetCitizenSubsystemClaims(User user);

        public abstract bool IsEnable { get; }

        public abstract Task SendNewMessageEmail(Message message);
        public virtual void AddGqlQueryFields(ObjectGraphType<object> gql)
        { }

        public virtual void AddGqlMutationFields(ObjectGraphType gql)
        {}

        public virtual void RegisterServices(IServiceCollection services)
        { }
    }

    public static class SSUID
    {
        public static readonly string citizens = "citizens";
        public static readonly string organizations = "organizations";
        public static readonly string vote = "vote";
        public static readonly string news = "news";
    }

    public static class SSClaimSuffix
    {
        public static class Ss
        {
            public static readonly string available = "available";
            public static readonly string create_appeal = "create_appeal";
            public static readonly string create_message = "create_message";
            public static readonly string create_voting = "create_voting";
            public static readonly string create_news = "create_news";
            public static readonly string allowed = "allowed";
            public static readonly string executor = "executor";
            public static readonly string feedback_admin = "fbadm";
        }
        public static class Mu
        {
            public static readonly string rubrics = "rubrics";
            public static readonly string territories = "territories";
            public static readonly string regions = "regions";
            public static readonly string topics = "topics";
            public static readonly string control = "control";
        }
        public static class Md
        {
            public static readonly string enable = "enable";
            public static readonly string super = "super";
            public static readonly string all_rubrics = "all_rubrics";
            public static readonly string all_territories = "all_territories";
            public static readonly string all_regions = "all_regions";
            public static readonly string all_topics = "all_topics";
        }
    }
}
