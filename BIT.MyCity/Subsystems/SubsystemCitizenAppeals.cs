using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
//using GraphQL.Utilities;
using log4net;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Subsystems
{
    public class SubsystemCitizenAppeals : SubsystemBase
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(SubsystemCitizenAppeals));

        internal static readonly string DefaultName = "Обращения граждан";
        internal static readonly string DefaultDescription = "тут должно быть описание";

        public const string BaseSystemKey = "citizens";

        public override string SystemKey => BaseSystemKey;

        public override bool IsEnable
        {
            get
            {
                var settingsManager = ServiceProviderServiceExtensions.GetRequiredService<SettingsManager>(ServiceProvider);

                return settingsManager.SettingValue<bool>($"subsystem.{SettingsKey.Enable}");
            }
        }

        public override async Task SendNewMessageEmail(Message message)
        {
            try
            {
                if (message.AppealId.HasValue)
                {
                    using var scope = ScopeFactory.CreateScope();

                    var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                    var appeal = message.Appeal
                                 ?? await context.Appeals
                                     .Include(el => el.Subsystem)
                                     .Include(el => el.Author)
                                     .Where(el => el.Id == message.Id)
                                     .FirstOrDefaultAsync();

                    if (appeal == null
                        || appeal.Subsystem?.UID != BaseSystemKey
                        || (appeal.AppealType == AppealTypeEnum.NORMAL &&
                            appeal.SubscribeToAnswers == SubscribeToAnswersEnum.Not_Subscribed))
                    {
                        return;
                    }

                    var emailEntityMetadata = new List<EmailEntityMetadata>
                    {
                        EmailEntityMetadata.Generate(context, appeal),
                        EmailEntityMetadata.Generate(context, message)
                    };

                    if (appeal.Author != null)
                    {
                        emailEntityMetadata.Add(EmailEntityMetadata.Generate(context, appeal.Author));
                    }

                    var enumVerbManager = scope.ServiceProvider.GetRequiredService<EnumVerbManager>();

                    var status = await enumVerbManager.GetEnumVerbAsync(
                        appeal.SiteStatus,
                        appeal.AppealType == AppealTypeEnum.NORMAL ? appeal.Subsystem?.UID : null,
                        appeal.AppealType == AppealTypeEnum.NORMAL ? "appealStatus" : "feedbackStatus"
                    );

                    if (!status.CustomSettings.TryDeserializeJson<AppealStatusSettings>(out var statusSettings))
                    {
                        statusSettings = null;
                    }

                    var statusOptions = statusSettings?.StatusOptions;

                    var mailMessage = new MailMessage
                    {
                        Subject = $"Обращение от {appeal.CreatedAt.ToLocalTime():dd.MM.yyyy}",
                        TemplateKey = appeal.AppealType == AppealTypeEnum.NORMAL
                            ? "messageToAppealCitizen"
                            : "appealFeedback",
                        Email = string.IsNullOrWhiteSpace(appeal.Email) ? appeal.Author.Email : appeal.Email,
                        AddingEntity = emailEntityMetadata,
                        TemplateModel = new
                        {
                            SiteStatus = appeal.SiteStatus,
                            SiteStatusName = status.Name
                        },
                        FilesIds = message.AttachedFiles?
                            .Select(el => el.Id)
                            .ToArray(),
                        WithDeliveryNotice = statusOptions?.Email?.WithDeliveryNotice ?? false,
                        DuplicateToSender = statusOptions?.Email?.DuplicateToSender ?? false

                    };

                    var emailService = scope.ServiceProvider.GetRequiredService<MailService>();

                    await emailService.SendEmailAsync(mailMessage);
                }
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка отправки Email", ex);
            }
        }

        public SubsystemCitizenAppeals(IServiceProvider serviceProvider, IServiceScopeFactory scopeFactory)
            : base(serviceProvider, scopeFactory)
        {
        }

        public static class SettingsKey
        {
            public static readonly string CitizenSettings = $"{BaseSystemKey}.";
            public static readonly string Enable = $"{CitizenSettings}enable.";
            public static readonly string Name = $"{CitizenSettings}name.";
            public static readonly string Description = $"{CitizenSettings}description.";
            
            public static readonly string AppealUrlTemplate = $"{CitizenSettings}appeal_url_template.";
            public static readonly string CitizenSettingsWeb = $"{CitizenSettings}web.";

            public static readonly string CitizenSettingsExecutorFeature = $"{CitizenSettings}executor_feature.";
            public static readonly string CitizenSettingsRegisterWait = $"{CitizenSettings}autoregister_wait.";
            public static readonly string CitizenSettingsWebAddAppeal = $"{CitizenSettingsWeb}appeal_add.";
            public static readonly string CitizenSettingsWebEditAppeal = $"{CitizenSettingsWeb}appeal_edit.";
            public static readonly string CitizenSettingsWebDelAppeal = $"{CitizenSettingsWeb}appeal_del.";
            public static readonly string CitizenSettingsWebStatusesChange = $"{CitizenSettingsWeb}status_edit.";
            public static readonly string CitizenSettingsAppealWebAnswer = $"{CitizenSettingsWeb}appeal_web_answer.";
            public static readonly string SubscribeToAnswers = $"{CitizenSettings}subscribe_to_answers.";
            public static readonly string SubscribeToAnswersToAnswers = $"{CitizenSettings}subscribe_to_answers_to_answers.";

            //public static readonly string CitizenSettingsNotDbAppeals = $"{CitizenSettings}not_db_appeals.";
            //public static readonly string CitizenSettingsNotDbAppealsEnable = $"{CitizenSettingsNotDbAppeals}enable.";
            //public static readonly string CitizenSettingsNotDbAppealsOnlyAuthorized = $"{CitizenSettingsNotDbAppeals}only_auth.";
            //public static readonly string CitizenSettingsNotDbAppealsHostUrl = $"{CitizenSettingsNotDbAppeals}host_url.";
            //public static readonly string CitizenSettingsNotDbAppealsHostLogin = $"{CitizenSettingsNotDbAppeals}host_login.";
            //public static readonly string CitizenSettingsNotDbAppealsHostPassword = $"{CitizenSettingsNotDbAppeals}host_password.";
            //public static readonly string CitizenSettingsNotDbAppealsAnswerDocGroups = $"{CitizenSettingsNotDbAppeals}answer_dg_due.";
            //public static readonly string CitizenSettingsNotDbAppealsAnswerLinks = $"{CitizenSettingsNotDbAppeals}answer_link_isn.";
        }

        protected override IEnumerable<Settings> SubsystemDefaultSettings => new[]
        {
            new Settings
            {
                Key = SettingsKey.Enable,
                Description = "Активирована",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKey.Name,
                Description = "Название",
                Value = DefaultName,
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKey.Description,
                Description = "Описание",
                Value = DefaultDescription,
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKey.AppealUrlTemplate,
                Description = "Шаблон URL для обращения",
                Value = "http://localhost/{appealId}",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKey.CitizenSettingsWebAddAppeal,
                Description = "Добавление обращений из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 5
            },
            new Settings
            {
                Key = SettingsKey.CitizenSettingsWebEditAppeal,
                Description = "Редактирование обращений из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 6
            },
            new Settings
            {
                Key = SettingsKey.CitizenSettingsWebDelAppeal,
                Description = "Удаление обращений из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 7
            },
            new Settings
            {
                Key = SettingsKey.CitizenSettingsWebStatusesChange,
                Description = "Изменение статусов из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 8
            },
            new Settings
            {
                Key = SettingsKey.CitizenSettingsAppealWebAnswer,
                Description = "Ответ на обращение из WEB",
                Value = "true",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 10
            },
            new Settings
            {
                Key = SettingsKey.SubscribeToAnswers,
                Description = "Уведомления о комментариях на новости",
                Value = SubscribeToAnswersEnum.Not_Subscribed.ToString(),
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(SubscribeToAnswersEnum).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 20,
                Values = new SettingsValueItem[]
                {
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Not_Subscribed, "Не отправлять"),
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Subscribed, "Отправлять")
                }
            },
            new Settings
            {
                Key = SettingsKey.SubscribeToAnswersToAnswers,
                Description = "Уведомления об ответах на комментарии на новости",
                Value = SubscribeToAnswersEnum.Not_Subscribed.ToString(),
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(SubscribeToAnswersEnum).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 30,
                Values = new SettingsValueItem[]
                {
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Not_Subscribed, "Не отправлять"),
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Subscribed, "Отправлять")
                }
            },
            new Settings
            {
                Key = SettingsKey.CitizenSettingsRegisterWait,
                Description = "Ожидание ручной регистрации перед автоматической (минут, 0 = авто, -1 = только ручная)",
                Value = "0",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(int).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 40
            },
            new Settings
            {
                Key = SettingsKey.CitizenSettingsExecutorFeature,
                Description = "Функционал Исполнителей",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 50
            }

            //new Settings
            //{
            //    Key = SettingsKey.CitizenSettingsNotDbAppeals,
            //    Description = "Поиск во внешней БД",
            //    ReadingRightLevel = RightLevel.AdministratorSettings,
            //    SettingNodeType = SettingNodeType.Group,
            //    Weight = 10
            //},
            //new Settings
            //{
            //    Key = SettingsKey.CitizenSettingsNotDbAppealsEnable,
            //    Description = "Активирован",
            //    Value = "false",
            //    ReadingRightLevel = RightLevel.Unauthorized,
            //    ValueType = typeof(bool).FullName,
            //    Nullable = false,
            //    SettingType = SettingType.Value,
            //    Weight = 1
            //},
            //new Settings
            //{
            //    Key = SettingsKey.CitizenSettingsNotDbAppealsOnlyAuthorized,
            //    Description = "Только авторизованные",
            //    Value = "true",
            //    ReadingRightLevel = RightLevel.Unauthorized,
            //    ValueType = typeof(bool).FullName,
            //    Nullable = false,
            //    SettingType = SettingType.Value,
            //    Weight = 2
            //},
            //new Settings
            //{
            //    Key = SettingsKey.CitizenSettingsNotDbAppealsHostUrl,
            //    Description = "URL хоста Rest Api",
            //    Value = "http://",
            //    ReadingRightLevel = RightLevel.AdministratorSettings,
            //    ValueType = typeof(string).FullName,
            //    Nullable = false,
            //    SettingType = SettingType.Value,
            //    Weight = 3
            //},
            //new Settings
            //{
            //    Key = SettingsKey.CitizenSettingsNotDbAppealsHostLogin,
            //    Description = "Логин авторизации",
            //    Value = "",
            //    ReadingRightLevel = RightLevel.AdministratorSettings,
            //    ValueType = typeof(string).FullName,
            //    Nullable = false,
            //    SettingType = SettingType.Value,
            //    Weight = 4
            //},
            //new Settings
            //{
            //    Key = SettingsKey.CitizenSettingsNotDbAppealsHostPassword,
            //    Description = "Пароль авторизации",
            //    Value = "",
            //    ReadingRightLevel = RightLevel.AdministratorSettings,
            //    ValueType = typeof(string).FullName,
            //    Nullable = false,
            //    SettingType = SettingType.Value,
            //    Weight = 5
            //},
            //new Settings
            //{
            //    Key = SettingsKey.CitizenSettingsNotDbAppealsAnswerDocGroups,
            //    Description = "DUE групп документов ответа (разделитель '|')",
            //    Value = "",
            //    ReadingRightLevel = RightLevel.AdministratorSettings,
            //    ValueType = typeof(string).FullName,
            //    Nullable = false,
            //    SettingType = SettingType.Value,
            //    Weight = 6
            //},
            //new Settings
            //{
            //    Key = SettingsKey.CitizenSettingsNotDbAppealsAnswerLinks,
            //    Description = "ISN связки ответа (разделитель '|')",
            //    Value = "",
            //    ReadingRightLevel = RightLevel.AdministratorSettings,
            //    ValueType = typeof(string).FullName,
            //    Nullable = false,
            //    SettingType = SettingType.Value,
            //    Weight = 6
            //}
        };

        public override IEnumerable<ClaimInfoModel> ClaimInfos
        {
            get
            {
                var subsystemName = SettingValue($"subsystem.{SettingsKey.Name}");

                var settingsManager = ServiceProvider.GetRequiredService<SettingsManager>();

                return new[]
                {
                    new ClaimInfoModel(ClaimName.Subsystem,
                        new[]
                        {
                            new ClaimValueModel(1, $"{BaseSystemKey}.{SSClaimSuffix.Ss.available}",
                                "Доступ разрешен"),
                            new ClaimValueModel(2, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_appeal}",
                                "Создание обращений"),
                            new ClaimValueModel(3, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_message}",
                                $"Создание сообщений"),
                            new ClaimValueModel(4, $"{BaseSystemKey}.{SSClaimSuffix.Ss.executor}",
                                $"Исполнитель"),
                            new ClaimValueModel(5, $"{BaseSystemKey}.{SSClaimSuffix.Ss.feedback_admin}",
                                $"Администратор сообщений обратной связи"),
                        },
                        $"Доступ к подсистеме \"{subsystemName}\"",
                        ClaimEntity.Role, ClaimEntity.User),

                    new ClaimInfoModel(ClaimName.Manage,
                        new[]
                        {
                            new ClaimValueModel(1, $"{BaseSystemKey}.{SSClaimSuffix.Mu.rubrics}",
                                $"Настройка справочника \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitle)}\""),
                            new ClaimValueModel(2, $"{BaseSystemKey}.{SSClaimSuffix.Mu.territories}",
                                $"Настройка справочника \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitle)}\""),
                            new ClaimValueModel(3, $"{BaseSystemKey}.{SSClaimSuffix.Mu.regions}",
                                $"Настройка справочника  \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitle)}\""),
                            new ClaimValueModel(4, $"{BaseSystemKey}.{SSClaimSuffix.Mu.topics}",
                                $"Настройка справочника \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitle)}\"")
                        },
                        $"Разрешения управления для подсистемы \"{subsystemName}\"",
                        ClaimEntity.Role, ClaimEntity.User),

                    new ClaimInfoModel(ClaimName.Moderate,
                        new[]
                        {
                            new ClaimValueModel(1, $"{BaseSystemKey}.{SSClaimSuffix.Md.enable}",
                                "Разрешить модерирование"),
                            new ClaimValueModel(2, $"{BaseSystemKey}.{SSClaimSuffix.Md.super}",
                                "Супер права модерирования"),
                            new ClaimValueModel(3, $"{BaseSystemKey}.{SSClaimSuffix.Md.all_rubrics}",
                                $"Все \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitle)}\""),
                            new ClaimValueModel(4, $"{BaseSystemKey}.{SSClaimSuffix.Md.all_territories}",
                                $"Все \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitle)}\""),
                            new ClaimValueModel(5, $"{BaseSystemKey}.{SSClaimSuffix.Md.all_regions}",
                                $"Все \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitle)}\""),
                            new ClaimValueModel(6, $"{BaseSystemKey}.{SSClaimSuffix.Md.all_topics}",
                                $"Все \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitle)}\"")
                        },
                        $"Разрешения модерирования для подсистемы \"{subsystemName}\"",
                        ClaimEntity.Role, ClaimEntity.User)
                };
            }
        }

        public override IEnumerable<Claim> GetCitizenSubsystemClaims(User user)
        {
            var result = new List<Claim>();

            result.AddRange(new []
            {
                new Claim(ClaimName.Subsystem, $"{BaseSystemKey}.{SSClaimSuffix.Ss.available}"),
                new Claim(ClaimName.Subsystem, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_message}")
            });

            if ((user.LoginSystem == LoginSystem.Form && user.SelfFormRegister) || (user.LoginSystem != LoginSystem.Form && user.ProfileConfirmed))
            {
                result.Add(new Claim(ClaimName.Subsystem, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_appeal}"));
            }

            return result;
        }
    }
}
