using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Subsystems
{
    public class SubsystemOrganizationAppeals : SubsystemBase
    {
        internal static readonly string DefaultName = "Обращения организаций";
        internal static readonly string DefaultDescription = "тут должно быть описание";

        public const string BaseSystemKey = "organizations";

        public override string SystemKey => BaseSystemKey;

        public override bool IsEnable
        {
            get
            {
                var settingsManager = ServiceProviderServiceExtensions.GetRequiredService<SettingsManager>(ServiceProvider);

                return settingsManager.SettingValue<bool>($"subsystem.{SettingsKey.Enable}");
            }
        }

        public override async Task SendNewMessageEmail(Message message)
        {
            if (message.AppealId.HasValue)
            {
                using var scope = ScopeFactory.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                var appeal = message.Appeal
                             ?? await context.Appeals
                                 .Include(el => el.Subsystem)
                                 .Include(el => el.Author)
                                 .Where(el => el.Id == message.Id)
                                 .FirstOrDefaultAsync();

                if (appeal == null || appeal.SubscribeToAnswers == SubscribeToAnswersEnum.Not_Subscribed || appeal.Subsystem?.UID != BaseSystemKey)
                {
                    return;
                }

                var mailMessage = new MailMessage
                {
                    //Subject = $"Обращение от {appeal.CreatedAt.ToLocalTime():dd.MM.yyyy}",
                    TemplateKey = "messageToAppealOrganization",
                    Email = string.IsNullOrWhiteSpace(appeal.Email) ? appeal.Author.Email : appeal.Email,
                    AddingEntity = new List<EmailEntityMetadata>
                    {
                        EmailEntityMetadata.Generate(context, appeal.AuthorId),
                        EmailEntityMetadata.Generate(context, appeal),
                        EmailEntityMetadata.Generate(context, message)
                    },
                    FilesIds = message.AttachedFiles?
                        .Select(el => el.Id)
                        .ToArray()
                };

                var emailService = scope.ServiceProvider.GetRequiredService<MailService>();

                await emailService.SendEmailAsync(mailMessage);
            }
        }

        public SubsystemOrganizationAppeals(IServiceProvider serviceProvider, IServiceScopeFactory scopeFactory)
            : base(serviceProvider, scopeFactory)
        { }

        public static class SettingsKey
        {
            public static readonly string OrganizationSettings = $"{BaseSystemKey}.";
            public static readonly string Enable = $"{OrganizationSettings}enable.";
            public static readonly string Name = $"{OrganizationSettings}name.";
            public static readonly string Description = $"{OrganizationSettings}description.";
            public static readonly string AppealUrlTemplate = $"{OrganizationSettings}appeal_url_template.";

            public static readonly string OrganizationSettingsExecutorFeature = $"{OrganizationSettings}executor_feature.";
            public static readonly string OrganizationSettingsWeb = $"{OrganizationSettings}web.";
            public static readonly string OrganizationSettingsWebAddAppeal = $"{OrganizationSettingsWeb}appeal_add.";
            public static readonly string OrganizationSettingsWebEditAppeal = $"{OrganizationSettingsWeb}appeal_edit.";
            public static readonly string OrganizationSettingsWebDelAppeal = $"{OrganizationSettingsWeb}appeal_del.";
            public static readonly string OrganizationSettingsAppealWebAnswer = $"{OrganizationSettingsWeb}appeal_web_answer.";

            public static readonly string SubscribeToAnswers = $"{OrganizationSettings}subscribe_to_answers.";
            public static readonly string SubscribeToAnswersToAnswers = $"{OrganizationSettings}subscribe_to_answers_to_answers.";
        }

        protected override IEnumerable<Settings> SubsystemDefaultSettings => new []
        {
            new Settings
            {
                Key = SettingsKey.Enable,
                Description = "Активирована",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 1
            },
            new Settings
            {
                Key = SettingsKey.Name,
                Description = "Название",
                Value = DefaultName,
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 2
            },
            new Settings
            {
                Key = SettingsKey.Description,
                Description = "Описание",
                Value = DefaultDescription,
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 3
            },
            new Settings
            {
                Key = SettingsKey.AppealUrlTemplate,
                Description = "Шаблон URL для обращения",
                Value = "http://localhost/{appealId}",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(string).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 4
            },
            new Settings
            {
                Key = SettingsKey.OrganizationSettingsWebAddAppeal,
                Description = "Добавление обращений из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 5
            },
            new Settings
            {
                Key = SettingsKey.OrganizationSettingsWebEditAppeal,
                Description = "Редактирование обращений из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 6
            },
            new Settings
            {
                Key = SettingsKey.OrganizationSettingsWebDelAppeal,
                Description = "Удаление обращений из WEB",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 7
            },
             new Settings
            {
                Key = SettingsKey.OrganizationSettingsAppealWebAnswer,
                Description = "Ответ на обращение из WEB",
                Value = "true",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 10
            },
            new Settings
            {
                Key = SettingsKey.SubscribeToAnswers,
                Description = "Уведомления о комментариях на новости",
                Value = SubscribeToAnswersEnum.Not_Subscribed.ToString(),
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(SubscribeToAnswersEnum).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 20,
                Values = new SettingsValueItem[]
                {
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Not_Subscribed, "Не отправлять"),
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Subscribed, "Отправлять")
                }
            },
            new Settings
            {
                Key = SettingsKey.SubscribeToAnswersToAnswers,
                Description = "Уведомления об ответах на комментарии на новости",
                Value = SubscribeToAnswersEnum.Not_Subscribed.ToString(),
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(SubscribeToAnswersEnum).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 30,
                Values = new SettingsValueItem[]
                {
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Not_Subscribed, "Не отправлять"),
                    new SettingsValueItem<SubscribeToAnswersEnum>(SubscribeToAnswersEnum.Subscribed, "Отправлять")
                }
            },
            new Settings
            {
                Key = SettingsKey.OrganizationSettingsExecutorFeature,
                Description = "Функционал Исполнителей",
                Value = "false",
                ReadingRightLevel = RightLevel.Unauthorized,
                ValueType = typeof(bool).FullName,
                Nullable = false,
                SettingType = SettingType.Value,
                Weight = 40
            },

        };

        public override IEnumerable<ClaimInfoModel> ClaimInfos
        {
            get
            {
                var subsystemName = SettingValue($"subsystem.{SettingsKey.Name}");

                var settingsManager = ServiceProvider.GetRequiredService<SettingsManager>();

                return new[]
                {
                    new ClaimInfoModel(ClaimName.Subsystem,
                        new[]
                        {
                            new ClaimValueModel(1, $"{BaseSystemKey}.{SSClaimSuffix.Ss.available}",
                                "Доступ разрешен"),
                            new ClaimValueModel(2, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_appeal}",
                                "Создание обращений"),
                            new ClaimValueModel(3, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_message}",
                                $"Создание сообщений"),
                            new ClaimValueModel(4, $"{BaseSystemKey}.{SSClaimSuffix.Ss.executor}",
                                $"Исполнитель"),
                            new ClaimValueModel(5, $"{BaseSystemKey}.{SSClaimSuffix.Ss.feedback_admin}",
                                $"Администратор сообщений обратной связи"),
                        },
                        $"Доступ к подсистеме \"{subsystemName}\"",
                        ClaimEntity.Role, ClaimEntity.User),

                    new ClaimInfoModel(ClaimName.Manage,
                        new[]
                        {
                            new ClaimValueModel(1, $"{BaseSystemKey}.{SSClaimSuffix.Mu.rubrics}",
                                $"Настройка справочника \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitle)}\""),
                            new ClaimValueModel(2, $"{BaseSystemKey}.{SSClaimSuffix.Mu.territories}",
                                $"Настройка справочника \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitle)}\""),
                            new ClaimValueModel(3, $"{BaseSystemKey}.{SSClaimSuffix.Mu.regions}",
                                $"Настройка справочника  \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitle)}\""),
                            new ClaimValueModel(4, $"{BaseSystemKey}.{SSClaimSuffix.Mu.topics}",
                                $"Настройка справочника \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitle)}\"")
                        },
                        $"Разрешения управления для подсистемы \"{subsystemName}\"",
                        ClaimEntity.Role, ClaimEntity.User),

                    new ClaimInfoModel(ClaimName.Moderate,
                        new[]
                        {
                            new ClaimValueModel(1, $"{BaseSystemKey}.{SSClaimSuffix.Md.enable}",
                                "Разрешить модерирование"),
                            new ClaimValueModel(2, $"{BaseSystemKey}.{SSClaimSuffix.Md.super}",
                                "Супер права модерирования"),
                            new ClaimValueModel(3, $"{BaseSystemKey}.{SSClaimSuffix.Md.all_rubrics}",
                                $"Все \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRubricTitle)}\""),
                            new ClaimValueModel(4, $"{BaseSystemKey}.{SSClaimSuffix.Md.all_territories}",
                                $"Все \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTerritoryTitle)}\""),
                            new ClaimValueModel(5, $"{BaseSystemKey}.{SSClaimSuffix.Md.all_regions}",
                                $"Все \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebRegionTitle)}\""),
                            new ClaimValueModel(6, $"{BaseSystemKey}.{SSClaimSuffix.Md.all_topics}",
                                $"Все \"{settingsManager.SettingValue<string>(SettingsKeys.SystemWebTopicTitle)}\"")
                        },
                        $"Разрешения модерирования для подсистемы \"{subsystemName}\"",
                        ClaimEntity.Role, ClaimEntity.User)
                };
            }
        }

        public override IEnumerable<Claim> GetCitizenSubsystemClaims(User user)
        {
            var result = new List<Claim>();

            if (user.ProfileConfirmed && user.Users2Organizations.Any())
            {
                result.AddRange(new[]
                    {
                        new Claim(ClaimName.Subsystem, $"{BaseSystemKey}.{SSClaimSuffix.Ss.available}"),
                        new Claim(ClaimName.Subsystem, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_message}"),
                        new Claim(ClaimName.Subsystem, $"{BaseSystemKey}.{SSClaimSuffix.Ss.create_appeal}")
                    }
                );
            }

            return result;
        }
    }
}
