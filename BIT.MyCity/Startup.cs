using System;
using System.Net;
using System.Text.Json.Serialization;
using BIT.GraphQL.Authorization;
using BIT.GraphQL.Authorization.Requirements;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization;
using BIT.MyCity.Authorization.ExtApiHelpers;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Gc;
using BIT.MyCity.Loclization;
using BIT.MyCity.Managers;
using BIT.MyCity.QuartzJobs;
using BIT.MyCity.Reports;
using BIT.MyCity.Services;
using BIT.MyCity.Subsystems;
using BIT.MyCity.WebApi;
using BIT.MyCity.WebApi.Types.Users;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using GraphQL.Server.Ui.Playground;
using GraphQL.Server;
using GraphQL.Server.Ui.GraphiQL;
using GraphQL.Server.Ui.Voyager;
using GraphQL.Types;
using GraphQL.Server.Ui.Altair;
using Quartz;

namespace BIT.MyCity
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostEnvironment Environment { get; }

        public Startup(IConfiguration configuration, IHostEnvironment environment, ILoggerFactory loggerFactory)
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider
                .RegisterLicense("Mgo+DSMBaFt/QHFqVVhkW1pFdEBBXHxAd1p/VWJYdVt5flBPcDwsT3RfQF9jT35bdkZiXH5YeHBcQA==;Mgo+DSMBPh8sVXJ0S0d+XE9AcVRDX3xKf0x/TGpQb19xflBPallYVBYiSV9jS3xTcEVrWXxfdXVTTmVVVw==;ORg4AjUWIQA/Gnt2VVhhQlFaclhJXGFWfVJpTGpQdk5xdV9DaVZUTWY/P1ZhSXxRdkNjUX9dcXFUQGhbUUI=;NjUyNjIyQDMyMzAyZTMxMmUzMEp3UHlSdHFkL2tHM3B6ckhjL3kzU094bU9nZm53MXJNRWxVczRzRmZIUDg9;NjUyNjIzQDMyMzAyZTMxMmUzMFV5OGVYK0NRejdRT0ExRlFiQ1ZsZVYwYUY3YitsQWRHTHNlaWVwR0NSaTQ9;NRAiBiAaIQQuGjN/V0Z+XU9EaFtFVmJLYVB3WmpQdldgdVRMZVVbQX9PIiBoS35RdEVlWXdec3VQR2ZVU0N2;NjUyNjI1QDMyMzAyZTMxMmUzMFFOREtFZEhXaUd4MmdHY2Zmb0kxcjBncG1mSGNJVXJHRXZueUZkZFBGaU09;NjUyNjI2QDMyMzAyZTMxMmUzMElob1A1ZWRFdThqUkE1cGM0UXc1MHF5eU8xVzNFWHliYmlWbGNNcG04MWM9;Mgo+DSMBMAY9C3t2VVhhQlFaclhJXGFWfVJpTGpQdk5xdV9DaVZUTWY/P1ZhSXxRdkNjUX9dcXFUQGlUWEE=;NjUyNjI4QDMyMzAyZTMxMmUzMEhHZGU4a1MzN2ZFcVlHMWZ3NHZnRG85VWlSQzFZMkZwRnBEbU5MZlFCeU09;NjUyNjI5QDMyMzAyZTMxMmUzMG9KR1RKTGdvRjhwbGlTUXF1YTcwZW9zUThBWGwxVFJudmpRaFlEcWdPRXc9");

            Configuration = configuration;
            Environment = environment;
            loggerFactory.AddLog4Net();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);
            services.AddHttpContextAccessor();

            var dbConnectionString = Configuration.GetConnectionString("MainDatabase");
            //не нашел как приличнее вытащить версию БД в рантайме
            try
            {
                using var con = new Npgsql.NpgsqlConnection(dbConnectionString);

                con.Open();

                using var cmd = new Npgsql.NpgsqlCommand("SELECT version()", con);

                BaseConstants.Db_Version = cmd.ExecuteScalar().ToString();

                con.Close();
            }
            catch
            {
                BaseConstants.Db_Version = "?";
            }
            //конец

            services.AddDbContext<DocumentsContext>(optios =>
            {
                optios
                    .ReplaceService<Microsoft.EntityFrameworkCore.Migrations.IHistoryRepository, PostgreHistoryRepository>()
                    .UseNpgsql(dbConnectionString,
                        options => options.MigrationsHistoryTable("_ef_migrations", "public"));
            });
            ConfigureAuthServices(services);
            AddManagers(services);
            AddGcCleaners(services);
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.KnownProxies.Add(IPAddress.Parse("10.211.55.2"));
            });
            services.AddCors(options =>
            {
                options.AddPolicy("EnableCORS",
                    builder => { builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().Build(); });
            });

            services
                .AddMvc(options => { options.EnableEndpointRouting = false; })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                });
            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                });
            services.Configure<FormOptions>(x =>
            {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue;
            });
            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/dist"; });

            services.AddHttpClient();

            ConfigureGraphQlServices(services);

            services.AddAuthorization();

            services.AddTokenAuthentication(Configuration);
            //services.AddSingleton<IAuthorizationHandler, DocumentsAuthorizationHandler>();
            services.AddScoped<AuthManager>();
            services.AddSingleton<AuthExtApiHelperFabric>();
            services.AddElasticsearch(Configuration);

            services.AddSingleton<ReportService>();
            services.AddSingleton<PushService>();
            //services.AddHostedService<PushService>(p => p.GetRequiredService<PushService>());
            
            AddQuartz(services);
        }

        private void ConfigureGraphQlServices(IServiceCollection services)
        {
            services.AddSingleton<MyCityQuery>();
            services.AddSingleton<MyCityMutation>();
            services.AddSingleton<UserType>();
            services.AddSingleton<ISchema, MyCityShema>();
            services.AddSingleton<MyCityShema>();

            services.AddGraphQLAuth((_, s) =>
            {
                _.AddPolicy(PolicyName.Authorized, p => p.RequireAuthenticatedUser());

                _.AddPolicy(PolicyName.ManageUser, p => p.RequireClaim(ClaimName.Manage, "users"));
                _.AddPolicy(PolicyName.ManageRoles, p => p.RequireClaim(ClaimName.Manage, "roles"));
                _.AddPolicy(PolicyName.ManageUsersOrRoles, p => p.RequireClaim(ClaimName.Manage, "users", "roles"));
                _.AddPolicy(PolicyName.ManageSettings, p => p.RequireClaim(ClaimName.Manage, "settings"));
                _.AddPolicy(PolicyName.ManageJournal, p => p.RequireClaim(ClaimName.Manage, "journal"));
                _.AddPolicy(PolicyName.ManageCustomEmail, p => p.RequireClaim(ClaimName.Manage, "custom_email"));
                _.AddPolicy(PolicyName.ManageReports, p => p.RequireClaim(ClaimName.Manage, "reports"));

                #region Словари
                _.AddPolicy(PolicyName.ManageRubrics, p => p.RequireClaim(ClaimName.Manage, "rubrics"));
                _.AddPolicy(PolicyName.ManageTerritories, p => p.RequireClaim(ClaimName.Manage, "territories"));
                _.AddPolicy(PolicyName.ManageRegions, p => p.RequireClaim(ClaimName.Manage, "regions"));
                _.AddPolicy(PolicyName.ManageTopics, p => p.RequireClaim(ClaimName.Manage, "topics"));

                _.AddPolicy(PolicyName.ManageRubricsSubsystem,
                    p => p.RequireClaim(ClaimName.Manage,
                        $"'{ClaimName.Moderate}' со значением *.rubrics",
                        value => value.EndsWith(".rubrics")));
                _.AddPolicy(PolicyName.ManageTerritoriesSubsystem,
                    p => p.RequireClaim(ClaimName.Manage,
                        $"'{ClaimName.Moderate}' со значением *.territories",
                        value => value.EndsWith(".territories")));
                _.AddPolicy(PolicyName.ManageRegionsSubsystem,
                    p => p.RequireClaim(ClaimName.Manage,
                        $"'{ClaimName.Moderate}' со значением *.regions",
                        value => value.EndsWith(".regions")));
                _.AddPolicy(PolicyName.ManageTopicsSubsystem, p => p.RequireClaim(ClaimName.Manage,
                    $"'{ClaimName.Moderate}' со значением *.topics",
                    value => value.EndsWith(".topics")));

                _.AddPolicy(PolicyName.ManageRubricsWithSubsystem, p => p
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Manage, "rubrics"),
                        new ClaimRequirement(ClaimName.Manage, $"'{ClaimName.Manage}' со значением *.rubrics",
                            value => value.EndsWith(".rubrics")
                    )));
                _.AddPolicy(PolicyName.ManageTerritoriesWithSubsystem, p => p
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Manage, "territories"),
                        new ClaimRequirement(ClaimName.Manage, $"'{ClaimName.Manage}' со значением *.territories",
                            value => value.EndsWith(".territories")
                        )));
                _.AddPolicy(PolicyName.ManageRegionsWithSubsystem, p => p
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Manage, "regions"),
                        new ClaimRequirement(ClaimName.Manage, $"'{ClaimName.Manage}' со значением *.regions",
                            value => value.EndsWith(".regions")
                        )));
                _.AddPolicy(PolicyName.ManageTopicsWithSubsystem, p => p
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Manage, "topics"),
                        new ClaimRequirement(ClaimName.Manage, $"'{ClaimName.Manage}' со значением *.topics",
                            value => value.EndsWith(".topics")
                        )));
                #endregion

                _.AddPolicy(PolicyName.LoginSystemForm, p =>
                    p.RequireClaim(ClaimName.LoginSystem, nameof(LoginSystem.Form)));

                _.AddPolicy(PolicyName.Moderate, p => p
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Moderate, $"'{ClaimName.Moderate}' со значением *.enable",
                            value => value.EndsWith(".enable"))
                    ));

                _.AddPolicy(PolicyName.ManageUsersOrModerate, p => p
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Manage, "users"),
                        new ClaimRequirement(ClaimName.Moderate, $"'{ClaimName.Moderate}' со значением *.enable",
                            value => value.EndsWith(".enable"))
                    ));
                _.AddPolicy(PolicyName.ManageUsersOrModerateOrganizations, p => p
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Manage, "users"),
                        new ClaimRequirement(ClaimName.Moderate, "organizations.available")
                    ));
                _.AddPolicy(PolicyName.ManageUsersOrModerateOrganizationsOrSubsystemOrganization, p => p
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Manage, "users"),
                        new ClaimRequirement(ClaimName.Moderate, "organizations.available"),
                        new ClaimRequirement(ClaimName.Subsystem, "organizations.available")
                    ));
                _.AddPolicy(PolicyName.SubsystemOrganizations, p => p.RequireClaim(ClaimName.Subsystem, "organizations.available"));

                _.AddPolicy(PolicyName.CreateAppeals, p => p
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Subsystem, $"'{ClaimName.Subsystem}' со значением *.available",
                            value => value.EndsWith(".available"))
                    )
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Subsystem, $"'{ClaimName.Subsystem}' со значением *.create_appeal",
                            value => value.EndsWith(".create_appeal"))
                    ));

                _.AddPolicy(PolicyName.CreateMessages, p => p
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Subsystem, $"'{ClaimName.Subsystem}' со значением *.available",
                            value => value.EndsWith(".available"))
                    )
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Subsystem, $"'{ClaimName.Subsystem}' со значением *.create_message",
                            value => value.EndsWith(".create_message"))
                    ));

                _.AddPolicy(PolicyName.Vote, p =>
                    p.RequireClaim(ClaimName.Subsystem,
                        $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Ss.available}",
                        $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Ss.allowed}")
                );

                _.AddPolicy(PolicyName.CreateVoting, p =>
                    p.RequireClaim(ClaimName.Subsystem,
                        $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Ss.available}",
                        $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Ss.create_voting}")
                );

                _.AddPolicy(PolicyName.ManageVoting, p => p
                    .RequireClaim(ClaimName.Subsystem, $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Ss.available}")
                    .RequireClaim(ClaimName.Manage, $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Mu.control}")
                );

                _.AddPolicy(PolicyName.ModerateVoting, p => p
                    .RequireClaim(ClaimName.Subsystem, $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Ss.available}")
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Subsystem,
                            $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Ss.create_voting}"),
                        new ClaimRequirement(ClaimName.Moderate,
                            $"{SubsystemVoting.BaseSystemKey}.{SSClaimSuffix.Md.enable}")
                    )
                );

                _.AddPolicy(PolicyName.CreateNews, p =>
                    p.RequireClaim(ClaimName.Subsystem,
                        $"{SubsystemNews.BaseSystemKey}.{SSClaimSuffix.Ss.available}",
                        $"{SubsystemNews.BaseSystemKey}.{SSClaimSuffix.Ss.create_news}")
                );

                _.AddPolicy(PolicyName.ManageNews, p => p
                    .RequireClaim(ClaimName.Subsystem, $"{SubsystemNews.BaseSystemKey}.{SSClaimSuffix.Ss.available}")
                    .RequireClaim(ClaimName.Manage, $"{SubsystemNews.BaseSystemKey}.{SSClaimSuffix.Mu.control}")
                );

                _.AddPolicy(PolicyName.ModerateNews, p => p
                    .RequireClaim(ClaimName.Subsystem, $"{SubsystemNews.BaseSystemKey}.{SSClaimSuffix.Ss.available}")
                    .RequireAnyClaim(
                        new ClaimRequirement(ClaimName.Subsystem,
                            $"{SubsystemNews.BaseSystemKey}.{SSClaimSuffix.Ss.create_news}"),
                        new ClaimRequirement(ClaimName.Moderate,
                            $"{SubsystemNews.BaseSystemKey}.{SSClaimSuffix.Md.enable}")
                    )
                );
            });

            services.AddGraphQL(options =>
            {
                options.EnableMetrics = Environment.IsDevelopment();
                options.ExposeExceptions = Environment.IsDevelopment();
                options.UnhandledExceptionDelegate = ctx => { Console.WriteLine(ctx.OriginalException); };
            })
                .AddUserContextBuilder(context => new GraphQLUserContext { User = context.User })
                .AddSystemTextJson(deserializerSettings => { }, serializerSettings => { })
                .AddWebSockets()
                .AddDataLoader()
                .AddGraphTypes(typeof(MyCityShema));
        }

        private static void AddManagers(IServiceCollection services)
        {
            services.AddScoped<UserManager>();
            services.AddScoped<RoleManager>();
            services.AddScoped<ClaimManager>();
            services.AddScoped<OrganizationManager>();

            services.AddScoped<TerritoryManager>();
            services.AddScoped<RubricManager>();
            services.AddScoped<RegionManager>();
            services.AddScoped<TopicManager>();

            services.AddScoped<AppealManager>();
            //services.AddScoped<AppealNotDbManager>();
            services.AddScoped<AppealExecutorManager>();
            services.AddScoped<AttachedFileManager>();
            services.AddScoped<MessageManager>();
            services.AddScoped<RubricatorManager>();
            services.AddScoped<SubsystemManager>();

            services.AddScoped<AppointmentManager>();
            services.AddScoped<VoteManager>();
            services.AddScoped<ProtocolRecordManager>();

            services.AddSingleton<SettingsManager>();
            services.AddSingleton<MailService>();
            
            services.AddSingleton<SubsystemService>();
            services.AddTransient<ModeratorsNotificationService>();
            services.AddTransient<AttachService>();
            services.AddTransient<VoteService>();
            services.AddTransient<VoteGarbageCollectorService>();
            services.AddSingleton<GQLSubscriptionService>();

            services.AddSingleton<SmsService>();
            services.AddScoped<SmsManager>();
            services.AddScoped<PhoneManager>();
            services.AddScoped<EmailManager>();

            //services.AddSingleton<PushService>();

            services.AddScoped<EnumVerbManager>();
            services.AddScoped<NewsManager>();
            services.AddScoped<AppealStatsManager>();
            services.AddScoped<PTokenManager>();
            services.AddScoped<JournalManager>();

            services.AddScoped<ReportManager>();

            services.AddScoped<CustomStorageManager>();
            
            AddSubsystemsServices(services);
            
        }

        private static void AddSubsystemsServices(IServiceCollection services)
        {
            var subsystemService = services.BuildServiceProvider().GetRequiredService<SubsystemService>();

            foreach (var subsystem in subsystemService.Subsystems)
            {
                subsystem.RegisterServices(services);
            }
        }

        private static void AddGcCleaners(IServiceCollection services)
        {
            services.AddTransient<IGcCleaner, SettingsGcCleaner>();
            services.AddTransient<IGcCleaner, AppealGcCleaner>();
            services.AddTransient<IGcCleaner, EmailsGcCleaner>();
            services.AddTransient<IGcCleaner, VoteGcCleaner>();
            services.AddTransient<IGcCleaner, AppointmentGcCleaner>();
            services.AddTransient<IGcCleaner, NewsGcCleaner>();
            services.AddTransient<IGcCleaner, ReportGcCleaner>();
        }

        private static void ConfigureAuthServices(IServiceCollection services)
        {
            services.AddIdentity<User, Role>(options =>
                    {
                        options.Password.RequireDigit = false;
                        options.Password.RequireLowercase = false;
                        options.Password.RequireNonAlphanumeric = false;
                        options.Password.RequireUppercase = false;
                        options.Password.RequiredLength = 6;

                        options.User.RequireUniqueEmail = false;
                        options.User.AllowedUserNameCharacters = null;
                    }
                )
                .AddRoles<Role>()
                .AddEntityFrameworkStores<DocumentsContext>()
                .AddDefaultTokenProviders()
                .AddErrorDescriber<RussianIdentityErrorDescriber>();
        }

        private static void AddQuartz(IServiceCollection services)
        {
            services.AddTransient<JobManager>();
            services.AddTransient<IPeriodicallyJob, JournalCleanerPeriodicallyJob>();
            services.AddTransient<IPeriodicallyJob, NotSendEmailPeriodicallyJob>();
            services.AddTransient<IPeriodicallyJob, AttachedFilesCleanerPeriodicallyJob>();
            services.AddTransient<IPeriodicallyJob, ModeratorsNotificationServicePeriodicallyJob>();
            services.AddTransient<IPeriodicallyJob, VotesCleanerPeriodicallyJob>();
            
            services.AddQuartz(q =>
            {
                q.SchedulerId = "MyCity-Scheduler";
                q.SchedulerName = "Quartz ASP.NET Core MyCity Scheduler";
                q.UseMicrosoftDependencyInjectionJobFactory();
                q.UseSimpleTypeLoader();
                q.UseInMemoryStore();

                q.UseDefaultThreadPool(tp => { tp.MaxConcurrency = 2; });

                q.ScheduleJob<JobManager>(trigger => trigger
                    .WithIdentity(typeof(JobManager).FullName ?? "JobManager", "Default")
                    .StartAt(DateBuilder.EvenSecondDate(DateTimeOffset.UtcNow.AddSeconds(20)))
                    .WithCronSchedule("0 0/1 * * * ?"));
            });

            services.AddQuartzServer(options => { options.WaitForJobsToComplete = true; });
        }

        public void Configure(IApplicationBuilder app, IHostEnvironment env, UserManager<User> userManager, ILoggerFactory loggerFactory)
        {
            //ApplicationDbInitializer.SeedUsers(userManager);
            ServiceActivator.Configure(app.ApplicationServices);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");

                app.UseHsts();
            }
            
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseCors("EnableCORS");
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseControlAuthorization();
            
            UpdateDatabase(app);

            //StartMailService(app);

            //StartModeratorsNotificationService(app);
            
            AddGraphQl(app);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";
                //spa.Options.DefaultPage
                //if (env.IsDevelopment())
                //{
                //    //spa.Options.StartupTimeout = new TimeSpan(0, 5, 0);
                //    spa.UseAngularCliServer(npmScript: "start");
                //}
            });
            
            //StartVoteService(app);
            StartSmsService(app);
            StartPushService(app);
            //StartReportService(app);
            //StartGarbageCollectorService(app);
        }

        public void AddGraphQl(IApplicationBuilder app)
        {
            if (Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseWebSockets();
            /*--------------------------------------------------------------------------*/
            app.UseGraphQLWebSockets<MyCityShema>("/subscription");
            /*--------------------------------------------------------------------------*/
            // use websocket middleware for ChatSchema at path /graphql
            //            app.UseGraphQLWebSockets<ChatSchema>("/graphql");

            app.UseGraphQL<MyCityShema, GraphQLHttpMiddlewareWithLogs<MyCityShema>>();

            //if (Environment.IsDevelopment())
            //{
            //    Test.MycityTest.UseGraphQL(app, "/graphql1");
            //}
            app.UseGraphQLPlayground(new GraphQLPlaygroundOptions()
            {
                Path = "/ui/playground"
            });
            app.UseGraphiQLServer(new GraphiQLOptions
            {
                Path = "/ui/graphiql",
                GraphQLEndPoint = "/graphql"
            });
            app.UseGraphQLVoyager(new GraphQLVoyagerOptions()
            {
                GraphQLEndPoint = "/graphql",
                Path = "/ui/voyager"
            });
            // use altair middleware at default url /ui/altair
            app.UseGraphQLAltair(new GraphQLAltairOptions()
            {
                GraphQLEndPoint = "/graphql",
                Path = "/ui/altair"
            });
        }

        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();

            var context = serviceScope.ServiceProvider.GetService<DocumentsContext>();

            if (context == null)
            {
                throw new Exception("Не удалось получить контекст БД.");
            }
            
            context.Database.Migrate();

            var dbInitializer = new RoleAdminInitializer(serviceScope.ServiceProvider);

            var task = dbInitializer.Initialize();

            task.Wait();

            var initResult = task.Result;

            if (!initResult)
            {
                throw new Exception("Ошибка инициализации БД");
            }
        }

        //private static void StartMailService(IApplicationBuilder app)
        //{
        //    using var serviceScope = app.ApplicationServices
        //        .GetRequiredService<IServiceScopeFactory>()
        //        .CreateScope();

        //    serviceScope.ServiceProvider.GetService<MailService>();
        //}

        //private static void StartModeratorsNotificationService(IApplicationBuilder app)
        //{
        //    using var serviceScope = app.ApplicationServices
        //        .GetRequiredService<IServiceScopeFactory>()
        //        .CreateScope();

        //    serviceScope.ServiceProvider.GetService<ModeratorsNotificationService>();
        //}

        //private static void StartAttachService(IApplicationBuilder app)
        //{
        //    using var serviceScope = app.ApplicationServices
        //        .GetRequiredService<IServiceScopeFactory>()
        //        .CreateScope();

        //    serviceScope.ServiceProvider.GetRequiredService<AttachService>();
        //}

        //private static void StartVoteService(IApplicationBuilder app)
        //{
        //    using var serviceScope = app.ApplicationServices
        //        .GetRequiredService<IServiceScopeFactory>()
        //        .CreateScope();

        //    serviceScope.ServiceProvider.GetRequiredService<VoteService>();
        //}

        //private static void StartGarbageCollectorService(IApplicationBuilder app)
        //{
        //    using var serviceScope = app.ApplicationServices
        //        .GetRequiredService<IServiceScopeFactory>()
        //        .CreateScope();

        //    serviceScope.ServiceProvider.GetRequiredService<GarbageCollectorService>();
        //}

        // TODO: Это для тестирования. Убрать в релизе
        //private static void StartReportService(IApplicationBuilder app)
        //{
        //    using var serviceScope = app.ApplicationServices
        //        .GetRequiredService<IServiceScopeFactory>()
        //        .CreateScope();

        //    serviceScope.ServiceProvider.GetRequiredService<ReportService>();
        //}

        private static void StartSmsService(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();

            serviceScope.ServiceProvider.GetRequiredService<SmsService>();
        }

        private static void StartPushService(IApplicationBuilder app)
        {
            // using var serviceScope = app.ApplicationServices
            //     .GetRequiredService<IServiceScopeFactory>()
            //     .CreateScope();

            app.ApplicationServices.GetRequiredService<PushService>();
        }
    }
}
