using System;
using System.Collections.Generic;

namespace BIT.MyCity.Model
{
    public class UserForm : IUser
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public List<RoleForm> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }

        public UserForm()
        {
            AllRoles = new List<RoleForm>();
            UserRoles = new List<string>();
        }
    }
}
