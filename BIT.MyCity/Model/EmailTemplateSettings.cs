using System.Collections.Generic;

namespace BIT.MyCity.Model
{
    public class EmailTemplateSettings
    {
        /// <summary>
        /// Шаблон темы обращения
        /// </summary>
        public string SubjectTemplate { get; set; } = null;

        /// <summary>
        /// Формат даты
        /// </summary>
        public string DateTimeFormat { get; set; } = "dd.MM.yyyy";

        /// <summary>
        /// Включаемые сущности
        /// </summary>
        public Dictionary<string, string[]> EntityInclude { get; set; } = new Dictionary<string, string[]>();
    }
}
