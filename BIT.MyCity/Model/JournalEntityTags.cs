using System.Collections.Generic;

namespace BIT.MyCity.Model
{
    public class JournalTags
    {
        public string Tag { get; set; }

        public string Name { get; set; }
    }

    public class JournalEntityTags
    {
        public string Tag { get; set; }

        public string Name { get; set; }

        public JournalTags[] PropertyTags { get; set; } = new JournalTags[0];
    }
}
