using System;
using System.Collections.Generic;

namespace BIT.MyCity.Model
{
    public sealed class ExternalSystemAppeal
    {
        public ExternalSystemAppeal(long id, string number, DateTime date)
        {
            Id = id;
            Number = number;
            Date = date;
        }

        public long Id { get; }

        public string Number { get; }

        public DateTime Date { get; }

        public string CitizenName { get; set; }

        public List<ExternalSystemAppealRubric> Rubrics { get; } = new List<ExternalSystemAppealRubric>();
    }

    public sealed class ExternalSystemAppealRubric
    {
        public ExternalSystemAppealRubric(string name, string status)
        {
            Name = name;
            Status = status;
        }

        public string Name { get; }

        public string Status { get; }
    }
}
