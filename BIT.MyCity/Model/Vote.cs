﻿using BIT.MyCity.Database;
using System.Collections.Generic;

namespace BIT.MyCity.Model
{
    public class VoteAction
    {
        public long VoteId { get; set; }
        public string Comment { get; set; }
        public List<long> VoteItemIds { get; set; } = new List<long>();
        public List<int> VoteItemNumbers { get; set; } = new List<int>();
    }
    public class VoteOffer
    {
        public string Text { get; set; }
        public User User { get; set; }
    }
    public class VoteWIV
    {
        public bool Voted { get; set; }
        public string Text { get; set; }
        public List<long> VoteItemsIds { get; set; } = new List<long>();
    }
}
