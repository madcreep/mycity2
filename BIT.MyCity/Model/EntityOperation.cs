using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using BIT.MyCity.Attributes;

namespace BIT.MyCity.Model
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum EntityOperation
    {
        [MapNameEnum("Не определена")]
        [EnumMember(Value = "UNDEFINED")]

        UNDEFINED = 0,

        [MapNameEnum("Создание")]
        [EnumMember(Value = "CREATING")]
        CREATING = 1,

        [MapNameEnum("Изменение")]
        [EnumMember(Value = "CHANGING")]
        CHANGING = 2,

        [MapNameEnum("Удаление")]
        [EnumMember(Value = "DELETING")]
        DELETING = -1
    }
}
