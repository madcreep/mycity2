using System.Collections.Generic;

namespace BIT.MyCity.Model
{
    public class RoleFilter
    {
        public ICollection<long> Id { get; set; }

        public long? UserId { get; set; }

        public bool? Disconnected { get; set; }

        public bool? Editable { get; set; }
    }
}
