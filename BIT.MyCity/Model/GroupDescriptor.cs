using System.Collections.Generic;

namespace BIT.MyCity.Model
{
    public class GroupDescriptorSettings
    {
        /// <summary>
        /// Readonly
        /// </summary>
        public bool Readonly { get; set; }
    }
}
