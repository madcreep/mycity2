namespace BIT.MyCity.Model
{
    public class UserSubsystemDirectoryId
    {
        public long[] Ids { get; set; }

        public long? SubsystemId { get; set; }

        public string SubsystemUid { get; set; }
    }
}
