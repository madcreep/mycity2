using System;
using BIT.MyCity.Database;

namespace BIT.MyCity.Model
{
    public class AppealStatus
    {
        public long Id { get; }
        public long Weight { get; }
        public long? RubricId { get; }
        public Rubric Rubric { get; }
        public long? RegionId { get; }
        public Region Region { get; }
        public long? TerritoryId { get; }
        public Territory Territory { get; }
        public long? TopicId { get; }
        public Topic Topic { get; }
        public int SiteStatus { get; }
        public string ExtNumber { get; }
        public DateTime? ExtDate { get; }

        public AppealStatus(Appeal appeal)
        {
            if (appeal == null)
            {
                return;
            }

            Id = appeal.Id;
            Weight = appeal.Weight;
            RubricId = appeal.RubricId;
            Rubric = appeal.Rubric;
            RegionId = appeal.RegionId;
            Region = appeal.Region;
            TerritoryId = appeal.TerritoryId;
            Territory = appeal.Territory;
            TopicId = appeal.TopicId;
            Topic = appeal.Topic;
            SiteStatus = appeal.SiteStatus;
            ExtNumber = appeal.ExtNumber;
            ExtDate = appeal.ExtDate;
        }
    }
}
