using BIT.MyCity.Database;
namespace BIT.MyCity.Model
{
    public class RegisterUserModel : User
    {
        public string Password { get; set; }

        public string ConfirmPassword { get; set; }
    }
}
