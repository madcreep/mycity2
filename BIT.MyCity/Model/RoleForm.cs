using System;

namespace BIT.MyCity.Model
{
    public class RoleForm
    {
        public long? Id { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public bool SpecificRight { get; set; }
    }
}
