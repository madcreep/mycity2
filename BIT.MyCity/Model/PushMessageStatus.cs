namespace BIT.MyCity.Model
{
    public enum PushMessageStatus
    {
        /// <summary>
        /// Создано
        /// </summary>
        Created = 0,
        /// <summary>
        /// Принято сервисом
        /// </summary>
        Accepted = 1,
        /// <summary>
        /// Находится в очереди
        /// </summary>
        Queued = 2,
        /// <summary>
        /// Доставлено
        /// </summary>
        Delivered = 3,
        /// <summary>
        /// Ошибка (возможна повторная отправка)
        /// </summary>
        Error = -1,
        /// <summary>
        /// Ошибка сообщения не требующая повторных
        /// попыток отправки
        /// </summary>
        FatalError = -2,

    }
}