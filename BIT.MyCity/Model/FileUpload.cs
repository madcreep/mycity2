﻿namespace BIT.MyCity.Model
{
    public class FileUploadForm
    {
        public long Id { get; set; }
        public long ParentId { get; set; }
        public string ParentType { get; set; }
        public bool Sequre { get; set; }
    }

    public class FileUpload
    {
        public string Name { get; set; }
        public long? Id { get; set; }
        public long Len { get; set; }
        public long Pos { get; set; }
        public string Data { get; set; }
        public long? ParentId { get; set; }
        public string ParentType { get; set; }
        public string ContentType { get; set; }
        public bool? Sequre { get; set; }
    }
    public class FileUploadResult
    {
        public long Id { get; set; }
        public long Next { get; set; }
        public int Percent { get; set; }
    }
    public class FileDownloadRequest
    {
        public long Id { get; set; }
        public long Len { get; set; }
        public long Pos { get; set; }
    }
    public class FileDownload
    {
        public long Id { get; set; }
        public long Pos { get; set; }
        public string Data { get; set; }
    }
}
