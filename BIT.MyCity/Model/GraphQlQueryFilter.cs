using System;
using BIT.MyCity.ApiInterface.Models;
using System.Collections.Generic;
using BIT.GraphQL.Extensions.Filters;
using BIT.MyCity.Extensions;
using SkiaSharp;

namespace BIT.MyCity.Model
{
    public class QueryOptions
    {
        public List<GraphQlQueryFilter> Filters { get; set; }

        //public List<FilterItem> FiltersNew { get; set; }

        public SelectionRange Range { get; set; }
        public bool WithDeleted { get; set; } = false;
        public bool WithLinkedByOrganizations { get; set; } = false;
        public string ThumbSize { get; set; }
        public string ThumbSizeTolerance { get; set; }
        public SKEncodedImageFormat? ThumbFormat { get; set; }
        public GraphQlKeyDictionary RequestedFields { get; set; }
    }

    public enum FilterNextOpEnum
    {
        AND,
        OR,
        EXCEPT,
        PUSH,
        XY,
        POP,
    }
    public class GraphQlQueryFilter : ICloneable
    {
        public string Name { get; set; }
        public string Like { get; set; }
        public bool? Not { get; set; }
        public List<string> In { get; set; } = new List<string>();
        public string Equal { get; set; }
        public string More { get; set; }
        public string Less { get; set; }
        public string GreaterOrEqual { get; set; }
        public string LessOrEqual { get; set; }
        public FilterNextOpEnum Op { get; set; } = FilterNextOpEnum.AND;
        public bool? WithDeleted { get; set; }
        public bool? Stacked { get; set; }
        public string Where { get; set; }
        public object Clone()
        {
            return new GraphQlQueryFilter
            {
                Name = Name,
                Like = Like,
                Not = Not,
                In = In == null
                        ? null
                        : new List<string>(In),
                Equal = Equal,
                More = More,
                Less = Less,
                GreaterOrEqual = GreaterOrEqual,
                LessOrEqual = LessOrEqual,
                Op = Op,
                WithDeleted = WithDeleted,
                Stacked = Stacked
            };
        }
    }

    public class SelectionRange
    {
        public OrderByItem[] OrderBy { get; set; }
        public int? Skip { get; set; }
        public int? Take { get; set; }
    }

    public class FilterSearch
    {
        public string Name { get; set; }
        public string Match { get; set; }
    }

    public class RangeSearch
    {
        public int? Skip { get; set; }
        public int? Take { get; set; }
    }
}
