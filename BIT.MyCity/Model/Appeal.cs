﻿using BIT.MyCity.Database;
using System;

namespace BIT.MyCity.Model
{
    public class AppealApprove
    {
        public bool? Public_ { get; set; }
        public ApprovedEnum? ApprovedEnum { get; set; } = Database.ApprovedEnum.UNDEFINED;
        public string RejectionReason { get; set; }
        public ModerationStageEnum? ModerationStage { get; set; } = Database.ModerationStageEnum.WAIT;
    }

    public class AppealsStatBy
    {
        public Rubric Rubric { get; set; }
        public Region Region { get; set; }
        public Territory Territory { get; set; }
        public Topic Topic { get; set; }
        public DateTime CreatedAtYear { get; set; }
        public DateTime UpdatedAtYear { get; set; }
        public DateTime CreatedAtMonth { get; set; }
        public DateTime UpdatedAtMonth { get; set; }
        public DateTime CreatedAtDay { get; set; }
        public DateTime UpdatedAtDay { get; set; }
        public DateTime CreatedAtHour { get; set; }
        public DateTime UpdatedAtHour { get; set; }
        //public int CreatedAtYear { get; set; }
        //public int UpdatedAtYear { get; set; }
        //public int CreatedAtMonth { get; set; }
        //public int UpdatedAtMonth { get; set; }
        //public int CreatedAtDay { get; set; }
        //public int UpdatedAtDay { get; set; }
        //public int CreatedAtHour { get; set; }
        //public int UpdatedAtHour { get; set; }

        public long AppealCount { get; set; }
    }
}
