﻿using BIT.MyCity.Database;

namespace BIT.MyCity.Model
{
    public class MessageApprove
    {
        public bool? Public_ { get; set; }
        public ApprovedEnum? ApprovedEnum { get; set; } = Database.ApprovedEnum.UNDEFINED;
        public ModerationStageEnum? ModerationStage { get; set; } = Database.ModerationStageEnum.WAIT;
        public string Reason { get; set; }
    }
    public class MessageDelete
    {
        public string Reason { get; set; }
    }
}
