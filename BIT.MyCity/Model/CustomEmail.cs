using System.Collections.Generic;

namespace BIT.MyCity.Model
{
    public class CustomEmail
    {
        /// <summary>
        /// От кого
        /// </summary>
        public EmailPerson From { get; set; }

        /// <summary>
        /// Кому
        /// </summary>
        public List<EmailPerson> To { get; set; }

        /// <summary>
        /// Tема
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Идентификаторы прикрепляемы файлов
        /// </summary>
        public List<long> FilesId { get; set; }

        /// <summary>
        /// С уведомлением о доставке
        /// </summary>
        public bool WithDeliveryNotice { get; set; } = false;

        /// <summary>
        /// Дублировать отправителю
        /// </summary>
        public bool DuplicateToSender { get; set; } = false;
    }

    public class EmailPerson
    {
        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Имя отправителя/получателя
        /// </summary>
        public string Name { get; set; }
    }
}
