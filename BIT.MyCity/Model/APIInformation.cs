﻿using System;

namespace BIT.MyCity.Model
{
    public class ApiInformation
    {
        public string OsDescription { get; set; }
        public string AspDotnetVersion { get; set; }
        public string AppVersion { get; set; }
        public DateTime AppBuildTime { get; set; }
        public string AppConfiguration { get; set; }
        public string ASPNETCORE_ENVIRONMENT { get; set; }
        public string DbVersion { get; set; }
    }
}
