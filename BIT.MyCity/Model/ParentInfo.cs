using BIT.MyCity.Database;

namespace BIT.MyCity.Model
{
    public class ParentInfo : IHasParent
    {
        public ParentInfo(long? parentId, string parentType)
        {
            ParentId = parentId;
            ParentType = parentType;
        }

        public long? ParentId { get; set; }

        public string ParentType { get; set; }
    }
}
