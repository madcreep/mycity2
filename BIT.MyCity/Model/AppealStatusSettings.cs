using System.Linq;
using Newtonsoft.Json;

namespace BIT.MyCity.Model
{
    public class AppealStatusSettings
    {
        public StatusOptions StatusOptions { get; set; }
    }

    public class StatusOptions
    {
        public int[] NextStatus { get; set; }

        public bool IsInitial { get; set; }

        [JsonIgnore] public bool IsFinal => !NextStatus.Any();

        public AppealStatusEmail Email { get; set; }
    }

    public class AppealStatusEmail
    {
        public bool Send { get; set; }

        public string Template { get; set; }

        /// <summary>
        /// С уведомлением о доставке
        /// </summary>
        public bool WithDeliveryNotice { get; set; } = false;

        /// <summary>
        /// Дублировать отправителю
        /// </summary>
        public bool DuplicateToSender { get; set; } = false;

        public AppealStatusSendAdditional SendAdditional { get; set; }
    }

    public class AppealStatusSendAdditional
    {
        public bool SendFiles { get; set; }

        public bool SendMessage { get; set; }

        public string MessageKind { get; set; }
    }
}
