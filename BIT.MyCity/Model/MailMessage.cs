using System.Collections.Generic;

namespace BIT.MyCity.Model
{
    public class MailMessage
    {
        public long[] SendUsersIds { get; set; }

        /// <summary>
        /// Имя получателя
        /// </summary>
        public string ToName { get; set; }

        /// <summary>
        /// Адрес получателя
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Дополнительные получатели
        /// </summary>
        public EmailPerson[] AdditionalRecipients { get; set; }

        /// <summary>
        /// Тема
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Template Key
        /// </summary>
        public string TemplateKey { get; set; }

        /// <summary>
        /// Модель
        /// </summary>
        public object TemplateModel { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Идентификаторы подгружаемых сущностей
        /// </summary>
        public List<EmailEntityMetadata> AddingEntity { get; set; } = new List<EmailEntityMetadata>();

        /// <summary>
        /// Идентификаторы включаемых файлов
        /// </summary>
        public long[] FilesIds { get; set; }

        /// <summary>
        /// Электронный адрес отправителя
        /// </summary>
        public string FromEmail { get; set; }

        /// <summary>
        /// Имя отправителя
        /// </summary>
        public string FromName { get; set; }

        /// <summary>
        /// С уведомлением о доставке
        /// </summary>
        public bool WithDeliveryNotice { get; set; }

        /// <summary>
        /// Дублировать отправителю
        /// </summary>
        public bool DuplicateToSender { get; set; }
    }
}
