using BIT.MyCity.Helpers;

namespace BIT.MyCity.Model
{
    public class JournalOperation
    {
        public JournalOperation(EntityOperation operation)
        {
            Operation = operation;
        }

        public EntityOperation Operation { get; }

        public string OperationName => Operation.MapName();
    }
}
