using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BIT.MyCity.Model
{
    public class HttpRequestParams
    {
        public string RequestScheme { get; set; }

        public string RequestHost { get; set; }

        public string RemoteIpAddress { get; set; }
    }
}
