﻿using System;

namespace BIT.MyCity.Model
{
    public enum LikeRequestEnum
    {
        NONE,
        LIKE,
        LIKE_OFF,
        DISLIKE,
        DISLIKE_OFF,
    }
    public enum LikeStateEnum
    {
        NONE,
        LIKED,
        DISLIKED,
    }

    public class LikeRequest
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public LikeRequestEnum Liked { get; set; }
    }
    public class LikeReport
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public LikeStateEnum Liked { get; set; } = LikeStateEnum.NONE;
        public int LikeCount { get; set; }
        public int DislikeCount { get; set; }
    }
    public class ViewRequest
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public bool Viewed { get; set; }
    }
    public class ViewReport
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public bool Viewed { get; set; }
        public int ViewCount { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
    public class RegardRequest
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public int Regard { get; set; }
    }
    public class RegardReport
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public int Regard { get; set; }
        public int RegardCount { get; set; }
    }
    public class LikeWIL
    {
        public LikeStateEnum Liked { get; set; } = LikeStateEnum.NONE;
    }

    public class ViewWIV
    {
        public bool Viewed { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
    }
    public class RegardWIR
    {
        public int Regard { get; set; }
    }
}
