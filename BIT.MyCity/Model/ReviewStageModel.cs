using System;
using BIT.MyCity.Database;

namespace BIT.MyCity.Model
{
    public class ReviewStageModel
    {
        public Guid Uuid { get; set; }
        public Guid DocumentUuid { get; set; }
        public int DocumentId { get; set; }
        public DateTime Date { get; set; }
        public string Number { get; set; }
        public StageTypes StageType { get; set; }
        public string ProfileCommittee { get; set; }
        public DateTime? SendDate { get; set; }
    }

}