using System;

namespace BIT.MyCity.Model
{
    public class RubricModel
    {
//        public Guid Uuid { get; set; }

        public long Id { get; set; }
        public string Due { get; set; }
        public string Name { get; set; }
    }
}
