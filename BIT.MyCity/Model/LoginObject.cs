namespace BIT.MyCity.Model
{
    public class LoginObject
    {
        public string Token { get; set; }

        public UserForm User { get; set; }
    }
}
