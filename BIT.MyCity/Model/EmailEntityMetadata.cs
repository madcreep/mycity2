using System.Linq;
using BIT.MyCity.Database;

namespace BIT.MyCity.Model
{
    public class EmailEntityMetadata
    {
        public string TypeName { get; set; }

        public string CustomName { get; set; }

        public object[] KeyValue { get; set; }

        public object Entity { get; set; }

        public static EmailEntityMetadata Generate(DocumentsContext ctx, object entity, string customName = null)
        {
            var result = new EmailEntityMetadata();

            var entityType = entity.GetType();

            result.TypeName = entityType.FullName;

            result.CustomName = string.IsNullOrWhiteSpace(customName)
                ? entityType.Name
                : customName;

            var hasInEf = ctx.Model
                .GetEntityTypes()
                .Any(el => el.ClrType.FullName == entityType.FullName);

            if (hasInEf)
            {
                var entry = ctx.Entry(entity);

                var keyParts = entry.Metadata.FindPrimaryKey()
                    .Properties
                    .Select(p => entry.Property(p.Name).CurrentValue)
                    .ToArray();

                result.KeyValue = keyParts;
            }
            else
            {
                result.Entity = entity;
            }

            return result;
        }
    }
}
