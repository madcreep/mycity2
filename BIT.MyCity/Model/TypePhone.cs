namespace BIT.MyCity.Model
{
    public enum TypePhone
    {
        /// <summary>
        /// Не определен
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// Домашний
        /// </summary>
        Phone = 1,
        /// <summary>
        /// Рабочий
        /// </summary>
        Work = 2,
        /// <summary>
        /// Мобильный
        /// </summary>
        Mobile = 3,
        /// <summary>
        /// Факс
        /// </summary>
        Fax = 4
    }
}
