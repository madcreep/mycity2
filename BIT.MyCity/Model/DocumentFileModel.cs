using System;

namespace BIT.MyCity.Model
{
    public class DocumentFileModel
    {
        public Guid Uuid { get; set; }

        public int Id { get; set; }
        
        public string FileName { get; set; }
        
        public string Extension { get; set; }

        public int? Size { get; set; }

        public string Description { get; set; }
        
        public int? OrderNum { get; set; }
        
        public bool Send { get; set; }
        
        public Guid DocumentUuid { get; set; }
        public int DocumentId{ get; set; }
        
    }
}