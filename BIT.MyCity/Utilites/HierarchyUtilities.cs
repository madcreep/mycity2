using System;
using System.Collections.Generic;
using System.Linq;

namespace BIT.MyCity.Utilites
{
    public static class HierarchyUtilities
    {
        public static IEnumerable<T> ToHierarchy<T>(
            IEnumerable<T> inObj,
            Func<T, string> treeKey,
            Action<T, T> setParent,
            Action<T, T> addChildren)
            where T : class
        {
            var tmpArray = inObj
                .OrderBy(el => treeKey(el));

            var result = new List<T>();

            var stack = new Stack<T>();

            foreach (var item in tmpArray)
            {
                if (!stack.TryPeek(out var top))
                {
                    top = null;
                }

                while (top != null && !treeKey(top).StartsWith(treeKey(item)))
                {
                    stack.Pop();

                    if (!stack.TryPeek(out top))
                    {
                        top = null;
                    }
                }

                setParent(item, top);

                if (top != null)
                {
                    addChildren(top, item);
                }
                else
                {
                    result.Add(item);
                }

                stack.Push(item);
            }

            return result;
        }
    }
}
