using System;
using System.Collections.Generic;
using System.Linq;

namespace BIT.MyCity.Utilites
{
    public static class UpdateUtilites
    {
        public static void UpdateEntityByFieldsList<T>(T dest, T src, IEnumerable<string> fields)
        {
            var fieldNameArray = fields as string[] ?? fields.ToArray();

            if (dest == null || src == null || fieldNameArray.All(String.IsNullOrWhiteSpace))
            {
                return;
            }

            var destType = dest.GetType();

            var srcType = src.GetType();

            foreach (var field in fieldNameArray)
            {
                var destProperty = destType
                  .GetProperty(field, System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);

                var srcProperty = srcType
                  .GetProperty(field, System.Reflection.BindingFlags.IgnoreCase | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);

                if (destProperty != null && srcProperty != null)
                {
                    destProperty.SetValue(dest, srcProperty.GetValue(src));
                }
            }
        }
    }
}
