using System.Text.RegularExpressions;

namespace BIT.MyCity.Utilites
{
    public static class EmailUtility
    {
        internal static bool CheckEmail(string eMail)
        {
            const string eMailPattern =
                "^(?(\")(\".+?(?<!\\\\)\"@)|(([0-9a-zа-я]((\\.(?!\\.))|[-!#\\$%&'\\*\\+/=\\?\\^`\\{\\}\\|~\\w])*)(?<=[0-9a-zа-я])@))(?(\\[)(\\[(\\d{1,3}\\.){3}\\d{1,3}\\])|(([0-9a-zа-я][-0-9a-zа-я]*[0-9a-zа-я]*\\.)+[a-zа-я0-9][\\-a-z-а-я0-9]{0,22}[a-zа-я0-9]))$";

            return Regex.IsMatch(eMail, eMailPattern, RegexOptions.IgnoreCase);
        }
    }
}
