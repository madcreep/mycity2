using System;
using System.Globalization;
using System.Linq;

namespace BIT.MyCity.Reports
{
    public class ReportSettingsNumberRange
    {
        private decimal? _from;
        private decimal? _to;

        public ReportSettingsNumberRange(decimal? from, decimal? to)
        {
            _from = from;
            _to = to;

            if (!IsValid)
            {
                throw new ArgumentException($"{nameof(From)} должна быть больше или равна {nameof(To)}", nameof(To));
            }
        }

        public ReportSettingsNumberRange()
        : this(null, null)
        { }
        
        public decimal? From
        {
            get => _from;
            private set => _from = value;
        }

        public decimal? To
        {
            get => _to;
            private set => _to = value;
        }

        public bool HasValues => _from.HasValue || _to.HasValue;

        public string RangeString
        {
            get
            {
                if (!_from.HasValue && !_to.HasValue)
                {
                    return null;
                }

                var fromDate = _from.HasValue ? _from.Value.ToString(CultureInfo.InvariantCulture) : "null";

                var toDate = _to.HasValue ? _to.Value.ToString(CultureInfo.InvariantCulture) : "null";

                return $"{fromDate}:{toDate}";

            }
        }

        public bool InRange(decimal value)
        {
            return (!_from.HasValue || value >= _from)
                   && (!_to.HasValue || value <= _to);
        }

        public bool IsValid
        {
            get
            {
                if (!From.HasValue || !To.HasValue)
                {
                    return true;
                }

                return From.Value <= To.Value;
            }
        }

        public static bool TryParse(string value, out ReportSettingsNumberRange range)
        {
            range = null;

            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            var dateItems = value.Split(':')
                .Select(el=>el.Trim())
                .ToArray();

            if (dateItems.Length != 2)
            {
                return false;
            }

            if (!ParseNumber(dateItems[0], out var fromNumber)
                || !ParseNumber(dateItems[1], out var toNumber))
            {
                return false;
            }

            range = new ReportSettingsNumberRange
            {
                _from = fromNumber,
                _to = toNumber
            };

            return true;
        }

        private static bool ParseNumber(string numberString, out decimal? value)
        {
            value = null;

            if (numberString == "null" || string.IsNullOrWhiteSpace(numberString))
            {
                return true;
            }

            var tmpString = numberString
                .Replace(',', '.')
                .Replace(" ", string.Empty);

            const NumberStyles style = NumberStyles.Number | NumberStyles.AllowCurrencySymbol;

            var culture = CultureInfo.InvariantCulture;

            if (!decimal.TryParse(tmpString, style, culture, out var result))
            {
                return false;
            }
            
            value = result;

            return true;
        }
    }
}
