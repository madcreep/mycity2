using System;
using System.Collections.Generic;
using System.Linq;

namespace BIT.MyCity.Reports
{
    public class ReportSettings
    {
        private Dictionary<string, ReportSingleSettings> _parameters = new Dictionary<string, ReportSingleSettings>();
        
        public Guid ReportId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int? TimeOffset { get; set; }

        public ReportSingleSettings this[string name]
        {
            get => _parameters.ContainsKey(name) ? _parameters[name] : null;
            set
            {
                if (value == null)
                {
                    return;
                }

                if (_parameters.ContainsKey(value.Name))
                {
                    _parameters[name] = value;
                }
                else
                {
                    _parameters.Add(value.Name, value);
                }
            }
        }
        
        public ReportSingleSettings[] Parameters
        {
            get => _parameters.Values.ToArray();
            set => _parameters = value.ToDictionary(el => el.Name);
        }
        
        internal ReportSettings() { }


        public ReportSettings(Guid reportId)
        {
            ReportId = reportId;
        }
    }
}
