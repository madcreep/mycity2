namespace BIT.MyCity.Reports
{
    public class NumberRangeSingleSettingsOptions
    {
        public decimal? MaxValue { get; set; }

        public decimal? MinValue { get; set; }

        public NumberRangeType RangeType { get; set; }

        /// <summary>
        /// Знаков после запятой
        /// </summary>
        public uint DecimalPlaces { get; set; }
    }
}
