using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BIT.MyCity.Reports
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SelectionMode
    {
        Single,
        Multi
    }
}
