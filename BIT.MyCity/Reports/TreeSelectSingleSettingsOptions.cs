namespace BIT.MyCity.Reports
{
    public class TreeSelectSingleSettingsOptions
    {
        public TreeListElementSelection SelectOnlyMode { get; set; }
        public SelectionMode SelectMode { get; set; }
    }
}
