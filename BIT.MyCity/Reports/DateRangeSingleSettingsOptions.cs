namespace BIT.MyCity.Reports
{
    public class DateRangeSingleSettingsOptions : DateRangeSingleSettingsSaveOptions
    {
        public DateRangeControlMode[] AvailableDateRanges { get; set; }
    }

    public class DateRangeSingleSettingsSaveOptions
    {
        public int DateRangePlusN { get; set; }
        public DateRangeControlMode? DateRangeMode { get; set; }
    }
}
