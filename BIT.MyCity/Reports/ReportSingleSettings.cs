using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BIT.MyCity.Reports
{
    public class ReportSingleSettings : ReportValuesSingleSetting
    {
        /// <summary>
        /// Тип элемента управления
        /// </summary>
        public ReportSettingsControlType ControlType { get; set; }
        
        /// <summary>
        /// Отображаемое название параметра
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Обязательный параметр
        /// </summary>
        public bool Required { get; set; }
        
        /// <summary>
        /// Перечисление возможных значений параметров
        /// </summary>
        public string Values { get; set; }
        
        /// <summary>
        /// Показывать чекбокс "Все"
        /// </summary>
        public bool? AllEnable { get; set; }
    }

    public class ReportValuesSingleSetting
    {
        /// <summary>
        /// Название параметра
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Выбранные значения параметров
        /// </summary>
        public string Value { get; set; }
        
        /// <summary>
        /// Состояние чекбокса "Все"
        /// </summary>
        public bool? AllSelected { get; set; }

        /// <summary>
        /// Опции настройки элемента
        /// </summary>
        public string Options { get; set; }

        public void SetOptions(object options)
        {
            if (options == null)
            {
                return;
            }

            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            Options = JsonConvert.SerializeObject(options, Formatting.None, serializerSettings);
        }

        public object GetOptions<T>()
        {
            return string.IsNullOrWhiteSpace(Options)
                ? null
                : JsonConvert.DeserializeObject(Options, typeof(T));
        }
    }
}
