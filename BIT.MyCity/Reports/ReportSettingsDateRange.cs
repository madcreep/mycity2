using System;
using System.Globalization;
using System.Linq;

namespace BIT.MyCity.Reports
{
    public class ReportSettingsDateRange
    {
        private const string DateFormat = "yyyy-MM-dd";

        private DateTime? _from;
        private DateTime? _to;

        public DateTime? From
        {
            get => _from;
            private set => _from = value?.Date;
        }

        public DateTime? To
        {
            get => _to;
            private set => _to = value?.Date;
        }
        
        public string RangeString
        {
            get
            {
                var fromDate = _from.HasValue
                    ? _from.Value.ToString(DateFormat, CultureInfo.InvariantCulture)
                    : "null";

                var toDate = _to.HasValue
                    ? _to.Value.ToString(DateFormat, CultureInfo.InvariantCulture)
                    : "null";

                return $"{fromDate}:{toDate}";
            }
        }
        
        public ReportSettingsDateRange(DateTime? fromDate = null, DateTime? toDate = null)
        {
            From = fromDate?.Date;

            To = toDate?.Date;

            if (!IsValid)
            {
                throw new ArgumentException($"{nameof(toDate)} должна быть больше или равна {nameof(fromDate)}", nameof(toDate));
            }
        }

        public bool InRange(DateTime date)
        {
            return (!_from.HasValue || date >= _from)
                   && (!_to.HasValue || date <= _to);
        }

        public bool IsValid
        {
            get
            {
                if (!From.HasValue || !To.HasValue)
                {
                    return true;
                }

                return From.Value <= To.Value;
            }
        }

        public static bool TryParse(string value, out ReportSettingsDateRange range)
        {
            range = null;

            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            var dateItems = value.Split(':')
                .Select(el => el.Trim())
                .ToArray();

            if (dateItems.Length != 2)
            {
                return false;
            }

            if (!ParseDate(dateItems[0], out var fromDate)
                || !ParseDate(dateItems[1], out var toDate))
            {
                return false;
            }

            range = new ReportSettingsDateRange
            {
                _from = fromDate,
                _to = toDate
            };

            return true;
        }

        private static bool ParseDate(string dateString, out DateTime? date)
        {
            date = null;

            if (dateString == "null" || string.IsNullOrWhiteSpace(dateString))
            {
                return true;
            }

            if (!DateTime.TryParseExact(dateString, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out var fromDate))
            {
                return false;
            }

            date = fromDate;

            return true;
        }
    }
}
