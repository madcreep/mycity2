using System;
using System.Collections.Generic;
using System.Linq;
using BIT.MyCity.Database;

namespace BIT.MyCity.Reports
{
    public class ExecuteReportResult
    {
        public ExecuteReportResult(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }

        public bool Success => !Errors.Any();

        public List<string> Errors { get; } = new List<string>();

        public string Template { get; set; }

        public AttachedFile File { get; set; }
    }
}
