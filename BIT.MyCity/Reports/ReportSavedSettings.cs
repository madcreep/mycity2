using System.Collections.Generic;

namespace BIT.MyCity.Reports
{
    public class ReportSavedSettings
    {
        public uint? Id { get; set; }

        public string Name { get; set; }

        public List<ReportValuesSingleSetting> Parameters { get; set; }
    }
}
