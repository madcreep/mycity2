using System.Diagnostics.CodeAnalysis;
using System.IO;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.Pdf.Parsing;
using Syncfusion.XlsIO;
using Syncfusion.XlsIO.Implementation;

namespace BIT.MyCity.Reports
{
    public static class TemplateGenerator
    {
        public static string Generate([NotNull] AttachedFile aFile, [NotNull] AttachedFileManager fileManager)
        {
            var fileName = fileManager.GetFileFullName(aFile);

            if (string.IsNullOrWhiteSpace(fileName) || !File.Exists(fileName))
            {
                return null;
            }

            var extension = Path.GetExtension(aFile.Name)?
                .Trim(' ', '.')
                .ToLower();

            switch (extension)
            {
                case "doc":
                case "docx":
                    return GenerateWord(fileName);
                case "xls":
                case "xlsx":
                    return GenerateExcel(fileName);
                case "pdf":
                    return GeneratePdf(fileName);
                default:
                    return $"<h1>Не удалось сгенерировать разметку для расширения файла '{extension}'.</h1>";
            }
        }

        private static string GenerateExcel(string fileName)
        {
            string result;

            var excelEngine = new ExcelEngine();

            var application = excelEngine.Excel;

            application.DefaultVersion = ExcelVersion.Excel2016;

            using var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read)
            {
                Position = 0
            };
            
            var workbook = application.Workbooks.Open(fileStream);

            var worksheet = workbook.Worksheets[0];

            using var memoryStream = new MemoryStream();

            try
            {
                worksheet.SaveAsHtml(memoryStream, new HtmlSaveOptions
                {
                    TextMode = HtmlSaveOptions.GetText.Value
                });
                memoryStream.Position = 0;
                var reader = new StreamReader(memoryStream);
                result = reader.ReadToEnd();
            }
            finally
            {
                workbook.Close();
                excelEngine.Dispose();
            }

            return result;
        }

        private static string GenerateWord(string fileName)
        {
            using var fileStream = File.OpenRead(fileName);

            var document = new WordDocument(fileStream, FormatType.Docx);

            using var stream = new MemoryStream();

            document.Save(stream, FormatType.Html);

            document.Close();

            stream.Position = 0;

            var reader = new StreamReader(stream);

            return reader.ReadToEnd();
        }

        private static string GeneratePdf(string fileName)
        {
            using var fileStream = File.OpenRead(fileName);

            var document = new PdfLoadedDocument(fileStream);

            using var stream = new MemoryStream();

            document.Save(stream);

            document.Close(true);

            stream.Position = 0;

            var reader = new StreamReader(stream);

            return reader.ReadToEnd();
        }
    }
}
