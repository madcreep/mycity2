using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Linq;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Reports
{

    public abstract class BaseReport: IReport
    {
        public const string FileTypeName = "report_file";

        public abstract string SubsystemUid { get; }

        public abstract Guid Id { get; }

        public abstract string Name { get; }

        public abstract string Description { get; }

        public abstract Task<ReportSettings> GetSettings(IServiceProvider servicesProvider);

        public abstract Task<ExecuteReportResult> Execute(IServiceProvider servicesProvider, ReportSettings settings);

        public virtual IEnumerable<string> TestSettings(IServiceProvider servicesProvider, ReportSettings defSettings, ReportSettings settings)
        {
            var result = new List<string>();

            if (!settings.TimeOffset.HasValue)
            {
                result.Add("Укажите локальное смещение времени.");
            }

            foreach (var def in defSettings.Parameters)
            {
                result.AddRange(TestSingleSettings(def, settings.Parameters.SingleOrDefault(el=>el.Name == def.Name)));
            }

            return result;
        }

        private static IEnumerable<string> TestSingleSettings(ReportSingleSettings def, ReportSingleSettings test)
        {
            var result = new List<string>();

            if (test == null)
            {
                result.Add($"Отсутствует параметр <{def.Title}>");

                return result;
            }

            switch (def.ControlType)
            {
                case ReportSettingsControlType.DateRange:
                    result.AddRange(TestDateRangeSingleSettings(def, test));
                    break;
                case ReportSettingsControlType.SingleCheckbox:
                    result.AddRange(TestSingleCheckboxSettings(def, test));
                    break;
                case ReportSettingsControlType.ListSelect:
                    result.AddRange(TestListSelectSettings(def, test));
                    break;
                case ReportSettingsControlType.TreeSelect:
                    result.AddRange(TestTreeSelectSettings(def, test));
                    break;
                case ReportSettingsControlType.NumberRange:
                    result.AddRange(TestNumberRangeSettings(def, test));
                    break;
                case ReportSettingsControlType.DiscreteSelector:
                    result.AddRange(TestDiscreteSelectorSettings(def, test));
                    break;
                case ReportSettingsControlType.CustomText:
                    result.AddRange(CustomTextRangeSettings(def, test));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return result;
        }

        private static IEnumerable<string> TestDateRangeSingleSettings(ReportSingleSettings def, ReportSingleSettings test)
        {
            var result = new List<string>();

            if (!ReportSettingsDateRange.TryParse(test.Value, out var range))
            {
                result.Add($"Не верный формат значения параметра <{def.Title}>.");
            }
            else
            {
                if (!range.IsValid)
                {
                    result.Add($"Для параметра <{def.Title}> задан не валидный интервал ({test.Value}).");
                }
            }

            if (!(test.GetOptions<DateRangeSingleSettingsOptions>() is DateRangeSingleSettingsOptions options))
            {
                result.Add($"Для параметра <{def.Title}> не заданы опциональные параметры.");

                return result;
            }

            var defOptions = (DateRangeSingleSettingsOptions)def.GetOptions<DateRangeSingleSettingsOptions>();

            if (!options.DateRangeMode.HasValue)
            {
                result.Add($"Для параметра <{def.Title}> не задано опциональное значение свойства <{nameof(DateRangeSingleSettingsOptions.DateRangeMode)}>.");
            }
            else
            {
                if (defOptions.AvailableDateRanges != null
                    && defOptions.AvailableDateRanges.Any()
                    && !defOptions.AvailableDateRanges.Contains(options.DateRangeMode.Value))
                {
                    result.Add($"Для параметра <{def.Title}> задано недопустимое значение свойства <{nameof(DateRangeSingleSettingsOptions.DateRangeMode)}>.");
                }

                if (range != null)
                {
                    switch (options.DateRangeMode.Value)
                    {
                        case DateRangeControlMode.All:
                        case DateRangeControlMode.Range:
                            break;
                        case DateRangeControlMode.CurrentDay:
                        case DateRangeControlMode.SingleDay:
                        case DateRangeControlMode.CurrentDayPlusN:
                            if (!range.From.HasValue)
                            {
                                result.Add(
                                    $"Для параметра <{def.Title}> начальное значение интервала должно быть установленно.");
                            }

                            if (!range.To.HasValue)
                            {
                                result.Add(
                                    $"Для параметра <{def.Title}> конечное значение интервала должно быть установленно.");
                            }

                            if (range.From != range.To)
                            {
                                result.Add(
                                    $"Для параметра <{def.Title}> начальное значение интервала должно равно конечному.");
                            }
                            break;
                        case DateRangeControlMode.PlusNAndBefore:
                            if (range.From.HasValue)
                            {
                                result.Add(
                                    $"Для параметра <{def.Title}> начальное значение интервала не должно быть установленно.");
                            }

                            if (!range.To.HasValue)
                            {
                                result.Add(
                                    $"Для параметра <{def.Title}> конечное значение интервала должно быть установленно.");
                            }
                            break;
                        case DateRangeControlMode.PlusNAndLater:
                            if (!range.From.HasValue)
                            {
                                result.Add(
                                    $"Для параметра <{def.Title}> начальное значение интервала должно быть установленно.");
                            }

                            if (range.To.HasValue)
                            {
                                result.Add(
                                    $"Для параметра <{def.Title}> конечное значение интервала не должно быть установленно.");
                            }
                            break;
                        case DateRangeControlMode.CurrentWeek:
                        case DateRangeControlMode.CurrentMonth:
                        case DateRangeControlMode.CurrentQuarter:
                        case DateRangeControlMode.CurrentYear:
                        case DateRangeControlMode.PrevWeek:
                        case DateRangeControlMode.PrevMonth:
                        case DateRangeControlMode.PrevQuarter:
                        case DateRangeControlMode.PrevYear:
                        case DateRangeControlMode.LastWeek:
                        case DateRangeControlMode.LastMonth:
                        case DateRangeControlMode.LastQuarter:
                        case DateRangeControlMode.LastYear:
                            if (!range.From.HasValue)
                            {
                                result.Add(
                                    $"Для параметра <{def.Title}> начальное значение интервала должно быть установленно.");
                            }

                            if (!range.To.HasValue)
                            {
                                result.Add(
                                    $"Для параметра <{def.Title}> конечное значение интервала должно быть установленно.");
                            }
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }

            return result;
        }

        private static IEnumerable<string> TestSingleCheckboxSettings(ReportSingleSettings def, ReportSingleSettings test)
        {
            var result = new List<string>();

            if (!bool.TryParse(test.Value, out _))
            {
                result.Add($"Не верный формат значения параметра <{def.Title}>.");
            }

            return result;
        }

        private static IEnumerable<string> TestNumberRangeSettings(ReportSingleSettings def, ReportSingleSettings test)
        {
            var result = new List<string>();

            if (!ReportSettingsNumberRange.TryParse(test.Value, out var range))
            {
                result.Add($"Не верный формат значения параметра <{def.Title}>.");

                return result;
            }

            var defOptions = (NumberRangeSingleSettingsOptions)def.GetOptions<NumberRangeSingleSettingsOptions>();

            if (defOptions.RangeType == NumberRangeType.Single)
            {
                if (def.Required && !range.From.HasValue)
                {
                    result.Add(
                        $"Для параметра <{def.Title}> начальное значение должно быть установленно.");
                }
            }

            if (!range.IsValid)
            {
                result.Add($"Для параметра <{def.Title}> задан не валидный интервал ({test.Value}).");
            }

            if (range.From.HasValue)
            {
                var testValue = Math.Round(range.From.Value, (int)defOptions.DecimalPlaces);

                if (range.From.Value != testValue)
                {
                    result.Add($"Для параметра <{def.Title}> начальное значение должно иметь не больше {defOptions.DecimalPlaces} знаков в десятичной части.");
                }

                if (defOptions.MinValue.HasValue && range.From.Value < defOptions.MinValue.Value)
                {
                    result.Add($"Для параметра <{def.Title}> начальное значение должно быть больше {defOptions.MinValue}.");
                }

                if (defOptions.MaxValue.HasValue && range.From.Value > defOptions.MaxValue)
                {
                    result.Add($"Для параметра <{def.Title}> начальное значение должно быть меньше {defOptions.MaxValue}.");
                }
            }

            if (range.To.HasValue)
            {
                var testValue = Math.Round(range.To.Value, (int)defOptions.DecimalPlaces);

                if (range.To.Value != testValue)
                {
                    result.Add($"Для параметра <{def.Title}> конечное значение должно иметь не больше {defOptions.DecimalPlaces} знаков в десятичной части.");
                }

                if (defOptions.MinValue.HasValue && range.To.Value < defOptions.MinValue.Value)
                {
                    result.Add($"Для параметра <{def.Title}> конечное значение должно быть больше {defOptions.MinValue}.");
                }

                if (defOptions.MaxValue.HasValue && range.To.Value > defOptions.MaxValue.Value)
                {
                    result.Add($"Для параметра <{def.Title}> конечное значение должно быть меньше {defOptions.MaxValue}.");
                }
            }

            return result;
        }

        private static IEnumerable<string> CustomTextRangeSettings(ReportSingleSettings def, ReportSingleSettings test)
        {
            var result = new List<string>();

            if (def.Required && string.IsNullOrWhiteSpace(test.Value))
            {
                result.Add($"Для параметра <{def.Title}> значение должно быть задано.");

                return result;
            }

            var defOptions = (CustomTextSingleSettingsOptions)def.GetOptions<CustomTextSingleSettingsOptions>();

            if (defOptions.TextLength.HasValue
                && test.Value != null
                && test.Value.Length > defOptions.TextLength.Value)
            {
                result.Add(
                    $"Для параметра <{def.Title}> значение не должно превышать длинну в {defOptions.TextLength.Value} символов.");
            }

            return result;
        }

        private static IEnumerable<string> TestListSelectSettings(ReportSingleSettings def, ReportSingleSettings test)
        {
            var result = new List<string>();

            if (!ReportSettingsHelper.TryParseLongValues(test.Value, out var values))
            {
                result.Add($"Не верный формат значения параметра <{def.Title}>.");

                return result;
            }

            if (def.Required)
            {
                if ((test.AllEnable != true && !values.Any())
                        || (test.AllEnable == true && test.AllSelected != true && !values.Any()))
                {
                    result.Add(
                        $"Для параметра <{def.Title}> значение должно быть установленно.");
                }

                if (test.AllEnable == true
                    && test.AllSelected == true
                    && values.Any())
                {
                    result.Add(
                        $"Для параметра <{def.Title}> при установленном свойстве <Все> свойство <Value> не должно содержать элементов.");
                }
            }

            var defOptions = (ListSelectSingleSettingsOptions)def.GetOptions<ListSelectSingleSettingsOptions>();

            if (defOptions.SelectMode == SelectionMode.Single
                && values.Any() && values.Count != 1)
            {
                result.Add(
                    $"Для параметра <{def.Title}> свойство <Value> не должно содержать только 1 элемент.");
            }

            try
            {
                var availableValues = ReportSettingsHelper.ParseValues(def.Values);

                var notValues = values
                    .Where(value => availableValues.All(el => el.Key != value))
                    .ToList();

                if (notValues.Any())
                {
                    result.Add(
                        $"Для параметра <{def.Title}> указаны недопустимые значения <{string.Join('|', notValues)}>.");
                }
            }
            catch
            {
                result.Add(
                    $"Для параметра <{def.Title}> указаны недопустимые значения.");
            }

            return result;
        }

        private static IEnumerable<string> TestTreeSelectSettings(ReportSingleSettings def, ReportSingleSettings test)
        {
            var result = new List<string>();

            if (!ReportSettingsHelper.TryParseLongValues(test.Value, out var values))
            {
                result.Add($"Неверный формат значения параметра <{def.Title}>.");

                return result;
            }

            if (def.Required)
            {
                if ((test.AllEnable != true && !values.Any())
                        || (test.AllEnable == true && test.AllSelected != true && !values.Any()))
                {
                    result.Add(
                        $"Для параметра <{def.Title}> значение должно быть установленно.");
                }

                if (test.AllEnable == true
                    && test.AllSelected == true
                    && values.Any())
                {
                    result.Add(
                        $"Для параметра <{def.Title}> при установленном свойстве <Все> свойство <Value> не должно содержать элементов.");
                }
            }

            var defOptions = (TreeSelectSingleSettingsOptions)def.GetOptions<TreeSelectSingleSettingsOptions>();

            if (defOptions.SelectMode == SelectionMode.Single
                && values.Any() && values.Count != 1)
            {
                result.Add(
                    $"Для параметра <{def.Title}> свойство <Value> не должно содержать только 1 элемент.");
            }

            var availableValues = ReportSettingsHelper.ParseValues(def.Values);

            if (defOptions.SelectOnlyMode != TreeListElementSelection.Both)
            {
                availableValues = availableValues
                    .Where(el =>
                        el.IsNode == (defOptions.SelectOnlyMode == TreeListElementSelection.Nodes ? true : false))
                    .ToList();
            }

            var notValues = values
                .Where(value => availableValues.All(el => el.Key != value))
                .ToList();

            if (notValues.Any())
            {
                result.Add(
                    $"Для параметра <{def.Title}> указаны недопустимые значения <{string.Join('|', notValues)}>.");
            }

            return result;
        }

        private static IEnumerable<string> TestDiscreteSelectorSettings(ReportSingleSettings def, ReportSingleSettings test)
        {
            var result = new List<string>();

            if (!ReportSettingsHelper.TryParseLongValues(test.Value, out var values))
            {
                result.Add($"Не верный формат значения параметра <{def.Title}>.");

                return result;
            }

            var defOptions = (DiscreteSelectorSingleSettingsOptions)def.GetOptions<DiscreteSelectorSingleSettingsOptions>();

            if (def.Required
                || defOptions.SelectorType == DiscreteSelectorType.Combo
                || defOptions.SelectorType == DiscreteSelectorType.Radio)
            {
                if (!values.Any())
                {
                    result.Add(
                        $"Для параметра <{def.Title}> значение должно быть установленно.");
                }
            }

            if (!values.Any())
            {
                return result;
            }

            if ((defOptions.SelectorType == DiscreteSelectorType.Combo
                 || defOptions.SelectorType == DiscreteSelectorType.Radio)
                && values.Count != 1)
            {
                result.Add(
                    $"Для параметра <{def.Title}> свойство <Value> не должно содержать только 1 элемент.");
            }

            var availableValues = ReportSettingsHelper.ParseValues(def.Values);

            var notValues = values
                .Where(value => availableValues.All(el => el.Key != value))
                .ToList();

            if (notValues.Any())
            {
                result.Add(
                    $"Для параметра <{def.Title}> указаны недопустимые значения <{string.Join('|', notValues)}>.");
            }

            return result;
        }

        protected virtual async Task<AttachedFile> SaveFile(IServiceProvider servicesProvider, string fileName, Stream fileStream)
        {
            var fileManager = servicesProvider.GetRequiredService<AttachedFileManager>();

            var aFile = new AttachedFile
            {
                Name = fileName,
                Type = FileTypeName,
                IsPublic = true
            };

            return await fileManager.CreateDbAsync(aFile, fileStream);
        }
    }
}
