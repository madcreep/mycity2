using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace BIT.MyCity.Reports
{
    public interface IReport
    {
        string SubsystemUid { get; }

        Guid Id { get; }

        string Name { get; }

        string Description { get; }

        Task<ReportSettings> GetSettings(IServiceProvider servicesProvider);

        Task<ExecuteReportResult> Execute(IServiceProvider servicesProvider, ReportSettings settings);

        IEnumerable<string> TestSettings(IServiceProvider servicesProvider, ReportSettings defSettings,
            ReportSettings settings);
    }

    //public enum ParamType 
    //{
    //    Date,
    //    DateRange,
    //    String,
    //    Number,
    //    Boolean
    //}

    //public enum RangeTypes 
    //{
    //    None,
    //    Boundaries,
    //    Array,
    //    Both
    //}

    //public enum ResultType 
    //{
    //    JSON,
    //    Excel,
    //    Both
    //}

    //public class Parameter
    //{
    //    public Parameter(string name, ParamType paramType, RangeTypes rangeTypes, bool rangeOnly, bool required) 
    //    {
    //        Name = name;
    //        ParamType = paramType;
    //        RangeTypes = rangeTypes;
    //        RangeOnly = rangeOnly;
    //        Required = required;
    //    }
    //    public string Name { get; }
    //    public ParamType ParamType { get; }
    //    public RangeTypes RangeTypes { get; }

    //    public bool RangeOnly { get; }

    //    public bool Required { get; }

    //    public object Value { get; set; }
    //}
}
