using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;

namespace BIT.MyCity.Reports
{
    public class ReportSettingsBuilder
    {
        private readonly ReportSettings _settings = new ReportSettings();

        private readonly Dictionary<string, ReportSingleSettings> _parameters = new Dictionary<string, ReportSingleSettings>();

        public ReportSettings Build
        {
            get
            {
                _settings.Parameters = _parameters.Values.ToArray();

                return _settings;
            }
        }

        public ReportSettingsBuilder(Guid reportId)
        {
            _settings.ReportId = reportId;
        }

        #region Парамерры отчета
        public ReportSettingsBuilder AddDiscreteSelector(string title, string paramName,
            DiscreteSelectorType selectorType, [NotNull] IEnumerable<ReportSettingsValue> values,
            bool required = false,
            uint countElementsInRow = 1, uint elementWidth = 12,
            IEnumerable<int> selectedValues = null)
        {
            if (countElementsInRow < 1)
            {
                countElementsInRow = 1;
            }
            else if (countElementsInRow > 12)
            {
                countElementsInRow = 12;
            }

            if (elementWidth < 1)
            {
                elementWidth = 1;
            }
            else if (elementWidth > 12)
            {
                elementWidth = 12;
            }

            var testElementWith = 12 / countElementsInRow;

            if (testElementWith < elementWidth)
            {
                elementWidth = testElementWith;
            }

            var reportSingleSettings = new ReportSingleSettings
            {
                ControlType = ReportSettingsControlType.DiscreteSelector,
                Required = required,
                Name = paramName.Trim(),
                Title = title.Trim(),
                Value = selectedValues == null ? string.Empty : string.Join("|", selectedValues),
                Values = string.Join("|", values.Select(el => el.ToString()))
                
            };

            var options = new DiscreteSelectorSingleSettingsOptions
            {
                CountElementsInRow = countElementsInRow,
                ElementWidth = elementWidth,
                SelectorType = selectorType
            };

            reportSingleSettings.SetOptions(options);

            AddParameter(reportSingleSettings);

            return this;
        }

        public ReportSettingsBuilder AddNumberRange(string title, string paramName,
            bool required = false, uint decimalPlaces = 0,
            decimal? minValue = null, decimal? maxValue = null,
            decimal? valueFrom = null, decimal? valueTo = null,
            NumberRangeType numberRangeType = NumberRangeType.Dual)
        {
            if (numberRangeType == NumberRangeType.Single)
            {
                valueTo = null;
            }

            var reportSingleSettings = new ReportSingleSettings
            {
                ControlType = ReportSettingsControlType.NumberRange,
                Name = paramName.Trim(),
                Title = title.Trim(),
                Required = required,
                Value = new ReportSettingsNumberRange(valueFrom, valueTo).RangeString
                //Value = $"{(valueFrom.HasValue ? valueFrom.Value.ToString(CultureInfo.InvariantCulture) : "null")}:{(valueTo.HasValue ? valueTo.Value.ToString(CultureInfo.InvariantCulture) : "null")}"
            };

            var options = new NumberRangeSingleSettingsOptions
            {
                MinValue = minValue,
                MaxValue = maxValue,
                DecimalPlaces = decimalPlaces,
                RangeType = numberRangeType
            };

            reportSingleSettings.SetOptions(options);
            
            AddParameter(reportSingleSettings);
            
            return this;
        }

        public ReportSettingsBuilder AddDateRange(string title, string paramName,
            bool required = false, DateRangeControlMode? mode = null,
            int? plusN = null, DateTime? dateFrom = null, DateTime? dateTo = null,
            DateRangeControlMode[] availableDateRanges = null)
        {
            if (mode.HasValue && availableDateRanges != null && availableDateRanges.Any()
                && availableDateRanges.All(el => el != mode.Value))
            {
                throw new Exception(
                    $"В допустимых значениях диапазона дат отсутствует значение '{mode.Value}'");
            }

            var reportSingleSettings = new ReportSingleSettings
            {
                ControlType = ReportSettingsControlType.DateRange,
                Required = required,
                Name = paramName.Trim(),
                Title = title.Trim(),
                Value = new ReportSettingsDateRange(dateFrom, dateTo).RangeString
            };

            var options = new DateRangeSingleSettingsOptions
            {
                DateRangePlusN = plusN ?? 0,
                DateRangeMode = mode,
                AvailableDateRanges = availableDateRanges
            };

            reportSingleSettings.SetOptions(options);

            AddParameter(reportSingleSettings);

            return this;
        }

        public ReportSettingsBuilder AddSingleCheckbox(string title, string paramName, bool value = false)
        {
            AddParameter(new ReportSingleSettings
            {
                ControlType = ReportSettingsControlType.SingleCheckbox,
                Name = paramName.Trim(),
                Title = title.Trim(),
                Value = value ? "true" : "false"
            });

            return this;
        }

        public ReportSettingsBuilder AddTreeSelect(string title, string paramName,
            [NotNull]IEnumerable<ReportSettingsValue> values,
            bool required = false,
            TreeListElementSelection selectElements = TreeListElementSelection.Both,
            SelectionMode selectionMode = SelectionMode.Multi,
            IEnumerable<object> selectedValues = null,
            bool allEnable = false, bool allSelected = false)
        {
            var reportSingleSettings = new ReportSingleSettings
            {
                ControlType = ReportSettingsControlType.TreeSelect,
                Required = required,
                Name = paramName.Trim(),
                Title = title.Trim(),
                Value = string.Join("|", (selectedValues ?? Array.Empty<object>()).Select(el => el.ToString())),
                Values = string.Join("|", values.Select(el => el.ToString())),
                AllEnable = selectionMode != SelectionMode.Single && allEnable,
                AllSelected = selectionMode == SelectionMode.Single && allSelected
            };

            var options = new TreeSelectSingleSettingsOptions
            {
                SelectOnlyMode = selectElements,
                SelectMode = selectionMode
            };

            reportSingleSettings.SetOptions(options);

            AddParameter(reportSingleSettings);

            return this;
        }

        public ReportSettingsBuilder AddListSelect(string title, string paramName,
            [NotNull] IEnumerable<ReportSettingsValue> values,
            bool required = false,
            SelectionMode selectionMode = SelectionMode.Multi,
            IEnumerable<object> selectedValues = null,
            bool allEnable = false, bool allSelected = false)
        {
            var reportSingleSettings = new ReportSingleSettings
            {
                ControlType = ReportSettingsControlType.ListSelect,
                Required = required,
                Name = paramName.Trim(),
                Title = title.Trim(),
                Value = string.Join("|", (selectedValues ?? Array.Empty<object>()).Select(el => el.ToString())),
                Values = string.Join("|",
                    values.Select(el => el.ToString())),
                AllEnable = selectionMode != SelectionMode.Single && allEnable,
                AllSelected = selectionMode == SelectionMode.Single && allSelected
            };

            var options = new ListSelectSingleSettingsOptions
            {
                SelectMode = selectionMode
            };

            reportSingleSettings.SetOptions(options);

            AddParameter(reportSingleSettings);

            return this;
        }

        /// <summary>
        /// Добавляет описание для элемента управления "Обычный текст"
        /// </summary>
        /// <param name="title">Отображаемое название параметра</param>
        /// <param name="paramName">Имя параметра</param>
        /// <param name="required">Обязательный параметр</param>
        /// <param name="mode">Режим отображения (однострочный/многострочный). По умолчанию - однострочный</param>
        /// <param name="textLength">Длина текста. По умолчанию 'null' - без огрничений</param>
        /// <param name="textAreaRows">Высота текстового поля в строках при отображении 'многострочный'</param>
        /// <param name="value">Предустановленное значение</param>
        /// <returns>ReportSettingsBuilder</returns>
        public ReportSettingsBuilder AddCustomText(string title,
            string paramName, bool required = false,
            CustomTextMode mode = CustomTextMode.SingleRow, uint? textLength = null, uint textAreaRows = 3,
            string value = null)
        {
            var reportSingleSettings = new ReportSingleSettings
            {
                ControlType = ReportSettingsControlType.CustomText,
                Required = required,
                Name = paramName.Trim(),
                Title = title.Trim(),
                Value = value
            };

            var options = new CustomTextSingleSettingsOptions
            {
                TextMode = mode,
                TextLength = textLength,
                TextAreaRows = mode == CustomTextMode.SingleRow ? (uint?) null : textAreaRows
            };

            reportSingleSettings.SetOptions(options);

            AddParameter(reportSingleSettings);

            return this;
        }
        #endregion
        
        #region Private
        private void AddParameter(ReportSingleSettings parameter)
        {
            if (string.IsNullOrWhiteSpace(parameter.Name))
            {
                throw new ArgumentException($"Имя параметра не может быть пустым");
            }
            if (_parameters.ContainsKey(parameter.Name))
            {
                throw new ArgumentException($"Дублирующееся имя параметра <{parameter.Name}>");
            }

            _parameters.Add(parameter.Name, parameter);
        }
        #endregion
    }
}
