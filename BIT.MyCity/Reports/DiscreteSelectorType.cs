namespace BIT.MyCity.Reports
{
    public enum DiscreteSelectorType
    {
        Radio,
        Checkbox,
        Combo
    }
}
