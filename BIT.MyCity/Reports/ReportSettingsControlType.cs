namespace BIT.MyCity.Reports
{
    public enum ReportSettingsControlType
    {
        Undefined,
        DateRange,
        SingleCheckbox,
        ListSelect,
        TreeSelect,
        NumberRange,
        DiscreteSelector,
        CustomText
    }
}
