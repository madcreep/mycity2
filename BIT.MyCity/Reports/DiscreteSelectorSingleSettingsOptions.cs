namespace BIT.MyCity.Reports
{
    public class DiscreteSelectorSingleSettingsOptions
    {
        /// <summary>
        /// Количество элементов в строке
        /// </summary>
        public uint CountElementsInRow { get; set; }

        /// <summary>
        /// Размер элемента (1 - 12)
        /// Размер указывается в количестве занимаемых столбцов (как в Bootstrap)
        /// </summary>
        public uint ElementWidth { get; set; }

        public DiscreteSelectorType SelectorType { get; set; }
    }
}
