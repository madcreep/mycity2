namespace BIT.MyCity.Reports
{
    public class CustomTextSingleSettingsOptions
    {
        public CustomTextMode TextMode { get; set; }
        public uint? TextLength { get; set; }
        public uint? TextAreaRows { get; set; }
    }
}
