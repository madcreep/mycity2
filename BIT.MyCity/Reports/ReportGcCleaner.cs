using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Gc;
using log4net;

namespace BIT.MyCity.Reports
{
    public sealed class ReportGcCleaner : IGcCleaner
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ReportGcCleaner));

        public async Task ClearUnusedEntities()
        { }

        public IQueryable<long> UsingFilesIdsQuery(DocumentsContext ctx)
        {
            var lifeDate = DateTime.UtcNow.AddMinutes(-30);
            
            var usingFilesIdsQuery = ctx.AttachedFiles
                .Where(el => el.Type == BaseReport.FileTypeName
                             && el.CreatedAt >= lifeDate)
                .Select(el => el.Id);

            _log.Debug($"ReportGcCleaner : {string.Join(", ", usingFilesIdsQuery.ToArray()) }");

            return usingFilesIdsQuery;
        }
    }
}
