using System.Collections.Generic;
using System.Linq;

namespace BIT.MyCity.Reports
{
    public static class ReportSettingsHelper
    {
        public static bool TryParseLongValues(string str, out List<long> result)
        {
            result = new List<long>();
            
            if (string.IsNullOrWhiteSpace(str))
            {
                return true;
            }

            var items = str.Split('|')
                .Select(el=>el.Trim())
                .ToArray();
            
            foreach (var item in items)
            {
                if (!long.TryParse(item, out var itemValue))
                {
                    result = null;

                    return false;
                }

                result.Add(itemValue);
            }
            
            return true;
        }

        public static List<ReportSettingsValue> ParseValues(string str)
        {
            return str.Split('|')
                .Select(ReportSettingsValue.ParseSettingsString)
                .ToList();
        }
    }
}
