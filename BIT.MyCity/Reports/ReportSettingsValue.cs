using System.Runtime.InteropServices.ComTypes;

namespace BIT.MyCity.Reports
{
    public class ReportSettingsValue
    {
        public ReportSettingsValue()
        {}

        public ReportSettingsValue(long key, string name, long weight, string due, bool isNode)
        {
            Key = key;
            Name = name;
            Weight = weight;
            Due = due;
            IsNode = isNode;
        }

        public long Key { get; set; }

        public string Name { get; set; }

        public long Weight { get; set; }

        public string Due { get; set; }

        public bool IsNode { get; set; }

        public static ReportSettingsValue ParseSettingsString(string str)
        {
            var items = str.Split(':');

            return new ReportSettingsValue
            {
                Key = long.Parse(items[0]),
                Name = items[1],
                Weight = long.Parse(items[2]),
                Due = items[3],
                IsNode = bool.Parse(items[4])
            };
        }

        public override string ToString()
        {
            return $"{Key}:{MaskString(Name)}:{Weight}:{MaskString(Due ?? "0.")}:{IsNode}";
        }

        private static string MaskString(string str)
        {
            return str?
                .Trim()
                .Replace('|', '/')
                .Replace(':', ';');
        }
    }
}
