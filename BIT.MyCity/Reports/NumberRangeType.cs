using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BIT.MyCity.Reports
{
    /// <summary>
    /// Тип отображения элемента "Диапазон чисел"
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum NumberRangeType
    {
        /// <summary>
        /// Одно значение
        /// </summary>
        Single,
        /// <summary>
        /// Два значения
        /// </summary>
        Dual
    }
}
