using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BIT.MyCity.Reports
{
    /// <summary>
    /// Режим отображения элемента управления "Обычный текст"
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CustomTextMode
    {
        /// <summary>
        /// Однострочный
        /// </summary>
        SingleRow,
        /// <summary>
        /// Многострочный
        /// </summary>
        MultiRows
    }
}
