using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BIT.MyCity.Reports
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DateRangeControlMode
    {
        /// <summary>
        /// Все
        /// </summary>
        All,

        /// <summary>
        /// Диапазон
        /// </summary>
        Range,

        /// <summary>
        /// Один день
        /// </summary>
        SingleDay,

        /// <summary>
        /// Сегодня
        /// </summary>
        CurrentDay,

        /// <summary>
        /// Сегодня + N дней
        /// </summary>
        CurrentDayPlusN,

        /// <summary>
        /// N дней и ранее
        /// </summary>
        PlusNAndBefore,

        /// <summary>
        /// N дней и позднее
        /// </summary>
        PlusNAndLater,

        /// <summary>
        /// Текущая неделя
        /// </summary>
        CurrentWeek,

        /// <summary>
        /// Текущий месяц
        /// </summary>
        CurrentMonth,

        /// <summary>
        /// Текущий квартал
        /// </summary>
        CurrentQuarter,

        /// <summary>
        /// Текущий год
        /// </summary>
        CurrentYear,

        /// <summary>
        /// Предыдущая неделя
        /// </summary>
        PrevWeek,

        /// <summary>
        /// Предыдущий месяц
        /// </summary>
        PrevMonth,

        /// <summary>
        /// Предыдущий квартал
        /// </summary>
        PrevQuarter,

        /// <summary>
        /// Предыдущий год
        /// </summary>
        PrevYear,

        /// <summary>
        /// Последняя неделя
        /// </summary>
        LastWeek,

        /// <summary>
        /// Последний месяц
        /// </summary>
        LastMonth,

        /// <summary>
        /// Последний квартал
        /// </summary>
        LastQuarter,

        /// <summary>
        /// Последний год
        /// </summary>
        LastYear
    }
}
