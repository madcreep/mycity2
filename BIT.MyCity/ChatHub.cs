using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using BIT.MyCity.Database;

namespace BIT.MyCity
{
    public static class UserHandler
    {
        public static readonly HashSet<string> ConnectedIds = new HashSet<string>();
    }
    public class ChatHub : Hub
    {
        private DocumentsContext _context;
        public ChatHub(DocumentsContext context)
        {
            _context = context;
        }
        
        public override Task OnDisconnectedAsync(Exception exception)
        {
            if (UserHandler.ConnectedIds.Contains(Context.ConnectionId))
            {
                UserHandler.ConnectedIds.Remove(Context.ConnectionId);
            }
            return base.OnDisconnectedAsync(exception);
        }
        
        public async Task SendToAll(string email, string message)
        {
            string msgId = null;
            if (email != null && email != "admin")
            {
                var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
                var ip = Context.GetHttpContext().Connection.RemoteIpAddress.ToString();
                //var msg = _context.ChatMessages.Add(new ChatMessage {Value = message, Author = user, Source = ip, Created = DateTime.Now});
                await _context.SaveChangesAsync();
//                msgId = msg.Entity.Uuid.ToString();
                await Clients.Caller.SendAsync("id", email, message, msgId);
            }

            await Clients.All.SendAsync("sendToAll", email, message, msgId);
        }

        public async Task Reply(string email, string message, string parentId)
        {
//            if (email == GlobalProperties.InternalUserName && !UserHandler.ConnectedIds.Contains(Context.ConnectionId))
//            {
//                UserHandler.ConnectedIds.Add(Context.ConnectionId);
//            }
            var id = Guid.Parse(parentId); 
//            var parent = await _context.ChatMessages.FirstOrDefaultAsync(c => c.Uuid == id);
//            if (parent == null) return;
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Email == email);
            var ip = Context.GetHttpContext().Connection.RemoteIpAddress.ToString();
//            _context.ChatMessages.Add(new ChatMessage {Value = message, Author = user, Source = ip, Parent = parent, Created = DateTime.Now});
            await _context.SaveChangesAsync();
        }

        public Task Message(string email, string message)
        {
            if (message == "/hello")
                UserHandler.ConnectedIds.Add(Context.ConnectionId);
            return Task.CompletedTask;
        }
    } 
}