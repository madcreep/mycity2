﻿namespace BIT.MyCity
{
    public interface IUser
    {
        string Email { get; set; }
        string Password { get; set; }
    }
}
