using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Model;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Helpers
{
    public static class HierarchyHelper
    {
        private static string _base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string StartDue = "0.";

        public static async Task UpdateHierarchy(DocumentsContext ctx, IHierarchical dest, ParentInfo oldParent)
        {
            if (dest == null)
            {
                return;
            }

            var parent = await GetParentAsync(ctx, dest);
            
            if (dest.ParentId.HasValue)
            {
                switch (parent)
                {
                    case null:
                        throw new Exception("Не найдена родительская сущность.");
                    case IDeletable dParent when dParent.Deleted:
                        throw new Exception("Невозможно назначить родителем удаленный элемент");
                    case IHierarchical hParent when parent.GetType() == dest.GetType() && !string.IsNullOrWhiteSpace(dest.Due) && hParent.Due.StartsWith(dest.Due):
                        throw new Exception("Невозможно назначить родителя из собственной ветки");
                    
                }
            }

            var oldDue = dest.Due;

            dest.ParentId = parent?.Id;
            dest.ParentType = parent?.GetType().Name;
            dest.Due = CreateKey(dest, parent as IHierarchical);
            dest.Layer = (parent != null && parent.GetType() == dest.GetType() ? ((IHierarchical)parent).Layer : 0) + 1;

            if (parent != null && parent.GetType() == dest.GetType() && parent is IHierarchical hierarchyParent)
            {
                hierarchyParent.IsNode = true;
            }

            await ctx.SaveChangesAsync();

            if (!string.IsNullOrWhiteSpace(oldDue) && ctx.DbSet<IKey>(dest.GetType()) is IQueryable<IHierarchical> hierarchyDbSet)
            {
                var childArray = await hierarchyDbSet
                    .Where(el => el.Due.StartsWith(oldDue) && el.Due != oldDue)
                    .OrderBy(el => el.Due)
                    .ToArrayAsync();

                if (childArray.Any())
                {
                    RebaseHierarchy(childArray, dest);
                }
            }

            await ctx.SaveChangesAsync();

            if (oldParent != null && oldParent.ParentType == dest.GetType().Name)
            {
                await UpdateOldParent(ctx, oldParent);
            }
        }

        public static void RebaseHierarchy(IEnumerable<IHierarchical> rebaseArray, IKey parent = null)
        {
            var stack = new Stack<HrItem>();

            var parentItem = parent == null
                ? null
                : new HrItem(parent);

            HrItem prevItem = null;

            foreach (var item in rebaseArray.OrderBy(el => el.Due))
            {
                var hrItem = new HrItem(item);

                while (prevItem != null && !hrItem.OldDue.StartsWith(prevItem.OldDue))
                {
                    stack.Pop();

                    prevItem = stack.Count == 0 ? null : stack.Peek();
                }

                prevItem ??= parentItem;

                hrItem.BhItem.Due = CreateKey(
                    hrItem.BhItem,
                    prevItem != null && prevItem.ItemType == hrItem.BhItem.GetType()
                        ? prevItem.BhItem
                        : null);
                hrItem.BhItem.ParentId = prevItem?.Id;
                hrItem.BhItem.ParentType = prevItem?.ItemType.Name;
                hrItem.BhItem.IsNode = false;
                hrItem.BhItem.Layer = (prevItem?.BhItem?.Layer ?? 0) + 1;

                if (prevItem?.BhItem != null && prevItem.ItemType == hrItem.BhItem.GetType())
                {
                    prevItem.BhItem.IsNode = true;
                }

                if (!(hrItem.BhItem is IDeletable {Deleted: true}))
                {
                    stack.Push(hrItem);

                    prevItem = hrItem;
                }
            }
        }

        public static async Task<IEnumerable<T>> GetChildAsync<T>(DocumentsContext ctx, IKey parent, Type childType = null, bool oneLevel = true)
            where T: IHasParent
        {
            if (parent == null && childType == null)
            {
                return new T[0];
            }

            var cType  = childType ?? parent.GetType();
            
            var parentTypeName = parent?.GetType().Name;

            if ((childType == null || childType == parent?.GetType()) && parent is IHierarchical hParent)
            {
                var dbSet = ctx.DbSet<IHierarchical>(cType);

                if (dbSet == null)
                {
                    return new T[0];
                }

                return await dbSet
                    .Where(el => el.Due.StartsWith(hParent.Due) && ((oneLevel && el.Layer > hParent.Layer) || (!oneLevel && el.Layer == hParent.Layer + 1)))
                    .Cast<T>()
                    .ToArrayAsync();
            }
            
            {
                var dbSet = ctx.DbSet<T>(cType);

                if (dbSet == null)
                {
                    return new T[0];
                }

                var parentId = parent?.Id;

                var firstLevel = await dbSet
                    .Where(el =>
                        el.ParentType == parentTypeName && el.ParentId.Value == parentId)
                    .ToArrayAsync();

                if (oneLevel || cType.GetInterfaces().All(el => el != typeof(IHierarchical)))
                {
                    return firstLevel;
                }
                
                {
                    IQueryable<IHierarchical> request = null;

                    var dbSetHierarchical = ctx.DbSet<IHierarchical>(cType);

                    if (dbSetHierarchical == null)
                    {
                        return new T[0];
                    }

                    foreach (var item in firstLevel)
                    {
                        if (!(item is IHierarchical hItem))
                        {
                            continue;
                        }

                        if (request == null)
                        {
                            request = dbSetHierarchical.Where(el =>
                                el.Due.StartsWith(hItem.Due) && el.Layer > hItem.Layer);
                        }
                        else
                        {
                            request = request
                                .Union(dbSetHierarchical.Where(el =>
                                    el.Due.StartsWith(hItem.Due) && el.Layer > hItem.Layer));
                        }
                    }

                    if (request == null)
                    {
                        return firstLevel;
                    }

                    var otherLevels = await request
                        .Cast<T>()
                        .ToArrayAsync();

                    var result = firstLevel
                        .Union(otherLevels)
                        .ToArray();

                    return result;
                }

            }
        }

        public static async Task<IKey> GetParentAsync(DocumentsContext ctx, IHasParent child)
        {
            if (child == null || string.IsNullOrWhiteSpace(child.ParentType) || !child.ParentId.HasValue )
            {
                return null;
            }
            
            var dbSet = ctx.DbSet<IKey>(child.ParentType);

            if (dbSet == null)
            {
                return null;
            }
            
            return await dbSet
                .Where(el => el.Id == child.ParentId.Value)
                .SingleOrDefaultAsync();
        }

        public static async Task UpdateDeleted(DocumentsContext ctx, IHierarchical dest)
        {
            if (!(dest is IDeletable dDest))
            {
                return;
            }

            var parent = await GetParentAsync(ctx, dest);

            if (dest is IDeletable { Deleted: true })
            {
                dest.IsNode = false;

                var parenLastWeight = await WeightHelper.GetLastWeightAsync(ctx,
                    dest.GetType(),
                    parent != null && parent.GetType() == dest.GetType() ? parent.Id : (long?)null);

                var child = await GetChildAsync<IHasParent>(ctx, dest);

                foreach (var item in child)
                {
                    if (item is ISortable sItem)
                    {
                        sItem.Weight = ++parenLastWeight;
                    }
                }

                var rebaseChild =  await GetChildAsync<IHierarchical>(ctx, parent, dest.GetType(), false);

                RebaseHierarchy(rebaseChild, parent);
            }

            //var items = await dbSet
            //    .Where(el => el.Due.StartsWith(dest.Due) && (el.Id == dest.Id))
            //    .ToArrayAsync();

            //foreach (var item in items)
            //{
            //    if (!(item is IDeletable dItem))
            //    {
            //        continue;
            //    }

            //    dItem.Deleted = dDest.Deleted;
            //    dItem.DeleteTimestamp = dDest.DeleteTimestamp;
            //}

            await ctx.SaveChangesAsync();
        }

        public static async Task UpdateDeleted(DocumentsContext ctx, IHasParent dest, bool deleted)
        {
            var nowTs = DateTime.UtcNow; 

            var dbSet = ctx.DbSet<IKey>(dest.ParentType);

            if (dbSet == null)
            {
                return;
            }

            var items = await dbSet
                .Where(el => el.Id == dest.ParentId)
                .ToArrayAsync();

            foreach (var item in items)
            {
                if (!(item is IDeletable dItem))
                {
                    continue;
                }

                dItem.Deleted = deleted;
                dItem.DeleteTimestamp = deleted ? nowTs : (DateTime?) null;

                if (item is IHierarchical hItem)
                {
                    await UpdateDeleted(ctx, hItem);
                }
            }

            await ctx.SaveChangesAsync();
        }

        private static async Task UpdateOldParent(DocumentsContext ctx, IHasParent oldParent)
        {
            var dbSet = ctx.DbSet<IHierarchical>(oldParent.ParentType);

            if (dbSet == null)
            {
                return;
            }

            var entity = await dbSet
                .Where(el => el.Id == oldParent.ParentId)
                .FirstOrDefaultAsync();

            if (entity == null)
            {
                return;
            }

            entity.IsNode = await dbSet.Where(el => el.ParentId == entity.Id).AnyAsync();

            await ctx.SaveChangesAsync();
        }

        private static string CreateKey(IKey dest, IHierarchical parent)
        {
            if (dest == null)
            {
                return null;
            }

            if (parent != null && dest.GetType() == parent.GetType() && string.IsNullOrWhiteSpace(parent.Due))
            {
                throw new Exception("У родительского элемнта обязательно должен быть ключ дерева.");
            }

            var due = IndexToDue(dest.Id);

            var parentDue = parent != null && parent.GetType() == dest.GetType()
                ? parent.Due
                : StartDue;

            return string.Concat(parentDue, due);
        }

        private static string IndexToDue(long index)
        {
            var due = string.Empty;
            while (index > 0)
            {
                due += _base[(int)(index % _base.Length)];
                index /= _base.Length;
            }

            return string.IsNullOrEmpty(due)
                ? StartDue
                : $"{due}.";
        }

        private class HrItem
        {
            public HrItem(IKey bhItem)
            {
                ItemType = bhItem.GetType();
                Id = bhItem.Id;
                BhItem = bhItem as IHierarchical;
                OldDue = BhItem?.Due;

            }

            public Type ItemType { get; }

            public long Id { get; }

            public IHierarchical BhItem { get; }

            public string OldDue { get; }
        }
    }
}
