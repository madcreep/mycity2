using System;
using System.Collections.Generic;
using System.Linq;
using BIT.MyCity.Attributes;

namespace BIT.MyCity.Helpers
{
    public static class EnumHelper
    {
        public static string? MapName(this Enum? enumVal)
        {
            if (enumVal == null)
            {
                return null;
            }

            var attribute = enumVal
                .GetAttributeOfType<MapNameEnumAttribute>()
                .FirstOrDefault();

            return attribute?.Name
                   ?? enumVal.ToString();
        }

        public static bool TryParse<TEnum>(string? value, out TEnum result) where TEnum : struct
        {
            if (System.Enum.TryParse(value, out result))
            {
                return true;
            }

            var values = System.Enum.GetValues(typeof(TEnum))
                .Cast<TEnum>();

            foreach (var item in values)
            {
                var name = MapName(item as System.Enum);

                if (!(name?.Equals(value, StringComparison.CurrentCultureIgnoreCase) ?? false))
                {
                    continue;
                }

                result = item;

                return true;
            }

            return false;
        }

        public static bool TryParse(string? value, Type type, out object? result)
        {
            if (System.Enum.TryParse(type, value, out result))
            {
                return true;
            }

            var values = System.Enum.GetValues(type);

            foreach (var item in values)
            {
                var name = MapName(item as System.Enum);

                if (!(name?.Equals(value, StringComparison.CurrentCultureIgnoreCase) ?? false))
                {
                    continue;
                }

                result = item;

                return true;
            }

            return false;
        }

        private static IEnumerable<T> GetAttributeOfType<T>(this System.Enum enumVal) where T : Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return attributes
                .Cast<T>();
        }
    }
}
