using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Helpers
{
    public static class WeightHelper
    {
        public static async Task SetNewWeightAsync(DocumentsContext ctx, ISortable sortObj, long? oldWeight = null)
        {
            if (sortObj.Weight <= 0)
            {
                var lastWeight = await GetLastWeightAsync(ctx, sortObj.GetType(), sortObj.Id);

                sortObj.Weight = ++lastWeight;

                await ctx.SaveChangesAsync();

                return;
            }

            await RebaseWeight(ctx, sortObj, oldWeight);
        }

        private static async Task RebaseWeight(DocumentsContext ctx, ISortable sortObj, long? oldWeight)
        {
            if (sortObj == null || (oldWeight.HasValue && sortObj.Weight == oldWeight.Value))
            {
                return;
            }

            long startWeight;
            long? endWeight;

            if (oldWeight.HasValue)
            {
                startWeight = sortObj.Weight > oldWeight ? oldWeight.Value : sortObj.Weight;
                endWeight = sortObj.Weight > oldWeight ? sortObj.Weight : oldWeight.Value;
            }
            else
            {
                startWeight = sortObj.Weight;
                endWeight = null;
            }

            var sortArray = sortObj is IHierarchical hObj
                ? await GetSortArray(ctx, hObj, startWeight, endWeight)
                : await GetSortArray(ctx, sortObj, startWeight, endWeight);
            
            var index = !oldWeight.HasValue || oldWeight.Value > sortObj.Weight ? 1 : 0;

            foreach (var item in sortArray)
            {
                item.Weight = startWeight + index++;
            }

            if (oldWeight.HasValue && oldWeight.Value < sortObj.Weight)
            {
                sortObj.Weight = startWeight + index;
            }

            await ctx.SaveChangesAsync();
        }

        private static async Task<ISortable[]> GetSortArray(DocumentsContext ctx, IKey sortObj, long startWeight, long? endWeight)
        {
            var dbSet = ctx.DbSet<ISortable>(sortObj.GetType());

            if (dbSet == null)
            {
                throw new Exception($"Не удалось найти DbSet для типа <{sortObj.GetType().Name}>");
            }

            var sortArray = await dbSet
                .Where(el =>
                    el.Weight >= startWeight && (endWeight == null || el.Weight <= endWeight) && el.Id != sortObj.Id)
                .OrderBy(el => el.Weight)
                .ToArrayAsync();

            return sortArray;
        }

        private static async Task<ISortable[]> GetSortArray(DocumentsContext ctx, IHierarchical sortObj, long startWeight, long? endWeight)
        {
            var dbSet = ctx.DbSet<IHierarchical>(sortObj.GetType());

            if (dbSet == null)
            {
                throw new Exception($"Не удалось найти DbSet для типа <{sortObj.GetType().Name}>");
            }

            ISortable[] sortArray = await dbSet
                .Where(el =>
                    el.Weight >= startWeight && (endWeight == null || el.Weight <= endWeight) && el.Id != sortObj.Id &&
                    el.ParentId == sortObj.ParentId && el.ParentType == sortObj.ParentType)
                .OrderBy(el => el.Weight)
                .ToArrayAsync();

            return sortArray;
        }

        public static async Task<long> GetLastWeightAsync(DocumentsContext ctx, Type entityType, long? id = null)
        {
            var dbSet = ctx.DbSet<IKey>(entityType);

            if (dbSet == null)
            {
                throw new Exception($"Не удалось найти DbSet для типа <{entityType.Name}>");
            }

            IKey obj = null;

            if (id.HasValue)
            {
                obj = await dbSet
                    .Where(el => el.Id == id.Value)
                    .SingleOrDefaultAsync();
            }

            if (entityType.GetInterfaces().Contains(typeof(IHierarchical)))
            {
                return await GetLastWeight(ctx, entityType, (obj as IHierarchical)?.ParentId, (obj as IHierarchical)?.ParentType);
            }

            if (entityType.GetInterfaces().Contains(typeof(ISortable)))
            {
                return await GetLastWeight(ctx, entityType);
            }
            
            throw new Exception($"Тип <{entityType.FullName}> не реализует интерфейс {nameof(ISortable)}");
        }

        private static async Task<long> GetLastWeight(DocumentsContext ctx, Type objType)
        {
            var dbSet = ctx.DbSet<ISortable>(objType);

            if (dbSet == null)
            {
                throw new Exception($"Не удалось найти DbSet для типа <{objType.Name}>");
            }

            var result = await dbSet.MaxAsync(el => (long?)el.Weight);

            return result ?? 0;
        }

        private static async Task<long> GetLastWeight(DocumentsContext ctx, Type objType, long? parentId, string parentType)
        {
            var dbSet = ctx.DbSet<IHierarchical>(objType);

            if (dbSet == null)
            {
                throw new Exception($"Не удалось найти DbSet для типа <{objType.Name}>");
            }

            var result = await dbSet
                .Where(el => el.ParentId == parentId && (!parentId.HasValue || el.ParentType == parentType))
                .MaxAsync(el => (long?) el.Weight);

            return result ?? 0;
        }
    }
}
