using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Sms.Models;

namespace BIT.MyCity.Sms
{
    public class TestSmsHelper : ISmsHelper
    {
        private const string LoginTest = "test";
        private const string SmsId = "11111111";

        private readonly string _login;
        private readonly string _password;
        private readonly string _sender;
        public string Name => "Тестовый";
        public Balance Balance => IsConfigured ? new Balance("RUB", 1234.56M, 100) : new Balance("Сервис не настроен", true);

        public TestSmsHelper(string login, string password, string sender)
        {
            _login = login;
            _password = password;
            _sender = sender;
        }

        public async Task<Balance> GetBalanceAsync()
        {
            return Balance;
        }

        public async Task<SmsStatus> SendAsync(SmsMessage message)
        {
            return new SmsStatus(SmsId, IsConfigured ? SmsMessageStatus.Accepted : SmsMessageStatus.Error);
        }

        public async Task<IEnumerable<SmsStatus>> GetStatusAsync(params string[] smsId)
        {
            var result = new List<SmsStatus>();

            if (smsId == null || !smsId.Any())
            {
                return result;
            }

            result.AddRange(
                smsId
                    .Select(item => new SmsStatus(item,
                        item == SmsId ? SmsMessageStatus.Delivered : SmsMessageStatus.FatalError))
            );

            return result;
        }

        private bool IsConfigured => _login == LoginTest && _password == LoginTest && _sender == LoginTest;
    }
}
