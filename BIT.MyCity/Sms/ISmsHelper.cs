using System.Collections.Generic;
using System.Threading.Tasks;
using BIT.MyCity.Sms.Models;

namespace BIT.MyCity.Sms
{ 
    interface ISmsHelper
    {
        string Name { get; }

        Balance Balance { get; }

        Task<Balance> GetBalanceAsync();

        Task<SmsStatus> SendAsync(SmsMessage message);

        Task<IEnumerable<SmsStatus>> GetStatusAsync(params string[] smsId);
    }
}
