using System;

namespace BIT.MyCity.Sms.Models
{
    public class SmsMessage
    {
        /// <summary>
        /// Номер телефона получателя
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Дата отложенной отправки 
        /// </summary>
        public DateTime? ScheduleTime { get; set; }

        /// <summary>
        /// Подпись отправителя
        /// </summary>
        public string? Sender { get; set; }
    }
}
