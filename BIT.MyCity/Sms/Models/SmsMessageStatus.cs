using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BIT.MyCity.Sms.Models
{
    public enum SmsMessageStatus
    {
        /// <summary>
        /// Создано
        /// </summary>
        Created = 0,
        /// <summary>
        /// Принято сервисом
        /// </summary>
        Accepted = 1,
        /// <summary>
        /// Находится в очереди
        /// </summary>
        Queued = 2,
        /// <summary>
        /// Доставлено
        /// </summary>
        Delivered = 3,
        /// <summary>
        /// Ошибка (возможна повторная отправка)
        /// </summary>
        Error = -1,
        /// <summary>
        /// Ошибка сообщения не требующая повторных
        /// попыток отправки
        /// </summary>
        FatalError = -2,
        /// <summary>
        /// Ошибка недостатка баланса
        /// </summary>
        BalanceError = -3
    }
}
