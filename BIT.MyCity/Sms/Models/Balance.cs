namespace BIT.MyCity.Sms.Models
{
    public class Balance
    {
        public bool Success { get; }

        public string Currency { get; }

        public decimal? Amount { get; }

        public uint? SmsCount { get; }

        public string ErrorMessage { get; }

        public bool IsUnauthorized { get; }

        private Balance(bool success, string currency, decimal? amount, uint? smsCount, string errorMessage, bool isUnauthorized = false)
        {
            Success = success;
            Currency = currency;
            Amount = amount;
            SmsCount = smsCount;
            ErrorMessage = errorMessage;
            IsUnauthorized = isUnauthorized;
        }

        public Balance(string currency, decimal? amount, uint? smsCount)
            : this(true, currency, amount, smsCount, null)
        {
        }

        public Balance(string errorMessage, bool isUnauthorized = false)
            : this(false, null, null, null, errorMessage, isUnauthorized)
        {
        }
    }
}
