using System;

namespace BIT.MyCity.Sms.Models
{
    public class SmsStatus
    {
        public string SmsId { get; }

        public SmsMessageStatus Status { get; }

        public string ErrorMessage { get; }

        public SmsStatus(string smsId, SmsMessageStatus status, string errorMessage)
        {
            if (status == SmsMessageStatus.Error && string.IsNullOrWhiteSpace(errorMessage))
            {
                throw new ArgumentNullException(nameof(errorMessage));
            }

            SmsId = smsId;

            Status = status;

            ErrorMessage = errorMessage;
        }

        public SmsStatus(string smsId, SmsMessageStatus status)
            : this(smsId, status, null)
        {
        }

        //public SmsStatus(string smsId, string errorMessage)
        //    : this(smsId, SmsMessageStatus.Error, errorMessage)
        //{
        //}

        //public SmsStatus(string errorMessage)
        //    : this(null, SmsMessageStatus.Error, errorMessage)
        //{

        //}
    }
}
