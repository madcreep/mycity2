using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using BIT.MyCity.Sms;
using BIT.MyCity.Sms.Models;
using log4net;
using Microsoft.AspNetCore.WebUtilities;

namespace BIT.MyCity.SMSHelpers
{
    public class ExpectoHelper : ISmsHelper
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ExpectoHelper));

        private const string SendMessageUrl = "https://apisms.expecto.me/messages/v2/send/";
        private const string GetStatusUrl = "https://apisms.expecto.me/messages/v2/status/";
        private const string GetBalanceUrl = "https://apisms.expecto.me/messages/v2/balance";


        private readonly string _login;
        private readonly string _password;
        private readonly string _sender;

        public ExpectoHelper(string login, string password, string sender)
        {
            _login = login;
            _password = password;
            _sender = sender;
        }


        public string Name => "ЭКСПЕКТО";

        public Balance Balance 
        {
            get
            {
                var task = GetBalanceAsync();

                task.Wait();

                return task.Result;
            }
        }

        public async Task<SmsStatus> SendAsync(SmsMessage message)
        {
            var queryParams = new Dictionary<string, string>
            {
                {"phone", message.PhoneNumber },
                {"text", message.Message },
                {"sender", string.IsNullOrWhiteSpace(message.Sender) ? _sender : message.Sender.Trim() }
            };

            if (message.ScheduleTime.HasValue)
            {
                queryParams.Add("scheduleTime", message.ScheduleTime.Value.ToString("s"));
            }

            var baseUrl = string.Format(SendMessageUrl, _login, _password);

            var url = QueryHelpers.AddQueryString(baseUrl, queryParams);

            Console.WriteLine("URL: {0}", url);

            var answer = await SendRequestAsync(url);

            Console.WriteLine("Success: {0}; ResponseString: {1}; Error: {2}",
                answer.Success, answer.ResponseString, answer.Error);

            return answer.Success
                ? GetSendSmsStatus(answer.ResponseString)
                : new SmsStatus(null, SmsMessageStatus.Error, answer.Error);
        }

        public async Task<IEnumerable<SmsStatus>> GetStatusAsync(params string[] smsId)
        {
            if (smsId == null)
            {
                return new List<SmsStatus>();
            }

            var idList = smsId
                .Where(el => !string.IsNullOrWhiteSpace(el))
                .ToArray();

            if (!idList.Any())
            {
                return new List<SmsStatus>();
            }

            var parameters = $"?id={string.Join("&id=", idList)}";

            var url = $"{GetStatusUrl}{parameters}";

            Console.WriteLine("URL: {0}", url);

            var answer = await SendRequestAsync(url);

            Console.WriteLine("Success: {0}; ResponseString: {1}; Error: {2}; StatusCode: {3}",
                answer.Success, answer.ResponseString, answer.Error, answer.StatusCode.ToString());

            return GetSmsStatuses(answer);
        }

        private IEnumerable<SmsStatus> GetSmsStatuses(SendAnswer answer)
        {
            try
            {
                if (!answer.Success)
                {
                    return null;
                }

                var strings = answer.ResponseString.Trim(' ', '\n', '\r')
                    .Split('\n')
                    .Select(el => el.Trim(' ', '\n', '\r'))
                    .Where(el => !string.IsNullOrWhiteSpace(el))
                    .Select(el => el.Split(';'))
                    .ToDictionary(el => el[0], el => el[1]);

                var result = new List<SmsStatus>();

                foreach (var (smsId, status) in strings)
                {
                    result.Add(GetSmsStatus(smsId, status));
                }

                return result;
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка парсинга статусов сообщений ({answer.ResponseString})", ex);

                return null;
            }
        }

        private async Task<SendAnswer> SendRequestAsync(string url)
        {
            var result = new SendAnswer();

            var client = new HttpClient();

            var token = EncodeBase64($"{_login}:{_password}");

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);

            try
            {
                var response = await client.GetAsync(url);

                result.StatusCode = response.StatusCode;

                if (response.IsSuccessStatusCode)
                {
                    result.ResponseString = await response.Content.ReadAsStringAsync();

                    return result;
                }

                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка выполнения запроса (url = {url})");

                result.Error = ex.ToString();
            }

            return result;
        }

        public async Task<Balance> GetBalanceAsync()
        {
            var answer = await SendRequestAsync(GetBalanceUrl);

            //Console.WriteLine("Success: {0}; ResponseString: {1}; Error: {2}; StatusCode: {3}",
            //    answer.Success, answer.ResponseString, answer.Error, answer.StatusCode);

            if (!answer.Success)
            {
                return new Balance(answer.Error, answer.StatusCode == HttpStatusCode.Unauthorized);
            }

            var parts = answer.ResponseString
                .Split(';')
                .Select(el => el.Trim())
                .ToArray();

            if (parts.Length != 3)
            {
                return new Balance($"Не верный формат ответа сервиса ({answer.ResponseString})");
            }

            if (!decimal.TryParse(parts[1], NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out var amount))
            {
                return new Balance($"Ошибка парсинга значения 'amount' ({answer.ResponseString})");
            }

            if (!uint.TryParse(parts[2], out var smsCount))
            {
                return new Balance($"Ошибка парсинга значения 'smsCount' ({answer.ResponseString})");
            }

            return new Balance(parts[0], amount, smsCount);
        }

        private class SendAnswer
        {
            public string ResponseString { get; set; }

            public HttpStatusCode? StatusCode { get; set; }

            public string Error { get; set; }

            public bool Success => string.IsNullOrWhiteSpace(Error);
        }

        private static SmsStatus GetSendSmsStatus(string responceStr)
        {
            if (string.IsNullOrWhiteSpace(responceStr))
            {
                return new SmsStatus(null, SmsMessageStatus.Error, "Получен пустой ответ сервиса");
            }

            var answerParts = responceStr.Split(';');

            var testStatusStr = answerParts[0].Trim().ToLower();

            if (string.IsNullOrWhiteSpace(testStatusStr))
            {
                return new SmsStatus(null, SmsMessageStatus.Error, "В ответе сервиса отсутствует статус обработки сообщения");
            }

            if (testStatusStr == "accepted")
            {
                if (answerParts.Length != 2)
                {
                    return new SmsStatus(null, SmsMessageStatus.FatalError, "В ответе сервиса отсутствует идентификатор сообщения");
                }

                return new SmsStatus(answerParts[1].Trim(), SmsMessageStatus.Accepted);
            }

            string error;

            testStatusStr = answerParts[1].Trim().ToLower();

            SmsMessageStatus status;

            switch (testStatusStr)
            {
                case "invalid mobile phone":
                    error = "Неверно задан номер телефона";
                    status = SmsMessageStatus.FatalError;
                    break;
                case "text is empty":
                    error = "Отсутствует текст сообщения";
                    status = SmsMessageStatus.FatalError;
                    break;
                case "sender address invalid":
                    error = "Неверная (незарегистрированная) подпись отправителя";
                    status = SmsMessageStatus.FatalError;
                    break;
                case "wapurl invalid":
                    error = "Неправильный формат wap-push ссылки";
                    status = SmsMessageStatus.FatalError;
                    break;
                case "invalid schedule time format":
                    error = "Неверный формат даты отложенной отправки сообщения";
                    status = SmsMessageStatus.FatalError;
                    break;
                case "invalid status queue name":
                    error = "Неверное название очереди статусов сообщений";
                    status = SmsMessageStatus.FatalError;
                    break;
                case "not enough balance":
                    error = "Баланс пуст (проверьте баланс)";
                    status = SmsMessageStatus.BalanceError;
                    break;
                default:
                    error = $"Получен неизвестный статус обработки сообщения <{testStatusStr}>";
                    status = SmsMessageStatus.FatalError;
                    break;
            }

            return new SmsStatus(answerParts[1].Trim(), status, error);
        }

        private static SmsStatus GetSmsStatus(string smsId, string smsStatus)
        {
            SmsMessageStatus status;

            string errorMessage = null;

            switch (smsStatus.ToLower())
            {
                case "queued":
                    status = SmsMessageStatus.Queued;
                    break;
                case "delivered":
                    status = SmsMessageStatus.Delivered;
                    break;
                case "smsc submit":
                    status = SmsMessageStatus.Queued;
                    break;
                case "delivery error":
                    status = SmsMessageStatus.FatalError;
                    errorMessage = "Ошибка доставки SMS (абонент в течение времени доставки находился вне зоны действия сети или номер абонента заблокирован)";
                    break;
                case "smsc reject":
                    status = SmsMessageStatus.FatalError;
                    errorMessage = "Сообщение отвергнуто SMSC (номер заблокирован или не существует)";
                    break;
                case "incorrect id":
                    status = SmsMessageStatus.FatalError;
                    errorMessage = "Неверный идентификатор сообщения";
                    break;
                default:
                    status = SmsMessageStatus.FatalError;
                    errorMessage = "Неизвестный статус";
                    break;
            }

            return new SmsStatus(smsId, status, errorMessage);
        }

        private static string EncodeBase64(string str)
        {
            var encodedBytes = System.Text.Encoding.UTF8.GetBytes(str);

            var encodedTxt = Convert.ToBase64String(encodedBytes);

            return encodedTxt;
        }
    }
}
