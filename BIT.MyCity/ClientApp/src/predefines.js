var debug_text
process.argv.forEach(function (val, index, array) {
  // console.log(index + ': ' + val);
  if (val.toUpperCase() === 'BUILD') {
  	debug_text = 'false'
  	print_d();
  } else if (val.toUpperCase() === 'DEBUG') {
  	debug_text = 'true'
	print_d();
  }
});

function print_d() {
	// console.log('export const DEBUGANGULAR = ' + debug_text + ';');
	console.log("export const APP_BUILD_DATE = \'" + dateToStringValue(new Date(), true) + "';");


    // var fs = require('fs'),
    // path = require('path'),
    // filePath = path.join(__dirname, 'start.html');
    // filePath = './release.gen'

    // fs.readFile(filePath, {encoding: 'utf-8'}, function(err,data){
    // if (!err) {
    //     console.log("export const APP_BUILD_NUMBER = \'" + data.trim() + "';");
    //     // console.log("export const APP_BUILD_NUMBER1 = \'" + environment.appVersion + "';");
    // } else {
    //     console.log(err);
    // }
    // });

	// console.log("export const APP_BUILD_NUMBER = \'" + num1 + "';");
}

function digitpad(n) {
  return n < 10 ? '0' + n : '' + n;
}


 function dateToStringValue(date, joinTime) {
        if (typeof date === 'string') {
            // [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.
            date = new Date(date);
       }

     // return date.toLocaleString('ru-RU', { timeZone: 'Europe/Moscow', year: 'numeric', month: '2-digit', day: '2-digit', hourCycle: "h24", hour: '2-digit', minute: '2-digit', second: '2-digit'});

     // const offset = date.getTimezoneOffset()
     // date = new Date(date.getTime() - (offset*60*1000))

        if (date instanceof Date && !isNaN(date.getTime())) {
            let res = [
              date.getFullYear(),
              digitpad(date.getMonth() + 1),
              digitpad(date.getDate()),
              ].join('.');
            if (joinTime) {
                res += ' ' + [
                    digitpad(date.getHours()),
                    digitpad(date.getMinutes()),
                    digitpad(date.getSeconds())].join(':');
            }
            return res
        } else {
            return null;
        }
    }
