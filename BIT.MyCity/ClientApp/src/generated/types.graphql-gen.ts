import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
import * as ApolloCore from '@apollo/client/core';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  BigInt: any;
  Byte: any;
  /**
   * The `Date` scalar type represents a year, month and day in accordance with the
   * [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.
   */
  Date: any;
  /**
   * The `DateTime` scalar type represents a date and time. `DateTime` expects
   * timestamps to be formatted in accordance with the
   * [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.
   */
  DateTime: any;
  /**
   * The `DateTimeOffset` scalar type represents a date, time and offset from UTC.
   * `DateTimeOffset` expects timestamps to be formatted in accordance with the
   * [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.
   */
  DateTimeOffset: any;
  Decimal: any;
  Guid: any;
  Long: any;
  /** The `Milliseconds` scalar type represents a period of time represented as the total number of milliseconds. */
  Milliseconds: any;
  SByte: any;
  /** The `Seconds` scalar type represents a period of time represented as the total number of seconds. */
  Seconds: any;
  Short: any;
  UInt: any;
  ULong: any;
  UShort: any;
  Uri: any;
};



export enum IAddrType {
  Actual = 'ACTUAL',
  Legal = 'LEGAL',
  Registration = 'REGISTRATION',
  Residential = 'RESIDENTIAL',
  Unkown = 'UNKOWN'
}

export interface IAddressType {
  __typename?: 'AddressType';
  /** Доп. территория */
  additionArea?: Maybe<Scalars['String']>;
  /** Улица на доп. территории */
  additionAreaStreet?: Maybe<Scalars['String']>;
  /** Тип адреса */
  addrType?: Maybe<IAddrType>;
  /** Район */
  area?: Maybe<Scalars['String']>;
  /** Строение */
  building?: Maybe<Scalars['String']>;
  /** Город */
  city?: Maybe<Scalars['String']>;
  /** Идентификатор страны */
  countryId?: Maybe<Scalars['String']>;
  /** Внутригородской район */
  district?: Maybe<Scalars['String']>;
  /** Код КЛАДР */
  fiasCode?: Maybe<Scalars['String']>;
  /** Квартира */
  flat?: Maybe<Scalars['String']>;
  /** Корпус */
  frame?: Maybe<Scalars['String']>;
  /** Строка полного адреса */
  fullAddress?: Maybe<Scalars['String']>;
  /** Дом */
  house?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id: Scalars['Long'];
  /** Строка адреса (не включая дом, строение, корпус, номер квартиры) */
  partialAddress?: Maybe<Scalars['String']>;
  /** Регион */
  region?: Maybe<Scalars['String']>;
  /** Поселение */
  settlement?: Maybe<Scalars['String']>;
  /** Улица */
  street?: Maybe<Scalars['String']>;
  /** Индекс */
  zipCode?: Maybe<Scalars['String']>;
}

export enum IAgencyType {
  Fed = 'FED',
  Fnd = 'FND',
  Gov = 'GOV',
  Lcl = 'LCL',
  Mcl = 'MCL',
  Reg = 'REG',
  Unknown = 'UNKNOWN'
}

export interface IApiInformation {
  __typename?: 'ApiInformation';
  /** ASPNETCORE_ENVIRONMENT */
  aSPNETCORE_ENVIRONMENT?: Maybe<Scalars['String']>;
  /** Дата сборки */
  appBuildTime?: Maybe<Scalars['DateTime']>;
  /** Конфигурация сборки */
  appConfiguration?: Maybe<Scalars['String']>;
  /** Версия сборки */
  appVersion?: Maybe<Scalars['String']>;
  /** Framework version */
  aspDotnetVersion?: Maybe<Scalars['String']>;
  /** DB Info */
  dbVersion?: Maybe<Scalars['String']>;
  /** Host OS version */
  osDescription?: Maybe<Scalars['String']>;
}

/** Обращение */
export interface IAppeal {
  __typename?: 'Appeal';
  /** --- */
  address?: Maybe<Scalars['String']>;
  /** Должностное лицо - адресат сообщения */
  addressee?: Maybe<Scalars['String']>;
  /** --- */
  answerByPost?: Maybe<Scalars['Boolean']>;
  /** Исполнители */
  appealExecutors?: Maybe<Array<Maybe<IAppealExecutor>>>;
  /** Ответственные исполнители */
  appealPrincipalExecutors?: Maybe<Array<Maybe<IAppealExecutor>>>;
  /** Тип обращения (обычное/сообщение обратной связи) */
  appealType?: Maybe<IAppealTypeEnum>;
  /** Принято к публикации/отказано */
  approved?: Maybe<IApprovedEnum>;
  /** Присоединенные файлы */
  attachedFiles?: Maybe<Array<Maybe<IAttachedFile>>>;
  /** Автор */
  author?: Maybe<IUser>;
  /** --- */
  authorName?: Maybe<Scalars['String']>;
  /** --- */
  clientType?: Maybe<Scalars['Int']>;
  /** --- */
  collective?: Maybe<Scalars['Boolean']>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** --- */
  deloStatus?: Maybe<Scalars['String']>;
  /** --- */
  deloText?: Maybe<Scalars['String']>;
  /** --- */
  description?: Maybe<Scalars['String']>;
  /** --- */
  email?: Maybe<Scalars['String']>;
  /** Дата регистрации */
  extDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  extId?: Maybe<Scalars['String']>;
  /** Рег № */
  extNumber?: Maybe<Scalars['String']>;
  /** --- */
  externalExec?: Maybe<Scalars['Boolean']>;
  /** --- */
  factDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  filesPush?: Maybe<Scalars['DateTime']>;
  /** --- */
  firstName?: Maybe<Scalars['String']>;
  /**
   * ---
   * @deprecated Не пригодилась
   */
  fulltext?: Maybe<Scalars['String']>;
  /** Есть неотмодерированные комментарии */
  hasUnapprovedMessages: Scalars['Boolean'];
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** --- */
  inn?: Maybe<Scalars['String']>;
  /** --- */
  lastName?: Maybe<Scalars['String']>;
  /** Место на карте: широта */
  latitude?: Maybe<Scalars['Float']>;
  /** Как я лайкнул */
  likeWIL?: Maybe<ILikeWil>;
  /** Счетчик лайков */
  likesCount: Scalars['Int'];
  /** Место на карте: долгота */
  longitude?: Maybe<Scalars['Float']>;
  /** --- */
  markSend?: Maybe<Scalars['Boolean']>;
  /** Ответы */
  messages?: Maybe<Array<Maybe<IMessage>>>;
  /** Всего комментариев */
  messagesCount: Scalars['Int'];
  /** Стадии модерации */
  moderationStage?: Maybe<IModerationStageEnum>;
  /** Модераторы */
  moderators?: Maybe<Array<Maybe<IUser>>>;
  /** --- */
  municipality?: Maybe<Scalars['String']>;
  /** Нужно обновить во внешней системе */
  needToUpdateInExt?: Maybe<Scalars['Boolean']>;
  /** --- */
  number?: Maybe<Scalars['Int']>;
  /** Идентификатор обращения в системе из которой осуществен перенос */
  oldSystemId?: Maybe<Scalars['String']>;
  /** Организация */
  organization?: Maybe<IOrganizationType>;
  /** --- */
  patronim?: Maybe<Scalars['String']>;
  /** --- */
  phone?: Maybe<Scalars['String']>;
  /** Место на карте: описание */
  place?: Maybe<Scalars['String']>;
  /** --- */
  planDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  platform?: Maybe<Scalars['String']>;
  /** Должность */
  post?: Maybe<Scalars['String']>;
  /** Публичность запрошена */
  publicGranted?: Maybe<Scalars['Boolean']>;
  /** Публичность подтверждена */
  public_: Scalars['Boolean'];
  /** --- */
  pushesSent?: Maybe<Scalars['Boolean']>;
  /** Дата регистрации документа */
  regDate?: Maybe<Scalars['DateTime']>;
  /** Регистрационный номер документа в организации/ИП */
  regNumber?: Maybe<Scalars['String']>;
  /** Режим оценки отчетов исполнителей */
  regardMode?: Maybe<IRegardModeEnum>;
  /** Регион */
  region?: Maybe<IRegion>;
  /** --- */
  region1?: Maybe<Scalars['String']>;
  /** Причина отказа в публикации */
  rejectionReason?: Maybe<Scalars['String']>;
  /** --- */
  resDate?: Maybe<Scalars['DateTime']>;
  /** Рубрика */
  rubric?: Maybe<IRubric>;
  /** --- */
  sentToDelo?: Maybe<Scalars['Boolean']>;
  /** Подписал */
  signatory?: Maybe<Scalars['String']>;
  /** --- */
  siteStatus?: Maybe<Scalars['Int']>;
  /** --- */
  statusConfirmed?: Maybe<Scalars['Boolean']>;
  /** Уведомлять об ответах на данное обращение */
  subscribeToAnswers?: Maybe<ISubscribeToAnswersEnum>;
  /** Подсистема */
  subsystem?: Maybe<ISubsystem>;
  /** Территория */
  territory?: Maybe<ITerritory>;
  /** --- */
  title?: Maybe<Scalars['String']>;
  /** Топик */
  topic?: Maybe<ITopic>;
  /** --- */
  topicUpdated?: Maybe<Scalars['Boolean']>;
  /** неотмодерированные комментарии */
  unapprovedMessages?: Maybe<Array<Maybe<IMessage>>>;
  /** --- */
  unapprovedMessagesCount: Scalars['Int'];
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** --- */
  uploaded?: Maybe<Scalars['Boolean']>;
  /** Кто поставил лайк или дизлайк */
  usersLiked?: Maybe<Array<Maybe<IUser>>>;
  /** Как я посмотрел */
  viewWIV?: Maybe<IViewWiv>;
  /** --- */
  views?: Maybe<Scalars['Int']>;
  /** Счетчик просмотров */
  viewsCount: Scalars['Int'];
  /** Без номера */
  wORegNumberRegDate?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
  /** --- */
  zip?: Maybe<Scalars['String']>;
}

/** Исполнитель */
export interface IAppealExecutor {
  __typename?: 'AppealExecutor';
  /** Обращения, в которых есть такой исполнитель */
  appeals?: Maybe<Array<Maybe<IAppeal>>>;
  /** Обращения, в которых есть такой ответствнный исполнитель */
  appealsPrincipal?: Maybe<Array<Maybe<IAppeal>>>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** --- */
  department?: Maybe<Scalars['String']>;
  /** --- */
  departmentId?: Maybe<Scalars['String']>;
  /** Идентификатор во внешней системе */
  extId?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** --- */
  organization?: Maybe<Scalars['String']>;
  /** --- */
  position?: Maybe<Scalars['String']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Пользователь */
  user?: Maybe<IUser>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IAppealExecutorPageType {
  __typename?: 'AppealExecutorPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IAppealExecutor>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IAppealPageType {
  __typename?: 'AppealPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IAppeal>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

/** Статистика обращений */
export interface IAppealStats {
  __typename?: 'AppealStats';
  /** Количество обращений */
  appealsCount?: Maybe<Scalars['Int']>;
  /** Одобренные */
  approved?: Maybe<IApprovedEnum>;
  /** Дата создания */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удаленные */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Публичные */
  public?: Maybe<Scalars['Boolean']>;
  /** Регион */
  region?: Maybe<IRegion>;
  /** Рубрика */
  rubric?: Maybe<IRubric>;
  /** Зарегистрировано в Деле */
  sentToDelo?: Maybe<Scalars['Boolean']>;
  /** Статус */
  siteStatus?: Maybe<Scalars['Int']>;
  /** Подсистема */
  subsystem?: Maybe<ISubsystem>;
  /** Территория */
  territory?: Maybe<ITerritory>;
  /** Тема */
  topic?: Maybe<ITopic>;
}

export interface IAppealStatsPageType {
  __typename?: 'AppealStatsPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IAppealStats>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export enum IAppealTypeEnum {
  Feedback = 'FEEDBACK',
  Normal = 'NORMAL'
}

export interface IAppealsStatBy {
  __typename?: 'AppealsStatBy';
  appealCount: Scalars['Long'];
  createdAtDay?: Maybe<Scalars['DateTime']>;
  createdAtHour?: Maybe<Scalars['DateTime']>;
  createdAtMonth?: Maybe<Scalars['DateTime']>;
  createdAtYear?: Maybe<Scalars['DateTime']>;
  region?: Maybe<IRegion>;
  rubric?: Maybe<IRubric>;
  territory?: Maybe<ITerritory>;
  topic?: Maybe<ITopic>;
  updatedAtDay?: Maybe<Scalars['DateTime']>;
  updatedAtHour?: Maybe<Scalars['DateTime']>;
  updatedAtMonth?: Maybe<Scalars['DateTime']>;
  updatedAtYear?: Maybe<Scalars['DateTime']>;
}

export interface IAppealsStatByPageType {
  __typename?: 'AppealsStatByPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IAppealsStatBy>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IAppointment {
  __typename?: 'Appointment';
  address?: Maybe<Scalars['String']>;
  admin?: Maybe<Scalars['Boolean']>;
  answers?: Maybe<Array<Maybe<IAppointmentNotification>>>;
  appointmentRange?: Maybe<IAppointmentRange>;
  attachedFiles?: Maybe<Array<Maybe<IAttachedFile>>>;
  authorName?: Maybe<Scalars['String']>;
  authorNameUpperCase?: Maybe<Scalars['String']>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  date?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  description?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  municipality?: Maybe<Scalars['String']>;
  number?: Maybe<Scalars['Int']>;
  officer?: Maybe<IOfficer>;
  phone?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
  rejectionReason?: Maybe<Scalars['String']>;
  rubric?: Maybe<IRubric>;
  status?: Maybe<Scalars['Int']>;
  topic?: Maybe<ITopic>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
  zip?: Maybe<Scalars['String']>;
}

export interface IAppointmentNotification {
  __typename?: 'AppointmentNotification';
  appointment?: Maybe<IAppointment>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  date?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  html?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  status?: Maybe<Scalars['Int']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IAppointmentNotificationPageType {
  __typename?: 'AppointmentNotificationPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IAppointmentNotification>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IAppointmentPageType {
  __typename?: 'AppointmentPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IAppointment>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IAppointmentRange {
  __typename?: 'AppointmentRange';
  appointments?: Maybe<Scalars['Int']>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  date?: Maybe<Scalars['DateTime']>;
  deleteMark?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  end?: Maybe<Scalars['DateTime']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  officer?: Maybe<IOfficer>;
  officerName?: Maybe<Scalars['String']>;
  registeredAppointments?: Maybe<Array<Maybe<IAppointment>>>;
  rubricName?: Maybe<Scalars['String']>;
  rubricOrder?: Maybe<Scalars['Int']>;
  start?: Maybe<Scalars['DateTime']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IAppointmentRangePageType {
  __typename?: 'AppointmentRangePageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IAppointmentRange>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export enum IApprovedEnum {
  Accepted = 'ACCEPTED',
  Rejected = 'REJECTED',
  Undefined = 'UNDEFINED'
}

export interface IAttachedFile {
  __typename?: 'AttachedFile';
  /** Content-Type */
  contentType?: Maybe<Scalars['String']>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Внешний идентификатор */
  extId?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Не докачан */
  incomplete: Scalars['Boolean'];
  /** Файл публичный */
  isPublic?: Maybe<Scalars['Boolean']>;
  /** Длина */
  len: Scalars['Long'];
  /** Имя файла */
  name?: Maybe<Scalars['String']>;
  /** Путь */
  path?: Maybe<Scalars['String']>;
  /** Файл приватный */
  private?: Maybe<Scalars['Boolean']>;
  /** Без доступа по чтению */
  sequre: Scalars['Boolean'];
  /** Миниатюра */
  thumb?: Maybe<Scalars['String']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Кем загружен на сервер */
  uploadedBy?: Maybe<IUser>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IAttachedFilePageType {
  __typename?: 'AttachedFilePageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IAttachedFile>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IBalance {
  __typename?: 'Balance';
  /** Сумма */
  amount?: Maybe<Scalars['Decimal']>;
  /** Валюта */
  currency?: Maybe<Scalars['String']>;
  /** Количество доступных отправок СМС */
  smsCount?: Maybe<Scalars['UInt']>;
}



export interface IChangeJournalItemPageType {
  __typename?: 'ChangeJournalItemPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IJournalItemType>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export enum IClaimEntity {
  Role = 'ROLE',
  System = 'SYSTEM',
  User = 'USER'
}

export interface IClaimInfoType {
  __typename?: 'ClaimInfoType';
  /** Сущности к которым может применяться разрешение */
  claimEntity?: Maybe<Array<Maybe<IClaimEntity>>>;
  /** Тип разрешения */
  claimType: Scalars['String'];
  /** Описание разрешения */
  description: Scalars['String'];
  /** Возможные значения */
  values?: Maybe<Array<Maybe<IClaimInfoValueType>>>;
}

export interface IClaimInfoValueType {
  __typename?: 'ClaimInfoValueType';
  /** Описание */
  description: Scalars['String'];
  /** Значение */
  value: Scalars['String'];
}

export type IClaimInputType = {
  /** Тип разрешения */
  type: Scalars['String'];
  /** Значение разрешения */
  value: Scalars['String'];
};

export interface IClaimType {
  __typename?: 'ClaimType';
  /** Тип разрешения */
  type: Scalars['String'];
  /** Значение разрешения */
  value: Scalars['String'];
}

export type IClerkInSubsystemInputType = {
  /** Подсистема */
  subsystemUid?: Maybe<Scalars['ID']>;
  /** Клерки */
  userIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
};

export interface IClerkInSubsystemType {
  __typename?: 'ClerkInSubsystemType';
  /** Подсистема */
  subsystemUid: Scalars['String'];
  /** Клерки */
  users?: Maybe<Array<Maybe<IUser>>>;
}

export type IConfirmEmailType = {
  /** Код подтверждения */
  code?: Maybe<Scalars['String']>;
  /** Хэш код Email */
  sh?: Maybe<Scalars['Int']>;
  /** Идентификатор пользователя */
  userId?: Maybe<Scalars['Long']>;
};

export type ICustomEmail = {
  /** Идентификаторы прикрепляемых файлов */
  filesId?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Отправитель */
  from?: Maybe<IEmailPerson>;
  /** Сообщение */
  message: Scalars['String'];
  /** Тема */
  subject: Scalars['String'];
  /** Получатели */
  to?: Maybe<Array<Maybe<IEmailPerson>>>;
};





export enum IDocType {
  BirthCert = 'BIRTH_CERT',
  DrivingLicense = 'DRIVING_LICENSE',
  Foreign = 'FOREIGN',
  ForeignPassport = 'FOREIGN_PASSPORT',
  MedicalPolicy = 'MEDICAL_POLICY',
  Military = 'MILITARY',
  None = 'NONE',
  Passport = 'PASSPORT'
}

export type IEmailPerson = {
  /** Адрес электронной почты */
  email?: Maybe<Scalars['String']>;
  /** Имя отправителя/получателя */
  name?: Maybe<Scalars['String']>;
};

export interface IEmailTemplatePageType {
  __typename?: 'EmailTemplatePageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IEmailTemplateType>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IEmailTemplateType {
  __typename?: 'EmailTemplateType';
  /** Дата создания */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Ключ */
  key?: Maybe<Scalars['ID']>;
  /** Шаблон */
  template: Scalars['String'];
  /** Дата обновления */
  updatedAt?: Maybe<Scalars['DateTime']>;
}

export enum IEntityOperation {
  Changing = 'CHANGING',
  Creating = 'CREATING',
  Deleting = 'DELETING',
  Undefined = 'UNDEFINED'
}

export type IEntitySelector = {
  /** id */
  id: Scalars['Long'];
  /** Имя объекта */
  name: Scalars['String'];
};

export interface IEnumVerb {
  __typename?: 'EnumVerb';
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  customSettings?: Maybe<Scalars['String']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  description?: Maybe<Scalars['String']>;
  group?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  shortName?: Maybe<Scalars['String']>;
  subsystemUID?: Maybe<Scalars['String']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  value?: Maybe<Scalars['String']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IEnumVerbPageType {
  __typename?: 'EnumVerbPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IEnumVerb>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IExecuteReportResult {
  __typename?: 'ExecuteReportResult';
  /** Ошибки формирования отчета */
  errors?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Файл отчета */
  file?: Maybe<IAttachedFile>;
  /** Идентификатор отчета */
  id?: Maybe<Scalars['Guid']>;
  /** Признау успешного создания отчета */
  success?: Maybe<Scalars['Boolean']>;
  /** Отображаемая HTML разметка отчета */
  template?: Maybe<Scalars['String']>;
}

export interface IFileDownload {
  __typename?: 'FileDownload';
  /** Данные в base64 */
  data: Scalars['String'];
  /** Id файла */
  id?: Maybe<Scalars['ID']>;
  /** Позиция в файле */
  pos: Scalars['Long'];
}

export type IFileDownloadRequest = {
  /** Id файла */
  id?: Maybe<Scalars['ID']>;
  /** Запрашиваемая длина */
  len: Scalars['Long'];
  /** Запрашиваемая позиция в файле */
  pos: Scalars['Long'];
};

export type IFileUpload = {
  /** Content-Type */
  contentType?: Maybe<Scalars['String']>;
  /** Данные в base64 */
  data: Scalars['String'];
  /** Id файла. Передается в последующих кусочках */
  id?: Maybe<Scalars['ID']>;
  /** Общая длина файла. Передается в первом кусочке */
  len?: Maybe<Scalars['Long']>;
  /** Имя файла. Передается в первом кусочке */
  name?: Maybe<Scalars['String']>;
  /** Id объекта, куда аттачить файл */
  parentId?: Maybe<Scalars['ID']>;
  /** Тип объекта */
  parentType?: Maybe<Scalars['String']>;
  /** Позиция передаваемых данных */
  pos: Scalars['Long'];
  /** Без доступа по чтению */
  sequre?: Maybe<Scalars['Boolean']>;
};

export interface IFileUploadResult {
  __typename?: 'FileUploadResult';
  /** Id файла */
  id?: Maybe<Scalars['ID']>;
  /** Ближайшая к началу позиция файла где данные еще не загружены */
  next?: Maybe<Scalars['Long']>;
  /** Процент загруженности (0-100) */
  percent?: Maybe<Scalars['Int']>;
}

export type IFilter = {
  /** Равенство. Для строк с учетом регистра. */
  equal?: Maybe<Scalars['String']>;
  /** Больше или равно */
  greaterOrEqual?: Maybe<Scalars['String']>;
  /** Есть в списке */
  in?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Меньше */
  less?: Maybe<Scalars['String']>;
  /** Меньше или равно */
  lessOrEqual?: Maybe<Scalars['String']>;
  /** Поиск подстроки (%ищу%), начала (ищу%) или конца (%ищу) строки в любом регистре */
  like?: Maybe<Scalars['String']>;
  /** Больше */
  more?: Maybe<Scalars['String']>;
  /** Имя поля */
  name?: Maybe<Scalars['String']>;
  /** Инверсия условия */
  not?: Maybe<Scalars['Boolean']>;
  /** Объединение с накопленным результатом */
  op?: Maybe<IFilterNextOpEnum>;
  /** Использование стека */
  stacked?: Maybe<Scalars['Boolean']>;
  /** Условия фильтрации через DynamicLinq */
  where?: Maybe<Scalars['String']>;
  /** Включить удаленные элементы */
  withDeleted?: Maybe<Scalars['Boolean']>;
};

export enum IFilterNextOpEnum {
  And = 'AND',
  Except = 'EXCEPT',
  Or = 'OR',
  Pop = 'POP',
  Push = 'PUSH',
  Xy = 'XY'
}

export type IForgotPasswordType = {
  /** Адрес электронной почты */
  email?: Maybe<Scalars['String']>;
  /** Логин */
  login?: Maybe<Scalars['String']>;
};

export enum IGender {
  Female = 'FEMALE',
  Male = 'MALE'
}


export interface IJournalEntityTagsType {
  __typename?: 'JournalEntityTagsType';
  /** Назване сущности */
  name: Scalars['String'];
  /** Тэги свойств */
  propertyTags?: Maybe<Array<Maybe<IJournalTagsType>>>;
  /** Имя сущности */
  tag: Scalars['String'];
}

export interface IJournalItemType {
  __typename?: 'JournalItemType';
  /** Ключ сущности */
  entityKey: Scalars['String'];
  /** Имя сущности */
  entityName: Scalars['String'];
  /** Название сущности */
  friendlyName: Scalars['String'];
  /** Тип операции */
  operation?: Maybe<IEntityOperation>;
  /** Название операции */
  operationName: Scalars['String'];
  /** Измененные свойства */
  properties?: Maybe<Array<Maybe<IJournalPropertiesType>>>;
  /** Штамп времени */
  timestamp?: Maybe<Scalars['DateTime']>;
  /** Пользователь */
  user?: Maybe<IUser>;
}

export interface IJournalOperationsType {
  __typename?: 'JournalOperationsType';
  /** Тип операции */
  operation?: Maybe<IEntityOperation>;
  /** Назване операции */
  operationName: Scalars['String'];
}

export interface IJournalPropertiesType {
  __typename?: 'JournalPropertiesType';
  /** Назване свойства */
  friendlyName: Scalars['String'];
  /** Новое значение свойства */
  newValue?: Maybe<Scalars['String']>;
  /** Новое значение свойства (дружественное) */
  newValueFriendly?: Maybe<Scalars['String']>;
  /** Старое значение свойства */
  oldValue?: Maybe<Scalars['String']>;
  /** Имя поля */
  tag: Scalars['String'];
}

export interface IJournalTagsType {
  __typename?: 'JournalTagsType';
  /** Назване свойства */
  name: Scalars['String'];
  /** Имя поля */
  tag: Scalars['String'];
}

export interface ILikeReport {
  __typename?: 'LikeReport';
  /** Счетчик дизлайков */
  dislikeCount: Scalars['Int'];
  /** Счетчик лайков */
  likeCount: Scalars['Int'];
  /** NONE - ничего,LIKED - стоит лайк, DISLIKED - cтоит дизлайк */
  liked?: Maybe<ILikeStateEnum>;
}

export type ILikeRequest = {
  /** Id объекта */
  id?: Maybe<Scalars['ID']>;
  /**
   * NONE - ничего не делать,LIKE - поставить лайк,LIKE_OFF - снять лайк,DISLIKE -
   * поставить дизлайк,DISLIKE_OFF - снять дизлайк
   */
  liked?: Maybe<ILikeRequestEnum>;
  /** Тип объекта */
  type?: Maybe<Scalars['String']>;
};

export enum ILikeRequestEnum {
  Dislike = 'DISLIKE',
  DislikeOff = 'DISLIKE_OFF',
  Like = 'LIKE',
  LikeOff = 'LIKE_OFF',
  None = 'NONE'
}

export enum ILikeStateEnum {
  Disliked = 'DISLIKED',
  Liked = 'LIKED',
  None = 'NONE'
}

export interface ILikeWil {
  __typename?: 'LikeWIL';
  /** NONE - ничего,LIKED - стоит лайк, DISLIKED - cтоит дизлайк */
  liked?: Maybe<ILikeStateEnum>;
}

export enum ILoginRequestSource {
  Mobile = 'MOBILE',
  Site = 'SITE',
  Undefined = 'UNDEFINED'
}

export interface ILoginResultType {
  __typename?: 'LoginResultType';
  /** Сообщение об ошибке */
  errorMessage?: Maybe<Scalars['String']>;
  /** Ключ подписки */
  key?: Maybe<Scalars['String']>;
  /** Статус операции авторизации */
  loginStatus?: Maybe<Scalars['String']>;
  /** Токен */
  token?: Maybe<Scalars['String']>;
  /** Пользователь */
  user?: Maybe<IUser>;
}

export enum ILoginSystem {
  Esia = 'ESIA',
  Facebook = 'FACEBOOK',
  Form = 'FORM',
  Google = 'GOOGLE',
  Vk = 'VK'
}

export type ILoginType = {
  /** URL-обратного вызова */
  callbackUrl?: Maybe<Scalars['String']>;
  /** Код доступа */
  code?: Maybe<Scalars['String']>;
  /** Логин */
  login?: Maybe<Scalars['String']>;
  /** Тип системы через которую запорсили авторизацию (Возможные значения SITE или MOBILE) */
  loginRequestSource?: Maybe<ILoginRequestSource>;
  /** Система авторизации */
  loginSystem?: Maybe<ILoginSystem>;
  /** Пароль */
  password?: Maybe<Scalars['String']>;
  /** Описание ошибки запроса кода доступа через сторонние системы авторизации */
  requestCodeError?: Maybe<Scalars['String']>;
  /** Ключ подписки */
  subscribeKey?: Maybe<Scalars['String']>;
};


/** Сообщение/комментарий */
export interface IMessage {
  __typename?: 'Message';
  /** Корневое обращение */
  appeal?: Maybe<IAppeal>;
  /** Автор сообщения: Исполнитель (может быть внешним) */
  appealExecutor?: Maybe<IAppealExecutor>;
  /** Принято к публикации/отказано */
  approved?: Maybe<IApprovedEnum>;
  /** присоединенные файлы */
  attachedFiles?: Maybe<Array<Maybe<IAttachedFile>>>;
  /** Автор сообщения: Пользователь (внутренний) */
  author?: Maybe<IUser>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Счетчик дизлайков */
  dislikesCount: Scalars['Int'];
  /** Ключ для упрощения доступа к иерархическим объектам */
  due?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Является папкой */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Вид сообщения */
  kind?: Maybe<IMessageKindEnum>;
  /** Как я лайкнул */
  likeWIL?: Maybe<ILikeWil>;
  /** Счетчик лайков */
  likesCount: Scalars['Int'];
  /** Стадии модерации */
  moderationStage?: Maybe<IModerationStageEnum>;
  /** Идентификатор родителя */
  parentId?: Maybe<Scalars['ID']>;
  /** Наименование типа родителя */
  parentType?: Maybe<Scalars['String']>;
  /** Средняя оценка */
  regardAverage: Scalars['Float'];
  /** Как я оценил */
  regardWIR?: Maybe<IRegardWir>;
  /** Счетчик оценок */
  regardsCount: Scalars['Int'];
  /** Уведомлять об ответах на данное сообщение */
  subscribeToAnswers?: Maybe<ISubscribeToAnswersEnum>;
  /** Текст сообщения */
  text?: Maybe<Scalars['String']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Лайки */
  usersLiked?: Maybe<Array<Maybe<IUser>>>;
  /** Оценки */
  usersRegard?: Maybe<Array<Maybe<IUser>>>;
  /** Просмотры */
  usersViewed?: Maybe<Array<Maybe<IUser>>>;
  /** Как я посмотрел */
  viewWIV?: Maybe<IViewWiv>;
  /** Счетчик просмотров */
  viewsCount: Scalars['Int'];
  /** Корневое голосование */
  voteRoot?: Maybe<IVoteRoot>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export enum IMessageKindEnum {
  Normal = 'NORMAL',
  ReasonToParentMessage = 'REASON_TO_PARENT_MESSAGE',
  SystemUserMessage = 'SYSTEM_USER_MESSAGE'
}

export interface IMessagePageType {
  __typename?: 'MessagePageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IMessage>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

/** Подписка на комментарии */
export interface IMessageSubscribeGet {
  __typename?: 'MessageSubscribeGet';
  /** DUE */
  due: Scalars['String'];
  /** Уровень вложенности */
  level: Scalars['Int'];
  /** Корневой объект */
  rootType: Scalars['String'];
}

/** Подписка на комментарии */
export interface IMessageSubscribeGet_ {
  __typename?: 'MessageSubscribeGet_';
  /** DUE */
  due: Scalars['String'];
  /** Уровень вложенности */
  level: Scalars['Int'];
  /** Корневой объект */
  rootType: Scalars['String'];
}

/** Подписка на комментарии */
export type IMessageSubscribeSet = {
  /** ID объекта */
  entityId?: Maybe<Scalars['ID']>;
  /** Имя объекта */
  entityType: Scalars['String'];
  /** Уровень вложенности */
  level?: Maybe<Scalars['Int']>;
};


export enum IModeEnum {
  ManySelect = 'MANY_SELECT',
  OneSelect = 'ONE_SELECT'
}

export enum IModerationStageEnum {
  Done = 'DONE',
  Inprogress = 'INPROGRESS',
  Wait = 'WAIT'
}

export interface IMutation {
  __typename?: 'Mutation';
  /** Принятие к публикации или отклонение обращения (Policy = Authorized) */
  approveAppeal?: Maybe<IAppeal>;
  /** Публикация файла (Policy = Authorized) */
  approveAttachedFile?: Maybe<IAttachedFile>;
  /** Принятие к публикации или отклонение комментария (Policy = Authorized) */
  approveMessage?: Maybe<IMessagePageType>;
  /** Принятие к публикации или отклонение новости (Policy = Authorized) */
  approveNews?: Maybe<INews>;
  /** Отправлка СМС подтверждения телефонного номера (Policy = LoginSystemForm) */
  confirmPhone?: Maybe<IOperationResultType>;
  /** Создание нового обращения */
  createAppeal?: Maybe<IAppeal>;
  /** Создание нового исполнителя (Policy = Authorized) */
  createAppealExecutor?: Maybe<IAppealExecutor>;
  /**  (Policy = Authorized) */
  createAppointment?: Maybe<IAppointment>;
  /**  (Policy = Authorized) */
  createAppointmentNotification?: Maybe<IAppointmentNotification>;
  /**  (Policy = Authorized) */
  createAppointmentRange?: Maybe<IAppointmentRange>;
  /** Создание файла (ссылки) */
  createAttachedFile?: Maybe<IAttachedFile>;
  /** Создание сообщения электронной почты (Policy = ManageCustomEmail) */
  createEmail?: Maybe<Scalars['Boolean']>;
  /**  (Policy = Authorized) */
  createEnumVerb?: Maybe<IEnumVerb>;
  /** Создание нового сообщения обратной связи */
  createFBAppeal?: Maybe<IAppeal>;
  /**
   * Создать новое сообщение/комментарий. Сообщение должно быть привязано
   * (parentId/parentType) к обращению (Appeal), сообщению (Message), новости
   * (News) либо к голосованию (Vote) (Policy = Authorized)
   */
  createMessage?: Maybe<IMessage>;
  /** Создание новой новости (Policy = CreateNews) */
  createNews?: Maybe<INews>;
  /**  (Policy = Authorized) */
  createOfficer?: Maybe<IOfficer>;
  /** Регистрация нового токена (Policy = Authorized) */
  createPToken?: Maybe<IPToken>;
  /** создание нового Региона (Policy = ManageRegions) */
  createRegion?: Maybe<IRegion>;
  /** Создание роли (Policy = ManageRoles) */
  createRole?: Maybe<IRoleType>;
  /** создание новой Рубрики (Policy = ManageRubrics) */
  createRubric?: Maybe<IRubric>;
  /**  (Policy = Authorized) */
  createSubsystem?: Maybe<ISubsystem>;
  /** создание новой Территории (Policy = ManageTerritories) */
  createTerritory?: Maybe<ITerritory>;
  /** создание нового Топика (Policy = ManageTopics) */
  createTopic?: Maybe<ITopic>;
  /** Создание пользователя (Policy = Manage) */
  createUser?: Maybe<IUser>;
  /**  (Policy = CreateVoting) */
  createVote?: Maybe<IVote>;
  /**  (Policy = ModerateVoting) */
  createVoteItem?: Maybe<IVoteItem>;
  /**  (Policy = CreateVoting) */
  createVoteRoot?: Maybe<IVoteRoot>;
  /**  (Policy = Authorized) */
  delete: Scalars['String'];
  /** Удаление сообщения с описанием причины (Policy = Authorized) */
  deleteMessageWithComment?: Maybe<IMessage>;
  /** Удаление токена (Policy = Authorized) */
  deletePToken?: Maybe<IPToken>;
  /** Удалить пользовательские параметры отчета (Policy = ManageReports) */
  deleteReportSettings?: Maybe<Scalars['Boolean']>;
  /** Загрузка файла на сервер */
  fileUpload?: Maybe<IFileUploadResult>;
  /** Поставить лайк (Policy = Authorized) */
  like?: Maybe<ILikeReport>;
  /** Поставить лайк Обращению (Policy = Authorized) */
  likeAppeal?: Maybe<ILikeReport>;
  /** Поставить лайк Сообщению (Policy = Authorized) */
  likeMessage?: Maybe<ILikeReport>;
  /**  (Policy = Authorized) */
  messageSubscribe?: Maybe<IMessageSubscribeGet>;
  /**  (Policy = Authorized) */
  messageUnSubscribe: Scalars['String'];
  /** Поставить оценку (Policy = Authorized) */
  regard?: Maybe<IRegardReport>;
  /** Изменение существующего обращения (Policy = Authorized) */
  registerAppeal?: Maybe<IAppeal>;
  /** Регистрация пользователя */
  registerUser?: Maybe<ILoginResultType>;
  /** Удаление роли (Policy = ManageRoles) */
  removeRole?: Maybe<Scalars['Boolean']>;
  /** Смена пароля (Policy = Authorized) */
  resetPassword?: Maybe<IOperationResultType>;
  /** Восстановление пароля */
  restorePassword?: Maybe<IOperationResultType>;
  /** Сохранить пользовательские параметры отчета (Policy = ManageReports) */
  saveReportSettings?: Maybe<IReportSavedSettings>;
  /**  (Policy = ManageSettings) */
  setSettings?: Maybe<Array<Maybe<ISettings>>>;
  /** Сортировка ролей (Policy = ManageRoles) */
  sortRole?: Maybe<Array<Maybe<IRoleSortedType>>>;
  /** Изменение существующего обращения (Policy = Authorized) */
  updateAppeal?: Maybe<IAppeal>;
  /** Изменение существующего исполнителя (Policy = Authorized) */
  updateAppealExecutor?: Maybe<IAppealExecutor>;
  /**  (Policy = Authorized) */
  updateAppointment?: Maybe<IAppointment>;
  /**  (Policy = Authorized) */
  updateAppointmentNotification?: Maybe<IAppointmentNotification>;
  /**  (Policy = Authorized) */
  updateAppointmentRange?: Maybe<IAppointmentRange>;
  /** Изменение ссылки на файл (Policy = Authorized) */
  updateAttachedFile?: Maybe<IAttachedFile>;
  /** Обновление по ID (Policy = Authorized) */
  updateByIDEnumVerb?: Maybe<IEnumVerb>;
  /** Обновление по тройке полей {value, group, subsystemUID} (Policy = Authorized) */
  updateByKeyEnumVerb?: Maybe<IEnumVerb>;
  /**
   * Если в теле объекта задан id, то объект ищется по id и остальные поля
   * редактируются. Если id не задан, то должна быть задана тройка полей {value,
   * group, subsystemUID} и по ней ищется объект.  (Policy = Authorized)
   */
  updateEnumVerb?: Maybe<IEnumVerb>;
  /** Изменить существующее сообщение (Policy = Authorized) */
  updateMessage?: Maybe<IMessage>;
  /** Изменение существующей новости (Policy = Authorized) */
  updateNews?: Maybe<INews>;
  /**  (Policy = Authorized) */
  updateOfficer?: Maybe<IOfficer>;
  /** изменение существующего Региона */
  updateRegion?: Maybe<IRegion>;
  /** Редактирование роли (Policy = ManageRoles) */
  updateRole?: Maybe<IRoleType>;
  /** изменение существующей Рубрики */
  updateRubric?: Maybe<IRubric>;
  /**  (Policy = Authorized) */
  updateSubsystem?: Maybe<ISubsystem>;
  /** изменение существующей Территории */
  updateTerritory?: Maybe<ITerritory>;
  /** изменение существующего Топика */
  updateTopic?: Maybe<ITopic>;
  /** Редактирование пользователя (Policy = LoginSystemForm) */
  updateUser?: Maybe<IUser>;
  /**  (Policy = ModerateVoting) */
  updateVote?: Maybe<IVote>;
  /**  (Policy = ModerateVoting) */
  updateVoteItem?: Maybe<IVoteItem>;
  /**  (Policy = ModerateVoting) */
  updateVoteRoot?: Maybe<IVoteRoot>;
  /** Увидеть/развидеть (Policy = Authorized) */
  view?: Maybe<IViewReport>;
  /**  (Policy = Vote) */
  voteAction?: Maybe<IVote>;
  /**  (Policy = ManageVoting) */
  voteStart?: Maybe<IVoteRoot>;
  /**  (Policy = ManageVoting) */
  voteStop?: Maybe<IVoteRoot>;
  /**  (Policy = Vote) */
  votesAction?: Maybe<IVotePageType>;
  /**  (Policy = Authorized) */
  writeEnumVerb?: Maybe<IEnumVerb>;
}


export type IMutationApproveAppealArgs = {
  appealApprove: IApproveAppeal;
  id: Scalars['ID'];
};


export type IMutationApproveAttachedFileArgs = {
  attachedFileApprove: IApproveAttachedFile;
  id: Scalars['ID'];
};


export type IMutationApproveMessageArgs = {
  id: Scalars['ID'];
  messageApprove: IApproveMessage;
};


export type IMutationApproveNewsArgs = {
  id: Scalars['ID'];
  newsApprove: IApproveNews;
};


export type IMutationConfirmPhoneArgs = {
  code: Scalars['String'];
  phoneId: Scalars['ID'];
};


export type IMutationCreateAppealArgs = {
  appeal: ICreateAppeal;
};


export type IMutationCreateAppealExecutorArgs = {
  appealExecutor: ICreateAppealExecutor;
};


export type IMutationCreateAppointmentArgs = {
  appointment: ICreateAppointment;
};


export type IMutationCreateAppointmentNotificationArgs = {
  appointmentNotification: ICreateAppointmentNotification;
};


export type IMutationCreateAppointmentRangeArgs = {
  appointmentRange: ICreateAppointmentRange;
};


export type IMutationCreateAttachedFileArgs = {
  attachedFile: ICreateAttachedFile;
};


export type IMutationCreateEmailArgs = {
  customEmail: ICustomEmail;
};


export type IMutationCreateEnumVerbArgs = {
  enumVerb?: Maybe<ICreateEnumVerb>;
};


export type IMutationCreateFbAppealArgs = {
  appeal: ICreateFbAppeal;
};


export type IMutationCreateMessageArgs = {
  message: ICreateMessage;
};


export type IMutationCreateNewsArgs = {
  news: ICreateNews;
};


export type IMutationCreateOfficerArgs = {
  officer: ICreateOfficer;
};


export type IMutationCreatePTokenArgs = {
  pToken: ICreatePToken;
};


export type IMutationCreateRegionArgs = {
  region: ICreateRegion;
};


export type IMutationCreateRoleArgs = {
  role: IRoleInputType;
};


export type IMutationCreateRubricArgs = {
  rubric: ICreateRubric;
};


export type IMutationCreateSubsystemArgs = {
  subsystem: ICreateSubsystem;
};


export type IMutationCreateTerritoryArgs = {
  territory: ICreateTerritory;
};


export type IMutationCreateTopicArgs = {
  topic: ICreateTopic;
};


export type IMutationCreateUserArgs = {
  user: IRegisterModelType;
};


export type IMutationCreateVoteArgs = {
  vote: ICreateVote;
};


export type IMutationCreateVoteItemArgs = {
  voteItem: ICreateVoteItem;
};


export type IMutationCreateVoteRootArgs = {
  voteRoot: ICreateVoteRoot;
};


export type IMutationDeleteArgs = {
  entity: IEntitySelector;
};


export type IMutationDeleteMessageWithCommentArgs = {
  id: Scalars['ID'];
  messageDelete: IDeleteMessage;
};


export type IMutationDeletePTokenArgs = {
  pToken: IDeletePToken;
  userId?: Maybe<Scalars['ID']>;
};


export type IMutationDeleteReportSettingsArgs = {
  id?: Maybe<Scalars['ID']>;
  reportId: Scalars['Guid'];
};


export type IMutationFileUploadArgs = {
  file: IFileUpload;
};


export type IMutationLikeArgs = {
  like: ILikeRequest;
};


export type IMutationLikeAppealArgs = {
  like: ILikeRequest;
};


export type IMutationLikeMessageArgs = {
  like: ILikeRequest;
};


export type IMutationMessageSubscribeArgs = {
  subscribe?: Maybe<IMessageSubscribeSet>;
};


export type IMutationMessageUnSubscribeArgs = {
  unsubscribe_mask?: Maybe<IMessageSubscribeSet>;
};


export type IMutationRegardArgs = {
  regard: IRegardRequest;
};


export type IMutationRegisterAppealArgs = {
  id: Scalars['ID'];
};


export type IMutationRegisterUserArgs = {
  user: IRegisterModelType;
};


export type IMutationRemoveRoleArgs = {
  id: Scalars['ID'];
};


export type IMutationResetPasswordArgs = {
  resetPassword: IResetPasswordInputType;
};


export type IMutationRestorePasswordArgs = {
  restorePassword: IRestorePasswordType;
};


export type IMutationSaveReportSettingsArgs = {
  reportId: Scalars['Guid'];
  reportSavedSettingsInput: IReportSavedSettingsInput;
};


export type IMutationSetSettingsArgs = {
  settings?: Maybe<Array<Maybe<IUpdateSettings>>>;
};


export type IMutationSortRoleArgs = {
  role: Array<Maybe<IRoleSortingType>>;
};


export type IMutationUpdateAppealArgs = {
  appeal: IUpdateAppeal;
  id: Scalars['ID'];
};


export type IMutationUpdateAppealExecutorArgs = {
  appealExecutor: IUpdateAppealExecutor;
  id: Scalars['ID'];
};


export type IMutationUpdateAppointmentArgs = {
  appointment: IUpdateAppointment;
  id: Scalars['ID'];
};


export type IMutationUpdateAppointmentNotificationArgs = {
  appointmentNotification: IUpdateAppointmentNotification;
  id: Scalars['ID'];
};


export type IMutationUpdateAppointmentRangeArgs = {
  appointmentRange: IUpdateAppointmentRange;
  id: Scalars['ID'];
};


export type IMutationUpdateAttachedFileArgs = {
  attachedFile: IUpdateAttachedFile;
  id: Scalars['ID'];
};


export type IMutationUpdateByIdEnumVerbArgs = {
  enumVerb?: Maybe<IUpdateEnumVerb>;
  id?: Maybe<Scalars['ID']>;
};


export type IMutationUpdateByKeyEnumVerbArgs = {
  enumVerb?: Maybe<IUpdateEnumVerb>;
  group?: Maybe<Scalars['String']>;
  subsystemUID?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};


export type IMutationUpdateEnumVerbArgs = {
  enumVerb?: Maybe<IUpdateEnumVerb>;
};


export type IMutationUpdateMessageArgs = {
  id: Scalars['ID'];
  message: IUpdateMessage;
};


export type IMutationUpdateNewsArgs = {
  id: Scalars['ID'];
  news: IUpdateNews;
};


export type IMutationUpdateOfficerArgs = {
  id: Scalars['ID'];
  officer: IUpdateOfficer;
};


export type IMutationUpdateRegionArgs = {
  id: Scalars['ID'];
  region: IUpdateRegion;
};


export type IMutationUpdateRoleArgs = {
  id: Scalars['ID'];
  role: IRoleInputType;
};


export type IMutationUpdateRubricArgs = {
  id: Scalars['ID'];
  rubric: IUpdateRubric;
};


export type IMutationUpdateSubsystemArgs = {
  subsystem: IUpdateSubsystem;
  uid: Scalars['ID'];
};


export type IMutationUpdateTerritoryArgs = {
  id: Scalars['ID'];
  territory: IUpdateTerritory;
};


export type IMutationUpdateTopicArgs = {
  id: Scalars['ID'];
  topic: IUpdateTopic;
};


export type IMutationUpdateUserArgs = {
  id?: Maybe<Scalars['ID']>;
  user: IUserInputType;
};


export type IMutationUpdateVoteArgs = {
  id: Scalars['ID'];
  vote: IUpdateVote;
};


export type IMutationUpdateVoteItemArgs = {
  id: Scalars['ID'];
  voteItem: IUpdateVoteItem;
};


export type IMutationUpdateVoteRootArgs = {
  id: Scalars['ID'];
  voteRoot: IUpdateVoteRoot;
};


export type IMutationViewArgs = {
  view: IViewRequest;
};


export type IMutationVoteActionArgs = {
  voteAction: IVoteAction;
};


export type IMutationVoteStartArgs = {
  id: Scalars['ID'];
};


export type IMutationVoteStopArgs = {
  id: Scalars['ID'];
};


export type IMutationVotesActionArgs = {
  voteAction?: Maybe<Array<Maybe<IVoteAction>>>;
};


export type IMutationWriteEnumVerbArgs = {
  enumVerb?: Maybe<ICreateEnumVerb>;
};

/** Новость */
export interface INews {
  __typename?: 'News';
  /** Присоединенные файлы */
  attachedFiles?: Maybe<Array<Maybe<IAttachedFile>>>;
  /** Автор */
  author?: Maybe<IUser>;
  /** Пользователи могут оставлять комментарии */
  commentsAllowed?: Maybe<Scalars['Boolean']>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Как я лайкнул */
  likeWIL?: Maybe<ILikeWil>;
  /** Счетчик лайков */
  likesCount: Scalars['Int'];
  /** Ответы */
  messages?: Maybe<Array<Maybe<IMessage>>>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Регион */
  region?: Maybe<IRegion>;
  /** Рубрика */
  rubric?: Maybe<IRubric>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Краткий текст новости / начало текста новости */
  shortText?: Maybe<Scalars['String']>;
  /** Состояние */
  state?: Maybe<INewsStateEnum>;
  /** Территория */
  territory?: Maybe<ITerritory>;
  /** Содержимое новости */
  text?: Maybe<Scalars['String']>;
  /** Топик */
  topic?: Maybe<ITopic>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Кто поставил лайк или дизлайк */
  usersLiked?: Maybe<Array<Maybe<IUser>>>;
  /** Как я посмотрел */
  viewWIV?: Maybe<IViewWiv>;
  /** Счетчик просмотров */
  viewsCount: Scalars['Int'];
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface INewsPageType {
  __typename?: 'NewsPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<INews>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export enum INewsStateEnum {
  Draft = 'DRAFT',
  Moderate = 'MODERATE',
  Published = 'PUBLISHED',
  Unpublished = 'UNPUBLISHED'
}

export interface IOfficer {
  __typename?: 'Officer';
  authority?: Maybe<Scalars['String']>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  extId?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  place?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  ranges?: Maybe<Array<Maybe<IAppointmentRange>>>;
  rubric?: Maybe<IRubric>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IOfficerPageType {
  __typename?: 'OfficerPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IOfficer>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IOperationResultType {
  __typename?: 'OperationResultType';
  /** Сообщение об ошибке */
  errorMessages?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** Результат операции */
  success?: Maybe<Scalars['Boolean']>;
}

export type IOrderByItemType = {
  /** Направление сортировки */
  direction?: Maybe<ISearchDirection>;
  /** Имя поля для сортировки */
  field: Scalars['String'];
};

export interface IOrganizationType {
  __typename?: 'OrganizationType';
  /** Адреса */
  addresses?: Maybe<Array<Maybe<IAddressType>>>;
  /**
   * Территориальная принадлежность ОГВ (только для государственных организаций,
   * код по справочнику «Субъекты Российской федерации» (ССРФ)
   */
  agencyTerRang?: Maybe<Scalars['String']>;
  /** тип ОГВ */
  agencyType?: Maybe<IAgencyType>;
  /** Идентификаторы руководителей организации (Policy = ManageUsersOrModerateOrganizations) */
  chiefIds?: Maybe<Array<Scalars['Long']>>;
  /** Email */
  email?: Maybe<Scalars['String']>;
  /** Email подтвержден */
  emailVerified?: Maybe<Scalars['Boolean']>;
  /** Полное наименование */
  fullName?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id: Scalars['Long'];
  /** ИНН */
  inn?: Maybe<Scalars['String']>;
  /** Признак филиала */
  isBrunch: Scalars['Boolean'];
  /** КПП */
  kpp?: Maybe<Scalars['String']>;
  /** Код организационно-правовой формы по общероссийскому классификатору организационно-правовых форм */
  leg?: Maybe<Scalars['String']>;
  /** ОГРН */
  ogrn?: Maybe<Scalars['String']>;
  /** Головная организация (Policy = ManageUsersOrModerateOrganizations) */
  parent?: Maybe<Array<Maybe<IOrganizationType>>>;
  /** Телефоны */
  phones?: Maybe<Array<Maybe<IPhoneType>>>;
  /** Краткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Идентификаторы сотрудников организации (Policy = ManageUsersOrModerateOrganizations) */
  userIds?: Maybe<Array<Scalars['Long']>>;
}

export interface IPToken {
  __typename?: 'PToken';
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  deviceType: Scalars['String'];
  /** Дата протухания объекта */
  expiredAt?: Maybe<Scalars['DateTime']>;
  /** Id объекта */
  id?: Maybe<Scalars['ID']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  value: Scalars['String'];
}

export interface IPTokenPageType {
  __typename?: 'PTokenPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IPToken>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export type IPhoneInputType = {
  /** Добавочный номер (Policy = LoginSystemForm) */
  additional?: Maybe<Scalars['String']>;
  /** Признак подтвержденного номера (Policy = Manage) */
  confirmed?: Maybe<Scalars['Boolean']>;
  /** Идентификатор объекта (Policy = LoginSystemForm) */
  id?: Maybe<Scalars['Long']>;
  /** Номер телефона (Policy = LoginSystemForm) */
  number: Scalars['String'];
  /** Тип телефона (Policy = LoginSystemForm) */
  type?: Maybe<ITypePhone>;
};

export interface IPhoneType {
  __typename?: 'PhoneType';
  /** Добавочный номер */
  additional?: Maybe<Scalars['String']>;
  /** Признак подтвержденного номера */
  confirmed: Scalars['Boolean'];
  /** Идентификатор объекта */
  id: Scalars['Long'];
  /** Номер телефона */
  number: Scalars['String'];
  /** Тип телефона */
  type?: Maybe<ITypePhone>;
}

export interface IProtocolRecord {
  __typename?: 'ProtocolRecord';
  action?: Maybe<Scalars['String']>;
  appeal?: Maybe<IAppeal>;
  appealExecutor?: Maybe<IAppealExecutor>;
  appointment?: Maybe<IAppointment>;
  appointmentNotification?: Maybe<IAppointmentNotification>;
  appointmentRange?: Maybe<IAppointmentRange>;
  attachedFile?: Maybe<IAttachedFile>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  data?: Maybe<Scalars['String']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  message?: Maybe<IMessage>;
  officer?: Maybe<IOfficer>;
  region?: Maybe<IRegion>;
  rubric?: Maybe<IRubric>;
  subsystem?: Maybe<ISubsystem>;
  territory?: Maybe<ITerritory>;
  topic?: Maybe<ITopic>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  user?: Maybe<IUser>;
  vote?: Maybe<IVote>;
  voteItem?: Maybe<IVoteItem>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IProtocolRecordPageType {
  __typename?: 'ProtocolRecordPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IProtocolRecord>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IQuery {
  __typename?: 'Query';
  /** Системная информация серверной части. Версии. */
  apiInformation?: Maybe<IApiInformation>;
  /** Получение списка обращений */
  appeal?: Maybe<IAppealPageType>;
  /** Получение списка исполнителей */
  appealExecutor?: Maybe<IAppealExecutorPageType>;
  /** Получение списка сообщений обратной связи */
  appealFB?: Maybe<IAppealPageType>;
  /**
   * Получение комментариев к обращению в виде массива
   * @deprecated Используйте обобщенный метод messageList
   */
  appealMessageList?: Maybe<IMessagePageType>;
  /** Получение статистики */
  appealStats?: Maybe<IAppealStatsPageType>;
  /** Получение списка обращений с указанным исполнителем */
  appealWhereExecutor?: Maybe<IAppealPageType>;
  /** Получение списка обращений с указанным модератором */
  appealWhereModerator?: Maybe<IAppealPageType>;
  /** Получение статистики по рубрикаторам и датам */
  appealsStatBy?: Maybe<IAppealsStatByPageType>;
  appointment?: Maybe<IAppointmentPageType>;
  appointmentNotification?: Maybe<IAppointmentNotificationPageType>;
  appointmentRange?: Maybe<IAppointmentRangePageType>;
  /** Получение списка присоединенных файлов */
  attachedFile?: Maybe<IAttachedFilePageType>;
  /** Получение списка исполнителей по подсистемам (Policy = ManageUsersOrModerate) */
  availableExecutors?: Maybe<IUserPageType>;
  /** Получение списка модераторов по подсистемам (Policy = ManageUsersOrModerate) */
  availableModerators?: Maybe<IUserPageType>;
  /** Получение информации о доступных разрешениях (Policy = ManageUsersOrRoles) */
  claimsInfo?: Maybe<Array<Maybe<IClaimInfoType>>>;
  /** Подтверждение электронной почты */
  confirmEmail?: Maybe<IOperationResultType>;
  /** Отправлка СМС подтверждения телефонного номера (Policy = LoginSystemForm) */
  confirmPhone?: Maybe<IOperationResultType>;
  /** Получение текущего пользователя (Policy = Authorized) */
  currentUser?: Maybe<IUser>;
  /** Запрос организаций текущего пользователя (Policy = SubsystemOrganizations) */
  currentUserOrganization?: Maybe<IUserOrganizationPageType>;
  /** Получение списка шаблонов Email (Policy = ManageSettings) */
  emailTemplate?: Maybe<IEmailTemplatePageType>;
  /** Получение списка записей */
  enumVerb?: Maybe<IEnumVerbPageType>;
  /** Получить отчет (Policy = ManageReports) */
  executeReport?: Maybe<IExecuteReportResult>;
  /** Скачивание файла с сервера */
  fileDownload?: Maybe<IFileDownload>;
  /** Поиск обращений */
  ftsAppeal?: Maybe<IAppealPageType>;
  /** Запрос журнала изменений (Policy = ManageJournal) */
  journal?: Maybe<IChangeJournalItemPageType>;
  /** Операции журнала изменений (Policy = ManageJournal) */
  journalOperations?: Maybe<Array<Maybe<IJournalOperationsType>>>;
  /** Тэги журнала изменений (Policy = ManageJournal) */
  journalTags?: Maybe<Array<Maybe<IJournalEntityTagsType>>>;
  /** Получение токена авторизации */
  login?: Maybe<ILoginResultType>;
  /** Запрос мнемоник доступных внешних систем авторизации */
  loginModes?: Maybe<Array<Maybe<ILoginSystem>>>;
  /** Запрос адреса страницы авторизации */
  loginRedirectUrl?: Maybe<IRedirectUrl>;
  /** Запрос адреса страницы авторизации */
  logoutRedirectUrl?: Maybe<IRedirectUrl>;
  /** Получение списка сообщений/комментариев */
  message?: Maybe<IMessagePageType>;
  /** Получение выбранного сообщения и ответов на него в виде массива */
  messageList?: Maybe<IMessagePageType>;
  /** Получение всех подписок на комментарии пользователя */
  messageSubscribes?: Maybe<ISubs2DueMessagesPageType>;
  /** Получение списка новостей */
  news?: Maybe<INewsPageType>;
  officer?: Maybe<IOfficerPageType>;
  /** Получение списка токенов (Policy = Authorized) */
  pToken?: Maybe<IPTokenPageType>;
  /**  (Policy = Authorized) */
  protocolRecord?: Maybe<IProtocolRecordPageType>;
  /** получение списка Регионов */
  region?: Maybe<IRegionPageType>;
  /** Пользовательские сохраненные параметры (Policy = ManageReports) */
  reportSavedSettings?: Maybe<Array<Maybe<IReportSavedSettings>>>;
  /** Настройки отчета (Policy = ManageReports) */
  reportSettings?: Maybe<Array<Maybe<IReportSettings>>>;
  /** Восстановление забытого пароля */
  restorePassword?: Maybe<IOperationResultType>;
  /** Запрос ролей (Policy = ManageUsersOrRoles) */
  role?: Maybe<IRolePageType>;
  /** получение списка Рубрик */
  rubric?: Maybe<IRubricPageType>;
  /** Поиск обращений */
  searchAppeal?: Maybe<IAppealPageType>;
  /** Поиск сообщений/комментариев */
  searchMessage?: Maybe<IMessagePageType>;
  /** Поиск новостей */
  searchNews?: Maybe<INewsPageType>;
  /** Послать электронное письмо подтверждения Email (Policy = Authorized) */
  sendConfirmEmail?: Maybe<IOperationResultType>;
  settings?: Maybe<Array<Maybe<ISettings>>>;
  /** Запрос баланса СМС сервиса (Policy = ManageSettings) */
  smsBalance?: Maybe<IBalance>;
  /** Получение списка и параметров подсистем */
  subsystem?: Maybe<ISubsystemPageType>;
  /** получение списка Территорий */
  territory?: Maybe<ITerritoryPageType>;
  /** получение списка Топиков */
  topic?: Maybe<ITopicPageType>;
  /** Обновление токена авторизации (Policy = Authorized) */
  updateToken?: Maybe<ILoginResultType>;
  /** Получение списка пользователей (Policy = ManageUsersOrModerate) */
  user?: Maybe<IUserPageType>;
  /** Запрос организаций текущего пользователя (Policy = ManageUsersOrModerateOrganizations) */
  userOrganization?: Maybe<IUserOrganizationPageType>;
  vote?: Maybe<IVotePageType>;
  voteItem?: Maybe<IVoteItemPageType>;
  /**
   * Получение комментариев к голосованию в виде массива
   * @deprecated Используйте обобщенный метод messageList
   */
  voteMessageList?: Maybe<IMessagePageType>;
  voteRoot?: Maybe<IVoteRootPageType>;
}


export type IQueryAppealArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryAppealExecutorArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryAppealFbArgs = {
  email?: Maybe<Scalars['String']>;
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryAppealMessageListArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  id: Scalars['Long'];
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryAppealStatsArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryAppealWhereExecutorArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  id?: Maybe<Scalars['ID']>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
  subsystemUIDs?: Maybe<Array<Maybe<Scalars['String']>>>;
};


export type IQueryAppealWhereModeratorArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  id?: Maybe<Scalars['ID']>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
  subsystemUIDs?: Maybe<Array<Maybe<Scalars['String']>>>;
};


export type IQueryAppealsStatByArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  offsetHours?: Maybe<Scalars['Int']>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryAppointmentArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryAppointmentNotificationArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryAppointmentRangeArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryAttachedFileArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryAvailableExecutorsArgs = {
  range?: Maybe<IRange>;
  subsystemUid?: Maybe<Array<Maybe<Scalars['String']>>>;
};


export type IQueryAvailableModeratorsArgs = {
  range?: Maybe<IRange>;
  subsystemUid?: Maybe<Array<Maybe<Scalars['String']>>>;
};


export type IQueryConfirmEmailArgs = {
  confirmEmail: IConfirmEmailType;
};


export type IQueryConfirmPhoneArgs = {
  phoneId: Scalars['ID'];
};


export type IQueryCurrentUserOrganizationArgs = {
  range?: Maybe<IRange>;
};


export type IQueryEmailTemplateArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  range?: Maybe<IRange>;
};


export type IQueryEnumVerbArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryExecuteReportArgs = {
  reportSettings: IReportSettingsInput;
};


export type IQueryFileDownloadArgs = {
  file: IFileDownloadRequest;
};


export type IQueryFtsAppealArgs = {
  options?: Maybe<IQueryOptions>;
  query?: Maybe<Scalars['String']>;
  range?: Maybe<IRangeSearch>;
};


export type IQueryJournalArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  range?: Maybe<IRange>;
};


export type IQueryLoginArgs = {
  login: ILoginType;
};


export type IQueryLoginRedirectUrlArgs = {
  redirectUrl: IRedirectInputUrl;
};


export type IQueryLogoutRedirectUrlArgs = {
  redirectUrl: IRedirectInputUrl;
};


export type IQueryMessageArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryMessageListArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  id: Scalars['Long'];
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
  type: Scalars['String'];
};


export type IQueryMessageSubscribesArgs = {
  options?: Maybe<IQueryOptions>;
};


export type IQueryNewsArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryOfficerArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryPTokenArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
  userId?: Maybe<Scalars['ID']>;
};


export type IQueryProtocolRecordArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryRegionArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryReportSavedSettingsArgs = {
  reportId: Scalars['Guid'];
};


export type IQueryReportSettingsArgs = {
  reportId?: Maybe<Scalars['Guid']>;
};


export type IQueryRestorePasswordArgs = {
  restorePassword: IForgotPasswordType;
};


export type IQueryRoleArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  range?: Maybe<IRange>;
};


export type IQueryRubricArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQuerySearchAppealArgs = {
  options?: Maybe<IQueryOptions>;
  query?: Maybe<Scalars['String']>;
  range?: Maybe<IRangeSearch>;
};


export type IQuerySearchMessageArgs = {
  options?: Maybe<IQueryOptions>;
  query?: Maybe<Scalars['String']>;
  range?: Maybe<IRangeSearch>;
};


export type IQuerySearchNewsArgs = {
  options?: Maybe<IQueryOptions>;
  query?: Maybe<Scalars['String']>;
  range?: Maybe<IRangeSearch>;
};


export type IQuerySendConfirmEmailArgs = {
  id?: Maybe<Scalars['ID']>;
};


export type IQuerySettingsArgs = {
  key?: Maybe<Array<Maybe<Scalars['String']>>>;
};


export type IQuerySubsystemArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryTerritoryArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryTopicArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryUserArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  range?: Maybe<IRange>;
};


export type IQueryUserOrganizationArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  range?: Maybe<IRange>;
};


export type IQueryVoteArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryVoteItemArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryVoteMessageListArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  id: Scalars['Long'];
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};


export type IQueryVoteRootArgs = {
  filter?: Maybe<Array<Maybe<IFilter>>>;
  options?: Maybe<IQueryOptions>;
  range?: Maybe<IRange>;
};

export type IQueryOptions = {
  /** Фильтр */
  filters?: Maybe<Array<Maybe<IFilter>>>;
  /** Сортировка и пагинация */
  range?: Maybe<IRange>;
  /** Формат миниатюры */
  thumbFormat?: Maybe<ISkEncodedImageFormat>;
  /** Желаемый размер миниатюры в аттачах, например 100x100 */
  thumbSize?: Maybe<Scalars['String']>;
  /** Допустимое отклонение размера миниатюры в аттачах, например 20%x15%, 10x10 */
  thumbSizeTolerance?: Maybe<Scalars['String']>;
  /** Включить удаленные */
  withDeleted?: Maybe<Scalars['Boolean']>;
  /** Включить связанные через организации пользователя */
  withLinkedByOrganizations?: Maybe<Scalars['Boolean']>;
};

export type IRange = {
  /** Массив полей для сортировки */
  orderBy?: Maybe<Array<Maybe<IOrderByItemType>>>;
  /** Сколько элементов пропустить с начала */
  skip?: Maybe<Scalars['Int']>;
  /** Сколько элементов вернуть */
  take?: Maybe<Scalars['Int']>;
};

export type IRangeSearch = {
  /** Сколько элементов пропустить с начала */
  skip?: Maybe<Scalars['Int']>;
  /** Сколько элементов вернуть */
  take?: Maybe<Scalars['Int']>;
};

export type IRedirectInputUrl = {
  /** Мнемоника системы авторизации */
  loginSystem?: Maybe<Scalars['String']>;
  /** URL перенаправления */
  url: Scalars['String'];
};

export interface IRedirectUrl {
  __typename?: 'RedirectUrl';
  /** Мнемоника системы авторизации */
  loginSystem?: Maybe<Scalars['String']>;
  /** URL перенаправления */
  url: Scalars['String'];
}

export enum IRegardModeEnum {
  Anyuser = 'ANYUSER',
  Appealauthor = 'APPEALAUTHOR',
  Disabled = 'DISABLED'
}

export interface IRegardReport {
  __typename?: 'RegardReport';
  /** Оценка */
  regard: Scalars['Int'];
  /** Сколько юзеров оценили */
  regardCount: Scalars['Int'];
}

export type IRegardRequest = {
  /** Id объекта */
  id?: Maybe<Scalars['ID']>;
  /** Оценка */
  regard: Scalars['Int'];
  /** Тип объекта */
  type?: Maybe<Scalars['String']>;
};

export interface IRegardWir {
  __typename?: 'RegardWIR';
  /** Оценка */
  regard: Scalars['Int'];
}

export interface IRegion {
  __typename?: 'Region';
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Ключ для упрощения доступа к иерархическим объектам */
  due?: Maybe<Scalars['String']>;
  /** Исполнители */
  executors?: Maybe<Array<Maybe<IUser>>>;
  /** Исполнители по подсистемам */
  executorsOfSubsystems?: Maybe<Array<Maybe<IUserOfSubsystem>>>;
  /** Идентификатор во внешней системе */
  extId?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Является папкой */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Уровень */
  layer: Scalars['Int'];
  /** Модераторы */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemType>>>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя */
  parentId?: Maybe<Scalars['ID']>;
  /** Наименование типа родителя */
  parentType?: Maybe<Scalars['String']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Подсистемы */
  subsystems?: Maybe<Array<Maybe<ISubsystem>>>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IRegionPageType {
  __typename?: 'RegionPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IRegion>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export type IRegisterModelType = {
  /** Клаймы */
  claims?: Maybe<Array<Maybe<IClaimInputType>>>;
  /** Подтверждение пароля */
  confirmPassword?: Maybe<Scalars['String']>;
  /** Признак отключенного пользователя */
  disconnected?: Maybe<Scalars['Boolean']>;
  /** Адрес электронной почты */
  email?: Maybe<Scalars['String']>;
  /** Признак подтвержденности электронной почты */
  emailConfirmed?: Maybe<Scalars['Boolean']>;
  /** Полное Имя */
  fullName?: Maybe<Scalars['String']>;
  /** Пол */
  gender?: Maybe<IGender>;
  /** Пароль */
  password: Scalars['String'];
  /** Телефоны */
  phones?: Maybe<Array<Maybe<IPhoneInputType>>>;
  /** Идентификаторы ролей */
  roleIds?: Maybe<Array<Maybe<Scalars['BigInt']>>>;
  /** Логин */
  userName: Scalars['String'];
};

export interface IReportSavedSettings {
  __typename?: 'ReportSavedSettings';
  /** Идентификатор набора */
  id?: Maybe<Scalars['ID']>;
  /** Название набора */
  name: Scalars['String'];
  /** Параметры */
  parameters?: Maybe<Array<Maybe<IReportValuesSingleSetting>>>;
}

export type IReportSavedSettingsInput = {
  /** Идентификатор набора */
  id?: Maybe<Scalars['ID']>;
  /** Название набора */
  name: Scalars['String'];
  /** Параметры */
  parameters?: Maybe<Array<Maybe<IReportValuesSingleSettingInput>>>;
};

export interface IReportSettings {
  __typename?: 'ReportSettings';
  /** Описание отчета */
  description?: Maybe<Scalars['String']>;
  /** Название отчета */
  name?: Maybe<Scalars['String']>;
  /** Параметры отчета */
  parameters?: Maybe<Array<Maybe<IReportSingleSettings>>>;
  /** Идентификатор отчета */
  reportId?: Maybe<Scalars['Guid']>;
}

export enum IReportSettingsControlType {
  CustomText = 'CUSTOM_TEXT',
  DateRange = 'DATE_RANGE',
  DiscreteSelector = 'DISCRETE_SELECTOR',
  ListSelect = 'LIST_SELECT',
  NumberRange = 'NUMBER_RANGE',
  SingleCheckbox = 'SINGLE_CHECKBOX',
  TreeSelect = 'TREE_SELECT',
  Undefined = 'UNDEFINED'
}

export type IReportSettingsInput = {
  /** Параметры отчета */
  parameters?: Maybe<Array<Maybe<IReportSingleSettingsInput>>>;
  /** Идентификатор отчета */
  reportId?: Maybe<Scalars['Guid']>;
  /** Пользовательское смещение времени */
  timeOffset?: Maybe<Scalars['Int']>;
};

export interface IReportSingleSettings {
  __typename?: 'ReportSingleSettings';
  /** Отображать чекбокс "Все" */
  allEnable?: Maybe<Scalars['Boolean']>;
  /** Состояние чекбокса "Все" */
  allSelected?: Maybe<Scalars['Boolean']>;
  /** Тип элемента управления */
  controlType?: Maybe<IReportSettingsControlType>;
  /** Имя параметра */
  name: Scalars['String'];
  /** Дополнительные параметры */
  options?: Maybe<Scalars['String']>;
  /** Признак обязательного параметра */
  required?: Maybe<Scalars['Boolean']>;
  /** Отображаемое имя параметра */
  title: Scalars['String'];
  /** Установленное начение */
  value?: Maybe<Scalars['String']>;
  /** Возможные значения параметра */
  values?: Maybe<Scalars['String']>;
}

export type IReportSingleSettingsInput = {
  /** Отображать чекбокс "Все" */
  allEnable?: Maybe<Scalars['Boolean']>;
  /** Состояние чекбокса "Все" */
  allSelected?: Maybe<Scalars['Boolean']>;
  /** Имя параметра */
  name?: Maybe<Scalars['String']>;
  /** Дополнительные параметры */
  options?: Maybe<Scalars['String']>;
  /** Установленное начение */
  value?: Maybe<Scalars['String']>;
};

export interface IReportValuesSingleSetting {
  __typename?: 'ReportValuesSingleSetting';
  /** Состояние чекбокса "Все" */
  allSelected?: Maybe<Scalars['Boolean']>;
  /** Имя параметра */
  name: Scalars['String'];
  /** Дополнительные параметры */
  options?: Maybe<Scalars['String']>;
  /** Установленное начение */
  value?: Maybe<Scalars['String']>;
}

export type IReportValuesSingleSettingInput = {
  /** Состояние чекбокса "Все" */
  allSelected?: Maybe<Scalars['Boolean']>;
  /** Имя параметра */
  name: Scalars['String'];
  /** Дополнительные параметры */
  options?: Maybe<Scalars['String']>;
  /** Установленное начение */
  value?: Maybe<Scalars['String']>;
};

export type IResetPasswordInputType = {
  /** Повторение нового пароля */
  confirmPassword?: Maybe<Scalars['String']>;
  /** Идентификатор пользователя */
  id?: Maybe<Scalars['ID']>;
  /** Новый пароль */
  newPassword: Scalars['String'];
  /** Старый пароль */
  oldPassword?: Maybe<Scalars['String']>;
};

export type IRestorePasswordType = {
  /** Код восстановления пароля */
  code: Scalars['String'];
  /** Подтверждение пароля */
  confirmPassword: Scalars['String'];
  /** Новый пароль */
  password: Scalars['String'];
  /** Идентификатор пользователя */
  userId: Scalars['Long'];
};

export enum IRightLevel {
  AdministratorSettings = 'ADMINISTRATOR_SETTINGS',
  Authorized = 'AUTHORIZED',
  Manager = 'MANAGER',
  Unauthorized = 'UNAUTHORIZED',
  Undefined = 'UNDEFINED'
}

export type IRoleInputType = {
  /** Разрешения */
  claims?: Maybe<Array<Maybe<IClaimInputType>>>;
  /** Описание роли */
  description?: Maybe<Scalars['String']>;
  /** Признак отключения роли */
  disconnected?: Maybe<Scalars['Boolean']>;
  /** Название роли */
  name?: Maybe<Scalars['String']>;
};

export interface IRolePageType {
  __typename?: 'RolePageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IRoleType>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IRoleSortedType {
  __typename?: 'RoleSortedType';
  /** Идентификатор роли */
  id?: Maybe<Scalars['ID']>;
  /** Признак сортироваки */
  weight: Scalars['Int'];
}

export type IRoleSortingType = {
  /** Идентификатор роли */
  id?: Maybe<Scalars['ID']>;
  /** Признак сортироваки */
  weight: Scalars['Int'];
};

export interface IRoleType {
  __typename?: 'RoleType';
  /** Разрешения */
  claims?: Maybe<Array<Maybe<IClaimType>>>;
  /** Дата создания (UTC) */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Описание роли */
  description?: Maybe<Scalars['String']>;
  /** Признак блокированной роли */
  disconnected: Scalars['Boolean'];
  /** Дата блокирования (UTC) */
  disconnectedTimestamp?: Maybe<Scalars['DateTime']>;
  /** Возможность редактирования */
  editable: Scalars['Boolean'];
  /** Идентификатор роли */
  id?: Maybe<Scalars['ID']>;
  /** Возможность назначать создаваемым пользователям */
  isAdminRegister: Scalars['Boolean'];
  /** Название роли */
  name: Scalars['String'];
  /** Дата редактирования (UTC) */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Пользователи включенные в роль */
  users?: Maybe<Array<Maybe<IUser>>>;
  /** Признак сортироваки */
  weight: Scalars['Int'];
}

export interface IRubric {
  __typename?: 'Rubric';
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Описание */
  description?: Maybe<Scalars['String']>;
  /** Группа документов для ЮЛ */
  docGroupForUL?: Maybe<Scalars['String']>;
  /** Группа документов */
  docgroup?: Maybe<Scalars['String']>;
  /** Ключ для упрощения доступа к иерархическим объектам */
  due?: Maybe<Scalars['String']>;
  /** Исполнители */
  executors?: Maybe<Array<Maybe<IUser>>>;
  /** Исполнители по подсистемам */
  executorsOfSubsystems?: Maybe<Array<Maybe<IUserOfSubsystem>>>;
  /** Идентификатор во внешней системе */
  extId?: Maybe<Scalars['String']>;
  /** --- */
  extType?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Является папкой */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Уровень */
  layer: Scalars['Int'];
  /** Модераторы */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemType>>>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя */
  parentId?: Maybe<Scalars['ID']>;
  /** Наименование типа родителя */
  parentType?: Maybe<Scalars['String']>;
  /** --- */
  receivers?: Maybe<Scalars['String']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Подсистемы */
  subsystems?: Maybe<Array<Maybe<ISubsystem>>>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Запись на прием */
  useForPA?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IRubricPageType {
  __typename?: 'RubricPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IRubric>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}


export enum ISearchDirection {
  Asc = 'ASC',
  Desc = 'DESC'
}


export enum ISettingNodeType {
  Group = 'GROUP',
  None = 'NONE',
  TabGroup = 'TAB_GROUP',
  Value = 'VALUE'
}

export enum ISettingType {
  File = 'FILE',
  SecretFile = 'SECRET_FILE',
  Undefined = 'UNDEFINED',
  Value = 'VALUE',
  ValueAndFile = 'VALUE_AND_FILE',
  ValueAndSecretFile = 'VALUE_AND_SECRET_FILE'
}

/** Получение настроек */
export interface ISettings {
  __typename?: 'Settings';
  /** Присоединенные файлы */
  attachedFiles?: Maybe<Array<Maybe<IAttachedFile>>>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Описание настройки */
  description?: Maybe<Scalars['String']>;
  /** Ключ */
  key?: Maybe<Scalars['ID']>;
  /** Требуемый уровень доступа по чтению */
  readingRightLevel?: Maybe<IRightLevel>;
  /** Тип узла настройки */
  settingNodeType?: Maybe<ISettingNodeType>;
  /** Тип настройки */
  settingType?: Maybe<ISettingType>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Значение */
  value?: Maybe<Scalars['String']>;
  /** Тип значения настройки */
  valueType?: Maybe<Scalars['String']>;
  /** Возможные значения */
  values?: Maybe<Array<Maybe<ICreateSettingsValueItem>>>;
  /** Вес (признак сортировки) */
  weight: Scalars['Int'];
}


export enum ISkEncodedImageFormat {
  Astc = 'ASTC',
  Bmp = 'BMP',
  Dng = 'DNG',
  Gif = 'GIF',
  Heif = 'HEIF',
  Ico = 'ICO',
  Jpeg = 'JPEG',
  Ktx = 'KTX',
  Pkm = 'PKM',
  Png = 'PNG',
  Wbmp = 'WBMP',
  Webp = 'WEBP'
}

export enum IStatusEnum {
  Draft = 'DRAFT',
  InProgress = 'IN_PROGRESS',
  IsArchived = 'IS_ARCHIVED',
  IsDone = 'IS_DONE',
  IsPlanned = 'IS_PLANNED'
}

export interface ISubs2DueMessagesPageType {
  __typename?: 'Subs2DueMessagesPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IMessageSubscribeGet_>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export enum ISubscribeToAnswersEnum {
  NotSubscribed = 'NOT_SUBSCRIBED',
  Subscribed = 'SUBSCRIBED'
}

/** Подписки на всякие события */
export interface ISubscriptions {
  __typename?: 'Subscriptions';
  /** Подписка на авторизацию */
  login?: Maybe<ILoginResultType>;
  /** Подписка на обновление комментариев */
  newMessage?: Maybe<IMessage>;
  /** Подписка на обновление настроек */
  newSettings?: Maybe<ISettings>;
}


/** Подписки на всякие события */
export type ISubscriptionsLoginArgs = {
  login: ILoginType;
};


/** Подписки на всякие события */
export type ISubscriptionsNewMessageArgs = {
  subscribe?: Maybe<IMessageSubscribeSet>;
};


/** Подписки на всякие события */
export type ISubscriptionsNewSettingsArgs = {
  in?: Maybe<Array<Maybe<Scalars['String']>>>;
  startsWith?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export interface ISubsystem {
  __typename?: 'Subsystem';
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Описание подсистемы */
  description?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** ? */
  isPublicDefault: Scalars['Boolean'];
  /** Поля основного объекта подсистемы */
  mainEntityFields?: Maybe<Array<Maybe<ISubsystemMainEntityField>>>;
  /** Имя API основного объекта подсистемы */
  mainEntityName?: Maybe<Scalars['String']>;
  /** Модератор подсистемы */
  moderator?: Maybe<IUser>;
  /** Печатное наименование подсистемы */
  name?: Maybe<Scalars['String']>;
  /** ? */
  privateOnly: Scalars['Boolean'];
  /** Список регионов подсистемы */
  regions?: Maybe<Array<Maybe<IRegion>>>;
  /** Список рубрик подсистемы */
  rubrics?: Maybe<Array<Maybe<IRubric>>>;
  /** Список территорий подсистемы */
  territories?: Maybe<Array<Maybe<ITerritory>>>;
  /** Список топиков подсистемы */
  topics?: Maybe<Array<Maybe<ITopic>>>;
  /** Уникальное наименование подсистемы */
  uID?: Maybe<Scalars['ID']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** ? */
  useAttaches: Scalars['Boolean'];
  /** ? */
  useComments: Scalars['Boolean'];
  /** ? */
  useDislikes: Scalars['Boolean'];
  /** ? */
  useInMobileClient?: Maybe<Scalars['Boolean']>;
  /** ? */
  useLikesForComments: Scalars['Boolean'];
  /** ? */
  useLikesForMain: Scalars['Boolean'];
  /** ? */
  useMaps: Scalars['Boolean'];
  /** ? */
  useRates: Scalars['Boolean'];
  /** ? */
  useRegionsDescription: Scalars['Boolean'];
  /** ? */
  useRubricsDescription: Scalars['Boolean'];
  /** ? */
  useTerritoriesDescription: Scalars['Boolean'];
  /** ? */
  useTopicsDescription?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface ISubsystemMainEntityField {
  __typename?: 'SubsystemMainEntityField';
  /** Описание поля для пользователя */
  description?: Maybe<Scalars['String']>;
  /** Имя поля объекта в подсистеме */
  nameAPI: Scalars['String'];
  /** имя другого поля, содержимое которого выводим если основное не заполнено */
  nameAPISecond?: Maybe<Scalars['String']>;
  /** Имя поля объекта в для пользователя */
  nameDisplayed?: Maybe<Scalars['String']>;
  /** выводим это если поле не заполнено */
  placeholder?: Maybe<Scalars['String']>;
  /** обязательно к заполнению при создании объекта */
  required?: Maybe<Scalars['Boolean']>;
  /** подсистема */
  subsystem?: Maybe<ISubsystem>;
  /** вес для сортировки */
  weight?: Maybe<Scalars['Int']>;
}

export interface ISubsystemPageType {
  __typename?: 'SubsystemPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<ISubsystem>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface ITerritory {
  __typename?: 'Territory';
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Ключ для упрощения доступа к иерархическим объектам */
  due?: Maybe<Scalars['String']>;
  /** Исполнители */
  executors?: Maybe<Array<Maybe<IUser>>>;
  /** Исполнители по подсистемам */
  executorsOfSubsystems?: Maybe<Array<Maybe<IUserOfSubsystem>>>;
  /** Идентификатор во внешней системе */
  extId?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Является папкой */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Уровень */
  layer: Scalars['Int'];
  /** Модераторы */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemType>>>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя */
  parentId?: Maybe<Scalars['ID']>;
  /** Наименование типа родителя */
  parentType?: Maybe<Scalars['String']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Подсистемы */
  subsystems?: Maybe<Array<Maybe<ISubsystem>>>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface ITerritoryPageType {
  __typename?: 'TerritoryPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<ITerritory>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface ITopic {
  __typename?: 'Topic';
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Ключ для упрощения доступа к иерархическим объектам */
  due?: Maybe<Scalars['String']>;
  /** Исполнители */
  executors?: Maybe<Array<Maybe<IUser>>>;
  /** Исполнители по подсистемам */
  executorsOfSubsystems?: Maybe<Array<Maybe<IUserOfSubsystem>>>;
  /** Идентификатор во внешней системе */
  extId?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Является папкой */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Уровень */
  layer: Scalars['Int'];
  /** Модераторы */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemType>>>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя */
  parentId?: Maybe<Scalars['ID']>;
  /** Наименование типа родителя */
  parentType?: Maybe<Scalars['String']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /**
   * Подсистема
   * @deprecated Теперь в 'Subsystems'
   */
  subsystem?: Maybe<ISubsystem>;
  /** Подсистемы */
  subsystems?: Maybe<Array<Maybe<ISubsystem>>>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface ITopicPageType {
  __typename?: 'TopicPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<ITopic>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export enum ITypePhone {
  Fax = 'FAX',
  Mobile = 'MOBILE',
  Phone = 'PHONE',
  Undefined = 'UNDEFINED',
  Work = 'WORK'
}





export interface IUser {
  __typename?: 'User';
  /** Телефоны */
  addresses?: Maybe<Array<Maybe<IAddressType>>>;
  /** Сводный список разрешений */
  allClaims?: Maybe<Array<Maybe<IClaimType>>>;
  /** Разрешения (Policy = Manage) */
  claims?: Maybe<Array<Maybe<IClaimType>>>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Признак блокировки */
  disconnected: Scalars['Boolean'];
  /** Штамп времени блокировки (Policy = ManageUsersOrModerate) */
  disconnectedTimestamp?: Maybe<Scalars['DateTime']>;
  /** Адрес электронной почты */
  email?: Maybe<Scalars['String']>;
  /** Признак подтвержденного адреса электронной почты */
  emailConfirmed?: Maybe<Scalars['Boolean']>;
  /** Идентификатор пользователя во внешней системе (Policy = Manage) */
  externalId?: Maybe<Scalars['String']>;
  /** Имя */
  firstName?: Maybe<Scalars['String']>;
  /** Полное Имя */
  fullName?: Maybe<Scalars['String']>;
  /** Пол */
  gender?: Maybe<IGender>;
  /** Идентификатор объекта */
  id: Scalars['Long'];
  /** ИНН */
  inn?: Maybe<Scalars['String']>;
  /** Фамилия */
  lastName?: Maybe<Scalars['String']>;
  /** Система логирования (Policy = ManageUsersOrModerate) */
  loginSystem?: Maybe<ILoginSystem>;
  /** Отчество */
  middleName?: Maybe<Scalars['String']>;
  /** Идентификатор пользователя в системе из которой осуществен перенос (Policy = Manage) */
  oldSystemId?: Maybe<Scalars['String']>;
  /** Организации (Policy = ManageUsersOrModerateOrganizationsOrSubsystemOrganization) */
  organizations?: Maybe<Array<Maybe<IOrganizationType>>>;
  /** Телефоны */
  phones?: Maybe<Array<Maybe<IPhoneType>>>;
  /** Признак подтвержденного профиля */
  profileConfirmed?: Maybe<Scalars['Boolean']>;
  /** Роли (Policy = Manage) */
  roles?: Maybe<Array<Maybe<IRoleType>>>;
  /** Признак пользователя зарегистрировавшегося через формы */
  selfFormRegister?: Maybe<Scalars['Boolean']>;
  /** СНИЛС */
  snils?: Maybe<Scalars['String']>;
  /** Дата изменения объекта (Policy = Manage) */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Документы */
  userIdentityDocuments?: Maybe<Array<Maybe<IUserIdentityDocumentsType>>>;
  /** Логин */
  userName?: Maybe<Scalars['String']>;
}

export interface IUserIdentityDocumentsType {
  __typename?: 'UserIdentityDocumentsType';
  /** Cрок действия документа */
  expiryDate?: Maybe<Scalars['DateTime']>;
  /** Идентификатор объекта */
  id: Scalars['Long'];
  /** Дата выдачи */
  issueDate?: Maybe<Scalars['DateTime']>;
  /** Код подразделения */
  issueId?: Maybe<Scalars['String']>;
  /** Кем выдан */
  issuedBy?: Maybe<Scalars['String']>;
  /** Номер документа */
  number?: Maybe<Scalars['String']>;
  /** Серия документа */
  series?: Maybe<Scalars['String']>;
  /** Тип документа */
  type?: Maybe<IDocType>;
  /** Подтвержден */
  verified: Scalars['Boolean'];
}

export type IUserInputType = {
  /** Клаймы (Policy = Manage) */
  claims?: Maybe<Array<Maybe<IClaimInputType>>>;
  /** Признак отключенного пользователя (Policy = Manage) */
  disconnected?: Maybe<Scalars['Boolean']>;
  /** Адрес электронной почты (Policy = LoginSystemForm) */
  email?: Maybe<Scalars['String']>;
  /** Признак подтвержденности электронной почты (Policy = Manage) */
  emailConfirmed?: Maybe<Scalars['Boolean']>;
  /** Идентификатор пользователя во внешней системе (Policy = Manage) */
  externalId?: Maybe<Scalars['String']>;
  /** Полное Имя (Policy = LoginSystemForm) */
  fullName?: Maybe<Scalars['String']>;
  /** Пол (Policy = LoginSystemForm) */
  gender?: Maybe<IGender>;
  /** Идентификатор пользователя в системе из которой осуществен перенос (Policy = Manage) */
  oldSystemId?: Maybe<Scalars['String']>;
  /** Телефоны (Policy = LoginSystemForm) */
  phones?: Maybe<Array<Maybe<IPhoneInputType>>>;
  /** Идентификаторы ролей (Policy = Manage) */
  roleIds?: Maybe<Array<Maybe<Scalars['BigInt']>>>;
  /** Логин (Policy = LoginSystemForm) */
  userName?: Maybe<Scalars['String']>;
};

export interface IUserOfSubsystem {
  __typename?: 'UserOfSubsystem';
  /** Подсистема */
  subsystem?: Maybe<ISubsystem>;
  /** Пользователь */
  user?: Maybe<IUser>;
}

export interface IUserOrganizationPageType {
  __typename?: 'UserOrganizationPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IOrganizationType>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IUserPageType {
  __typename?: 'UserPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IUser>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IViewReport {
  __typename?: 'ViewReport';
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Сколько юзеров просмотрели */
  viewCount: Scalars['Int'];
  /** Просмотрено */
  viewed: Scalars['Boolean'];
}

export type IViewRequest = {
  /** Id объекта */
  id?: Maybe<Scalars['ID']>;
  /** Тип объекта */
  type?: Maybe<Scalars['String']>;
  /** Просмотрено */
  viewed: Scalars['Boolean'];
};

export interface IViewWiv {
  __typename?: 'ViewWIV';
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Я видел */
  viewed: Scalars['Boolean'];
}

export interface IVote {
  __typename?: 'Vote';
  /** Присоединенные файлы */
  attachedFiles?: Maybe<Array<Maybe<IAttachedFile>>>;
  /** Пользователи могут оставлять комментарии */
  commentsAllowed?: Maybe<Scalars['Boolean']>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Ответы */
  messages?: Maybe<Array<Maybe<IMessage>>>;
  /** Режим голосования ONE_SELECT: можно голосовать за 1 пункт, MANY_SELECT - за несколько пунктов */
  mode?: Maybe<IModeEnum>;
  /** Максимальное число пунктов в режиме MANY_SELECT */
  modeLimitValue?: Maybe<Scalars['Int']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Можно предложить свой вариант */
  offerEnabled?: Maybe<Scalars['Boolean']>;
  /** Индивидуальные предложения */
  offers?: Maybe<Array<Maybe<IVoteOffer>>>;
  /** Сколько пользователей внесли свои предложения */
  offersCount: Scalars['Int'];
  /** Пункты голосования */
  parentVoteItems?: Maybe<Array<Maybe<IVoteItem>>>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Описание голосования */
  text?: Maybe<Scalars['String']>;
  /** Описание итогов голосования */
  textResult?: Maybe<Scalars['String']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Голосовавшие пользователи */
  users?: Maybe<Array<Maybe<IUser>>>;
  /** Сколько пользователей голосовало */
  usersCount: Scalars['Int'];
  /** Пункты голосования */
  voteItems?: Maybe<Array<Maybe<IVoteItem>>>;
  /** Корень опроса */
  voteRoot?: Maybe<IVoteRoot>;
  /** Как я проголосовал */
  voteWIV?: Maybe<IVoteWiv>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export type IVoteAction = {
  /** Текст индивидуального предложения */
  comment?: Maybe<Scalars['String']>;
  /** Id голосования */
  voteId?: Maybe<Scalars['ID']>;
  /** Массив Id выбранных пунктов голосования */
  voteItemIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Массив номеров выбранных пунктов голосования */
  voteItemNumbers?: Maybe<Array<Maybe<Scalars['Int']>>>;
};

export interface IVoteItem {
  __typename?: 'VoteItem';
  /** Присоединенные файлы */
  attachedFiles?: Maybe<Array<Maybe<IAttachedFile>>>;
  /** Сколько пользователей проголосовало за данный пункт */
  counter: Scalars['Int'];
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Пространное описание */
  description?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Голосование, (следующий узел) */
  nextVote?: Maybe<IVote>;
  /** Порядковый номер пункта */
  number?: Maybe<Scalars['Int']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Голосование, (текущий узел) */
  vote?: Maybe<IVote>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IVoteItemPageType {
  __typename?: 'VoteItemPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IVoteItem>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IVoteOffer {
  __typename?: 'VoteOffer';
  /** Текст индивидуального предложения */
  text?: Maybe<Scalars['String']>;
  /** Кто внёс предложение */
  user?: Maybe<IUser>;
}

export interface IVotePageType {
  __typename?: 'VotePageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IVote>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IVoteRoot {
  __typename?: 'VoteRoot';
  /** Когда архивируется */
  archiveAt?: Maybe<Scalars['DateTime']>;
  /** Когда заархивировалось */
  archivedAt?: Maybe<Scalars['DateTime']>;
  /** Присоединенные файлы */
  attachedFiles?: Maybe<Array<Maybe<IAttachedFile>>>;
  /** Пользователи могут оставлять комментарии */
  commentsAllowed?: Maybe<Scalars['Boolean']>;
  /** Дата создания объекта */
  createdAt?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Когда завершится */
  endAt?: Maybe<Scalars['DateTime']>;
  /** Когда завершилось */
  endedAt?: Maybe<Scalars['DateTime']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  /** Ответы */
  messages?: Maybe<Array<Maybe<IMessage>>>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Id Region */
  region?: Maybe<IRegion>;
  /** Id Rubric */
  rubric?: Maybe<IRubric>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Когда начнется */
  startAt?: Maybe<Scalars['DateTime']>;
  /** Когда началось */
  startedAt?: Maybe<Scalars['DateTime']>;
  /** Текущее состояние (БУДЕТ/ИДЕТ/ЗАКОНЧИЛОСЬ) */
  state?: Maybe<IStatusEnum>;
  /** Id Territory */
  territory?: Maybe<ITerritory>;
  /** Описание голосования */
  text?: Maybe<Scalars['String']>;
  /** Описание итогов голосования */
  textResult?: Maybe<Scalars['String']>;
  /** Id Topic */
  topic?: Maybe<ITopic>;
  /** Дата изменения объекта */
  updatedAt?: Maybe<Scalars['DateTime']>;
  /** Сколько пользователей голосовало */
  usersCount: Scalars['Int'];
  /** Узлы голосования */
  votes?: Maybe<Array<Maybe<IVote>>>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
}

export interface IVoteRootPageType {
  __typename?: 'VoteRootPageType';
  /** Элементы коллекции */
  items?: Maybe<Array<Maybe<IVoteRoot>>>;
  /** Размер выборки */
  total: Scalars['Long'];
}

export interface IVoteWiv {
  __typename?: 'VoteWIV';
  /** Текст индивидуального предложения */
  text?: Maybe<Scalars['String']>;
  /** Список Id пунктов голосования */
  voteItemsIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Голосование имело место быть */
  voted: Scalars['Boolean'];
}

/** Принятие к публикации или отклонение обращения */
export type IApproveAppeal = {
  /** Принято к публикации/отказано */
  approvedEnum?: Maybe<IApprovedEnum>;
  /** Стадии модерации */
  moderationStage?: Maybe<IModerationStageEnum>;
  /** Публичное */
  public_?: Maybe<Scalars['Boolean']>;
  /** Причина отказа в публикации */
  rejectionReason?: Maybe<Scalars['String']>;
};

/** Принятие к публикации или отклонение приложенного файла */
export type IApproveAttachedFile = {
  /** Публичный */
  public_?: Maybe<Scalars['Boolean']>;
};

/** Принятие к публикации или отклонение комментария */
export type IApproveMessage = {
  /** Принято к публикации/отказано */
  approvedEnum?: Maybe<IApprovedEnum>;
  /** Стадии модерации */
  moderationStage?: Maybe<IModerationStageEnum>;
  /** Публичное */
  public_?: Maybe<Scalars['Boolean']>;
  /** Создать комментарий с описанием причины */
  reason?: Maybe<Scalars['String']>;
};

/** Публикация или снятие с публикации новости */
export type IApproveNews = {
  /** Состояние */
  state?: Maybe<INewsStateEnum>;
};

/** Обращение */
export type ICreateAppeal = {
  /** --- */
  address?: Maybe<Scalars['String']>;
  /** Должностное лицо - адресат сообщения */
  addressee?: Maybe<Scalars['String']>;
  /** --- */
  answerByPost?: Maybe<Scalars['Boolean']>;
  /** Id Исполнителей */
  appealExecutorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Исполнителей (+/-) */
  appealExecutorIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Ответственных исполнителей */
  appealPrincipalExecutorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Ответственных исполнителей (+/-) */
  appealPrincipalExecutorIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Тип обращения (обычное/сообщение обратной связи) */
  appealType?: Maybe<IAppealTypeEnum>;
  /** Id Присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** --- */
  authorName?: Maybe<Scalars['String']>;
  /** --- */
  clientType?: Maybe<Scalars['Int']>;
  /** --- */
  collective?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** --- */
  deloStatus?: Maybe<Scalars['String']>;
  /** --- */
  deloText?: Maybe<Scalars['String']>;
  /** --- */
  description?: Maybe<Scalars['String']>;
  /** --- */
  email?: Maybe<Scalars['String']>;
  /** Дата регистрации */
  extDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  extId?: Maybe<Scalars['String']>;
  /** Рег № */
  extNumber?: Maybe<Scalars['String']>;
  /** --- */
  externalExec?: Maybe<Scalars['Boolean']>;
  /** --- */
  factDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  filesPush?: Maybe<Scalars['DateTime']>;
  /** --- */
  firstName?: Maybe<Scalars['String']>;
  /** --- */
  fulltext?: Maybe<Scalars['String']>;
  /** --- */
  inn?: Maybe<Scalars['String']>;
  /** --- */
  lastName?: Maybe<Scalars['String']>;
  /** Место на карте: широта */
  latitude?: Maybe<Scalars['Float']>;
  /** Место на карте: долгота */
  longitude?: Maybe<Scalars['Float']>;
  /** --- */
  markSend?: Maybe<Scalars['Boolean']>;
  /** Id Модераторов */
  moderatorsIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Модераторов (+/-) */
  moderatorsIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** --- */
  municipality?: Maybe<Scalars['String']>;
  /** --- */
  number?: Maybe<Scalars['Int']>;
  /** Идентификатор обращения в системе из которой осуществен перенос */
  oldSystemId?: Maybe<Scalars['String']>;
  /** Id Организации */
  organizationId?: Maybe<Scalars['ID']>;
  /** --- */
  patronim?: Maybe<Scalars['String']>;
  /** --- */
  phone?: Maybe<Scalars['String']>;
  /** Место на карте: описание */
  place?: Maybe<Scalars['String']>;
  /** --- */
  planDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  platform?: Maybe<Scalars['String']>;
  /** Должность */
  post?: Maybe<Scalars['String']>;
  /** Публичность запрошена */
  publicGranted?: Maybe<Scalars['Boolean']>;
  /** --- */
  pushesSent?: Maybe<Scalars['Boolean']>;
  /** Дата регистрации документа */
  regDate?: Maybe<Scalars['DateTime']>;
  /** Регистрационный номер документа в организации/ИП */
  regNumber?: Maybe<Scalars['String']>;
  /** Режим оценки отчетов исполнителей */
  regardMode?: Maybe<IRegardModeEnum>;
  /** --- */
  region1?: Maybe<Scalars['String']>;
  /** Id Региона */
  regionId?: Maybe<Scalars['ID']>;
  /** --- */
  resDate?: Maybe<Scalars['DateTime']>;
  /** Id Рубрики */
  rubricId?: Maybe<Scalars['ID']>;
  /** --- */
  sentToDelo?: Maybe<Scalars['Boolean']>;
  /** Подписал */
  signatory?: Maybe<Scalars['String']>;
  /** --- */
  siteStatus?: Maybe<Scalars['Int']>;
  /** --- */
  statusConfirmed?: Maybe<Scalars['Boolean']>;
  /** Уведомлять об ответах на данное обращение */
  subscribeToAnswers?: Maybe<ISubscribeToAnswersEnum>;
  /** Уникальное наименование подсистемы */
  subsystemId: Scalars['ID'];
  /** Id Территории */
  territoryId?: Maybe<Scalars['ID']>;
  /** --- */
  title?: Maybe<Scalars['String']>;
  /** Id Топика */
  topicId?: Maybe<Scalars['ID']>;
  /** --- */
  topicUpdated?: Maybe<Scalars['Boolean']>;
  /** --- */
  uploaded?: Maybe<Scalars['Boolean']>;
  /** --- */
  views?: Maybe<Scalars['Int']>;
  /** Без номера */
  wORegNumberRegDate?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
  /** --- */
  zip?: Maybe<Scalars['String']>;
};

/** Обращение */
export type ICreateAppealExecutor = {
  /** Id обращений, в которых есть такой исполнитель */
  appealIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id обращений, в которых есть такой исполнитель (+/-) */
  appealIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id обращений, в которых есть такой ответственный исполнитель */
  appealPrincipalIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id обращений, в которых есть такой ответственный исполнитель (+/-) */
  appealPrincipalIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** --- */
  department?: Maybe<Scalars['String']>;
  /** --- */
  departmentId?: Maybe<Scalars['String']>;
  /** Идентификатор во внешней системе */
  extId?: Maybe<Scalars['String']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** --- */
  organization?: Maybe<Scalars['String']>;
  /** --- */
  position?: Maybe<Scalars['String']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Id пользователя */
  userId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateAppointment = {
  address?: Maybe<Scalars['String']>;
  admin?: Maybe<Scalars['Boolean']>;
  answersIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  answersIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  appointmentRangeId?: Maybe<Scalars['ID']>;
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  authorName?: Maybe<Scalars['String']>;
  authorNameUpperCase?: Maybe<Scalars['String']>;
  date?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  description?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  municipality?: Maybe<Scalars['String']>;
  number?: Maybe<Scalars['Int']>;
  officerId?: Maybe<Scalars['ID']>;
  phone?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
  rejectionReason?: Maybe<Scalars['String']>;
  rubricId?: Maybe<Scalars['ID']>;
  status?: Maybe<Scalars['Int']>;
  topicId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
  zip?: Maybe<Scalars['String']>;
};

export type ICreateAppointmentNotification = {
  appointmentId?: Maybe<Scalars['Long']>;
  date?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  html?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['Int']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateAppointmentRange = {
  appointments?: Maybe<Scalars['Int']>;
  date?: Maybe<Scalars['DateTime']>;
  deleteMark?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  end?: Maybe<Scalars['DateTime']>;
  officerId?: Maybe<Scalars['ID']>;
  officerName?: Maybe<Scalars['String']>;
  registeredAppointmentsIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  registeredAppointmentsIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  rubricName?: Maybe<Scalars['String']>;
  rubricOrder?: Maybe<Scalars['Int']>;
  start?: Maybe<Scalars['DateTime']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateAttachedFile = {
  /** Content-Type */
  contentType?: Maybe<Scalars['String']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Внешний идентификатор */
  extId?: Maybe<Scalars['String']>;
  /** Файл публичный */
  isPublic?: Maybe<Scalars['Boolean']>;
  /** Имя файла */
  name?: Maybe<Scalars['String']>;
  /** Путь */
  path?: Maybe<Scalars['String']>;
  /** Файл приватный */
  private?: Maybe<Scalars['Boolean']>;
  /** Без доступа по чтению */
  sequre?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateEnumVerb = {
  customSettings?: Maybe<Scalars['String']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  description?: Maybe<Scalars['String']>;
  group?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  shortName?: Maybe<Scalars['String']>;
  subsystemUID?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

/** Сообщение обратной связи */
export type ICreateFbAppeal = {
  /** --- */
  address?: Maybe<Scalars['String']>;
  /** Должностное лицо - адресат сообщения */
  addressee?: Maybe<Scalars['String']>;
  /** --- */
  answerByPost?: Maybe<Scalars['Boolean']>;
  /** Id Исполнителей */
  appealExecutorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Исполнителей (+/-) */
  appealExecutorIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Ответственных исполнителей */
  appealPrincipalExecutorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Ответственных исполнителей (+/-) */
  appealPrincipalExecutorIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** --- */
  authorName?: Maybe<Scalars['String']>;
  /** --- */
  clientType?: Maybe<Scalars['Int']>;
  /** --- */
  collective?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** --- */
  deloStatus?: Maybe<Scalars['String']>;
  /** --- */
  deloText?: Maybe<Scalars['String']>;
  /** --- */
  description?: Maybe<Scalars['String']>;
  /** --- */
  email?: Maybe<Scalars['String']>;
  /** Дата регистрации */
  extDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  extId?: Maybe<Scalars['String']>;
  /** Рег № */
  extNumber?: Maybe<Scalars['String']>;
  /** --- */
  externalExec?: Maybe<Scalars['Boolean']>;
  /** --- */
  factDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  filesPush?: Maybe<Scalars['DateTime']>;
  /** --- */
  firstName?: Maybe<Scalars['String']>;
  /** --- */
  fulltext?: Maybe<Scalars['String']>;
  /** --- */
  inn?: Maybe<Scalars['String']>;
  /** --- */
  lastName?: Maybe<Scalars['String']>;
  /** Место на карте: широта */
  latitude?: Maybe<Scalars['Float']>;
  /** Место на карте: долгота */
  longitude?: Maybe<Scalars['Float']>;
  /** --- */
  markSend?: Maybe<Scalars['Boolean']>;
  /** Id Модераторов */
  moderatorsIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Модераторов (+/-) */
  moderatorsIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** --- */
  municipality?: Maybe<Scalars['String']>;
  /** --- */
  number?: Maybe<Scalars['Int']>;
  /** Идентификатор обращения в системе из которой осуществен перенос */
  oldSystemId?: Maybe<Scalars['String']>;
  /** Id Организации */
  organizationId?: Maybe<Scalars['ID']>;
  /** --- */
  patronim?: Maybe<Scalars['String']>;
  /** --- */
  phone?: Maybe<Scalars['String']>;
  /** Место на карте: описание */
  place?: Maybe<Scalars['String']>;
  /** --- */
  planDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  platform?: Maybe<Scalars['String']>;
  /** Должность */
  post?: Maybe<Scalars['String']>;
  /** Публичность запрошена */
  publicGranted?: Maybe<Scalars['Boolean']>;
  /** --- */
  pushesSent?: Maybe<Scalars['Boolean']>;
  /** Дата регистрации документа */
  regDate?: Maybe<Scalars['DateTime']>;
  /** Регистрационный номер документа в организации/ИП */
  regNumber?: Maybe<Scalars['String']>;
  /** Режим оценки отчетов исполнителей */
  regardMode?: Maybe<IRegardModeEnum>;
  /** --- */
  region1?: Maybe<Scalars['String']>;
  /** Id Региона */
  regionId?: Maybe<Scalars['ID']>;
  /** --- */
  resDate?: Maybe<Scalars['DateTime']>;
  /** Id Рубрики */
  rubricId?: Maybe<Scalars['ID']>;
  /** --- */
  sentToDelo?: Maybe<Scalars['Boolean']>;
  /** Подписал */
  signatory?: Maybe<Scalars['String']>;
  /** --- */
  siteStatus?: Maybe<Scalars['Int']>;
  /** --- */
  statusConfirmed?: Maybe<Scalars['Boolean']>;
  /** Уведомлять об ответах на данное обращение */
  subscribeToAnswers?: Maybe<ISubscribeToAnswersEnum>;
  /** Уникальное наименование подсистемы */
  subsystemId: Scalars['ID'];
  /** Id Территории */
  territoryId?: Maybe<Scalars['ID']>;
  /** --- */
  title?: Maybe<Scalars['String']>;
  /** Id Топика */
  topicId?: Maybe<Scalars['ID']>;
  /** --- */
  topicUpdated?: Maybe<Scalars['Boolean']>;
  /** --- */
  uploaded?: Maybe<Scalars['Boolean']>;
  /** --- */
  views?: Maybe<Scalars['Int']>;
  /** Без номера */
  wORegNumberRegDate?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
  /** --- */
  zip?: Maybe<Scalars['String']>;
};

/** Сообщение/комментарий */
export type ICreateMessage = {
  /** Исполнитель */
  appealExecutorId?: Maybe<Scalars['ID']>;
  /** Принято к публикации/отказано */
  approved?: Maybe<IApprovedEnum>;
  /** Id присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Является папкой */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Вид сообщения */
  kind?: Maybe<IMessageKindEnum>;
  /** Идентификатор родителя */
  parentId?: Maybe<Scalars['ID']>;
  /** Наименование типа родителя */
  parentType?: Maybe<Scalars['String']>;
  /** Уведомлять об ответах на данное сообщение */
  subscribeToAnswers?: Maybe<ISubscribeToAnswersEnum>;
  /** Текст сообщения */
  text?: Maybe<Scalars['String']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

/** Обращение */
export type ICreateNews = {
  /** Id Присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Пользователи могут оставлять комментарии */
  commentsAllowed?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Id Региона */
  regionId?: Maybe<Scalars['ID']>;
  /** Id Рубрики */
  rubricId?: Maybe<Scalars['ID']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Краткий текст новости / начало текста новости */
  shortText?: Maybe<Scalars['String']>;
  /** Id Территории */
  territoryId?: Maybe<Scalars['ID']>;
  /** Содержимое новости */
  text?: Maybe<Scalars['String']>;
  /** Id Топика */
  topicId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateOfficer = {
  authority?: Maybe<Scalars['String']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  extId?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  place?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  rangesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  rangesIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  rubricId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreatePToken = {
  deviceType: Scalars['String'];
  /** Дата протухания объекта */
  expiredAt?: Maybe<Scalars['DateTime']>;
  value: Scalars['String'];
};

export type ICreateRegion = {
  /** Удален (Policy = ManageRegions) */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Id исполнителей (Policy = ManageRegionsSubsystem) */
  executorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем для задания исполнителей (Policy = ManageRegionsSubsystem) */
  executorIdsSubSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Идентификатор во внешней системе (Policy = ManageRegions) */
  extId?: Maybe<Scalars['String']>;
  /** Является папкой (Policy = ManageRegions) */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Mодераторы (Policy = ManageRegionsSubsystem) */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemInputType>>>;
  /** Наименование (Policy = ManageRegions) */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя (Policy = ManageRegions) */
  parentId?: Maybe<Scalars['ID']>;
  /** Наименование типа родителя (Policy = ManageRegions) */
  parentType?: Maybe<Scalars['String']>;
  /** Короткое наименование (Policy = ManageRegions) */
  shortName?: Maybe<Scalars['String']>;
  /** UID подсистем (Policy = ManageRegionsSubsystem) */
  subSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Для особой сортировки (Policy = ManageRegions) */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateRubric = {
  /** Удален (Policy = ManageRubrics) */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Описание (Policy = ManageRubrics) */
  description?: Maybe<Scalars['String']>;
  /** Группа документов для ЮЛ (Policy = ManageRubrics) */
  docGroupForUL?: Maybe<Scalars['String']>;
  /** Группа документов (Policy = ManageRubrics) */
  docgroup?: Maybe<Scalars['String']>;
  /** Id исполнителей (Policy = ManageRubricsSubsystem) */
  executorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем для задания исполнителей (Policy = ManageRubricsSubsystem) */
  executorIdsSubSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Идентификатор во внешней системе (Policy = ManageRubrics) */
  extId?: Maybe<Scalars['String']>;
  /** --- (Policy = ManageRubrics) */
  extType?: Maybe<Scalars['String']>;
  /** Является папкой (Policy = ManageRubrics) */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Mодераторы (Policy = ManageRubricsSubsystem) */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemInputType>>>;
  /** Наименование (Policy = ManageRubrics) */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя (Policy = ManageRubrics) */
  parentId?: Maybe<Scalars['ID']>;
  /** Наименование типа родителя (Policy = ManageRubrics) */
  parentType?: Maybe<Scalars['String']>;
  /** --- (Policy = ManageRubrics) */
  receivers?: Maybe<Scalars['String']>;
  /** Короткое наименование (Policy = ManageRubrics) */
  shortName?: Maybe<Scalars['String']>;
  /** UID подсистем (Policy = ManageRubricsSubsystem) */
  subSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Запись на прием (Policy = ManageRubrics) */
  useForPA?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки (Policy = ManageRubrics) */
  weight?: Maybe<Scalars['Long']>;
};

export interface ICreateSettingsValueItem {
  __typename?: 'createSettingsValueItem';
  /** Отображаемый текст */
  displayText: Scalars['String'];
  /** Значение параметра */
  value?: Maybe<Scalars['String']>;
}

export type ICreateSubsystem = {
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Описание подсистемы */
  description?: Maybe<Scalars['String']>;
  /** ? */
  isPublicDefault?: Maybe<Scalars['Boolean']>;
  /** Поля основного объекта подсистемы */
  mainEntityFields?: Maybe<Array<Maybe<ICreateSubsystemMainEntityField>>>;
  /** Имя API основного объекта подсистемы */
  mainEntityName?: Maybe<Scalars['String']>;
  /** Id модератора подсистемы */
  moderatorId?: Maybe<Scalars['ID']>;
  /** Печатное наименование подсистемы */
  name?: Maybe<Scalars['String']>;
  /** ? */
  privateOnly?: Maybe<Scalars['Boolean']>;
  /** Id регионов */
  regionIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id рубрик */
  rubricIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id территорий */
  territoryIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id топиков */
  topicIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Уникальное наименование подсистемы */
  uID?: Maybe<Scalars['ID']>;
  /** ? */
  useAttaches?: Maybe<Scalars['Boolean']>;
  /** ? */
  useComments?: Maybe<Scalars['Boolean']>;
  /** ? */
  useDislikes?: Maybe<Scalars['Boolean']>;
  /** ? */
  useInMobileClient?: Maybe<Scalars['Boolean']>;
  /** ? */
  useLikesForComments?: Maybe<Scalars['Boolean']>;
  /** ? */
  useLikesForMain?: Maybe<Scalars['Boolean']>;
  /** ? */
  useMaps?: Maybe<Scalars['Boolean']>;
  /** ? */
  useRates?: Maybe<Scalars['Boolean']>;
  /** ? */
  useRegionsDescription?: Maybe<Scalars['Boolean']>;
  /** ? */
  useRubricsDescription?: Maybe<Scalars['Boolean']>;
  /** ? */
  useTerritoriesDescription?: Maybe<Scalars['Boolean']>;
  /** ? */
  useTopicsDescription?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateSubsystemMainEntityField = {
  /** Описание поля для пользователя */
  description?: Maybe<Scalars['String']>;
  /** Имя поля объекта в подсистеме */
  nameAPI: Scalars['String'];
  /** имя другого поля, содержимое которого выводим если основное не заполнено */
  nameAPISecond?: Maybe<Scalars['String']>;
  /** Имя поля объекта в для пользователя */
  nameDisplayed?: Maybe<Scalars['String']>;
  /** выводим это если поле не заполнено */
  placeholder?: Maybe<Scalars['String']>;
  /** обязательно к заполнению при создании объекта */
  required?: Maybe<Scalars['Boolean']>;
  /** вес для сортировки */
  weight?: Maybe<Scalars['Int']>;
};

export type ICreateTerritory = {
  /** Удален (Policy = ManageTerritories) */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Id исполнителей (Policy = ManageTerritoriesSubsystem) */
  executorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем для задания исполнителей (Policy = ManageTerritoriesSubsystem) */
  executorIdsSubSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Идентификатор во внешней системе (Policy = ManageTerritories) */
  extId?: Maybe<Scalars['String']>;
  /** Является папкой (Policy = ManageTerritories) */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Mодераторы (Policy = ManageTerritoriesSubsystem) */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemInputType>>>;
  /** Наименование (Policy = ManageTerritories) */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя (Policy = ManageTerritories) */
  parentId?: Maybe<Scalars['ID']>;
  /** Наименование типа родителя (Policy = ManageTerritories) */
  parentType?: Maybe<Scalars['String']>;
  /** Короткое наименование (Policy = ManageTerritories) */
  shortName?: Maybe<Scalars['String']>;
  /** UID подсистем (Policy = ManageTerritoriesSubsystem) */
  subSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Для особой сортировки (Policy = ManageTerritories) */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateTopic = {
  /** Удален (Policy = ManageTopics) */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Id исполнителей (Policy = ManageTopicsSubsystem) */
  executorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем для задания исполнителей (Policy = ManageTopicsSubsystem) */
  executorIdsSubSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Идентификатор во внешней системе (Policy = ManageTopics) */
  extId?: Maybe<Scalars['String']>;
  /** Является папкой (Policy = ManageTopics) */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Mодераторы (Policy = ManageTopicsSubsystem) */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemInputType>>>;
  /** Наименование (Policy = ManageTopics) */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя (Policy = ManageTopics) */
  parentId?: Maybe<Scalars['ID']>;
  /** Наименование типа родителя (Policy = ManageTopics) */
  parentType?: Maybe<Scalars['String']>;
  /** Короткое наименование (Policy = ManageTopics) */
  shortName?: Maybe<Scalars['String']>;
  /** UID подсистем (Policy = ManageTopicsSubsystem) */
  subSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Уникальное наименование подсистемы (Policy = ManageTopicsSubsystem) */
  subsystemId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки (Policy = ManageTopics) */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateVote = {
  /** Id присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Пользователи могут оставлять комментарии */
  commentsAllowed?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Режим голосования ONE_SELECT: можно голосовать за 1 пункт, MANY_SELECT - за несколько пунктов */
  mode?: Maybe<IModeEnum>;
  /** Максимальное число пунктов в режиме MANY_SELECT */
  modeLimitValue?: Maybe<Scalars['Int']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Можно предложить свой вариант */
  offerEnabled?: Maybe<Scalars['Boolean']>;
  /** Родительские пункты многоузлового голосования */
  parentVoteItemsIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Описание голосования */
  text?: Maybe<Scalars['String']>;
  /** Описание итогов голосования */
  textResult?: Maybe<Scalars['String']>;
  /** Пункты голосования */
  voteItems?: Maybe<Array<Maybe<ICreateVoteItem>>>;
  /** Список Id пунктов голосования */
  voteItemsIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id корня */
  voteRootId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateVoteItem = {
  /** Id присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Пространное описание */
  description?: Maybe<Scalars['String']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Id голосования, (следующего узла) */
  nextVoteId?: Maybe<Scalars['ID']>;
  /** Порядковый номер пункта */
  number?: Maybe<Scalars['Int']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Id голосования, (текущий узел) */
  voteId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type ICreateVoteRoot = {
  /** Когда архивируется */
  archiveAt?: Maybe<Scalars['DateTime']>;
  /** Id присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Пользователи могут оставлять комментарии */
  commentsAllowed?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Когда завершится */
  endAt?: Maybe<Scalars['DateTime']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Id Region */
  regionId?: Maybe<Scalars['ID']>;
  /** Id Rubric */
  rubricId?: Maybe<Scalars['ID']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Когда начнется */
  startAt?: Maybe<Scalars['DateTime']>;
  /** Id Territory */
  territoryId?: Maybe<Scalars['ID']>;
  /** Описание голосования */
  text?: Maybe<Scalars['String']>;
  /** Описание итогов голосования */
  textResult?: Maybe<Scalars['String']>;
  /** Id Topic */
  topicId?: Maybe<Scalars['ID']>;
  /** Id узла входа */
  voteEntryId?: Maybe<Scalars['ID']>;
  /** Список Id узлов голосования */
  votesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

/** Удаление комментария */
export type IDeleteMessage = {
  /** Причина удаления */
  reason?: Maybe<Scalars['String']>;
};

export type IDeletePToken = {
  value: Scalars['String'];
};

/** Обращение */
export type IUpdateAppeal = {
  /** --- */
  address?: Maybe<Scalars['String']>;
  /** Должностное лицо - адресат сообщения */
  addressee?: Maybe<Scalars['String']>;
  /** --- */
  answerByPost?: Maybe<Scalars['Boolean']>;
  /** Id Исполнителей */
  appealExecutorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Исполнителей (+/-) */
  appealExecutorIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Ответственных исполнителей */
  appealPrincipalExecutorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Ответственных исполнителей (+/-) */
  appealPrincipalExecutorIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** --- */
  authorName?: Maybe<Scalars['String']>;
  /** --- */
  clientType?: Maybe<Scalars['Int']>;
  /** --- */
  collective?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** --- */
  deloStatus?: Maybe<Scalars['String']>;
  /** --- */
  deloText?: Maybe<Scalars['String']>;
  /** --- */
  description?: Maybe<Scalars['String']>;
  /** --- */
  email?: Maybe<Scalars['String']>;
  /** Дата регистрации */
  extDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  extId?: Maybe<Scalars['String']>;
  /** Рег № */
  extNumber?: Maybe<Scalars['String']>;
  /** --- */
  externalExec?: Maybe<Scalars['Boolean']>;
  /** --- */
  factDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  filesPush?: Maybe<Scalars['DateTime']>;
  /** --- */
  firstName?: Maybe<Scalars['String']>;
  /** --- */
  fulltext?: Maybe<Scalars['String']>;
  /** --- */
  inn?: Maybe<Scalars['String']>;
  /** --- */
  lastName?: Maybe<Scalars['String']>;
  /** Место на карте: широта */
  latitude?: Maybe<Scalars['Float']>;
  /** Место на карте: долгота */
  longitude?: Maybe<Scalars['Float']>;
  /** --- */
  markSend?: Maybe<Scalars['Boolean']>;
  /** Id Модераторов */
  moderatorsIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id Модераторов (+/-) */
  moderatorsIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** --- */
  municipality?: Maybe<Scalars['String']>;
  /** Нужно обновить во внешней системе */
  needToUpdateInExt?: Maybe<Scalars['Boolean']>;
  /** --- */
  number?: Maybe<Scalars['Int']>;
  /** Идентификатор обращения в системе из которой осуществен перенос */
  oldSystemId?: Maybe<Scalars['String']>;
  /** Id Организации */
  organizationId?: Maybe<Scalars['ID']>;
  /** --- */
  patronim?: Maybe<Scalars['String']>;
  /** --- */
  phone?: Maybe<Scalars['String']>;
  /** Место на карте: описание */
  place?: Maybe<Scalars['String']>;
  /** --- */
  planDate?: Maybe<Scalars['DateTime']>;
  /** --- */
  platform?: Maybe<Scalars['String']>;
  /** Должность */
  post?: Maybe<Scalars['String']>;
  /** Публичность запрошена */
  publicGranted?: Maybe<Scalars['Boolean']>;
  /** --- */
  pushesSent?: Maybe<Scalars['Boolean']>;
  /** Дата регистрации документа */
  regDate?: Maybe<Scalars['DateTime']>;
  /** Регистрационный номер документа в организации/ИП */
  regNumber?: Maybe<Scalars['String']>;
  /** Режим оценки отчетов исполнителей */
  regardMode?: Maybe<IRegardModeEnum>;
  /** --- */
  region1?: Maybe<Scalars['String']>;
  /** Id Региона */
  regionId?: Maybe<Scalars['ID']>;
  /** --- */
  resDate?: Maybe<Scalars['DateTime']>;
  /** Id Рубрики */
  rubricId?: Maybe<Scalars['ID']>;
  /** --- */
  sentToDelo?: Maybe<Scalars['Boolean']>;
  /** Подписал */
  signatory?: Maybe<Scalars['String']>;
  /** --- */
  siteStatus?: Maybe<Scalars['Int']>;
  /** --- */
  statusConfirmed?: Maybe<Scalars['Boolean']>;
  /** Уведомлять об ответах на данное обращение */
  subscribeToAnswers?: Maybe<ISubscribeToAnswersEnum>;
  /** Id Территории */
  territoryId?: Maybe<Scalars['ID']>;
  /** --- */
  title?: Maybe<Scalars['String']>;
  /** Id Топика */
  topicId?: Maybe<Scalars['ID']>;
  /** --- */
  topicUpdated?: Maybe<Scalars['Boolean']>;
  /** --- */
  uploaded?: Maybe<Scalars['Boolean']>;
  /** --- */
  views?: Maybe<Scalars['Int']>;
  /** Без номера */
  wORegNumberRegDate?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
  /** --- */
  zip?: Maybe<Scalars['String']>;
};

/** Обращение */
export type IUpdateAppealExecutor = {
  /** Id обращений, в которых есть такой исполнитель */
  appealIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id обращений, в которых есть такой исполнитель (+/-) */
  appealIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id обращений, в которых есть такой ответственный исполнитель */
  appealPrincipalIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id обращений, в которых есть такой ответственный исполнитель (+/-) */
  appealPrincipalIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** --- */
  department?: Maybe<Scalars['String']>;
  /** --- */
  departmentId?: Maybe<Scalars['String']>;
  /** Идентификатор во внешней системе */
  extId?: Maybe<Scalars['String']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** --- */
  organization?: Maybe<Scalars['String']>;
  /** --- */
  position?: Maybe<Scalars['String']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Id пользователя */
  userId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateAppointment = {
  address?: Maybe<Scalars['String']>;
  admin?: Maybe<Scalars['Boolean']>;
  answersIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  answersIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  appointmentRangeId?: Maybe<Scalars['ID']>;
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  authorName?: Maybe<Scalars['String']>;
  authorNameUpperCase?: Maybe<Scalars['String']>;
  date?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  description?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  municipality?: Maybe<Scalars['String']>;
  number?: Maybe<Scalars['Int']>;
  officerId?: Maybe<Scalars['ID']>;
  phone?: Maybe<Scalars['String']>;
  region?: Maybe<Scalars['String']>;
  rejectionReason?: Maybe<Scalars['String']>;
  rubricId?: Maybe<Scalars['ID']>;
  status?: Maybe<Scalars['Int']>;
  topicId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
  zip?: Maybe<Scalars['String']>;
};

export type IUpdateAppointmentNotification = {
  appointmentId?: Maybe<Scalars['Long']>;
  date?: Maybe<Scalars['DateTime']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  html?: Maybe<Scalars['String']>;
  status?: Maybe<Scalars['Int']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateAppointmentRange = {
  appointments?: Maybe<Scalars['Int']>;
  date?: Maybe<Scalars['DateTime']>;
  deleteMark?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  end?: Maybe<Scalars['DateTime']>;
  officerId?: Maybe<Scalars['ID']>;
  officerName?: Maybe<Scalars['String']>;
  registeredAppointmentsIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  registeredAppointmentsIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  rubricName?: Maybe<Scalars['String']>;
  rubricOrder?: Maybe<Scalars['Int']>;
  start?: Maybe<Scalars['DateTime']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateAttachedFile = {
  /** Content-Type */
  contentType?: Maybe<Scalars['String']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Внешний идентификатор */
  extId?: Maybe<Scalars['String']>;
  /** Файл публичный */
  isPublic?: Maybe<Scalars['Boolean']>;
  /** Имя файла */
  name?: Maybe<Scalars['String']>;
  /** Путь */
  path?: Maybe<Scalars['String']>;
  /** Файл приватный */
  private?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateEnumVerb = {
  customSettings?: Maybe<Scalars['String']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  description?: Maybe<Scalars['String']>;
  group?: Maybe<Scalars['String']>;
  /** Идентификатор объекта */
  id?: Maybe<Scalars['ID']>;
  name?: Maybe<Scalars['String']>;
  shortName?: Maybe<Scalars['String']>;
  subsystemUID?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

/** Сообщение/комментарий */
export type IUpdateMessage = {
  /** Id присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Является папкой */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Уведомлять об ответах на данное сообщение */
  subscribeToAnswers?: Maybe<ISubscribeToAnswersEnum>;
  /** Текст сообщения */
  text?: Maybe<Scalars['String']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

/** Новость */
export type IUpdateNews = {
  /** Id Присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Пользователи могут оставлять комментарии */
  commentsAllowed?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Id Региона */
  regionId?: Maybe<Scalars['ID']>;
  /** Id Рубрики */
  rubricId?: Maybe<Scalars['ID']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Краткий текст новости / начало текста новости */
  shortText?: Maybe<Scalars['String']>;
  /** Id Территории */
  territoryId?: Maybe<Scalars['ID']>;
  /** Содержимое новости */
  text?: Maybe<Scalars['String']>;
  /** Id Топика */
  topicId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateOfficer = {
  authority?: Maybe<Scalars['String']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  extId?: Maybe<Scalars['String']>;
  name?: Maybe<Scalars['String']>;
  place?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  rangesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  rangesIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  rubricId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateRegion = {
  /** Удален (Policy = ManageRegions) */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Id исполнителей (Policy = ManageRegionsSubsystem) */
  executorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id исполнителей (+/-) (Policy = ManageRegionsSubsystem) */
  executorIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем для задания исполнителей (Policy = ManageRegionsSubsystem) */
  executorIdsSubSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Идентификатор во внешней системе (Policy = ManageRegions) */
  extId?: Maybe<Scalars['String']>;
  /** Является папкой (Policy = ManageRegions) */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Mодераторы (Policy = ManageRegionsSubsystem) */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemInputType>>>;
  /** Наименование (Policy = ManageRegions) */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя (Policy = ManageRegions) */
  parentId?: Maybe<Scalars['ID']>;
  /** Короткое наименование (Policy = ManageRegions) */
  shortName?: Maybe<Scalars['String']>;
  /** UID подсистем (Policy = ManageRegionsSubsystem) */
  subSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем (+/-) (Policy = ManageRegionsSubsystem) */
  subSystemUIDsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Для особой сортировки (Policy = ManageRegions) */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateRubric = {
  /** Удален (Policy = ManageRubrics) */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Описание (Policy = ManageRubrics) */
  description?: Maybe<Scalars['String']>;
  /** Группа документов для ЮЛ (Policy = ManageRubrics) */
  docGroupForUL?: Maybe<Scalars['String']>;
  /** Группа документов (Policy = ManageRubrics) */
  docgroup?: Maybe<Scalars['String']>;
  /** Id исполнителей (Policy = ManageRubricsSubsystem) */
  executorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем для задания исполнителей (Policy = ManageRubricsSubsystem) */
  executorIdsSubSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Идентификатор во внешней системе (Policy = ManageRubrics) */
  extId?: Maybe<Scalars['String']>;
  /** --- (Policy = ManageRubrics) */
  extType?: Maybe<Scalars['String']>;
  /** Является папкой (Policy = ManageRubrics) */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Mодераторы (Policy = ManageRubricsSubsystem) */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemInputType>>>;
  /** Наименование (Policy = ManageRubrics) */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя (Policy = ManageRubrics) */
  parentId?: Maybe<Scalars['ID']>;
  /** --- (Policy = ManageRubrics) */
  receivers?: Maybe<Scalars['String']>;
  /** Короткое наименование (Policy = ManageRubrics) */
  shortName?: Maybe<Scalars['String']>;
  /** UID подсистем (Policy = ManageRubricsSubsystem) */
  subSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем (+/-) (Policy = ManageRubricsSubsystem) */
  subSystemUIDsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Запись на прием (Policy = ManageRubrics) */
  useForPA?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки (Policy = ManageRubrics) */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateSettings = {
  /** Id присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Ключ */
  key?: Maybe<Scalars['ID']>;
  /** Значение */
  value?: Maybe<Scalars['String']>;
};

export type IUpdateSubsystem = {
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Описание подсистемы */
  description?: Maybe<Scalars['String']>;
  /** ? */
  isPublicDefault?: Maybe<Scalars['Boolean']>;
  /** Поля основного объекта подсистемы */
  mainEntityFields?: Maybe<Array<Maybe<ICreateSubsystemMainEntityField>>>;
  /** Поля основного объекта подсистемы */
  mainEntityFieldsUpdate?: Maybe<Array<Maybe<ICreateSubsystemMainEntityField>>>;
  /** Имя API основного объекта подсистемы */
  mainEntityName?: Maybe<Scalars['String']>;
  /** Id модератора подсистемы */
  moderatorId?: Maybe<Scalars['ID']>;
  /** Печатное наименование подсистемы */
  name?: Maybe<Scalars['String']>;
  /** ? */
  privateOnly?: Maybe<Scalars['Boolean']>;
  /** Id регионов */
  regionIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id регионов (+/-) */
  regionIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id рубрик */
  rubricIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id рубрик (+/-) */
  rubricIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id территорий */
  territoryIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id территорий (+/-) */
  territoryIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id топиков */
  topicIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id топиков (+/-) */
  topicIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** ? */
  useAttaches?: Maybe<Scalars['Boolean']>;
  /** ? */
  useComments?: Maybe<Scalars['Boolean']>;
  /** ? */
  useDislikes?: Maybe<Scalars['Boolean']>;
  /** ? */
  useLikesForComments?: Maybe<Scalars['Boolean']>;
  /** ? */
  useLikesForMain?: Maybe<Scalars['Boolean']>;
  /** ? */
  useMaps?: Maybe<Scalars['Boolean']>;
  /** ? */
  useRates?: Maybe<Scalars['Boolean']>;
  /** ? */
  useRegionsDescription?: Maybe<Scalars['Boolean']>;
  /** ? */
  useRubricsDescription?: Maybe<Scalars['Boolean']>;
  /** ? */
  useTerritoriesDescription?: Maybe<Scalars['Boolean']>;
  /** ? */
  useTopicsDescription?: Maybe<Scalars['Boolean']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateTerritory = {
  /** Удален (Policy = ManageTerritories) */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Id исполнителей (Policy = ManageTerritoriesSubsystem) */
  executorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id исполнителей (+/-) (Policy = ManageTerritoriesSubsystem) */
  executorIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем для задания исполнителей (Policy = ManageTerritoriesSubsystem) */
  executorIdsSubSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Идентификатор во внешней системе (Policy = ManageTerritories) */
  extId?: Maybe<Scalars['String']>;
  /** Является папкой (Policy = ManageTerritories) */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Mодераторы (Policy = ManageTerritoriesSubsystem) */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemInputType>>>;
  /** Наименование (Policy = ManageTerritories) */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя (Policy = ManageTerritories) */
  parentId?: Maybe<Scalars['ID']>;
  /** Короткое наименование (Policy = ManageTerritories) */
  shortName?: Maybe<Scalars['String']>;
  /** UID подсистем (Policy = ManageTerritoriesSubsystem) */
  subSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем (+/-) (Policy = ManageTerritoriesSubsystem) */
  subSystemUIDsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Для особой сортировки (Policy = ManageTerritories) */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateTopic = {
  /** Удален (Policy = ManageTopics) */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Id исполнителей (Policy = ManageTopicsSubsystem) */
  executorIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id исполнителей (+/-) (Policy = ManageTopicsSubsystem) */
  executorIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем для задания исполнителей (Policy = ManageTopicsSubsystem) */
  executorIdsSubSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Идентификатор во внешней системе (Policy = ManageTopics) */
  extId?: Maybe<Scalars['String']>;
  /** Является папкой (Policy = ManageTopics) */
  isNode?: Maybe<Scalars['Boolean']>;
  /** Mодераторы (Policy = ManageTopicsSubsystem) */
  moderators?: Maybe<Array<Maybe<IClerkInSubsystemInputType>>>;
  /** Наименование (Policy = ManageTopics) */
  name?: Maybe<Scalars['String']>;
  /** Идентификатор родителя (Policy = ManageTopics) */
  parentId?: Maybe<Scalars['ID']>;
  /** Короткое наименование (Policy = ManageTopics) */
  shortName?: Maybe<Scalars['String']>;
  /** UID подсистем (Policy = ManageTopicsSubsystem) */
  subSystemUIDs?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** UID подсистем (+/-) (Policy = ManageTopicsSubsystem) */
  subSystemUIDsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Для особой сортировки (Policy = ManageTopics) */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateVote = {
  /** Id присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Пользователи могут оставлять комментарии */
  commentsAllowed?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Режим голосования ONE_SELECT: можно голосовать за 1 пункт, MANY_SELECT - за несколько пунктов */
  mode?: Maybe<IModeEnum>;
  /** Максимальное число пунктов в режиме MANY_SELECT */
  modeLimitValue?: Maybe<Scalars['Int']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Можно предложить свой вариант */
  offerEnabled?: Maybe<Scalars['Boolean']>;
  /** Родительские пункты многоузлового голосования */
  parentVoteItemsIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Родительские пункты многоузлового голосования (+/-) */
  parentVoteItemsIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Описание голосования */
  text?: Maybe<Scalars['String']>;
  /** Описание итогов голосования */
  textResult?: Maybe<Scalars['String']>;
  /** Пункты голосования */
  voteItems?: Maybe<Array<Maybe<IUpdateVoteItem>>>;
  /** Список Id пунктов голосования */
  voteItemsIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Список Id пунктов голосования (+/-) */
  voteItemsIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Id корня */
  voteRootId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateVoteItem = {
  /** Id присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Пространное описание */
  description?: Maybe<Scalars['String']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Id голосования, (следующего узла) */
  nextVoteId?: Maybe<Scalars['ID']>;
  /** Порядковый номер пункта */
  number?: Maybe<Scalars['Int']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Id голосования, (текущий узел) */
  voteId?: Maybe<Scalars['ID']>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type IUpdateVoteRoot = {
  /** Когда архивируется */
  archiveAt?: Maybe<Scalars['DateTime']>;
  /** Id присоединенных файлов */
  attachedFilesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Пользователи могут оставлять комментарии */
  commentsAllowed?: Maybe<Scalars['Boolean']>;
  /** Удален */
  deleted?: Maybe<Scalars['Boolean']>;
  /** Когда завершится */
  endAt?: Maybe<Scalars['DateTime']>;
  /** Наименование */
  name?: Maybe<Scalars['String']>;
  /** Id Region */
  regionId?: Maybe<Scalars['ID']>;
  /** Id Rubric */
  rubricId?: Maybe<Scalars['ID']>;
  /** Короткое наименование */
  shortName?: Maybe<Scalars['String']>;
  /** Когда начнется */
  startAt?: Maybe<Scalars['DateTime']>;
  /** Id Territory */
  territoryId?: Maybe<Scalars['ID']>;
  /** Описание голосования */
  text?: Maybe<Scalars['String']>;
  /** Описание итогов голосования */
  textResult?: Maybe<Scalars['String']>;
  /** Id Topic */
  topicId?: Maybe<Scalars['ID']>;
  /** Id узла входа */
  voteEntryId?: Maybe<Scalars['ID']>;
  /** Список Id узлов голосования */
  votesIds?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Список Id узлов голосования (+/-) */
  votesIdsChange?: Maybe<Array<Maybe<Scalars['ID']>>>;
  /** Для особой сортировки */
  weight?: Maybe<Scalars['Long']>;
};

export type IDeleteRecordMutationVariables = Exact<{
  entity: Scalars['String'];
  id: Scalars['Long'];
}>;


export type IDeleteRecordMutation = { __typename?: 'Mutation', delete: string };

export type IMapPointFragment = { __typename?: 'Appeal', longitude?: Maybe<number>, latitude?: Maybe<number>, place?: Maybe<string> };

export type IAppealPublicFragmentFragment = { __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, description?: Maybe<string>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, views?: Maybe<number>, viewsCount: number, platform?: Maybe<string>, longitude?: Maybe<number>, latitude?: Maybe<number>, place?: Maybe<string>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> };

export type IAppealUlFragmentFragment = { __typename?: 'Appeal', post?: Maybe<string>, signatory?: Maybe<string>, regNumber?: Maybe<string>, regDate?: Maybe<any>, wORegNumberRegDate?: Maybe<boolean> };

export type IAppealModListFragmentFragment = { __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, hasUnapprovedMessages: boolean, publicGranted?: Maybe<boolean>, post?: Maybe<string>, signatory?: Maybe<string>, regNumber?: Maybe<string>, regDate?: Maybe<any>, wORegNumberRegDate?: Maybe<boolean>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string>, addrType?: Maybe<IAddrType> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, organization?: Maybe<{ __typename?: 'OrganizationType', id: any, email?: Maybe<string>, fullName?: Maybe<string>, shortName?: Maybe<string>, ogrn?: Maybe<string>, kpp?: Maybe<string>, inn?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> }>>> }> };

export type IAppealModSmallListFragmentFragment = { __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, hasUnapprovedMessages: boolean, publicGranted?: Maybe<boolean>, oldSystemId?: Maybe<string>, author?: Maybe<{ __typename?: 'User', id: any, fullName?: Maybe<string> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, name?: Maybe<string> }>, appealExecutors?: Maybe<Array<Maybe<{ __typename?: 'AppealExecutor', name?: Maybe<string>, department?: Maybe<string>, position?: Maybe<string>, organization?: Maybe<string> }>>> };

export type IAppealAuthorContactFragment = { __typename?: 'Appeal', address?: Maybe<string>, addressee?: Maybe<string>, firstName?: Maybe<string>, authorName?: Maybe<string>, lastName?: Maybe<string>, email?: Maybe<string>, inn?: Maybe<string>, phone?: Maybe<string>, zip?: Maybe<string> };

export type IAppealModFullFragmentFragment = { __typename?: 'Appeal', description?: Maybe<string>, answerByPost?: Maybe<boolean>, clientType?: Maybe<number>, collective?: Maybe<boolean>, factDate?: Maybe<any>, deloStatus?: Maybe<string>, deloText?: Maybe<string>, externalExec?: Maybe<boolean>, extId?: Maybe<string>, filesPush?: Maybe<any>, markSend?: Maybe<boolean>, municipality?: Maybe<string>, number?: Maybe<number>, patronim?: Maybe<string>, planDate?: Maybe<any>, public_: boolean, publicGranted?: Maybe<boolean>, pushesSent?: Maybe<boolean>, region1?: Maybe<string>, rejectionReason?: Maybe<string>, resDate?: Maybe<any>, sentToDelo?: Maybe<boolean>, statusConfirmed?: Maybe<boolean>, topicUpdated?: Maybe<boolean>, hasUnapprovedMessages: boolean, uploaded?: Maybe<boolean>, oldSystemId?: Maybe<string>, id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, address?: Maybe<string>, addressee?: Maybe<string>, firstName?: Maybe<string>, lastName?: Maybe<string>, email?: Maybe<string>, inn?: Maybe<string>, phone?: Maybe<string>, zip?: Maybe<string>, post?: Maybe<string>, signatory?: Maybe<string>, regNumber?: Maybe<string>, regDate?: Maybe<any>, wORegNumberRegDate?: Maybe<boolean>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string>, createdAt?: Maybe<any>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string>, addrType?: Maybe<IAddrType> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, organization?: Maybe<{ __typename?: 'OrganizationType', id: any, email?: Maybe<string>, fullName?: Maybe<string>, shortName?: Maybe<string>, ogrn?: Maybe<string>, kpp?: Maybe<string>, inn?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> }>>> }> };

export type IGetAppeal_1ListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetAppeal_1ListQuery = { __typename?: 'Query', appeal?: Maybe<{ __typename?: 'AppealPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, hasUnapprovedMessages: boolean, publicGranted?: Maybe<boolean>, post?: Maybe<string>, signatory?: Maybe<string>, regNumber?: Maybe<string>, regDate?: Maybe<any>, wORegNumberRegDate?: Maybe<boolean>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string>, addrType?: Maybe<IAddrType> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, organization?: Maybe<{ __typename?: 'OrganizationType', id: any, email?: Maybe<string>, fullName?: Maybe<string>, shortName?: Maybe<string>, ogrn?: Maybe<string>, kpp?: Maybe<string>, inn?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> }>>> }> }>>> }> };

export type IGetAppealListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetAppealListQuery = { __typename?: 'Query', appeal?: Maybe<{ __typename?: 'AppealPageType', items?: Maybe<Array<Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, hasUnapprovedMessages: boolean, publicGranted?: Maybe<boolean>, oldSystemId?: Maybe<string>, author?: Maybe<{ __typename?: 'User', id: any, fullName?: Maybe<string> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, name?: Maybe<string> }>, appealExecutors?: Maybe<Array<Maybe<{ __typename?: 'AppealExecutor', name?: Maybe<string>, department?: Maybe<string>, position?: Maybe<string>, organization?: Maybe<string> }>>> }>>> }> };

export type IGetAppealListWTotalQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetAppealListWTotalQuery = { __typename?: 'Query', appeal?: Maybe<{ __typename?: 'AppealPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, hasUnapprovedMessages: boolean, publicGranted?: Maybe<boolean>, oldSystemId?: Maybe<string>, author?: Maybe<{ __typename?: 'User', id: any, fullName?: Maybe<string> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, name?: Maybe<string> }>, appealExecutors?: Maybe<Array<Maybe<{ __typename?: 'AppealExecutor', name?: Maybe<string>, department?: Maybe<string>, position?: Maybe<string>, organization?: Maybe<string> }>>> }>>> }> };

export type IGetAppealListCountQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetAppealListCountQuery = { __typename?: 'Query', appeal?: Maybe<{ __typename?: 'AppealPageType', total: any }> };

export type IGetAppealOrgListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetAppealOrgListQuery = { __typename?: 'Query', appeal?: Maybe<{ __typename?: 'AppealPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, hasUnapprovedMessages: boolean, publicGranted?: Maybe<boolean>, post?: Maybe<string>, signatory?: Maybe<string>, regNumber?: Maybe<string>, regDate?: Maybe<any>, wORegNumberRegDate?: Maybe<boolean>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string>, addrType?: Maybe<IAddrType> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, organization?: Maybe<{ __typename?: 'OrganizationType', id: any, email?: Maybe<string>, fullName?: Maybe<string>, shortName?: Maybe<string>, ogrn?: Maybe<string>, kpp?: Maybe<string>, inn?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> }>>> }> }>>> }> };

export type IGetAppealSListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetAppealSListQuery = { __typename?: 'Query', appeal?: Maybe<{ __typename?: 'AppealPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, hasUnapprovedMessages: boolean, publicGranted?: Maybe<boolean>, post?: Maybe<string>, signatory?: Maybe<string>, regNumber?: Maybe<string>, regDate?: Maybe<any>, wORegNumberRegDate?: Maybe<boolean>, subsystem?: Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string>, addrType?: Maybe<IAddrType> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, organization?: Maybe<{ __typename?: 'OrganizationType', id: any, email?: Maybe<string>, fullName?: Maybe<string>, shortName?: Maybe<string>, ogrn?: Maybe<string>, kpp?: Maybe<string>, inn?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> }>>> }> }>>> }> };

export type IGetAppealPublicListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
}>;


export type IGetAppealPublicListQuery = { __typename?: 'Query', appeal?: Maybe<{ __typename?: 'AppealPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, description?: Maybe<string>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, views?: Maybe<number>, viewsCount: number, platform?: Maybe<string>, longitude?: Maybe<number>, latitude?: Maybe<number>, place?: Maybe<string>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }>>> }> };

export type IGetAppealQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetAppealQuery = { __typename?: 'Query', appeal?: Maybe<{ __typename?: 'AppealPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appeal', description?: Maybe<string>, answerByPost?: Maybe<boolean>, clientType?: Maybe<number>, collective?: Maybe<boolean>, factDate?: Maybe<any>, deloStatus?: Maybe<string>, deloText?: Maybe<string>, externalExec?: Maybe<boolean>, extId?: Maybe<string>, filesPush?: Maybe<any>, markSend?: Maybe<boolean>, municipality?: Maybe<string>, number?: Maybe<number>, patronim?: Maybe<string>, planDate?: Maybe<any>, public_: boolean, publicGranted?: Maybe<boolean>, pushesSent?: Maybe<boolean>, region1?: Maybe<string>, rejectionReason?: Maybe<string>, resDate?: Maybe<any>, sentToDelo?: Maybe<boolean>, statusConfirmed?: Maybe<boolean>, topicUpdated?: Maybe<boolean>, hasUnapprovedMessages: boolean, uploaded?: Maybe<boolean>, oldSystemId?: Maybe<string>, id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, address?: Maybe<string>, addressee?: Maybe<string>, firstName?: Maybe<string>, lastName?: Maybe<string>, email?: Maybe<string>, inn?: Maybe<string>, phone?: Maybe<string>, zip?: Maybe<string>, post?: Maybe<string>, signatory?: Maybe<string>, regNumber?: Maybe<string>, regDate?: Maybe<any>, wORegNumberRegDate?: Maybe<boolean>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string>, createdAt?: Maybe<any>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string>, addrType?: Maybe<IAddrType> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, organization?: Maybe<{ __typename?: 'OrganizationType', id: any, email?: Maybe<string>, fullName?: Maybe<string>, shortName?: Maybe<string>, ogrn?: Maybe<string>, kpp?: Maybe<string>, inn?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> }>>> }> }>>> }> };

export type IGetPublicAppealQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetPublicAppealQuery = { __typename?: 'Query', appeal?: Maybe<{ __typename?: 'AppealPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, description?: Maybe<string>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, views?: Maybe<number>, viewsCount: number, platform?: Maybe<string>, longitude?: Maybe<number>, latitude?: Maybe<number>, place?: Maybe<string>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }>>> }> };

export type ICreateAppealMutationVariables = Exact<{
  appeal: ICreateAppeal;
}>;


export type ICreateAppealMutation = { __typename?: 'Mutation', createAppeal?: Maybe<{ __typename?: 'Appeal', description?: Maybe<string>, answerByPost?: Maybe<boolean>, clientType?: Maybe<number>, collective?: Maybe<boolean>, factDate?: Maybe<any>, deloStatus?: Maybe<string>, deloText?: Maybe<string>, externalExec?: Maybe<boolean>, extId?: Maybe<string>, filesPush?: Maybe<any>, markSend?: Maybe<boolean>, municipality?: Maybe<string>, number?: Maybe<number>, patronim?: Maybe<string>, planDate?: Maybe<any>, public_: boolean, publicGranted?: Maybe<boolean>, pushesSent?: Maybe<boolean>, region1?: Maybe<string>, rejectionReason?: Maybe<string>, resDate?: Maybe<any>, sentToDelo?: Maybe<boolean>, statusConfirmed?: Maybe<boolean>, topicUpdated?: Maybe<boolean>, hasUnapprovedMessages: boolean, uploaded?: Maybe<boolean>, oldSystemId?: Maybe<string>, id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, address?: Maybe<string>, addressee?: Maybe<string>, firstName?: Maybe<string>, lastName?: Maybe<string>, email?: Maybe<string>, inn?: Maybe<string>, phone?: Maybe<string>, zip?: Maybe<string>, post?: Maybe<string>, signatory?: Maybe<string>, regNumber?: Maybe<string>, regDate?: Maybe<any>, wORegNumberRegDate?: Maybe<boolean>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string>, createdAt?: Maybe<any>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string>, addrType?: Maybe<IAddrType> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, organization?: Maybe<{ __typename?: 'OrganizationType', id: any, email?: Maybe<string>, fullName?: Maybe<string>, shortName?: Maybe<string>, ogrn?: Maybe<string>, kpp?: Maybe<string>, inn?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> }>>> }> }> };

export type IUpdateAppealMutationVariables = Exact<{
  id: Scalars['ID'];
  appeal: IUpdateAppeal;
}>;


export type IUpdateAppealMutation = { __typename?: 'Mutation', updateAppeal?: Maybe<{ __typename?: 'Appeal', description?: Maybe<string>, answerByPost?: Maybe<boolean>, clientType?: Maybe<number>, collective?: Maybe<boolean>, factDate?: Maybe<any>, deloStatus?: Maybe<string>, deloText?: Maybe<string>, externalExec?: Maybe<boolean>, extId?: Maybe<string>, filesPush?: Maybe<any>, markSend?: Maybe<boolean>, municipality?: Maybe<string>, number?: Maybe<number>, patronim?: Maybe<string>, planDate?: Maybe<any>, public_: boolean, publicGranted?: Maybe<boolean>, pushesSent?: Maybe<boolean>, region1?: Maybe<string>, rejectionReason?: Maybe<string>, resDate?: Maybe<any>, sentToDelo?: Maybe<boolean>, statusConfirmed?: Maybe<boolean>, topicUpdated?: Maybe<boolean>, hasUnapprovedMessages: boolean, uploaded?: Maybe<boolean>, oldSystemId?: Maybe<string>, id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, address?: Maybe<string>, addressee?: Maybe<string>, firstName?: Maybe<string>, lastName?: Maybe<string>, email?: Maybe<string>, inn?: Maybe<string>, phone?: Maybe<string>, zip?: Maybe<string>, post?: Maybe<string>, signatory?: Maybe<string>, regNumber?: Maybe<string>, regDate?: Maybe<any>, wORegNumberRegDate?: Maybe<boolean>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string>, createdAt?: Maybe<any>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string>, addrType?: Maybe<IAddrType> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, organization?: Maybe<{ __typename?: 'OrganizationType', id: any, email?: Maybe<string>, fullName?: Maybe<string>, shortName?: Maybe<string>, ogrn?: Maybe<string>, kpp?: Maybe<string>, inn?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> }>>> }> }> };

export type IApproveAppealMutationVariables = Exact<{
  id: Scalars['ID'];
  approve: IApproveAppeal;
}>;


export type IApproveAppealMutation = { __typename?: 'Mutation', approveAppeal?: Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, approved?: Maybe<IApprovedEnum>, rejectionReason?: Maybe<string> }> };

export type IRegisterAppealMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type IRegisterAppealMutation = { __typename?: 'Mutation', registerAppeal?: Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, approved?: Maybe<IApprovedEnum>, rejectionReason?: Maybe<string>, deloStatus?: Maybe<string> }> };

export type IAppointmentNotificationFragmentFragment = { __typename?: 'AppointmentNotification', createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, html?: Maybe<string>, id?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any> };

export type IAppointmentRangeFullFragmentFragment = { __typename?: 'AppointmentRange', appointments?: Maybe<number>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, deleteMark?: Maybe<boolean>, end?: Maybe<any>, id?: Maybe<string>, officerName?: Maybe<string>, rubricName?: Maybe<string>, rubricOrder?: Maybe<number>, weight?: Maybe<any>, start?: Maybe<any>, updatedAt?: Maybe<any>, officer?: Maybe<{ __typename?: 'Officer', authority?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> }>, registeredAppointments?: Maybe<Array<Maybe<{ __typename?: 'Appointment', address?: Maybe<string>, admin?: Maybe<boolean>, authorName?: Maybe<string>, authorNameUpperCase?: Maybe<string>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, description?: Maybe<string>, email?: Maybe<string>, id?: Maybe<string>, municipality?: Maybe<string>, number?: Maybe<number>, phone?: Maybe<string>, region?: Maybe<string>, rejectionReason?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any>, zip?: Maybe<string>, answers?: Maybe<Array<Maybe<{ __typename?: 'AppointmentNotification', createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, html?: Maybe<string>, id?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any> }>>>, appointmentRange?: Maybe<{ __typename?: 'AppointmentRange', appointments?: Maybe<number>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, deleteMark?: Maybe<boolean>, end?: Maybe<any>, id?: Maybe<string>, officerName?: Maybe<string>, rubricName?: Maybe<string>, rubricOrder?: Maybe<number>, weight?: Maybe<any>, start?: Maybe<any>, updatedAt?: Maybe<any> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, officer?: Maybe<{ __typename?: 'Officer', authority?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }>>> };

export type IPgAppointmentRangeLiteFragmentFragment = { __typename?: 'AppointmentRangePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'AppointmentRange', rubricName?: Maybe<string>, start?: Maybe<any>, end?: Maybe<any>, date?: Maybe<any>, appointments?: Maybe<number>, registeredAppointments?: Maybe<Array<Maybe<{ __typename?: 'Appointment', id?: Maybe<string> }>>>, officer?: Maybe<{ __typename?: 'Officer', id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, authority?: Maybe<string> }> }>>> };

export type IAppointmentRangeLiteFragmentFragment = { __typename?: 'AppointmentRange', rubricName?: Maybe<string>, start?: Maybe<any>, end?: Maybe<any>, date?: Maybe<any>, appointments?: Maybe<number>, registeredAppointments?: Maybe<Array<Maybe<{ __typename?: 'Appointment', id?: Maybe<string> }>>>, officer?: Maybe<{ __typename?: 'Officer', id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, authority?: Maybe<string> }> };

export type IGetAppointmentRangeListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetAppointmentRangeListQuery = { __typename?: 'Query', appointmentRange?: Maybe<{ __typename?: 'AppointmentRangePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'AppointmentRange', rubricName?: Maybe<string>, start?: Maybe<any>, end?: Maybe<any>, date?: Maybe<any>, appointments?: Maybe<number>, registeredAppointments?: Maybe<Array<Maybe<{ __typename?: 'Appointment', id?: Maybe<string> }>>>, officer?: Maybe<{ __typename?: 'Officer', id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, authority?: Maybe<string> }> }>>> }> };

export type IPgAppointmentFullFragmentFragment = { __typename?: 'AppointmentPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appointment', address?: Maybe<string>, admin?: Maybe<boolean>, authorName?: Maybe<string>, authorNameUpperCase?: Maybe<string>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, description?: Maybe<string>, email?: Maybe<string>, id?: Maybe<string>, municipality?: Maybe<string>, number?: Maybe<number>, phone?: Maybe<string>, region?: Maybe<string>, rejectionReason?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any>, zip?: Maybe<string>, answers?: Maybe<Array<Maybe<{ __typename?: 'AppointmentNotification', createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, html?: Maybe<string>, id?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any> }>>>, appointmentRange?: Maybe<{ __typename?: 'AppointmentRange', appointments?: Maybe<number>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, deleteMark?: Maybe<boolean>, end?: Maybe<any>, id?: Maybe<string>, officerName?: Maybe<string>, rubricName?: Maybe<string>, rubricOrder?: Maybe<number>, weight?: Maybe<any>, start?: Maybe<any>, updatedAt?: Maybe<any> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, officer?: Maybe<{ __typename?: 'Officer', authority?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }>>> };

export type IAppointmentFullFragmentFragment = { __typename?: 'Appointment', address?: Maybe<string>, admin?: Maybe<boolean>, authorName?: Maybe<string>, authorNameUpperCase?: Maybe<string>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, description?: Maybe<string>, email?: Maybe<string>, id?: Maybe<string>, municipality?: Maybe<string>, number?: Maybe<number>, phone?: Maybe<string>, region?: Maybe<string>, rejectionReason?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any>, zip?: Maybe<string>, answers?: Maybe<Array<Maybe<{ __typename?: 'AppointmentNotification', createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, html?: Maybe<string>, id?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any> }>>>, appointmentRange?: Maybe<{ __typename?: 'AppointmentRange', appointments?: Maybe<number>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, deleteMark?: Maybe<boolean>, end?: Maybe<any>, id?: Maybe<string>, officerName?: Maybe<string>, rubricName?: Maybe<string>, rubricOrder?: Maybe<number>, weight?: Maybe<any>, start?: Maybe<any>, updatedAt?: Maybe<any> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, officer?: Maybe<{ __typename?: 'Officer', authority?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> };

export type IAppointmentRangeFragmentFragment = { __typename?: 'AppointmentRange', appointments?: Maybe<number>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, deleteMark?: Maybe<boolean>, end?: Maybe<any>, id?: Maybe<string>, officerName?: Maybe<string>, rubricName?: Maybe<string>, rubricOrder?: Maybe<number>, weight?: Maybe<any>, start?: Maybe<any>, updatedAt?: Maybe<any> };

export type IGetAppointmentListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetAppointmentListQuery = { __typename?: 'Query', appointment?: Maybe<{ __typename?: 'AppointmentPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appointment', address?: Maybe<string>, admin?: Maybe<boolean>, authorName?: Maybe<string>, authorNameUpperCase?: Maybe<string>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, description?: Maybe<string>, email?: Maybe<string>, id?: Maybe<string>, municipality?: Maybe<string>, number?: Maybe<number>, phone?: Maybe<string>, region?: Maybe<string>, rejectionReason?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any>, zip?: Maybe<string>, answers?: Maybe<Array<Maybe<{ __typename?: 'AppointmentNotification', createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, html?: Maybe<string>, id?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any> }>>>, appointmentRange?: Maybe<{ __typename?: 'AppointmentRange', appointments?: Maybe<number>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, deleteMark?: Maybe<boolean>, end?: Maybe<any>, id?: Maybe<string>, officerName?: Maybe<string>, rubricName?: Maybe<string>, rubricOrder?: Maybe<number>, weight?: Maybe<any>, start?: Maybe<any>, updatedAt?: Maybe<any> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, officer?: Maybe<{ __typename?: 'Officer', authority?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }>>> }> };

export type IGetAppointmentQueryVariables = Exact<{
  id?: Maybe<Scalars['String']>;
}>;


export type IGetAppointmentQuery = { __typename?: 'Query', appointment?: Maybe<{ __typename?: 'AppointmentPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appointment', address?: Maybe<string>, admin?: Maybe<boolean>, authorName?: Maybe<string>, authorNameUpperCase?: Maybe<string>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, description?: Maybe<string>, email?: Maybe<string>, id?: Maybe<string>, municipality?: Maybe<string>, number?: Maybe<number>, phone?: Maybe<string>, region?: Maybe<string>, rejectionReason?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any>, zip?: Maybe<string>, answers?: Maybe<Array<Maybe<{ __typename?: 'AppointmentNotification', createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, html?: Maybe<string>, id?: Maybe<string>, weight?: Maybe<any>, status?: Maybe<number>, updatedAt?: Maybe<any> }>>>, appointmentRange?: Maybe<{ __typename?: 'AppointmentRange', appointments?: Maybe<number>, createdAt?: Maybe<any>, date?: Maybe<any>, deleted?: Maybe<boolean>, deleteMark?: Maybe<boolean>, end?: Maybe<any>, id?: Maybe<string>, officerName?: Maybe<string>, rubricName?: Maybe<string>, rubricOrder?: Maybe<number>, weight?: Maybe<any>, start?: Maybe<any>, updatedAt?: Maybe<any> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, officer?: Maybe<{ __typename?: 'Officer', authority?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }>>> }> };

export type IAttachedFileFragmentFragment = { __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> };

export type IAttachedFilePublicFragmentFragment = { __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> };

export type IAttachSetIsPublicMutationVariables = Exact<{
  id: Scalars['ID'];
  isPublic: Scalars['Boolean'];
}>;


export type IAttachSetIsPublicMutation = { __typename?: 'Mutation', approveAttachedFile?: Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string>, isPublic?: Maybe<boolean> }> };

export type IClaimFragmentFragment = { __typename?: 'ClaimType', type: string, value: string };

export type IClaimInfoValueTypeFragmentFragment = { __typename?: 'ClaimInfoValueType', description: string, value: string };

export type IClaimsInfoFragmentFragment = { __typename?: 'ClaimInfoType', claimEntity?: Maybe<Array<Maybe<IClaimEntity>>>, claimType: string, description: string, values?: Maybe<Array<Maybe<{ __typename?: 'ClaimInfoValueType', description: string, value: string }>>> };

export type IGetClaimInfosQueryVariables = Exact<{ [key: string]: never; }>;


export type IGetClaimInfosQuery = { __typename?: 'Query', claimsInfo?: Maybe<Array<Maybe<{ __typename?: 'ClaimInfoType', claimEntity?: Maybe<Array<Maybe<IClaimEntity>>>, claimType: string, description: string, values?: Maybe<Array<Maybe<{ __typename?: 'ClaimInfoValueType', description: string, value: string }>>> }>>> };

export type IMessageFragmentFragment = { __typename?: 'Message', id?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, dislikesCount: number, likesCount: number, parentId?: Maybe<string>, parentType?: Maybe<string>, weight?: Maybe<any>, text?: Maybe<string>, due?: Maybe<string>, kind?: Maybe<IMessageKindEnum>, approved?: Maybe<IApprovedEnum>, moderationStage?: Maybe<IModerationStageEnum>, viewsCount: number, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, appealExecutor?: Maybe<{ __typename?: 'AppealExecutor', id?: Maybe<string> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> };

export type ICreateMessageMutationVariables = Exact<{
  message: ICreateMessage;
}>;


export type ICreateMessageMutation = { __typename?: 'Mutation', createMessage?: Maybe<{ __typename?: 'Message', id?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, dislikesCount: number, likesCount: number, parentId?: Maybe<string>, parentType?: Maybe<string>, weight?: Maybe<any>, text?: Maybe<string>, due?: Maybe<string>, kind?: Maybe<IMessageKindEnum>, approved?: Maybe<IApprovedEnum>, moderationStage?: Maybe<IModerationStageEnum>, viewsCount: number, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, appealExecutor?: Maybe<{ __typename?: 'AppealExecutor', id?: Maybe<string> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }> };

export type ICreateMessage2AppealMutationVariables = Exact<{
  parentId: Scalars['ID'];
  text: Scalars['String'];
}>;


export type ICreateMessage2AppealMutation = { __typename?: 'Mutation', createMessage?: Maybe<{ __typename?: 'Message', id?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, dislikesCount: number, likesCount: number, parentId?: Maybe<string>, parentType?: Maybe<string>, weight?: Maybe<any>, text?: Maybe<string>, due?: Maybe<string>, kind?: Maybe<IMessageKindEnum>, approved?: Maybe<IApprovedEnum>, moderationStage?: Maybe<IModerationStageEnum>, viewsCount: number, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, appealExecutor?: Maybe<{ __typename?: 'AppealExecutor', id?: Maybe<string> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }> };

export type ICreateMessage2MessageMutationVariables = Exact<{
  parentId: Scalars['ID'];
  text: Scalars['String'];
}>;


export type ICreateMessage2MessageMutation = { __typename?: 'Mutation', createMessage?: Maybe<{ __typename?: 'Message', id?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, dislikesCount: number, likesCount: number, parentId?: Maybe<string>, parentType?: Maybe<string>, weight?: Maybe<any>, text?: Maybe<string>, due?: Maybe<string>, kind?: Maybe<IMessageKindEnum>, approved?: Maybe<IApprovedEnum>, moderationStage?: Maybe<IModerationStageEnum>, viewsCount: number, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, appealExecutor?: Maybe<{ __typename?: 'AppealExecutor', id?: Maybe<string> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }> };

export type IUpdateMessageMutationVariables = Exact<{
  id: Scalars['ID'];
  message: IUpdateMessage;
}>;


export type IUpdateMessageMutation = { __typename?: 'Mutation', updateMessage?: Maybe<{ __typename?: 'Message', id?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, dislikesCount: number, likesCount: number, parentId?: Maybe<string>, parentType?: Maybe<string>, weight?: Maybe<any>, text?: Maybe<string>, due?: Maybe<string>, kind?: Maybe<IMessageKindEnum>, approved?: Maybe<IApprovedEnum>, moderationStage?: Maybe<IModerationStageEnum>, viewsCount: number, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, appealExecutor?: Maybe<{ __typename?: 'AppealExecutor', id?: Maybe<string> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }> };

export type IApproveMessageMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type IApproveMessageMutation = { __typename?: 'Mutation', approveMessage?: Maybe<{ __typename?: 'MessagePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Message', id?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, dislikesCount: number, likesCount: number, parentId?: Maybe<string>, parentType?: Maybe<string>, weight?: Maybe<any>, text?: Maybe<string>, due?: Maybe<string>, kind?: Maybe<IMessageKindEnum>, approved?: Maybe<IApprovedEnum>, moderationStage?: Maybe<IModerationStageEnum>, viewsCount: number, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, appealExecutor?: Maybe<{ __typename?: 'AppealExecutor', id?: Maybe<string> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }> };

export type ILikeReportFragmentFragment = { __typename?: 'LikeReport', likeCount: number, dislikeCount: number, liked?: Maybe<ILikeStateEnum> };

export type ILikeMessageMutationVariables = Exact<{
  id?: Maybe<Scalars['ID']>;
  operation: ILikeRequestEnum;
}>;


export type ILikeMessageMutation = { __typename?: 'Mutation', likeMessage?: Maybe<{ __typename?: 'LikeReport', likeCount: number, dislikeCount: number, liked?: Maybe<ILikeStateEnum> }> };

export type IRejectMessageMutationVariables = Exact<{
  id: Scalars['ID'];
  reason: Scalars['String'];
}>;


export type IRejectMessageMutation = { __typename?: 'Mutation', approveMessage?: Maybe<{ __typename?: 'MessagePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Message', id?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, dislikesCount: number, likesCount: number, parentId?: Maybe<string>, parentType?: Maybe<string>, weight?: Maybe<any>, text?: Maybe<string>, due?: Maybe<string>, kind?: Maybe<IMessageKindEnum>, approved?: Maybe<IApprovedEnum>, moderationStage?: Maybe<IModerationStageEnum>, viewsCount: number, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, appealExecutor?: Maybe<{ __typename?: 'AppealExecutor', id?: Maybe<string> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }> };

export type IMessageListQueryVariables = Exact<{
  id: Scalars['Long'];
  type: Scalars['String'];
}>;


export type IMessageListQuery = { __typename?: 'Query', messageList?: Maybe<{ __typename?: 'MessagePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Message', id?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, dislikesCount: number, likesCount: number, parentId?: Maybe<string>, parentType?: Maybe<string>, weight?: Maybe<any>, text?: Maybe<string>, due?: Maybe<string>, kind?: Maybe<IMessageKindEnum>, approved?: Maybe<IApprovedEnum>, moderationStage?: Maybe<IModerationStageEnum>, viewsCount: number, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>, appealExecutor?: Maybe<{ __typename?: 'AppealExecutor', id?: Maybe<string> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }> };

export type IEnumRecFragment = { __typename?: 'EnumVerb', description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, group?: Maybe<string>, subsystemUID?: Maybe<string>, value?: Maybe<string>, id?: Maybe<string> };

export type IReadAllEnumsQueryVariables = Exact<{ [key: string]: never; }>;


export type IReadAllEnumsQuery = { __typename?: 'Query', appealStatus?: Maybe<{ __typename?: 'EnumVerbPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'EnumVerb', description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, group?: Maybe<string>, subsystemUID?: Maybe<string>, value?: Maybe<string>, id?: Maybe<string> }>>> }>, feedbackStatus?: Maybe<{ __typename?: 'EnumVerbPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'EnumVerb', description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, group?: Maybe<string>, subsystemUID?: Maybe<string>, value?: Maybe<string>, id?: Maybe<string> }>>> }>, externalLink?: Maybe<{ __typename?: 'EnumVerbPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'EnumVerb', description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, group?: Maybe<string>, subsystemUID?: Maybe<string>, value?: Maybe<string>, id?: Maybe<string> }>>> }>, groupDescriptor?: Maybe<{ __typename?: 'EnumVerbPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'EnumVerb', description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, group?: Maybe<string>, subsystemUID?: Maybe<string>, value?: Maybe<string>, id?: Maybe<string> }>>> }> };

export type IGetEnumVerbModListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetEnumVerbModListQuery = { __typename?: 'Query', enumVerb?: Maybe<{ __typename?: 'EnumVerbPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'EnumVerb', customSettings?: Maybe<string>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, group?: Maybe<string>, subsystemUID?: Maybe<string>, value?: Maybe<string>, id?: Maybe<string> }>>> }> };

export type IGetEnumVerbModByIdQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetEnumVerbModByIdQuery = { __typename?: 'Query', enumVerb?: Maybe<{ __typename?: 'EnumVerbPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'EnumVerb', customSettings?: Maybe<string>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, group?: Maybe<string>, subsystemUID?: Maybe<string>, value?: Maybe<string>, id?: Maybe<string> }>>> }> };

export type ICreateEnumVerbMutationVariables = Exact<{
  enumVerb?: Maybe<ICreateEnumVerb>;
}>;


export type ICreateEnumVerbMutation = { __typename?: 'Mutation', createEnumVerb?: Maybe<{ __typename?: 'EnumVerb', description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, group?: Maybe<string>, subsystemUID?: Maybe<string>, value?: Maybe<string>, id?: Maybe<string> }> };

export type IUpdateEnumVerbMutationVariables = Exact<{
  enumVerb?: Maybe<IUpdateEnumVerb>;
}>;


export type IUpdateEnumVerbMutation = { __typename?: 'Mutation', updateEnumVerb?: Maybe<{ __typename?: 'EnumVerb', description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, group?: Maybe<string>, subsystemUID?: Maybe<string>, value?: Maybe<string>, id?: Maybe<string> }> };

export type IAvailableExecutorsQueryVariables = Exact<{ [key: string]: never; }>;


export type IAvailableExecutorsQuery = { __typename?: 'Query', availableExecutors?: Maybe<{ __typename?: 'UserPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> };

export type IGetExecutorAppealsListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetExecutorAppealsListQuery = { __typename?: 'Query', appealWhereExecutor?: Maybe<{ __typename?: 'AppealPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, appealType?: Maybe<IAppealTypeEnum>, authorName?: Maybe<string>, siteStatus?: Maybe<number>, weight?: Maybe<any>, title?: Maybe<string>, platform?: Maybe<string>, approved?: Maybe<IApprovedEnum>, deleted?: Maybe<boolean>, extNumber?: Maybe<string>, extDate?: Maybe<any>, hasUnapprovedMessages: boolean, publicGranted?: Maybe<boolean>, post?: Maybe<string>, signatory?: Maybe<string>, regNumber?: Maybe<string>, regDate?: Maybe<any>, wORegNumberRegDate?: Maybe<boolean>, subsystem?: Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string> }>>>, author?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string>, addrType?: Maybe<IAddrType> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, organization?: Maybe<{ __typename?: 'OrganizationType', id: any, email?: Maybe<string>, fullName?: Maybe<string>, shortName?: Maybe<string>, ogrn?: Maybe<string>, kpp?: Maybe<string>, inn?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> }>>> }> }>>> }> };

export type IFileUploadResultFragmentFragment = { __typename?: 'FileUploadResult', id?: Maybe<string>, next?: Maybe<any>, percent?: Maybe<number> };

export type IFileUploadMutationVariables = Exact<{
  file: IFileUpload;
}>;


export type IFileUploadMutation = { __typename?: 'Mutation', fileUpload?: Maybe<{ __typename?: 'FileUploadResult', id?: Maybe<string>, next?: Maybe<any>, percent?: Maybe<number> }> };

export type IGetJournalListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
}>;


export type IGetJournalListQuery = { __typename?: 'Query', journal?: Maybe<{ __typename?: 'ChangeJournalItemPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'JournalItemType', entityKey: string, entityName: string, friendlyName: string, operation?: Maybe<IEntityOperation>, operationName: string, timestamp?: Maybe<any>, properties?: Maybe<Array<Maybe<{ __typename?: 'JournalPropertiesType', friendlyName: string, newValue?: Maybe<string>, oldValue?: Maybe<string>, tag: string }>>>, user?: Maybe<{ __typename?: 'User', id: any, userName?: Maybe<string>, fullName?: Maybe<string> }> }>>> }> };

export type IGetJournalQueryVariables = Exact<{
  entityName?: Maybe<Scalars['String']>;
  key?: Maybe<Scalars['String']>;
}>;


export type IGetJournalQuery = { __typename?: 'Query', journal?: Maybe<{ __typename?: 'ChangeJournalItemPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'JournalItemType', entityKey: string, entityName: string, friendlyName: string, operation?: Maybe<IEntityOperation>, operationName: string, timestamp?: Maybe<any>, properties?: Maybe<Array<Maybe<{ __typename?: 'JournalPropertiesType', friendlyName: string, newValue?: Maybe<string>, oldValue?: Maybe<string>, newValueFriendly?: Maybe<string>, tag: string }>>>, user?: Maybe<{ __typename?: 'User', id: any, userName?: Maybe<string>, fullName?: Maybe<string> }> }>>> }> };

export type IAvailableModeratorsQueryVariables = Exact<{ [key: string]: never; }>;


export type IAvailableModeratorsQuery = { __typename?: 'Query', availableModerators?: Maybe<{ __typename?: 'UserPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> };

export type IAvailableModeratorsForQueryVariables = Exact<{
  subsystemUid?: Maybe<Array<Scalars['String']> | Scalars['String']>;
}>;


export type IAvailableModeratorsForQuery = { __typename?: 'Query', availableModerators?: Maybe<{ __typename?: 'UserPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> };

export type INewsPubFragItemFragment = { __typename?: 'News', commentsAllowed?: Maybe<boolean>, deleted?: Maybe<boolean>, id?: Maybe<string>, name?: Maybe<string>, state?: Maybe<INewsStateEnum>, text?: Maybe<string>, shortText?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, viewsCount: number, weight?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, usersLiked?: Maybe<Array<Maybe<{ __typename?: 'User', id: any }>>>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, author?: Maybe<{ __typename?: 'User', fullName?: Maybe<string> }>, likeWIL?: Maybe<{ __typename?: 'LikeWIL', liked?: Maybe<ILikeStateEnum> }>, viewWIV?: Maybe<{ __typename?: 'ViewWIV', viewed: boolean }> };

export type INewsPubFragListFragment = { __typename?: 'News', id?: Maybe<string>, name?: Maybe<string>, state?: Maybe<INewsStateEnum>, text?: Maybe<string>, shortText?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, viewsCount: number, weight?: Maybe<any>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, author?: Maybe<{ __typename?: 'User', fullName?: Maybe<string> }>, likeWIL?: Maybe<{ __typename?: 'LikeWIL', liked?: Maybe<ILikeStateEnum> }>, viewWIV?: Maybe<{ __typename?: 'ViewWIV', viewed: boolean }> };

export type IGetNewsModListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetNewsModListQuery = { __typename?: 'Query', news?: Maybe<{ __typename?: 'NewsPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'News', id?: Maybe<string>, name?: Maybe<string>, state?: Maybe<INewsStateEnum>, text?: Maybe<string>, shortText?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, viewsCount: number, weight?: Maybe<any>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, author?: Maybe<{ __typename?: 'User', fullName?: Maybe<string> }>, likeWIL?: Maybe<{ __typename?: 'LikeWIL', liked?: Maybe<ILikeStateEnum> }>, viewWIV?: Maybe<{ __typename?: 'ViewWIV', viewed: boolean }> }>>> }> };

export type IGetNewsModByIdQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetNewsModByIdQuery = { __typename?: 'Query', news?: Maybe<{ __typename?: 'NewsPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'News', commentsAllowed?: Maybe<boolean>, deleted?: Maybe<boolean>, id?: Maybe<string>, name?: Maybe<string>, state?: Maybe<INewsStateEnum>, text?: Maybe<string>, shortText?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, viewsCount: number, weight?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, usersLiked?: Maybe<Array<Maybe<{ __typename?: 'User', id: any }>>>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, author?: Maybe<{ __typename?: 'User', fullName?: Maybe<string> }>, likeWIL?: Maybe<{ __typename?: 'LikeWIL', liked?: Maybe<ILikeStateEnum> }>, viewWIV?: Maybe<{ __typename?: 'ViewWIV', viewed: boolean }> }>>> }> };

export type ICreateNewsMutationVariables = Exact<{
  news: ICreateNews;
}>;


export type ICreateNewsMutation = { __typename?: 'Mutation', createNews?: Maybe<{ __typename?: 'News', commentsAllowed?: Maybe<boolean>, deleted?: Maybe<boolean>, id?: Maybe<string>, name?: Maybe<string>, state?: Maybe<INewsStateEnum>, text?: Maybe<string>, shortText?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, viewsCount: number, weight?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, usersLiked?: Maybe<Array<Maybe<{ __typename?: 'User', id: any }>>>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, author?: Maybe<{ __typename?: 'User', fullName?: Maybe<string> }>, likeWIL?: Maybe<{ __typename?: 'LikeWIL', liked?: Maybe<ILikeStateEnum> }>, viewWIV?: Maybe<{ __typename?: 'ViewWIV', viewed: boolean }> }> };

export type IUpdateNewsMutationVariables = Exact<{
  id: Scalars['ID'];
  news: IUpdateNews;
}>;


export type IUpdateNewsMutation = { __typename?: 'Mutation', updateNews?: Maybe<{ __typename?: 'News', commentsAllowed?: Maybe<boolean>, deleted?: Maybe<boolean>, id?: Maybe<string>, name?: Maybe<string>, state?: Maybe<INewsStateEnum>, text?: Maybe<string>, shortText?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, viewsCount: number, weight?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, usersLiked?: Maybe<Array<Maybe<{ __typename?: 'User', id: any }>>>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, author?: Maybe<{ __typename?: 'User', fullName?: Maybe<string> }>, likeWIL?: Maybe<{ __typename?: 'LikeWIL', liked?: Maybe<ILikeStateEnum> }>, viewWIV?: Maybe<{ __typename?: 'ViewWIV', viewed: boolean }> }> };

export type ISetNewsPublishStateMutationVariables = Exact<{
  id: Scalars['ID'];
  state: INewsStateEnum;
}>;


export type ISetNewsPublishStateMutation = { __typename?: 'Mutation', approveNews?: Maybe<{ __typename?: 'News', commentsAllowed?: Maybe<boolean>, deleted?: Maybe<boolean>, id?: Maybe<string>, name?: Maybe<string>, state?: Maybe<INewsStateEnum>, text?: Maybe<string>, shortText?: Maybe<string>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, likesCount: number, viewsCount: number, weight?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, usersLiked?: Maybe<Array<Maybe<{ __typename?: 'User', id: any }>>>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, author?: Maybe<{ __typename?: 'User', fullName?: Maybe<string> }>, likeWIL?: Maybe<{ __typename?: 'LikeWIL', liked?: Maybe<ILikeStateEnum> }>, viewWIV?: Maybe<{ __typename?: 'ViewWIV', viewed: boolean }> }> };

export type IOfficerFragmentFragment = { __typename?: 'Officer', authority?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> };

export type IPgOfficerFragmentFragment = { __typename?: 'OfficerPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Officer', authority?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> }>>> };

export type IGetOfficerQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetOfficerQuery = { __typename?: 'Query', officer?: Maybe<{ __typename?: 'OfficerPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Officer', authority?: Maybe<string>, createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, place?: Maybe<string>, position?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> }>>> }> };

export type IOrganizationLiteFragmentFragment = { __typename?: 'OrganizationType', id: any, email?: Maybe<string>, fullName?: Maybe<string>, shortName?: Maybe<string>, ogrn?: Maybe<string>, kpp?: Maybe<string>, inn?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> }>>> };

export type IAddrFragmentFragment = { __typename?: 'AddressType', additionArea?: Maybe<string>, additionAreaStreet?: Maybe<string>, addrType?: Maybe<IAddrType>, area?: Maybe<string>, building?: Maybe<string>, city?: Maybe<string>, countryId?: Maybe<string>, district?: Maybe<string>, fiasCode?: Maybe<string>, flat?: Maybe<string>, frame?: Maybe<string>, fullAddress?: Maybe<string>, house?: Maybe<string>, id: any, partialAddress?: Maybe<string>, region?: Maybe<string>, settlement?: Maybe<string>, street?: Maybe<string>, zipCode?: Maybe<string> };

export type IProtocolRecordLiteFragmentFragment = { __typename?: 'ProtocolRecord', id?: Maybe<string>, action?: Maybe<string>, createdAt?: Maybe<any>, data?: Maybe<string>, deleted?: Maybe<boolean>, weight?: Maybe<any>, updatedAt?: Maybe<any>, appeal?: Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, number?: Maybe<number>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, firstName?: Maybe<string>, lastName?: Maybe<string>, extNumber?: Maybe<string>, extDate?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', shortName?: Maybe<string> }> }>, appointment?: Maybe<{ __typename?: 'Appointment', id?: Maybe<string>, address?: Maybe<string> }>, appointmentRange?: Maybe<{ __typename?: 'AppointmentRange', id?: Maybe<string>, start?: Maybe<any>, end?: Maybe<any> }>, attachedFile?: Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string>, name?: Maybe<string> }>, user?: Maybe<{ __typename?: 'User', id: any, userName?: Maybe<string>, fullName?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, name?: Maybe<string> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, name?: Maybe<string> }>, officer?: Maybe<{ __typename?: 'Officer', id?: Maybe<string>, name?: Maybe<string> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, name?: Maybe<string> }> };

export type IPgProtocolRecordLiteFragmentFragment = { __typename?: 'ProtocolRecordPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'ProtocolRecord', id?: Maybe<string>, action?: Maybe<string>, createdAt?: Maybe<any>, data?: Maybe<string>, deleted?: Maybe<boolean>, weight?: Maybe<any>, updatedAt?: Maybe<any>, appeal?: Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, number?: Maybe<number>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, firstName?: Maybe<string>, lastName?: Maybe<string>, extNumber?: Maybe<string>, extDate?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', shortName?: Maybe<string> }> }>, appointment?: Maybe<{ __typename?: 'Appointment', id?: Maybe<string>, address?: Maybe<string> }>, appointmentRange?: Maybe<{ __typename?: 'AppointmentRange', id?: Maybe<string>, start?: Maybe<any>, end?: Maybe<any> }>, attachedFile?: Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string>, name?: Maybe<string> }>, user?: Maybe<{ __typename?: 'User', id: any, userName?: Maybe<string>, fullName?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, name?: Maybe<string> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, name?: Maybe<string> }>, officer?: Maybe<{ __typename?: 'Officer', id?: Maybe<string>, name?: Maybe<string> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, name?: Maybe<string> }> }>>> };

export type IGetProtocolsQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetProtocolsQuery = { __typename?: 'Query', protocolRecord?: Maybe<{ __typename?: 'ProtocolRecordPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'ProtocolRecord', id?: Maybe<string>, action?: Maybe<string>, createdAt?: Maybe<any>, data?: Maybe<string>, deleted?: Maybe<boolean>, weight?: Maybe<any>, updatedAt?: Maybe<any>, appeal?: Maybe<{ __typename?: 'Appeal', id?: Maybe<string>, number?: Maybe<number>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, firstName?: Maybe<string>, lastName?: Maybe<string>, extNumber?: Maybe<string>, extDate?: Maybe<any>, rubric?: Maybe<{ __typename?: 'Rubric', shortName?: Maybe<string> }> }>, appointment?: Maybe<{ __typename?: 'Appointment', id?: Maybe<string>, address?: Maybe<string> }>, appointmentRange?: Maybe<{ __typename?: 'AppointmentRange', id?: Maybe<string>, start?: Maybe<any>, end?: Maybe<any> }>, attachedFile?: Maybe<{ __typename?: 'AttachedFile', id?: Maybe<string>, name?: Maybe<string> }>, user?: Maybe<{ __typename?: 'User', id: any, userName?: Maybe<string>, fullName?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', id?: Maybe<string>, name?: Maybe<string> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, name?: Maybe<string> }>, officer?: Maybe<{ __typename?: 'Officer', id?: Maybe<string>, name?: Maybe<string> }>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, name?: Maybe<string> }> }>>> }> };

export type IFtReeRegionFragment = { __typename?: 'Region', due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> };

export type IRegionPubFragFragment = { __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> };

export type IRegionPublicFragmentFragment = { __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> };

export type IRegionFullFragmentFragment = { __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> };

export type IGetRegionListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetRegionListQuery = { __typename?: 'Query', region?: Maybe<{ __typename?: 'RegionPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>>> }> };

export type IGetRegionByIdQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetRegionByIdQuery = { __typename?: 'Query', region?: Maybe<{ __typename?: 'RegionPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>>> }> };

export type ICreateRegionMutationVariables = Exact<{
  region: ICreateRegion;
}>;


export type ICreateRegionMutation = { __typename?: 'Mutation', createRegion?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> };

export type IUpdateRegionMutationVariables = Exact<{
  id: Scalars['ID'];
  region: IUpdateRegion;
}>;


export type IUpdateRegionMutation = { __typename?: 'Mutation', updateRegion?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> };

export type IGetReportsListQueryVariables = Exact<{ [key: string]: never; }>;


export type IGetReportsListQuery = { __typename?: 'Query', reports?: Maybe<Array<Maybe<{ __typename?: 'ReportSettings', reportId?: Maybe<any>, name?: Maybe<string>, description?: Maybe<string> }>>> };

export type IGetReportSettingsQueryVariables = Exact<{
  id?: Maybe<Scalars['Guid']>;
}>;


export type IGetReportSettingsQuery = { __typename?: 'Query', reportSettings?: Maybe<Array<Maybe<{ __typename?: 'ReportSettings', reportId?: Maybe<any>, name?: Maybe<string>, description?: Maybe<string>, parameters?: Maybe<Array<Maybe<{ __typename?: 'ReportSingleSettings', allEnable?: Maybe<boolean>, allSelected?: Maybe<boolean>, controlType?: Maybe<IReportSettingsControlType>, name: string, options?: Maybe<string>, required?: Maybe<boolean>, title: string, value?: Maybe<string>, values?: Maybe<string> }>>> }>>> };

export type IExecuteReportQueryVariables = Exact<{
  settings: IReportSettingsInput;
}>;


export type IExecuteReportQuery = { __typename?: 'Query', executeReport?: Maybe<{ __typename?: 'ExecuteReportResult', id?: Maybe<any>, success?: Maybe<boolean>, template?: Maybe<string>, errors?: Maybe<Array<Maybe<string>>>, file?: Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, deleted?: Maybe<boolean>, extId?: Maybe<string>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, private?: Maybe<boolean>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, updatedAt?: Maybe<any>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }> }> };

export type IPaginationRoleFullFragmentFragment = { __typename?: 'RolePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>> };

export type IPaginationRoleLiteFragmentFragment = { __typename?: 'RolePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>> };

export type IRoleLiteFragmentFragment = { __typename?: 'RoleType', id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean };

export type IRoleFullFragmentFragment = { __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> };

export type IGetRoleQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetRoleQuery = { __typename?: 'Query', role?: Maybe<{ __typename?: 'RolePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>> }> };

export type IGetRoleListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
}>;


export type IGetRoleListQuery = { __typename?: 'Query', role?: Maybe<{ __typename?: 'RolePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>> }> };

export type IGetRoleListFullQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
}>;


export type IGetRoleListFullQuery = { __typename?: 'Query', role?: Maybe<{ __typename?: 'RolePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>> }> };

export type IUpdateRole1MutationVariables = Exact<{
  id: Scalars['ID'];
  role: IRoleInputType;
}>;


export type IUpdateRole1Mutation = { __typename?: 'Mutation', updateRole?: Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }> };

export type IUpdateRoleMutationVariables = Exact<{
  id: Scalars['ID'];
  role: IRoleInputType;
}>;


export type IUpdateRoleMutation = { __typename?: 'Mutation', updateRole?: Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }> };

export type ICreateRoleMutationVariables = Exact<{
  role: IRoleInputType;
}>;


export type ICreateRoleMutation = { __typename?: 'Mutation', createRole?: Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }> };

export type IDeleteRoleMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type IDeleteRoleMutation = { __typename?: 'Mutation', removeRole?: Maybe<boolean> };

export type IRubricLinkFragmentFragment = { __typename?: 'Rubric', id?: Maybe<string>, name?: Maybe<string> };

export type IRubricPubFragFragment = { __typename?: 'Rubric', id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> };

export type IRubricFieldsUlFragment = { __typename?: 'Rubric', docGroupForUL?: Maybe<string> };

export type IRubricModeratedFragmentFragment = { __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> };

export type IRubricLiteFragmentFragment = { __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> };

export type IGetRubricListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetRubricListQuery = { __typename?: 'Query', rubric?: Maybe<{ __typename?: 'RubricPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>>> }> };

export type IGetRubricByIdQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetRubricByIdQuery = { __typename?: 'Query', rubric?: Maybe<{ __typename?: 'RubricPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>>> }> };

export type IUpdateRubricMutationVariables = Exact<{
  id: Scalars['ID'];
  rubric: IUpdateRubric;
}>;


export type IUpdateRubricMutation = { __typename?: 'Mutation', updateRubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> };

export type ICreateRubricMutationVariables = Exact<{
  rubric: ICreateRubric;
}>;


export type ICreateRubricMutation = { __typename?: 'Mutation', createRubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }> };

export type ISettingsFragmentFragment = { __typename?: 'Settings', key?: Maybe<string>, value?: Maybe<string>, settingType?: Maybe<ISettingType>, valueType?: Maybe<string>, settingNodeType?: Maybe<ISettingNodeType>, description?: Maybe<string>, weight: number, values?: Maybe<Array<Maybe<{ __typename?: 'createSettingsValueItem', displayText: string, value?: Maybe<string> }>>> };

export type ISettingsValueFragmentFragment = { __typename?: 'Settings', key?: Maybe<string>, value?: Maybe<string> };

export type IGetSettingsQueryVariables = Exact<{
  pathes: Scalars['String'];
}>;


export type IGetSettingsQuery = { __typename?: 'Query', settings?: Maybe<Array<Maybe<{ __typename?: 'Settings', key?: Maybe<string>, value?: Maybe<string>, settingType?: Maybe<ISettingType>, valueType?: Maybe<string>, settingNodeType?: Maybe<ISettingNodeType>, description?: Maybe<string>, weight: number, values?: Maybe<Array<Maybe<{ __typename?: 'createSettingsValueItem', displayText: string, value?: Maybe<string> }>>> }>>> };

export type ISetSettingsMutationVariables = Exact<{
  settings: Array<Maybe<IUpdateSettings>> | Maybe<IUpdateSettings>;
}>;


export type ISetSettingsMutation = { __typename?: 'Mutation', setSettings?: Maybe<Array<Maybe<{ __typename?: 'Settings', key?: Maybe<string>, value?: Maybe<string> }>>> };

export type INewSettingsSubscriptionVariables = Exact<{
  keys?: Maybe<Array<Maybe<Scalars['String']>> | Maybe<Scalars['String']>>;
}>;


export type INewSettingsSubscription = { __typename?: 'Subscriptions', newSettings?: Maybe<{ __typename?: 'Settings', key?: Maybe<string>, value?: Maybe<string> }> };

export type IFBaseSubsystemFragment = { __typename?: 'Subsystem', uID?: Maybe<string>, deleted?: Maybe<boolean>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, name?: Maybe<string>, description?: Maybe<string>, mainEntityFields?: Maybe<Array<Maybe<{ __typename?: 'SubsystemMainEntityField', nameAPI: string, description?: Maybe<string>, nameAPISecond?: Maybe<string>, nameDisplayed?: Maybe<string>, placeholder?: Maybe<string>, required?: Maybe<boolean>, weight?: Maybe<number> }>>> };

export type IGetSubSystemListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetSubSystemListQuery = { __typename?: 'Query', subsystem?: Maybe<{ __typename?: 'SubsystemPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string>, deleted?: Maybe<boolean>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, name?: Maybe<string>, description?: Maybe<string>, mainEntityFields?: Maybe<Array<Maybe<{ __typename?: 'SubsystemMainEntityField', nameAPI: string, description?: Maybe<string>, nameAPISecond?: Maybe<string>, nameDisplayed?: Maybe<string>, placeholder?: Maybe<string>, required?: Maybe<boolean>, weight?: Maybe<number> }>>> }>>> }> };

export type IGetSubSystemQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetSubSystemQuery = { __typename?: 'Query', subsystem?: Maybe<{ __typename?: 'SubsystemPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string>, deleted?: Maybe<boolean>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, name?: Maybe<string>, description?: Maybe<string>, mainEntityFields?: Maybe<Array<Maybe<{ __typename?: 'SubsystemMainEntityField', nameAPI: string, description?: Maybe<string>, nameAPISecond?: Maybe<string>, nameDisplayed?: Maybe<string>, placeholder?: Maybe<string>, required?: Maybe<boolean>, weight?: Maybe<number> }>>> }>>> }> };

export type IUpdateSubSystemMutationVariables = Exact<{
  id: Scalars['ID'];
  subsystem: IUpdateSubsystem;
}>;


export type IUpdateSubSystemMutation = { __typename?: 'Mutation', updateSubsystem?: Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string>, deleted?: Maybe<boolean>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, name?: Maybe<string>, description?: Maybe<string>, mainEntityFields?: Maybe<Array<Maybe<{ __typename?: 'SubsystemMainEntityField', nameAPI: string, description?: Maybe<string>, nameAPISecond?: Maybe<string>, nameDisplayed?: Maybe<string>, placeholder?: Maybe<string>, required?: Maybe<boolean>, weight?: Maybe<number> }>>> }> };

export type IFtReeTerritoryFragment = { __typename?: 'Territory', due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> };

export type ITerritoryLinkFragFragment = { __typename?: 'Territory', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string> };

export type ITerritoryPubFragFragment = { __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> };

export type ITerritoryFullFragmentFragment = { __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> };

export type IGetTerritoryListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetTerritoryListQuery = { __typename?: 'Query', territory?: Maybe<{ __typename?: 'TerritoryPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>>> }> };

export type IGetTerritoryByIdQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetTerritoryByIdQuery = { __typename?: 'Query', territory?: Maybe<{ __typename?: 'TerritoryPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>>> }> };

export type IUpdateTerritoryMutationVariables = Exact<{
  id: Scalars['ID'];
  territory: IUpdateTerritory;
}>;


export type IUpdateTerritoryMutation = { __typename?: 'Mutation', updateTerritory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> };

export type ICreateTerritoryMutationVariables = Exact<{
  territory: ICreateTerritory;
}>;


export type ICreateTerritoryMutation = { __typename?: 'Mutation', createTerritory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> };

export type IFtReeTopicFragment = { __typename?: 'Topic', due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> };

export type ITopicPubFragFragment = { __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> };

export type ITopicFullFragmentFragment = { __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> };

export type ITopicPublicFragmentFragment = { __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> };

export type IGetTopicListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
  withDeleted?: Maybe<Scalars['Boolean']>;
}>;


export type IGetTopicListQuery = { __typename?: 'Query', topic?: Maybe<{ __typename?: 'TopicPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>>> }> };

export type IGetTopicQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetTopicQuery = { __typename?: 'Query', topic?: Maybe<{ __typename?: 'TopicPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>>> }> };

export type ICreateTopicMutationVariables = Exact<{
  topic: ICreateTopic;
}>;


export type ICreateTopicMutation = { __typename?: 'Mutation', createTopic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> };

export type IUpdateTopicMutationVariables = Exact<{
  id: Scalars['ID'];
  topic: IUpdateTopic;
}>;


export type IUpdateTopicMutation = { __typename?: 'Mutation', updateTopic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, moderators?: Maybe<Array<Maybe<{ __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> }>>>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> };

export type IUserPublicFragmentFragment = { __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean };

export type IUserModFragmentFragment = { __typename?: 'User', disconnected: boolean, disconnectedTimestamp?: Maybe<any>, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, updatedAt?: Maybe<any>, loginSystem?: Maybe<ILoginSystem>, selfFormRegister?: Maybe<boolean>, roles?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>>, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> };

export type IClerkInSsFragmentFragment = { __typename?: 'ClerkInSubsystemType', subsystemUid: string, users?: Maybe<Array<Maybe<{ __typename?: 'User', disconnected: boolean, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, selfFormRegister?: Maybe<boolean>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string> }>>> };

export type IUserFullFragmentFragment = { __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, updatedAt?: Maybe<any>, loginSystem?: Maybe<ILoginSystem>, selfFormRegister?: Maybe<boolean>, roles?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>>, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> };

export type IFUserClaimsFragment = { __typename?: 'User', claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> };

export type IPaginationUserFullTypeFragment = { __typename?: 'UserPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, updatedAt?: Maybe<any>, loginSystem?: Maybe<ILoginSystem>, selfFormRegister?: Maybe<boolean>, roles?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>>, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>> };

export type IOrganizFragFragment = { __typename?: 'OrganizationType', fullName?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string> }>>> };

export type IGetUserListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filters?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
}>;


export type IGetUserListQuery = { __typename?: 'Query', user?: Maybe<{ __typename?: 'UserPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'User', userName?: Maybe<string>, id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>> }> };

export type IGetUserByIdQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetUserByIdQuery = { __typename?: 'Query', user?: Maybe<{ __typename?: 'UserPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, updatedAt?: Maybe<any>, loginSystem?: Maybe<ILoginSystem>, selfFormRegister?: Maybe<boolean>, roles?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>>, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>> }>, userOrganization?: Maybe<{ __typename?: 'UserOrganizationPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'OrganizationType', fullName?: Maybe<string>, addresses?: Maybe<Array<Maybe<{ __typename?: 'AddressType', fullAddress?: Maybe<string> }>>> }>>> }> };

export type IUpdateUserMutationVariables = Exact<{
  id: Scalars['ID'];
  user: IUserInputType;
}>;


export type IUpdateUserMutation = { __typename?: 'Mutation', updateUser?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, updatedAt?: Maybe<any>, loginSystem?: Maybe<ILoginSystem>, selfFormRegister?: Maybe<boolean>, roles?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>>, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }> };

export type ICreateUserMutationVariables = Exact<{
  user: IRegisterModelType;
}>;


export type ICreateUserMutation = { __typename?: 'Mutation', createUser?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, updatedAt?: Maybe<any>, loginSystem?: Maybe<ILoginSystem>, selfFormRegister?: Maybe<boolean>, roles?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>>, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }> };

export type IResetPasswordMutationVariables = Exact<{
  resetPassword: IResetPasswordInputType;
}>;


export type IResetPasswordMutation = { __typename?: 'Mutation', resetPassword?: Maybe<{ __typename?: 'OperationResultType', errorMessages?: Maybe<Array<Maybe<string>>>, success?: Maybe<boolean> }> };

export type IVersionsInfoQueryVariables = Exact<{ [key: string]: never; }>;


export type IVersionsInfoQuery = { __typename?: 'Query', apiInformation?: Maybe<{ __typename?: 'ApiInformation', appBuildTime?: Maybe<any>, appConfiguration?: Maybe<string>, appVersion?: Maybe<string>, aspDotnetVersion?: Maybe<string>, aSPNETCORE_ENVIRONMENT?: Maybe<string>, dbVersion?: Maybe<string>, osDescription?: Maybe<string> }> };

export type IVoteRootRoFragmentFragment = { __typename?: 'VoteRoot', id?: Maybe<string>, state?: Maybe<IStatusEnum>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, startedAt?: Maybe<any>, endedAt?: Maybe<any>, archivedAt?: Maybe<any> };

export type IVoteRootPublicFragmentFragment = { __typename?: 'VoteRoot', weight?: Maybe<any>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, commentsAllowed?: Maybe<boolean>, id?: Maybe<string>, state?: Maybe<IStatusEnum>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, startedAt?: Maybe<any>, endedAt?: Maybe<any>, archivedAt?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, votes?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> };

export type IVoteRootModEditFragmentFragment = { __typename?: 'VoteRoot', startAt?: Maybe<any>, endAt?: Maybe<any>, archiveAt?: Maybe<any>, deleted?: Maybe<boolean>, weight?: Maybe<any>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, commentsAllowed?: Maybe<boolean>, id?: Maybe<string>, state?: Maybe<IStatusEnum>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, startedAt?: Maybe<any>, endedAt?: Maybe<any>, archivedAt?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, votes?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> };

export type IGetVoteRootPublicListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filter?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
}>;


export type IGetVoteRootPublicListQuery = { __typename?: 'Query', voteRoot?: Maybe<{ __typename?: 'VoteRootPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'VoteRoot', weight?: Maybe<any>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, commentsAllowed?: Maybe<boolean>, id?: Maybe<string>, state?: Maybe<IStatusEnum>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, startedAt?: Maybe<any>, endedAt?: Maybe<any>, archivedAt?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, votes?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }>>> }> };

export type IGetVoteRootModListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filter?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
}>;


export type IGetVoteRootModListQuery = { __typename?: 'Query', voteRoot?: Maybe<{ __typename?: 'VoteRootPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'VoteRoot', startAt?: Maybe<any>, endAt?: Maybe<any>, archiveAt?: Maybe<any>, deleted?: Maybe<boolean>, weight?: Maybe<any>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, commentsAllowed?: Maybe<boolean>, id?: Maybe<string>, state?: Maybe<IStatusEnum>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, startedAt?: Maybe<any>, endedAt?: Maybe<any>, archivedAt?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, votes?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }>>> }> };

export type IGetVoteRootModIdQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetVoteRootModIdQuery = { __typename?: 'Query', voteRoot?: Maybe<{ __typename?: 'VoteRootPageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'VoteRoot', startAt?: Maybe<any>, endAt?: Maybe<any>, archiveAt?: Maybe<any>, deleted?: Maybe<boolean>, weight?: Maybe<any>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, commentsAllowed?: Maybe<boolean>, id?: Maybe<string>, state?: Maybe<IStatusEnum>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, startedAt?: Maybe<any>, endedAt?: Maybe<any>, archivedAt?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, votes?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }>>> }> };

export type ICreateVoteRootModMutationVariables = Exact<{
  voteRoot: ICreateVoteRoot;
}>;


export type ICreateVoteRootModMutation = { __typename?: 'Mutation', createVoteRoot?: Maybe<{ __typename?: 'VoteRoot', startAt?: Maybe<any>, endAt?: Maybe<any>, archiveAt?: Maybe<any>, deleted?: Maybe<boolean>, weight?: Maybe<any>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, commentsAllowed?: Maybe<boolean>, id?: Maybe<string>, state?: Maybe<IStatusEnum>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, startedAt?: Maybe<any>, endedAt?: Maybe<any>, archivedAt?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, votes?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }> };

export type IUpdateVoteRootModMutationVariables = Exact<{
  id: Scalars['ID'];
  voteRoot: IUpdateVoteRoot;
}>;


export type IUpdateVoteRootModMutation = { __typename?: 'Mutation', updateVoteRoot?: Maybe<{ __typename?: 'VoteRoot', startAt?: Maybe<any>, endAt?: Maybe<any>, archiveAt?: Maybe<any>, deleted?: Maybe<boolean>, weight?: Maybe<any>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, commentsAllowed?: Maybe<boolean>, id?: Maybe<string>, state?: Maybe<IStatusEnum>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, startedAt?: Maybe<any>, endedAt?: Maybe<any>, archivedAt?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, votes?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }> };

export type IVoteItemLiteFragmentFragment = { __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> };

export type IVoteOfferFragmentFragment = { __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> };

export type IVotePublicFragmentFragment = { __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> };

export type IVoteFullFragmentFragment = { __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> };

export type IGetVotesListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filter?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
}>;


export type IGetVotesListQuery = { __typename?: 'Query', vote?: Maybe<{ __typename?: 'VotePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>> }> };

export type IGetVotesPublicListQueryVariables = Exact<{
  range?: Maybe<IRange>;
  filter?: Maybe<Array<Maybe<IFilter>> | Maybe<IFilter>>;
}>;


export type IGetVotesPublicListQuery = { __typename?: 'Query', vote?: Maybe<{ __typename?: 'VotePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>> }> };

export type IGetVoteQueryVariables = Exact<{
  id: Scalars['String'];
}>;


export type IGetVoteQuery = { __typename?: 'Query', vote?: Maybe<{ __typename?: 'VotePageType', items?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>> }> };

export type IGetWidgetVotesQueryVariables = Exact<{ [key: string]: never; }>;


export type IGetWidgetVotesQuery = { __typename?: 'Query', inProgress?: Maybe<{ __typename?: 'VotePageType', total: any }>, isDone?: Maybe<{ __typename?: 'VotePageType', total: any }>, isPlanned?: Maybe<{ __typename?: 'VotePageType', total: any }>, hots?: Maybe<{ __typename?: 'VotePageType', total: any, items?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>> }> };

export type ICreateVoteMutationVariables = Exact<{
  vote: ICreateVote;
}>;


export type ICreateVoteMutation = { __typename?: 'Mutation', createVote?: Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }> };

export type IUpdateVoteMutationVariables = Exact<{
  id: Scalars['ID'];
  vote: IUpdateVote;
}>;


export type IUpdateVoteMutation = { __typename?: 'Mutation', updateVote?: Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }> };

export type IVoteStartMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type IVoteStartMutation = { __typename?: 'Mutation', voteStart?: Maybe<{ __typename?: 'VoteRoot', startAt?: Maybe<any>, endAt?: Maybe<any>, archiveAt?: Maybe<any>, deleted?: Maybe<boolean>, weight?: Maybe<any>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, commentsAllowed?: Maybe<boolean>, id?: Maybe<string>, state?: Maybe<IStatusEnum>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, startedAt?: Maybe<any>, endedAt?: Maybe<any>, archivedAt?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, votes?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }> };

export type IVoteStopMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type IVoteStopMutation = { __typename?: 'Mutation', voteStop?: Maybe<{ __typename?: 'VoteRoot', startAt?: Maybe<any>, endAt?: Maybe<any>, archiveAt?: Maybe<any>, deleted?: Maybe<boolean>, weight?: Maybe<any>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, commentsAllowed?: Maybe<boolean>, id?: Maybe<string>, state?: Maybe<IStatusEnum>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, startedAt?: Maybe<any>, endedAt?: Maybe<any>, archivedAt?: Maybe<any>, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, votes?: Maybe<Array<Maybe<{ __typename?: 'Vote', id?: Maybe<string>, weight?: Maybe<any>, createdAt?: Maybe<any>, updatedAt?: Maybe<any>, mode?: Maybe<IModeEnum>, modeLimitValue?: Maybe<number>, name?: Maybe<string>, shortName?: Maybe<string>, text?: Maybe<string>, textResult?: Maybe<string>, usersCount: number, offerEnabled?: Maybe<boolean>, offersCount: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, offers?: Maybe<Array<Maybe<{ __typename?: 'VoteOffer', text?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>>, parentVoteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>>, voteItems?: Maybe<Array<Maybe<{ __typename?: 'VoteItem', name?: Maybe<string>, description?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, counter: number, attachedFiles?: Maybe<Array<Maybe<{ __typename?: 'AttachedFile', createdAt?: Maybe<any>, id?: Maybe<string>, name?: Maybe<string>, len: any, path?: Maybe<string>, weight?: Maybe<any>, contentType?: Maybe<string>, isPublic?: Maybe<boolean>, thumb?: Maybe<string>, uploadedBy?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }> }>>> }>>> }>>>, region?: Maybe<{ __typename?: 'Region', id?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, weight?: Maybe<any>, updatedAt?: Maybe<any>, due?: Maybe<string>, isNode?: Maybe<boolean>, parentId?: Maybe<string>, parentType?: Maybe<string> }>, rubric?: Maybe<{ __typename?: 'Rubric', docgroup?: Maybe<string>, extType?: Maybe<string>, extId?: Maybe<string>, receivers?: Maybe<string>, useForPA?: Maybe<boolean>, id?: Maybe<string>, createdAt?: Maybe<any>, description?: Maybe<string>, name?: Maybe<string>, shortName?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, deleted?: Maybe<boolean>, docGroupForUL?: Maybe<string>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>>, executors?: Maybe<Array<Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean }>>> }>, territory?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }>, topic?: Maybe<{ __typename?: 'Topic', id?: Maybe<string>, deleted?: Maybe<boolean>, name?: Maybe<string>, shortName?: Maybe<string>, createdAt?: Maybe<any>, extId?: Maybe<string>, weight?: Maybe<any>, updatedAt?: Maybe<any>, subsystems?: Maybe<Array<Maybe<{ __typename?: 'Subsystem', uID?: Maybe<string> }>>> }> }> };

export type ILoginFullFragmentFragment = { __typename?: 'LoginResultType', token?: Maybe<string>, loginStatus?: Maybe<string>, errorMessage?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, createdAt?: Maybe<any>, email?: Maybe<string>, fullName?: Maybe<string>, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, userName?: Maybe<string>, emailConfirmed?: Maybe<boolean>, updatedAt?: Maybe<any>, loginSystem?: Maybe<ILoginSystem>, selfFormRegister?: Maybe<boolean>, roles?: Maybe<Array<Maybe<{ __typename?: 'RoleType', createdAt?: Maybe<any>, updatedAt?: Maybe<any>, weight: number, disconnected: boolean, disconnectedTimestamp?: Maybe<any>, editable: boolean, id?: Maybe<string>, name: string, description?: Maybe<string>, isAdminRegister: boolean, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }>>>, claims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }> };

export type ILoginLiteFragmentFragment = { __typename?: 'LoginResultType', token?: Maybe<string>, loginStatus?: Maybe<string>, errorMessage?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, fullName?: Maybe<string>, email?: Maybe<string>, createdAt?: Maybe<any>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }> };

export type ILoginCurrentUserFragment = { __typename?: 'User', id: any, fullName?: Maybe<string>, email?: Maybe<string>, createdAt?: Maybe<any>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> };

export type ILoginQueryVariables = Exact<{
  login: Scalars['String'];
  password: Scalars['String'];
}>;


export type ILoginQuery = { __typename?: 'Query', login?: Maybe<{ __typename?: 'LoginResultType', token?: Maybe<string>, loginStatus?: Maybe<string>, errorMessage?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, fullName?: Maybe<string>, email?: Maybe<string>, createdAt?: Maybe<any>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }> }> };

export type IGetCurrentUserQueryVariables = Exact<{ [key: string]: never; }>;


export type IGetCurrentUserQuery = { __typename?: 'Query', currentUser?: Maybe<{ __typename?: 'User', id: any, fullName?: Maybe<string>, email?: Maybe<string>, createdAt?: Maybe<any>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }> };

export type IUpdateTokenQueryVariables = Exact<{ [key: string]: never; }>;


export type IUpdateTokenQuery = { __typename?: 'Query', updateToken?: Maybe<{ __typename?: 'LoginResultType', errorMessage?: Maybe<string>, loginStatus?: Maybe<string>, token?: Maybe<string>, user?: Maybe<{ __typename?: 'User', id: any, fullName?: Maybe<string>, email?: Maybe<string>, createdAt?: Maybe<any>, allClaims?: Maybe<Array<Maybe<{ __typename?: 'ClaimType', type: string, value: string }>>> }> }> };

export type ITest00002createTerritoryMutationVariables = Exact<{ [key: string]: never; }>;


export type ITest00002createTerritoryMutation = { __typename?: 'Mutation', ct1?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, weight?: Maybe<any> }>, ct2?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, weight?: Maybe<any> }> };

export type ITest00002updateTerritoryMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ITest00002updateTerritoryMutation = { __typename?: 'Mutation', ut1?: Maybe<{ __typename?: 'Territory', id?: Maybe<string>, weight?: Maybe<any> }> };

export const UserPublicFragmentFragmentDoc = gql`
    fragment UserPublicFragment on User {
  id
  createdAt
  email
  fullName
  disconnected
}
    `;
export const AttachedFilePublicFragmentFragmentDoc = gql`
    fragment AttachedFilePublicFragment on AttachedFile {
  createdAt
  id
  name
  len
  path
  weight
  contentType
  isPublic
  thumb
  uploadedBy {
    ...UserPublicFragment
  }
}
    ${UserPublicFragmentFragmentDoc}`;
export const RubricPubFragFragmentDoc = gql`
    fragment RubricPubFrag on Rubric {
  id
  createdAt
  description
  name
  shortName
  weight
  updatedAt
  deleted
  executors {
    ...UserPublicFragment
  }
}
    ${UserPublicFragmentFragmentDoc}`;
export const FtReeRegionFragmentDoc = gql`
    fragment FTReeRegion on Region {
  due
  isNode
  parentId
  parentType
}
    `;
export const RegionPublicFragmentFragmentDoc = gql`
    fragment RegionPublicFragment on Region {
  id
  name
  shortName
  createdAt
  weight
  updatedAt
  ...FTReeRegion
}
    ${FtReeRegionFragmentDoc}`;
export const TerritoryPubFragFragmentDoc = gql`
    fragment TerritoryPubFrag on Territory {
  id
  deleted
  name
  shortName
  createdAt
  extId
  weight
  updatedAt
  subsystems {
    uID
  }
}
    `;
export const TopicPubFragFragmentDoc = gql`
    fragment TopicPubFrag on Topic {
  id
  deleted
  name
  shortName
  createdAt
  extId
  weight
  updatedAt
  subsystems {
    uID
  }
}
    `;
export const TopicPublicFragmentFragmentDoc = gql`
    fragment TopicPublicFragment on Topic {
  ...TopicPubFrag
}
    ${TopicPubFragFragmentDoc}`;
export const MapPointFragmentDoc = gql`
    fragment mapPoint on Appeal {
  longitude
  latitude
  place
}
    `;
export const AppealPublicFragmentFragmentDoc = gql`
    fragment AppealPublicFragment on Appeal {
  id
  createdAt
  updatedAt
  description
  likesCount
  appealType
  attachedFiles {
    ...AttachedFilePublicFragment
  }
  author {
    ...UserPublicFragment
  }
  authorName
  rubric {
    ...RubricPubFrag
  }
  region {
    ...RegionPublicFragment
  }
  siteStatus
  weight
  territory {
    ...TerritoryPubFrag
  }
  topic {
    ...TopicPublicFragment
  }
  views
  viewsCount
  platform
  views
  viewsCount
  ...mapPoint
}
    ${AttachedFilePublicFragmentFragmentDoc}
${UserPublicFragmentFragmentDoc}
${RubricPubFragFragmentDoc}
${RegionPublicFragmentFragmentDoc}
${TerritoryPubFragFragmentDoc}
${TopicPublicFragmentFragmentDoc}
${MapPointFragmentDoc}`;
export const TerritoryLinkFragFragmentDoc = gql`
    fragment TerritoryLinkFrag on Territory {
  id
  name
  shortName
}
    `;
export const RubricLinkFragmentFragmentDoc = gql`
    fragment RubricLinkFragment on Rubric {
  id
  name
}
    `;
export const AppealModSmallListFragmentFragmentDoc = gql`
    fragment AppealModSmallListFragment on Appeal {
  id
  createdAt
  author {
    id
    fullName
  }
  authorName
  territory {
    ...TerritoryLinkFrag
  }
  rubric {
    ...RubricLinkFragment
  }
  siteStatus
  title
  platform
  appealExecutors {
    name
    department
    position
    organization
  }
  approved
  deleted
  extNumber
  extDate
  hasUnapprovedMessages
  publicGranted
  oldSystemId
}
    ${TerritoryLinkFragFragmentDoc}
${RubricLinkFragmentFragmentDoc}`;
export const AppealUlFragmentFragmentDoc = gql`
    fragment AppealULFragment on Appeal {
  post
  signatory
  regNumber
  regDate
  wORegNumberRegDate
}
    `;
export const AddrFragmentFragmentDoc = gql`
    fragment addrFragment on AddressType {
  additionArea
  additionAreaStreet
  addrType
  area
  building
  city
  countryId
  district
  fiasCode
  flat
  frame
  fullAddress
  house
  id
  partialAddress
  region
  settlement
  street
  zipCode
}
    `;
export const OrganizationLiteFragmentFragmentDoc = gql`
    fragment OrganizationLiteFragment on OrganizationType {
  id
  email
  fullName
  shortName
  ogrn
  kpp
  inn
  addresses {
    ...addrFragment
  }
}
    ${AddrFragmentFragmentDoc}`;
export const AppealModListFragmentFragmentDoc = gql`
    fragment AppealModListFragment on Appeal {
  id
  createdAt
  updatedAt
  likesCount
  appealType
  attachedFiles {
    id
  }
  author {
    ...UserPublicFragment
    addresses {
      fullAddress
      addrType
    }
  }
  authorName
  rubric {
    ...RubricPubFrag
  }
  region {
    ...RegionPublicFragment
  }
  siteStatus
  weight
  title
  platform
  ...AppealULFragment
  approved
  deleted
  extNumber
  extDate
  hasUnapprovedMessages
  publicGranted
  organization {
    ...OrganizationLiteFragment
  }
}
    ${UserPublicFragmentFragmentDoc}
${RubricPubFragFragmentDoc}
${RegionPublicFragmentFragmentDoc}
${AppealUlFragmentFragmentDoc}
${OrganizationLiteFragmentFragmentDoc}`;
export const AppealAuthorContactFragmentDoc = gql`
    fragment AppealAuthorContact on Appeal {
  address
  addressee
  firstName
  authorName
  lastName
  email
  inn
  phone
  zip
}
    `;
export const AppealModFullFragmentFragmentDoc = gql`
    fragment AppealModFullFragment on Appeal {
  ...AppealModListFragment
  ...AppealAuthorContact
  ...AppealULFragment
  territory {
    ...TerritoryPubFrag
  }
  topic {
    ...TopicPublicFragment
  }
  attachedFiles {
    ...AttachedFilePublicFragment
  }
  description
  answerByPost
  clientType
  collective
  factDate
  deloStatus
  deloText
  externalExec
  extId
  filesPush
  markSend
  municipality
  number
  patronim
  planDate
  public_
  publicGranted
  pushesSent
  region1
  rejectionReason
  resDate
  sentToDelo
  statusConfirmed
  topicUpdated
  hasUnapprovedMessages
  uploaded
  oldSystemId
}
    ${AppealModListFragmentFragmentDoc}
${AppealAuthorContactFragmentDoc}
${AppealUlFragmentFragmentDoc}
${TerritoryPubFragFragmentDoc}
${TopicPublicFragmentFragmentDoc}
${AttachedFilePublicFragmentFragmentDoc}`;
export const RubricFieldsUlFragmentDoc = gql`
    fragment RubricFieldsUL on Rubric {
  docGroupForUL
}
    `;
export const RubricLiteFragmentFragmentDoc = gql`
    fragment RubricLiteFragment on Rubric {
  ...RubricPubFrag
  docgroup
  extType
  extId
  receivers
  useForPA
  ...RubricFieldsUL
  subsystems {
    uID
  }
}
    ${RubricPubFragFragmentDoc}
${RubricFieldsUlFragmentDoc}`;
export const OfficerFragmentFragmentDoc = gql`
    fragment OfficerFragment on Officer {
  authority
  createdAt
  deleted
  extId
  id
  name
  place
  position
  rubric {
    ...RubricLiteFragment
  }
  weight
  updatedAt
}
    ${RubricLiteFragmentFragmentDoc}`;
export const AppointmentNotificationFragmentFragmentDoc = gql`
    fragment AppointmentNotificationFragment on AppointmentNotification {
  createdAt
  date
  deleted
  html
  id
  weight
  status
  updatedAt
}
    `;
export const AppointmentRangeFragmentFragmentDoc = gql`
    fragment AppointmentRangeFragment on AppointmentRange {
  appointments
  createdAt
  date
  deleted
  deleteMark
  end
  id
  officerName
  rubricName
  rubricOrder
  weight
  start
  updatedAt
}
    `;
export const AttachedFileFragmentFragmentDoc = gql`
    fragment AttachedFileFragment on AttachedFile {
  createdAt
  deleted
  extId
  id
  name
  len
  path
  private
  weight
  contentType
  isPublic
  updatedAt
  uploadedBy {
    ...UserPublicFragment
  }
  thumb
}
    ${UserPublicFragmentFragmentDoc}`;
export const ClerkInSsFragmentFragmentDoc = gql`
    fragment ClerkInSSFragment on ClerkInSubsystemType {
  subsystemUid
  users {
    ...UserPublicFragment
    disconnected
    userName
    emailConfirmed
    selfFormRegister
  }
}
    ${UserPublicFragmentFragmentDoc}`;
export const TopicFullFragmentFragmentDoc = gql`
    fragment TopicFullFragment on Topic {
  ...TopicPubFrag
  moderators {
    ...ClerkInSSFragment
  }
}
    ${TopicPubFragFragmentDoc}
${ClerkInSsFragmentFragmentDoc}`;
export const AppointmentFullFragmentFragmentDoc = gql`
    fragment AppointmentFullFragment on Appointment {
  address
  admin
  answers {
    ...AppointmentNotificationFragment
  }
  appointmentRange {
    ...AppointmentRangeFragment
  }
  attachedFiles {
    ...AttachedFileFragment
  }
  authorName
  authorNameUpperCase
  createdAt
  date
  deleted
  description
  email
  id
  municipality
  number
  officer {
    ...OfficerFragment
  }
  phone
  region
  rejectionReason
  rubric {
    ...RubricLiteFragment
  }
  weight
  status
  topic {
    ...TopicFullFragment
  }
  updatedAt
  zip
}
    ${AppointmentNotificationFragmentFragmentDoc}
${AppointmentRangeFragmentFragmentDoc}
${AttachedFileFragmentFragmentDoc}
${OfficerFragmentFragmentDoc}
${RubricLiteFragmentFragmentDoc}
${TopicFullFragmentFragmentDoc}`;
export const AppointmentRangeFullFragmentFragmentDoc = gql`
    fragment AppointmentRangeFullFragment on AppointmentRange {
  appointments
  createdAt
  date
  deleted
  deleteMark
  end
  id
  officer {
    ...OfficerFragment
  }
  officerName
  registeredAppointments {
    ...AppointmentFullFragment
  }
  rubricName
  rubricOrder
  weight
  start
  updatedAt
}
    ${OfficerFragmentFragmentDoc}
${AppointmentFullFragmentFragmentDoc}`;
export const AppointmentRangeLiteFragmentFragmentDoc = gql`
    fragment AppointmentRangeLiteFragment on AppointmentRange {
  rubricName
  start
  end
  date
  appointments
  registeredAppointments {
    id
  }
  officer {
    id
    name
    place
    position
    authority
  }
}
    `;
export const PgAppointmentRangeLiteFragmentFragmentDoc = gql`
    fragment PgAppointmentRangeLiteFragment on AppointmentRangePageType {
  items {
    ...AppointmentRangeLiteFragment
  }
  total
}
    ${AppointmentRangeLiteFragmentFragmentDoc}`;
export const PgAppointmentFullFragmentFragmentDoc = gql`
    fragment PgAppointmentFullFragment on AppointmentPageType {
  items {
    ...AppointmentFullFragment
  }
  total
}
    ${AppointmentFullFragmentFragmentDoc}`;
export const ClaimInfoValueTypeFragmentFragmentDoc = gql`
    fragment ClaimInfoValueTypeFragment on ClaimInfoValueType {
  description
  value
}
    `;
export const ClaimsInfoFragmentFragmentDoc = gql`
    fragment ClaimsInfoFragment on ClaimInfoType {
  claimEntity
  claimType
  description
  values {
    ...ClaimInfoValueTypeFragment
  }
}
    ${ClaimInfoValueTypeFragmentFragmentDoc}`;
export const MessageFragmentFragmentDoc = gql`
    fragment MessageFragment on Message {
  id
  author {
    ...UserPublicFragment
  }
  createdAt
  deleted
  dislikesCount
  likesCount
  parentId
  parentType
  weight
  text
  due
  kind
  approved
  moderationStage
  viewsCount
  appealExecutor {
    id
  }
  attachedFiles {
    ...AttachedFileFragment
  }
}
    ${UserPublicFragmentFragmentDoc}
${AttachedFileFragmentFragmentDoc}`;
export const LikeReportFragmentFragmentDoc = gql`
    fragment LikeReportFragment on LikeReport {
  likeCount
  dislikeCount
  liked
}
    `;
export const EnumRecFragmentDoc = gql`
    fragment EnumRec on EnumVerb {
  description
  name
  shortName
  group
  subsystemUID
  value
  id
}
    `;
export const FileUploadResultFragmentFragmentDoc = gql`
    fragment FileUploadResultFragment on FileUploadResult {
  id
  next
  percent
}
    `;
export const RegionPubFragFragmentDoc = gql`
    fragment RegionPubFrag on Region {
  id
  deleted
  name
  shortName
  createdAt
  extId
  weight
  updatedAt
  subsystems {
    uID
  }
}
    `;
export const NewsPubFragListFragmentDoc = gql`
    fragment NewsPubFragList on News {
  id
  name
  state
  text
  shortText
  createdAt
  updatedAt
  territory {
    ...TerritoryPubFrag
  }
  rubric {
    ...RubricPubFrag
  }
  region {
    ...RegionPubFrag
  }
  topic {
    ...TopicPubFrag
  }
  author {
    fullName
  }
  likesCount
  likeWIL {
    liked
  }
  viewsCount
  viewWIV {
    viewed
  }
  weight
}
    ${TerritoryPubFragFragmentDoc}
${RubricPubFragFragmentDoc}
${RegionPubFragFragmentDoc}
${TopicPubFragFragmentDoc}`;
export const NewsPubFragItemFragmentDoc = gql`
    fragment NewsPubFragItem on News {
  attachedFiles {
    ...AttachedFilePublicFragment
  }
  ...NewsPubFragList
  commentsAllowed
  deleted
  usersLiked {
    id
  }
}
    ${AttachedFilePublicFragmentFragmentDoc}
${NewsPubFragListFragmentDoc}`;
export const PgOfficerFragmentFragmentDoc = gql`
    fragment PgOfficerFragment on OfficerPageType {
  items {
    ...OfficerFragment
  }
  total
}
    ${OfficerFragmentFragmentDoc}`;
export const ProtocolRecordLiteFragmentFragmentDoc = gql`
    fragment protocolRecordLiteFragment on ProtocolRecord {
  id
  action
  appeal {
    id
    number
    createdAt
    updatedAt
    firstName
    lastName
    extNumber
    extDate
    rubric {
      shortName
    }
  }
  appointment {
    id
    address
  }
  appointmentRange {
    id
    start
    end
  }
  attachedFile {
    id
    name
  }
  user {
    id
    userName
    fullName
  }
  rubric {
    id
    name
  }
  territory {
    id
    name
  }
  createdAt
  data
  deleted
  officer {
    id
    name
  }
  region {
    id
    name
  }
  weight
  topic {
    id
    name
  }
  updatedAt
}
    `;
export const PgProtocolRecordLiteFragmentFragmentDoc = gql`
    fragment PgProtocolRecordLiteFragment on ProtocolRecordPageType {
  items {
    ...protocolRecordLiteFragment
  }
  total
}
    ${ProtocolRecordLiteFragmentFragmentDoc}`;
export const RegionFullFragmentFragmentDoc = gql`
    fragment RegionFullFragment on Region {
  ...RegionPubFrag
  ...FTReeRegion
  moderators {
    ...ClerkInSSFragment
  }
}
    ${RegionPubFragFragmentDoc}
${FtReeRegionFragmentDoc}
${ClerkInSsFragmentFragmentDoc}`;
export const RoleLiteFragmentFragmentDoc = gql`
    fragment RoleLiteFragment on RoleType {
  id
  name
  description
  isAdminRegister
}
    `;
export const ClaimFragmentFragmentDoc = gql`
    fragment ClaimFragment on ClaimType {
  type
  value
}
    `;
export const RoleFullFragmentFragmentDoc = gql`
    fragment RoleFullFragment on RoleType {
  ...RoleLiteFragment
  createdAt
  updatedAt
  weight
  disconnected
  disconnectedTimestamp
  editable
  claims {
    ...ClaimFragment
  }
}
    ${RoleLiteFragmentFragmentDoc}
${ClaimFragmentFragmentDoc}`;
export const PaginationRoleFullFragmentFragmentDoc = gql`
    fragment PaginationRoleFullFragment on RolePageType {
  items {
    ...RoleFullFragment
  }
  total
}
    ${RoleFullFragmentFragmentDoc}`;
export const PaginationRoleLiteFragmentFragmentDoc = gql`
    fragment PaginationRoleLiteFragment on RolePageType {
  items {
    ...RoleFullFragment
  }
  total
}
    ${RoleFullFragmentFragmentDoc}`;
export const RubricModeratedFragmentFragmentDoc = gql`
    fragment RubricModeratedFragment on Rubric {
  ...RubricLiteFragment
  moderators {
    ...ClerkInSSFragment
  }
}
    ${RubricLiteFragmentFragmentDoc}
${ClerkInSsFragmentFragmentDoc}`;
export const SettingsFragmentFragmentDoc = gql`
    fragment SettingsFragment on Settings {
  key
  value
  settingType
  valueType
  settingNodeType
  description
  weight
  values {
    displayText
    value
  }
}
    `;
export const SettingsValueFragmentFragmentDoc = gql`
    fragment SettingsValueFragment on Settings {
  key
  value
}
    `;
export const FBaseSubsystemFragmentDoc = gql`
    fragment FBaseSubsystem on Subsystem {
  uID
  deleted
  createdAt
  weight
  updatedAt
  weight
  name
  description
  mainEntityFields {
    nameAPI
    description
    nameAPISecond
    nameDisplayed
    placeholder
    required
    weight
  }
}
    `;
export const FtReeTerritoryFragmentDoc = gql`
    fragment FTReeTerritory on Territory {
  due
  isNode
  parentId
  parentType
}
    `;
export const TerritoryFullFragmentFragmentDoc = gql`
    fragment TerritoryFullFragment on Territory {
  ...TerritoryPubFrag
  moderators {
    ...ClerkInSSFragment
  }
}
    ${TerritoryPubFragFragmentDoc}
${ClerkInSsFragmentFragmentDoc}`;
export const FtReeTopicFragmentDoc = gql`
    fragment FTReeTopic on Topic {
  due
  isNode
  parentId
  parentType
}
    `;
export const FUserClaimsFragmentDoc = gql`
    fragment FUserClaims on User {
  claims {
    ...ClaimFragment
  }
  allClaims {
    ...ClaimFragment
  }
}
    ${ClaimFragmentFragmentDoc}`;
export const UserModFragmentFragmentDoc = gql`
    fragment UserModFragment on User {
  disconnected
  disconnectedTimestamp
  userName
  emailConfirmed
  updatedAt
  loginSystem
  selfFormRegister
  roles {
    ...RoleFullFragment
  }
  ...FUserClaims
}
    ${RoleFullFragmentFragmentDoc}
${FUserClaimsFragmentDoc}`;
export const UserFullFragmentFragmentDoc = gql`
    fragment UserFullFragment on User {
  ...UserPublicFragment
  ...UserModFragment
}
    ${UserPublicFragmentFragmentDoc}
${UserModFragmentFragmentDoc}`;
export const PaginationUserFullTypeFragmentDoc = gql`
    fragment PaginationUserFullType on UserPageType {
  items {
    ...UserFullFragment
  }
  total
}
    ${UserFullFragmentFragmentDoc}`;
export const OrganizFragFragmentDoc = gql`
    fragment OrganizFrag on OrganizationType {
  fullName
  addresses {
    fullAddress
  }
}
    `;
export const VoteRootRoFragmentFragmentDoc = gql`
    fragment VoteRootROFragment on VoteRoot {
  id
  state
  createdAt
  updatedAt
  startedAt
  endedAt
  archivedAt
}
    `;
export const VoteOfferFragmentFragmentDoc = gql`
    fragment VoteOfferFragment on VoteOffer {
  text
  user {
    ...UserPublicFragment
  }
}
    ${UserPublicFragmentFragmentDoc}`;
export const VoteItemLiteFragmentFragmentDoc = gql`
    fragment VoteItemLiteFragment on VoteItem {
  attachedFiles {
    ...AttachedFilePublicFragment
  }
  name
  description
  shortName
  weight
  counter
}
    ${AttachedFilePublicFragmentFragmentDoc}`;
export const VotePublicFragmentFragmentDoc = gql`
    fragment VotePublicFragment on Vote {
  id
  weight
  createdAt
  updatedAt
  mode
  modeLimitValue
  name
  shortName
  text
  textResult
  usersCount
  attachedFiles {
    ...AttachedFilePublicFragment
  }
  offerEnabled
  offers {
    ...VoteOfferFragment
  }
  offersCount
  parentVoteItems {
    ...VoteItemLiteFragment
  }
  voteItems {
    ...VoteItemLiteFragment
  }
}
    ${AttachedFilePublicFragmentFragmentDoc}
${VoteOfferFragmentFragmentDoc}
${VoteItemLiteFragmentFragmentDoc}`;
export const VoteRootPublicFragmentFragmentDoc = gql`
    fragment VoteRootPublicFragment on VoteRoot {
  ...VoteRootROFragment
  attachedFiles {
    ...AttachedFilePublicFragment
  }
  weight
  votes {
    ...VotePublicFragment
  }
  region {
    ...RegionPublicFragment
  }
  rubric {
    ...RubricLiteFragment
  }
  territory {
    ...TerritoryPubFrag
  }
  topic {
    ...TopicPublicFragment
  }
  name
  shortName
  text
  textResult
  commentsAllowed
}
    ${VoteRootRoFragmentFragmentDoc}
${AttachedFilePublicFragmentFragmentDoc}
${VotePublicFragmentFragmentDoc}
${RegionPublicFragmentFragmentDoc}
${RubricLiteFragmentFragmentDoc}
${TerritoryPubFragFragmentDoc}
${TopicPublicFragmentFragmentDoc}`;
export const VoteRootModEditFragmentFragmentDoc = gql`
    fragment VoteRootMODEditFragment on VoteRoot {
  ...VoteRootPublicFragment
  startAt
  endAt
  archiveAt
  deleted
}
    ${VoteRootPublicFragmentFragmentDoc}`;
export const VoteFullFragmentFragmentDoc = gql`
    fragment VoteFullFragment on Vote {
  ...VotePublicFragment
}
    ${VotePublicFragmentFragmentDoc}`;
export const LoginFullFragmentFragmentDoc = gql`
    fragment loginFullFragment on LoginResultType {
  token
  loginStatus
  errorMessage
  user {
    ...UserFullFragment
  }
}
    ${UserFullFragmentFragmentDoc}`;
export const LoginCurrentUserFragmentDoc = gql`
    fragment loginCurrentUser on User {
  id
  fullName
  email
  createdAt
  allClaims {
    type
    value
  }
}
    `;
export const LoginLiteFragmentFragmentDoc = gql`
    fragment loginLiteFragment on LoginResultType {
  token
  loginStatus
  errorMessage
  user {
    ...loginCurrentUser
  }
}
    ${LoginCurrentUserFragmentDoc}`;
export const DeleteRecordDocument = gql`
    mutation deleteRecord($entity: String!, $id: Long!) {
  delete(entity: {name: $entity, id: $id})
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IDeleteRecordGQL extends Apollo.Mutation<IDeleteRecordMutation, IDeleteRecordMutationVariables> {
    document = DeleteRecordDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppeal_1ListDocument = gql`
    query getAppeal_1List($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  appeal(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    total
    items {
      ...AppealModListFragment
    }
  }
}
    ${AppealModListFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppeal_1ListGQL extends Apollo.Query<IGetAppeal_1ListQuery, IGetAppeal_1ListQueryVariables> {
    document = GetAppeal_1ListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppealListDocument = gql`
    query getAppealList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  appeal(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    items {
      ...AppealModSmallListFragment
    }
  }
}
    ${AppealModSmallListFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppealListGQL extends Apollo.Query<IGetAppealListQuery, IGetAppealListQueryVariables> {
    document = GetAppealListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppealListWTotalDocument = gql`
    query getAppealListWTotal($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  appeal(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    total
    items {
      ...AppealModSmallListFragment
    }
  }
}
    ${AppealModSmallListFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppealListWTotalGQL extends Apollo.Query<IGetAppealListWTotalQuery, IGetAppealListWTotalQueryVariables> {
    document = GetAppealListWTotalDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppealListCountDocument = gql`
    query getAppealListCount($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  appeal(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    total
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppealListCountGQL extends Apollo.Query<IGetAppealListCountQuery, IGetAppealListCountQueryVariables> {
    document = GetAppealListCountDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppealOrgListDocument = gql`
    query getAppealOrgList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  appeal(
    options: {range: $range, filters: $filters, withDeleted: $withDeleted, withLinkedByOrganizations: true}
  ) {
    total
    items {
      ...AppealModListFragment
    }
  }
}
    ${AppealModListFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppealOrgListGQL extends Apollo.Query<IGetAppealOrgListQuery, IGetAppealOrgListQueryVariables> {
    document = GetAppealOrgListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppealSListDocument = gql`
    query getAppealSList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  appeal(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    total
    items {
      ...AppealModListFragment
      subsystem {
        uID
      }
    }
  }
}
    ${AppealModListFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppealSListGQL extends Apollo.Query<IGetAppealSListQuery, IGetAppealSListQueryVariables> {
    document = GetAppealSListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppealPublicListDocument = gql`
    query getAppealPublicList($range: Range, $filters: [Filter]) {
  appeal(options: {range: $range, filters: $filters}) {
    total
    items {
      ...AppealPublicFragment
    }
  }
}
    ${AppealPublicFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppealPublicListGQL extends Apollo.Query<IGetAppealPublicListQuery, IGetAppealPublicListQueryVariables> {
    document = GetAppealPublicListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppealDocument = gql`
    query getAppeal($id: String!) {
  appeal(options: {filters: {name: "id", equal: $id}, thumbSize: "400x300"}) {
    total
    items {
      ...AppealModFullFragment
    }
  }
}
    ${AppealModFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppealGQL extends Apollo.Query<IGetAppealQuery, IGetAppealQueryVariables> {
    document = GetAppealDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetPublicAppealDocument = gql`
    query getPublicAppeal($id: String!) {
  appeal(options: {filters: {name: "id", equal: $id}, thumbSize: "400x300"}) {
    total
    items {
      ...AppealPublicFragment
    }
  }
}
    ${AppealPublicFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetPublicAppealGQL extends Apollo.Query<IGetPublicAppealQuery, IGetPublicAppealQueryVariables> {
    document = GetPublicAppealDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateAppealDocument = gql`
    mutation createAppeal($appeal: createAppeal!) {
  createAppeal(appeal: $appeal) {
    ...AppealModFullFragment
  }
}
    ${AppealModFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateAppealGQL extends Apollo.Mutation<ICreateAppealMutation, ICreateAppealMutationVariables> {
    document = CreateAppealDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateAppealDocument = gql`
    mutation updateAppeal($id: ID!, $appeal: updateAppeal!) {
  updateAppeal(id: $id, appeal: $appeal) {
    ...AppealModFullFragment
  }
}
    ${AppealModFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateAppealGQL extends Apollo.Mutation<IUpdateAppealMutation, IUpdateAppealMutationVariables> {
    document = UpdateAppealDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ApproveAppealDocument = gql`
    mutation approveAppeal($id: ID!, $approve: approveAppeal!) {
  approveAppeal(id: $id, appealApprove: $approve) {
    id
    approved
    rejectionReason
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IApproveAppealGQL extends Apollo.Mutation<IApproveAppealMutation, IApproveAppealMutationVariables> {
    document = ApproveAppealDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RegisterAppealDocument = gql`
    mutation registerAppeal($id: ID!) {
  registerAppeal(id: $id) {
    id
    approved
    rejectionReason
    deloStatus
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IRegisterAppealGQL extends Apollo.Mutation<IRegisterAppealMutation, IRegisterAppealMutationVariables> {
    document = RegisterAppealDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppointmentRangeListDocument = gql`
    query getAppointmentRangeList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  appointmentRange(
    options: {range: $range, filters: $filters, withDeleted: $withDeleted}
  ) {
    ...PgAppointmentRangeLiteFragment
  }
}
    ${PgAppointmentRangeLiteFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppointmentRangeListGQL extends Apollo.Query<IGetAppointmentRangeListQuery, IGetAppointmentRangeListQueryVariables> {
    document = GetAppointmentRangeListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppointmentListDocument = gql`
    query getAppointmentList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  appointment(
    options: {range: $range, filters: $filters, withDeleted: $withDeleted}
  ) {
    ...PgAppointmentFullFragment
  }
}
    ${PgAppointmentFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppointmentListGQL extends Apollo.Query<IGetAppointmentListQuery, IGetAppointmentListQueryVariables> {
    document = GetAppointmentListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetAppointmentDocument = gql`
    query getAppointment($id: String) {
  appointment(filter: {name: "id", equal: $id}) {
    ...PgAppointmentFullFragment
  }
}
    ${PgAppointmentFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetAppointmentGQL extends Apollo.Query<IGetAppointmentQuery, IGetAppointmentQueryVariables> {
    document = GetAppointmentDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AttachSetIsPublicDocument = gql`
    mutation attachSetIsPublic($id: ID!, $isPublic: Boolean!) {
  approveAttachedFile(id: $id, attachedFileApprove: {public_: $isPublic}) {
    id
    isPublic
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IAttachSetIsPublicGQL extends Apollo.Mutation<IAttachSetIsPublicMutation, IAttachSetIsPublicMutationVariables> {
    document = AttachSetIsPublicDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetClaimInfosDocument = gql`
    query getClaimInfos {
  claimsInfo {
    ...ClaimsInfoFragment
  }
}
    ${ClaimsInfoFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetClaimInfosGQL extends Apollo.Query<IGetClaimInfosQuery, IGetClaimInfosQueryVariables> {
    document = GetClaimInfosDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateMessageDocument = gql`
    mutation createMessage($message: createMessage!) {
  createMessage(message: $message) {
    ...MessageFragment
  }
}
    ${MessageFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateMessageGQL extends Apollo.Mutation<ICreateMessageMutation, ICreateMessageMutationVariables> {
    document = CreateMessageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateMessage2AppealDocument = gql`
    mutation createMessage2Appeal($parentId: ID!, $text: String!) {
  createMessage(
    message: {parentId: $parentId, parentType: "Appeal", text: $text, kind: NORMAL}
  ) {
    ...MessageFragment
  }
}
    ${MessageFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateMessage2AppealGQL extends Apollo.Mutation<ICreateMessage2AppealMutation, ICreateMessage2AppealMutationVariables> {
    document = CreateMessage2AppealDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateMessage2MessageDocument = gql`
    mutation createMessage2Message($parentId: ID!, $text: String!) {
  createMessage(
    message: {parentId: $parentId, parentType: "Message", text: $text}
  ) {
    ...MessageFragment
  }
}
    ${MessageFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateMessage2MessageGQL extends Apollo.Mutation<ICreateMessage2MessageMutation, ICreateMessage2MessageMutationVariables> {
    document = CreateMessage2MessageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateMessageDocument = gql`
    mutation updateMessage($id: ID!, $message: updateMessage!) {
  updateMessage(id: $id, message: $message) {
    ...MessageFragment
  }
}
    ${MessageFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateMessageGQL extends Apollo.Mutation<IUpdateMessageMutation, IUpdateMessageMutationVariables> {
    document = UpdateMessageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ApproveMessageDocument = gql`
    mutation approveMessage($id: ID!) {
  approveMessage(
    id: $id
    messageApprove: {approvedEnum: ACCEPTED, moderationStage: DONE, public_: true}
  ) {
    total
    items {
      ...MessageFragment
    }
  }
}
    ${MessageFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IApproveMessageGQL extends Apollo.Mutation<IApproveMessageMutation, IApproveMessageMutationVariables> {
    document = ApproveMessageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LikeMessageDocument = gql`
    mutation likeMessage($id: ID, $operation: LikeRequestEnum!) {
  likeMessage(like: {id: $id, liked: $operation}) {
    ...LikeReportFragment
  }
}
    ${LikeReportFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ILikeMessageGQL extends Apollo.Mutation<ILikeMessageMutation, ILikeMessageMutationVariables> {
    document = LikeMessageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const RejectMessageDocument = gql`
    mutation rejectMessage($id: ID!, $reason: String!) {
  approveMessage(
    id: $id
    messageApprove: {approvedEnum: REJECTED, moderationStage: DONE, public_: false, reason: $reason}
  ) {
    total
    items {
      ...MessageFragment
    }
  }
}
    ${MessageFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IRejectMessageGQL extends Apollo.Mutation<IRejectMessageMutation, IRejectMessageMutationVariables> {
    document = RejectMessageDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const MessageListDocument = gql`
    query messageList($id: Long!, $type: String!) {
  messageList(
    options: {thumbSize: "280x200", withDeleted: true}
    id: $id
    type: $type
  ) {
    total
    items {
      ...MessageFragment
    }
  }
}
    ${MessageFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IMessageListGQL extends Apollo.Query<IMessageListQuery, IMessageListQueryVariables> {
    document = MessageListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ReadAllEnumsDocument = gql`
    query readAllEnums {
  appealStatus: enumVerb(
    options: {filters: [{name: "group", equal: "appealStatus", op: AND}], range: {orderBy: {field: "value"}}}
  ) {
    items {
      ...EnumRec
    }
    total
  }
  feedbackStatus: enumVerb(
    options: {filters: [{name: "group", equal: "feedbackStatus", op: AND}], range: {orderBy: {field: "value"}}}
  ) {
    items {
      ...EnumRec
    }
    total
  }
  externalLink: enumVerb(
    options: {filters: [{name: "group", equal: "externalLink", op: AND}], range: {orderBy: {field: "name"}}}
  ) {
    items {
      ...EnumRec
    }
    total
  }
  groupDescriptor: enumVerb(
    options: {filters: [{name: "group", equal: "groupDescriptor", op: AND}], range: {orderBy: {field: "value"}}}
  ) {
    items {
      ...EnumRec
    }
    total
  }
}
    ${EnumRecFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IReadAllEnumsGQL extends Apollo.Query<IReadAllEnumsQuery, IReadAllEnumsQueryVariables> {
    document = ReadAllEnumsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetEnumVerbModListDocument = gql`
    query getEnumVerbModList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  enumVerb(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    total
    items {
      ...EnumRec
      customSettings
    }
  }
}
    ${EnumRecFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetEnumVerbModListGQL extends Apollo.Query<IGetEnumVerbModListQuery, IGetEnumVerbModListQueryVariables> {
    document = GetEnumVerbModListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetEnumVerbModByIdDocument = gql`
    query getEnumVerbModById($id: String!) {
  enumVerb(options: {filters: [{name: "id", equal: $id}]}) {
    items {
      ...EnumRec
      customSettings
    }
    total
  }
}
    ${EnumRecFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetEnumVerbModByIdGQL extends Apollo.Query<IGetEnumVerbModByIdQuery, IGetEnumVerbModByIdQueryVariables> {
    document = GetEnumVerbModByIdDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateEnumVerbDocument = gql`
    mutation createEnumVerb($enumVerb: createEnumVerb) {
  createEnumVerb(enumVerb: $enumVerb) {
    ...EnumRec
  }
}
    ${EnumRecFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateEnumVerbGQL extends Apollo.Mutation<ICreateEnumVerbMutation, ICreateEnumVerbMutationVariables> {
    document = CreateEnumVerbDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateEnumVerbDocument = gql`
    mutation updateEnumVerb($enumVerb: updateEnumVerb) {
  updateEnumVerb(enumVerb: $enumVerb) {
    ...EnumRec
  }
}
    ${EnumRecFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateEnumVerbGQL extends Apollo.Mutation<IUpdateEnumVerbMutation, IUpdateEnumVerbMutationVariables> {
    document = UpdateEnumVerbDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AvailableExecutorsDocument = gql`
    query availableExecutors {
  availableExecutors(subsystemUid: ["citizens"]) {
    total
    items {
      ...UserPublicFragment
    }
  }
}
    ${UserPublicFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IAvailableExecutorsGQL extends Apollo.Query<IAvailableExecutorsQuery, IAvailableExecutorsQueryVariables> {
    document = AvailableExecutorsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetExecutorAppealsListDocument = gql`
    query getExecutorAppealsList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  appealWhereExecutor(
    subsystemUIDs: ["citizens"]
    options: {range: $range, filters: $filters, withDeleted: $withDeleted}
  ) {
    total
    items {
      ...AppealModListFragment
      subsystem {
        uID
      }
    }
  }
}
    ${AppealModListFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetExecutorAppealsListGQL extends Apollo.Query<IGetExecutorAppealsListQuery, IGetExecutorAppealsListQueryVariables> {
    document = GetExecutorAppealsListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const FileUploadDocument = gql`
    mutation fileUpload($file: FileUpload!) {
  fileUpload(file: $file) {
    ...FileUploadResultFragment
  }
}
    ${FileUploadResultFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IFileUploadGQL extends Apollo.Mutation<IFileUploadMutation, IFileUploadMutationVariables> {
    document = FileUploadDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetJournalListDocument = gql`
    query getJournalList($range: Range, $filters: [Filter]) {
  journal(range: $range, filter: $filters) {
    items {
      entityKey
      entityName
      friendlyName
      operation
      operationName
      timestamp
      properties {
        friendlyName
        newValue
        oldValue
        tag
      }
      user {
        id
        userName
        fullName
      }
    }
    total
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetJournalListGQL extends Apollo.Query<IGetJournalListQuery, IGetJournalListQueryVariables> {
    document = GetJournalListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetJournalDocument = gql`
    query getJournal($entityName: String, $key: String) {
  journal(
    filter: [{name: "entityName", equal: $entityName, op: AND}, {name: "entityKey", equal: $key, op: AND}]
  ) {
    items {
      entityKey
      entityName
      friendlyName
      operation
      operationName
      timestamp
      properties {
        friendlyName
        newValue
        oldValue
        newValueFriendly
        tag
      }
      user {
        id
        userName
        fullName
      }
    }
    total
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetJournalGQL extends Apollo.Query<IGetJournalQuery, IGetJournalQueryVariables> {
    document = GetJournalDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AvailableModeratorsDocument = gql`
    query availableModerators {
  availableModerators(subsystemUid: ["organizations", "citizens"]) {
    total
    items {
      ...UserPublicFragment
    }
  }
}
    ${UserPublicFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IAvailableModeratorsGQL extends Apollo.Query<IAvailableModeratorsQuery, IAvailableModeratorsQueryVariables> {
    document = AvailableModeratorsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AvailableModeratorsForDocument = gql`
    query availableModeratorsFor($subsystemUid: [String!]) {
  availableModerators(subsystemUid: $subsystemUid) {
    total
    items {
      ...UserPublicFragment
    }
  }
}
    ${UserPublicFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IAvailableModeratorsForGQL extends Apollo.Query<IAvailableModeratorsForQuery, IAvailableModeratorsForQueryVariables> {
    document = AvailableModeratorsForDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetNewsModListDocument = gql`
    query getNewsModList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  news(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    total
    items {
      ...NewsPubFragList
    }
  }
}
    ${NewsPubFragListFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetNewsModListGQL extends Apollo.Query<IGetNewsModListQuery, IGetNewsModListQueryVariables> {
    document = GetNewsModListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetNewsModByIdDocument = gql`
    query getNewsModById($id: String!) {
  news(filter: {name: "id", equal: $id}) {
    total
    items {
      ...NewsPubFragItem
    }
  }
}
    ${NewsPubFragItemFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetNewsModByIdGQL extends Apollo.Query<IGetNewsModByIdQuery, IGetNewsModByIdQueryVariables> {
    document = GetNewsModByIdDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateNewsDocument = gql`
    mutation createNews($news: createNews!) {
  createNews(news: $news) {
    ...NewsPubFragItem
  }
}
    ${NewsPubFragItemFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateNewsGQL extends Apollo.Mutation<ICreateNewsMutation, ICreateNewsMutationVariables> {
    document = CreateNewsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateNewsDocument = gql`
    mutation updateNews($id: ID!, $news: updateNews!) {
  updateNews(id: $id, news: $news) {
    ...NewsPubFragItem
  }
}
    ${NewsPubFragItemFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateNewsGQL extends Apollo.Mutation<IUpdateNewsMutation, IUpdateNewsMutationVariables> {
    document = UpdateNewsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const SetNewsPublishStateDocument = gql`
    mutation setNewsPublishState($id: ID!, $state: NewsStateEnum!) {
  approveNews(id: $id, newsApprove: {state: $state}) {
    ...NewsPubFragItem
  }
}
    ${NewsPubFragItemFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ISetNewsPublishStateGQL extends Apollo.Mutation<ISetNewsPublishStateMutation, ISetNewsPublishStateMutationVariables> {
    document = SetNewsPublishStateDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetOfficerDocument = gql`
    query getOfficer($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  officer(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    ...PgOfficerFragment
  }
}
    ${PgOfficerFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetOfficerGQL extends Apollo.Query<IGetOfficerQuery, IGetOfficerQueryVariables> {
    document = GetOfficerDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetProtocolsDocument = gql`
    query getProtocols($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  protocolRecord(
    options: {range: $range, filters: $filters, withDeleted: $withDeleted}
  ) {
    ...PgProtocolRecordLiteFragment
  }
}
    ${PgProtocolRecordLiteFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetProtocolsGQL extends Apollo.Query<IGetProtocolsQuery, IGetProtocolsQueryVariables> {
    document = GetProtocolsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetRegionListDocument = gql`
    query getRegionList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  region(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    total
    items {
      ...RegionPubFrag
    }
  }
}
    ${RegionPubFragFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetRegionListGQL extends Apollo.Query<IGetRegionListQuery, IGetRegionListQueryVariables> {
    document = GetRegionListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetRegionByIdDocument = gql`
    query getRegionById($id: String!) {
  region(filter: {name: "ID", equal: $id}) {
    total
    items {
      ...RegionFullFragment
    }
  }
}
    ${RegionFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetRegionByIdGQL extends Apollo.Query<IGetRegionByIdQuery, IGetRegionByIdQueryVariables> {
    document = GetRegionByIdDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateRegionDocument = gql`
    mutation createRegion($region: createRegion!) {
  createRegion(region: $region) {
    ...RegionFullFragment
  }
}
    ${RegionFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateRegionGQL extends Apollo.Mutation<ICreateRegionMutation, ICreateRegionMutationVariables> {
    document = CreateRegionDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateRegionDocument = gql`
    mutation updateRegion($id: ID!, $region: updateRegion!) {
  updateRegion(id: $id, region: $region) {
    ...RegionFullFragment
  }
}
    ${RegionFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateRegionGQL extends Apollo.Mutation<IUpdateRegionMutation, IUpdateRegionMutationVariables> {
    document = UpdateRegionDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetReportsListDocument = gql`
    query getReportsList {
  reports: reportSettings {
    reportId
    name
    description
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetReportsListGQL extends Apollo.Query<IGetReportsListQuery, IGetReportsListQueryVariables> {
    document = GetReportsListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetReportSettingsDocument = gql`
    query getReportSettings($id: Guid) {
  reportSettings(reportId: $id) {
    reportId
    name
    description
    parameters {
      allEnable
      allSelected
      controlType
      name
      options
      required
      title
      value
      values
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetReportSettingsGQL extends Apollo.Query<IGetReportSettingsQuery, IGetReportSettingsQueryVariables> {
    document = GetReportSettingsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ExecuteReportDocument = gql`
    query executeReport($settings: ReportSettingsInput!) {
  executeReport(reportSettings: $settings) {
    id
    success
    template
    errors
    file {
      ...AttachedFileFragment
    }
  }
}
    ${AttachedFileFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IExecuteReportGQL extends Apollo.Query<IExecuteReportQuery, IExecuteReportQueryVariables> {
    document = ExecuteReportDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetRoleDocument = gql`
    query getRole($id: String!) {
  role(filter: {name: "id", equal: $id}) {
    ...PaginationRoleFullFragment
  }
}
    ${PaginationRoleFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetRoleGQL extends Apollo.Query<IGetRoleQuery, IGetRoleQueryVariables> {
    document = GetRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetRoleListDocument = gql`
    query getRoleList($range: Range, $filters: [Filter]) {
  role(range: $range, filter: $filters) {
    ...PaginationRoleFullFragment
  }
}
    ${PaginationRoleFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetRoleListGQL extends Apollo.Query<IGetRoleListQuery, IGetRoleListQueryVariables> {
    document = GetRoleListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetRoleListFullDocument = gql`
    query getRoleListFull($range: Range, $filters: [Filter]) {
  role(range: $range, filter: $filters) {
    ...PaginationRoleLiteFragment
  }
}
    ${PaginationRoleLiteFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetRoleListFullGQL extends Apollo.Query<IGetRoleListFullQuery, IGetRoleListFullQueryVariables> {
    document = GetRoleListFullDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateRole1Document = gql`
    mutation updateRole1($id: ID!, $role: RoleInputType!) {
  updateRole(id: $id, role: $role) {
    ...RoleFullFragment
  }
}
    ${RoleFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateRole1GQL extends Apollo.Mutation<IUpdateRole1Mutation, IUpdateRole1MutationVariables> {
    document = UpdateRole1Document;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateRoleDocument = gql`
    mutation updateRole($id: ID!, $role: RoleInputType!) {
  updateRole(id: $id, role: $role) {
    ...RoleFullFragment
  }
}
    ${RoleFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateRoleGQL extends Apollo.Mutation<IUpdateRoleMutation, IUpdateRoleMutationVariables> {
    document = UpdateRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateRoleDocument = gql`
    mutation createRole($role: RoleInputType!) {
  createRole(role: $role) {
    ...RoleFullFragment
  }
}
    ${RoleFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateRoleGQL extends Apollo.Mutation<ICreateRoleMutation, ICreateRoleMutationVariables> {
    document = CreateRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const DeleteRoleDocument = gql`
    mutation deleteRole($id: ID!) {
  removeRole(id: $id)
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IDeleteRoleGQL extends Apollo.Mutation<IDeleteRoleMutation, IDeleteRoleMutationVariables> {
    document = DeleteRoleDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetRubricListDocument = gql`
    query getRubricList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  rubric(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    total
    items {
      ...RubricLiteFragment
    }
  }
}
    ${RubricLiteFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetRubricListGQL extends Apollo.Query<IGetRubricListQuery, IGetRubricListQueryVariables> {
    document = GetRubricListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetRubricByIdDocument = gql`
    query getRubricByID($id: String!) {
  rubric(filter: {name: "id", equal: $id}) {
    total
    items {
      ...RubricModeratedFragment
    }
  }
}
    ${RubricModeratedFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetRubricByIdGQL extends Apollo.Query<IGetRubricByIdQuery, IGetRubricByIdQueryVariables> {
    document = GetRubricByIdDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateRubricDocument = gql`
    mutation updateRubric($id: ID!, $rubric: updateRubric!) {
  updateRubric(id: $id, rubric: $rubric) {
    ...RubricModeratedFragment
  }
}
    ${RubricModeratedFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateRubricGQL extends Apollo.Mutation<IUpdateRubricMutation, IUpdateRubricMutationVariables> {
    document = UpdateRubricDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateRubricDocument = gql`
    mutation createRubric($rubric: createRubric!) {
  createRubric(rubric: $rubric) {
    ...RubricModeratedFragment
  }
}
    ${RubricModeratedFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateRubricGQL extends Apollo.Mutation<ICreateRubricMutation, ICreateRubricMutationVariables> {
    document = CreateRubricDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetSettingsDocument = gql`
    query getSettings($pathes: String!) {
  settings(key: [$pathes]) {
    ...SettingsFragment
  }
}
    ${SettingsFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetSettingsGQL extends Apollo.Query<IGetSettingsQuery, IGetSettingsQueryVariables> {
    document = GetSettingsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const SetSettingsDocument = gql`
    mutation setSettings($settings: [updateSettings]!) {
  setSettings(settings: $settings) {
    ...SettingsValueFragment
  }
}
    ${SettingsValueFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ISetSettingsGQL extends Apollo.Mutation<ISetSettingsMutation, ISetSettingsMutationVariables> {
    document = SetSettingsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const NewSettingsDocument = gql`
    subscription newSettings($keys: [String]) {
  newSettings(startsWith: $keys) {
    ...SettingsValueFragment
  }
}
    ${SettingsValueFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class INewSettingsGQL extends Apollo.Subscription<INewSettingsSubscription, INewSettingsSubscriptionVariables> {
    document = NewSettingsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetSubSystemListDocument = gql`
    query getSubSystemList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  subsystem(
    options: {range: $range, filters: $filters, withDeleted: $withDeleted}
  ) {
    total
    items {
      ...FBaseSubsystem
    }
  }
}
    ${FBaseSubsystemFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetSubSystemListGQL extends Apollo.Query<IGetSubSystemListQuery, IGetSubSystemListQueryVariables> {
    document = GetSubSystemListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetSubSystemDocument = gql`
    query getSubSystem($id: String!) {
  subsystem(filter: {name: "uID", equal: $id}) {
    total
    items {
      ...FBaseSubsystem
    }
  }
}
    ${FBaseSubsystemFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetSubSystemGQL extends Apollo.Query<IGetSubSystemQuery, IGetSubSystemQueryVariables> {
    document = GetSubSystemDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateSubSystemDocument = gql`
    mutation updateSubSystem($id: ID!, $subsystem: updateSubsystem!) {
  updateSubsystem(uid: $id, subsystem: $subsystem) {
    ...FBaseSubsystem
  }
}
    ${FBaseSubsystemFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateSubSystemGQL extends Apollo.Mutation<IUpdateSubSystemMutation, IUpdateSubSystemMutationVariables> {
    document = UpdateSubSystemDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetTerritoryListDocument = gql`
    query getTerritoryList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  territory(
    options: {range: $range, filters: $filters, withDeleted: $withDeleted}
  ) {
    total
    items {
      ...TerritoryPubFrag
    }
  }
}
    ${TerritoryPubFragFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetTerritoryListGQL extends Apollo.Query<IGetTerritoryListQuery, IGetTerritoryListQueryVariables> {
    document = GetTerritoryListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetTerritoryByIdDocument = gql`
    query getTerritoryById($id: String!) {
  territory(filter: {name: "ID", equal: $id}) {
    total
    items {
      ...TerritoryFullFragment
    }
  }
}
    ${TerritoryFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetTerritoryByIdGQL extends Apollo.Query<IGetTerritoryByIdQuery, IGetTerritoryByIdQueryVariables> {
    document = GetTerritoryByIdDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateTerritoryDocument = gql`
    mutation updateTerritory($id: ID!, $territory: updateTerritory!) {
  updateTerritory(id: $id, territory: $territory) {
    ...TerritoryFullFragment
  }
}
    ${TerritoryFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateTerritoryGQL extends Apollo.Mutation<IUpdateTerritoryMutation, IUpdateTerritoryMutationVariables> {
    document = UpdateTerritoryDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateTerritoryDocument = gql`
    mutation createTerritory($territory: createTerritory!) {
  createTerritory(territory: $territory) {
    ...TerritoryFullFragment
  }
}
    ${TerritoryFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateTerritoryGQL extends Apollo.Mutation<ICreateTerritoryMutation, ICreateTerritoryMutationVariables> {
    document = CreateTerritoryDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetTopicListDocument = gql`
    query getTopicList($range: Range, $filters: [Filter], $withDeleted: Boolean) {
  topic(options: {range: $range, filters: $filters, withDeleted: $withDeleted}) {
    total
    items {
      ...TopicPubFrag
    }
  }
}
    ${TopicPubFragFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetTopicListGQL extends Apollo.Query<IGetTopicListQuery, IGetTopicListQueryVariables> {
    document = GetTopicListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetTopicDocument = gql`
    query getTopic($id: String!) {
  topic(filter: {name: "id", equal: $id}) {
    total
    items {
      ...TopicFullFragment
    }
  }
}
    ${TopicFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetTopicGQL extends Apollo.Query<IGetTopicQuery, IGetTopicQueryVariables> {
    document = GetTopicDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateTopicDocument = gql`
    mutation createTopic($topic: createTopic!) {
  createTopic(topic: $topic) {
    ...TopicFullFragment
  }
}
    ${TopicFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateTopicGQL extends Apollo.Mutation<ICreateTopicMutation, ICreateTopicMutationVariables> {
    document = CreateTopicDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateTopicDocument = gql`
    mutation updateTopic($id: ID!, $topic: updateTopic!) {
  updateTopic(id: $id, topic: $topic) {
    ...TopicFullFragment
  }
}
    ${TopicFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateTopicGQL extends Apollo.Mutation<IUpdateTopicMutation, IUpdateTopicMutationVariables> {
    document = UpdateTopicDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetUserListDocument = gql`
    query getUserList($range: Range, $filters: [Filter]) {
  user(filter: $filters, range: $range) {
    items {
      userName
      ...UserPublicFragment
      ...FUserClaims
    }
    total
  }
}
    ${UserPublicFragmentFragmentDoc}
${FUserClaimsFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetUserListGQL extends Apollo.Query<IGetUserListQuery, IGetUserListQueryVariables> {
    document = GetUserListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetUserByIdDocument = gql`
    query getUserByID($id: String!) {
  user(filter: {name: "id", equal: $id}) {
    ...PaginationUserFullType
  }
  userOrganization(filter: {name: "id", equal: $id}) {
    total
    items {
      ...OrganizFrag
    }
  }
}
    ${PaginationUserFullTypeFragmentDoc}
${OrganizFragFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetUserByIdGQL extends Apollo.Query<IGetUserByIdQuery, IGetUserByIdQueryVariables> {
    document = GetUserByIdDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateUserDocument = gql`
    mutation updateUser($id: ID!, $user: UserInputType!) {
  updateUser(id: $id, user: $user) {
    ...UserFullFragment
  }
}
    ${UserFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateUserGQL extends Apollo.Mutation<IUpdateUserMutation, IUpdateUserMutationVariables> {
    document = UpdateUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateUserDocument = gql`
    mutation createUser($user: RegisterModelType!) {
  createUser(user: $user) {
    ...UserFullFragment
  }
}
    ${UserFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateUserGQL extends Apollo.Mutation<ICreateUserMutation, ICreateUserMutationVariables> {
    document = CreateUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const ResetPasswordDocument = gql`
    mutation resetPassword($resetPassword: ResetPasswordInputType!) {
  resetPassword(resetPassword: $resetPassword) {
    errorMessages
    success
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IResetPasswordGQL extends Apollo.Mutation<IResetPasswordMutation, IResetPasswordMutationVariables> {
    document = ResetPasswordDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const VersionsInfoDocument = gql`
    query versionsInfo {
  apiInformation {
    appBuildTime
    appConfiguration
    appVersion
    aspDotnetVersion
    aSPNETCORE_ENVIRONMENT
    dbVersion
    osDescription
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class IVersionsInfoGQL extends Apollo.Query<IVersionsInfoQuery, IVersionsInfoQueryVariables> {
    document = VersionsInfoDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetVoteRootPublicListDocument = gql`
    query getVoteRootPublicList($range: Range, $filter: [Filter]) {
  voteRoot(options: {range: $range, filters: $filter, thumbSize: "280x200"}) {
    total
    items {
      ...VoteRootPublicFragment
    }
  }
}
    ${VoteRootPublicFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetVoteRootPublicListGQL extends Apollo.Query<IGetVoteRootPublicListQuery, IGetVoteRootPublicListQueryVariables> {
    document = GetVoteRootPublicListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetVoteRootModListDocument = gql`
    query getVoteRootModList($range: Range, $filter: [Filter]) {
  voteRoot(options: {range: $range, filters: $filter, thumbSize: "280x200"}) {
    total
    items {
      ...VoteRootMODEditFragment
    }
  }
}
    ${VoteRootModEditFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetVoteRootModListGQL extends Apollo.Query<IGetVoteRootModListQuery, IGetVoteRootModListQueryVariables> {
    document = GetVoteRootModListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetVoteRootModIdDocument = gql`
    query getVoteRootModId($id: String!) {
  voteRoot(options: {filters: {name: "id", equal: $id}, thumbSize: "280x200"}) {
    total
    items {
      ...VoteRootMODEditFragment
    }
  }
}
    ${VoteRootModEditFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetVoteRootModIdGQL extends Apollo.Query<IGetVoteRootModIdQuery, IGetVoteRootModIdQueryVariables> {
    document = GetVoteRootModIdDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateVoteRootModDocument = gql`
    mutation createVoteRootMod($voteRoot: createVoteRoot!) {
  createVoteRoot(voteRoot: $voteRoot) {
    ...VoteRootMODEditFragment
  }
}
    ${VoteRootModEditFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateVoteRootModGQL extends Apollo.Mutation<ICreateVoteRootModMutation, ICreateVoteRootModMutationVariables> {
    document = CreateVoteRootModDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateVoteRootModDocument = gql`
    mutation updateVoteRootMod($id: ID!, $voteRoot: updateVoteRoot!) {
  updateVoteRoot(id: $id, voteRoot: $voteRoot) {
    ...VoteRootMODEditFragment
  }
}
    ${VoteRootModEditFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateVoteRootModGQL extends Apollo.Mutation<IUpdateVoteRootModMutation, IUpdateVoteRootModMutationVariables> {
    document = UpdateVoteRootModDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetVotesListDocument = gql`
    query getVotesList($range: Range, $filter: [Filter]) {
  vote(range: $range, filter: $filter) {
    total
    items {
      ...VotePublicFragment
    }
  }
}
    ${VotePublicFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetVotesListGQL extends Apollo.Query<IGetVotesListQuery, IGetVotesListQueryVariables> {
    document = GetVotesListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetVotesPublicListDocument = gql`
    query getVotesPublicList($range: Range, $filter: [Filter]) {
  vote(options: {range: $range, filters: $filter, thumbSize: "280x200"}) {
    total
    items {
      ...VotePublicFragment
    }
  }
}
    ${VotePublicFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetVotesPublicListGQL extends Apollo.Query<IGetVotesPublicListQuery, IGetVotesPublicListQueryVariables> {
    document = GetVotesPublicListDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetVoteDocument = gql`
    query getVote($id: String!) {
  vote(options: {filters: {name: "id", equal: $id}, thumbSize: "150x100"}) {
    items {
      ...VotePublicFragment
    }
  }
}
    ${VotePublicFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetVoteGQL extends Apollo.Query<IGetVoteQuery, IGetVoteQueryVariables> {
    document = GetVoteDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetWidgetVotesDocument = gql`
    query getWidgetVotes {
  inProgress: vote(filter: [{name: "state", equal: "IN_PROGRESS"}]) {
    total
  }
  isDone: vote(filter: [{name: "state", equal: "IS_DONE"}]) {
    total
  }
  isPlanned: vote(filter: [{name: "state", equal: "IS_PLANNED"}]) {
    total
  }
  hots: vote(
    options: {range: {take: 4, orderBy: [{field: "createdAt", direction: DESC}]}, filters: [{name: "state", equal: "IN_PROGRESS"}], thumbSize: "280x200"}
  ) {
    total
    items {
      ...VotePublicFragment
    }
  }
}
    ${VotePublicFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetWidgetVotesGQL extends Apollo.Query<IGetWidgetVotesQuery, IGetWidgetVotesQueryVariables> {
    document = GetWidgetVotesDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const CreateVoteDocument = gql`
    mutation createVote($vote: createVote!) {
  createVote(vote: $vote) {
    ...VoteFullFragment
  }
}
    ${VoteFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ICreateVoteGQL extends Apollo.Mutation<ICreateVoteMutation, ICreateVoteMutationVariables> {
    document = CreateVoteDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateVoteDocument = gql`
    mutation updateVote($id: ID!, $vote: updateVote!) {
  updateVote(id: $id, vote: $vote) {
    ...VoteFullFragment
  }
}
    ${VoteFullFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateVoteGQL extends Apollo.Mutation<IUpdateVoteMutation, IUpdateVoteMutationVariables> {
    document = UpdateVoteDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const VoteStartDocument = gql`
    mutation voteStart($id: ID!) {
  voteStart(id: $id) {
    ...VoteRootMODEditFragment
  }
}
    ${VoteRootModEditFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IVoteStartGQL extends Apollo.Mutation<IVoteStartMutation, IVoteStartMutationVariables> {
    document = VoteStartDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const VoteStopDocument = gql`
    mutation voteStop($id: ID!) {
  voteStop(id: $id) {
    ...VoteRootMODEditFragment
  }
}
    ${VoteRootModEditFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IVoteStopGQL extends Apollo.Mutation<IVoteStopMutation, IVoteStopMutationVariables> {
    document = VoteStopDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LoginDocument = gql`
    query login($login: String!, $password: String!) {
  login(login: {login: $login, password: $password}) {
    ...loginLiteFragment
  }
}
    ${LoginLiteFragmentFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class ILoginGQL extends Apollo.Query<ILoginQuery, ILoginQueryVariables> {
    document = LoginDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetCurrentUserDocument = gql`
    query getCurrentUser {
  currentUser {
    ...loginCurrentUser
  }
}
    ${LoginCurrentUserFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IGetCurrentUserGQL extends Apollo.Query<IGetCurrentUserQuery, IGetCurrentUserQueryVariables> {
    document = GetCurrentUserDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const UpdateTokenDocument = gql`
    query updateToken {
  updateToken {
    errorMessage
    loginStatus
    token
    user {
      ...loginCurrentUser
    }
  }
}
    ${LoginCurrentUserFragmentDoc}`;

  @Injectable({
    providedIn: 'root'
  })
  export class IUpdateTokenGQL extends Apollo.Query<IUpdateTokenQuery, IUpdateTokenQueryVariables> {
    document = UpdateTokenDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const Test00002createTerritoryDocument = gql`
    mutation test00002createTerritory {
  ct1: createTerritory(territory: {name: "test1"}) {
    id
    weight
  }
  ct2: createTerritory(territory: {name: "test2"}) {
    id
    weight
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ITest00002createTerritoryGQL extends Apollo.Mutation<ITest00002createTerritoryMutation, ITest00002createTerritoryMutationVariables> {
    document = Test00002createTerritoryDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const Test00002updateTerritoryDocument = gql`
    mutation test00002updateTerritory($id: ID!) {
  ut1: updateTerritory(id: $id, territory: {name: "test1_updated"}) {
    id
    weight
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class ITest00002updateTerritoryGQL extends Apollo.Mutation<ITest00002updateTerritoryMutation, ITest00002updateTerritoryMutationVariables> {
    document = Test00002updateTerritoryDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }

  type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

  interface WatchQueryOptionsAlone<V>
    extends Omit<ApolloCore.WatchQueryOptions<V>, 'query' | 'variables'> {}
    
  interface QueryOptionsAlone<V>
    extends Omit<ApolloCore.QueryOptions<V>, 'query' | 'variables'> {}
    
  interface MutationOptionsAlone<T, V>
    extends Omit<ApolloCore.MutationOptions<T, V>, 'mutation' | 'variables'> {}
    
  interface SubscriptionOptionsAlone<V>
    extends Omit<ApolloCore.SubscriptionOptions<V>, 'query' | 'variables'> {}

  @Injectable({ providedIn: 'root' })
  export class ApolloAngularSDK {
    constructor(
      private iDeleteRecordGql: IDeleteRecordGQL,
      private iGetAppeal_1ListGql: IGetAppeal_1ListGQL,
      private iGetAppealListGql: IGetAppealListGQL,
      private iGetAppealListWTotalGql: IGetAppealListWTotalGQL,
      private iGetAppealListCountGql: IGetAppealListCountGQL,
      private iGetAppealOrgListGql: IGetAppealOrgListGQL,
      private iGetAppealSListGql: IGetAppealSListGQL,
      private iGetAppealPublicListGql: IGetAppealPublicListGQL,
      private iGetAppealGql: IGetAppealGQL,
      private iGetPublicAppealGql: IGetPublicAppealGQL,
      private iCreateAppealGql: ICreateAppealGQL,
      private iUpdateAppealGql: IUpdateAppealGQL,
      private iApproveAppealGql: IApproveAppealGQL,
      private iRegisterAppealGql: IRegisterAppealGQL,
      private iGetAppointmentRangeListGql: IGetAppointmentRangeListGQL,
      private iGetAppointmentListGql: IGetAppointmentListGQL,
      private iGetAppointmentGql: IGetAppointmentGQL,
      private iAttachSetIsPublicGql: IAttachSetIsPublicGQL,
      private iGetClaimInfosGql: IGetClaimInfosGQL,
      private iCreateMessageGql: ICreateMessageGQL,
      private iCreateMessage2AppealGql: ICreateMessage2AppealGQL,
      private iCreateMessage2MessageGql: ICreateMessage2MessageGQL,
      private iUpdateMessageGql: IUpdateMessageGQL,
      private iApproveMessageGql: IApproveMessageGQL,
      private iLikeMessageGql: ILikeMessageGQL,
      private iRejectMessageGql: IRejectMessageGQL,
      private iMessageListGql: IMessageListGQL,
      private iReadAllEnumsGql: IReadAllEnumsGQL,
      private iGetEnumVerbModListGql: IGetEnumVerbModListGQL,
      private iGetEnumVerbModByIdGql: IGetEnumVerbModByIdGQL,
      private iCreateEnumVerbGql: ICreateEnumVerbGQL,
      private iUpdateEnumVerbGql: IUpdateEnumVerbGQL,
      private iAvailableExecutorsGql: IAvailableExecutorsGQL,
      private iGetExecutorAppealsListGql: IGetExecutorAppealsListGQL,
      private iFileUploadGql: IFileUploadGQL,
      private iGetJournalListGql: IGetJournalListGQL,
      private iGetJournalGql: IGetJournalGQL,
      private iAvailableModeratorsGql: IAvailableModeratorsGQL,
      private iAvailableModeratorsForGql: IAvailableModeratorsForGQL,
      private iGetNewsModListGql: IGetNewsModListGQL,
      private iGetNewsModByIdGql: IGetNewsModByIdGQL,
      private iCreateNewsGql: ICreateNewsGQL,
      private iUpdateNewsGql: IUpdateNewsGQL,
      private iSetNewsPublishStateGql: ISetNewsPublishStateGQL,
      private iGetOfficerGql: IGetOfficerGQL,
      private iGetProtocolsGql: IGetProtocolsGQL,
      private iGetRegionListGql: IGetRegionListGQL,
      private iGetRegionByIdGql: IGetRegionByIdGQL,
      private iCreateRegionGql: ICreateRegionGQL,
      private iUpdateRegionGql: IUpdateRegionGQL,
      private iGetReportsListGql: IGetReportsListGQL,
      private iGetReportSettingsGql: IGetReportSettingsGQL,
      private iExecuteReportGql: IExecuteReportGQL,
      private iGetRoleGql: IGetRoleGQL,
      private iGetRoleListGql: IGetRoleListGQL,
      private iGetRoleListFullGql: IGetRoleListFullGQL,
      private iUpdateRole1Gql: IUpdateRole1GQL,
      private iUpdateRoleGql: IUpdateRoleGQL,
      private iCreateRoleGql: ICreateRoleGQL,
      private iDeleteRoleGql: IDeleteRoleGQL,
      private iGetRubricListGql: IGetRubricListGQL,
      private iGetRubricByIdGql: IGetRubricByIdGQL,
      private iUpdateRubricGql: IUpdateRubricGQL,
      private iCreateRubricGql: ICreateRubricGQL,
      private iGetSettingsGql: IGetSettingsGQL,
      private iSetSettingsGql: ISetSettingsGQL,
      private iNewSettingsGql: INewSettingsGQL,
      private iGetSubSystemListGql: IGetSubSystemListGQL,
      private iGetSubSystemGql: IGetSubSystemGQL,
      private iUpdateSubSystemGql: IUpdateSubSystemGQL,
      private iGetTerritoryListGql: IGetTerritoryListGQL,
      private iGetTerritoryByIdGql: IGetTerritoryByIdGQL,
      private iUpdateTerritoryGql: IUpdateTerritoryGQL,
      private iCreateTerritoryGql: ICreateTerritoryGQL,
      private iGetTopicListGql: IGetTopicListGQL,
      private iGetTopicGql: IGetTopicGQL,
      private iCreateTopicGql: ICreateTopicGQL,
      private iUpdateTopicGql: IUpdateTopicGQL,
      private iGetUserListGql: IGetUserListGQL,
      private iGetUserByIdGql: IGetUserByIdGQL,
      private iUpdateUserGql: IUpdateUserGQL,
      private iCreateUserGql: ICreateUserGQL,
      private iResetPasswordGql: IResetPasswordGQL,
      private iVersionsInfoGql: IVersionsInfoGQL,
      private iGetVoteRootPublicListGql: IGetVoteRootPublicListGQL,
      private iGetVoteRootModListGql: IGetVoteRootModListGQL,
      private iGetVoteRootModIdGql: IGetVoteRootModIdGQL,
      private iCreateVoteRootModGql: ICreateVoteRootModGQL,
      private iUpdateVoteRootModGql: IUpdateVoteRootModGQL,
      private iGetVotesListGql: IGetVotesListGQL,
      private iGetVotesPublicListGql: IGetVotesPublicListGQL,
      private iGetVoteGql: IGetVoteGQL,
      private iGetWidgetVotesGql: IGetWidgetVotesGQL,
      private iCreateVoteGql: ICreateVoteGQL,
      private iUpdateVoteGql: IUpdateVoteGQL,
      private iVoteStartGql: IVoteStartGQL,
      private iVoteStopGql: IVoteStopGQL,
      private iLoginGql: ILoginGQL,
      private iGetCurrentUserGql: IGetCurrentUserGQL,
      private iUpdateTokenGql: IUpdateTokenGQL,
      private iTest00002createTerritoryGql: ITest00002createTerritoryGQL,
      private iTest00002updateTerritoryGql: ITest00002updateTerritoryGQL
    ) {}
      
    deleteRecord(variables: IDeleteRecordMutationVariables, options?: MutationOptionsAlone<IDeleteRecordMutation, IDeleteRecordMutationVariables>) {
      return this.iDeleteRecordGql.mutate(variables, options)
    }
    
    getAppeal_1List(variables?: IGetAppeal_1ListQueryVariables, options?: QueryOptionsAlone<IGetAppeal_1ListQueryVariables>) {
      return this.iGetAppeal_1ListGql.fetch(variables, options)
    }
    
    getAppeal_1ListWatch(variables?: IGetAppeal_1ListQueryVariables, options?: WatchQueryOptionsAlone<IGetAppeal_1ListQueryVariables>) {
      return this.iGetAppeal_1ListGql.watch(variables, options)
    }
    
    getAppealList(variables?: IGetAppealListQueryVariables, options?: QueryOptionsAlone<IGetAppealListQueryVariables>) {
      return this.iGetAppealListGql.fetch(variables, options)
    }
    
    getAppealListWatch(variables?: IGetAppealListQueryVariables, options?: WatchQueryOptionsAlone<IGetAppealListQueryVariables>) {
      return this.iGetAppealListGql.watch(variables, options)
    }
    
    getAppealListWTotal(variables?: IGetAppealListWTotalQueryVariables, options?: QueryOptionsAlone<IGetAppealListWTotalQueryVariables>) {
      return this.iGetAppealListWTotalGql.fetch(variables, options)
    }
    
    getAppealListWTotalWatch(variables?: IGetAppealListWTotalQueryVariables, options?: WatchQueryOptionsAlone<IGetAppealListWTotalQueryVariables>) {
      return this.iGetAppealListWTotalGql.watch(variables, options)
    }
    
    getAppealListCount(variables?: IGetAppealListCountQueryVariables, options?: QueryOptionsAlone<IGetAppealListCountQueryVariables>) {
      return this.iGetAppealListCountGql.fetch(variables, options)
    }
    
    getAppealListCountWatch(variables?: IGetAppealListCountQueryVariables, options?: WatchQueryOptionsAlone<IGetAppealListCountQueryVariables>) {
      return this.iGetAppealListCountGql.watch(variables, options)
    }
    
    getAppealOrgList(variables?: IGetAppealOrgListQueryVariables, options?: QueryOptionsAlone<IGetAppealOrgListQueryVariables>) {
      return this.iGetAppealOrgListGql.fetch(variables, options)
    }
    
    getAppealOrgListWatch(variables?: IGetAppealOrgListQueryVariables, options?: WatchQueryOptionsAlone<IGetAppealOrgListQueryVariables>) {
      return this.iGetAppealOrgListGql.watch(variables, options)
    }
    
    getAppealSList(variables?: IGetAppealSListQueryVariables, options?: QueryOptionsAlone<IGetAppealSListQueryVariables>) {
      return this.iGetAppealSListGql.fetch(variables, options)
    }
    
    getAppealSListWatch(variables?: IGetAppealSListQueryVariables, options?: WatchQueryOptionsAlone<IGetAppealSListQueryVariables>) {
      return this.iGetAppealSListGql.watch(variables, options)
    }
    
    getAppealPublicList(variables?: IGetAppealPublicListQueryVariables, options?: QueryOptionsAlone<IGetAppealPublicListQueryVariables>) {
      return this.iGetAppealPublicListGql.fetch(variables, options)
    }
    
    getAppealPublicListWatch(variables?: IGetAppealPublicListQueryVariables, options?: WatchQueryOptionsAlone<IGetAppealPublicListQueryVariables>) {
      return this.iGetAppealPublicListGql.watch(variables, options)
    }
    
    getAppeal(variables: IGetAppealQueryVariables, options?: QueryOptionsAlone<IGetAppealQueryVariables>) {
      return this.iGetAppealGql.fetch(variables, options)
    }
    
    getAppealWatch(variables: IGetAppealQueryVariables, options?: WatchQueryOptionsAlone<IGetAppealQueryVariables>) {
      return this.iGetAppealGql.watch(variables, options)
    }
    
    getPublicAppeal(variables: IGetPublicAppealQueryVariables, options?: QueryOptionsAlone<IGetPublicAppealQueryVariables>) {
      return this.iGetPublicAppealGql.fetch(variables, options)
    }
    
    getPublicAppealWatch(variables: IGetPublicAppealQueryVariables, options?: WatchQueryOptionsAlone<IGetPublicAppealQueryVariables>) {
      return this.iGetPublicAppealGql.watch(variables, options)
    }
    
    createAppeal(variables: ICreateAppealMutationVariables, options?: MutationOptionsAlone<ICreateAppealMutation, ICreateAppealMutationVariables>) {
      return this.iCreateAppealGql.mutate(variables, options)
    }
    
    updateAppeal(variables: IUpdateAppealMutationVariables, options?: MutationOptionsAlone<IUpdateAppealMutation, IUpdateAppealMutationVariables>) {
      return this.iUpdateAppealGql.mutate(variables, options)
    }
    
    approveAppeal(variables: IApproveAppealMutationVariables, options?: MutationOptionsAlone<IApproveAppealMutation, IApproveAppealMutationVariables>) {
      return this.iApproveAppealGql.mutate(variables, options)
    }
    
    registerAppeal(variables: IRegisterAppealMutationVariables, options?: MutationOptionsAlone<IRegisterAppealMutation, IRegisterAppealMutationVariables>) {
      return this.iRegisterAppealGql.mutate(variables, options)
    }
    
    getAppointmentRangeList(variables?: IGetAppointmentRangeListQueryVariables, options?: QueryOptionsAlone<IGetAppointmentRangeListQueryVariables>) {
      return this.iGetAppointmentRangeListGql.fetch(variables, options)
    }
    
    getAppointmentRangeListWatch(variables?: IGetAppointmentRangeListQueryVariables, options?: WatchQueryOptionsAlone<IGetAppointmentRangeListQueryVariables>) {
      return this.iGetAppointmentRangeListGql.watch(variables, options)
    }
    
    getAppointmentList(variables?: IGetAppointmentListQueryVariables, options?: QueryOptionsAlone<IGetAppointmentListQueryVariables>) {
      return this.iGetAppointmentListGql.fetch(variables, options)
    }
    
    getAppointmentListWatch(variables?: IGetAppointmentListQueryVariables, options?: WatchQueryOptionsAlone<IGetAppointmentListQueryVariables>) {
      return this.iGetAppointmentListGql.watch(variables, options)
    }
    
    getAppointment(variables?: IGetAppointmentQueryVariables, options?: QueryOptionsAlone<IGetAppointmentQueryVariables>) {
      return this.iGetAppointmentGql.fetch(variables, options)
    }
    
    getAppointmentWatch(variables?: IGetAppointmentQueryVariables, options?: WatchQueryOptionsAlone<IGetAppointmentQueryVariables>) {
      return this.iGetAppointmentGql.watch(variables, options)
    }
    
    attachSetIsPublic(variables: IAttachSetIsPublicMutationVariables, options?: MutationOptionsAlone<IAttachSetIsPublicMutation, IAttachSetIsPublicMutationVariables>) {
      return this.iAttachSetIsPublicGql.mutate(variables, options)
    }
    
    getClaimInfos(variables?: IGetClaimInfosQueryVariables, options?: QueryOptionsAlone<IGetClaimInfosQueryVariables>) {
      return this.iGetClaimInfosGql.fetch(variables, options)
    }
    
    getClaimInfosWatch(variables?: IGetClaimInfosQueryVariables, options?: WatchQueryOptionsAlone<IGetClaimInfosQueryVariables>) {
      return this.iGetClaimInfosGql.watch(variables, options)
    }
    
    createMessage(variables: ICreateMessageMutationVariables, options?: MutationOptionsAlone<ICreateMessageMutation, ICreateMessageMutationVariables>) {
      return this.iCreateMessageGql.mutate(variables, options)
    }
    
    createMessage2Appeal(variables: ICreateMessage2AppealMutationVariables, options?: MutationOptionsAlone<ICreateMessage2AppealMutation, ICreateMessage2AppealMutationVariables>) {
      return this.iCreateMessage2AppealGql.mutate(variables, options)
    }
    
    createMessage2Message(variables: ICreateMessage2MessageMutationVariables, options?: MutationOptionsAlone<ICreateMessage2MessageMutation, ICreateMessage2MessageMutationVariables>) {
      return this.iCreateMessage2MessageGql.mutate(variables, options)
    }
    
    updateMessage(variables: IUpdateMessageMutationVariables, options?: MutationOptionsAlone<IUpdateMessageMutation, IUpdateMessageMutationVariables>) {
      return this.iUpdateMessageGql.mutate(variables, options)
    }
    
    approveMessage(variables: IApproveMessageMutationVariables, options?: MutationOptionsAlone<IApproveMessageMutation, IApproveMessageMutationVariables>) {
      return this.iApproveMessageGql.mutate(variables, options)
    }
    
    likeMessage(variables: ILikeMessageMutationVariables, options?: MutationOptionsAlone<ILikeMessageMutation, ILikeMessageMutationVariables>) {
      return this.iLikeMessageGql.mutate(variables, options)
    }
    
    rejectMessage(variables: IRejectMessageMutationVariables, options?: MutationOptionsAlone<IRejectMessageMutation, IRejectMessageMutationVariables>) {
      return this.iRejectMessageGql.mutate(variables, options)
    }
    
    messageList(variables: IMessageListQueryVariables, options?: QueryOptionsAlone<IMessageListQueryVariables>) {
      return this.iMessageListGql.fetch(variables, options)
    }
    
    messageListWatch(variables: IMessageListQueryVariables, options?: WatchQueryOptionsAlone<IMessageListQueryVariables>) {
      return this.iMessageListGql.watch(variables, options)
    }
    
    readAllEnums(variables?: IReadAllEnumsQueryVariables, options?: QueryOptionsAlone<IReadAllEnumsQueryVariables>) {
      return this.iReadAllEnumsGql.fetch(variables, options)
    }
    
    readAllEnumsWatch(variables?: IReadAllEnumsQueryVariables, options?: WatchQueryOptionsAlone<IReadAllEnumsQueryVariables>) {
      return this.iReadAllEnumsGql.watch(variables, options)
    }
    
    getEnumVerbModList(variables?: IGetEnumVerbModListQueryVariables, options?: QueryOptionsAlone<IGetEnumVerbModListQueryVariables>) {
      return this.iGetEnumVerbModListGql.fetch(variables, options)
    }
    
    getEnumVerbModListWatch(variables?: IGetEnumVerbModListQueryVariables, options?: WatchQueryOptionsAlone<IGetEnumVerbModListQueryVariables>) {
      return this.iGetEnumVerbModListGql.watch(variables, options)
    }
    
    getEnumVerbModById(variables: IGetEnumVerbModByIdQueryVariables, options?: QueryOptionsAlone<IGetEnumVerbModByIdQueryVariables>) {
      return this.iGetEnumVerbModByIdGql.fetch(variables, options)
    }
    
    getEnumVerbModByIdWatch(variables: IGetEnumVerbModByIdQueryVariables, options?: WatchQueryOptionsAlone<IGetEnumVerbModByIdQueryVariables>) {
      return this.iGetEnumVerbModByIdGql.watch(variables, options)
    }
    
    createEnumVerb(variables?: ICreateEnumVerbMutationVariables, options?: MutationOptionsAlone<ICreateEnumVerbMutation, ICreateEnumVerbMutationVariables>) {
      return this.iCreateEnumVerbGql.mutate(variables, options)
    }
    
    updateEnumVerb(variables?: IUpdateEnumVerbMutationVariables, options?: MutationOptionsAlone<IUpdateEnumVerbMutation, IUpdateEnumVerbMutationVariables>) {
      return this.iUpdateEnumVerbGql.mutate(variables, options)
    }
    
    availableExecutors(variables?: IAvailableExecutorsQueryVariables, options?: QueryOptionsAlone<IAvailableExecutorsQueryVariables>) {
      return this.iAvailableExecutorsGql.fetch(variables, options)
    }
    
    availableExecutorsWatch(variables?: IAvailableExecutorsQueryVariables, options?: WatchQueryOptionsAlone<IAvailableExecutorsQueryVariables>) {
      return this.iAvailableExecutorsGql.watch(variables, options)
    }
    
    getExecutorAppealsList(variables?: IGetExecutorAppealsListQueryVariables, options?: QueryOptionsAlone<IGetExecutorAppealsListQueryVariables>) {
      return this.iGetExecutorAppealsListGql.fetch(variables, options)
    }
    
    getExecutorAppealsListWatch(variables?: IGetExecutorAppealsListQueryVariables, options?: WatchQueryOptionsAlone<IGetExecutorAppealsListQueryVariables>) {
      return this.iGetExecutorAppealsListGql.watch(variables, options)
    }
    
    fileUpload(variables: IFileUploadMutationVariables, options?: MutationOptionsAlone<IFileUploadMutation, IFileUploadMutationVariables>) {
      return this.iFileUploadGql.mutate(variables, options)
    }
    
    getJournalList(variables?: IGetJournalListQueryVariables, options?: QueryOptionsAlone<IGetJournalListQueryVariables>) {
      return this.iGetJournalListGql.fetch(variables, options)
    }
    
    getJournalListWatch(variables?: IGetJournalListQueryVariables, options?: WatchQueryOptionsAlone<IGetJournalListQueryVariables>) {
      return this.iGetJournalListGql.watch(variables, options)
    }
    
    getJournal(variables?: IGetJournalQueryVariables, options?: QueryOptionsAlone<IGetJournalQueryVariables>) {
      return this.iGetJournalGql.fetch(variables, options)
    }
    
    getJournalWatch(variables?: IGetJournalQueryVariables, options?: WatchQueryOptionsAlone<IGetJournalQueryVariables>) {
      return this.iGetJournalGql.watch(variables, options)
    }
    
    availableModerators(variables?: IAvailableModeratorsQueryVariables, options?: QueryOptionsAlone<IAvailableModeratorsQueryVariables>) {
      return this.iAvailableModeratorsGql.fetch(variables, options)
    }
    
    availableModeratorsWatch(variables?: IAvailableModeratorsQueryVariables, options?: WatchQueryOptionsAlone<IAvailableModeratorsQueryVariables>) {
      return this.iAvailableModeratorsGql.watch(variables, options)
    }
    
    availableModeratorsFor(variables?: IAvailableModeratorsForQueryVariables, options?: QueryOptionsAlone<IAvailableModeratorsForQueryVariables>) {
      return this.iAvailableModeratorsForGql.fetch(variables, options)
    }
    
    availableModeratorsForWatch(variables?: IAvailableModeratorsForQueryVariables, options?: WatchQueryOptionsAlone<IAvailableModeratorsForQueryVariables>) {
      return this.iAvailableModeratorsForGql.watch(variables, options)
    }
    
    getNewsModList(variables?: IGetNewsModListQueryVariables, options?: QueryOptionsAlone<IGetNewsModListQueryVariables>) {
      return this.iGetNewsModListGql.fetch(variables, options)
    }
    
    getNewsModListWatch(variables?: IGetNewsModListQueryVariables, options?: WatchQueryOptionsAlone<IGetNewsModListQueryVariables>) {
      return this.iGetNewsModListGql.watch(variables, options)
    }
    
    getNewsModById(variables: IGetNewsModByIdQueryVariables, options?: QueryOptionsAlone<IGetNewsModByIdQueryVariables>) {
      return this.iGetNewsModByIdGql.fetch(variables, options)
    }
    
    getNewsModByIdWatch(variables: IGetNewsModByIdQueryVariables, options?: WatchQueryOptionsAlone<IGetNewsModByIdQueryVariables>) {
      return this.iGetNewsModByIdGql.watch(variables, options)
    }
    
    createNews(variables: ICreateNewsMutationVariables, options?: MutationOptionsAlone<ICreateNewsMutation, ICreateNewsMutationVariables>) {
      return this.iCreateNewsGql.mutate(variables, options)
    }
    
    updateNews(variables: IUpdateNewsMutationVariables, options?: MutationOptionsAlone<IUpdateNewsMutation, IUpdateNewsMutationVariables>) {
      return this.iUpdateNewsGql.mutate(variables, options)
    }
    
    setNewsPublishState(variables: ISetNewsPublishStateMutationVariables, options?: MutationOptionsAlone<ISetNewsPublishStateMutation, ISetNewsPublishStateMutationVariables>) {
      return this.iSetNewsPublishStateGql.mutate(variables, options)
    }
    
    getOfficer(variables?: IGetOfficerQueryVariables, options?: QueryOptionsAlone<IGetOfficerQueryVariables>) {
      return this.iGetOfficerGql.fetch(variables, options)
    }
    
    getOfficerWatch(variables?: IGetOfficerQueryVariables, options?: WatchQueryOptionsAlone<IGetOfficerQueryVariables>) {
      return this.iGetOfficerGql.watch(variables, options)
    }
    
    getProtocols(variables?: IGetProtocolsQueryVariables, options?: QueryOptionsAlone<IGetProtocolsQueryVariables>) {
      return this.iGetProtocolsGql.fetch(variables, options)
    }
    
    getProtocolsWatch(variables?: IGetProtocolsQueryVariables, options?: WatchQueryOptionsAlone<IGetProtocolsQueryVariables>) {
      return this.iGetProtocolsGql.watch(variables, options)
    }
    
    getRegionList(variables?: IGetRegionListQueryVariables, options?: QueryOptionsAlone<IGetRegionListQueryVariables>) {
      return this.iGetRegionListGql.fetch(variables, options)
    }
    
    getRegionListWatch(variables?: IGetRegionListQueryVariables, options?: WatchQueryOptionsAlone<IGetRegionListQueryVariables>) {
      return this.iGetRegionListGql.watch(variables, options)
    }
    
    getRegionById(variables: IGetRegionByIdQueryVariables, options?: QueryOptionsAlone<IGetRegionByIdQueryVariables>) {
      return this.iGetRegionByIdGql.fetch(variables, options)
    }
    
    getRegionByIdWatch(variables: IGetRegionByIdQueryVariables, options?: WatchQueryOptionsAlone<IGetRegionByIdQueryVariables>) {
      return this.iGetRegionByIdGql.watch(variables, options)
    }
    
    createRegion(variables: ICreateRegionMutationVariables, options?: MutationOptionsAlone<ICreateRegionMutation, ICreateRegionMutationVariables>) {
      return this.iCreateRegionGql.mutate(variables, options)
    }
    
    updateRegion(variables: IUpdateRegionMutationVariables, options?: MutationOptionsAlone<IUpdateRegionMutation, IUpdateRegionMutationVariables>) {
      return this.iUpdateRegionGql.mutate(variables, options)
    }
    
    getReportsList(variables?: IGetReportsListQueryVariables, options?: QueryOptionsAlone<IGetReportsListQueryVariables>) {
      return this.iGetReportsListGql.fetch(variables, options)
    }
    
    getReportsListWatch(variables?: IGetReportsListQueryVariables, options?: WatchQueryOptionsAlone<IGetReportsListQueryVariables>) {
      return this.iGetReportsListGql.watch(variables, options)
    }
    
    getReportSettings(variables?: IGetReportSettingsQueryVariables, options?: QueryOptionsAlone<IGetReportSettingsQueryVariables>) {
      return this.iGetReportSettingsGql.fetch(variables, options)
    }
    
    getReportSettingsWatch(variables?: IGetReportSettingsQueryVariables, options?: WatchQueryOptionsAlone<IGetReportSettingsQueryVariables>) {
      return this.iGetReportSettingsGql.watch(variables, options)
    }
    
    executeReport(variables: IExecuteReportQueryVariables, options?: QueryOptionsAlone<IExecuteReportQueryVariables>) {
      return this.iExecuteReportGql.fetch(variables, options)
    }
    
    executeReportWatch(variables: IExecuteReportQueryVariables, options?: WatchQueryOptionsAlone<IExecuteReportQueryVariables>) {
      return this.iExecuteReportGql.watch(variables, options)
    }
    
    getRole(variables: IGetRoleQueryVariables, options?: QueryOptionsAlone<IGetRoleQueryVariables>) {
      return this.iGetRoleGql.fetch(variables, options)
    }
    
    getRoleWatch(variables: IGetRoleQueryVariables, options?: WatchQueryOptionsAlone<IGetRoleQueryVariables>) {
      return this.iGetRoleGql.watch(variables, options)
    }
    
    getRoleList(variables?: IGetRoleListQueryVariables, options?: QueryOptionsAlone<IGetRoleListQueryVariables>) {
      return this.iGetRoleListGql.fetch(variables, options)
    }
    
    getRoleListWatch(variables?: IGetRoleListQueryVariables, options?: WatchQueryOptionsAlone<IGetRoleListQueryVariables>) {
      return this.iGetRoleListGql.watch(variables, options)
    }
    
    getRoleListFull(variables?: IGetRoleListFullQueryVariables, options?: QueryOptionsAlone<IGetRoleListFullQueryVariables>) {
      return this.iGetRoleListFullGql.fetch(variables, options)
    }
    
    getRoleListFullWatch(variables?: IGetRoleListFullQueryVariables, options?: WatchQueryOptionsAlone<IGetRoleListFullQueryVariables>) {
      return this.iGetRoleListFullGql.watch(variables, options)
    }
    
    updateRole1(variables: IUpdateRole1MutationVariables, options?: MutationOptionsAlone<IUpdateRole1Mutation, IUpdateRole1MutationVariables>) {
      return this.iUpdateRole1Gql.mutate(variables, options)
    }
    
    updateRole(variables: IUpdateRoleMutationVariables, options?: MutationOptionsAlone<IUpdateRoleMutation, IUpdateRoleMutationVariables>) {
      return this.iUpdateRoleGql.mutate(variables, options)
    }
    
    createRole(variables: ICreateRoleMutationVariables, options?: MutationOptionsAlone<ICreateRoleMutation, ICreateRoleMutationVariables>) {
      return this.iCreateRoleGql.mutate(variables, options)
    }
    
    deleteRole(variables: IDeleteRoleMutationVariables, options?: MutationOptionsAlone<IDeleteRoleMutation, IDeleteRoleMutationVariables>) {
      return this.iDeleteRoleGql.mutate(variables, options)
    }
    
    getRubricList(variables?: IGetRubricListQueryVariables, options?: QueryOptionsAlone<IGetRubricListQueryVariables>) {
      return this.iGetRubricListGql.fetch(variables, options)
    }
    
    getRubricListWatch(variables?: IGetRubricListQueryVariables, options?: WatchQueryOptionsAlone<IGetRubricListQueryVariables>) {
      return this.iGetRubricListGql.watch(variables, options)
    }
    
    getRubricById(variables: IGetRubricByIdQueryVariables, options?: QueryOptionsAlone<IGetRubricByIdQueryVariables>) {
      return this.iGetRubricByIdGql.fetch(variables, options)
    }
    
    getRubricByIdWatch(variables: IGetRubricByIdQueryVariables, options?: WatchQueryOptionsAlone<IGetRubricByIdQueryVariables>) {
      return this.iGetRubricByIdGql.watch(variables, options)
    }
    
    updateRubric(variables: IUpdateRubricMutationVariables, options?: MutationOptionsAlone<IUpdateRubricMutation, IUpdateRubricMutationVariables>) {
      return this.iUpdateRubricGql.mutate(variables, options)
    }
    
    createRubric(variables: ICreateRubricMutationVariables, options?: MutationOptionsAlone<ICreateRubricMutation, ICreateRubricMutationVariables>) {
      return this.iCreateRubricGql.mutate(variables, options)
    }
    
    getSettings(variables: IGetSettingsQueryVariables, options?: QueryOptionsAlone<IGetSettingsQueryVariables>) {
      return this.iGetSettingsGql.fetch(variables, options)
    }
    
    getSettingsWatch(variables: IGetSettingsQueryVariables, options?: WatchQueryOptionsAlone<IGetSettingsQueryVariables>) {
      return this.iGetSettingsGql.watch(variables, options)
    }
    
    setSettings(variables: ISetSettingsMutationVariables, options?: MutationOptionsAlone<ISetSettingsMutation, ISetSettingsMutationVariables>) {
      return this.iSetSettingsGql.mutate(variables, options)
    }
    
    newSettings(variables?: INewSettingsSubscriptionVariables, options?: SubscriptionOptionsAlone<INewSettingsSubscriptionVariables>) {
      return this.iNewSettingsGql.subscribe(variables, options)
    }
    
    getSubSystemList(variables?: IGetSubSystemListQueryVariables, options?: QueryOptionsAlone<IGetSubSystemListQueryVariables>) {
      return this.iGetSubSystemListGql.fetch(variables, options)
    }
    
    getSubSystemListWatch(variables?: IGetSubSystemListQueryVariables, options?: WatchQueryOptionsAlone<IGetSubSystemListQueryVariables>) {
      return this.iGetSubSystemListGql.watch(variables, options)
    }
    
    getSubSystem(variables: IGetSubSystemQueryVariables, options?: QueryOptionsAlone<IGetSubSystemQueryVariables>) {
      return this.iGetSubSystemGql.fetch(variables, options)
    }
    
    getSubSystemWatch(variables: IGetSubSystemQueryVariables, options?: WatchQueryOptionsAlone<IGetSubSystemQueryVariables>) {
      return this.iGetSubSystemGql.watch(variables, options)
    }
    
    updateSubSystem(variables: IUpdateSubSystemMutationVariables, options?: MutationOptionsAlone<IUpdateSubSystemMutation, IUpdateSubSystemMutationVariables>) {
      return this.iUpdateSubSystemGql.mutate(variables, options)
    }
    
    getTerritoryList(variables?: IGetTerritoryListQueryVariables, options?: QueryOptionsAlone<IGetTerritoryListQueryVariables>) {
      return this.iGetTerritoryListGql.fetch(variables, options)
    }
    
    getTerritoryListWatch(variables?: IGetTerritoryListQueryVariables, options?: WatchQueryOptionsAlone<IGetTerritoryListQueryVariables>) {
      return this.iGetTerritoryListGql.watch(variables, options)
    }
    
    getTerritoryById(variables: IGetTerritoryByIdQueryVariables, options?: QueryOptionsAlone<IGetTerritoryByIdQueryVariables>) {
      return this.iGetTerritoryByIdGql.fetch(variables, options)
    }
    
    getTerritoryByIdWatch(variables: IGetTerritoryByIdQueryVariables, options?: WatchQueryOptionsAlone<IGetTerritoryByIdQueryVariables>) {
      return this.iGetTerritoryByIdGql.watch(variables, options)
    }
    
    updateTerritory(variables: IUpdateTerritoryMutationVariables, options?: MutationOptionsAlone<IUpdateTerritoryMutation, IUpdateTerritoryMutationVariables>) {
      return this.iUpdateTerritoryGql.mutate(variables, options)
    }
    
    createTerritory(variables: ICreateTerritoryMutationVariables, options?: MutationOptionsAlone<ICreateTerritoryMutation, ICreateTerritoryMutationVariables>) {
      return this.iCreateTerritoryGql.mutate(variables, options)
    }
    
    getTopicList(variables?: IGetTopicListQueryVariables, options?: QueryOptionsAlone<IGetTopicListQueryVariables>) {
      return this.iGetTopicListGql.fetch(variables, options)
    }
    
    getTopicListWatch(variables?: IGetTopicListQueryVariables, options?: WatchQueryOptionsAlone<IGetTopicListQueryVariables>) {
      return this.iGetTopicListGql.watch(variables, options)
    }
    
    getTopic(variables: IGetTopicQueryVariables, options?: QueryOptionsAlone<IGetTopicQueryVariables>) {
      return this.iGetTopicGql.fetch(variables, options)
    }
    
    getTopicWatch(variables: IGetTopicQueryVariables, options?: WatchQueryOptionsAlone<IGetTopicQueryVariables>) {
      return this.iGetTopicGql.watch(variables, options)
    }
    
    createTopic(variables: ICreateTopicMutationVariables, options?: MutationOptionsAlone<ICreateTopicMutation, ICreateTopicMutationVariables>) {
      return this.iCreateTopicGql.mutate(variables, options)
    }
    
    updateTopic(variables: IUpdateTopicMutationVariables, options?: MutationOptionsAlone<IUpdateTopicMutation, IUpdateTopicMutationVariables>) {
      return this.iUpdateTopicGql.mutate(variables, options)
    }
    
    getUserList(variables?: IGetUserListQueryVariables, options?: QueryOptionsAlone<IGetUserListQueryVariables>) {
      return this.iGetUserListGql.fetch(variables, options)
    }
    
    getUserListWatch(variables?: IGetUserListQueryVariables, options?: WatchQueryOptionsAlone<IGetUserListQueryVariables>) {
      return this.iGetUserListGql.watch(variables, options)
    }
    
    getUserById(variables: IGetUserByIdQueryVariables, options?: QueryOptionsAlone<IGetUserByIdQueryVariables>) {
      return this.iGetUserByIdGql.fetch(variables, options)
    }
    
    getUserByIdWatch(variables: IGetUserByIdQueryVariables, options?: WatchQueryOptionsAlone<IGetUserByIdQueryVariables>) {
      return this.iGetUserByIdGql.watch(variables, options)
    }
    
    updateUser(variables: IUpdateUserMutationVariables, options?: MutationOptionsAlone<IUpdateUserMutation, IUpdateUserMutationVariables>) {
      return this.iUpdateUserGql.mutate(variables, options)
    }
    
    createUser(variables: ICreateUserMutationVariables, options?: MutationOptionsAlone<ICreateUserMutation, ICreateUserMutationVariables>) {
      return this.iCreateUserGql.mutate(variables, options)
    }
    
    resetPassword(variables: IResetPasswordMutationVariables, options?: MutationOptionsAlone<IResetPasswordMutation, IResetPasswordMutationVariables>) {
      return this.iResetPasswordGql.mutate(variables, options)
    }
    
    versionsInfo(variables?: IVersionsInfoQueryVariables, options?: QueryOptionsAlone<IVersionsInfoQueryVariables>) {
      return this.iVersionsInfoGql.fetch(variables, options)
    }
    
    versionsInfoWatch(variables?: IVersionsInfoQueryVariables, options?: WatchQueryOptionsAlone<IVersionsInfoQueryVariables>) {
      return this.iVersionsInfoGql.watch(variables, options)
    }
    
    getVoteRootPublicList(variables?: IGetVoteRootPublicListQueryVariables, options?: QueryOptionsAlone<IGetVoteRootPublicListQueryVariables>) {
      return this.iGetVoteRootPublicListGql.fetch(variables, options)
    }
    
    getVoteRootPublicListWatch(variables?: IGetVoteRootPublicListQueryVariables, options?: WatchQueryOptionsAlone<IGetVoteRootPublicListQueryVariables>) {
      return this.iGetVoteRootPublicListGql.watch(variables, options)
    }
    
    getVoteRootModList(variables?: IGetVoteRootModListQueryVariables, options?: QueryOptionsAlone<IGetVoteRootModListQueryVariables>) {
      return this.iGetVoteRootModListGql.fetch(variables, options)
    }
    
    getVoteRootModListWatch(variables?: IGetVoteRootModListQueryVariables, options?: WatchQueryOptionsAlone<IGetVoteRootModListQueryVariables>) {
      return this.iGetVoteRootModListGql.watch(variables, options)
    }
    
    getVoteRootModId(variables: IGetVoteRootModIdQueryVariables, options?: QueryOptionsAlone<IGetVoteRootModIdQueryVariables>) {
      return this.iGetVoteRootModIdGql.fetch(variables, options)
    }
    
    getVoteRootModIdWatch(variables: IGetVoteRootModIdQueryVariables, options?: WatchQueryOptionsAlone<IGetVoteRootModIdQueryVariables>) {
      return this.iGetVoteRootModIdGql.watch(variables, options)
    }
    
    createVoteRootMod(variables: ICreateVoteRootModMutationVariables, options?: MutationOptionsAlone<ICreateVoteRootModMutation, ICreateVoteRootModMutationVariables>) {
      return this.iCreateVoteRootModGql.mutate(variables, options)
    }
    
    updateVoteRootMod(variables: IUpdateVoteRootModMutationVariables, options?: MutationOptionsAlone<IUpdateVoteRootModMutation, IUpdateVoteRootModMutationVariables>) {
      return this.iUpdateVoteRootModGql.mutate(variables, options)
    }
    
    getVotesList(variables?: IGetVotesListQueryVariables, options?: QueryOptionsAlone<IGetVotesListQueryVariables>) {
      return this.iGetVotesListGql.fetch(variables, options)
    }
    
    getVotesListWatch(variables?: IGetVotesListQueryVariables, options?: WatchQueryOptionsAlone<IGetVotesListQueryVariables>) {
      return this.iGetVotesListGql.watch(variables, options)
    }
    
    getVotesPublicList(variables?: IGetVotesPublicListQueryVariables, options?: QueryOptionsAlone<IGetVotesPublicListQueryVariables>) {
      return this.iGetVotesPublicListGql.fetch(variables, options)
    }
    
    getVotesPublicListWatch(variables?: IGetVotesPublicListQueryVariables, options?: WatchQueryOptionsAlone<IGetVotesPublicListQueryVariables>) {
      return this.iGetVotesPublicListGql.watch(variables, options)
    }
    
    getVote(variables: IGetVoteQueryVariables, options?: QueryOptionsAlone<IGetVoteQueryVariables>) {
      return this.iGetVoteGql.fetch(variables, options)
    }
    
    getVoteWatch(variables: IGetVoteQueryVariables, options?: WatchQueryOptionsAlone<IGetVoteQueryVariables>) {
      return this.iGetVoteGql.watch(variables, options)
    }
    
    getWidgetVotes(variables?: IGetWidgetVotesQueryVariables, options?: QueryOptionsAlone<IGetWidgetVotesQueryVariables>) {
      return this.iGetWidgetVotesGql.fetch(variables, options)
    }
    
    getWidgetVotesWatch(variables?: IGetWidgetVotesQueryVariables, options?: WatchQueryOptionsAlone<IGetWidgetVotesQueryVariables>) {
      return this.iGetWidgetVotesGql.watch(variables, options)
    }
    
    createVote(variables: ICreateVoteMutationVariables, options?: MutationOptionsAlone<ICreateVoteMutation, ICreateVoteMutationVariables>) {
      return this.iCreateVoteGql.mutate(variables, options)
    }
    
    updateVote(variables: IUpdateVoteMutationVariables, options?: MutationOptionsAlone<IUpdateVoteMutation, IUpdateVoteMutationVariables>) {
      return this.iUpdateVoteGql.mutate(variables, options)
    }
    
    voteStart(variables: IVoteStartMutationVariables, options?: MutationOptionsAlone<IVoteStartMutation, IVoteStartMutationVariables>) {
      return this.iVoteStartGql.mutate(variables, options)
    }
    
    voteStop(variables: IVoteStopMutationVariables, options?: MutationOptionsAlone<IVoteStopMutation, IVoteStopMutationVariables>) {
      return this.iVoteStopGql.mutate(variables, options)
    }
    
    login(variables: ILoginQueryVariables, options?: QueryOptionsAlone<ILoginQueryVariables>) {
      return this.iLoginGql.fetch(variables, options)
    }
    
    loginWatch(variables: ILoginQueryVariables, options?: WatchQueryOptionsAlone<ILoginQueryVariables>) {
      return this.iLoginGql.watch(variables, options)
    }
    
    getCurrentUser(variables?: IGetCurrentUserQueryVariables, options?: QueryOptionsAlone<IGetCurrentUserQueryVariables>) {
      return this.iGetCurrentUserGql.fetch(variables, options)
    }
    
    getCurrentUserWatch(variables?: IGetCurrentUserQueryVariables, options?: WatchQueryOptionsAlone<IGetCurrentUserQueryVariables>) {
      return this.iGetCurrentUserGql.watch(variables, options)
    }
    
    updateToken(variables?: IUpdateTokenQueryVariables, options?: QueryOptionsAlone<IUpdateTokenQueryVariables>) {
      return this.iUpdateTokenGql.fetch(variables, options)
    }
    
    updateTokenWatch(variables?: IUpdateTokenQueryVariables, options?: WatchQueryOptionsAlone<IUpdateTokenQueryVariables>) {
      return this.iUpdateTokenGql.watch(variables, options)
    }
    
    test00002createTerritory(variables?: ITest00002createTerritoryMutationVariables, options?: MutationOptionsAlone<ITest00002createTerritoryMutation, ITest00002createTerritoryMutationVariables>) {
      return this.iTest00002createTerritoryGql.mutate(variables, options)
    }
    
    test00002updateTerritory(variables: ITest00002updateTerritoryMutationVariables, options?: MutationOptionsAlone<ITest00002updateTerritoryMutation, ITest00002updateTerritoryMutationVariables>) {
      return this.iTest00002updateTerritoryGql.mutate(variables, options)
    }
  }

      export interface PossibleTypesResultData {
        possibleTypes: {
          [key: string]: string[]
        }
      }
      const result: PossibleTypesResultData = {
  "possibleTypes": {}
};
      export default result;
    