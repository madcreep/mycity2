import { Component } from '@angular/core';
import { Subscriptionable } from '../common/subscriptionable';
import { SettingsService } from '../services/settings.service';
import { SETTINGS_MAIN_TITLE } from '../services/setting-names.const';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
})
export class HomeComponent extends Subscriptionable{
    mainTitle: string = '';
    constructor (
        private setSrv: SettingsService,
    ) {
        super();
        this.subscriptions.push(
            this.setSrv.subscribeToValue(SETTINGS_MAIN_TITLE,
                (value) => this.mainTitle = value
            )
        );
    }



}
