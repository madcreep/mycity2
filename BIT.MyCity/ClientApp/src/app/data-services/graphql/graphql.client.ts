import {
    ILoginQueryVariables,
    ISetSettingsMutationVariables, IGetSettingsQueryVariables,
    IFilterNextOpEnum, IRange, IFilter, IGetVoteRootModIdQueryVariables, IVoteRoot, IGetReportSettingsQueryVariables,
} from './../../../generated/types.graphql-gen';
import { ApolloAngularSDK } from 'src/generated/types.graphql-gen';
import { DATAERROR_UNKNOWN_ENTITY } from 'src/app/errors.consts';
import { AppUtils } from 'src/app/helpers/app.static';
import { GqlApiQueries, ICacheUpdateRecord } from './sdk-map.interfaces';

import { QueryOptionsAlone } from 'apollo-angular/types';
import { Injectable } from '@angular/core';
import { from } from 'rxjs/internal/observable/from';
import ApolloCacheUpdater from 'apollo-cache-updater';

@Injectable({
    providedIn: 'root'
})
export class GraphQLClient extends ApolloAngularSDK {
    sdkMap: GqlApiQueries[] = [];

    addApiDescriptor(sdkApi: GqlApiQueries) {
        const api = AppUtils.deepCopy(sdkApi);
        api.create = api.create ? api.create.bind(this) : undefined;
        api.query = api.query ? api.query.bind(this) : undefined;
        api.update = api.update ? api.update.bind(this) : undefined;
        api.list = api.list ? api.list.bind(this) : undefined;
        api.delete = api.delete ? api.delete.bind(this) : undefined;

        this.sdkMap.push(api);
    }

    getDocumentByName(name: string): any {
        for (const key in this) {
            if (this.hasOwnProperty(key)) {
                const el = this[key];
                if (el['document'] && el['document']['definitions'] && el['document']['definitions'].length) {
                    const res = el['document']['definitions'].find(d =>
                        d.kind === 'OperationDefinition' &&
                        d.operation === 'query' &&
                        d.name && d.name.value === name
                    );
                    if (res) {
                        return el['document'];
                    }
                }
            }
        }
        return null;
    }

    findTypenameKey(entity: string, data: any): string {
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                const el = data[key];
                if (el.__typename && el.__typename.toLowerCase() === entity.toLowerCase()) {
                    return key;
                }
            }
        }
        return null;
    }

    listCacheUpdaterAfterInsert(entity: string, cacheupdate: ICacheUpdateRecord[]) {

        return (proxy, { data: result }): any => { // your mutation response

            if (!cacheupdate || !cacheupdate.length) {
                return;
            }

            cacheupdate.forEach(cu => {
                const query = typeof cu.obj === 'string' ? this.getDocumentByName(cu.obj) : cu.obj;
                const key = this.findTypenameKey(entity, result);
                const mutationResult = result[key]; // mutation result to pass into the updater
                const updates = ApolloCacheUpdater({
                    proxy, // apollo proxy
                    queriesToUpdate: [query],
                    searchVariables: cu.variables || {
                        // published: true, // update queries in the cache that have these vars
                    },
                    mutationResult,
                });

                if (updates) {
                }

            });

        };

        // return (store, obj): any => {
        //     if (!cacheupdate || !cacheupdate.length) {
        //         return;
        //     }

        //     cacheupdate.forEach(updateDescr => {
        //         const updateObj = typeof updateDescr.obj === 'string' ? this.getDocumentByName(updateDescr.obj) : updateDescr.obj;
        //         const updateKey = updateDescr.key || entity;
        //         const query: any = { query: updateObj };
        //         if (updateDescr.variables) {
        //             query.variables = updateDescr.variables;
        //         }
        //         const cacheddata = store.readQuery(query);

        //         for (const key in obj.data) {
        //             if (obj.data.hasOwnProperty(key)) {
        //                 const el = obj.data[key];
        //                 if (el.__typename && el.__typename.toLowerCase() === entity.toLowerCase()) {
        //                     const newData = obj.data[key];
        //                     cacheddata[updateKey] = [...cacheddata[updateKey], newData];
        //                 }
        //             }
        //         }

        //         query.data = cacheddata;
        //         store.writeQuery(query);
        //     });
        // };
    }

    listCacheUpdaterAfterDelete(api: GqlApiQueries, id, key = 'id') {
        return (store, obj): any => {
            // console.log('GraphQLClient -> listCacheUpdaterAfterDelete -> obj', obj);
            if (!(api && api.cacheupdate && api.cacheupdate.length)) {
                return;
            }
            // Read data from the cache.
            api.cacheupdate.forEach(updateDescr => {
                const updateObj = updateDescr.obj;
                const updateKey = updateDescr.key || api.entity;
                const cacheddata = store.readQuery({ query: updateObj });

                // const index = cacheddata[updateKey].findIndex( o => o[key] === id);
                cacheddata[updateKey] = cacheddata[updateKey].filter(o => o[key] !== id);
                // if (index !== -1) {
                //     cacheddata[updateKey] = cacheddata[updateKey].splice(index, 1);
                // }

                // for (const key in obj.data) {
                //     if (obj.data.hasOwnProperty(key)) {
                //                 const el = obj.data[key];
                //         //         if (el.__typename && el.__typename.toLowerCase() === api.entity.toLowerCase()) {
                //         //             const newData = obj.data[key];
                //         //             cacheddata[updateKey] = [ ... cacheddata[updateKey], newData];
                //         //         }

                //     }
                // }
                store.writeQuery({ query: updateObj, data: cacheddata });
            });
        };
    }

    listCacheUpdaterAfterUpdateSettings(api: GqlApiQueries /*, id: any, data: any, operation: string*/) {
        return (store, obj): any => {
            if (!(api && api.cacheupdate && api.cacheupdate.length)) {
                return;
            }
            // Read data from the cache.
            api.cacheupdate.forEach(updateDescr => {
                const updateObj = updateDescr.obj;
                const updateKey = updateDescr.key || api.entity;
                const cacheddata = store.readQuery({ query: updateObj });
                for (const key in obj.data) {
                    if (obj.data.hasOwnProperty(key)) {
                        const el = obj.data[key];
                        if (el.__typename && el.__typename.toLowerCase() === api.entity.toLowerCase()) {
                            const newData = obj.data[key];
                            cacheddata[updateKey] = [...cacheddata[updateKey], newData];
                        }

                    }
                }
                store.writeQuery({ query: updateObj, data: cacheddata });
            });
        };
    }

    getRecord(entityName: string, id: any, noCached: boolean = true): Promise<any> {

        if (entityName === 'settings') {
            return this.getSettingsObject(id, noCached);
        }

        if (entityName === 'reports') {
            return this.getReportSettingsValues(id, noCached);
        }

        const api: GqlApiQueries = this.sdkMap.find(a => a.entity === entityName);

        if (api && api.query) {
            return api.query({ id: id },
                noCached ? { fetchPolicy: 'no-cache' } : {}
            ).toPromise().then((data) => {
                const res = data.data[entityName];
                if (res instanceof Array && res.length === 1) {
                    return res[0];
                } else if (res.items && res.items instanceof Array) {
                    if (res.items.length === 1) {
                        return res.items[0];
                    } else {
                        return res.items;
                    }
                }
                return res;
            });
        }

        const err = Object.assign({}, DATAERROR_UNKNOWN_ENTITY, { description: 'getRecord for entity: "' + entityName + '" not implemented.' });
        return Promise.reject(err);
    }

    updateRecord(entityName: string, id: string | number, data: any): Promise<any> {
        if (entityName === 'settings') {
            return this.setSettingsObject(data);
        }
        const api: GqlApiQueries = this.sdkMap.find(a => a.entity === entityName);

        if (api && api.update) {
            return api.update({
                id: id,
                [api.updateKey || entityName]: data,
            }).toPromise().then((result) => {
                return result;
                // return result.data[api.updateKey || entityName];
            });
        }

        const err = Object.assign({}, DATAERROR_UNKNOWN_ENTITY, { description: 'updateRecord for entity: "' + entityName + '" not implemented.' });
        return Promise.reject(err);
    }

    createRecord(entityName: string, data: any, cacheUpdate: () => ICacheUpdateRecord[] = null): Promise<any> {
        const api: GqlApiQueries = this.sdkMap.find(a => a.entity === entityName);

        if (api && api.create) {

            const updateObjs = cacheUpdate ? cacheUpdate() : api.cacheupdate;

            return api.create({ [entityName]: data }, {

                update: this.listCacheUpdaterAfterInsert(api.entity, updateObjs)

            }).toPromise().then((result) => {
                return result;
            });
        }

        const err = Object.assign({}, DATAERROR_UNKNOWN_ENTITY, { description: 'createRecord for entity: "' + entityName + '" not implemented.' });
        return Promise.reject(err);
    }

    // getList(entityName: string, filters = {}, range: ISelectionRange | null, forceRead = false) {
    //     const api: GqlApiQueries = this.sdkMap.find(a => a.entity === entityName);

    //     if (api && api.list) {
    //         const options: QueryOptionsAlone<any> = {};
    //         if (forceRead) {
    //             options.fetchPolicy = 'network-only';

    //         }
    //         const variables = {};
    //         if (filters && !AppUtils.isObjectEmpty(filters)) {
    //             variables['filter'] = this.calcFilter(filters);
    //         }

    //         if (range && !AppUtils.isObjectEmpty(range)) {
    //             variables['range'] = range;
    //         }

    //         return api.list(variables, options).toPromise().then((result) => {
    //             return result.data[api.listKey || entityName];
    //         });


    //     }

    //     const err = Object.assign({}, DATAERROR_UNKNOWN_ENTITY, { description: 'getList for entity: "' + entityName + '" not implemented.' });
    //     return Promise.reject(err);
    // }

    getPgList(entityName: string, filters = {}, range: IRange | null, forceRead = false, showDeleted = false) {
        const api: GqlApiQueries = this.sdkMap.find(a => a.id === entityName);

        if (api && api.list) {
            const options: QueryOptionsAlone<any, any> = {};
            if (forceRead) {
                options.fetchPolicy = 'no-cache';

            }

            const variables = {};
            if (filters && !AppUtils.isObjectEmpty(filters)) {
                // FIXIT: дикий костыль - решить с фильтрацией по 'author.fullName' + 'authorName'
                if (filters['author.fullName']) {
                    const vf = AppUtils.deepCopy(filters);
                    const vv = vf['author.fullName']
                    const ff = [{ name: 'authorName', like: vv, op: IFilterNextOpEnum.Or }, { name: 'author.fullName', like: vv, op: IFilterNextOpEnum.Or }]
                    delete vf['author.fullName']

                    variables['filters'] = [...ff, ... this.calcFilter(vf)];

                } else {
                    variables['filters'] = this.calcFilter(filters);
                }
            }

            if (range && !AppUtils.isObjectEmpty(range)) {
                variables['range'] = range;
                variables['skipTotal'] = range.skip !== 0;
            }
            if (showDeleted) {
                variables['withDeleted'] = true;
            }

            return api.list(variables, options).toPromise().then((result) => {
                return result.data[api.listKey || entityName];
            });
        }

        const err = Object.assign({}, DATAERROR_UNKNOWN_ENTITY, { description: 'getList for entity: "' + entityName + '" not implemented.' });
        return Promise.reject(err);
    }

    calcFilter(filters: any, prefix = ''): IFilter[] {
        let res: IFilter[] = [];

        for (const key in filters) {
            if (filters.hasOwnProperty(key)) {
                const fi = filters[key];

                if (fi instanceof Array) {
                    res.push(
                        { name: prefix + key, in: fi, op: IFilterNextOpEnum.And },
                    );
                } else if (fi instanceof Object) {
                    let isObj = true;
                    if (fi.lessOrEqual !== undefined) {
                        res.push(
                            { name: prefix + key, lessOrEqual: fi.lessOrEqual, op: IFilterNextOpEnum.And }
                        );
                        isObj = false;
                    }
                    if (fi.less !== undefined) {
                        res.push(
                            { name: prefix + key, less: fi.less, op: IFilterNextOpEnum.And }
                        );
                        isObj = false;
                    }
                    if (fi.more !== undefined) {
                        res.push(
                            { name: prefix + key, more: fi.more, op: IFilterNextOpEnum.And }
                        );
                        isObj = false;
                    }
                    if (fi.greaterOrEqual !== undefined) {
                        res.push(
                            { name: prefix + key, greaterOrEqual: fi.greaterOrEqual, op: IFilterNextOpEnum.And }
                        );
                        isObj = false;
                    }
                    if (isObj) {
                        res = res.concat(this.calcFilter(fi, key + '.'));
                    }
                } else {
                    if (fi === 'IS_NULL') {
                        res.push(
                            { name: prefix + key, equal: 'null', op: IFilterNextOpEnum.And }
                        );
                    } else if (fi === 'IS_NOT_NULL') {
                        res.push(
                            { not: true, name: prefix + key, equal: 'null', op: IFilterNextOpEnum.And }
                        );
                    } else {
                        if (typeof fi === 'string' && fi.indexOf('%') !== -1) {
                            res.push(
                                { name: prefix + key, like: fi, op: IFilterNextOpEnum.And }
                            );
                        } else {
                            res.push(
                                { name: prefix + key, equal: fi, op: IFilterNextOpEnum.And }
                            );
                        }
                    }
                }
            }
        }

        return res;
    }

    login(variables: ILoginQueryVariables, options?: QueryOptionsAlone<ILoginQueryVariables, any>) {
        if (!options) {
            options = {};
        }
        options.fetchPolicy = 'no-cache';

        return super.login(variables, options);
    }


    setSettingsObject(data: any) {
        const values = AppUtils.objectToKeyValue(data);
        values.forEach(
            (s) => s.key = s.key + '.'
        );

        if (values.length) {

            return this.setSettings(<ISetSettingsMutationVariables>{ settings: values }, {
                // fetchPolicy: 'no-cache',
                //  update: this.listCacheUpdaterAfterUpdateSettings({entity: 'settings', cacheupdate: [{obj: GetSettingsDocument}]})
            }).toPromise();


            //  if (api && api.create) {
            //     return api.create({ [entityName]: data[entityName] }, {
            //             update: this.listCacheUpdaterAfterInsert(api/*, -1, data[entityName], ''*/)
            //         }).toPromise().then ( (result) => {
            //         });
            // }
        }

        return Promise.resolve(null);
    }

    getReportSettingsValues(key: string, noCached: boolean = true): Promise<any> {
        return this.getReportSettings(<IGetReportSettingsQueryVariables>{ id: key },
            noCached ? { fetchPolicy: 'no-cache', } : {}
        ).toPromise().then((data) => {
                return data?.data?.reportSettings[0];
        });
    }

    getSettingsObject(key: string, noCached: boolean = true): Promise<any> {
        if (key[key.length - 1] !== '.') {
            key = key + '.';
        }
        return this.getSettings(<IGetSettingsQueryVariables>{ pathes: key },
            noCached ? { fetchPolicy: 'no-cache', } : {}
        ).toPromise().then((data) => {
            if (data && data.data) {
                return {
                    value: AppUtils.keyValueToObject(<{ key: string, value: string }[]>data.data.settings),
                    config: data.data.settings.map(s => {
                        // if (s.key === 'system.api.') {
                        //     console.log('s', s);
                        // }
                        if (s.key.endsWith('.')) {
                            const config = Object.assign({}, s);
                            config.key = s.key.substr(0, s.key.length - 1);
                            return config;
                        }
                        return s;
                    }),
                };
            } else {
                return null;
            }
        });
    }

    clearCashe() {
        // ApolloClient
        // gqlCache.gc();
        // IGetAppealGQL.
        // this.
        //     this.refetch({}, {
        //         awaitRefetchQueries: true,
        //         refetchQueries: [IGetTerritoryListGQL['document']],
        //     });

        //     await client.mutate({
        //         mutation: COMPLETE_CHALLENGE,
        //         variables: data,
        //         refetchQueries: () => [{query: UNCOMPLETED_CHALLENGES}]
        // })
        // return this.getTerritoryList({}, {
        //     fetchPolicy: 'network-only',
        // }).toPromise().then( d => {
        //     return d;
        // });
    }

    deleteRecordByID(entity: string, id: string | number): Promise<any> {
        const api: GqlApiQueries = this.sdkMap.find(a => a.entity === entity);
        if (api.delete) {
            return api.delete({ id: id }).toPromise();
        }
        return this.updateRecord(entity, id, { deleted: true });
    }

    undeleteRecordByID(entity: string, id: string | number): Promise<any> {
        // const api: GqlApiQueries = this.sdkMap.find(a => a.entity === entity);
        // if (api.delete) {
        //     return api.delete({id: id}).toPromise();
        // }
        return this.updateRecord(entity, id, { deleted: false });
    }

    rejectMessageId(id: string, text: string, cacheUpdate: () => ICacheUpdateRecord[] = null) {
        const updateObjs = cacheUpdate();

        return super.rejectMessage({ id: id, reason: text }, {
            update: (proxy, data): any => { // your mutation response
                // this.listCacheUpdaterAfterInsert('message', updateObjs)(proxy,
                // { data: { createMessage: data.data.createMessage } }
                // );

            }
        }).toPromise();
    }


    updateAppeal(variables, options) {
        return super.updateAppeal(variables, options);
    }

    getVoteRootModId(variables: IGetVoteRootModIdQueryVariables, options?: QueryOptionsAlone<IGetVoteRootModIdQueryVariables, any>) {
        return from(super.getVoteRootModId(variables, options).toPromise().then(
            data => {
                const polls = AppUtils.getValueByPath(data, 'data.voteRoot.items') as IVoteRoot[];
                for (let i = 0; i < polls.length; i++) {
                    const poll = polls[i];
                    if (poll && poll.votes) {
                        for (let questsi = 0; questsi < poll.votes.length; questsi++) {
                            const quest = poll.votes[questsi];
                            if (quest && quest.voteItems) {
                                quest.voteItems = quest.voteItems.sort((a, b) => (Math.sign(a.weight - b.weight)));
                            }
                        }
                        poll.votes = poll.votes.sort((a, b) => (Math.sign(a.weight - b.weight)));
                    }
                }
                return data;
            }
        ));

    }
}
