


export interface ICacheUpdateRecord {
    obj: any | string;
    key?: string;
    variables?: any;
}

export interface GqlApiQueries {
    id: string;
    entity: string;
    list?: any;
    listKey?: string;
    query?: any;
    update?: any;
    updateKey?: string;
    create?: any;
    delete?: any;
    cacheupdate?: ICacheUpdateRecord[];
}
