import {Component, OnInit, Input, ViewChild, Output, EventEmitter} from '@angular/core';
import {MessageService} from 'src/app/services/message.service';
import {FileIoService} from 'src/app/services/file-io.service';
import {AuthService} from 'src/app/services/auth.service';

@Component({
    selector: 'app-filepond-http',
    templateUrl: './filepond-http.component.html',
    styleUrls: ['./filepond-http.component.css']
})
export class FilepondHttpComponent implements OnInit {

    @Input() files: any;
    @ViewChild('myPond', {static: true}) myPond: any;
    @Output() oninit: EventEmitter<any> = new EventEmitter<any>();
    @Output() onaddfile: EventEmitter<any> = new EventEmitter<any>();


    pondOptions = {
        class: 'my-filepond',
        multiple: true,
        'data-max-file-size': '3MB',
        'data-max-files': 3,
        labelIdle: 'Перетащите файлы сюда',
        acceptedFileTypes: 'image/jpeg, image/png',

        allowDrop: true,
        allowReplace: true,
        instantUpload: true,
        server: {
            url: '/client',
            process: (fieldName, file, metadata, load, error, progress, abort) => {

                // MessageService.sendingFile = true;
                // fieldName is the name of the input field
                // file is the actual file object to send
                const formData = new FormData();
                formData.append('uploadedFile', file, file.name);

                const request = new XMLHttpRequest();


                request.open('POST', 'http://localhost:6000/api/file'); // '/api/file');
                request.setRequestHeader('Authorization', 'Bearer ' + this._authSrv.getToken());

                // Should call the progress method to update the progress to 100% before calling load
                // Setting computable to false switches the loading indicator to infinite mode
                request.upload.onprogress = (e) => {
                    progress(e.lengthComputable, e.loaded, e.total);
                };

                // Should call the load method when done and pass the returned server file id
                // this server file id is then used later on when reverting or restoring a file
                // so your server knows which file to return without exposing that info to the client
                const files = this.files;
                request.onload = function () {
                    if (request.status >= 200 && request.status < 300) {
                        // the load method accepts either a string (id) or an object
                        let id = -1;
                        if (request.responseText.startsWith('id=')) {
                            id = Number(request.responseText.substr(3));
                        }


                        // const ufile = JSON.parse('{' + request.responseText + ' }');
                        load(id);
                        // files.push(ufile.extId);
                        // MessageService.sendingFile = false;
                    } else {
                        // Can call the error method if something is wrong, should exit after
                        error('oh no');
                        // MessageService.sendingFile = false;
                    }
                };
                request.send(formData);

                // Should expose an abort method so the request can be cancelled
                return {
                    abort: () => {
                        // This function is entered if the user has tapped the cancel button
                        request.abort();

                        // Let FilePond know the request has been cancelled
                        abort();
                    }
                };
            },
            revert: (uniqueFileId, load, error) => {

                // Should remove the earlier created temp file here
                // ...

                // Can call the error method if something is wrong, should exit after
                // MessageService.sendingFile = true;
                const request = new XMLHttpRequest();
                request.open('DELETE', '/appointment/removeFile/' + uniqueFileId);
                const files = this.files;
                request.onload = function () {
                    if (request.status >= 200 && request.status < 300) {
                        for (let i = 0; i < files.length; i++) {
                            if (files[i] === uniqueFileId) {
                                files.splice(i, 1);
                                break;
                            }
                        }
                        load();
                    } else {
                        // Can call the error method if something is wrong, should exit after
                        error('oh no');
                    }
                    // MessageService.sendingFile = false;
                };
                request.send();
            },
            restore: null,
            // restore: './restore.php?id=',
            fetch: '/getFile?id='
        }
    };


    constructor(
        private _authSrv: AuthService,
        // private _fio: FileIoService,
    ) {
    }

    ngOnInit() {
    }

    pondHandleInit() {
    }

    pondHandleAddFile(event: any) {
    }
}


