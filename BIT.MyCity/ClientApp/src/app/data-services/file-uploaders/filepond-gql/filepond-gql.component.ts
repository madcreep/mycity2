import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { GraphQLClient } from '../../graphql/graphql.client';
import { Observable, fromEvent } from 'rxjs';
import { pluck } from 'rxjs/operators';
import { FileIoService } from 'src/app/services/file-io.service';
import { IFileUpload } from 'src/generated/types.graphql-gen';

@Component({
    selector: 'app-filepond-gql',
    templateUrl: './filepond-gql.component.html',
    styleUrls: ['./filepond-gql.component.css']
})
export class FilepondGqlComponent implements OnInit {

    @Input() files: any;
    @ViewChild('myPond', { static: true }) myPond: any;
    @Output() oninit: EventEmitter<any> = new EventEmitter<any>();
    @Output() onaddfile: EventEmitter<any> = new EventEmitter<any>();

    public pondOptions = {
        class: 'my-filepond',
        multiple: true,
        'data-max-file-size': '3MB',
        'data-max-files': 3,
        labelIdle: 'Перетащите файлы сюда',
        acceptedFileTypes: 'image/jpeg, image/png',

        allowDrop: true,
        allowReplace: true,
        instantUpload: true,
        server: {
            url: '/client',
            process: this.pondProcessFn.bind(this),
            revert: this.pondRevertFn.bind(this),
            restore: null,
            // restore: './restore.php?id=',
            fetch: '/getFile?id='
        }
    };

    constructor(
        private _gql: GraphQLClient,
        private _fio: FileIoService,
    ) { }


    ngOnInit() {
    }

    fileToBase64(fileReader: FileReader, fileToRead: File): Observable<string> {
        fileReader.readAsDataURL(fileToRead);
        return fromEvent(fileReader, 'load').pipe(
            pluck('currentTarget', 'result')
        );
    }

    pondProcessFn(fieldName, file, metadata, load, error, progress, abort) {
        const BLOCKSIZE = 0x100000;
        let fid = '-1';
        // let firstBlock = Promise.resolve(true);

        this._fio.parseFile(file, BLOCKSIZE, (offset, length, data) => {

            const filevar: IFileUpload = {
                pos: 0,
                data: null,
            };

            if (offset === 0) {
                filevar.name = file.name;
                filevar.len = file.size;
            } else {
                filevar.id = fid;
                filevar.pos = offset;
            }
            filevar.data = data;
            const sep = ';base64,';
            const infopos = data.indexOf(sep);
            if (infopos !== -1) {
                filevar.data = data.substr(infopos + sep.length);
            }

            const loadblock = new Promise<boolean>(
                (res, rej) => {
                    return this._gql.fileUpload({
                        file: filevar,
                    }).toPromise().then( (uploaddata) => {
                        if (offset === 0) {
                            fid = uploaddata.data.fileUpload.id;
                        }
                        progress(uploaddata.data.fileUpload.percent, offset, file.size);

                        if (uploaddata.data.fileUpload.percent === 100) {
                            load(fid);
                        }
                        return res(true);
                    });
                });

            return loadblock;

        });


        // const fin: File = <File>file; //file.name;
        // const myReader: FileReader = new FileReader();

        // this.fileToBase64(myReader, fin)
        // .subscribe(base64image => {
        //     console.log('FilepondGqlComponent -> pondProcessFn -> base64image', base64image);
        //   // do something with base64 image..
        // });

        // myReader.onloadend = (e) => {
        //   this.image = myReader.result;
        // };
        // myReader.readAsDataURL(file);



        // const i = 0;
        // const fid = -1;
        // const BLOCKSIZE = 0x1000;
        // const filevar: IFileUpload = {
        //     pos: 0,
        //     data: null,
        //     // parentId: 0,
        //     // parentType: 'hz',
        // };
        // if (i === 0) {
        //     filevar.name = file.name;
        //     filevar.len = file.size;
        // } else {
        //     filevar.id = fid;
        //     filevar.pos = i*BLOCKSIZE;
        // }

        // gql.fileUpload({
        //     file: filevar,
        // }).toPromise().then( (data) => {
        //     console.log('data', data);
        //     // progress(e.lengthComputable, e.loaded, e.total);
        // });

        // MessageService.sendingFile = true;
        // fieldName is the name of the input field
        // file is the actual file object to send
        // const formData = new FormData();
        // formData.append('file', file, file.name);

        // const request = new XMLHttpRequest();
        // request.open('POST', '/appointment/upload');


        // Should call the progress method to update the progress to 100% before calling load
        // Setting computable to false switches the loading indicator to infinite mode
        // request.upload.onprogress = (e) => {
        // progress(e.lengthComputable, e.loaded, e.total);
        // };

        // Should call the load method when done and pass the returned server file id
        // this server file id is then used later on when reverting or restoring a file
        // so your server knows which file to return without exposing that info to the client
        // const files = this.files;
        // request.onload = function () {
        //     if (request.status >= 200 && request.status < 300) {
        //         // the load method accepts either a string (id) or an object
        //         const file1 = JSON.parse(request.responseText);
        //         load(file1.extId);
        //         files.push(file1.extId);
        //         // MessageService.sendingFile = false;
        //     } else {
        //         // Can call the error method if something is wrong, should exit after
        //         error('oh no');
        //         // MessageService.sendingFile = false;
        //     }
        // };
        // request.send(formData);

        // Should expose an abort method so the request can be cancelled
        return {
            abort: () => {
                // This function is entered if the user has tapped the cancel button
                // request.abort();

                // Let FilePond know the request has been cancelled
                abort();
            }
        };
    }

    pondRevertFn(uniqueFileId, load, error) {

        // Should remove the earlier created temp file here
        // ...

        // Can call the error method if something is wrong, should exit after
        // MessageService.sendingFile = true;
        // console.log(uniqueFileId);
        // const request = new XMLHttpRequest();
        // request.open('DELETE', '/appointment/removeFile/' + uniqueFileId);
        // const files = this.files;
        // request.onload = function () {
        //     if (request.status >= 200 && request.status < 300) {
        //         for (let i = 0; i < files.length; i++) {
        //             if (files[i] === uniqueFileId) {
        //                 files.splice(i, 1);
        //                 break;
        //             }
        //         }
        //         load();
        //     } else {
        //         // Can call the error method if something is wrong, should exit after
        //         error('oh no');
        //     }
        //     // MessageService.sendingFile = false;
        // };
        // request.send();
    }


    pondHandleInit() {
    }
    pondHandleAddFile(event: any) {
    }
}
