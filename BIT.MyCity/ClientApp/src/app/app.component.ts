import { Component, OnInit } from '@angular/core';
import { L10n, loadCldr } from '@syncfusion/ej2-base';
import { AuthService } from './services/auth.service';
import { ErrorsService } from './services/errors.service';
import { Title } from '@angular/platform-browser';
import { SettingsService } from './services/settings.service';
import { Subscriptionable } from './common/subscriptionable';
import { SETTINGS_MAIN_TITLE } from './services/setting-names.const';


loadCldr(
    require('cldr-data/supplemental/numberingSystems.json'),
    require('cldr-data/main/ru/ca-gregorian.json'),
    require('cldr-data/main/ru/numbers.json'),
    require('cldr-data/main/ru/timeZoneNames.json'),
    require('cldr-data/supplemental/weekData.json') // To load the culture based first day of week
);

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent extends Subscriptionable implements OnInit {
    title = 'app';

    // isUpdating = true;

    constructor (
        private _authSrv: AuthService,
        private _errSrv: ErrorsService,
        private titleSrv: Title,
        private settingsSrv: SettingsService,
    ) {
        super();
        this.subscriptions.push(
            this.settingsSrv.subscribeToValue(SETTINGS_MAIN_TITLE,
                (value) => this.titleSrv.setTitle(value)
            )
        );
    }
    ngOnInit(): void {
        L10n.load({
            'ru': {
                'dropdowns': {
                    'noRecordsTemplate': 'Не найдено записей',
                    'actionFailureTemplate': 'Запрос прерван',
                    'overflowCountTemplate': '+${count} еще..',
                    'totalCountTemplate': '${count} выбрано'
                },
                'daterangepicker': {
                    placeholder: '... - ...',
                    startLabel: 'Дата начала',
                    endLabel: 'Дата окончания',
                    applyText: 'Применить',
                    cancelText: 'Отмена',
                    selectedDays: 'Выбранные дни',
                    days: 'Дней',
                    customRange: 'benutzerdefinierten Bereich'
                },
            },
        });
    }

}
