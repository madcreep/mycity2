import { Injectable } from '@angular/core';
import { GraphQLClient } from '../data-services/graphql/graphql.client';
import { RUBRIC_ORG_DESCRIPTOR } from '../model/datatypes/rubric-organisations.const';
import { TERRITORY_DESCRIPTOR } from '../model/datatypes/territory.const';
import { VOTEROOTS_DESCRIPTOR } from '../model/datatypes/vote-roots.const';
import { CLAIMS_DESCRIPTOR } from '../model/datatypes/claims.const';
import { MODERATORS_DESCRIPTOR } from '../model/datatypes/moderators.const';
import { APPEAL_CIT_DESCRIPTOR } from '../model/datatypes/appeals/appeal-citizen.const';
import { APPEAL_UL_DESCRIPTOR } from '../model/datatypes/appeals/appeal-organizations.const';
import { NEWS_DESCRIPTOR } from '../model/datatypes/news.const';
import { TOPIC_DESCRIPTOR } from '../model/datatypes/topic';
import { FEEDBACK_DESCRIPTOR } from '../model/datatypes/appeals/appeal-feedback.const';
import { ENUMVERBS_DESCRIPTOR } from '../model/datatypes/enumverbs.const';
import { RUBRIC_MAIN_DESCRIPTOR } from '../model/datatypes/rubric-main.const';
import { EXECUTORS_DESCRIPTOR } from '../model/datatypes/executors.const';
import { APPEAL_CIT_EX_DESCRIPTOR } from '../model/datatypes/appeals/appeal-citizen-executors.const';
import { InjectorInstance } from '../helpers/app.static';



const DESCRIPTORS = [
    RUBRIC_ORG_DESCRIPTOR,
    RUBRIC_MAIN_DESCRIPTOR,
    TERRITORY_DESCRIPTOR,
    VOTEROOTS_DESCRIPTOR,
    MODERATORS_DESCRIPTOR,
    CLAIMS_DESCRIPTOR,
    APPEAL_CIT_DESCRIPTOR,
    APPEAL_UL_DESCRIPTOR,
    APPEAL_CIT_EX_DESCRIPTOR,
    NEWS_DESCRIPTOR,
    TOPIC_DESCRIPTOR,
    FEEDBACK_DESCRIPTOR,
    ENUMVERBS_DESCRIPTOR,
    EXECUTORS_DESCRIPTOR,

];


@Injectable({
    providedIn: 'root'
})
export class DescriptorsLoaderService {


    registerApis() {
        setTimeout(() => {
        DESCRIPTORS
            .forEach(descr => {
                const gqlSdk = InjectorInstance.get(GraphQLClient);
                const api = descr.getApiDescriptor(gqlSdk);
                if (api) {
                    return gqlSdk.addApiDescriptor(api);
                }
            });

        }, 100);

    }

    static initialize = (service: DescriptorsLoaderService) => {
        return () => {
            return service.registerApis();
        };
    };

}
