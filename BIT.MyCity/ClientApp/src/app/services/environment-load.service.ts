import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnvSettings } from '../model/settings/envsettings';

@Injectable({
    providedIn: 'root'
})
export class EnvironmentLoadService {

    static isLoaded = false;
    static loader: Promise<any> = null;
    static angconfigUrl = 'assets/angconfig.json';
    constructor(private http: HttpClient) {
    }

    static initializeEnvironmentConfig = (appConfig: EnvironmentLoadService) => {
        return () => {
            return appConfig.init();
        };
    };

    init() {
        if (!EnvironmentLoadService.loader) {
            EnvironmentLoadService.loader = this.http.get(
                EnvironmentLoadService.angconfigUrl
            ).toPromise().then(data => {
                EnvSettings.load(data);
                EnvironmentLoadService.isLoaded = true;
            });
        }
        return EnvironmentLoadService.loader;
    }
}


