import { SettingsService } from './settings.service';
import { RestError } from '../rest-error';
import { Injectable, EventEmitter } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { } from '@angular/common/http';
import { Subscription, BehaviorSubject } from 'rxjs';
import { IUser } from 'src/generated/types.graphql-gen';
import { DataService } from './data.service';
import { AppUtils } from '../helpers/app.static';
import { Subscriptionable } from '../common/subscriptionable';
import { IClaim, CLAIM_SA_ADMIN } from './claims.const';

const STORAGE_TOKEN_NAME = 'jwt';
const TOKEN_UPDATE_BEFORE_END = 30 * 1000; // * 60 * 1000; /* за 30 сек до окончания */
const IDLE_TIME_BEFORE_DISCONNECT = 5 * 60 * 1000;

@Injectable({
    providedIn: 'root'
})
export class AuthService extends Subscriptionable {
    loggedInUser: IUser;
    loggedClaims: IClaim[] = [];
    decodedToken: any;

    public loginChanged: BehaviorSubject<any> = new BehaviorSubject<any>({}); // EventEmitter<any> = new EventEmitter<any>();
    public onNewToken: EventEmitter<any> = new EventEmitter<any>();

    private _returnUrl: string;
    private _updateTokenTimer: NodeJS.Timeout;
    private _lastSyncTime: number;

    constructor(
        private jwtHelper: JwtHelperService,
        private dataSrv: DataService, ) {
        super();

        this.subscriptions.push(this.dataSrv.onQuery.subscribe (
            () => {
                this._lastSyncTime = new Date().getTime();
        }));
    }


    get returnUrl () {
        return this._returnUrl;
    }


    checkAuth(): Promise<any> {
        const token = localStorage.getItem(STORAGE_TOKEN_NAME);
        if (!token || !this.tokenIsOk(token)) {
            return Promise.resolve(false);
        } else {
            if (!this.loggedInUser) {
                return this.updateToken().then ( (res) => {
                    return res;
                });
            } else {
                this.tokenRefresherStart();
                return Promise.resolve(true);
            }
        }
    }

    updateToken() {
        return this.dataSrv.updateToken().then(data => {
            this.setToken(data.data.updateToken.token);
            this.loggedInUser = <IUser>data.data.updateToken.user;
            this.loggedClaims = this.loggedInUser.allClaims;
            this.loginChanged.next(this.loggedInUser);
            this.tokenRefresherStart();
                return true;
        }).catch( (err) => {
            return false;
        });
    }

    tokenRefresherStart() {
        if (this._updateTokenTimer) {
            return;
        }
        const token = localStorage.getItem(STORAGE_TOKEN_NAME);
        this.decodedToken = this.jwtHelper.decodeToken(token);
        const tokenExpirationDate = this.jwtHelper.getTokenExpirationDate(token);
        let currTime = new Date().getTime();
        const updatePeriod = tokenExpirationDate.getTime() - currTime - TOKEN_UPDATE_BEFORE_END;
        if (updatePeriod > 0) {
            this._updateTokenTimer = setTimeout(() => {
                currTime = new Date().getTime();
                clearTimeout(this._updateTokenTimer);
                if (currTime - this._lastSyncTime < IDLE_TIME_BEFORE_DISCONNECT) {
                    this._updateTokenTimer = null;
                    this.updateToken();
                }
            }, updatePeriod);
        }
    }


    tokenIsOk (token: string): boolean {
        try {
            if (token === null || token === '') {
                return false;
            }
            const tokenExpirationDate = this.jwtHelper.getTokenExpirationDate(token);
            if ( (tokenExpirationDate.getTime() - (new Date()).getTime()) > 0) {
                return true;
            }
        } catch (e) {
            console.log(e);
        }
        return false;
    }


    /**
     *
     * @param login
     * @param password
     * @param isSilenceUpdate предотвращает перерисовку интерфейса на момент обновлени токена.
     */
    public loginUser (login: string, password: string, isSilenceUpdate = false): Promise<any> {
        if (!isSilenceUpdate) {
            this._clearAuth();

        }
        return this.dataSrv.login( login, password).then((data) => {
            let token = null;

            if (data && data.data && data.data.login && data.data.login.token) {
                token = data.data.login.token;
                this.setToken(token);
                this.tokenRefresherStart();

            }

            if (!token) {
                throw new RestError ( {http: 434 });
            }
            this.loggedInUser = <IUser>data.data.login.user;
            this.loggedClaims = this.loggedInUser.allClaims;

            this.loginChanged.next(this.loggedInUser);
            return this.loggedInUser;
        }).catch((error) => {
            this.setToken(null);
            throw error;
        });
    }

    public logoutUser(requestServer: boolean = true): Promise<any> {
        this._clearAuth();
        this.loginChanged.next(null);
        return Promise.resolve(null);
    }

    _clearAuth() {
        this.loggedInUser = null;
        this.decodedToken = null;
        this.setToken(null);
        clearTimeout(this._updateTokenTimer);
    }

    public isLoggedIn(): boolean {
        const token = localStorage.getItem(STORAGE_TOKEN_NAME);
        return (token !== null) && !!this.loggedInUser;
    }


    public getToken() {
        const token = localStorage.getItem(STORAGE_TOKEN_NAME);
        return token;
    }

    public registerUser(credentials: string, success, error): Subscription {
        // TODO: [MYC-107] register user
    //     return this.http.post<any>(this.baseUrl + 'api/auth/register', credentials, {
    //         headers: new HttpHeaders({
    //             'Content-Type': 'application/json'
    //         })
    //     }).subscribe(response => {
    //         this.userFromResponse(response);
    //         success();
    //     }, err => {
    //         error(err);
    //     });
        return null;
    }

    public isAuthorizedTo(section): boolean {

        if (!this.loggedInUser || !this.isLoggedIn() || !this.loggedClaims) {
            return false;
        }
        const needClaims = AppUtils.getValueByPath(section, 'route.data.needClaims');

        if (needClaims) {
            return this.isClaimHave(needClaims);
        }
        return true;
    }

    // [
    //     [ p1 && p2 && p3] || [ p4 && p5 && p6]
    // ]
    public isClaimHave(permissions: IClaim | IClaim[] | IClaim[][], claims: IClaim[] = this.loggedClaims) {
        if (!Array.isArray(permissions)) {
            const nc: IClaim = permissions;
            return !!claims.find( rc => (rc.type === nc.type) && ((rc.value === nc.value) || (!nc.value)));
        } else if (!permissions.length) {
            return true;
        } else {
            for (const per of permissions) {
                if (Array.isArray(per)) {
                    const andVar = this._isClaimHaveAll(per, claims);
                    if (andVar) {
                        return true;
                    }
                } else {
                    const anyVar = this.isClaimHave(per, claims);
                    if (anyVar) {
                        return true;
                    }
                }
            }
            return false;
        }

    }

    public _isClaimHaveOneOf(permissions: IClaim[], claims: IClaim[] = this.loggedClaims) {
        const founded = permissions.map(
            nc => claims.find ( rc => (rc.type === nc.type) && ((rc.value === nc.value) || (!nc.value)) )).filter (r => !!r);
        if (founded.length > 0) {
            return true;
        }
        return false;
    }

    public _isClaimHaveAll(permissions: IClaim[], claims: IClaim[] = this.loggedClaims) {

        const founded = permissions.map(
            nc => claims.find ( rc => (rc.type === nc.type) && ((rc.value === nc.value) || (!nc.value)) )).filter (r => !!r);
        if (founded.length < permissions.length) {
            return false;
        }
        return true;
    }


    public isSuperAdmin(user: IUser = this.loggedInUser) {
        if (!user || !user.allClaims) {
            return false;
        }
        return user.allClaims.find (c => c.type === CLAIM_SA_ADMIN.type);
    }


    private setToken(token: any) {
        if (token && token.startsWith('Bearer')) {
            token = token.split(' ')[1];
        }

        const oldToken = this.getToken();
        if (oldToken !== token) {
            this.onNewToken.emit(token);
        }
        if (token) {
            localStorage.setItem(STORAGE_TOKEN_NAME, token);
        } else {
            localStorage.removeItem(STORAGE_TOKEN_NAME);
        }

    }
}
