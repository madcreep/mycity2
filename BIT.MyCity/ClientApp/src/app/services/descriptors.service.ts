import { Injectable } from '@angular/core';
import { BaseTableDescriptor } from '../model/datatypes/descriptors.const';
import { DATAERROR_UNKNOWN_ENTITY } from '../errors.consts';
import { RestError } from '../rest-error';
import { SettingsService } from './settings.service';
import { Subscriptionable } from '../common/subscriptionable';
import { DataService } from './data.service';
@Injectable({
    providedIn: 'root'
})
export class DescriptorsService extends Subscriptionable {

    static preRegisters = [];

    private _descriptors = new Map<string, BaseTableDescriptor>();

    constructor(
        private _settingsSrv: SettingsService,
        private _dataSrv: DataService,
    ) {
        super();

        for (const descr of DescriptorsService.preRegisters) {
            this._descriptors.set(descr.id, descr);
            const api = descr.getApiDescriptor(this._dataSrv.getApiContext());
            if (api) {
                this._dataSrv.registerApi(api);
            }
        }

    }

    static preRegisterDescriptor(id, descriptor) {
        this.preRegisters.push(descriptor);
    }

    public dataRead(descriptor: BaseTableDescriptor, id: string | number, targetData: any) {
        return this._dataSrv.getRecord(descriptor.entityName, id).then (data => {
            targetData[descriptor.entityName] = data;

            if (descriptor.entityName === 'subsystem') {
                return (descriptor as any).__GENERAteSubsTMPFoo(descriptor, id, targetData, this._settingsSrv);
            }
            return targetData;
        });
    }

    public descriptorForEntity(entity: string): BaseTableDescriptor {
        const descr = this._descriptors.get(entity);
        if (!descr) {
            throw new RestError ({ code: DATAERROR_UNKNOWN_ENTITY.code, message: DATAERROR_UNKNOWN_ENTITY.message + ' ' + entity } );
        }
        return descr;
    }

    public additionalDataRead(descriptor: BaseTableDescriptor, id: string | number, targetData: any): Promise<any> {

        return Promise.resolve(null);
    }

}
