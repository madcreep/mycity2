import {Injectable} from '@angular/core';
import {Workbook} from 'exceljs';
import * as fs from 'file-saver';
import {InjectorInstance} from '../helpers/app.static';
import {EnumverbService} from './enumverb.service';
import {GridFormatters} from '../model/datatypes/grid-formatters';

@Injectable({
    providedIn: 'root'
})
export class ExportExcelService {

    constructor() {
    }

    saveAppeals(excelData: { title: any; data: any; headers: any }) {
        const instance = InjectorInstance.get(EnumverbService);
        const statusesList = instance.getEnumsUnsafe('appealStatus');

        function statuseText(status: string) {
            const record = statusesList.find( ev => ev.value === status);
            return record?.shortName ?? '';
        }

        const title = excelData.title;
        const header = excelData.headers;
        const data = excelData.data;

        console.log(data);
        const workbook = new Workbook();
        const worksheet = workbook.addWorksheet(title);


        worksheet.addRow([]);
        const v1widthMultiplier = 0.15;
        worksheet.columns = [
            {header: '№ п/п', key: 'id', width: 60},
            {header: 'Орган власти', key: 'name', width: 400},
            {header: 'Номер обращения', key: 'DOB', width: 100, outlineLevel: 1},
            {header: 'Дата обращения', key: 'DOB', width: 100, outlineLevel: 1},
            {header: 'ФИО автора', key: 'DOB', width: 300, outlineLevel: 1},
            {header: 'Статус', key: 'DOB', width: 200, outlineLevel: 1},
            {header: 'Исполнитель', key: 'DOB', width: 500, outlineLevel: 1},
        ].map(v => {
            v.width = v.width * v1widthMultiplier;
            return v;
        });

        worksheet.columns.forEach(column => {
            column.eachCell(cell => {
                cell.border = {
                    top: {style: 'thin'},
                    left: {style: 'thin'},
                    bottom: {style: 'thin'},
                    right: {style: 'thin'}
                };
            });
        });
        const row = worksheet.lastRow;
        row.font = {
            //     name: 'Calibri',
            size: 14,
            //     underline: 'single',
            bold: true,
            //     color: { argb: '0085A3' },
        };
        row.alignment = {vertical: 'middle', horizontal: 'center', wrapText: true};
        row.height = 42;
        worksheet.insertRow(0, []);
        worksheet.insertRow(0, []);
        worksheet.insertRow(0, []);
        worksheet.insertRow(0, []);
        worksheet.mergeCells('A1:G1');
        const titleRow = worksheet.getCell('A1');
        titleRow.value = 'Обращения';
        titleRow.font = {
            //     name: 'Calibri',
            size: 16,
            //     underline: 'single',
            bold: true,
            //     color: { argb: '0085A3' },
        };
        titleRow.alignment = {vertical: 'middle', horizontal: 'center'};


        let numberpp = 1;
        data.forEach( record => {
            // console.log(record);
            const rowData = [
                numberpp,
                record.rubric?.name ?? '',
                record.extNumber ?? '',
                GridFormatters.dateFormatter1(record.createdAt) ?? '',
                record.author?.fullName || record.authorName || '',
                statuseText(String(record.siteStatus)),
                record.appealExecutors[0]?.department ?? record.appealExecutors[0]?.organization ?? ''
            ];

            const dataRow = worksheet.addRow(rowData);
            dataRow.font = {
                size: 12,
            };
            dataRow.alignment = {vertical: 'middle', horizontal: 'left', wrapText: true};
            numberpp++;
        });

        // {
        //     "id": "606193",
        //     "createdAt": "2020-04-03T11:25:06.642Z",
        //     "author": {
        //     "id": 825190,
        //         "fullName": "Луганская Олеся Игоревна",
        //         "__typename": "User"
        // },
        //     "authorName": "Луганская Олеся Игоревна",
        //     "territory": {
        //     "id": "1831",
        //         "name": "Батайск",
        //         "shortName": "",
        //         "__typename": "Territory"
        // },
        //     "rubric": {
        //     "id": "2146",
        //         "name": "Правительство Ростовской области",
        //         "__typename": "Rubric"
        // },
        //     "siteStatus": 5,
        //     "title": "",
        //     "platform": "Web",
        //     "approved": "UNDEFINED",
        //     "deleted": false,
        //     "extNumber": "5.5-7353-20",
        //     "extDate": "2020-04-03T09:00:00Z",
        //     "hasUnapprovedMessages": false,
        //     "publicGranted": false,
        //     "oldSystemId": "5e871d12621a9850e7ffd6f3",
        //     "__typename": "Appeal"
        // }
        // Blank Row
        // worksheet.addRow([]);
        // let headerRow = worksheet.addRow(header);
        // headerRow.eachCell((cell, number) => {
        //     cell.fill = {
        //         type: 'pattern',
        //         pattern: 'solid',
        //         fgColor: {argb: '4167B8'},
        //         bgColor: {argb: ''},
        //     };
        //     cell.font = {
        //         bold: true,
        //         color: {argb: 'FFFFFF'},
        //         size: 12,
        //     };
        // });


        // worksheet.getColumn(3).width = 20;
        // worksheet.addRow([]);

        // //Footer Row
        // let footerRow = worksheet.addRow([
        //     'Employee Sales Report Generated from example.com at ' + date,
        // ]);
        // footerRow.getCell(1).fill = {
        //     type: 'pattern',
        //     pattern: 'solid',
        //     fgColor: { argb: 'FFB050' },
        // };

        //Merge Cells
        // worksheet.mergeCells(`A${footerRow.number}:F${footerRow.number}`);

        console.log(workbook);
        // Generate & Save Excel File
        workbook.xlsx.writeBuffer().then((blobdata) => {
            const blob = new Blob([blobdata], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
            fs.saveAs(blob, title + '.xlsx');
        });
        // let worksheet = workbook.addWorksheet('Sales Data');
    }
}
