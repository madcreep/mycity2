export class Message {
    public text: string;
    public header: string;
    public type: MessageType;
    public class: string;
    public obj: any;

    constructor(_header: string, _text: string, _type: MessageType) {
        this.header = _header;
        this.text = _text;
        this.type = _type;
    }
}

export enum MessageType {
    info,
    success,
    error
}
