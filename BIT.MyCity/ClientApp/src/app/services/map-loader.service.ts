import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class MapLoaderService {

    mapObj: BehaviorSubject<any> = new BehaviorSubject<any>(null);


    geolocation: any;

    private _map: any;

    getMap(element: string, coords: number[], zoom: number = 9): BehaviorSubject<any> {

        if (this._map === undefined) {
            const scriptYmaps = document.createElement('script');
            scriptYmaps.src = 'https://api-maps.yandex.ru/2.1/?load=Map,Placemark,geocode,geolocation,geoObject.addon.balloon&apikey=aa9828b0-50ca-49c6-9ee5-c2ea6331653a&lang=ru_RU';
            // scriptYmaps.src = 'https://api-maps.yandex.ru/2.1/?apikey=aa9828b0-50ca-49c6-9ee5-c2ea6331653a&lang=ru_RU';
            document.body.appendChild(scriptYmaps);

            const createMap = () => {
                const map = new ymaps.Map(
                    element,
                    {
                        center: coords,
                        zoom,
                        controls: [],
                    }, {
                        searchControlProvider: 'yandex#search',
                        suppressMapOpenBlock: true,
                    }
                );
                this._map = map;
                this.geolocation = ymaps.geolocation,

                this.mapObj.next(this._map);
            }

            scriptYmaps.onload = () => {
                ymaps.ready(createMap);
            }
        } else {
            const map = new ymaps.Map(
                element,
                {
                    center: coords,
                    zoom: zoom
                }, {
                searchControlProvider: 'yandex#search'
            }
            );
            this._map = map;
            this.mapObj.next(this._map);
        //     this.map$.next(this.map);
        }


        return this.mapObj;
    }

    showGeolocation() {
        return this.geolocation.get({
            provider: 'yandex',
            mapStateAutoApply: true
        }).then( (result) => {
            // Красным цветом пометим положение, вычисленное через ip.
            result.geoObjects.options.set('preset', 'islands#redCircleIcon');
            result.geoObjects.get(0).properties.set({
                balloonContentBody: 'Мое местоположение'
            });
            this._map.geoObjects.add(result.geoObjects);
        });
    }
}
