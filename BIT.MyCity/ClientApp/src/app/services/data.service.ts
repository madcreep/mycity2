import { Inject, Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GraphQLClient } from '../data-services/graphql/graphql.client';
import {
    IUser,
    IOrderByItemType,
    IRange,
    ILikeRequestEnum,
    IResetPasswordInputType,
    INewsStateEnum,
    ISubsystem,
    IApprovedEnum,
    IReportSettingsInput,
    IExecuteReportResult
} from 'src/generated/types.graphql-gen';
import { ICacheUpdateRecord, GqlApiQueries } from '../data-services/graphql/sdk-map.interfaces';

export interface IListRange {
    offset: number;
    length: number;
}

@Injectable({
    providedIn: 'root'
})
export class DataService {
    public onQuery: EventEmitter<any> = new EventEmitter<any>();
    constructor(
        private http: HttpClient,
        @Inject('BASE_URL') private baseUrl: string,
        private gqlSdk: GraphQLClient,
    ) { }

    clearCashe() {
        this.gqlSdk.clearCashe();
    }

    public registerApi(api: GqlApiQueries) {
        return this.gqlSdk.addApiDescriptor(api);
    }

    getApiContext(): any {
        return this.gqlSdk;
    }

    public login(login: string, password: string): Promise<any> {
        return this.gqlSdk.login({ login: login, password: password }).toPromise();
    }

    public updateToken() {
        return this.gqlSdk.updateToken({}, {
             fetchPolicy: 'no-cache',
        }).toPromise();
    }

    getCurrentUser(): Promise<IUser> {

        return this.gqlSdk.getCurrentUser().toPromise().then(data => {
            return Promise.resolve(<IUser>data.data.currentUser);
        });
    }

    deleteRecord(entityName: string, id: string | number): Promise<any> {
        this.onQuery.emit(null);
        return this.gqlSdk.deleteRecordByID(entityName, id);
    }

    undeleteRecord(entityName: string, id: string | number): Promise<any> {
        this.onQuery.emit(null);
        return this.gqlSdk.undeleteRecordByID(entityName, id);
    }

    createRecord(entityName: string, data: any, cacheUpdate: () => ICacheUpdateRecord[] = null): Promise<any> {
        this.onQuery.emit(null);
        return this.gqlSdk.createRecord(entityName, data, cacheUpdate);
    }

    updateRecord(entityName: string, id: string | number, data: any): Promise<any> {
        this.onQuery.emit(null);
        return this.gqlSdk.updateRecord(entityName, id, data);
    }

    getRecord(entityName: string, id: any, noCached: boolean = true): Promise<any> {
        this.onQuery.emit(null);
        return this.gqlSdk.getRecord(entityName, id, noCached);
    }

    getPgList(entityName: string,
        orderBy: IOrderByItemType[],
        filters: any,
        range: IListRange | null,
        showDeleted: boolean,
        reread: boolean = true): Promise<any> {

        const gqlrange: IRange = range || orderBy ? { } : null;
        if (range) {
            gqlrange.skip = range.offset;
            gqlrange.take = range.length;
        }
        if (orderBy && orderBy.length) {
            gqlrange.orderBy = orderBy;
        }

        this.onQuery.emit(null);
        return this.gqlSdk.getPgList(entityName, filters, gqlrange, reread, showDeleted);
    }


    getMessageList(id: string | number, type: string): Promise<any> {
        this.onQuery.emit(null);
        return this.gqlSdk.messageList({ id, type }, {
            fetchPolicy: 'no-cache',
        }).toPromise();
    }

    approveMessage(id: string, cacheUpdate: () => ICacheUpdateRecord[] = null) {
        return this.gqlSdk.approveMessage({id: id}).toPromise();
    }

    rejectMessage(id: string, text: string, cacheUpdate: () => ICacheUpdateRecord[] = null) {
        return this.gqlSdk.rejectMessageId(id, text, cacheUpdate);
    }

    likeMessage(id: string, operation: ILikeRequestEnum): Promise<any> {
        return this.gqlSdk.likeMessage({
            id: id,
            operation: operation
        }).toPromise();
    }


    voteStart(id: string) {
        return this.gqlSdk.voteStart({id: id}).toPromise();
    }
    voteStop(id: string) {
        return this.gqlSdk.voteStop({id: id}).toPromise();
    }


    watch(entityName: string, variable: { data: any; }) {
        if (entityName === 'settings') {
            return this.gqlSdk.newSettings({ keys: variable.data }, {
                // fetchPolicy: 'no-cache',
            });
        }
        return null;
    }

    resetPassword(variables: IResetPasswordInputType, options?: any): Promise<any> {
        return this.gqlSdk.resetPassword({ resetPassword: variables }, options).toPromise().then(
            (d) => d.data.resetPassword
        );
    }

    getSubSystemList(vars: {}) {
        return this.gqlSdk.getSubSystemList().toPromise().then(
            (result) => result.data.subsystem.items as ISubsystem[]
        );
    }

    setNewsPublishState(id: string, state: INewsStateEnum): Promise<any> {
        return this.gqlSdk.setNewsPublishState( { id, state }).toPromise();
    }

    readAllEnums() {
        return this.gqlSdk.readAllEnums().toPromise();
    }

    getVersionsInfo() {
        return this.gqlSdk.versionsInfo().toPromise().then(
            (data) => data.data
        );
    }

    attachSetIsPublic(id: string, isPublic: boolean) {
        return this.gqlSdk.attachSetIsPublic({ id: id, isPublic: isPublic }).toPromise().then(
            (data) => data.data
        );
    }


    approveAppeal(appealId, value: IApprovedEnum, reasonText: string = null) {
        const v: any = { approve: {}, id: appealId };
        v.approve.approvedEnum = value;
        v.approve.public_ = v.approve.approvedEnum === IApprovedEnum.Accepted;
        v.approve.rejectionReason = reasonText;
        // delete v.appeal.approved;
        // delete v.appeal.rejectionReason;

        return this.gqlSdk.approveAppeal(v).toPromise();
        // if (value === IApprovedEnum.Rejected) {

        // }
        // if (v.appeal.approved || v.appeal.rejectionReason) {
        //     v.approve = {};
        //     v.approve.approvedEnum = v.appeal.approved || (v.appeal.rejectionReason ? IApprovedEnum.Rejected : IApprovedEnum.Undefined);
        //     v.approve.public_ = v.approve.approvedEnum === IApprovedEnum.Accepted;
        //     v.approve.rejectionReason = v.appeal.rejectionReason;

        //     return super.approveAppeal(v);

        //     this.gqlSdk.approveAppeal({})
        // }
    }
    registerAppeal(appealId) {
        const v: any = { approve: {}, id: appealId };
        return this.gqlSdk.registerAppeal(v).toPromise();
    }

    public availableModeratorsFor(subsystems: string[]) {
        const v: any = { subsystemUid: subsystems };
        return this.gqlSdk.availableModeratorsFor(v).toPromise().then(
            (data) => data.data
        );
    }

    public getJournal(entityName: string, key: string): Promise<any> {
        this.onQuery.emit(null);
        return this.gqlSdk.getJournal({ entityName, key }, {
            fetchPolicy: 'no-cache',
        }).toPromise().then(
            (data) => data.data.journal
        );
    }

    public executeReport(settings: IReportSettingsInput): Promise<IExecuteReportResult> {
        return this.gqlSdk.executeReport({ settings: settings }).toPromise().then(
            (data) => {
                if (!!data.errors) {
                    return { errors: data.errors.map( e => e.message) } as IExecuteReportResult;
                }
                return (data.data.executeReport as IExecuteReportResult);
            }
        ).catch( error => {
            return { errors: [error.message] } as IExecuteReportResult;
        });
    }
}
