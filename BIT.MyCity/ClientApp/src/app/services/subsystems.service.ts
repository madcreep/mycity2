import { Injectable } from '@angular/core';
import { Subscriptionable } from '../common/subscriptionable';
import { DataService } from './data.service';
import { ErrorsService } from './errors.service';
import { BehaviorSubject } from 'rxjs';
import { ISubsystem } from 'src/generated/types.graphql-gen';
import {AuthService} from './auth.service';


export const SUBSYSTEM_CITIZEN_ID = 'citizens';
export const SUBSYSTEM_ORG_ID = 'organizations';
export const SUBSYSTEM_VOTE = 'vote';
export const SUBSYSTEM_NEWS_ID = 'news';

@Injectable({
    providedIn: 'root'
})
export class SubsystemsService extends Subscriptionable {

    public onAfterInit: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    subsystems: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

    private list: ISubsystem[];

    constructor(
        private dataSrv: DataService,
        private errSrv: ErrorsService,
    ) {
        super();
        this.dataSrv.getSubSystemList( { } ).then(
            (result) => {
                this.list = result;
                this.subsystems.next(this.list);
                this.onAfterInit.next(true);
            }
        ).catch(error => {
            return this.errSrv.passGlobalErrors(error).then(() => {
            });
        });
    }

    listOfSubSystems(): string[] {
        return this.list.map(s => s.uID);
    }

    titleForSubsistem(uId) {
        const ss = this.list.find(s => s.uID === uId);
        return ss ? ss.name : null;
    }
}
