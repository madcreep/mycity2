import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from './message.service';
import { Message, MessageType } from './message';

@Injectable({
  providedIn: 'root'
})
export class ErrorsService {

constructor(
    private _router: Router,
    private _msgSrv: MessageService,
) { }


    /**
     * Глобальный обработчик ошибок. Возвращает true если обработал.
     * @param error
     */
    public passGlobalErrors(error): Promise<boolean> {
        if (error && error.graphQLErrors && error.graphQLErrors.length) {
            let text: string = error.graphQLErrors[0].message;
            if (text.length > 200) {
                text = text.substr(0, 200);
            }
            const msg = new Message('Ошибка', text,  MessageType.error);
            msg.obj = error;
            this._msgSrv.addMessage(msg);
            const gqlError = error.graphQLErrors[0];
            if (gqlError.extensions && gqlError.extensions.code === 'authorization' ) {
                this._router.navigateByUrl('login');
            }
        } else if (error && error.message) {
            const code = !error.code ? '' : '0x' + error.code.toString(16) + ' ';
            const msg = new Message(code + error.message, error.description,  MessageType.error);
            msg.obj = error;
            this._msgSrv.addMessage(msg);
        }


        return Promise.reject(error);
    }

}
