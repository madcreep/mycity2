export const SETTINGS_RUBRIC_TITLE = 'system.web.rubric.title';
export const SETTINGS_TERRITORY_TITLE = 'system.web.territory.title';
export const SETTINGS_TERRITORY_TITLEONE = 'system.web.territory.title_one';
export const SETTINGS_RUBRIC_TITLEONE = 'system.web.rubric.title_one';
export const SETTINGS_RUBRIC_ADDEN = 'system.web.rubric.add_enabled';
export const SETTINGS_TERRITORY_ADDEN = 'system.web.territory.add_enabled';
export const SETTINGS_TOPIC_ADDEN = 'system.web.topic.add_enabled';
export const SETTINGS_REGION_ADDEN = 'system.web.region.add_enabled';
export const SETTINGS_MAIN_TITLE = 'system.web.general.maintitle';
export const SETTINGS_GENERAL_ROOT = 'system.';
export const SETTINGS_REGIONS_TITLE = 'system.web.region.title';
export const SETTINGS_REGIONS_TITLEONE = 'system.web.region.title_one';

export const SETTINGS_SS_CIT_ENABLED = 'subsystem.citizens.enable';
export const SETTINGS_SS_CIT_ADDEN = 'subsystem.citizens.web.appeal_add';
export const SETTINGS_SS_CIT_EDITEN = 'subsystem.citizens.web.appeal_edit';
export const SETTINGS_SS_CIT_DELEN = 'subsystem.citizens.web.appeal_del';
export const SETTINGS_SS_CIT_ANSWER = 'subsystem.citizens.web.appeal_web_answer';
export const SETTINGS_SS_CIT_STED = 'subsystem.citizens.web.status_edit'
export const SETTINGS_SS_CIT_EXECUTOR = 'subsystem.citizens.executor_feature';
export const SETTINGS_SS_CIT_REGWAIT = 'subsystem.citizens.autoregister_wait';

export const SETTINGS_SS_ORG_ENABLED = 'subsystem.organizations.enable';
export const SETTINGS_SS_ORG_ADDEN = 'subsystem.organizations.web.appeal_add';
export const SETTINGS_SS_ORG_EDITDEN = 'subsystem.organizations.web.appeal_edit';
export const SETTINGS_SS_ORG_DELEN = 'subsystem.organizations.web.appeal_del';
export const SETTINGS_SS_ORG_ANSWER = 'subsystem.organizations.web.appeal_web_answer';



export const SETTINGS_SS_VOTE_EDITALL = 'subsystem.vote.always_editable';
export const SETTINGS_SS_VOTE_ENABLED = 'subsystem.vote.enable';

export const SETTINGS_SS_NEWS_ENABLED = 'subsystem.news.enable';
