import { EventEmitter, Injectable } from '@angular/core';
import { Message, MessageType } from './message';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';



class ToastExt {
    subscriptions: Subscription[] = [];
    obj: any;
    public unsubscribe() {
        this.subscriptions.forEach(s => s.unsubscribe());
        this.subscriptions = [];
    }
}

@Injectable({
    providedIn: 'root'
})
export class MessageService {

    //   public static  messages: Message[] = [];
    //   public static removeMessageEmitter: EventEmitter<Message> = new EventEmitter<Message>();
    //   public static addMessageEmitter: EventEmitter<Message> = new EventEmitter<Message>();
    //   public static fileEventEmitter: EventEmitter<Message> = new EventEmitter<Message>();
    //   public static fileRemovedEmitter: EventEmitter<Message> = new EventEmitter<Message>();

    //   private static _sendingFile: boolean;

    constructor(
        private _toastr: ToastrService,

    ) {
        // this._toastr.;
    }




    public addMessage(message: Message) {
        let toast;
        switch (message.type) {
            case MessageType.error:
                toast = this._toastr.error(message.text, message.header);

                // toast.onTap.subscribe((obj) => {
                //     console.log('MessageService -> addMessage -> toast', toast);

                // });
                // this._toastr.error(message.text, message.header).onTap.subscribe;
                break;
            case MessageType.info:
                toast = this._toastr.info(message.text, message.header);
                break;
            default:
                toast = this._toastr.success(message.text, message.header);
                break;

        }

        const toastExt: ToastExt = new ToastExt();
        toastExt.obj = message.obj;
        toastExt.subscriptions.push(toast.onTap.subscribe((t) => {
            const ext = toastExt; // t['toastExtension'];
            if (ext) {
                // console.log(JSON.stringify(toastExt.obj));
            }
        }));
        toastExt.subscriptions.push(toast.onHidden.subscribe((t) => {
            toastExt.unsubscribe();
        }));


        toast.toastExtension = toastExt;

    }

    //   public static get sendingFile(): boolean {
    //     return this._sendingFile;
    //   }

    //   public static set sendingFile(value: boolean) {
    //     this._sendingFile = value;
    //   }

    //   public static addMessage(message: Message) {
    //     switch (message.type) {
    //       case MessageType.success:
    //         message.class = 'bg-success text-light';
    //         break;
    //       case MessageType.info:
    //         message.class = 'bg-info text-light';
    //         break;
    //       case MessageType.error:
    //         message.class = 'bg-danger text-light';
    //         break;
    //     }
    //     this.messages.push(message);
    //     this.addMessageEmitter.emit(message);
    //     setTimeout(() => {
    //       const index  = this.messages.indexOf(message);
    //       if (index >= 0) {
    //         this.removeMessageEmitter.emit(message);
    //         this.messages.splice(index, 1);
    //       }
    //     }, 5000);
    //   }

    //   public static getMessages(): Message[] {
    //     return this.messages;
    //   }

    //   public static removeMessage(message: Message) {
    //     const index = this.messages.indexOf(message);
    //     if (index >= 0) {
    //       this.messages.splice(index, 1);
    //       this.removeMessageEmitter.emit(message);
    //     }
    //   }

    //   public static saveToPc(data, filename, contentType) {

    //     if (!data) {
    //       console.error('No data');
    //       return;
    //     }

    //     if (!filename) {
    //       filename = 'download.csv';
    //     }

    //     if (typeof data === 'object' && contentType === 'text/csv') {
    //       data = JSON.stringify(data, undefined, 2);
    //     }
    //     if (contentType === 'text/csv') {
    //       data = '\ufeff' + data;
    //     }
    //     let blob = new Blob([data], {type: contentType});

    //     // FOR IE:

    //     if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    //       window.navigator.msSaveOrOpenBlob(blob, filename);
    //     } else {
    //       const e = document.createEvent('MouseEvents'),
    //         a = document.createElement('a');

    //       a.download = filename;
    //       a.href = window.URL.createObjectURL(blob);
    //       a.dataset.downloadurl = [contentType, a.download, a.href].join(':');
    //       e.initEvent('click', true, false);
    //       a.dispatchEvent(e);
    //     }
    //   }

}
