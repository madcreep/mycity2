import { Injectable } from '@angular/core';
import { IFieldDescriptor, E_FIELD_TYPE, IEditPageDescriptor } from '../../model/datatypes/descriptors.interfaces';
import { InputBase } from '../../modules/dynamic-inputs/types/input-base';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppUtils } from '../../helpers/app.static';
import { Subscriptionable } from '../../common/subscriptionable';
import { DescriptorsService } from '../descriptors.service';
import { NOT_EMPTY_MULTYSTRING, NOT_EMPTY_STRING } from '../../consts/common.consts';
import { AuthService } from '../auth.service';
import {IReportSingleSettings, ISettingNodeType, ISettings} from 'src/generated/types.graphql-gen';
import {ReportSettingsConverter} from '../../helpers/converters/report-setiings.converter';
import {InputBuilder} from './input-builder';

@Injectable({
    providedIn: 'root'
})

export class InputsService extends Subscriptionable {
    constructor(
        private _descrSrv: DescriptorsService,
        private _authSrv: AuthService,
    ) {
        super();
    }

    public getInputs(values: any, isNewRecord: boolean, entKey: any = {}, init = true): Promise<InputBase<any>[]> {
        const inputs = [];

        for (const entity in values) {
            if (values.hasOwnProperty(entity)) {
                const dent = !entKey ? entity : entKey[entity] || entity;
                const descr = this._descrSrv.descriptorForEntity(dent);
                const fields: IFieldDescriptor[] = isNewRecord ? descr.fieldsInsert() : descr.fieldsEdit();

                for (let i = 0; i < fields.length; i++) {
                    const indescr = fields[i];
                    let value = null;
                    let extSelectOptions = null;

                    if (indescr.onGetValue !== undefined) {
                        value = indescr.onGetValue(values);
                    } else if (values[entity]) {
                        const eopts = indescr.editOptions;
                        if (eopts && eopts.arraySource && indescr.type === E_FIELD_TYPE.select) {
                            const v = values[entity][eopts.arraySource.arrayList];
                            if (v) {
                                value = v[eopts.arraySource.arrayKey];
                                if (v["deleted"] === true && eopts.arraySource.arrayTitle) {

                                    const title = (typeof eopts.arraySource.arrayTitle) === 'string' ? v[String(eopts.arraySource.arrayTitle)] :
                                        (<Function>eopts.arraySource.arrayTitle)(v);

                                    extSelectOptions = [{ value, title: "(удалено) " + title, disabled: true }]
                                }
                            } else {
                                value = AppUtils.getValueByPath(values[entity], indescr.key);
                            }
                        } else {
                            value = AppUtils.getValueByPath(values[entity], indescr.key);
                        }
                    }
                    let allowed = true;
                    if (indescr.needPermissions) {
                        allowed = this._authSrv.isClaimHave(indescr.needPermissions);
                    }
                    if (allowed) {
                        const input = InputBuilder.inputForDescr(indescr, entity, value, extSelectOptions);
                        if (input) {
                            inputs.push(input);
                        }
                    }
                }
            }
        }

        if (init) {
            return this.initInputs(inputs);
        } else {
            return Promise.resolve(inputs);
        }
    }

    public initInputs(inputs: InputBase<any>[]): Promise<any> {
        const inits1 = inputs.map(i => i.descriptor.initOptions ? i.descriptor.initOptions(i) : undefined).filter(v => !!v);
        const inits = inputs.map(i => i.init());
        return Promise.all([...inits, ...inits1]).then(
            () => {
                return inputs;
            }
        );
    }

    public toFormGroup(inputs: InputBase<any>[]): any {
        const group: any = {};

        Object.keys(inputs).forEach(input => {
            this._addInput(group, inputs[input]);
        });
        return new FormGroup(group);
    }

    public convertFromSettings(reportSettings: IReportSingleSettings[]): InputBase<any>[] {
        return reportSettings.map( setting => ReportSettingsConverter.toInputBase(setting));
    }

    public updateInputAndPageList(entityName: string, inputs: InputBase<any>[], pagesList: IEditPageDescriptor[]) {
        for (let page = 0; page < pagesList.length; page++) {
            const element = pagesList[page];
            _updateInputAndPageListFields(element.fields, page);
        }

        function _updateInputAndPageListFields(fields, page) {
            for (let i = 0; i < fields.length; i++) {
                const f = fields[i];
                if (f instanceof Array) {
                    _updateInputAndPageListFields(f, page);
                } else {
                    // const key = entityName + '.' + f;
                    // const key = (f.indexOf('/') !== 0 ? (entityName + '.' + f) : f);
                    const key = f;
                    const input = inputs.find(inp => inp.key === (key) || (('/' + inp.key) === (key)));
                    if (input) {
                        input.pageIndex = page;
                        // if ((<string>fields[i]).indexOf('/') !== 0 ) {
                        //     fields[i] = key;
                        // }
                    }
                }
            }
        }
    }

    public settingToInputDescriptor(field: ISettings): IFieldDescriptor {
        if (field.settingNodeType === ISettingNodeType.Value) {
            if (field.values) {
                const res = {
                    key: field.key,
                    title: field.description,
                    type: E_FIELD_TYPE.select,
                    options: field.values.map(v => ({ value: v.value, title: v.displayText }))
                } as IFieldDescriptor;

                return res;
            }
            let type;
            switch (field.valueType) {
                case 'System.UInt32':
                    type = E_FIELD_TYPE.number;
                    break;
                case 'System.Boolean':
                    type = E_FIELD_TYPE.boolean;
                    break;
                case 'System.String':
                default:
                    type = E_FIELD_TYPE.string;
                    break;
            }

            const res = {
                key: field.key,
                title: field.description,
                type: type,
            };

            return res;

        }
        return undefined;
    }

    private _addInput(group: any, input: InputBase<any>) {
        const value = input.value !== undefined ? input.value : null;
        const validators = input.validators || [];

        if (input.disabled) {
            group[input.key] = new FormControl({ value: value, disabled: true }, validators);
        } else {
            if (input.controlType === E_FIELD_TYPE.text) {
                validators.push(Validators.pattern(NOT_EMPTY_MULTYSTRING));
            } else if (input.controlType === E_FIELD_TYPE.string) {
                validators.push(Validators.pattern(NOT_EMPTY_STRING));
            }
            if (input.length) {
                validators.push(Validators.maxLength(input.length));
            }

            group[input.key] = new FormControl(value, validators);
            group[input.key].id = input.key;
            group[input.key].input = input;
        }
    }
}
