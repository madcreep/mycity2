import {E_FIELD_TYPE, IFieldDescriptor} from '../../model/datatypes/descriptors.interfaces';
import {InputBase} from '../../modules/dynamic-inputs/types/input-base';
import {TextRichInput} from '../../modules/dynamic-inputs/types/richtext-input';
import {TextInput} from '../../modules/dynamic-inputs/types/text-input';
import {StringInput} from '../../modules/dynamic-inputs/types/string-input';
import {CheckboxInput} from '../../modules/dynamic-inputs/types/checkbox-input';
import {CheckBoxGroupInput} from '../../modules/dynamic-inputs/types/check-box-group-input';
import {MultiselectInput} from '../../modules/dynamic-inputs/types/multiselect-input';
import {SelectInput} from '../../modules/dynamic-inputs/types/select-input';
import {DateInput} from '../../modules/dynamic-inputs/types/date-input';
import {DateTimeInput} from '../../modules/dynamic-inputs/types/datetime-input';
import {DateRangeInput} from '../../modules/dynamic-inputs/types/date-range-input';
import {FileListInput} from '../../modules/dynamic-inputs/types/file-list-input';
import {NumberInput} from '../../modules/dynamic-inputs/types/number-input';
import {BaseOptionsInput} from '../../modules/dynamic-inputs/types/base-options-input';
import {TableInput} from '../../modules/dynamic-inputs/types/table-input';
import {MapPointInput} from '../../modules/dynamic-inputs/types/map-point-input';
import {SeparatorInput} from '../../modules/dynamic-inputs/types/separator-input';
import {ObjectInput} from '../../modules/dynamic-inputs/types/object-input';
import {RadioGroupInput} from '../../modules/dynamic-inputs/types/radio-group-input';
import {NumberRangeInput} from '../../modules/dynamic-inputs/types/number-range-input';
import {ListInput} from '../../modules/dynamic-inputs/types/list-input';
import {TreeListInput} from '../../modules/dynamic-inputs/types/tree-list-input';
import {DateRangeExtInput} from '../../modules/dynamic-inputs/types/date-range-ext-input';

export class InputBuilder {
    public static inputForDescr(indescr: IFieldDescriptor, keyPrefix, value: any, extSelectOptions: any[] = null): InputBase<any> {
        let input = null;
        switch (indescr.type) {
            // case E_FIELD_TYPE.checkbox-group:
            // break;
            case E_FIELD_TYPE.textRich:
                input = new TextRichInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.text:
                input = new TextInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.string:
                input = new StringInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.boolean:
                input = new CheckboxInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.checkBoxGroup:
                input = new CheckBoxGroupInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.radioGroup:
                input = new RadioGroupInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.multiselect:
                input = new MultiselectInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.treeList:
                input = new TreeListInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.select:
                input = new SelectInput(indescr, keyPrefix, value, extSelectOptions);
                break;
            case E_FIELD_TYPE.list:
                input = new ListInput(indescr, keyPrefix, value, extSelectOptions);
                break;
            case E_FIELD_TYPE.date:
                input = new DateInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.datetime:
                input = new DateTimeInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.dateRange:
                input = new DateRangeInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.dateRangeExt:
                input = new DateRangeExtInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.filesPond:
                input = new FileListInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.number:
                input = new NumberInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.numberRange:
                input = new NumberRangeInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.claimSelect:
                input = new BaseOptionsInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.table:
                input = new TableInput(indescr, keyPrefix, value);
                break;
            case E_FIELD_TYPE.mapPoint: {
                input = new MapPointInput(indescr, keyPrefix, value);
                break;
            }
            case E_FIELD_TYPE.separator: {
                input = new SeparatorInput(indescr, keyPrefix, value);
                break;
            }
            case E_FIELD_TYPE.moderatorsSubS: {
                input = new ObjectInput(indescr, keyPrefix, value);
                break;
            }
            default: {
                input = new StringInput(indescr, keyPrefix, value + '<неподдерживаемый тип>');
            }
        }
        return input;
    }
}
