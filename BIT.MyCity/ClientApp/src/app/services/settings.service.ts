import {Observable} from '@apollo/client/core';
import {Injectable} from '@angular/core';
import {AppUtils, InjectorInstance} from '../helpers/app.static';
import {MessageService} from './message.service';
import {MessageType, Message} from './message';
import {BehaviorSubject, Subscription} from 'rxjs';
import {SETTINGS_GENERAL_ROOT} from './setting-names.const';
import {SubsystemsService} from './subsystems.service';
import {BaseSubscriptions} from '../common/base-subscriptions';


export interface ISettingsConfig {
    value: any;
    config: any;
}

const SETTINGS = 'settings';
const SETTINGS_SUBSYSTEMS = 'subsystem';

@Injectable({
    providedIn: 'root'
})

export class SettingsService extends BaseSubscriptions {

    public onAfterInit: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    private _subscr = new Map<string, BehaviorSubject<any>>();

    private _settings: any = {};
    private _settingsConfig: any = {};

    constructor(
        private _msgSrv: MessageService,
    ) {
        super();


    }

    // -------------------------------------------
    // сервер - сервис
    // -------------------------------------------
    public updateSettings(keys: string[], noCached: boolean): Promise<ISettingsConfig> {

        const reads: Promise<any>[] = [];

        keys.forEach(path => {
            reads.push(
                this.dataSrv.getRecord(SETTINGS, path, noCached).then((data) => {
                    this._storeValue(path, data.value);
                    this._storeConfig(data.config);
                })
            );
        });

        return Promise.all(reads).then(() => {
            const value = {};
            const configs = {};

            // возвращаем только то что спросили.
            keys.forEach(k => {
                AppUtils.setValueByPath(value, k, AppUtils.getValueByPath(this._settings, k));
                for (const cfg in this._settingsConfig) {
                    if (this._settingsConfig.hasOwnProperty(cfg)) {
                        const element = this._settingsConfig[cfg];
                        if (cfg.startsWith(k)) {
                            configs[cfg] = element;
                        }
                    }
                }
            });
            return <ISettingsConfig>{
                value,
                config: configs,
            };
        });
    }

    // -------------------------------------------
    // сервис - компоненты
    // -------------------------------------------

    public subscribeToValue(key: string, subscr: (value) => any): Subscription {
        let subj = this._subscr.get(key);
        if (!subj) {
            subj = new BehaviorSubject<any>(
                this.getValue(key)
            );
            this._subscr.set(key, subj);
        }
        return subj.subscribe(subscr, null);
    }

    public getSettings(path: string, reread: boolean): Promise<any> {
        if (reread) {
            return this.updateSettings([path], reread);
        }
        return Promise.resolve(<ISettingsConfig>{value: this._settings, config: this._settingsConfig});
    }

    public getValue(path: string): string {
        return AppUtils.getValueByPath(this._settings, path);
    }

    protected subLoginChanged(): (result) => any {
        return (result) => {
            super.subLoginChanged().call(this, result);
            const _ssysms = InjectorInstance.get(SubsystemsService);
            const keysList = [SETTINGS_GENERAL_ROOT,].concat(
                _ssysms.listOfSubSystems().map(s => SETTINGS_SUBSYSTEMS + '.' + s)
            );
            this.updateSettings(keysList, true).then(
                () => {
                    this.onAfterInit.next(true);
                    this.unsubscribe();
                    this._subscribeToSettings(keysList);
                });
        };
    }

    // -------------------------------------------

    private _subscribeToSettings(keys: string[]) {
        this.subscriptions.push(
            this.dataSrv.watch(SETTINGS, {data: keys})
                .subscribe((obj) => {
                    let key = obj.data.newSettings.key;
                    if (key[key.length - 1] === '.') {
                        key = key.substr(0, key.length - 1);
                    }
                    const prev = AppUtils.getValueByPath(this._settings, key);
                    const value = obj.data.newSettings.value;
                    if (prev !== value) {
                        this._msgSrv.addMessage(new Message('Info', 'Изменены настройки \n' + key, MessageType.info));
                        this._storeValue(key, value);
                    }
                }));
    }

    private _storeValue(key: string, value: any) {
        if (key[key.length - 1] === '.') {
            key = key.substr(0, key.length - 1);
        }

        if (value instanceof Object) {
            AppUtils.setValueByPath(this._settings, key, AppUtils.getValueByPath(value, key));
        } else {
            AppUtils.setValueByPath(this._settings, key, value);
        }

        this._subscr.forEach((s, k: string) => {
            if (k.startsWith(key)) {
                const prevV = s.value;
                const newV = AppUtils.getValueByPath(this._settings, k);
                if (prevV !== newV) {
                    s.next(newV);
                }
            }
        });

    }

    private _storeConfig(config: any[]) {
        config.forEach(c => {
            this._settingsConfig[c.key] = c;
        });
    }


}
