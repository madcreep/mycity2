import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { saveAs } from 'browser-filesaver/FileSaver.js';
import { AuthService } from './auth.service';
import { EnvSettings } from '../model/settings/envsettings';


export declare type FIOCallbackFn = (offset: number, length: number, data: string) => Promise<boolean>;

export interface IDwnFilee {
    filename: string;
    file: {type: string};
    serverId: string;
}


@Injectable({
    providedIn: 'root'
})


export class FileIoService {

    constructor(
        private http: HttpClient,
        private _authSrv: AuthService,
    ) { }

    parseFile(file: File, chunkSize, callback: FIOCallbackFn) {
        const fileSize = file.size;
        // const  chunkSize = 64 * 1024; // bytes
        let   offset = 0;
        const  self = this; // we need a reference to the current object
        let  chunkReaderBlock = null;
        let done = false;

        const  readEventHandler = function (evt) {
            if (done) {
                return;
            }
            if (evt.target.error == null) {
                const length = offset >= fileSize ? fileSize - offset : chunkSize;

                Promise.resolve(callback(offset, length, evt.target.result).then( more => {
                    if (!more) {
                        console.log('Read canceled');
                        done = true;
                        return;
                    }
                    offset += length;

                    if (offset >= fileSize) {
                        console.log('Done reading file');
                        done = true;
                        return;
                    }
                    // of to the next chunk
                    chunkReaderBlock(offset, length, file);
                }));
            } else {
                console.log('Read error: ' + evt.target.error);
                return;
            }


        };

        chunkReaderBlock = function (_offset, length, _file) {
            const r = new FileReader();
            const blob = _file.slice(_offset, length + _offset);
            r.onload = readEventHandler;
            r.readAsDataURL(blob);
        };

        // now let's start the read with the first block
        chunkReaderBlock(offset, chunkSize, file);
    }



    downloadFileWithAuth(file: IDwnFilee) {
        const ATTACHES_GETURL = EnvSettings.ATTACHES_ROOT;
        const s = this.http.get(ATTACHES_GETURL + '/' + file.serverId,
            {responseType: 'arraybuffer', headers: new HttpHeaders().set('Authorization', 'Bearer ' + this._authSrv.getToken())}).subscribe( (data) => {
                const blob = new Blob([data], {type: file.file.type});
                saveAs(blob, file.filename);
            });
        return s;
    }

}
