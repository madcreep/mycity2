import { SUBSYSTEM_ORG_ID, SUBSYSTEM_CITIZEN_ID, SUBSYSTEM_NEWS_ID } from './subsystems.service';


export interface IClaim {
    type: string;
    value: string;
}

export class ClaimRec implements IClaim {
    type: string;
    value: string;
    constructor (type, value, sub?: string) {
        this.type = type;
        this.value = sub ? sub + '.' + value : value;
    }
}

export const CLAIM_MU_USERS = new ClaimRec('mu', 'users');
export const CLAIM_MU_ROLES = new ClaimRec('mu', 'roles');
export const CLAIM_MU_REGIONS = new ClaimRec('mu', 'regions');
export const CLAIM_MU_TOPICS = new ClaimRec('mu', 'topics');
export const CLAIM_MU_RUBRICS = new ClaimRec('mu', 'rubrics');
export const CLAIM_MU_TERRITORIES = new ClaimRec('mu', 'territories');
export const CLAIM_MU_REPORTS = new ClaimRec('mu', 'reports');

export const CLAIM_SA_ADMIN = <IClaim>{type: 'sa', };
export const CLAIM_MU_SETTINGS = <IClaim>{type: 'mu', value: 'settings' };

export const CLAIM_MD_ORGANIZATIONS = new ClaimRec('md', 'enable', SUBSYSTEM_ORG_ID); // Разрешить модерирование
export const CLAIM_MD_ORG_SUPER = new ClaimRec('md', 'super', SUBSYSTEM_ORG_ID); // Супер права модерирования
export const CLAIM_MD_ORG_TOPICS = new ClaimRec('md', 'all_topics', SUBSYSTEM_ORG_ID); // Все "Темы обращений"
export const CLAIM_MD_ORG_REGIONS = new ClaimRec('md', 'all_regions', SUBSYSTEM_ORG_ID); // Все "Регионы"
export const CLAIM_SS_ORGANIZATIONS = new ClaimRec('ss', 'available', SUBSYSTEM_ORG_ID); // Доступ разрешен
export const CLAIM_SS_ORG_FBADMIN = new ClaimRec('ss', 'fbadm', SUBSYSTEM_ORG_ID); // Сообщения обратной связи
export const CLAIM_SS_ORG_APPEAL = new ClaimRec('ss', 'create_appeal', SUBSYSTEM_ORG_ID); // Создание обращений
export const CLAIM_SS_ORG_MESSAGE = new ClaimRec('ss', 'create_messsage', SUBSYSTEM_ORG_ID); // Создание сообщений "Обращения организаций"

export const CLAIM_MD_CITIZEN = new ClaimRec('md', 'enable', SUBSYSTEM_CITIZEN_ID); // Разрешить модерирование
export const CLAIM_MD_CIT_SUPER = new ClaimRec('md', 'super', SUBSYSTEM_CITIZEN_ID); // Супер права модерирования
export const CLAIM_MD_CIT_TOPICS = new ClaimRec('md', 'all_topics', SUBSYSTEM_CITIZEN_ID); // Все "Темы обращений"
export const CLAIM_MD_CIT_REGIONS = new ClaimRec('md', 'all_regions', SUBSYSTEM_CITIZEN_ID); // Все "Регионы"
export const CLAIM_SS_CITIZEN = new ClaimRec('ss', 'available', SUBSYSTEM_CITIZEN_ID); // Доступ разрешен
export const CLAIM_SS_CIT_FBADMIN = new ClaimRec('ss', 'fbadm', SUBSYSTEM_CITIZEN_ID); // Сообщения обратной связи
export const CLAIM_SS_CIT_APPEAL = new ClaimRec('ss', 'create_appeal', SUBSYSTEM_CITIZEN_ID); // Создание обращений
export const CLAIM_SS_CIT_EXECUTOR = new ClaimRec('ss', 'executor', SUBSYSTEM_CITIZEN_ID); // Исполнитель
export const CLAIM_SS_CIT_MESSAGE = new ClaimRec('ss', 'create_messsage', SUBSYSTEM_CITIZEN_ID); // Создание сообщений "Обращения граждан"

export const CLAIM_SS_NEWS = new ClaimRec('ss', 'available', SUBSYSTEM_NEWS_ID); // Доступ разрешен
