import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { InjectorInstance } from '../helpers/app.static';
import { IEnumVerb } from 'src/generated/types.graphql-gen';


// export enum EnumGroups {
//     appealStatus = 'appealStatus',
//     feedbackStatus = 'feedbackStatus',
//     externalLink = 'externalLink',
//     email = 'email',
// }

// export const EnumGroupsTypes = [
//     { value: EnumGroups.appealStatus, title: 'Статусы обращений' },
//     { value: EnumGroups.feedbackStatus, title: 'Сообщения обратной связи' },
//     { value: EnumGroups.externalLink, title: 'Линки для админки' },
//     { value: EnumGroups.email, title: 'Шаблоны почтовой рассылки' },

// ];

@Injectable({
    providedIn: 'root'
})
export class EnumverbService {

    static appealStatus = 'appealStatus';
    static groupDescriptor = 'groupDescriptor';
    static feedbackStatus = 'feedbackStatus';
    static externalLink = 'externalLink';
    // public appealStatus: IEnumVerb[] = [];
    // public feedbackStatus: IEnumVerb[] = [];
    // public externalLinks: IEnumVerb[] = [];

    public groupdescriptors: IEnumVerb[] = [];
    public enumGroupsTypes: { value: string, title: string }[] = [];
    private _readed = false;
    private enumVerbs;






    constructor(
        private dataSrv: DataService,
    ) {
    }

    static enum(group: string, subsystem: string, forceReread = false): Promise<IEnumVerb[]> {
        return new Promise((res, _rej) => {
            const instance = InjectorInstance.get(EnumverbService);
            return res(instance.getEnums(group, subsystem, forceReread));
        });
    }

    public getEnums(group: string, subsystem: string, forceReread = false): Promise<IEnumVerb[]> {
        return this._updateEnums(forceReread).then(
            (data) => {
                return !this.enumVerbs || !this.enumVerbs[group] ? [] : (
                    subsystem ?
                        this.enumVerbs[group].items.filter((r) => r.subsystemUID === subsystem) :
                        this.enumVerbs[group].items
                );
            }
        );
    }

    public getEnumsUnsafe(group: string): IEnumVerb[] {
        return this.enumVerbs ? this.enumVerbs[group].items || [] : [];
    }

    private _updateEnums(forceReread: boolean): Promise<IEnumVerb[]> {
        let readPromise = Promise.resolve(this.enumVerbs);
        if (!this._readed || forceReread) {
            readPromise = this.dataSrv.readAllEnums().then(
                (result) => {
                    // this.appealStatus = result.data.appealStatus.items as IEnumVerb[];
                    // this.feedbackStatus = result.data.feedbackStatus.items as IEnumVerb[];
                    // this.externalLinks = result.data.externalLinks.items as IEnumVerb[];
                    this.groupdescriptors = result.data.groupDescriptor.items as IEnumVerb[];
                    this.enumGroupsTypes = this.groupdescriptors.map(v => ({ value: v.value, title: v.shortName }));

                    this.enumVerbs = result.data;
                    this._readed = true;
                    return this.enumVerbs;

                }
            );
        }

        return readPromise;
    }

}
