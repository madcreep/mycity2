
export const NOT_EMPTY_STRING = /^((?!\:\"\|).)*$/;
export const NOT_EMPTY_MULTYSTRING = /^((?!\:\"\|).|\n|\r)*$/;
export const DATE_INPUT_PATERN = /.*(0[1-9]|[12][0-9]|3[01])\.(0[1-9]|1[0-2])\.(\d{4}).*?/;
export const DATE_JSON_PATTERN = /(\d{4})-(\d{2})-(\d{2})(T(\d{2}:){2}\d{2}.*)/;
export const YEAR_PATTERN = /(^\d{4}$)/;
export const DIGIT3_PATTERN = /(^\d{1,3}$)/;
export const DIGIT4_WITH_PERIOD_PATTERN = /(^(([1-9](\d{1,3})?)|(([1-9](\d{1,3})?)-([1-9](\d{1,3})?)))$)/;
export const DIGIT4_WITH_PERIOD_LIST_SEPARATED = /(^(([1-9](\d{1,3})?)|(([1-9](\d{1,3})?)-([1-9](\d{1,3})?)))((,\ *(([1-9](\d{1,3})?)|(([1-9](\d{1,3})?)-([1-9](\d{1,3})?))))?)*$)/;
export const NUMERIC_PATTERN = /(^-?\d{1,8}$)/;
export const EMAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

