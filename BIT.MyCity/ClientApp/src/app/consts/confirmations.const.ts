import { IConfirmWindow } from '../common/confirm-window/confirm-window.component';

export const BUTTON_RESULT_YES = 1;
export const BUTTON_RESULT_OK = 1;
export const BUTTON_RESULT_NO = 2;
export const BUTTON_RESULT_CANCEL = 3;


export const CONFIRM_OP_DELETE: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Вы действительно хотите удалить выбранную запись?',
    bodyAfterList: 'Продолжить?',
    buttons: [
        {title: 'Отменить', result: BUTTON_RESULT_CANCEL, },
        {title: 'Удалить',  result: BUTTON_RESULT_YES, isDefault: true, },
    ],
};

export const CONFIRM_OP_UNDELETE: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Вы действительно хотите восстановить выбранную запись?',
    bodyAfterList: 'Продолжить?',
    buttons: [
        {title: 'Отменить', result: BUTTON_RESULT_CANCEL, },
        {title: 'Восстановить',  result: BUTTON_RESULT_YES, isDefault: true, },
    ],
};


export const CONFIRM_LEAVE_FORM: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Имеются несохраненные данные.',
    bodyAfterList: 'Продолжить?',
    buttons: [
        {title: 'Продолжить', result: BUTTON_RESULT_OK, isDefault: true, },
        {title: 'Отменить', result: BUTTON_RESULT_CANCEL, },
    ],
};
