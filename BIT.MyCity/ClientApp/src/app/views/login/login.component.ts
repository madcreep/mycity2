
import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Subscription } from 'rxjs';
import { InjectorInstance } from '../../helpers/app.static';
import { BsModalRef } from 'ngx-bootstrap/modal';

interface ISocialButton {
    id: string;
    name: string;
    iconClass: string;
    title: string;
}
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

    @Input() isModal = false;

    socialButton: ISocialButton[] = [
        {
            id: 'login-with-local',
            name: 'Local',
            iconClass: 'local-btn',
            title: 'Sign in with local',
        }, {
            id: 'login-with-facebook',
            name: 'Facebook',
            iconClass: 'facebook-btn',
            title: 'Sign in with Facebook',
        }, {
            id: 'login-with-vk',
            name: 'VK',
            iconClass: 'vk-btn',
            title: 'Sign in with VK',
        }
    ];

    invalidLogin: boolean;
    backurl: string;
    loginFormGroup: FormGroup;
    private subscription: Subscription;

    constructor(
        private formBuilder: FormBuilder,
        public router: Router,
        private http: HttpClient,
        private _authSrv: AuthService,
        private _route: ActivatedRoute,
        public activeModal: BsModalRef) {

        this.loginFormGroup = this.formBuilder.group({
            login: ['', Validators.required],
            password: ['', Validators.required]
        });

    }


    static doLoginForm(url: string) {
        // this._returnUrl = url;
        const router = InjectorInstance.get(Router);
        router.navigate(['login']);
    }

    login() {
        this._authSrv.loginUser(
            this.loginFormGroup.controls.login.value,
            this.loginFormGroup.controls.password.value).then((result) => {
                if (this.isModal) {
                    this.activeModal.hide();
                } else {
                    if (this._authSrv.returnUrl) {
                        this.router.navigate([this._authSrv.returnUrl]);
                    } else {
                        this.router.navigate(['/']);
                    }
                }

            }).catch(error => {
                this.invalidLogin = true;
            });

    }

    logOut() {
        this._authSrv.logoutUser(true);
    }

    ngOnInit() {
        this.subscription = this._route.paramMap.subscribe((params: ParamMap) => {
            this.backurl = params.get('url');
        });
        if (this.router.url.startsWith('/logout')) {
            this._authSrv.logoutUser(true)
                .then(() => {
                    if (this.backurl) {
                        this.router.navigate([this.backurl]);
                    } else {
                        this.router.navigate(['/']);
                    }
                })
                .catch(err => {
                    console.error(err);
                    if (this.backurl) {
                        this.router.navigate([this.backurl]);
                    }
                });
        }
    }

    ngOnDestroy(): void {
        if (this.subscription && !this.subscription.closed) {
            this.subscription.unsubscribe();
        }
    }
}

