import {Component, OnInit} from '@angular/core';
import {ToggleService} from '../../toggle.service';
import {AuthService} from '../../services/auth.service';
import {MenuPages, IMenuPage} from '../menu-pages/menu-pages.const';
import {SettingsService} from '../../services/settings.service';
import {Subscriptionable} from '../../common/subscriptionable';
import {SETTINGS_MAIN_TITLE} from '../../services/setting-names.const';
import {APP_BUILD_DATE} from '../../defines.gen';
import {EnumverbService} from '../../services/enumverb.service';
import {EnvSettings} from '../../model/settings/envsettings';

@Component({
    selector: 'app-side-menu',
    templateUrl: './side-menu.component.html',
    styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent extends Subscriptionable implements OnInit {
    toggled: boolean;
    sections = [];
    mainTitle = '';
    private _subscr = {};

    constructor(
        private toggleService: ToggleService,
        private settingsSrv: SettingsService,
        private enumsSrv: EnumverbService,
        public auth: AuthService) {
        super();
    }

    ngOnInit() {
        this.toggleService.currentMessage.subscribe(message => this.toggled = message);

        this.subscriptions.push(
            this.settingsSrv.subscribeToValue(SETTINGS_MAIN_TITLE,
                (value) => this.mainTitle = value
            )
        );
        const sectionOfLinks = EnvSettings.URLsSections.map((section, index) => ({
            id: 'sections/' + index,
            title: section.title,
            class: 'fa fa-info-circle fa-fw',
        }));

        this.sections = [
            ...MenuPages,
            ...sectionOfLinks
        ];

        this.enumsSrv.getEnums(EnumverbService.externalLink, '').then(
            (res) => {
                this.sections =
                    [
                        ...this.sections,
                        ...res.map(extlink => ({

                                id: 'adminlink' + extlink.id, title: extlink.value, class: 'fa fa-external-link-square fa-fw',
                                urllink: extlink.shortName
                            }
                        )),
                    ];
            }
        );

        this.sections.forEach((section: IMenuPage) => {
            if (section.subscription) {
                this.subscriptions.push(
                    section.subscription()
                );
            }
        });

    }

    isSectionEnabled(section: IMenuPage): boolean {
        if (section.enabled !== undefined) {
            return section.enabled;
        }
        return true;
    }

    getTitle(section: IMenuPage): string {
        if (section.titlePath) {
            if (this._subscr[section.titlePath]) {
                this._subscr[section.titlePath].unsubscribe();
            }
            this._subscr[section.titlePath] = this.settingsSrv.subscribeToValue(section.titlePath,
                (value) => section.title = value
            );
            // return this.settingsSrv.getValue(section.titlePath);
        }
        return section.title;
    }
}
