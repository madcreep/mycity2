import {AppUtils} from 'src/app/helpers/app.static';
import {E_ACTION_TYPE, IEditPageDescriptor, E_FIELD_TYPE} from '../../model/datatypes/descriptors.interfaces';
import {DataService} from '../../services/data.service';
import {BaseTableDescriptor} from '../../model/datatypes/descriptors.const';
import {DescriptorsService} from '../../services/descriptors.service';
import {Component, OnChanges, Input, ViewChild, OnInit, Output, EventEmitter} from '@angular/core';
import {ActivatedRoute, Params, CanDeactivate} from '@angular/router';
import {ICardDescriptor, ICardAction, ICardActionButton} from './card.interfaces';
import {BaseCardFormComponent} from './card-form/base-card-form.component';
import {InputBase} from '../../modules/dynamic-inputs/types/input-base';
import {ErrorsService} from '../../services/errors.service';
import {SettingsService} from '../../services/settings.service';
import {Message, MessageType} from '../../services/message';
import {MessageService} from '../../services/message.service';
import {Subscriptionable} from '../../common/subscriptionable';
import {ConfirmWindowService} from '../../common/confirm-window/confirm-window.service';
import {BUTTON_RESULT_YES, CONFIRM_LEAVE_FORM} from '../../consts/confirmations.const';
import {InputsService} from '../../services/input.service/inputs.service';
import {setWeek} from 'ngx-bootstrap/chronos/units/week';


export const ID_ADD_RECORD = 'add';


@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})

export class CardComponent extends Subscriptionable implements OnInit, OnChanges, CanDeactivate<any> {
    @Input() editMode = true;
    @Input() card: ICardDescriptor = {entityName: '', id: <number | string>''};
    @Output() closeAction = new EventEmitter<any>();

    @ViewChild('cardForm') cardForm: BaseCardFormComponent;

    public isUpdate = true;
    // public card: ICardDescriptor;
    public actionList: ICardAction[] = [];
    public pagesList: IEditPageDescriptor[] = [];
    public title = '';
    public inputs: InputBase<any>[] = [];
    public formGroup: any;
    public data: any;
    public isNewRecord: boolean;
    public isFormInvalid: boolean;
    public descriptor: BaseTableDescriptor;

    private subscrTitle = null;

    constructor(
        protected descrSrv: DescriptorsService,
        protected settingsSrv: SettingsService,
        protected errSrv: ErrorsService,
        protected inpSrv: InputsService,
        protected confirmSrv: ConfirmWindowService,
        private _route: ActivatedRoute,
        private _msgSrv: MessageService,
        protected dataSrv: DataService,
    ) {
        super();

    }

    ngOnInit(): void {
        this.subscriptions.push(this._route.params.subscribe((params) => {
            if (params) {
                this.isUpdate = true;
                this.card.entityName = this.card.entityName || params.entityName || this.getEntityName();
                this.card.id = this.card.id || params.id;

                this.isNewRecord = (this.card.id === ID_ADD_RECORD);

                this.descriptor = this.descrSrv.descriptorForEntity(this.card.entityName);
                this.actionList = this.descriptor.actionsCard();
                this.pagesList = this.descriptor.getAbsPathesPageList(this.isNewRecord);

                if (this.subscrTitle) {
                    this.subscrTitle.unsubscribe();
                }

                if (this.descriptor.titlePath) {
                    this.subscrTitle = this.settingsSrv.subscribeToValue(this.descriptor.titlePath, (value => {
                        this.title = value;
                    }));
                    this.subscriptions.push(this.subscrTitle);
                } else {
                    this.title = this.descriptor.title;
                }

                this.updateForm(params);
            }
        }));
    }


    ngOnChanges(): void {
        this.updateActions();
    }

    public onActionClick(button: ICardActionButton) {
        switch (button.action.id) {
            case E_ACTION_TYPE.close:
            case E_ACTION_TYPE.cancel: {
                this.closeCard();
                break;
            }
            case E_ACTION_TYPE.save: {
                this.saveCard(true);
                break;
            }
            case E_ACTION_TYPE.apply: {
                this.saveCard(false);
                break;
            }
            default:
                this.cardForm.onActionClick(button);
                break;
        }

    }

    public formValidEvent(isInvalid) {
        this.isFormInvalid = isInvalid;
    }

    public formReadyEvent(cardForm: BaseCardFormComponent) {
        cardForm.updateActions(this.actionList);
    }

    public editModeChangedEvent(newEditMode) {
        this.editMode = newEditMode;
    }

    public getAdditionalDataFor = (input: InputBase<any>) => {
        if (input.controlType === E_FIELD_TYPE.filesPond) {
            const entity = this.descriptor.entityName;
            if (this.data && this.data[entity]) {
                return this.data[entity].attachedFiles;
            }
        }
        return null;
    }

    public canDeactivate(): Promise<boolean> | boolean {
        const changed = (this.cardForm && this.cardForm.isChanged);
        if (changed && this.editMode) {
            return this.confirmSrv.confirm(CONFIRM_LEAVE_FORM).then(button => {
                return (button && button.result === BUTTON_RESULT_YES);
            });
        }
        return true;
    }

    async savePromise(changes: any, isNewRecord: boolean, isSequenceResolve: boolean) {
        if (!isSequenceResolve) {
            const promises = [];
            for (const ent in changes) {
                if (changes.hasOwnProperty(ent)) {
                    const data = changes[ent];
                    if (this.isNewRecord) { // Insert
                        promises.push(
                            this.dataSrv.createRecord(ent, data)
                        );
                    } else {
                        promises.push(
                            this.dataSrv.updateRecord(ent, this.card.id, data)
                        );
                    }
                }
            }
            return Promise.all(promises);
        } else {
            let chain = Promise.resolve<any>({});
            const results = [];
            for (const ent in changes) {
                if (changes.hasOwnProperty(ent)) {
                    const data = changes[ent];
                    if (this.isNewRecord) { // Insert
                        chain = chain.then(() => {
                            return this.dataSrv.createRecord(ent, data).then((response) => {
                                results.push(response);
                                return response;
                            });
                        });
                    } else {
                        chain = chain.then(() => {
                            return this.dataSrv.updateRecord(ent, this.card.id, data).then((response) => {
                                results.push(response);
                                return response;
                            });
                        });
                    }
                }
            }
            chain = chain.then(() => {
                return results;
            });
            return Promise.resolve(chain);
        }
    }
    protected saveCard(closeOnDone = true): Promise<any> {
        if (this.isFormInvalid) {
            return;
        }

        return this.cardForm.onBeforeSave().then(
            (continueSave) => {
                if (!continueSave) {
                    return;
                }
                return this.getEditedData().then(
                    (changes) => {
                        if (AppUtils.isObjectEmpty(changes) && closeOnDone) {
                            this.closeCard();
                            return;
                        }
                        const savePromise = this.savePromise(changes, this.isNewRecord, true);

                        return savePromise.then((results) => {
                            const savedData = this.cardForm.getSavedDataFromResponses(results);

                            const msg = new Message('Сохранение', 'Данные сохранены', MessageType.success);
                            this._msgSrv.addMessage(msg);
                            this.cardForm.onAfterSave(results);
                            if (closeOnDone) {
                                this.closeCard(savedData);
                            }
                            this.isUpdate = false;
                        });
                    }
                );
            }
        ).catch(error => {
            return this.errSrv.passGlobalErrors(error).then(() => {
                this.isUpdate = false;
            });
        });
    }

    // Спрашиваем про entity у тех компонентов, которые пришли не по роутингу :entyty/:id
    protected getEntityName(): string {
        return null;
    }

    protected updateForm(params: Params) {
        this.isUpdate = true;
        const entKey = {[this.descriptor.entityName]: this.descriptor.id};
        if (this.isNewRecord) {
            this.card.id = -1;
            this.data = {[this.descriptor.entityName]: this.descriptor.genNewRecord(null)};
            this.title = this.descriptor.genRecordTitle(this.data, this.isNewRecord);
            this.inpSrv.getInputs(this.data, this.isNewRecord, entKey).then(inputs => {
                this.inputs = inputs;
                this.inpSrv.updateInputAndPageList(this.descriptor.entityName, this.inputs, this.pagesList);
                this.formGroup = this.inpSrv.toFormGroup(this.inputs);
                this.updateActions();
                this.isUpdate = false;
            });

        } else {
            this.card.id = this.card.id || params.id;
            this.data = {};

            this.descrSrv.dataRead(this.descriptor, this.card.id, this.data).then(() => {
                this.title = this.descriptor.genRecordTitle(this.data, this.isNewRecord);
                return this.inpSrv.getInputs(this.data, this.isNewRecord, entKey).then(inputs => {
                    this.inputs = inputs;
                    this.pagesList = this.descriptor.getAbsPathesPageList(this.isNewRecord, inputs);
                    this.inpSrv.updateInputAndPageList(this.descriptor.entityName, this.inputs, this.pagesList);
                    this.formGroup = this.inpSrv.toFormGroup(this.inputs);
                    this.updateActions();
                    this.isUpdate = false;
                });
            }).catch(error => {
                return this.errSrv.passGlobalErrors(error).then(() => {
                    this.closeCard();
                    this.isUpdate = false;
                });
            });
        }
    }

    protected updateActions() {
        if (this.cardForm) {
            this.cardForm.updateActions(this.actionList);
        }
    }

    protected getEditedData(): Promise<any> {
        return this.cardForm.getDataForSave();
    }

    protected closeCard(saveData: any = null) {
        this.editMode = undefined;
        if (!this.cardForm.closeCard()) {
            // this._location.back();
            this.closeAction.emit(saveData);
        }
    }
}
