import {Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute, CanDeactivate, Router} from '@angular/router';
import {ICardDescriptor} from '../card.interfaces';
import {CardComponent} from '../card.component';

@Component({
    selector: 'app-card-inline',
    templateUrl: './card-inline.component.html',
    styleUrls: ['./card-inline.component.scss']
})
export class CardInlineComponent implements OnInit, CanDeactivate<any> {
    @Output() closeAction = new EventEmitter<any>();
    @Input() card!: ICardDescriptor;
    @ViewChild('cardView') cardView: CardComponent;
    private router: Router;
    private route: ActivatedRoute;

    constructor(
        private injector: Injector,
    ) {
        this.router = injector.get(Router);
        this.route = injector.get(ActivatedRoute);
    }

    ngOnInit(): void {
    }

    closeCard(saveData: any) {
        // this.router.navigate(['../'], {relativeTo: this.route});
        this.closeAction.emit(saveData);
    }

    public canDeactivate(): Promise<boolean> | boolean {
        return this.cardView.canDeactivate();
    }
}
