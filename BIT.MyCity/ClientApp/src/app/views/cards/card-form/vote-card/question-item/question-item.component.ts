import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { VOTE_DESCRIPTOR } from 'src/app/model/datatypes/vote.const';
import { BaseCardEditComponent } from '../../base-card-edit/base-card-edit.component';
import { AccordionCardEditComponent } from '../../accordion-card-edit/accordion-card-edit.component';
import { IVote } from 'src/generated/types.graphql-gen';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';

@Component({
    selector: 'app-question-item',
    templateUrl: '../../accordion-card-edit/accordion-card-edit.component.html',
    styleUrls: ['../../accordion-card-edit/accordion-card-edit.component.scss']
})

export class QuestionItemComponent extends AccordionCardEditComponent implements OnInit {
    // @ViewChild('editForm') editForm: BaseCardEditComponent;

    @Input() data: { vote: IVote };
    @Input() index;
    @Input() isNewRecord;

    @Output() saveDoneEvent: EventEmitter<any> = new EventEmitter<any>();

    descriptor = VOTE_DESCRIPTOR;

    getAdditionalDataFor = (input: InputBase<any>) => {
        if (input.controlType === E_FIELD_TYPE.filesPond) {
            if (input.key === 'vote.attachedFilesIds') {
                return this.data.vote.attachedFiles;
            }
        }
        return null;
    }

    ngOnInit() {
        super.ngOnInit();
        // this.data.vote.voteItems.forEach(element => {
        //     if (element.attachedFiles) {
        //         element['attachedFilesIds'] = element.attachedFiles.map(a => a.id);
        //     }
        // });
    }

    public getTitle(): string {
        return this.index + ' - ' + super.getTitle();
    }

    isOpenChange (isOpen) {
        this.isOpen = isOpen;
        if (isOpen && this.inputs) {
            const input = this.inputs.find( i => i.key === 'vote.name');
            setTimeout(() => {
                input.dib.focus();
            }, 100);
        }
    }

    protected onAfterSave(saveData) {
        this.isChanged = false;
        const data = saveData[0].data;
        this.data.vote = data.createVote || data.updateVote;
        this.isNewRecord = false;
        this.saveDoneEvent.emit(this.data);
    }
    protected getEditedData() {

        const vote = this.editedData['vote'];
        if (vote && vote['voteItems']) {
            vote['voteItems'].forEach(element => {
                if (element.__typename) {
                    delete element.__typename;
                }
                if (element.counter !== undefined) {
                    delete element.counter;
                }
                // if (element.attachedFiles) {
                // //     element.attachedFilesIds = element.attachedFiles.map( a => a.id);
                //     delete element.attachedFiles;
                // }
            });
        }
        // return Promise.resolve(data);
        // const questionChanges = this.getQuestionArrayChanges();
        // if (questionChanges && questionChanges.length) {
        //     this.data.voteIds = questionChanges;
        // }
        return Promise.resolve(this.editedData);
    }
}
