import { Component, OnInit, OnChanges, ViewChildren, QueryList } from '@angular/core';
import { BaseCardFormComponent } from '../base-card-form.component';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { E_FIELD_TYPE, E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { ICardAction, ICardActionButton } from '../../card.interfaces';
import { IStatusEnum, IVote, IVoteRoot } from 'src/generated/types.graphql-gen';
import { VOTE_ITEM_DESCRIPTOR } from 'src/app/model/datatypes/vote-item.const';
import { VOTE_DESCRIPTOR } from 'src/app/model/datatypes/vote.const';
import { QuestionItemComponent } from './question-item/question-item.component';
import { AppUtils, InjectorInstance } from 'src/app/helpers/app.static';
import { SETTINGS_SS_VOTE_EDITALL } from 'src/app/services/setting-names.const';
import { ICommentsTreeOptions } from 'src/app/views/cards/card-pages/comments-tree-view/comments-tree-view.component';
import { Router } from '@angular/router';
import { VOTEROOTS_DESCRIPTOR } from 'src/app/model/datatypes/vote-roots.const';
import { MESSAGES_TAB_LABEL } from 'src/app/model/datatypes/appeals/appeal-base.const';


enum IEnableType {
    disabled,
    questions,
    answers,
}
const IMPORTSECTION_PATH = 'voteRoot.importSection';

@Component({
    selector: 'app-vote-card',
    templateUrl: './vote-card.component.html',
    styleUrls: ['./vote-card.component.css']
})

export class VoteCardComponent extends BaseCardFormComponent implements OnInit, OnChanges {

    @ViewChildren('questionComponents') questionComponents: QueryList<QuestionItemComponent>;

    answersPageIndex = -1;
    answersOptions: ICommentsTreeOptions = {
        addAnswerEnable: true,
    };

    questionsImportEn = false;
    answersImportEn = false;
    moreActionList = [];

    questionActionList = [
        <ICardAction>{ id: E_ACTION_TYPE.delete, title: 'Удалить вопрос' },
        <ICardAction>{ id: E_ACTION_TYPE.moveDown, title: '↓' },
        <ICardAction>{ id: E_ACTION_TYPE.moveUp, title: '↑' },
    ];

    answerActionList = [];

    voteId: number;
    openItemIndex = -1;
    openQuestIndex = -1;

    questionsDataArray: IVote[] = [];
    question_descriptor = VOTE_DESCRIPTOR;

    inputImport: InputBase<any>;
    public initial: { voteRoot: IVoteRoot };



    getAdditionalDataFor = (input: InputBase<any>) => {
        if (input.controlType === E_FIELD_TYPE.filesPond) {
            if (input.key === 'voteRoot.attachedFilesIds') {
                return this.data.voteRoot.attachedFiles;
            }
        }
        return null;
    }

    ngOnInit(): void {

        this.answersPageIndex = this.pages.findIndex(p => p.title === MESSAGES_TAB_LABEL);
        // this.genButtons();

        this.editMode = true;
        this.subscribe(
            this.settSrv.subscribeToValue(SETTINGS_SS_VOTE_EDITALL,
                (always) =>
                    this.editMode = this.isNewRecord || always || this.data.voteRoot.state === IStatusEnum.Draft
            )
        );

        this.inputImport = this.inputs.find(i => i.key === IMPORTSECTION_PATH);
        this.stringsImportSet(IEnableType.disabled);
    }

    ngOnChanges(changes) {
        super.ngOnChanges(changes);

        if (this.data.voteRoot.votes) {
            this.questionsDataArray = this.data.voteRoot.votes;
        }
    }

    public onBeforeSave(): Promise<boolean> {
        const arr = this.questionComponents.toArray();
        const savearr = [];
        arr.forEach(c => {
            if (c.isChanged) {
                // savearr.push(c.saveForm());
            }
        });

        return Promise.all(savearr).then(
            () => {
                return Promise.resolve(true);
            }
        );
    }

    public questionSaveDone(i, question, data) {
        this.questionsDataArray[i] = data.vote;
        this.setValue('voteRoot.votesIds', this.votesIds());
    }


    votesIds(): any {
        return this.questionsDataArray.map(q => q.id).filter(id => !!id);
    }

    getDataForSave(): Promise<any> {
        const questsSaves = this.questionComponents.toArray().map(q => q.saveForm());
        return Promise.all(questsSaves).then(
            () => {
                return super.getDataForSave().then(
                    (data) => {
                        return data;
                    });
            }
        );
    }

    calcStatPercents(allCount: number, varCount: number) {
        if (!allCount || !varCount) {
            return 0;
        }

        return Math.round(100 * varCount / allCount);
    }

    public questionActionEvent(i, question, button: ICardActionButton) {
        const action = button.action;
        if (action.id === E_ACTION_TYPE.delete) {
            this.questionsDataArray.splice(i, 1);
            this.setValue('voteRoot.votesIds', this.votesIds());
            this.isChanged = true;
        } else if (action.id === E_ACTION_TYPE.import) {
            this.stringsImportSet(IEnableType.questions);
        } else if (action.id === E_ACTION_TYPE.cancel) {
            this.stringsImportSet(IEnableType.disabled);
        } else if (action.id === E_ACTION_TYPE.start) {
            this.importQuestions(this.getValue(IMPORTSECTION_PATH));
            this.stringsImportSet(IEnableType.disabled);
        } else if (action.id === E_ACTION_TYPE.add) {
            this.questionsDataArray.push(
                VOTE_DESCRIPTOR.genNewRecord({})
            );
            setTimeout(
                () => this.openQuestIndex = this.questionsDataArray.length - 1
                , 1);

        } else if (action.id === E_ACTION_TYPE.moveDown) {
            if (i >= 0 && i < this.questionsDataArray.length) {
                this.swapQuestions(i, i + 1);
                this.isChanged = true;
            }

        } else if (action.id === E_ACTION_TYPE.moveUp) {
            if (i > 0 && i <= this.questionsDataArray.length) {
                this.swapQuestions(i - 1, i );
                this.isChanged = true;
            }

        }
    }
    swapQuestions(index1: number, index2: number) {
        const questComp1 = this.questionComponents.toArray()[index1];
        const questComp2 = this.questionComponents.toArray()[index2];
        if (questComp1 && questComp2) {
            questComp1.isChanged = true;
            questComp2.isChanged = true;
            let i1 = AppUtils.getValueByPath(questComp1, 'editedData.vote.weight') || AppUtils.getValueByPath(questComp1, 'data.vote.weight');
            let i2 = AppUtils.getValueByPath(questComp2, 'editedData.vote.weight') || AppUtils.getValueByPath(questComp2, 'data.vote.weight');
            if (i1 === i2) {
                i2++;
            }
            AppUtils.setValueByPath(questComp1, 'editedData.vote.weight', i2);
            AppUtils.setValueByPath(questComp2, 'editedData.vote.weight', i1);

            const t = this.questionsDataArray[index1];
            this.questionsDataArray[index1] = this.questionsDataArray[index2];
            this.questionsDataArray[index2] = t;

        }

        this.isChanged = true;

        // this.questionsDataArray = this.questionsDataArray.sort((a, b) => (a.weight > b .weight) ? 1 : (a.weight === b .weight) ? 0 : -1);
        // this.questionsDataArray = this.questionsDataArray.sort ((a, b) => ( Math.sign(a.weight - b.weight)));
    }

    public onAnswerActionClick(question: IVote, index, button: ICardActionButton) {
        const action = button.action;
        if (action.id === E_ACTION_TYPE.import) {
            this.stringsImportSet(IEnableType.answers);
        } else if (action.id === E_ACTION_TYPE.cancel) {
            this.stringsImportSet(IEnableType.disabled);
        } else if (action.id === E_ACTION_TYPE.start) {
            this.importAnswers(question, index, this.getValue(IMPORTSECTION_PATH));
            this.stringsImportSet(IEnableType.disabled);
        } else if (action.id === E_ACTION_TYPE.add) {
            const newData = VOTE_ITEM_DESCRIPTOR.genNewRecord({});
            question.voteItems.push(newData);
            const qc = this.questionComponents.toArray()[index];
            qc.setValue('vote.voteItems', question.voteItems);
            this.openItemIndex = question.voteItems.length - 1;
        }

    }
    importAnswers(question: IVote, index, text: string) {
        const arr = text.split('\n').map(s => s.trim()).filter(s => s);

        for (const q of arr) {
            const newData = VOTE_ITEM_DESCRIPTOR.genNewRecord({ name: q });
            question.voteItems.push(newData);
        }
        const qc = this.questionComponents.toArray()[index];
        qc.setValue('vote.voteItems', question.voteItems);
        this.openItemIndex = -1;

    }
    importQuestions(text: string) {
        const arr = text.split('\n').map(s => s.trim()).filter(s => s);

        for (const q of arr) {
            this.questionsDataArray.push(
                VOTE_DESCRIPTOR.genNewRecord({ name: q })
            );
        }
        this.openQuestIndex = -1;

    }

    public answerItems(question: IVote, index) {
        return question.voteItems;
    }

    onItemValueChanged(question, q_index, index, item) {
        // this.isChanged = true;
        const questComp = this.questionComponents.toArray()[q_index];
        questComp.isChanged = true;
        if (!questComp.editedData) {
            questComp.editedData = { vote: {} };
        }
        if (!questComp.editedData['vote']) {
            questComp.editedData['vote'] = {};
        }
        questComp.editedData['vote'].voteItems = questComp.data['vote'].voteItems;
    }

    onItemDeleteClick(question, q_index, answ_index, item) {
        // this.isChanged = true;
        const questComp = this.questionComponents.toArray()[q_index];
        questComp.isChanged = true;
        const data = AppUtils.getValueByPath(questComp, 'editedData.vote.voteItems');
        if (!(data && data.length)) {
            AppUtils.setValueByPath(questComp, 'editedData.vote.voteItems',
                AppUtils.getValueByPath(questComp, 'data.vote.voteItems')
            );
        }
        questComp.editedData['vote'].voteItems.splice(answ_index, 1);

    }

    resultTotalCounter() {
        if (this.initial.voteRoot.votes && this.initial.voteRoot.votes.length) {
            return this.initial.voteRoot.votes[0].usersCount;
        }
    }


    answersListFor(question: IVote) {
        if (question.offerEnabled && question.offersCount) {
            return [... question.voteItems,
                {
                    counter: question.offersCount,
                    name: '... Свой вариант',
                }
            ];
        }
        return question.voteItems;
    }

    public onAfterSave(results) {
        this.isChanged = false;
        if (this.isNewRecord) {
            const id = AppUtils.getValueByPath(results[0], 'data.createVoteRoot.id');
            if (id) {
                const router = InjectorInstance.get(Router);
                router.navigate([VOTEROOTS_DESCRIPTOR.id, id]);
            }
        }
    }

    protected onValueChanged(path: string, newValue: any, prevValue: any /*, initialValue: any*/) {
        if (path === IMPORTSECTION_PATH) {
            return true;
        }
        return false;
    }

    private stringsImportSet(value: IEnableType) {
        this.questionsImportEn = value === IEnableType.questions;
        this.answersImportEn = value === IEnableType.answers;
        this.inputImport.hidden = value === IEnableType.disabled;
        this.inputImport.readonly = value === IEnableType.disabled;

        this._refreshActionLists();

    }

    private _refreshActionLists() {
        this.moreActionList = this.questionsImportEn ?
            [
                <ICardAction>{ id: E_ACTION_TYPE.start, title: 'Применить импорт' },
                <ICardAction>{ id: E_ACTION_TYPE.cancel, title: 'Отмена' },
            ] : [
                <ICardAction>{ id: E_ACTION_TYPE.add, title: 'Добавить вопрос' },
                <ICardAction>{ id: E_ACTION_TYPE.import, title: 'Импорт' },
            ];
        this.answerActionList = this.answersImportEn ?
            [
                <ICardAction>{ id: E_ACTION_TYPE.start, title: 'Применить импорт' },
                <ICardAction>{ id: E_ACTION_TYPE.cancel, title: 'Отмена' },
            ] : [
                <ICardAction>{ id: E_ACTION_TYPE.add, title: 'Добавить вариант ответа' },
                <ICardAction>{ id: E_ACTION_TYPE.import, title: 'Импорт' },
            ];

    }


}























