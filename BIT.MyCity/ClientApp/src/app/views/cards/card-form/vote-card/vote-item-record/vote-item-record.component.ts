import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { IVoteItem } from 'src/generated/types.graphql-gen';
import { VOTE_ITEM_DESCRIPTOR } from 'src/app/model/datatypes/vote-item.const';
import { InputsService } from 'src/app/services/input.service/inputs.service';
import { Editable } from 'src/app/common/editable';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { FT_ATTACHED_FILES } from 'src/app/model/datatypes/fields.const';
import {InputBuilder} from '../../../../../services/input.service/input-builder';

@Component({
    selector: 'app-vote-item-record',
    templateUrl: './vote-item-record.component.html',
    styleUrls: ['./vote-item-record.component.css']
})
export class VoteItemRecordComponent extends Editable implements OnInit {

    @Input() item: any;
    @Input() isOpen = false;
    @Input() oneAtATime = true;
    @Input() editMode = true;
    @Output() valueChanged: EventEmitter<any> = new EventEmitter<any>();
    @Output() deleteItem: EventEmitter<IVoteItem> = new EventEmitter<IVoteItem>();

    public inputs: InputBase<any>[];
    public isUpdating = true;
    attachedFiles: any;

    protected descriptor = VOTE_ITEM_DESCRIPTOR;

    constructor(
        protected inpSrv: InputsService,
    ) {
        super();
    }


    ngOnInit() {
        // if (this.item.attachedFiles) {
            // this.attachedFiles = this.item.attachedFiles;
            // this.item.attachedFilesIds = this.attachedFiles.map(a => a.id);
            // delete this.item.attachedFiles;
        // }
        return this.refresh();
    }



    refresh() {
        this.isUpdating = true;
        this.editedData = {};
        this.inputs = [];

        const fields = this.descriptor.fieldsEdit();

        for (let i = 0; i < fields.length; i++) {
            const indescr = fields[i];
            const value = indescr.onGetValue !== undefined ?  indescr.onGetValue(this.item) : this.item[indescr.key];
            if (indescr.key === FT_ATTACHED_FILES.key) {
                this.attachedFiles = this.item.attachedFiles;
                delete this.item.attachedFiles;
            }
            const input = InputBuilder.inputForDescr(indescr, '', value);
            if (input) {
                this.inputs.push(input);
            }
        }

        const inits = this.inputs.map(i => i.init());

        return Promise.all(inits).then (
            () => {
                this.form = this.inpSrv.toFormGroup(this.inputs);
                this.resubscribe();
                this.isUpdating = false;
            }
        );
    }


    public getTitle(item = this.item): string {
        return item.shortName || item.name || '...';
    }

    public customClass() {
        return 'accordion-thin';
    }

    public apply($event) {

    }

    isOpenChange (isOpen) {
        this.isOpen = isOpen;
        if (isOpen && this.inputs) {
            const i = this.inputs.find( i => i.key === 'name');
            setTimeout(() => {
                i.dib.focus();
            }, 100);
        }
    }

    getAdditionalDataFor = (input: InputBase<any>) => {
        if (input.controlType === E_FIELD_TYPE.filesPond) {
            // if (input.key === 'vote.attachedFilesIds') {
                return this.attachedFiles;
            // }
        }
        return null;
    }

    deleteClick(event) {
        this.deleteItem.emit(this.item);
        event.preventDefault();
        event.stopPropagation();
    }

    protected onValueChanged(path: any, newValue: any, prevValue: any, initialValue: any): boolean {
        this.item[path] = newValue;
        this.valueChanged.emit(this.item);
        return true;
    }
}
