import { Component, OnInit } from '@angular/core';
import { AppealCardComponent, PATH_APPEAL_STATUS } from '../appeal-card/appeal-card.component';
import { SETTINGS_SS_CIT_ADDEN, SETTINGS_SS_CIT_EDITEN, SETTINGS_SS_CIT_REGWAIT, SETTINGS_SS_CIT_STED } from 'src/app/services/setting-names.const';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { ICardAction } from '../../card.interfaces';
import {
    FT_RUBRICLINK,
    FT_TOPICLINK,
    FT_MAP_POINT,
    FT_MAP_TEXT,
    FT_TERRITORYLINK,
    FT_DESCRIPTION,
    FT_ATTACHED_FILES,
    FT_APPEAL_EXTDATE,
    FT_APPEAL_EXTNUMBER,
    FT_REGIONLINK
} from 'src/app/model/datatypes/fields.const';

@Component({
    selector: 'app-appeal-citizen-card',
    templateUrl: '../appeal-card/appeal-card.component.html',
    styleUrls: ['./appeal-citizen-card.component.css']
})
export class AppealCitizenCardComponent extends AppealCardComponent implements OnInit {

    private isRegistratonEnabled = false;


    ngOnInit() {
        if (this.isNewRecord) {
            this.subscribe(
                this.settSrv.subscribeToValue(SETTINGS_SS_CIT_ADDEN,
                    (value) => {
                        if (value === 'true') {
                            this.enableEditFields();
                        }
                    }

                )
            );
        } else {
            this.subscribe(
                this.settSrv.subscribeToValue(SETTINGS_SS_CIT_EDITEN,
                    (value) => {
                        if (value === 'true') {
                            this.enableEditFields();
                        }
                    }

                )
            );
        }

        super.ngOnInit();
        this.subscribe(
            this.settSrv.subscribeToValue(SETTINGS_SS_CIT_REGWAIT,
                (value) => {
                    this.isRegistratonEnabled = (value !== '0');
                    this.updateActions(this.actionList);
                }
            )
        );

        this.subscribe(
            this.settSrv.subscribeToValue(SETTINGS_SS_CIT_STED,
                (value) => {
                    const input_Status = this.getInput(PATH_APPEAL_STATUS);
                    input_Status.readonly = value !== 'true';
                }

            )
        );

    }
    enableEditFields() {
        this.inputs.find(f => f.key === 'appeal.' + FT_RUBRICLINK.key).readonly = false;
        this.inputs.find(f => f.key === 'appeal.' + FT_REGIONLINK.key).readonly = false;
        this.inputs.find(f => f.key === 'appeal.' + FT_TOPICLINK.key).readonly = false;
        this.inputs.find(f => f.key === 'appeal.' + FT_TERRITORYLINK.key).readonly = false;
        this.inputs.find(f => f.key === 'appeal.' + FT_DESCRIPTION.key).readonly = false;
        this.inputs.find(f => f.key === 'appeal.' + FT_ATTACHED_FILES.key).readonly = false;
        this.inputs.find(f => f.key === 'appeal.' + 'publicGranted').readonly = false;
        this.inputs.find(f => f.key === 'appeal.' + FT_APPEAL_EXTNUMBER.key).readonly = false;
        this.inputs.find(f => f.key === 'appeal.' + FT_APPEAL_EXTDATE.key).readonly = false;
        this.inputs.find(f => f.key === 'appeal.' + FT_ATTACHED_FILES.key).readonly = false;
    }

    updateActions(actionList: ICardAction[]) {
        super.updateActions(actionList);
        for (const act of actionList) {
            switch (act.id) {
                case E_ACTION_TYPE.register:
                    act.hidden = !this.isRegistratonEnabled;
                    act.disabled = !(this.isRegistratonEnabled && !this.data.appeal.extNumber && this.data.appeal.deloStatus !== '2register' && !this.isRegistratonSended);
                    break;
                default:
                    break;
            }
        }

    }

}
