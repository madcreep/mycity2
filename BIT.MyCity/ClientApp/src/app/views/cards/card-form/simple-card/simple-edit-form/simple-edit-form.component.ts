import { Component, OnInit, Input } from '@angular/core';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'app-simple-edit-form',
    templateUrl: './simple-edit-form.component.html',
    styleUrls: ['./simple-edit-form.component.css']
})
export class SimpleEditFormComponent implements OnInit {

    @Input() inputs: InputBase<any>[];
    @Input() form: FormGroup;
    @Input() hidden = false;
    @Input() readonly = false;
    @Input() fields: any[] = [];
    @Input() getAdditionalDataFn: (input: InputBase<any>) => any;
    @Input() getInputOptsFn: (input: InputBase<any>) => any;

    constructor() { }

    ngOnInit() {
    }

    public isArray(item): boolean {
        return item instanceof Array;
    }

    public  getInput(key) {
        if (key[0] === '/') {
            return this.inputs.find( i => ('/' + i.key === key) );
        }
        return this.inputs.find( i => (i.key === key));
    }


}
