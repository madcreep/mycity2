import { Component } from '@angular/core';
import { ICommentsTreeOptions } from 'src/app/views/cards/card-pages/comments-tree-view/comments-tree-view.component';
import { IConfirmWindow } from 'src/app/common/confirm-window/confirm-window.component';
import { BUTTON_RESULT_CANCEL, BUTTON_RESULT_YES } from 'src/app/consts/confirmations.const';
import { IMessageKindEnum } from 'src/generated/types.graphql-gen';
import { AppealCardComponent, PATH_APPEAL_STATUS, APPEAL_STATUS_VAL_RUBRIC_SET } from '../appeal-card/appeal-card.component';

export const APPEAL_STATUS_VAL_REGISTER = 1; // зарегестрирован
export const APPEAL_STATUS_VAL_DONE = 5; // (Дан ответ) Завершено





const CONFIRM_STATUS_CHANGE: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Вы не можете изменить статус на "Дан ответ", так как ответы отсутствуют.',
    // bodyAfterList: 'Продолжить?',
    buttons: [
        // { title: 'Продолжить', result: BUTTON_RESULT_OK, isDefault: true, },
        { title: 'Продолжить', result: BUTTON_RESULT_CANCEL, },
    ],
};

@Component({
    selector: 'app-appeal-feedback-card',
    templateUrl: '../appeal-card/appeal-card.component.html',
    styleUrls: ['./appeal-feedback-card.component.scss']
})

export class AppealFeedbackCardComponent extends AppealCardComponent {
    messages: any[] = [];

    public createMessageEvent(value) {
        const status = this.getValue(PATH_APPEAL_STATUS);
        if (status === APPEAL_STATUS_VAL_RUBRIC_SET || status === APPEAL_STATUS_VAL_REGISTER) {
            this.setValue(PATH_APPEAL_STATUS, APPEAL_STATUS_VAL_DONE);
        }
    }

    public loadMessagesEvent(messages) {
        this.messages = messages;
    }


    public onBeforeSave(): Promise<boolean> {
        if (!this.isNewRecord &&
            this.editedData && this.editedData.appeal && this.editedData.appeal.siteStatus === APPEAL_STATUS_VAL_DONE
            && (!this.messages || !this.messages.length)
        ) {
            return this.confirmSrv.confirm(CONFIRM_STATUS_CHANGE).then(button => {
                return (button && button.result === BUTTON_RESULT_YES);
            });
        }
        return Promise.resolve(true);
    }

    protected onValueChanged(path: string, newValue: any, prevValue: any /*, initialValue: any*/) {
        return super.onValueChanged(path, newValue, prevValue);
    }

    protected getAnswersOptions(): Promise<ICommentsTreeOptions> {
        return Promise.resolve ({
            addAnswerEnable: true,
            // readOnlyMode: true,
            useKindRoot: IMessageKindEnum.SystemUserMessage,
            moderationDisabled: true,
        });
    }


}
