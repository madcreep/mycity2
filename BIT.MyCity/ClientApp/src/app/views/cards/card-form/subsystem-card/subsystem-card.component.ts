import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { BaseCardFormComponent } from '../base-card-form.component';
import { ISettingsConfig } from 'src/app/services/settings.service';
import { IPagesSettings } from 'src/app/views/cards/card-pages/settings-pages/settings-pages.component';
import { SUBSYSTEM_DESCRIPTOR } from 'src/app/model/datatypes/subsystem.const';
import { AppUtils } from 'src/app/helpers/app.static';
import { SETTINGS_DESCRIPTOR } from 'src/app/model/datatypes/settings.const';
import { ISettings, ISettingType } from 'src/generated/types.graphql-gen';
import { E_FIELD_TYPE, IEditPageDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';

@Component({
    selector: 'app-subsystem-card',
    templateUrl: './subsystem-card.component.html',
    styleUrls: ['./subsystem-card.component.css']
})
export class SubsystemCardComponent extends BaseCardFormComponent implements OnInit, OnChanges {

    // @Input() subsystemKey: string;

    settingsPages: IPagesSettings[] = [];
    settingsKeys;

    _subsystemKey: string;
    isUpdating: boolean;
    inputscfg: any;
    pagesFields: IEditPageDescriptor[];
    pages1: any[];

    ngOnInit() {


    }

    ngOnChanges(changes: SimpleChanges): void {
        super.ngOnChanges (changes);
        this._subsystemKey = this.data[SUBSYSTEM_DESCRIPTOR.entityName]['uID'];
        this.settingsPages = [
            {title: 'Настройки', keys: [SUBSYSTEM_DESCRIPTOR.entityName + '.' + this._subsystemKey]},
        ];
        this.settingsKeys = AppUtils.flattenArray(this.settingsPages.map(p => p.keys));
        this.updateForm();
    }

    protected updateForm() {
        this.isUpdating = true;
        this.settSrv.updateSettings(this.settingsKeys, true).then((datacfg: ISettingsConfig) => {

            this.data[SETTINGS_DESCRIPTOR.entityName] =  datacfg.value;
            this.inputscfg = datacfg.config;

            this._updateDiscriptorForConfig();

            return this.inpSrv.getInputs(this.data, false).then((inputs) => {
                this.inputs = inputs;
                this.inpSrv.updateInputAndPageList(SETTINGS_DESCRIPTOR.entityName, this.inputs, this.pagesFields);
                this.form = this.inpSrv.toFormGroup(this.inputs);
                this.isUpdating = false;
            });
        }).catch(error => {
            return this.errSrv.passGlobalErrors(error).then(() => {
                this.isUpdating = false;
            });
        });
    }

    get isPagesPresent() {
        return true;
    }

    protected generatePages() {
        this.pageList = [
            { title: 'Основные', id: 0},
            { title: 'Настройки', id: 1},
        ];

    }

    private _updateDiscriptorForConfig() {
        const fields = [];
        for (const key in this.inputscfg) {
            if (this.inputscfg.hasOwnProperty(key)) {
                const field: ISettings = this.inputscfg[key];

                if (field.settingType === ISettingType.Value || field.settingType === ISettingType.Undefined) {
                    const inputDescriptor = this.inpSrv.settingToInputDescriptor(field);
                    fields.push(
                        inputDescriptor
                    );

                }
            }
        }

        SETTINGS_DESCRIPTOR.fields = fields;
        SETTINGS_DESCRIPTOR.edit_list = this.settingsPages.map (p => {
            return { title: p.title, fields:
                AppUtils.flattenArray(
                    fields.filter(s => p.keys.find( key => s.key.startsWith(key)))
                ).map(s => s.key)
            };
        });

        this.pagesFields = SETTINGS_DESCRIPTOR.edit_list;

        this.pages1 = this.pages.concat(
            this.pagesFields
        );


        // SETTINGS_DESCRIPTOR.edit_list = [
        //     { title: 'Основные', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.web.general.')).map(s => s.key) },
        //     { title: 'Рубрикатор', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.web.rubric.')).map(s => s.key) },
        //     { title: 'Социальные сети', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.auth.')).map(s => s.key) },
        //     { title: 'Тест', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.test.')).map(s => s.key) },
        // ];
        // this.pagesList = SETTINGS_DESCRIPTOR.edit_list;
    }

}
