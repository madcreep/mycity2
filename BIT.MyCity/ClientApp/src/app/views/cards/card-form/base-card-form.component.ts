import { IEditPageDescriptor, E_ACTION_TYPE } from '../../../model/datatypes/descriptors.interfaces';
import { FormGroup } from '@angular/forms';
import { AppUtils } from 'src/app/helpers/app.static';
import { ICardActionButton, ICardAction } from '../card.interfaces';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { SettingsService } from 'src/app/services/settings.service';
import { InputsService } from 'src/app/services/input.service/inputs.service';
import { ErrorsService } from 'src/app/services/errors.service';
import { Editable } from 'src/app/common/editable';
import {OnInit, Input, OnChanges, Output, EventEmitter, SimpleChanges, Directive, Component} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ConfirmWindowService } from 'src/app/common/confirm-window/confirm-window.service';
import { DataService } from 'src/app/services/data.service';
import { MessageService } from 'src/app/services/message.service';

@Directive()
// @Component( { template: '' } )
// tslint:disable-next-line:directive-class-suffix
export abstract class BaseCardFormComponent extends Editable implements OnChanges, OnInit {

    @Input() inputs: InputBase<any>[];
    @Input() form: FormGroup;
    @Input() editMode: boolean;
    @Input() isNewRecord: boolean;
    @Input() data: any;
    @Input() pages: IEditPageDescriptor[];
    @Input() descriptor: BaseTableDescriptor;
    @Input() getAdditionalDataFn: (input: InputBase<any>) => void;

    @Output() formInvalid: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() actionRequest: EventEmitter<any> = new EventEmitter<any>();
    @Output() formReady: EventEmitter<any> = new EventEmitter<any>();
    @Output() editModeChanged: EventEmitter<any> = new EventEmitter<any>();
    @Output() activePageIndex: BehaviorSubject<any> = new BehaviorSubject<any>(0);

    public isChanged = false;
    public pageList: ICardAction[] = [];
    public activePage = 0;

    private _currentFormStatus: any;

    get isPagesPresent() {
        return this.pages && this.pages.length > 1;
    }

    constructor (
        protected settSrv: SettingsService,
        protected confirmSrv: ConfirmWindowService,
        protected inpSrv: InputsService,
        protected dSrv: DataService,
        protected errSrv: ErrorsService,
        protected msgSrv: MessageService,
    ) {
        super();
    }

    ngOnInit() {
        this.formReady.emit(this);
        this.setEditMode(this.editMode);
    }

    closeCard() {
        return false;
    }

    getInputOptsFn = (input: InputBase<any>) => {
        return {};
    }

    getDataForSave(): Promise<any> {
        const res = AppUtils.deepCopy(this.editedData);
        for (const key in this.form.controls) {
            if (Object.prototype.hasOwnProperty.call(this.form.controls, key)) {
                const control = this.form.controls[key];
                const input = control['input'];
                if (input.noStoreValue && AppUtils.getValueByPath(res, key) !== undefined) {
                    AppUtils.deleteValue(res, key);
                }
            }
        }
        return Promise.resolve(res);
    }


    ngOnChanges(changes: SimpleChanges): void {
        if (this.form) {
            this.isChanged = false;
            this.prevValues = AppUtils.deepCopy(this.data);
            this.initial = AppUtils.deepCopy(this.data);
            this.editedData = this.isNewRecord ? AppUtils.deepCopy(this.data) : {};
            this.generatePages();
            this.resubscribe();
            this.validate();
        }
    }


    get inputsOnPage () {
        return this.getInputsOnPage();
    }

    getInputsOnPage(page = this.activePage) {
        if (this.isPagesPresent) {
            return this.pages[page].fields
                .map(f => this.inputs.find(i =>
                    i.key === this.descriptor.entityName + '.' + f
                    || i.key === f)
                    )
                .filter( f => !!f);
        } else {
            return this.inputs;
        }
    }

    isArray(item): boolean {
        return item instanceof Array;
    }

    public updateActions(actionList: ICardAction[]) {
        for (const act of actionList) {
            switch (act.id) {
                case E_ACTION_TYPE.save:
                        act.hidden = !this.editMode;
                    break;
                case E_ACTION_TYPE.cancel: {
                        act.title = this.editMode ? 'Отмена' : 'Закрыть';
                    break;
                }

                default:
                    break;
            }
        }
    }

    public validate() {
        this.form.updateValueAndValidity();
    }

    public onPageClick(button: ICardActionButton) {
        this.activePage = button.action.id;
        this.activePageIndex.next(this.activePage);
    }

    public onAfterSave(results: any) {
        this.isChanged = false;
    }

    public onBeforeSave(): Promise<boolean> {
        return Promise.resolve(true);
    }

    public getInput(key) {
        if (key[0] === '/') {
            return this.inputs.find( i => ('/' + i.key === key) );
        }
        return this.inputs.find( i => (i.key === key));
    }

    public onActionClick(button: ICardActionButton) {
    }
    public getSavedDataFromResponses(responses: any[]) {
        const result = responses.reduce((acc, r) => {
            if (r.data) {
                acc = Object.assign(acc, r.data);
            }
            return acc;
        }, {});
        if (!!this.descriptor.updateEntityName && result[this.descriptor.updateEntityName]) {
            result[this.descriptor.entityName] = result[this.descriptor.updateEntityName];
            delete result[this.descriptor.updateEntityName];
        }
        return result;
    }

    protected generatePages() {
        this.pageList = [];
        if (this.isPagesPresent) {
            for (let i = 0; i < this.pages.length; i++) {
                const page = this.pages[i];
                const action = <ICardAction>{ id: i, title: page.title, active: (i === 0), hidden: page.hidden };
                this.pageList.push(action);
            }
        }
    }

    // return true if no need to store
    protected onValueChanged(path: string, newValue: any, prevValue: any /*, initialValue: any*/) {
        return false;
    }

    protected resubscribe() {
        this.unsubscribe();
        super.resubscribe();

        this.subscriptions.push(this.form.statusChanges
            .subscribe((status) => {
                if (this._currentFormStatus !== status) {
                    this.formInvalid.emit(status === 'INVALID');
                }
                this._currentFormStatus = status;
            }));

    }

    protected setEditMode(newEditMode) {
        if (this.editMode !== newEditMode) {
            this.editMode = newEditMode;
            this.editModeChanged.emit(newEditMode);
        }
    }
}
