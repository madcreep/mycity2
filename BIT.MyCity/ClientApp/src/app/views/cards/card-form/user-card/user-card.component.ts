import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BaseCardFormComponent } from '../base-card-form.component';
import { MultiselectInput } from 'src/app/modules/dynamic-inputs/types/multiselect-input';
import { IRoleType, IClaimType, ILoginSystem, IUser } from 'src/generated/types.graphql-gen';
import { AppUtils } from 'src/app/helpers/app.static';
import { DynamicInputClaimSelectComponent } from 'src/app/modules/dynamic-inputs/components/claimselect/dynamic-input-claimselect.component';
import { RESETPASSWORD_DESCRIPTOR as RESETPASSWORD_DESCRIPTOR } from 'src/app/model/datatypes/user-reset-password.const';

@Component({
    selector: 'app-user-card',
    //   templateUrl: './user-card.component.html',
    //   styleUrls: ['./user-card.component.css'
    templateUrl: './../simple-card/simple-card.component.html',
    styleUrls: ['./../simple-card/simple-card.component.css']
})
export class UserCardComponent extends BaseCardFormComponent implements OnInit, AfterViewInit {
    public isESIAUser = false;

    ngOnInit() {
        if (!this.isNewRecord) {
            const claims = AppUtils.getValueByPath(this.data, 'user.allClaims');
            if (claims) {
                this.isESIAUser = !!claims.find(c => c.type === 'ls' && c.value === ILoginSystem.Esia);
                if (this.isESIAUser) {
                    const input = this.getInput('user.email');
                    input.readonly = true;
                    input.label = 'E-Mail (для пользователя ESIA изменить E-Mail нельзя)';
                }
            }

            const user = (this.data.user as IUser);
            if (!(user.loginSystem === ILoginSystem.Form && user.selfFormRegister !== true)) {
                this.inputs.forEach(input => input.readonly = true);
                const userDisconnectedInput = this.getInput('user.disconnected');
                if (userDisconnectedInput) {
                    userDisconnectedInput.readonly = false;
                }
            }
        }
        super.ngOnInit();
    }

    ngAfterViewInit(): void {
        this.updateClaimsControlForRoles(this.data.user?.roles?.map(r => r.id) ?? []);
    }

    isClaimEqual(c1: IClaimType, c2: IClaimType) {
        return (c1.type === c2.type) && (c1.value === c2.value);
    }

    getDataForSave(): Promise<any> {
        if (!this.isNewRecord) {
            const res = AppUtils.deepCopy(this.editedData);
            const resetPassword = res.user ? res.user[RESETPASSWORD_DESCRIPTOR.entityName] : null;
            if (resetPassword) {
                if (resetPassword.newPassword) {
                    res[RESETPASSWORD_DESCRIPTOR.entityName] = resetPassword;
                    res[RESETPASSWORD_DESCRIPTOR.entityName].id = this.data.user.id;
                }
                delete res.user[RESETPASSWORD_DESCRIPTOR.entityName];
            }

            return Promise.resolve(res);
        } else {
            return Promise.resolve(this.editedData);
        }
    }

    protected onValueChanged(path: string, newValue: any, prevValue: any /*, initialValue: any*/) {
        if (path === 'user.roleIds') {
            this.updateClaimsControlForRoles(newValue);
        }
        return false;
    }
    private updateClaimsControlForRoles(roleIds: []) {
        const rolesInput: MultiselectInput = <MultiselectInput>this.inputs.find(i => i.key === 'user.roleIds');
        const rolesList = <IRoleType[]>rolesInput.options.map(o => o.obj);
        const roles = roleIds.map(id => rolesList.find(r => r.id === id));
        const rolesClaims = !roles.length ? [] : roles.map(r => r.claims).reduce((x, y) => [...x, ...y]);

        const claimsInput = this.inputs.find(i => i.key === 'user.claims');
        if (!claimsInput) {
            return;
        }
        const clObj = <DynamicInputClaimSelectComponent>claimsInput.dib;

        const isEdited = (this.editedData && this.editedData.user && this.editedData.user.claims);
        let fixedClaims = isEdited ?
            this.editedData.user.claims : this.data.user.claims;

        if (fixedClaims) {
            fixedClaims = fixedClaims.filter(cl => {
                return !rolesClaims.find(rc => this.isClaimEqual(rc, cl));
            });
            if (isEdited) {
                this.setValue('user.claims', fixedClaims);
            }

        }

        clObj.updateForRoleClaims(rolesClaims);
    }





}
