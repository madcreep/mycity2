import {IExecuteReportResult, IReportSettingsInput, IReportSingleSettings, IReportSingleSettingsInput} from '../../../../../generated/types.graphql-gen';
import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {BaseCardFormComponent} from '../base-card-form.component';
import {ICardAction, ICardActionButton} from '../../card.interfaces';
import {E_ACTION_TYPE, E_FIELD_TYPE} from '../../../../model/datatypes/descriptors.interfaces';
import {AppUtils} from '../../../../helpers/app.static';
import {InputBase} from '../../../../modules/dynamic-inputs/types/input-base';

@Component({
    selector: 'app-report-card',
    templateUrl: './report-card.component.html',
    styleUrls: ['./report-card.component.scss']
})
export class ReportCardComponent extends BaseCardFormComponent implements OnInit, OnChanges {

    reportSettings: IReportSingleSettings[] = [];
    fields = [
        'CUSTOM_TEXT1',
        ['CUSTOM_TEXT1', 'CUSTOM_TEXT1'],
        ['-', 'CUSTOM_TEXT1'],
        ['CUSTOM_TEXT1', '-'],
        ['CUSTOM_TEXT1', '-', 'CUSTOM_TEXT1'],
        ['CUSTOM_TEXT1', '-', 'CUSTOM_TEXT1', '-', '-'],
        'CUSTOM_TEXT1',
    ];

    actionList: ICardAction[] = [
        {
            id: E_ACTION_TYPE.send,
            title: 'Отправить',
            class: 'btn-success'
        }
    ];

    isReportFilesExists = false;
    attachesFiles = [];
    buildedReport: IExecuteReportResult;

    ngOnInit(): void {
        super.ngOnInit();
        this.isReportFilesExists = false;
        this.reportSettings = this.data.reports?.parameters as IReportSingleSettings[] ?? [];
        this.inputs = this.inpSrv.convertFromSettings(this.reportSettings)
            .filter( input => !!input );

        this.fields = this.inputs.map( input => input.key);
        this.form = this.inpSrv.toFormGroup(this.inputs);
        this.resubscribe();
    }

    ngOnChanges(changes: SimpleChanges): void {
        super.ngOnChanges(changes);
    }

    protected onValueChanged(path: string, newValue: any, prevValue: any): boolean {
        return super.onValueChanged(path, newValue, prevValue);
    }

    public override updateActions(actionList: ICardAction[]) {
    }

    public override onActionClick(button: ICardActionButton) {
        console.log(this.form.value);
        if (button.action.id === E_ACTION_TYPE.send) {
            const settings: IReportSettingsInput = {
                reportId: this.data.reports.reportId,
                timeOffset: 0,
                parameters: this.buildReportValues()
            };
            this.dSrv.executeReport(settings).then( result => {
                if (result && result.errors && result.errors.length !== 0) {
                    const errorForm = this.confirmSrv.buildMultilineMessage('Ошибка построения отчета', '', result.errors);
                    this.confirmSrv.confirm(errorForm).then();
                } else {
                    console.log(result);
                    if (result?.file?.id) {
                        this.buildedReport = result;
                        this.attachesFiles = [
                            result.file
                        ];
                        this.isReportFilesExists = true;
                    }
                }
            });
        }
    }

    getAdditionalDataFor = (input: InputBase<any>) => {
        if (input.controlType === E_FIELD_TYPE.filesPond) {
            return  [this.buildedReport.file];
        }
        return null;
    }

    private optionsForReportValue(type: E_FIELD_TYPE, controlValue: any): string {
        switch (type) {
            case E_FIELD_TYPE.dateRangeExt: {
                const options = JSON.stringify({
                    dateRangePlusN: controlValue.nValue,
                    dateRangeMode: controlValue.optionValue,
                    availableDateRanges: null,
                });
                return options;
            }
            default: {
                return null;
            }
        }
    }
    private valueToReportValue(type: E_FIELD_TYPE, value: any): string {
        switch (type) {
            case E_FIELD_TYPE.dateRangeExt: {
                const startValue = AppUtils.dateToStringIso(value.dateRange[0]) ?? 'null';
                const endValue = AppUtils.dateToStringIso(value.dateRange[1]) ?? 'null';
                return `${startValue}:${endValue}`;
            }
            case E_FIELD_TYPE.checkBoxGroup: {
                return value.join('|');
            }
            case E_FIELD_TYPE.number: {
                return [value, null].join(':');
            }
            case E_FIELD_TYPE.numberRange: {
                return value.join(':');
            }
            case E_FIELD_TYPE.boolean: {
                return value ? 'true' : 'false';
            }
            default: {
                if (value instanceof Array) {
                    return value.join('|');
                }
                return value;
            }
        }
    }
    private buildReportValues(): IReportSingleSettingsInput[] {
        const result: IReportSingleSettingsInput[] = [];

        for (const input of this.inputs) {
            const type = input.controlType;
            const key = input.key;
            const rawValue = this.form.value[key];
            const value = this.valueToReportValue(type, rawValue);
            const reportValue = {
                name: key,
                value: value
            } as IReportSingleSettingsInput;
            const options = this.optionsForReportValue(type, rawValue);
            if (!!options) {
                reportValue.options = options;
            }
            result.push(reportValue);
        }
        return result;
    }
}
