import { Component, OnInit, Input } from '@angular/core';
import { Editable } from 'src/app/common/editable';
import { IEditPageDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { ICardAction } from '../../card.interfaces';
import { DescriptorsService } from 'src/app/services/descriptors.service';
import { ErrorsService } from 'src/app/services/errors.service';
import { InputsService } from 'src/app/services/input.service/inputs.service';
import { DataService } from 'src/app/services/data.service';
import { AppUtils } from 'src/app/helpers/app.static';

@Component({
    selector: 'app-base-card-edit',
    templateUrl: './base-card-edit.component.html',
    styleUrls: ['./base-card-edit.component.css']
})
export class BaseCardEditComponent extends Editable implements OnInit {

    @Input() data = {};
    @Input() isNewRecord = true;
    @Input() descriptor: BaseTableDescriptor;
    @Input() updateData = true;
    @Input() readonly = false;

    public inputs: InputBase<any>[] = [];
    public isFormInvalid: boolean;
    public actionList: ICardAction[] = [];
    public pagesList: IEditPageDescriptor[] = [];
    isUpdate: boolean;
    id: number;

    activePage = 0;


    constructor (
        protected descrSrv: DescriptorsService,
        // protected settingsSrv: SettingsService,
        protected errSrv: ErrorsService,
        protected inpSrv: InputsService,
        // protected confirmSrv: ConfirmWindowService,
        // private _route: ActivatedRoute,
        // private _location: Location,
        // private _msgSrv: MessageService,
        private _dataSrv: DataService,
    ) {
        super();
    }

    getAdditionalDataFor = (input: InputBase<any>) => {
        return null;
    }
    ngOnInit() {
        this.actionList = this.descriptor.actionsCard();
        this.pagesList = this.descriptor.getAbsPathesPageList(this.isNewRecord);
        this.updateForm();
    }

    savePromise(changes: any, isNewRecord: boolean) {
        const result = [];
        for (const ent in changes) {
            if (changes.hasOwnProperty(ent)) {
                const data = changes[ent];
                if (this.isNewRecord) { // Insert
                    result.push(
                        this._dataSrv.createRecord(ent, data)
                    );
                } else {
                    result.push(
                        this._dataSrv.updateRecord(ent, this.id, data)
                    );
                }
            }
        }
        return Promise.all(result);
    }

    public saveForm(closeOnDone = true): Promise<any> {
        if (this.isFormInvalid) {
            return;
        }

        return this.onBeforeSave().then(
            (continueSave) => {
                if (!continueSave) {
                    return;
                }
                return this.getEditedData().then(
                    (changes) => {
                        if (AppUtils.isObjectEmpty(changes)) {
                            this.closeCard();
                            return;
                        }
                        const savePromise = this.savePromise(changes, this.isNewRecord);

                        return savePromise.then((data) => {
                            // const msg = new Message('Сохранение', 'Данные сохранены', MessageType.success);
                            // this._msgSrv.addMessage(msg);
                            this.onAfterSave(data);
                            if (closeOnDone) {
                                // this.closeCard();
                            }
                            this.isUpdate = false;
                            return data;
                        });
                    }
                );
            }
        ).catch(error => {
            return this.errSrv.passGlobalErrors(error).then(() => {
                this.isUpdate = false;
            });
        });
    }

    public getTitle(): string {
        return this.descriptor.genRecordTitle(this.data, this.isNewRecord);
    }

    protected onAfterSave(data) {
        this.isChanged = false;
    }

    protected getEditedData(): Promise<any> {
        return Promise.resolve(this.editedData);
    }

    protected onBeforeSave(): Promise<boolean>  {
        return Promise.resolve(true);
    }

    protected updateForm() {
        this.isUpdate = true;
        const entKey = { [this.descriptor.entityName]: this.descriptor.id};
        let inputsPromise;
        if (this.isNewRecord) {
            if (this.updateData) {
                this.data = { [this.descriptor.entityName]: this.descriptor.genNewRecord(null) };
            }
            this.editedData = AppUtils.deepCopy(this.data);
            inputsPromise = this.inpSrv.getInputs(this.data, this.isNewRecord, entKey);
        } else {

            let updatePromise;
            if (this.updateData) {
                this.data = {};
                updatePromise = this.descrSrv.dataRead(this.descriptor, this.id, this.data);
            } else {
                updatePromise = Promise.resolve(this.data);
            }

            inputsPromise = updatePromise.then (() => {
                return this.inpSrv.getInputs(this.data, this.isNewRecord, entKey);
            });
        }

        return Promise.resolve(inputsPromise).then(
            inputs => {
                this.id = this.data[this.descriptor.entityName].id;
                this.inputs = inputs;
                this.pagesList = this.descriptor.getAbsPathesPageList(this.isNewRecord, inputs);
                this.inpSrv.updateInputAndPageList(this.descriptor.entityName, this.inputs, this.pagesList);
                this.form = this.inpSrv.toFormGroup(this.inputs);
                this.updateActions();
                this.resubscribe();
                this.isUpdate = false;
        }).catch (error => {
            return this.errSrv.passGlobalErrors(error).then(() => {
                this.closeCard();
                this.isUpdate = false;
            });
        });
    }

    protected updateActions() {
    }

    protected onValueChanged(path: any, newValue: any, prevValue: any, initialValue: any): boolean {
        return false;
    }
    protected closeCard() {
    }

}
