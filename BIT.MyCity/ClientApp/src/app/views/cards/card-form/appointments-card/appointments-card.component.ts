import { Component, OnInit, ViewChild } from '@angular/core';
import { MESSAGES_TAB_LABEL } from 'src/app/model/datatypes/appeals/appeal-base.const';
import { IAppointment } from 'src/generated/types.graphql-gen';
import { BaseCardFormComponent } from '../base-card-form.component';

@Component({
    selector: 'app-appointments-card',
    templateUrl: './appointments-card.component.html',
    styleUrls: ['./appointments-card.component.css']
})
export class AppointmentsCardComponent extends BaseCardFormComponent implements OnInit {

    @ViewChild('myPond', {static: true}) myPond: any;

    filePageIndex = -1;
    answersPageIndex = -1;

    files = [];


    // pondOptions = FILE_UPLOADER_GRAPHQL_POND;

    // {
    //     class: 'my-filepond',
    //     multiple: true,
    //     'data-max-file-size': '3MB',
    //     'data-max-files': 3,
    //     labelIdle: 'Перетащите файлы сюда',
    //     acceptedFileTypes: 'image/jpeg, image/png',

    //     allowDrop: true,
    //     allowReplace: true,
    //     instantUpload: true,
    //     server: {
    //         url: '/client',

    //         // TODO: [MYC-179] реализовать загрузку файлов
    //         process: (fieldName, file, metadata, load, error, progress, abort) => {

    //             // MessageService.sendingFile = true;
    //             // fieldName is the name of the input field
    //             // file is the actual file object to send
    //             const formData = new FormData();
    //             formData.append('file', file, file.name);

    //             const request = new XMLHttpRequest();
    //             request.open('POST', '/appointment/upload');


    //             // Should call the progress method to update the progress to 100% before calling load
    //             // Setting computable to false switches the loading indicator to infinite mode
    //             request.upload.onprogress = (e) => {
    //                 progress(e.lengthComputable, e.loaded, e.total);
    //             };

    //             // Should call the load method when done and pass the returned server file id
    //             // this server file id is then used later on when reverting or restoring a file
    //             // so your server knows which file to return without exposing that info to the client
    //             const files = this.files;
    //             request.onload = function () {
    //                 if (request.status >= 200 && request.status < 300) {
    //                     // the load method accepts either a string (id) or an object
    //                     const file1 = JSON.parse(request.responseText);
    //                     load(file1.extId);
    //                     files.push(file1.extId);
    //                     // MessageService.sendingFile = false;
    //                 } else {
    //                     // Can call the error method if something is wrong, should exit after
    //                     error('oh no');
    //                     // MessageService.sendingFile = false;
    //                 }
    //             };
    //             request.send(formData);

    //             // Should expose an abort method so the request can be cancelled
    //             return {
    //                 abort: () => {
    //                     // This function is entered if the user has tapped the cancel button
    //                     request.abort();

    //                     // Let FilePond know the request has been cancelled
    //                     abort();
    //                 }
    //             };
    //         },
    //         revert: (uniqueFileId, load, error) => {

    //             // Should remove the earlier created temp file here
    //             // ...

    //             // Can call the error method if something is wrong, should exit after
    //             // MessageService.sendingFile = true;
    //             console.log(uniqueFileId);
    //             const request = new XMLHttpRequest();
    //             request.open('DELETE', '/appointment/removeFile/' + uniqueFileId);
    //             const files = this.files;
    //             request.onload = function () {
    //                 if (request.status >= 200 && request.status < 300) {
    //                     for (let i = 0; i < files.length; i++) {
    //                         if (files[i] === uniqueFileId) {
    //                             files.splice(i, 1);
    //                             break;
    //                         }
    //                     }
    //                     load();
    //                 } else {
    //                     // Can call the error method if something is wrong, should exit after
    //                     error('oh no');
    //                 }
    //                 // MessageService.sendingFile = false;
    //             };
    //             request.send();
    //         },
    //         restore: null,
    //         // restore: './restore.php?id=',
    //         fetch: '/getFile?id='
    //     }
    // };
    pondFiles = [
    ];
    appointment: IAppointment;

    ngOnInit() {
        this.filePageIndex = this.pages.findIndex(p => p.title === 'Файлы');
        this.answersPageIndex = this.pages.findIndex(p => p.title === MESSAGES_TAB_LABEL);
        this.appointment = <IAppointment>this.data['appointment'];
    }


    pondHandleInit() {
        console.log('FilePond has initialised', this.myPond);
    }

    pondHandleAddFile(event: any) {
        console.log('A file was added', event);
    }

    public getAnswerStatus(status: number): string {
        let result = '<div %style% class="label label-%className%">%content%</div>';
        switch (status) {
          case 0:
            result = result.replace('%className%', 'default')
              .replace('%content%', 'Черновик')
              .replace('%style%', 'style="padding-left: 10px; padding-right: 10px;"');
            break;
          case 1:
            result = result.replace('%className%', 'info')
              .replace('%content%', 'На рассмотрении')
              .replace('%style%', '');
            break;
          case 2:
            result = result.replace('%className%', 'success')
              .replace('%content%', 'Подтверждено')
              .replace('%style%', 'style="padding-left: 10px; padding-right: 10px;"');
            break;
          case 3:
            result = result.replace('%className%', 'danger')
              .replace('%content%', 'Отказано')
              .replace('%style%', 'style="padding-left: 31px; padding-right: 31px;"');
            break;
          case 4:
            result = result.replace('%className%', 'warning')
              .replace('%content%', 'Отменено')
              .replace('%style%', 'style="padding-left: 10px; padding-right: 10px;"');
            break;
          case 5:
            result = result.replace('%className%', 'primary')
              .replace('%content%', 'Архивная')
              .replace('%style%', 'style="padding-left: 10px; padding-right: 10px;"');
            break;
        }
        return result;
      }
}
