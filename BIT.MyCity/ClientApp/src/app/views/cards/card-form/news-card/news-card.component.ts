import { Component, OnInit } from '@angular/core';
import { BaseCardFormComponent } from '../base-card-form.component';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';

@Component({
    selector: 'app-news-card',
    //   templateUrl: './news-card.component.html',
    templateUrl: './../simple-card/simple-card.component.html',
    styleUrls: ['./../simple-card/simple-card.component.css']
})
export class NewsCardComponent extends BaseCardFormComponent {


    getInputOptsFn = (input: InputBase<any>) => {
        if (input.key === 'news.text') {
            return {
                inputClass: 'news-textarea',
            }
        }
        return {};
    }


}
