import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { FT_EXTID } from 'src/app/model/datatypes/fields.const';
import { SETTINGS_TOPIC_ADDEN } from 'src/app/services/setting-names.const';
import { SimpleCardComponent } from '../simple-card/simple-card.component';

@Component({
  selector: 'app-topic-card',
  templateUrl: './../simple-card/simple-card.component.html',
  styleUrls: ['./../simple-card/simple-card.component.css']

})
export class TopicCardComponent extends SimpleCardComponent implements OnChanges {

    private _ENTITY = 'topic';

    ngOnChanges(changes: SimpleChanges): void {
        super.ngOnChanges(changes);
        this.updateModeratorsValues(this.getValue(this._ENTITY + '.subSystemUIDs'));
        this.subscribe(
            this.settSrv.subscribeToValue(SETTINGS_TOPIC_ADDEN,
                (value) => {
                    if (value === 'true') {
                        this._enableEditFields();
                    }
                }

            )
        );
    }

    protected onValueChanged(path: string, newValue: any, prevValue: any /*, initialValue: any*/) {
        switch (path) {
            case (this._ENTITY + '.subSystemUIDs'):
                this.updateModeratorsValues(newValue);

                break;

            default:
                break;
        }
        return false;
    }

    private updateModeratorsValues(subSystemUIDs: []) {
        let moderators: Array<any> = this.getValue(this._ENTITY + '.moderators');
        const ssToAdd = subSystemUIDs.filter(s => !moderators.find(m => m.subsystemUid === s));
        const ssToDelete = moderators.filter(m => !subSystemUIDs.find(s => m.subsystemUid === s)).map(m => m.subsystemUid);

        ssToAdd.forEach(element => {
            moderators.push({ subsystemUid: element, userIds: [] });
        });
        moderators = moderators.filter(m => !ssToDelete.find(s => s === m.subsystemUid));
        this.setValue(this._ENTITY + '.moderators', moderators);
    }

    private _enableEditFields() {
        this.inputs.find(f => f.key === this.descriptor.entityName + '.' + FT_EXTID.key).readonly = false;
    }
}
