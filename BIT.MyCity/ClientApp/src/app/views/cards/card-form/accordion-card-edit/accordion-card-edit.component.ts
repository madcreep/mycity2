import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BaseCardEditComponent } from '../base-card-edit/base-card-edit.component';
import { ICardAction, ICardActionButton } from '../../card.interfaces';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';

@Component({
    selector: 'app-accordion-card-edit',
    templateUrl: './accordion-card-edit.component.html',
    styleUrls: ['./accordion-card-edit.component.scss']
})
export class AccordionCardEditComponent extends BaseCardEditComponent implements OnInit {

    @Input() isOpen = false;
    @Input() headerActions: ICardAction[] = [];
    @Output() actionEvent: EventEmitter<ICardActionButton> = new EventEmitter<ICardActionButton>();



    ngOnInit() {
        super.ngOnInit();
    }

    onSaveClick($event) {
        this.saveForm();
        $event.preventDefault();
        $event.stopPropagation();
    }

    public customClass() {
        return 'accordion-thin';
    }

    actionClick($event, action) {
        this.actionEvent.emit({action});
        $event.stopPropagation();
    }

    isOpenChange (isOpen) {
        this.isOpen = isOpen;
    }
}
