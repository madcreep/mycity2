import { Component, Input, OnInit } from '@angular/core';
import { BaseCardFormComponent } from '../base-card-form.component';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { ISettingNodeType, ISettings } from 'src/generated/types.graphql-gen';
import { IDynamicInputOptions } from 'src/app/modules/dynamic-inputs/fasade/dynamic-input.interfaces';

@Component({
  selector: 'app-simple-card-key-values',
  templateUrl: './simple-card-key-values.component.html',
    styleUrls: ['./simple-card-key-values.component.scss']
})
export class SimpleCardKeyValuesComponent extends BaseCardFormComponent implements OnInit {


    types = E_FIELD_TYPE;
    viewOpts: IDynamicInputOptions = { hideLabel: true, inputClass: 'simple-thin-input'};
    pagesFields: any;

    public getLabelFor(input: InputBase<any>) {
        return input.label || input.key;
    }

    ngOnInit(): void {

        this.pagesFields = this.descriptor.fieldsEdit();

    }

}
