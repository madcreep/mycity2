import { Component, OnInit, Input, Directive } from '@angular/core';
import { BaseCardFormComponent } from '../base-card-form.component';
import { IAddrType, IAppeal, IApprovedEnum } from 'src/generated/types.graphql-gen';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { ICardAction, ICardActionButton } from '../../card.interfaces';
import { E_ACTION_TYPE, E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { ICommentsTreeOptions } from 'src/app/views/cards/card-pages/comments-tree-view/comments-tree-view.component';
import { ICoordPoint } from 'src/app/modules/dynamic-inputs/types/map-point-input';
import { AppUtils } from 'src/app/helpers/app.static';
import { CONTACT_TAB_LABEL, JOURNAL_TAB_LABEL, MESSAGES_TAB_LABEL } from 'src/app/model/datatypes/appeals/appeal-base.const';
import { MESSAGE_PARENTTYPE } from 'src/app/model/datatypes/message.const';
import { Message, MessageType } from 'src/app/services/message';
import { SETTINGS_SS_CIT_ANSWER } from 'src/app/services/setting-names.const';


const PATH_REASON = 'appeal.rejectionReason';
const PATH_REASONVARS = 'appeal.rejResVariants';
const PATH_APPROVE = 'appeal.approve';
const PATH_RUBRIC = 'appeal.rubricId';
export const PATH_APPEAL_STATUS = 'appeal.siteStatus';
export const APPEAL_STATUS_VAL_RUBRIC_SET = 2; // На рассмотрении
export const APPEAL_STATUS_VAL_RUBRIC_UNSET = 0; // Отправлено
export const APPEAL_STATUS_VAL_RUBRIC_DONE = 5; // (Дан ответ) Завершено

@Directive({
    selector: 'app-appeal-card',
    // templateUrl: './appeal-card.component.html',
    // styleUrls: ['./appeal-card.component.scss']
})
export abstract class AppealCardComponent extends BaseCardFormComponent implements OnInit {
    @Input() isCommentsAttaches = true;
    @Input() actionList: ICardAction[] = [];

    filePageIndex = -1;
    answersPageIndex = -1;
    journalPageIndex = -1;
    contactPageIndex = -1;
    contactAddressValue = '';

    answersOptions?: ICommentsTreeOptions = null;
    parenttypes = MESSAGE_PARENTTYPE;

    appealId: string;
    isAcceptButtonsEnabled: boolean;
    approveButtons = [
        <ICardAction>{ id: 1, title: 'Принять', class: 'btn-sm btn-success' },
        <ICardAction>{ id: 2, title: 'Отклонить', class: 'btn-sm btn-danger' },
    ];
    approveButtons1 = [
        <ICardAction>{ id: 3, title: 'Сохранить', class: 'btn-sm btn-success' },
        // <ICardAction>{ id: 2, title: 'Отклонить', class: 'btn-sm btn-danger' },
    ];

    pondFiles = [
    ];
    input_rejectionReason: InputBase<any>;
    input_approved: InputBase<any>;
    input_rejResVariants: InputBase<any>;
    approveUnsave: boolean;

    protected isRegistratonSended = false;

    get isApprovedText() {
        const appr = this.getValue('appeal.approved');
        return String(appr).toLocaleLowerCase();
        // if (appr === IApprovedEnum.Accepted) {
        //     return 'Опубликовно';
        // }
        // if (appr === IApprovedEnum.Undefined) {
        //     return 'Ожидает решения';
        // }
        // if (appr === IApprovedEnum.Rejected) {
        //     return 'В Публикации отказано';
        // }
    }

    pondHandleInit() {
        // console.log('FilePond has initialised');
    }

    pondHandleAddFile(event: any) {
        // console.log('A file was added', event);
    }


    get isApprovable() {
        return !this.isNewRecord && this.data.appeal.publicGranted;
    }

    ngOnInit() {
        this.getAnswersOptions().then (
            answersOptions => {
                this.answersOptions = answersOptions;
            }
        )

        this.appealId = this.isNewRecord ? -1 : this.data.appeal.id;

        this.filePageIndex = this.pages.findIndex(p => p.title === 'Файлы');
        this.answersPageIndex = this.pages.findIndex(p => p.title === MESSAGES_TAB_LABEL);
        this.contactPageIndex = this.pages.findIndex(p => p.title === CONTACT_TAB_LABEL);
        this.journalPageIndex = this.pages.findIndex(p => p.title === JOURNAL_TAB_LABEL);


        const addr = (this.data.appeal as IAppeal).author?.addresses?.find(a => a.addrType === IAddrType.Registration);
        this.contactAddressValue = addr ? addr.fullAddress : '';
        this.contactAddressValue = this.contactAddressValue || (this.data.appeal as IAppeal).address;

        if (!this.isNewRecord) {
            this.input_rejectionReason = this.getInput(PATH_REASON);
            this.input_approved = this.getInput('appeal.approved');
            this.input_rejResVariants = this.getInput(PATH_REASONVARS);

            if (this.input_approved) {
                this.form.controls[PATH_REASON].setValidators((control) => {
                    const old_accepting: IApprovedEnum = this.data.appeal.approved;
                    const new_accepting: IApprovedEnum = (this.editedData.appeal && this.editedData.appeal.approved) || old_accepting;
                    if (new_accepting === IApprovedEnum.Rejected) {
                        if (!control.value) {
                            return { valueError: 'Должна быть указана причина отказа' };
                        }
                    }
                    return null;

                });
            }
        }
        this.updateConrols(false);
        super.ngOnInit();
    }



    updateConrols(noUpdate = true) {
        if (!this.isNewRecord) {
            this.updateApproveControls(noUpdate);
        }
        this.inputs.forEach(input => {
            if (input.controlType === E_FIELD_TYPE.select) {
                const oldval = AppUtils.getValueByPath(this.data, input.key);
            }
        });
    }


    getDataForSave(): Promise<any> {
        return super.getDataForSave().then(
            (d) => {
                if (AppUtils.isObjectEmpty(AppUtils.getValueByPath(d, 'appeal'))) {
                    return {};
                }
                return d;
            }
        );
    }

    onApproveClick(button) {
        const hasChanges = this.isChanged;
        if (button.action.id === 1) {
            this.approveUnsave = true;
            this.setValue('appeal.approved', IApprovedEnum.Accepted);

            // this.actionRequest.emit(AppUtils.setValueByPath({}, 'action.id', E_ACTION_TYPE.apply))
            // this.data.appeal.approved = IApprovedEnum.Accepted
            // this.setValue('appeal.rejectionReason', null);
            this.dSrv.approveAppeal(this.appealId, IApprovedEnum.Accepted, null).then(
                (d) => {
                    // this.actionRequest.emit(AppUtils.setValueByPath({}, 'action.id', E_ACTION_TYPE.apply));
                    AppUtils.setValueByPath(this.data, 'appeal.approved', IApprovedEnum.Accepted);
                    this.isChanged = hasChanges;
                }
            );
        } else if (button.action.id === 2) {
            this.approveUnsave = true;
            this.setValue('appeal.approved', IApprovedEnum.Rejected);
            // this.actionRequest.emit(AppUtils.setValueByPath({}, 'action.id', E_ACTION_TYPE.apply))
            // this.data.appeal.approved = IApprovedEnum.Rejected
            this.dSrv.approveAppeal(this.appealId, IApprovedEnum.Rejected, this.getValue('appeal.rejectionReason')).then(
                (d) => {
                    // this.actionRequest.emit(AppUtils.setValueByPath({}, 'action.id', E_ACTION_TYPE.apply));
                    AppUtils.setValueByPath(this.data, 'appeal.approved', IApprovedEnum.Rejected);
                    AppUtils.setValueByPath(this.data, 'appeal.rejectionReason', this.getValue('appeal.rejectionReason'));
                    this.isChanged = hasChanges;
                }
            );
        } else if (button.action.id === 3) {
            // this.actionRequest.emit(AppUtils.setValueByPath({}, 'action.id', E_ACTION_TYPE.apply))
            this.dSrv.approveAppeal(this.appealId, IApprovedEnum.Rejected, this.getValue('appeal.rejectionReason')).then(
                (d) => {
                    // this.actionRequest.emit(AppUtils.setValueByPath({}, 'action.id', E_ACTION_TYPE.apply));
                    this.isChanged = hasChanges;
                }
            );
            this.approveUnsave = false;
        }
        this.updateConrols(false);

    }

    public getAnswerStatus(status: number): string {
        let result = '<div %style% class="label label-%className%">%content%</div>';
        switch (status) {
            case 0:
                result = result.replace('%className%', 'default')
                    .replace('%content%', 'Черновик')
                    .replace('%style%', 'style="padding-left: 10px; padding-right: 10px;"');
                break;
            case 1:
                result = result.replace('%className%', 'info')
                    .replace('%content%', 'На рассмотрении')
                    .replace('%style%', '');
                break;
            case 2:
                result = result.replace('%className%', 'success')
                    .replace('%content%', 'Подтверждено')
                    .replace('%style%', 'style="padding-left: 10px; padding-right: 10px;"');
                break;
            case 3:
                result = result.replace('%className%', 'danger')
                    .replace('%content%', 'Отказано')
                    .replace('%style%', 'style="padding-left: 31px; padding-right: 31px;"');
                break;
            case 4:
                result = result.replace('%className%', 'warning')
                    .replace('%content%', 'Отменено')
                    .replace('%style%', 'style="padding-left: 10px; padding-right: 10px;"');
                break;
            case 5:
                result = result.replace('%className%', 'primary')
                    .replace('%content%', 'Архивная')
                    .replace('%style%', 'style="padding-left: 10px; padding-right: 10px;"');
                break;
        }
        return result;
    }
    statusCSSClass() {
        if (this.isNewRecord) {
            return '';
        }
        switch (this.form.controls['appeal.approved'].value) {
            case IApprovedEnum.Accepted: {
                return 'approved';
            }
            case IApprovedEnum.Rejected: {
                return 'rejected';
            }
        }
        return '';
    }

    getAdditionalDataFor = (input: InputBase<any>) => {
        if (input.controlType === E_FIELD_TYPE.filesPond) {
            return this.data.appeal.attachedFiles;
        }
        return null;
    }

    public createMessageEvent(value) {
    }

    public loadMessagesEvent(messages) {
    }


    public onActionClick(button: ICardActionButton) {
        switch (button.action.id) {
            case E_ACTION_TYPE.register: {
                this.sendRegisterAppeal();
                break;
            }
        }

    }

    protected getAnswersOptions(): Promise<ICommentsTreeOptions> {
        return Promise.resolve ({
            addAnswerEnable: this.settSrv.getValue(SETTINGS_SS_CIT_ANSWER) === 'true',
            isCommentsAttaches: this.isCommentsAttaches,
        });
    }

    // protected getAnswersOptions(): Promise<ICommentsTreeOptions> {
    //     const result = <ICommentsTreeOptions>{
    //         addAnswerEnable: true,
    //         isCommentsAttaches: this.isCommentsAttaches,
    //     };
    //     return Promise.all (
    //         [
    //             this.settSrv.getValue(SETTINGS_SS_CIT_ANSWER).then (
    //                 addAnswerEnable => {result.addAnswerEnable = addAnswerEnable;}
    //             ),
    //         ]
    //     ).then (()=> {return result});
    // }

    protected updateApproveControls(noUpdate: boolean = true) {
        if (!this.input_rejResVariants) {
            return;
        }

        const hasChanges = this.isChanged;

        switch (this.getValue('appeal.approved')) {
            case IApprovedEnum.Rejected: {

                const v = this.getValue(PATH_REASON);
                const vv = this.getValue(PATH_REASONVARS);
                const opt = this.input_rejResVariants.options.find(t => t.value === v);
                if (opt && this.getValue(PATH_REASONVARS) !== opt.value && v !== vv) {
                    if (!noUpdate) {
                        this.setValue(PATH_REASONVARS, v);
                    }
                    this.input_rejectionReason.hidden = true;
                    this.input_rejectionReason.readonly = true;
                } else {
                    if (!noUpdate && v !== vv) {
                        this.setValue(PATH_REASONVARS, this.input_rejResVariants.options[this.input_rejResVariants.options.length - 1].value);
                    }
                    if (v === vv) {
                        this.input_rejectionReason.hidden = true;
                        this.input_rejectionReason.readonly = true;
                    } else {
                        this.input_rejectionReason.hidden = false;
                        this.input_rejectionReason.readonly = false;
                    }

                }

                this.input_rejResVariants.hidden = false;

                break;
            }
            default: {
                this.input_rejResVariants.hidden = true;
                this.input_rejectionReason.hidden = true;
                this.input_rejectionReason.readonly = true;
                break;
            }
        }

        this.form.controls[PATH_REASON].updateValueAndValidity();
        this.isAcceptButtonsEnabled = true;

        this.isChanged = hasChanges;

    }

    protected onValueChanged(path: string, newValue: any, prevValue: any /*, initialValue: any*/) {
        if (path === 'appeal.mappoint') {
            const v = (newValue as ICoordPoint);
            AppUtils.setValueByPath(this.editedData, 'appeal.longitude', v.coord[0]);
            AppUtils.setValueByPath(this.editedData, 'appeal.latitude', v.coord[1]);

            this.setValue('appeal.place', v.text);
            this.isChanged = true;
            return true;
        } else if (path === PATH_REASONVARS) {
            const another = this.input_rejResVariants.options[this.input_rejResVariants.options.length - 1].value;
            if (newValue !== another) {
                this.setValue(PATH_REASON, newValue);
                this.input_rejectionReason.hidden = true;
                this.input_rejectionReason.readonly = true;
            } else {
                this.input_rejectionReason.hidden = false;
                this.input_rejectionReason.readonly = false;
            }


            return true;
        } else if (path === PATH_REASON) {
            // Если поменялся только текст - пересохраняем сам статус - так как там мутатор который требует это поле тоже
            const t = this.getValue(PATH_APPROVE);
            this.setValue(PATH_APPROVE, undefined);
            this.setValue(PATH_APPROVE, t);
        } else if (path === PATH_RUBRIC) {
            if (newValue) {
                this.setValue(PATH_APPEAL_STATUS, APPEAL_STATUS_VAL_RUBRIC_SET);
            } else {
                this.setValue(PATH_APPEAL_STATUS, APPEAL_STATUS_VAL_RUBRIC_UNSET);
            }
        } else if (path === 'appeal.approved') {
            return true;
        }
        return false;
    }

    private sendRegisterAppeal() {
        this.dSrv.registerAppeal(this.appealId).then(
            (res) => {
                this.isRegistratonSended = true;
                this.updateActions(this.actionList);
                this.msgSrv.addMessage(new Message('Сохранение', 'Заявка на регистрацию отправлена', MessageType.success));
            }
        );
    }

}
