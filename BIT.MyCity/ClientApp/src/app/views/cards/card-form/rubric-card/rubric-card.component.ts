import { Component, OnChanges, SimpleChanges } from '@angular/core';
import { SimpleCardComponent } from '../simple-card/simple-card.component';
import { InjectorInstance } from 'src/app/helpers/app.static';
import { AuthService } from 'src/app/services/auth.service';
import { CLAIM_SS_CITIZEN } from 'src/app/services/claims.const';
import { SETTINGS_RUBRIC_ADDEN } from 'src/app/services/setting-names.const';
import { FT_EXTID } from 'src/app/model/datatypes/fields.const';

@Component({
    selector: 'app-rubric-card',
    templateUrl: './../simple-card/simple-card.component.html',
    styleUrls: ['./../simple-card/simple-card.component.css']
})
export class RubricCardComponent extends SimpleCardComponent implements OnChanges {

    ngOnChanges(changes: SimpleChanges): void {
        super.ngOnChanges(changes);
        const auth = InjectorInstance.get(AuthService);
        const citisen_access = auth.isClaimHave([CLAIM_SS_CITIZEN]);
        if (!citisen_access) {
            this.inputs = this.inputs;
            this.inputs.forEach(i => {
                if (i.key === 'rubric.docgroup'
                    || i.key === 'rubric.useForPA'
                ) {
                    i.hidden = true;
                }
            })
        }
        this.pages.forEach(page => {
            if (page.onDataUpdate !== undefined) {
                page.onDataUpdate(this.data);
            }
        });
        this.generatePages();
        this.updateModeratorsValues(this.getValue('rubric.subSystemUIDs'));

        this.subscribe(
            this.settSrv.subscribeToValue(SETTINGS_RUBRIC_ADDEN,
                (value) => {
                    if (value === 'true') {
                        this._enableEditFields();
                    }
                }

            )
        );
    }
    protected onValueChanged(path: string, newValue: any, prevValue: any /*, initialValue: any*/) {
        switch (path) {
            case 'rubric.subSystemUIDs':
                this.updateModeratorsValues(newValue);
                break;

            default:
                break;
        }
        return false;
    }


    private updateModeratorsValues(subSystemUIDs: []) {
        let moderators: Array<any> = this.getValue('rubric.moderators');
        const ssToAdd = subSystemUIDs?.filter(s => !moderators.find(m => m.subsystemUid === s));
        const ssToDelete = moderators?.filter(m => !subSystemUIDs.find(s => m.subsystemUid === s))?.map(m => m.subsystemUid);

        ssToAdd.forEach(element => {
            moderators.push({ subsystemUid: element, userIds: [] });
        });
        moderators = moderators?.filter(m => !ssToDelete.find(s => s === m.subsystemUid));
        this.setValue('rubric.moderators', moderators);
    }

    private _enableEditFields() {
        this.inputs.find(f => f.key === this.descriptor.entityName + '.' + FT_EXTID.key).readonly = false;
    }
}
