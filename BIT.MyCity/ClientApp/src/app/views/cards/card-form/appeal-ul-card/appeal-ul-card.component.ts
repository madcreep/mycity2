import { Component, OnInit } from '@angular/core';
import { AppealCardComponent } from '../appeal-card/appeal-card.component';
import { AbstractControl } from '@angular/forms';
import { ValidatorsControl, VALIDATOR_TYPE } from 'src/app/helpers/validators/validators-control';
import { SETTINGS_SS_ORG_ANSWER, SETTINGS_SS_ORG_EDITDEN } from 'src/app/services/setting-names.const';
import { ICommentsTreeOptions } from 'src/app/views/cards/card-pages/comments-tree-view/comments-tree-view.component';


const WOREG_CB_KEY = 'appeal.wORegNumberRegDate';

@Component({
    selector: 'app-appeal-ul-card',
    templateUrl: '../appeal-card/appeal-card.component.html',
    styleUrls: ['./appeal-ul-card.component.css']
})
export class AppealUlCardComponent extends AppealCardComponent implements OnInit {


    regNumber: AbstractControl;
    regDate: AbstractControl;
    wORegNumberRegDate: AbstractControl;

    ngOnInit() {

        this.setEditMode(this.settSrv.getValue(SETTINGS_SS_ORG_EDITDEN) === 'true');

        this.regNumber = this.form.controls['appeal.regNumber'];
        this.regDate = this.form.controls['appeal.regDate'];
        this.wORegNumberRegDate = this.form.controls[WOREG_CB_KEY];

        ValidatorsControl.appendValidator(this.regNumber,
            ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED_IF_NOTCHECKED, '', this.wORegNumberRegDate));
        ValidatorsControl.appendValidator(this.regDate,
            ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED_IF_NOTCHECKED, '', this.wORegNumberRegDate));
        this.isChanged = false;
        super.ngOnInit();
    }

    protected getAnswersOptions(): Promise<ICommentsTreeOptions> {
        return Promise.resolve ({
            addAnswerEnable: this.settSrv.getValue(SETTINGS_SS_ORG_ANSWER) === 'true',
            readOnlyMode: false,
        });
    }

    protected onValueChanged(path: string, newValue: any, prevValue: any) {
        if (path === WOREG_CB_KEY) {
            this.regNumber.updateValueAndValidity();
            this.regNumber['input'].dib.delayedTooltip();
            this.regDate.updateValueAndValidity();
            this.regDate['input'].dib.delayedTooltip();
        }
        return super.onValueChanged(path, newValue, prevValue);
    }

    get isApprovable() {
        return false;
    }
    protected updateApproveControls() {
    }
}
