import { JOURNAL_DESCRIPTOR } from '../../../../model/datatypes/journals.const';
import { IJournalItemType, IProtocolRecord } from '../../../../../generated/types.graphql-gen';
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { BaseSubscriptions } from 'src/app/common/base-subscriptions';
import { IGridOptions } from 'src/app/model/datatypes/descriptors.interfaces';
import { GridFormatters } from 'src/app/model/datatypes/grid-formatters';
import { ColDef, ColumnApi, GridApi } from 'ag-grid-community';
import { filter } from 'rxjs/operators';
import { IListRange } from 'src/app/services/data.service';
import { MenuPagesHelper } from 'src/app/views/menu-pages/menu-pages.helper';
import { PROTOCOL_DESCRIPTOR } from 'src/app/model/datatypes/protocols.const';

@Component({
  selector: 'app-journal-view',
  templateUrl: './journal-view.component.html',
  styleUrls: ['./journal-view.component.scss']
})
export class JournalViewComponent extends BaseSubscriptions implements OnInit {
    @Input() entityKey: string
    @Input() entityName: string

    items: IJournalItemType[] = []
    totalCount = 0
    isUpdating = true

    protected gridColumnApi: ColumnApi;
    protected gridApi: GridApi;

    public components: any = {}
    public defaultColDef: { sortable: boolean, resizable: boolean, sortingOrder: ['asc', 'desc'] };
    public columnDefs: ColDef[] = [
        {
            field: 'timestamp',
            headerName: 'Дата создания',
            sortable: true,
            // suppressMenu: false,
            valueFormatter: GridFormatters.dateTimeFormatter,
            width: 180,
            minWidth: 180,
            maxWidth: 180,
        },
        {
            field: 'user.fullName',
            width: 250,
            minWidth: 200,
            maxWidth: 300,
        },
        {
            field: 'eventText',
            valueFormatter: this.journalTextFormatter,
        }
    ]

    public groupDefaultExpanded = 1;
    public rowData: any[] = []

    constructor() {
        super()
        this.defaultColDef = this.getdefaultColDef();
    }

    ngOnInit(): void {
        this.isUpdating = true
        this.dataSrv.getJournal(this.entityName, this.entityKey).then (
            values => {
                this.items = values.items
                this.totalCount = values.total
                this.isUpdating = false
                this.rowData = values.items
                this.rowData.forEach (data => {
                    var count = data.properties?.length ?? 1
                    data.rowHeight = count * 28
                })
            }
        )

    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.gridApi.sizeColumnsToFit();
    }

    public getTableStyle(): string {
        return 'width: 100%; font-size: 14px; height: calc(100vh - 200px); ';
    }

    public onCellDoubleClicked(params) {
    }

    public onSelectionChanged(params) {
    }

    public getRowNodeId(item) {
        return item.id;
    }

    public onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.gridApi.sizeColumnsToFit();
    }

    getRowHeight(params): number | undefined | null {
        return params.data.rowHeight;
    }

    protected getdefaultColDef(): any {
        return <IGridOptions>{
            sortable: true,
            resizable: true,
            sortingOrder:  ['desc', 'asc'],
            cellStyle: {
                'white-space': 'break-spaces'
            },
        };
    }

    protected journalTextFormatter (data) {
        let res = '';

        const event = <IJournalItemType>data['data'];
        if (event) {
            res += event.friendlyName + '(' + event.operationName + ') ';

            if (event.properties && Array.isArray(event.properties)) {
                event.properties.forEach(prop => {
                    res += `${prop.friendlyName} [ ${prop.newValueFriendly} ]; \n`
                })
            }
        }
        return res;
    }
}
