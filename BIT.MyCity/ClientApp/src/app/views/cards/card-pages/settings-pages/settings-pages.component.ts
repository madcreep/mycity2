import { Component, OnInit, Input } from '@angular/core';
import { ISettingsConfig } from 'src/app/services/settings.service';
import { AppUtils } from 'src/app/helpers/app.static';
import { SETTINGS_DESCRIPTOR } from 'src/app/model/datatypes/settings.const';
import { E_FIELD_TYPE, IEditPageDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';
import { ISettings, ISettingType } from 'src/generated/types.graphql-gen';
import { BaseCardFormComponent } from 'src/app/views/cards/card-form/base-card-form.component';
import { IDynamicInputOptions } from 'src/app/modules/dynamic-inputs/fasade/dynamic-input.interfaces';

export interface IPagesSettings {
    title: string;
    keys: string[];
}

@Component({
    selector: 'app-settings-pages',
    templateUrl: './settings-pages.component.html',
    styleUrls: ['./settings-pages.component.css']
})
export class SettingsPagesComponent extends BaseCardFormComponent implements OnInit {

    @Input() pagesKeys: IPagesSettings[];

    keys = [];

    public pagesFields: IEditPageDescriptor[];

    isUpdating: boolean;
    inputscfg: any;

    viewOpts: IDynamicInputOptions = { hideLabel: true, inputClass: 'simple-thin-input', formGroupClass: 'no-marigin' };

    ngOnInit() {
        this.keys = AppUtils.flattenArray(this.pagesKeys.map(p => p.keys));
        this.updateForm();
    }

    protected updateForm() {
        this.isUpdating = true;
        this.settSrv.updateSettings(this.keys, true).then((datacfg: ISettingsConfig) => {
            const data = datacfg.value;
            this.data = { [SETTINGS_DESCRIPTOR.entityName]: data };
            this.inputscfg = datacfg.config;

            this._updateDiscriptorForConfig();

            return this.inpSrv.getInputs(this.data, false).then((inputs) => {
                this.inputs = inputs;
                this.inpSrv.updateInputAndPageList(SETTINGS_DESCRIPTOR.entityName, this.inputs, this.pagesFields);
                this.form = this.inpSrv.toFormGroup(this.inputs);
                this.isUpdating = false;
            });
        }).catch(error => {
            return this.errSrv.passGlobalErrors(error).then(() => {
                this.isUpdating = false;
            });
        });
    }

    private _updateDiscriptorForConfig() {
        const fields = [];
        for (const key in this.inputscfg) {
            if (this.inputscfg.hasOwnProperty(key)) {
                const field: ISettings = this.inputscfg[key];
                if (field.settingType === ISettingType.Value || field.settingType === ISettingType.Undefined) {
                    let type;

                    switch (field.valueType) {
                        case 'System.UInt32':
                            type = E_FIELD_TYPE.number;
                            break;
                        case 'System.Boolean':
                            type = E_FIELD_TYPE.boolean;
                            break;
                        case 'System.String':
                        default:

                            type = E_FIELD_TYPE.string;
                            break;
                    }

                    fields.push(
                        {
                            key: field.key,
                            title: field.description,
                            type: type,
                        }
                    );

                }
            }
        }

        SETTINGS_DESCRIPTOR.fields = fields;
        SETTINGS_DESCRIPTOR.edit_list = this.pagesKeys.map(p => {
            return {
                title: p.title, fields:
                    AppUtils.flattenArray(
                        fields.filter(s => p.keys.find(key => s.key.startsWith(key)))
                    ).map(s => s.key)
            };
        });

        this.pagesFields = SETTINGS_DESCRIPTOR.edit_list;


        // SETTINGS_DESCRIPTOR.edit_list = [
        //     { title: 'Основные', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.web.general.')).map(s => s.key) },
        //     { title: 'Рубрикатор', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.web.rubric.')).map(s => s.key) },
        //     { title: 'Социальные сети', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.auth.')).map(s => s.key) },
        //     { title: 'Тест', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.test.')).map(s => s.key) },
        // ];
        // this.pagesList = SETTINGS_DESCRIPTOR.edit_list;
    }
}



 // this.settingsSrv.getSettings(true).then((datacfg: ISettingsConfig) => {
        //     const data = datacfg.value;
        //     this.data = { [SETTINGS_DESCRIPTOR.entityName]: {[this.skey]: data} };
        //     this.inputscfg = datacfg.config;

        //     this.updateControls();
        //     SETTINGS_DESCRIPTOR.fields = this.fields;
        //     SETTINGS_DESCRIPTOR.edit_list = this.pagesList;

        //     return this.inpSrv.getInputs(this.data, false).then((inputs) => {
        //         this.inputs = inputs;
        //         this.formGroup = this.inpSrv.toFormGroup(this.inputs);
        //         this.isUpdate = false;
        //     });
        // }).catch(error => {
        //     return this.errSrv.passGlobalErrors(error).then(() => {

        //         this.isUpdate = false;
        //     });
        // });


    // updateControls() {
    //     this.fields = <IFieldDescriptor[]>this.inputscfg.filter((s: ISettings) =>
    //         s.settingType === ISettingType.Value || s.settingType === ISettingType.Undefined)
    //         .map((field: ISettings) => {
    //             let type;

    //             switch (field.valueType) {
    //                 case 'System.UInt32':
    //                     type = E_FIELD_TYPE.number;
    //                     break;
    //                 case 'System.Boolean':
    //                     type = E_FIELD_TYPE.boolean;
    //                     break;
    //                 case 'System.String':
    //                 default:
    //                     type = E_FIELD_TYPE.string;
    //                     break;
    //             }

    //             return {
    //                 key: field.key,
    //                 title: field.description,
    //                 type: type,
    //             };
    //         });



    // }
