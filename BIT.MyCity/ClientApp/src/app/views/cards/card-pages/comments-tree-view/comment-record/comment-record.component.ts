import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { MESSAGE_DESCRIPTOR, MESSAGE_PARENTTYPE as MESSAGE_PARENTTYPE } from 'src/app/model/datatypes/message.const';
import { Subscriptionable } from 'src/app/common/subscriptionable';
import { AppUtils } from 'src/app/helpers/app.static';
import { IMessage, IMessageKindEnum, IApprovedEnum, ILikeRequestEnum } from 'src/generated/types.graphql-gen';
import { ErrorsService } from 'src/app/services/errors.service';
import { InputsService } from 'src/app/services/input.service/inputs.service';


export interface IMessageView extends IMessage {
    level: number;
    children: any[];
    reason: any[];
}

@Component({
    selector: 'app-comment-record',
    templateUrl: './comment-record.component.html',
    styleUrls: ['./comment-record.component.css']
})
export class CommentRecordComponent extends Subscriptionable implements OnInit, OnChanges {

    @Input() message: IMessageView;
    @Input() level: number;
    @Input() addToParent: string;
    @Input() addToParentType: MESSAGE_PARENTTYPE;
    @Input() isModerationEnabled = false;
    @Input() isAnswerEnabled = false;
    @Input() isAttachEnabled = true;
    @Input() isLikeEnabled = false;
    @Input() isAddForever = false;
    @Input() mouseTrack = true;
    @Input() addedRecordFn: (message, any) => Promise<boolean>;
    @Input() likeRecordFn: (message, any) => Promise<boolean>;
    @Output() editExpanded: EventEmitter<boolean> = new EventEmitter<boolean>();
    @Output() approveClicked: EventEmitter<any> = new EventEmitter<boolean>();


    inputs: InputBase<any>[] = null;
    editData = {};
    isEditActive = false;
    isDeleting = false;
    form: any;
    active: boolean;
    isModerator = true;
    isAnswerable = true;
    commentDateTime: string;
    reasonDateTime: string;
    reason: any;

    ACCEPTED = IApprovedEnum.Accepted;
    REJECTED = IApprovedEnum.Rejected;
    UNDEFINED = IApprovedEnum.Undefined;
    messageClass: string;
    isApprovable: boolean;
    isRejectable: boolean;
    isLikeable: boolean;


    constructor(
        private _errSrv: ErrorsService,
        protected inpSrv: InputsService,
    ) {
        super();
    }

    ngOnInit(): void {
    }

    ngOnChanges(): void {
        if (!this.message) {
            return;
        }
        this.commentDateTime = this.message ? AppUtils.dateTimeToStringValue(this.message.createdAt) : '';
        this.reason = this.getReason();
        this.reasonDateTime = this.reason ? AppUtils.dateTimeToStringValue(this.reason.createdAt) : '';
        this.messageClass = AppUtils.joinSeparator(
            [
                ... this.reason ? ['has-reason'] : [],
                ... this.message.approved === this.ACCEPTED ? ['accepted'] : [],
                ... this.message.approved === this.REJECTED ? ['rejected'] : [],
                ... this.message.approved === this.UNDEFINED ? ['no-approved'] : [],
                ... this.message.deleted ? ['deleted'] : [],
                ... this.active ? ['active'] : [],
                ... this.isModerationEnabled ? ['moderation'] : [],
            ],
            ' ');
        this.isAnswerable = this.isAnswerEnabled && (this.message.approved !== this.REJECTED);
        this.isRejectable = this.isModerationEnabled && (this.message.approved !== this.REJECTED);
        this.isApprovable = this.isModerationEnabled && (this.message.approved !== this.ACCEPTED);
        this.isLikeable = this.isLikeEnabled && (this.message.approved !== this.REJECTED) && !this.message.deleted;
    }

    getReason() {
        if (this.message && this.message.reason && this.message.reason[0]) {
            return this.message.reason[0];
        }

        return null;
    }

    deleteClick(event) {
        this.initEditForm().then(() => {
            this.expandEdit(true, true);
        });
    }

    addClick(event) {
        this.initEditForm().then(() => {
            this.expandEdit(true);
        });
    }

    approveClick(event) {
        this.approveClicked.emit(this);
    }

    expandEdit(expand: boolean, isDeleting: boolean = false) {
        this.isEditActive = expand;
        this.isDeleting = isDeleting;
        if (expand) {
            if (this.isDeleting) {
                this.inputs[0].label = 'Причина удаления';
            } else {
                this.inputs[0].label = 'Введите ответ';
            }
            this.resubscribe();
        } else {
            this.unsubscribe();
        }
        this.editExpanded.emit(expand);
    }

    initEditForm(force = false): Promise<any> {
        if (force) {
            delete this.inputs;
        }
        if (this.inputs) {
            return Promise.resolve(null);
        }
        this.clear();
        return this.inpSrv.getInputs(this.editData, true, {}).then(inputs => {
            this.inputs = inputs;
            this.form = this.inpSrv.toFormGroup(this.inputs);

        });
    }

    additionalDataFor(input) {
        return [];
        // return this.message.attachedFiles;
    }

    clear() {
        this.editData = {
            [MESSAGE_DESCRIPTOR.entityName]: {
                text: '',
            }
        };
    }
    sendClick(event) {
        if (!this.editData[MESSAGE_DESCRIPTOR.entityName].text) {
            this.expandEdit(false);
            return;
        }
        if (this.addedRecordFn) {
            (<IMessage>this.editData[MESSAGE_DESCRIPTOR.entityName]).parentId = this.addToParent;
            (<IMessage>this.editData[MESSAGE_DESCRIPTOR.entityName]).parentType = this.addToParentType;
            (<IMessage>this.editData[MESSAGE_DESCRIPTOR.entityName]).kind = this.isDeleting ?
                IMessageKindEnum.ReasonToParentMessage : IMessageKindEnum.Normal;

            this.addedRecordFn(this.message, this.editData).then((result) => {
                if (result) {
                    this.expandEdit(false);
                    return this.initEditForm(true);
                }
            }).catch(error => {
                return this._errSrv.passGlobalErrors(error).then(() => {
                });
            });
        }
    }



    cancelClick(event) {
        this.expandEdit(false);
    }

    clickLike($event) {
        if (this.likeRecordFn) {
            this.likeRecordFn(this.message, ILikeRequestEnum.Like);
        }
    }

    clickDislike($event) {
        if (this.likeRecordFn) {
            this.likeRecordFn(this.message, ILikeRequestEnum.Dislike);
        }
    }

    elementMouseEnter(event) {
        this.active = this.mouseTrack;
    }

    elementMouseLeave(event) {
        if (!this.isEditActive) {
            this.active = false;
        }
    }

    onKeyDown(evt: KeyboardEvent) {
        if (!evt) {
            return;
        }
        if (evt.keyCode === 27) {
            this.cancelClick(evt);
        } else if (evt.keyCode === 13 && evt.ctrlKey) {
            this.sendClick(evt);
        }

    }


    messageDateTime(message: IMessage): string {
        return AppUtils.dateTimeToStringValue(message.createdAt);
    }



    protected resubscribe() {
        this.unsubscribe();
        for (const key in this.form.controls) {
            if (this.form.controls.hasOwnProperty(key)) {
                const control = this.form.controls[key];
                this.subscriptions.push(control.valueChanges.subscribe((newValue) => {
                    const path = control['id'];
                    AppUtils.setValueByPath(this.editData, path, newValue);
                }));
            }
        }
    }



}
