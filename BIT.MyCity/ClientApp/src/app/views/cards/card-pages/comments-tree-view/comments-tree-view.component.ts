import {Component, Input, OnChanges, Output, EventEmitter} from '@angular/core';
import {MESSAGE_PARENTTYPE, MESSAGE_DESCRIPTOR} from 'src/app/model/datatypes/message.const';
import {IMessageView} from './comment-record/comment-record.component';
import {ICreateMessage, IMessageKindEnum, ILikeRequestEnum, IApprovedEnum} from 'src/generated/types.graphql-gen';
import {AppUtils} from 'src/app/helpers/app.static';
import {BaseSubscriptions} from 'src/app/common/base-subscriptions';

export interface ICommentsTreeOptions {
    addAnswerEnable: boolean;
    readOnlyMode?: boolean;
    isCommentsAttaches?: boolean;
    moderationDisabled?: boolean;
    useKindRoot?: IMessageKindEnum;
}

@Component({
    selector: 'app-comments-tree-view',
    templateUrl: './comments-tree-view.component.html',
    styleUrls: ['./comments-tree-view.component.css']
})
export class CommentsTreeViewComponent extends BaseSubscriptions implements OnChanges {
    @Input() parentType: MESSAGE_PARENTTYPE;
    @Input() parentId: string;
    @Input() options: ICommentsTreeOptions;
    @Output() createMessage: EventEmitter<any> = new EventEmitter<any>();
    @Output() loadMessages: EventEmitter<any> = new EventEmitter<any>();

    parenttypes = MESSAGE_PARENTTYPE;
    messages: IMessageView[] = [];
    isUpdate: boolean;
    mouseTrack = true;

    constructor() {
        super();
    }

    ngOnChanges(): void {
        this.isUpdate = true;
        this.updateMessages().then(
            () => {
                this.isUpdate = false;
            }
        );
    }

    onLikeRecord(message: IMessageView, data: ILikeRequestEnum): Promise<boolean> {
        return this.dataSrv.likeMessage(message.id, data).then(
            (ldata) => {
                return this.updateMessages().then(() => true);
            }
        );
    }

    onAddRecord(message: IMessageView, data: { message: ICreateMessage }): Promise<boolean> {
        if (data.message.kind === IMessageKindEnum.ReasonToParentMessage) {
            return this.dataSrv.rejectMessage(message.id, data.message.text, () => {
                return [
                    // { obj: 'getAppealMessageList', key: 'appealMessageList', variables: {id: this.parentId}},
                    {obj: 'messageList', key: 'messageList', variables: {id: this.parentId}},
                ];
            }).then(() => {
                this.createMessage.emit({message, data});
                return this.updateMessages().then(() => true);
            });
        } else {
            if (this.options.useKindRoot) {
                data.message.kind = this.options.useKindRoot;
            }
            return this.dataSrv.createRecord(MESSAGE_DESCRIPTOR.entityName, data.message, () => {
                return [
                    {obj: 'messageList', key: 'messageList', variables: {id: this.parentId}},
                ];
            }).then(() => {
                this.createMessage.emit({message, data});
                return this.updateMessages().then(() => true);
            });
        }


    }

    updateMessages(): Promise<any> {
        return this.dataSrv.getMessageList(this.parentId, this.parentType).then(
            (data) => {
                let mess = <IMessageView[]>data.data.messageList.items.map(m => {
                    return <IMessageView>Object.assign({}, m, {
                        level: m.due ? AppUtils.levelByDue(m.due) : 0,
                        children: [],
                        reason: [],
                    });
                });

                mess = mess.sort((a, b) => a.level > b.level ? -1 : a.level === b.level ? 0 : 1);

                for (let i = 0; i < mess.length; i++) {
                    const e1 = mess[i];
                    if (!e1) {
                        continue;
                    }
                    for (let n = i; n < mess.length; n++) {
                        const e2 = mess[n];
                        if (!e2) {
                            continue;
                        }
                        if ((e1.level === e2.level + 1) && e1.due.startsWith(e2.due)) {
                            if ((e2.deleted || e2.approved === IApprovedEnum.Rejected) && e1.kind === IMessageKindEnum.ReasonToParentMessage) {
                                e2.reason.push(e1);
                            } else {
                                e2.children.push(e1);
                            }

                            mess[i] = null;
                            break;
                        }
                    }
                }
                this.messages = [];
                this.makeTreeList(this.messages, mess);
                this.loadMessages.emit(this.messages);
            });
    }

    makeTreeList(trg: any[], src: any[]) {
        for (let i = src.length - 1; i >= 0; i--) {
            const e1 = src[i];
            if (e1) {
                trg.push(e1);
                if (e1.children.length) {
                    this.makeTreeList(trg, e1.children);
                }
            }
        }
    }

    editMessageExpanded(expanded: boolean, message = null) {
        this.mouseTrack = !expanded;
    }

    onApproveClick(id, mess) {
        this.dataSrv.approveMessage(id).then(
            (data) => {
                return this.updateMessages().then(() => true);
            }
        );
    }
}
