import { E_ACTION_TYPE } from '../../model/datatypes/descriptors.interfaces';
import { IClaim } from '../../services/claims.const';

export interface ICardDescriptor {
    entityName: string;
    id: number | string;
}

export interface ICardAction {
    id: E_ACTION_TYPE;
    title: string;
    toglabble?: boolean;
    active?: boolean;
    class?: string;
    hidden?: boolean;
    disabled?: boolean;
    permissions?: IClaim[];
}

export interface ICardActionButton {
    action: ICardAction;
    toggled?: boolean;
    hidden?: boolean;
    active?: boolean;
    class?: string;
    disabled?: boolean;
}
