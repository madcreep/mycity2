import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InputBase } from '../../modules/dynamic-inputs/types/input-base';
import { DescriptorsService } from '../../services/descriptors.service';
import { IFieldDescriptor, E_FIELD_TYPE } from '../../model/datatypes/descriptors.interfaces';
import { Subscription } from 'rxjs';
// import { BaseTableDescriptor } from '../../../../datatypes/descriptors.const';
import { AppUtils } from '../../helpers/app.static';
import { InputsService } from '../../services/input.service/inputs.service';
import { LayoutsRoutine } from '../../common/layouts';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import {InputBuilder} from '../../services/input.service/input-builder';

const STORAGE_FILTER_NAME = 'filter';
@Component({
    selector: 'app-filter-control',
    templateUrl: './filter-control.component.html',
    styleUrls: ['./filter-control.component.css']
})

export class FilterControlComponent implements OnInit {
    @Input() filters: IFieldDescriptor[];
    @Input() keyPrefix = '';
    @Input() isOpen = false;
    @Input() applyImmediantly = false;
    @Input() layout = [];
    @Input() useStorageDescriptor: string;
    @Input() data = {};
    @Input() headerLabel: string | undefined;
    @Output() filterApply: EventEmitter<any> = new EventEmitter<any>();
    @Output() openToggle: EventEmitter<boolean> = new EventEmitter<boolean>();

    inputs: InputBase<any>[];
    form: any;
    isUpdating = true;
    isChanged = true;
    // data;
    inputsLayout: any[];
    recordsCounter = 0;
    protected subscriptions: Subscription[] = [];
    protected _editedData: any;

    constructor(
        protected descrSrv: DescriptorsService,
        protected inpSrv: InputsService,
    ) { }

    ngOnInit() {
        return this.refresh();
    }

    isOpenChange(isOpen) {
        if (this.useStorageDescriptor) {
            localStorage.setItem(STORAGE_FILTER_NAME + '-open-' + this.useStorageDescriptor, String(isOpen));
        }
        this.isOpen = isOpen;
        this.openToggle.emit(isOpen);
    }

    refresh() {
        this.isUpdating = true;
        if (this.useStorageDescriptor) {
            const str = localStorage.getItem(STORAGE_FILTER_NAME + '-' + this.useStorageDescriptor);
            this._editedData = AppUtils.deepCopy(JSON.parse(str), '___', '.') || {};

            this.data = { ['filter']: this._editedData };
            this.isOpen = localStorage.getItem(STORAGE_FILTER_NAME + '-open-' + this.useStorageDescriptor) === 'true';
        } else {
            this.data = { ['filter']: BaseTableDescriptor.genNewData({}, this.filters) };
            this._editedData = {};
        }



        this.inputs = [];
        for (let i = 0; i < this.filters.length; i++) {
            const indescr = this.filters[i];
            const value = (indescr.filterOptions && indescr.filterOptions.onFilterSetValue !== undefined) ? indescr.filterOptions.onFilterSetValue(this._editedData, this._editedData[indescr.key])
                : indescr.onGetValue !== undefined ? indescr.onGetValue(null) : this._editedData[indescr.key];
            const input = InputBuilder.inputForDescr(indescr, this.keyPrefix, value);
            if (input) {
                this.inputs.push(input);
            }
        }

        this.inputsLayout = LayoutsRoutine.inputsToLayout(this.layout, this.inputs);

        const inits = this.inputs.map(i => i.init());

        return Promise.all(inits).then(
            () => {
                this.inputs.forEach(i => {
                    const fk = AppUtils.getValueByPath(i, 'descriptor.filterOptions.key');
                    if (fk) {
                        i.key = fk;
                    }
                });
                this.form = this.inpSrv.toFormGroup(this.inputs);
                this.resubscribe();
                this.isUpdating = false;
                if (this.useStorageDescriptor) {
                    this.applyFilter(null);
                }
            }
        );
    }

    get value() {
        return this._editedData;
    }

    public customClass() {
        return 'filter-panel-primary';
    }

    public applyFilter(event) {
        if (this.useStorageDescriptor) {
            // this._editedData = {};
            const str = JSON.stringify(AppUtils.deepCopy(this.value, '.', '___'));
            localStorage.setItem(STORAGE_FILTER_NAME + '-' + this.useStorageDescriptor, str);
        }
        this.filterApply.emit(this.value);
    }

    public clearFilter() {
        for (const c in this.form.controls) {
            if (this.form.controls.hasOwnProperty(c)) {
                const control = this.form.controls[c];
                control.setValue(null);
            }
        }
        this._editedData = {};
        this.applyFilter(null);
    }

    isArray(item): boolean {
        return item instanceof Array;
    }


    protected resubscribe() {
        this.unsubscribe();
        for (const key in this.form.controls) {
            if (this.form.controls.hasOwnProperty(key)) {
                const control = this.form.controls[key];
                this.subscriptions.push(control.valueChanges.subscribe((newValue) => {
                    const path = control['id'];

                    this.isChanged = true;

                    const initial = AppUtils.getValueByPath(this.data, path);

                    if (!this.onValueChanged(path, newValue, initial /*, initial*/)) {
                        if (initial === newValue || newValue === undefined || newValue === null || newValue.length === 0) {
                            delete this._editedData[path];
                        } else {
                            if (control.input.controlType === E_FIELD_TYPE.dateRange && newValue.length === 2) {
                                const moreDay = AppUtils.dateToStringIso(new Date(newValue[0]), true);
                                const lessDay = AppUtils.dateToStringIso(AppUtils.addDays(newValue[1], 1), true);
                                AppUtils.setValueByPath(this._editedData, path + '.greaterOrEqual', moreDay);
                                AppUtils.setValueByPath(this._editedData, path + '.less', lessDay);
                            } else {
                                if (control.input.descriptor.onFilterGetValue) {
                                    newValue = control.input.descriptor.onFilterGetValue(this._editedData, path, newValue);
                                    this._editedData[path] = newValue;
                                    // AppUtils.setValueByPath(this._editedData, path, newValue);
                                } else {
                                    this._editedData[path] = newValue;
                                    // AppUtils.setValueByPath(this._editedData, path, newValue);
                                }
                            }
                        }
                    }

                    if (this.applyImmediantly) {
                        this.applyFilter(null);
                    }

                }));
            }
        }
    }

    protected onValueChanged(path: any, newValue: any, initial: any) {
        return false;
    }

    protected unsubscribe() {
        this.subscriptions.forEach((subscription: Subscription) => {
            if (subscription != null && !subscription.closed) {
                subscription.unsubscribe();
            }
        });
        this.subscriptions = [];
    }

}
