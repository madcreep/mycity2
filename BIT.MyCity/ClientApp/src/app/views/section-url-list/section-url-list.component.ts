import { Component, OnInit } from '@angular/core';
import {CardComponent} from '../cards/card.component';
import {Subscriptionable} from '../../common/subscriptionable';
import {ActivatedRoute} from '@angular/router';
import {EnvSettings, LinksSection} from '../../model/settings/envsettings';

@Component({
  selector: 'app-section-url-list',
  templateUrl: './section-url-list.component.html',
  styleUrls: ['./section-url-list.component.scss']
})
export class SectionUrlListComponent extends Subscriptionable implements OnInit {
    public section: LinksSection;

  constructor(
      private _route: ActivatedRoute,
  ) {
      super();
  }

  ngOnInit(): void {
      this.subscribe(this._route.params.subscribe((params) => {
          if (params) {
              this.section = EnvSettings.URLsSections[params.id];
          }
      }));
  }
}
