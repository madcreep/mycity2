import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionUrlListComponent } from './section-url-list.component';

describe('SectionUrlListComponent', () => {
  let component: SectionUrlListComponent;
  let fixture: ComponentFixture<SectionUrlListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SectionUrlListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionUrlListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
