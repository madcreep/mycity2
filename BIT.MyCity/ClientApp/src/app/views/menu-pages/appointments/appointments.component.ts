import { Component } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { ICardAction } from 'src/app/views/cards/card.interfaces';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { APPOINTMENT_DESCRIPTOR } from 'src/app/model/datatypes/appointment.const';

@Component({
    selector: 'app-appointments',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./appointments.component.css']
})

export class AppointmentsComponent extends MenuBaseListComponent {

    public isUpdating(): boolean {
        return false;
    }
    protected getDescriptor(): BaseTableDescriptor {
        return APPOINTMENT_DESCRIPTOR;
    }

}
