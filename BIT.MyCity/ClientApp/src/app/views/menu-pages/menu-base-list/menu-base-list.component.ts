import { DescriptorsService } from '../../../services/descriptors.service';
import { IGridOptions, E_ACTION_TYPE, IFieldDescriptor } from '../../../model/datatypes/descriptors.interfaces';
import {ICardAction, ICardActionButton, ICardDescriptor} from '../../cards/card.interfaces';
import { HostListener, OnInit, Injector, ViewChild, Injectable, Directive } from '@angular/core';
import {Router, ActivatedRoute, CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree} from '@angular/router';
import {MenuPagesHelper} from '../menu-pages.helper';
import {DataService, IListRange} from 'src/app/services/data.service';
import {MessageService} from 'src/app/services/message.service';
import {MessageType, Message} from 'src/app/services/message';
import {BaseTableDescriptor} from 'src/app/model/datatypes/descriptors.const';
import {GridApi} from 'ag-grid-community/dist/lib/gridApi';
import {ConfirmWindowService} from 'src/app/common/confirm-window/confirm-window.service';
import {IConfirmWindow} from 'src/app/common/confirm-window/confirm-window.component';
import {CONFIRM_OP_DELETE, BUTTON_RESULT_YES, CONFIRM_OP_UNDELETE, CONFIRM_LEAVE_FORM} from 'src/app/consts/confirmations.const';
import {ErrorsService} from 'src/app/services/errors.service';
import {FT_WEIGHT, FT_DELETED} from 'src/app/model/datatypes/fields.const';
import {AgGridAngular} from 'ag-grid-angular';
import {ColumnApi} from 'ag-grid-community/dist/lib/columnController/columnApi';
import {IOrderByItemType, ISearchDirection} from 'src/generated/types.graphql-gen';
import {AppUtils} from 'src/app/helpers/app.static';
import {AuthService} from 'src/app/services/auth.service';
import {IClaim} from 'src/app/services/claims.const';
import {Subscriptionable} from 'src/app/common/subscriptionable';
import {SettingsService} from 'src/app/services/settings.service';
import {FilterControlComponent} from 'src/app/views/filter-control/filter-control.component';
import {ExportExcelService} from '../../../services/export-excel.service';
import {Observable} from 'rxjs';
import {viewClassName} from '@angular/compiler';
import {CardInlineComponent} from '../../cards/card-inline/card-inline.component';
import {ID_ADD_RECORD} from '../../cards/card.component';


// @Directive({
// selector: 'app-menu-base-list',
// templateUrl: './menu-base-list.component.html',
// styleUrls: ['./menu-base-list.component.css']
// })

export interface ISelectionInfo {
    selected: any[];
    isSelectedHasDeleted: boolean;
    isSelectedHasNoDeleted: boolean;
}

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class MenuBaseListComponent extends Subscriptionable implements CanDeactivate<any>, OnInit {
    @ViewChild('filterControl') filterControl: FilterControlComponent;
    @ViewChild('agGrid') agGrid: AgGridAngular;
    @ViewChild('detailedViewCard') detailedViewCard: CardInlineComponent;

    public descriptor: BaseTableDescriptor;

    public defaultColDef: {
        sortable: boolean,
        resizable: boolean,
        sortingOrder: ['asc', 'desc'],
        enableCellChangeFlash: true,
    };
    public columnDefs: IGridOptions[];

    public components: any;
    public actionList: ICardAction[];
    public subsystem: string;
    public isReadyForGrid: boolean;
    public filterInitData: any;
    public isFilterOpen = false;
    public totalCount?: number = undefined;

    public filterCtrlsD;
    public filterLayout;

    public detailedEntity?: ICardDescriptor = undefined;

    protected gridColumnApi: ColumnApi;
    protected gridApi: GridApi;

    // protected modalRef: BsModalRef;
    protected dataService: DataService;
    // protected modalService: BsModalService;
    protected router: Router;
    protected descrSrv: DescriptorsService;
    protected msgSrv: MessageService;
    protected route: ActivatedRoute;
    protected authSrv: AuthService;
    protected confirmSrv: ConfirmWindowService;
    protected errSrv: ErrorsService;
    protected settSrv: SettingsService;
    protected exportExcel: ExportExcelService;

    protected forsedRefresh: boolean;
    protected active_filters = {};
    protected _showDeleted = false;
    protected ldColumn: IGridOptions;

    constructor(
        private injector: Injector,
    ) {
        super();
        this.dataService = this.injector.get(DataService);
        this.router = this.injector.get(Router);
        this.route = injector.get(ActivatedRoute);
        this.descrSrv = injector.get(DescriptorsService);
        this.msgSrv = injector.get(MessageService);
        this.errSrv = injector.get(ErrorsService);
        this.confirmSrv = injector.get(ConfirmWindowService);

        this.authSrv = injector.get(AuthService);
        this.settSrv = injector.get(SettingsService);
        this.exportExcel = injector.get(ExportExcelService);

        this.actionList = this.getActionList();
        this.recalcActions();
        this.descriptor = this.getDescriptor();
        this.defaultColDef = this.getdefaultColDef();
        this.subsystem = '';
        if (this.route.snapshot.data) {
            this.subsystem = this.route.snapshot.data.subsystem;
        }
        this.isReadyForGrid = false;

        this.components = {
            loadingCellRenderer: function (params) {
                const val = params.getValue();
                if (val !== undefined) {
                    return val;
                } else {
                    // tslint:disable-next-line:max-line-length
                    return '<img *ngIf="dataLoading" src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" alt="loading"/>';
                }
            }
        };
    }

    public canDeactivate(): Promise<boolean> | boolean {
        if (!!this.detailedViewCard) {
            return this.detailedViewCard.canDeactivate();
        }
        // const changed = (this.cardForm && this.cardForm.isChanged);
        // if (changed && this.editMode) {
        //     return this.confirmSrv.confirm(CONFIRM_LEAVE_FORM).then(button => {
        //         return (button && button.result === BUTTON_RESULT_YES);
        //     });
        // }
        return true;
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.gridApi.sizeColumnsToFit();
    }

    ngOnInit() {
        this.settSrv.onAfterInit.subscribe(
                (value) => {
                    this.isReadyForGrid = value;

                    if (value) {
                        this.columnDefs = MenuPagesHelper.gridColumnsByDescriptor(this.descriptor);
                        this.ldColumn = this.columnDefs.find( c => c.field === FT_DELETED.key);
                        if (this.ldColumn) {
                            this.ldColumn.hide = true;
                        }
                        this.filterCtrlsD = this.getFilterControlDescriptions();
                        this.filterLayout = this.descriptor.filter_layout;
                        // subsc.unsubscribe();
                    }
                }
        );

        const subscrList = this.settingsSubscriptions();

        for (const path in subscrList) {
            if (subscrList.hasOwnProperty(path)) {
                const subscr = subscrList[path];
                this.subscribe(
                    this.settSrv.subscribeToValue(path, subscr)
                );
            }
        }

    }

    set showDeleted(value) {
        this._showDeleted = value;
        if (this.ldColumn && this.gridColumnApi) {
            this.gridColumnApi.setColumnVisible(this.ldColumn.field, value);
        }
    }
    get showDeleted() {
        return this._showDeleted;
    }

    public hasActions() {
        return this.actionList && this.actionList.length;
    }
    public getRowNodeId(item) {
        return item.id;
    }

    closeDetailView(saveData: any) {
        if (!AppUtils.isObjectEmpty(saveData)) {
            this.updateListFromSaveData(saveData, { entityName: this.descriptor. entityName, id: this.detailedEntity.id });
        }
        this.detailedEntity = undefined;
    }

    public onActionClick(button: ICardActionButton) {
         switch (button.action.id) {
            case E_ACTION_TYPE.add:
                this.addRecord();
                break;
            case E_ACTION_TYPE.refresh: {
                this.filterControl?.refresh();
                this.setDataSource(true);
                break;
            }
            case E_ACTION_TYPE.delete: {
                this.deleteRecords(this.gridApi.getSelectedRows());
                break;
            }
            case E_ACTION_TYPE.undelete: {
                this.undeleteRecords(this.gridApi.getSelectedRows());
                break;
            }
        }
        this.recalcActions();
    }

    public deleteRecords(data: any[]): Promise<any> {
        const rows = data.map(r => r.id);

        const confirmDelete: IConfirmWindow = Object.assign({}, CONFIRM_OP_DELETE);
        confirmDelete.bodyList = [];
        data.forEach( r =>
            confirmDelete.bodyList.push(
                this.descriptor.genRecordTitle(
                    { [this.descriptor.entityName]: r }, false)
            ));

        return this.confirmSrv.confirm(confirmDelete).then((confirm) => {
            if (confirm.result === BUTTON_RESULT_YES) {
                if (rows && rows.length) {

                    const promises: Promise<any>[] = [];

                    rows.forEach(row => {
                        const id: number | string = row;
                        promises.push(
                            this.dataService.deleteRecord(this.descriptor.entityName, id)
                        );
                    });
                    return Promise.all(promises).then(
                        (result) => {
                            const msg = new Message('Удаление', 'Запись удалена', MessageType.success);
                            this.msgSrv.addMessage(msg);
                            this.setDataSource();

                        }).catch(
                            (error) => {
                                return this.errSrv.passGlobalErrors(error).then(() => {
                                    this.setDataSource();
                                });
                            });
                }
            }
        });
    }

    public undeleteRecords(data: any[]): Promise<any> {
        const rows = data.map(r => r.id);

        const confirm: IConfirmWindow = Object.assign({}, CONFIRM_OP_UNDELETE);
        confirm.bodyList = [];
        data.forEach( r =>
            confirm.bodyList.push(
                this.descriptor.genRecordTitle(
                    { [this.descriptor.entityName]: r }, false)
            ));

        return this.confirmSrv.confirm(confirm).then((confirmResult) => {
            if (confirmResult.result === BUTTON_RESULT_YES) {
                if (rows && rows.length) {
                    const id: number | string = rows[0];
                    this.dataService.undeleteRecord(this.descriptor.entityName, id).then(
                        (result) => {
                            const msg = new Message('Восстановление', 'Запись восстановлена', MessageType.success);
                            this.msgSrv.addMessage(msg);
                            this.setDataSource();

                        }).catch(
                            (error) => {
                                return this.errSrv.passGlobalErrors(error).then(() => {
                                    this.setDataSource();
                                });
                            });
                }
            }
        });
    }
    public onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.agGrid.gridOptions.cacheBlockSize = 40;
        this.agGrid.cacheBlockSize = 40;
        this.setDataSource();
        this.gridApi.sizeColumnsToFit();
        this.agGrid.gridOptions.groupSelectsChildren = true;
        this.agGrid.sortingOrder =  ['desc', 'asc'];
    }

    public addRecord() {
        // this.router.navigateByUrl(this.descriptor.id + '/add');
        const card: ICardDescriptor = { entityName: this.descriptor.id, id: ID_ADD_RECORD};
        this.detailedEntity = card;
    }

    public onCellDoubleClicked(params) {
        if (params.data && !params.data.deleted) {
            this.openEntity(params.data);
        }
    }

    public onSelectionChanged(params) {
        if (!params || !params.data || !params.node.selected) {
            return ;
        }

        this.recalcActions();
    }

    public getTableStyle(): string {
        // return 'width: 100%; font-size: 14px; height: calc(100vh - 164px); ';
        return 'height: 100%; width: 100%; font-size: 14px;';
    }

    getTableRowHeight1() {
        return (params) => {
            return params.node.group ? 50 : 20;
        };
    }

    getTableRowHeight (): number {
        return 31;
    }

    public isFilterControlVisible(): boolean {
        // return false;
        return (this.descriptor.filter_layout && this.descriptor.filter_layout.length > 0);
    }

    public getFilterControlDescriptions(): IFieldDescriptor[] {
        return this.getDescriptor().fieldsFilter();
    }

    public filterOpenToggle(isOpen) {
        this.isFilterOpen = isOpen;
        // localStorage.setItem(STORAGE_FILTER_NAME + '-open-' + this.descriptor.id, String(isOpen));
    }

    public filterApply(filters) {

        // localStorage.setItem(STORAGE_FILTER_NAME + '-' + this.descriptor.id, JSON.stringify(filters));

        this.active_filters = AppUtils.deepCopy(filters);
        if (this.active_filters[FT_DELETED.key] !== undefined) {
            delete this.active_filters[FT_DELETED.key];
        }
        this.showDeleted = !!filters[FT_DELETED.key];
        if (this.gridApi && this.gridColumnApi) {
            this.showDeleted = !!filters[FT_DELETED.key];
            this.setDataSource();
        }

    }

    public abstract isUpdating(): boolean;

    protected getActionList(): ICardAction[] {
        const actions = [
            <ICardAction>{ id: E_ACTION_TYPE.add, title: 'Добавить' },
            <ICardAction>{ id: E_ACTION_TYPE.delete, title: 'Удалить',  },
            <ICardAction>{ id: E_ACTION_TYPE.undelete, title: 'Восстановить', hidden: true },
            <ICardAction>{ id: E_ACTION_TYPE.refresh, title: 'Обновить список' },
        ];
        actions.forEach (
            a => {
                a.permissions = this.actionPermissions(a.id);
            }
        );
        return actions;
    }

    protected settingsSubscriptions (): any { // {path: func ...}
        return {};
    }
    protected actionPermissions(id: E_ACTION_TYPE): IClaim[] {
        return [];
    }

    protected recalcActions() {
        const selected = this.gridApi ? this.gridApi.getSelectedRows() : [];
        const isSelectedHasDeleted = selected.find(s => s.deleted);
        const isSelectedHasNoDeleted = selected.find(s => !s.deleted);
        const selectionInfo: ISelectionInfo = {selected, isSelectedHasDeleted, isSelectedHasNoDeleted};

        this.actionList.forEach (a => {
            let hidden = false;
            if (a.permissions && a.permissions.length) {
                hidden = !this.authSrv.isClaimHave([a.permissions]); // all
            }

            if (a.id === E_ACTION_TYPE.delete) {
                hidden = hidden || !isSelectedHasNoDeleted || this.isActionHidden(a.id, selectionInfo);
            } else if (a.id === E_ACTION_TYPE.undelete) {
                hidden = hidden || !isSelectedHasDeleted || this.isActionHidden(a.id, selectionInfo);
            } else {
                hidden = hidden || this.isActionHidden(a.id, selectionInfo);
            }
            a.hidden = hidden;
        });
        return selectionInfo;
    }

    protected isActionHidden(id: E_ACTION_TYPE, selectionInfo: ISelectionInfo): boolean {
        return false;
    }

    protected getFilter() {
        return this.active_filters;
    }

    protected getDescriptor(): BaseTableDescriptor {
        const p = this.router.url.split('/');
        if (!p || p.length < 2) {
            return null;
        }
        return this.descrSrv.descriptorForEntity(p[1]);
    }

    protected getdefaultColDef(): any {
        return <IGridOptions>{
            sortable: true,
            resizable: true,
            sortingOrder:  ['desc', 'asc'],
            // cellClass: 'grid-cell'
            cellStyle: {
                'padding-left': '5px',
                'padding-right': '5px',
                'white-space': 'nowrap',
                'text-overflow': 'ellipsis',
                'line-height': '28px',
            },
            // cellStyle: {
            //     'align-items': 'center',
            //     display: 'flex',
            //     'padding-left': '5px',
            //     'padding-right': '5px',
            //     'white-space': 'nowrap',
            //     'text-overflow': 'ellipsis',
            //     ''
            //     // 'font-size': '14px',
            // },
        };
    }

    protected getOrderByKey(): IOrderByItemType[] {
        const sortCol = this.gridApi.getSortModel();
        if (sortCol && sortCol.length) {
            return sortCol.map( o => {
                return { field: o.colId,
                    direction:
                        o.sort === 'asc' ?  ISearchDirection.Asc :
                        o.sort === 'desc' ?  ISearchDirection.Desc :
                        undefined
                };
            });
        }
        return this.getUndefinedSort();
    }

    protected getUndefinedSort(): IOrderByItemType[] {
        return [{ field: FT_WEIGHT.key }];
    }

    protected setDataSource(forsedRefresh: boolean = true) {
        const dataService = this.dataService;
        const parentId = this.getParentId();
        const filter = this.getFilter();
        this.forsedRefresh = forsedRefresh;
        this.totalCount = undefined;
        this.gridApi.deselectAll();
        this.recalcActions();
        this.gridApi.setDatasource({
            // rowCont: null,
            getRows: function (params) {
                dataService.getPgList(this.getDescriptor().id,
                    this.getOrderByKey(),
                    filter,
                    <IListRange>{ offset: params.startRow, length: params.endRow - params.startRow, },
                    this.showDeleted,
                    this.forsedRefresh).then(result => {
                        this.totalCount = undefined;
                        if (result.total !== undefined) {
                            // this.gridApi.setVirtualRowCount(result.total)
                            this.totalCount = result.total;
                        }
                        if (result.items && result.items instanceof Array) {

                            if (result['result']) {
                                result = result['result'];
                            }
                            if (result.length < params.endRow - params.startRow) {
                                // lastRow = params.startRow + result.length;
                            }
                        }

                        params.successCallback(result.items, this.totalCount);
                    }).then(d => {
                        this.forsedRefresh = true; // пока отключил кеши - непонятно как обновлять пагинированное
                    });
            }.bind(this)
        });
    }

    protected openEntity(entity: any) {
        const idField = this.descriptor.keyField;
        // this.router.navigateByUrl(this.descriptor.id + '/' + entity[idField]);
        // this.router.navigate(['./' + entity[idField]], { relativeTo: this.route });
        const card: ICardDescriptor = { entityName: this.descriptor.id, id: entity[idField]};
        this.detailedEntity = card;
    }


    protected getParentId(): string {
        return null;
    }

    protected updateListFromSaveData(saveData: any, card?: ICardDescriptor) {
        const savedEntityObject = saveData[card.entityName];
        if (AppUtils.isObjectEmpty(savedEntityObject)) {
            return;
        }
        if ( savedEntityObject[this.descriptor.keyField] !== card.id) {
            return;
        }
        const id = savedEntityObject[this.descriptor.keyField];
        const node = this.gridApi.getRowNode(id);
        if (AppUtils.isObjectEmpty(node)) {
            return;
        }
        const mergedData = AppUtils.mergeObjects(node.data, savedEntityObject);
        node.updateData(mergedData);
    }
}
