import { IRegion } from '../../../../generated/types.graphql-gen';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { REGION_DESCRIPTOR } from 'src/app/model/datatypes/region.const';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';

@Component({
    selector: 'app-regions',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    // templateUrl: './regions.component.html',
    styleUrls: ['./regions.component.css']
})

export class RegionsComponent extends MenuBaseListComponent implements OnInit, OnDestroy {


    regions: IRegion[];


    public isUpdating(): boolean {
        return (!this.regions);
    }

    protected getDescriptor(): BaseTableDescriptor {
        return this.descrSrv.descriptorForEntity(REGION_DESCRIPTOR.id);
    }


}
