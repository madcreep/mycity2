import { Component } from '@angular/core';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { ICardAction } from 'src/app/views/cards/card.interfaces';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { APPOINTMENTRANGE_DESCRIPTOR } from 'src/app/model/datatypes/appointments-range.const';

@Component({
  selector: 'app-appointment-range',
  templateUrl: './../menu-base-list/menu-base-list.component.html',
    // templateUrl: './appointment-range.component.html',
  styleUrls: ['./appointment-range.component.css']
})
export class AppointmentRangeComponent extends MenuBaseListComponent {

    public isUpdating(): boolean {
        return false;
    }
    protected getDescriptor(): BaseTableDescriptor {
        return APPOINTMENTRANGE_DESCRIPTOR;
    }

    protected getActionList(): ICardAction[] {
        return [
            // <ICardAction>{ id: E_ACTION_TYPE.add,  title: 'Добавить' },
            // <ICardAction>{ id: E_ACTION_TYPE.delete,  title: 'Удалить' }
        ];
    }



}
