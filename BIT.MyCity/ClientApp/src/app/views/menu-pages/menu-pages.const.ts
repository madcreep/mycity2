import {REPORTS_DESCRIPTOR} from '../../model/datatypes/reports.const';
import {RegionsComponent} from './regions/regions.component';

import {Route} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {AuthGuard} from '../../guards/auth.guard';
import {HomeComponent} from '../../home/home.component';
import {TerritoriesComponent} from './territories/territories.component';
import {RolesComponent} from './roles/roles.component';
import {RubricsComponent} from './rubrics/rubrics.component';
import {AppealsCitizensListComponent as AppealsCitComponent} from './appeals-citizens/appeals-citizens.component';
import {TopicsComponent} from './topics/topics.component';
import {SettingsComponent} from './settings/settings.component';
import {AppointmentsPageComponent} from './appointments-page/appointments-page.component';
import {AppealsOrganizationsComponent} from './appeals-organizations/appeals-organizations.component';
import {APPEAL_UL_DESCRIPTOR} from '../../model/datatypes/appeals/appeal-organizations.const';
import {CanDeactivateGuard} from '../../guards/can-deactivate.guard';
import {VotesComponent} from './votes/votes.component';
import {TOPIC_DESCRIPTOR_C} from '../../model/datatypes/topic-citizens.const';
import {SubsystemsComponent} from './subsystems/subsystems.component';
import {SUBSYSTEM_ORG_ID, SUBSYSTEM_CITIZEN_ID} from '../../services/subsystems.service';
import {TOPIC_DESCRIPTOR_UL} from '../../model/datatypes/topic-organizations.cons';
import {
    CLAIM_MU_USERS,
    CLAIM_MU_ROLES,
    CLAIM_MU_REGIONS,
    CLAIM_SS_CITIZEN,
    CLAIM_MU_TERRITORIES,
    CLAIM_MU_RUBRICS,
    CLAIM_SS_ORGANIZATIONS,
    CLAIM_MU_TOPICS,
    CLAIM_SA_ADMIN,
    CLAIM_SS_NEWS,
    CLAIM_SS_CIT_EXECUTOR,
    CLAIM_MD_CITIZEN,
    CLAIM_SS_ORG_FBADMIN,
    CLAIM_SS_CIT_FBADMIN,
    CLAIM_MD_CIT_TOPICS,
    CLAIM_MD_ORG_TOPICS,
    CLAIM_MU_REPORTS
} from '../../services/claims.const';
import {VOTEROOTS_DESCRIPTOR} from '../../model/datatypes/vote-roots.const';
import {NewsComponent} from './news/news.component';
import {NEWS_DESCRIPTOR} from '../../model/datatypes/news.const';
import {FEEDBACK_DESCRIPTOR} from '../../model/datatypes/appeals/appeal-feedback.const';
import {FeedbackListComponent} from './feedback-list/feedback-list.component';
import {ENUMVERBS_DESCRIPTOR} from '../../model/datatypes/enumverbs.const';
import {EnumVerbsListComponent} from './enum-verbs/enum-verbs-list.component';
import {RUBRIC_ORG_DESCRIPTOR} from '../../model/datatypes/rubric-organisations.const';
import {APPEAL_CIT_DESCRIPTOR} from '../../model/datatypes/appeals/appeal-citizen.const';
import {APPEAL_CIT_EX_DESCRIPTOR} from '../../model/datatypes/appeals/appeal-citizen-executors.const';
import {AppealsCitizensExecutorComponent} from './appeals-citizens-executor/appeals-citizens-executor.component';
import {Subscription} from 'rxjs/internal/Subscription';
import {SettingsService} from '../../services/settings.service';
import {SETTINGS_SS_CIT_ENABLED, SETTINGS_SS_CIT_EXECUTOR, SETTINGS_SS_CIT_STED, SETTINGS_SS_NEWS_ENABLED, SETTINGS_SS_ORG_ENABLED, SETTINGS_SS_VOTE_ENABLED} from '../../services/setting-names.const';
import {AppUtils, InjectorInstance} from '../../helpers/app.static';
import {combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {ReportsListComponent} from './reports-list/reports-list.component';
import {EnvSettings} from '../../model/settings/envsettings';
import {CardComponent} from '../cards/card.component';
import {CardInlineComponent} from '../cards/card-inline/card-inline.component';
import {USER_DESCRIPTOR} from '../../model/datatypes/user.const';
import {TERRITORY_DESCRIPTOR} from '../../model/datatypes/territory.const';
import {REGION_DESCRIPTOR} from '../../model/datatypes/region.const';

export interface IMenuPage {
    id: string; // если нет link - это будет линком
    title?: string;
    titlePath?: string;
    link?: string;
    urllink?: string;
    class: string;
    component?: any;
    route?: Route;
    enabled?: boolean; // включить выключить секцию в реалтайме, например, подписка
    subscription?: () => Subscription;
}

export interface IMenuPageEdit extends IMenuPage {
    authorised: boolean;
}

function subscriptionsFor(obj, settings: string[]): Observable<any> {
    const settSrv = InjectorInstance.get(SettingsService);

    const arr: Observable<any>[] = settings.map((setting) => {
        return new Observable<any>(
            obs => {
                return settSrv.subscribeToValue(setting,
                    (value) => {
                        obs.next({[setting]: value});
                    }
                );
            }
        );
    });

    return combineLatest(arr).pipe(
        map((x) => {
            return AppUtils.mergeValues(x);
        })
    );
}

function subscriptionFor(obj, setting: string): Subscription {
    const settSrv = InjectorInstance.get(SettingsService);
    return settSrv.subscribeToValue(setting,
        (value) => obj.enabled = (value === 'true')
    );
}

export const MenuPages: IMenuPage[] = [
    {
        id: 'settings', title: 'Настройки', class: 'fa fa-cogs fa-fw',
        route: {
            component: SettingsComponent,
            data: {needClaims: [CLAIM_SA_ADMIN/*CLAIM_MU_SETTINGS*/]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
    }, {
        id: 'subsystems', title: 'Подсистемы', class: 'fa fa fa-superpowers fa-fw',
        route: {
            component: SubsystemsComponent,
            data: {needClaims: [CLAIM_SA_ADMIN/*CLAIM_MU_SETTINGS*/]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        }
    }, {
        id: 'users', title: 'Пользователи', class: 'fa fa-user-circle fa-fw',
        route: {
            component: UsersComponent,
            data: {needClaims: [CLAIM_MU_USERS]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
    }, {
        id: 'roles', title: 'Роли', class: 'fa fa-cubes fa-fw',
        route: {
            component: RolesComponent,
            data: {needClaims: [CLAIM_MU_ROLES]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
    }, {
        id: REPORTS_DESCRIPTOR.id, title: 'Отчеты', class: 'fa fa-cubes fa-fw',
        route: {
            component: ReportsListComponent,
            data: {needClaims: [CLAIM_MU_REPORTS]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
    }, {
        id: 'regions', titlePath: REGION_DESCRIPTOR.titlePath, class: 'fa fa-globe fa-fw',
        route: {
            component: RegionsComponent,
            data: {needClaims: [[CLAIM_MU_REGIONS, CLAIM_SS_CITIZEN]]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
        subscription() {
            return subscriptionFor(this, SETTINGS_SS_CIT_ENABLED);
        }
    }, {
        id: 'territories', titlePath: TERRITORY_DESCRIPTOR.titlePath, class: 'fa fa-map fa-fw',
        route: {
            component: TerritoriesComponent,
            data: {needClaims: [CLAIM_MU_TERRITORIES]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },

    }, {
        id: RUBRIC_ORG_DESCRIPTOR.id, titlePath: RUBRIC_ORG_DESCRIPTOR.titlePath, class: 'fa fa-cubes fa-fw',
        route: {
            component: RubricsComponent,
            data: {needClaims: [[CLAIM_MU_RUBRICS, CLAIM_SS_ORGANIZATIONS], [CLAIM_MU_RUBRICS, CLAIM_SS_CITIZEN]]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
    }, {
        id: TOPIC_DESCRIPTOR_C.id, title: 'Темы обращений', class: 'fa fa-list-ul fa-fw',
        route: {
            component: TopicsComponent,
            data: {
                needClaims: [[CLAIM_MU_TOPICS, CLAIM_SS_CITIZEN, CLAIM_MD_CIT_TOPICS]],
                subsystem: SUBSYSTEM_CITIZEN_ID,
                id: TOPIC_DESCRIPTOR_C.id,
            },
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
        subscription() {
            return subscriptionFor(this, SETTINGS_SS_CIT_ENABLED);
        }
    }, {
        id: TOPIC_DESCRIPTOR_UL.id, title: 'Темы для ЮЛ/ИП', class: 'fa fa-list-ul fa-fw',
        route: {
            component: TopicsComponent,
            data: {
                needClaims: [[CLAIM_MU_TOPICS, CLAIM_SS_ORGANIZATIONS, CLAIM_MD_ORG_TOPICS]],
                subsystem: SUBSYSTEM_ORG_ID,
                id: TOPIC_DESCRIPTOR_UL.id,
            },
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
        subscription() {
            return subscriptionFor(this, SETTINGS_SS_ORG_ENABLED);
        }
    }, {
        id: NEWS_DESCRIPTOR.id, title: NEWS_DESCRIPTOR.title, class: 'fa fa-newspaper-o fa-fw',
        route: {
            component: NewsComponent,
            data: {needClaims: [CLAIM_SS_NEWS]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
        subscription() {
            return subscriptionFor(this, SETTINGS_SS_NEWS_ENABLED);
        }
    }, {
        id: APPEAL_CIT_EX_DESCRIPTOR.id, title: APPEAL_CIT_EX_DESCRIPTOR.title, class: 'fa fa-user-circle fa-fw',
        route: {
            component: AppealsCitizensExecutorComponent,
            data: {needClaims: [[CLAIM_SS_CITIZEN, CLAIM_SS_CIT_EXECUTOR]]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
        subscription() {
            return subscriptionsFor(this, [SETTINGS_SS_CIT_ENABLED, SETTINGS_SS_CIT_EXECUTOR]).subscribe(
                settings => {
                    this.enabled = settings[SETTINGS_SS_CIT_ENABLED] === 'true' && settings[SETTINGS_SS_CIT_EXECUTOR] === 'true';
                }
            );
        }
    }, {
        id: APPEAL_CIT_DESCRIPTOR.id, title: APPEAL_CIT_DESCRIPTOR.title, class: 'fa fa-envelope-open fa-fw',
        route: {
            component: AppealsCitComponent,
            data: {needClaims: [[CLAIM_SS_CITIZEN, CLAIM_MD_CITIZEN]]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
        subscription() {
            return subscriptionFor(this, SETTINGS_SS_CIT_ENABLED);
        }
    }, {
        id: APPEAL_UL_DESCRIPTOR.id, title: APPEAL_UL_DESCRIPTOR.title, class: 'fa fa-users fa-fw',
        route: {
            component: AppealsOrganizationsComponent, data: {needClaims: [CLAIM_SS_ORGANIZATIONS]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
        subscription() {
            return subscriptionFor(this, SETTINGS_SS_ORG_ENABLED);
        }
    }, {
        id: FEEDBACK_DESCRIPTOR.id, title: FEEDBACK_DESCRIPTOR.title, class: 'fa fa-commenting-o fa-fw',
        route: {
            component: FeedbackListComponent, data: {
                debug: 1,
                needClaims: [
                    [CLAIM_SS_ORGANIZATIONS, CLAIM_SS_ORG_FBADMIN],
                    [CLAIM_SS_CITIZEN, CLAIM_SS_CIT_FBADMIN]]
            },
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
    }, {
        id: 'appointments', title: 'Запись на прием', class: 'fa fa-calendar fa-fw',
        route: {component: AppointmentsPageComponent, data: {needClaims: [CLAIM_SA_ADMIN,]}, canActivate: [AuthGuard],},
    }, {
        //     id: PROTOCOL_DESCRIPTOR.id, title: PROTOCOL_DESCRIPTOR.title, class: 'fa fa-calendar fa-fw',
        //     route: { component: ProtocolsComponent, data: { needClaims: [CLAIM_SA_ADMIN, ] }, canActivate: [AuthGuard], },
        // }, {
        id: VOTEROOTS_DESCRIPTOR.id, title: VOTEROOTS_DESCRIPTOR.title, class: 'fa fa-hand-paper-o fa-fw',
        route: {
            component: VotesComponent,
            data: {needClaims: [CLAIM_SA_ADMIN] },
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        },
        subscription() {
            return subscriptionFor(this, SETTINGS_SS_VOTE_ENABLED);
        }
        // }, {
        //     id: 'source-testing', title: 'Тестирование', class: 'fa fa-user-circle fa-fw',
        //     route: { component: SourceTestingComponent, canActivate: [AuthGuard], data: { needClaims: [CLAIM_SA_ADMIN] }, }
    }, {
        id: ENUMVERBS_DESCRIPTOR.id, title: ENUMVERBS_DESCRIPTOR.title, class: 'fa fa-external-link-square fa-fw',
        route: {
            component: EnumVerbsListComponent,
            data: {needClaims: [CLAIM_SA_ADMIN]},
            canActivate: [AuthGuard],
            canDeactivate: [CanDeactivateGuard],
        }
    },
];

export const MenuPagesRoutes = MenuPages.map(r => {
    const route = r.route || {path: 'home', component: HomeComponent};
    if (!r.route && !r.link) {
        r.link = route.path;
    }

    if (!route.path) {
        route.path = r.link || r.id;
    }
    return route;
});
