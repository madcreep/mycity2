import {ITerritory} from '../../../../generated/types.graphql-gen';
import {ICardAction} from '../../cards/card.interfaces';
import {Component, OnInit} from '@angular/core';
import {MenuBaseListComponent} from '../menu-base-list/menu-base-list.component';
import {E_ACTION_TYPE} from 'src/app/model/datatypes/descriptors.interfaces';
import {BaseTableDescriptor} from 'src/app/model/datatypes/descriptors.const';
import {TERRITORY_DESCRIPTOR} from 'src/app/model/datatypes/territory.const';
import {InjectorInstance} from 'src/app/helpers/app.static';
import {SETTINGS_TERRITORY_ADDEN} from 'src/app/services/setting-names.const';
import {SettingsService} from 'src/app/services/settings.service';

@Component({
    selector: 'app-territories',
    // templateUrl: './territories.component.html',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./territories.component.css']
})

export class TerritoriesComponent extends MenuBaseListComponent implements OnInit {

    territories: ITerritory[];
    private _isAddEnabled: boolean;

    ngOnInit() {
        super.ngOnInit();
        this.subscribe(
            this.settSrv.subscribeToValue(SETTINGS_TERRITORY_ADDEN,
                (value) => {
                    this._isAddEnabled = value === 'true';
                }
            )
        );
    }

    public isUpdating(): boolean {
        return !this.territories;
    }

    protected getDescriptor(): BaseTableDescriptor {
        return TERRITORY_DESCRIPTOR;
    }

    protected isActionHidden(id: E_ACTION_TYPE, selectionInfo: any): boolean {
        if (id === E_ACTION_TYPE.add) {
            return !this._isAddEnabled;
        }
        return super.isActionHidden(id, selectionInfo);
    }
}
