import { Component, OnDestroy, OnInit, Injector } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';

@Component({
    selector: 'app-topics',
    // templateUrl: './topics.component.html',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./topics.component.css']
})

export class TopicsComponent extends MenuBaseListComponent implements OnInit, OnDestroy {

    constructor (
        injector: Injector,
    ) {
        super(injector);

    }

    public isUpdating(): boolean {
        return false; // (!this.topics);
    }

    protected getDescriptor(): BaseTableDescriptor {
        return this.descrSrv.descriptorForEntity(this.route.snapshot.routeConfig.path);
    }

    protected getFilter() {
        const value = super.getFilter();
        value['subsystem.uID'] = this.subsystem;
        return value;
    }

}
