import { ITestRec, EStage, TestFn, ISubTestResult, ITestResults } from '../testing.interfaces';
import { GraphQLClient } from 'src/app/data-services/graphql/graphql.client';
import { InjectorInstance } from 'src/app/helpers/app.static';

export abstract class BaseTestClass implements ITestRec {

    abstract title: string;
    abstract func: TestFn;
    state: EStage;
    subs: ISubTestResult[];
    result?: any;
    isExpanded: boolean;

    protected _gqlSdk: GraphQLClient;
    constructor(
    ) {
        const injector = InjectorInstance;
        this._gqlSdk = injector.get(GraphQLClient);
        this.clearTest();
    }

    clearTest() {
        this.subs = [];
        this.state = EStage.wait;
    }

    subResult(isPassed: boolean, message: string, obj: any = {}) {
        this.addSub({state: !isPassed ? EStage.failed : EStage.passed, message: message, obj: obj });
    }

    subResultPromise(message: string, prms: () => Promise<any> ) {
        return Promise.resolve(prms()).then (d => {
            this.subResult(true, message, d);
        }).catch (err => {
            this.subResult(false, message, err);
        });
    }

    subResultCatchPromise(message: string, prms: () => Promise<any> ) {
        return Promise.resolve(prms()).then (d => {
            this.subResult(false, message, d);
        }).catch (err => {
            this.subResult(true, message, err);
        });
    }


    addSub(sub: ISubTestResult, throwOnFailed = true) {
        this.subs.push(sub);
        if (this.state !== EStage.failed && sub.state === EStage.failed) {
            this.state = EStage.failed;
            throw <ITestResults>{tests: this.subs, result: this.state, failed: sub};
        }
    }

    returnResults( error: any = null): ITestResults {
        return { tests: this.subs, result: this.state, };
    }

    catcherFn (error) {
        return Promise.reject(
            Object.assign (<ITestResults>{}, this.returnResults(), { lasterror: error.failed || error } )
        );
    }


    baseGQLPromise(prms: () => Promise<any>) {
        return prms().then( data => {
            this.subResult(!!data, 'ответ gql присутствует (запрос корректен)', data);
            this.subResult(!data.errors, 'в ответе нет ошибок GQL', data.errors);
            const answer = data.data;
            this.subResult(!!answer, 'Данные ответа присутствуют', data);
            return answer;
        }).catch (error => {
            this.subResult(false, 'Результат запроса', error);
        });
    }

}
