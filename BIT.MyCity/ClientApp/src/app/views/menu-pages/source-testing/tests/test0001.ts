import { BaseTestClass } from './base-test';

export class TestUpdateToken extends BaseTestClass {

    title = 'Корректность graphql - updateToken';
    func = () => {
        return this._gqlSdk.updateToken().toPromise().then(data => {
            this.subResult(!!data, 'updateToken - ответ присутствует', data);
            this.subResult(!data.errors, 'updateToken - в ответе нет ошибок GQL', data);
            this.subResult(!!data.data, 'ответ имеет data GQL', data);
            this.subResult(!!data.data.updateToken, 'updateToken - объект updateToken', data.data);

            const t = data.data.updateToken;
            this.subResult(t.loginStatus === 'Ok', 'updateToken - loginStatus Ok', t);
            this.subResult(t.token && t.token.startsWith('Bearer '), 'Наличие токена Bearer', t);
            // TODO: [MYC-230] в updateToken нет данных пользователя
            this.subResult(!!t.user, 'Наличие данных о пользователе MYC-230', t);
            return Promise.resolve(this.returnResults());
        }).catch ((error) => this.catcherFn(error));
    }
}
