import { BaseTestClass } from './base-test';

export class Test00002createTerritory extends BaseTestClass {

    title = 'Территории - создание и удаление';
    func = () => {
        let answer: any = {};
        return this.baseGQLPromise( () => this._gqlSdk.test00002createTerritory().toPromise()).then(answ => {
            answer = answ;
            this.subResult(!!(answer.ct1 && answer.ct1.id), 'id1 присутствует (запись создана)', answer);
            const id1 = answer.ct1.id;
            this.subResult(!!answer.ct1.weight, 'Вес присутствует и не равен нулю()', answer);

            return this.subResultPromise(
                'updateTerritory name успешно',
                () => this._gqlSdk.test00002updateTerritory({ id: id1}).toPromise());
        }).then (() => {
            const id1 = answer.ct1.id;
            return this.subResultPromise(
                'id1 удален корректно',
                 () => this._gqlSdk.deleteRecordByID('territory', id1) );
        }).then (() => {
            this.subResult(!!(answer.ct2 && answer.ct2.id), 'id2 присутствует (запись создана)', answer);
            this.subResult(!!answer.ct2.weight, 'Вес присутствует и не равен нулю()', answer);
            this.subResult(answer.ct2.weight > answer.ct1.weight, 'Веса разные и вес2 > вес1', answer);

            const id2 = answer.ct2.id;
            return this.subResultPromise(
                'id2 удален корректно',
                () => this._gqlSdk.deleteRecordByID('territory', id2));


        }).then (() => {
            return Promise.resolve(this.returnResults());
        })
        .catch ((error) => this.catcherFn(error));
    }

}



