import { BaseTestClass } from './base-test';
import { FT_WEIGHT } from 'src/app/model/datatypes/fields.const';
import { IFilterNextOpEnum } from 'src/generated/types.graphql-gen';

export class Test00005Users extends BaseTestClass {

    title = 'Тестирование таблицы пользователей';

    func = () => {
        return this.baseGQLPromise(() => this._gqlSdk.getUserList().toPromise()).then(answer => {

            this.subResult(!!(answer), 'Список всех юзеров читается', answer);

            return this.subResultPromise(
                'Список юзеров с заданным range',
                () => this._gqlSdk.getUserList({ range: { skip: 0, take: 100, } }).toPromise());

        }).then(() => {
            return this.subResultPromise(
                'Список юзеров с сортировкой по имени',
                () => this._gqlSdk.getUserList({ range: { orderBy: [{ field: 'fullName' }] } }).toPromise());
        }).then(() => {
            return this.subResultPromise(
                'Список юзеров с фильтром по клайму',
                () => this._gqlSdk.getUserList({ filters: [{name: 'allClaims.type', in: ['mu', 'md', ], } ]}).toPromise());

        }).then(() => {
        }).then(() => {
        }).then(() => {

        }).then(() => {
            return Promise.resolve(this.returnResults());
        }).catch((error) => this.catcherFn(error));
    }

}



