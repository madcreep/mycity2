import { BaseTestClass } from './base-test';
import { FT_WEIGHT } from 'src/app/model/datatypes/fields.const';

export class Test00003Roles extends BaseTestClass {

    title = 'Тестирование таблицы ролей';
    func = () => {
        return this.baseGQLPromise(() => this._gqlSdk.getRoleList().toPromise()).then(answer => {

            this.subResult(!!(answer), 'Роли читаются', answer);

            return this.subResultPromise(
                'Роли с заданным range',
                () => this._gqlSdk.getRoleList( { range: { skip: 0, take: 100, } }).toPromise());
        }).then(() => {
            return this.subResultPromise(
                'Роли с сортировкой по весу',
                () => this._gqlSdk.getRoleList( { range: { orderBy: [{ field: FT_WEIGHT.key }] } }
            ).toPromise() );
        // }).then(() => {

        }).then(() => {
            return Promise.resolve(this.returnResults());
        }).catch((error) => this.catcherFn(error));


    }
}


