import { BaseTestClass } from './base-test';
import { FT_WEIGHT } from 'src/app/model/datatypes/fields.const';
import { ICreateAppeal, IAppeal, IMessage } from 'src/generated/types.graphql-gen';
import { SUBSYSTEM_ORG_ID } from 'src/app/services/subsystems.service';

export class Test00004Appeals extends BaseTestClass {

    title = 'Тестирование таблицы обращений';
    read1;
    func = () => {
        let created_appeal: any;
        let created_message1: IMessage;
        return this.baseGQLPromise(() => this._gqlSdk.getAppealList().toPromise()).then(answer => {

            this.subResult(!!(answer), 'Список обращений читается', answer);

            return this.subResultPromise(
                'Список обращений с заданным range',
                () => this._gqlSdk.getAppealList({ range: { skip: 0, take: 100, } }).toPromise());

        }).then(() => {
            return this.subResultPromise(
                'Список обращений с сортировкой по весу',
                () => this._gqlSdk.getAppealList({ range: { orderBy: [{ field: FT_WEIGHT.key }] } }).toPromise());
        }).then(() => {
            return this.subResultCatchPromise(
                'Создание временного обращения без подсистемы (должна быть ошибка)',
                () => this._gqlSdk.createAppeal(
                    { appeal: <ICreateAppeal>{ description: 'asdgsadg', siteStatus: 1 } }
                ).toPromise().then(data => {
                    // created_appeal = data.data.createAppeal;
                }),
            );
        }).then(() => {
            return this.subResultPromise(
                'Создание временного обращения c подсистемой',
                () => this._gqlSdk.createAppeal(
                    { appeal: <ICreateAppeal>{ description: 'asdgsadg', siteStatus: 1, subsystemId: SUBSYSTEM_ORG_ID } }
                ).toPromise().then(data => {
                    created_appeal = data.data.createAppeal;
                }),
            );
        }).then(() => {
            return this.subResultPromise(
                'Создание первого комментария',
                () => this._gqlSdk.createMessage2Appeal (
                    { parentId: created_appeal.id, text: 'Текстовое сообщение 1'}
                ).toPromise().then(data => {
                    created_message1 = <IMessage>data.data.createMessage;
                }),
            );
        }).then(() => {
            return this.subResultPromise(
                'Чтение списка сообщений messageList',
                () => this._gqlSdk.messageList(
                    { id: created_appeal.id , type: 'Appeal'}
                ).toPromise().then(answer => {
                    this.read1 = answer;
                }),
            );
        }).then(() => {
            this.subResult(this.read1.data.messageList.total === 1, 'прочтено одно сообщение', this.read1);
            this.subResult(this.read1.data.messageList.items[0].author !== null, 'Автор есть', this.read1);
        }).then(() => {
            return this.subResultPromise(
                'Удаление временного обращения',
                () => this._gqlSdk.deleteRecordByID('appeal', created_appeal.id)
            );
        }).then(() => {

        }).then(() => {
            return Promise.resolve(this.returnResults());
        }).catch((error) => this.catcherFn(error));
    }

}



