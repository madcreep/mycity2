import { Component, OnInit } from '@angular/core';
import { EStage, ITestRec } from './testing.interfaces';
import { TestUpdateToken } from './tests/test0001';
import { BaseTestClass } from './tests/base-test';
import { Test00002createTerritory } from './tests/test0002_territory';
import { TERRITORY_DESCRIPTOR } from 'src/app/model/datatypes/territory.const';
import { REGION_DESCRIPTOR } from 'src/app/model/datatypes/region.const';
import { APPEAL_CIT_DESCRIPTOR } from 'src/app/model/datatypes/appeals/appeal-citizen.const';
import { APPOINTMENT_DESCRIPTOR } from 'src/app/model/datatypes/appointment.const';
import { APPOINTMENTRANGE_DESCRIPTOR } from 'src/app/model/datatypes/appointments-range.const';
import { VOTE_DESCRIPTOR } from 'src/app/model/datatypes/vote.const';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { ICardAction, ICardActionButton } from 'src/app/views/cards/card.interfaces';
import { GraphQLClient } from 'src/app/data-services/graphql/graphql.client';
import { Test00003Roles } from './tests/test0003-roles';
import { Test00004Appeals } from './tests/test0004-appeal';
import { Test00005Users } from './tests/test0005-users';
import { SUBSYSTEM_CITIZEN_ID } from 'src/app/services/subsystems.service';
import { TOPIC_DESCRIPTOR_C } from 'src/app/model/datatypes/topic-citizens.const';
import { RUBRIC_ORG_DESCRIPTOR } from 'src/app/model/datatypes/rubric-organisations.const';


@Component({
    selector: 'app-source-testing',
    templateUrl: './source-testing.component.html',
    styleUrls: ['./source-testing.component.css']
})
export class SourceTestingComponent implements OnInit {

    stages = EStage;
    tests: BaseTestClass[] = [
        new TestUpdateToken(),
        new Test00002createTerritory(),
        new Test00003Roles(),
        new Test00004Appeals(),
        new Test00005Users(),
    ];

    public actionList: ICardAction[] = [
        { id: 1, title: 'Тестирование', },
        { id: 2, title: 'Генерация данных', },
    ];

    generators: any[] = [
        REGION_DESCRIPTOR,
        TERRITORY_DESCRIPTOR,
        Object.assign({}, RUBRIC_ORG_DESCRIPTOR, {title: 'Рубрики'}),
        TOPIC_DESCRIPTOR_C,
        APPEAL_CIT_DESCRIPTOR,
        APPOINTMENT_DESCRIPTOR,
        APPOINTMENTRANGE_DESCRIPTOR,
        VOTE_DESCRIPTOR,
    ];

    genButtonsVisible = false;

    activePage: any = 1;
    genLog: string;

    constructor(
        private _gqlSdk: GraphQLClient,
    ) {

    }

    ngOnInit() {
        if (!('toJSON' in Error.prototype)) {
            Object.defineProperty(Error.prototype, 'toJSON', {
                value: function () {
                    const alt = {};

                    Object.getOwnPropertyNames(this).forEach(function (key) {
                        alt[key] = this[key];
                    }, this);

                    return alt;
                },
                configurable: true,
                writable: true
            });
        }
    }

    public onPageClick(button: ICardActionButton) {
        this.activePage = button.action.id;
    }

    generateData(descr: BaseTableDescriptor) {
        this.genLog = '';
        const count = 100;
        if (descr.id === REGION_DESCRIPTOR.id) {

            let generator: Promise<any> = Promise.resolve();

            for (let i = 0; i < count; i++) {
                const str = Math.random().toString(36).substring(2) + ' ' + Math.random().toString(36).substring(2);
                generator = generator.then ( () => {
                    return this._gqlSdk.createRegion({ region: { name: str, shortName: str }}).toPromise();
                }).then (data => {
                    const region = data.data.createRegion;
                    this.genLog += 'create: id: ' + region.id + ' name: "' + region.name + '" \n';
                });

            }
        }

        if (descr.id === TERRITORY_DESCRIPTOR.id) {

            let generator: Promise<any> = Promise.resolve();

            for (let i = 0; i < count; i++) {
                const str = Math.random().toString(36).substring(2) + ' ' + Math.random().toString(36).substring(2);
                generator = generator.then ( () => {
                    return this._gqlSdk.createTerritory({ territory: { name: str }}).toPromise();
                }).then (data => {
                    const obj = data.data.createTerritory;
                    this.genLog += 'create: id: ' + obj.id + ' name: "' + obj.name + '" \n';
                });

            }
        }

        if (descr.id === RUBRIC_ORG_DESCRIPTOR.id) {

            let generator: Promise<any> = Promise.resolve();

            for (let i = 0; i < count; i++) {
                const str = Math.random().toString(36).substring(2) + ' ' + Math.random().toString(36).substring(2);
                generator = generator.then ( () => {
                    return this._gqlSdk.createRubric({ rubric: { name: str }}).toPromise();
                }).then (data => {
                    const obj = data.data.createRubric;
                    this.genLog += 'create: id: ' + obj.id + ' name: "' + obj.name + '" \n';
                });

            }
        }

        if (descr.id === TOPIC_DESCRIPTOR_C.id) {

            let generator: Promise<any> = Promise.resolve();

            for (let i = 0; i < count; i++) {
                const str = Math.random().toString(36).substring(2) + ' ' + Math.random().toString(36).substring(2);
                generator = generator.then ( () => {
                    return this._gqlSdk.createTopic({ topic: { name: str, subsystemId: SUBSYSTEM_CITIZEN_ID}}).toPromise();
                }).then (data => {
                    const obj = data.data.createTopic;
                    this.genLog += 'create: id: ' + obj.id + ' name: "' + obj.name + '" \n';
                });

            }
        }
    }

    expand(item: ITestRec) {
        item.isExpanded = !item.isExpanded;
    }

    startAll() {
        this.tests.forEach(test => {
            test.clearTest();
            test.state = EStage.wait;
            test.isExpanded = true;
        });

        this.tests.forEach(test => {
            test.state = EStage.progress;
            Promise.resolve(test.func()).then(result => {
                test.state = EStage.passed;
                test.result = result;
            }).catch(err => {
                err.lastquery = '';
                if (err.lasterror) {
                    if (err.lasterror.obj &&
                        err.lasterror.obj.graphQLErrors &&
                        err.lasterror.obj.graphQLErrors.operation
                        ) {
                        const gql_query = err.lasterror.obj.graphQLErrors.operation.query.loc.source.body;
                        // const a = err.lasterror.obj.graphQLErrors.operation.getContext();
                        // console.log('SourceTestingComponent -> startAll -> gql_query', gql_query);
                        err.lastquery = 'query: \n' + gql_query + '\n'
                         + '\nvariables: ' + JSON.stringify(err.lasterror.obj.graphQLErrors.operation.variables) + '\n';
                        test.result = err.lastquery + '\n' + JSON.stringify(err.lasterror.obj.message) + '\n' +
                        JSON.stringify(err.lasterror.obj.stack);
                    } else {
                        test.result = err.lastquery + JSON.stringify(err.lasterror);
                    }

                } else if (err.message) {
                    test.result = 'message: ' + err.message;
                    if (err.stack) {
                        test.result += '\nstack: ' + err.stack;
                    }

                } else if (err.toJSON) {
                    test.result = JSON.stringify(err.toJSON());
                } else {
                    test.result = JSON.stringify(err);
                }

                test.state = EStage.failed;
            });
        });
    }

}
