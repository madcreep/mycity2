
export enum EStage {
    wait = 'wait',
    progress = 'in progress',
    failed = 'failed',
    passed = 'passed'
}

export interface ISubTestResult {
    state: EStage;
    message?: string;
    obj?: string;

}

export interface ITestResults {
    tests: ISubTestResult[];
    result?: EStage;
    lasterror?: any;
}

export declare type TestFn = () => Promise<ITestResults>;

export interface ITestRec {
    state: EStage;
    title: string;
    func: TestFn ;
    result?: any;
    isExpanded: boolean;
}
