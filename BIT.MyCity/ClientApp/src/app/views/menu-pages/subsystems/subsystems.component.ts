import { Component, OnInit } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { SUBSYSTEM_DESCRIPTOR } from 'src/app/model/datatypes/subsystem.const';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';

@Component({
    selector: 'app-subsystems',
    // templateUrl: './subsystems.component.html',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./subsystems.component.css']
})
export class SubsystemsComponent extends MenuBaseListComponent {

    public isUpdating(): boolean {
        return true;
    }

    public getRowNodeId(item) {
        return item.uID;
    }

    protected getDescriptor(): BaseTableDescriptor {
        return this.descrSrv.descriptorForEntity(SUBSYSTEM_DESCRIPTOR.id);
    }

    protected isActionHidden(id: E_ACTION_TYPE, selectionInfo: any): boolean {
        if (id === E_ACTION_TYPE.add) {
            return true;
        }
        return super.isActionHidden(id, selectionInfo);
    }

}
