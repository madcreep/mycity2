import { Component } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { ISearchDirection, IOrderByItemType } from 'src/generated/types.graphql-gen';
import { ENUMVERBS_DESCRIPTOR } from 'src/app/model/datatypes/enumverbs.const';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { FT_SUBSYSTEMUID } from 'src/app/model/datatypes/fields.const';

@Component({
    selector: 'app-enum-verbs-list',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./enum-verbs-list.component.scss']
})
export class EnumVerbsListComponent extends MenuBaseListComponent {


    public isUpdating(): boolean {
        return false;
    }
    // protecte
    // d getUndefinedSort() {
    //     return [{ field: 'state', direction: ISearchDirection.Asc }, { field: FT_ID.key, direction: ISearchDirection.Desc }];
    // }

    protected getDescriptor(): BaseTableDescriptor {
        return ENUMVERBS_DESCRIPTOR;
    }

    // protected getActionList(): ICardAction[] {
    //     return [
    //         <ICardAction>{ id: E_ACTION_TYPE.add, title: 'Добавить' },
    //         <ICardAction>{ id: E_ACTION_TYPE.delete, title: 'Удалить' },
    //         <ICardAction>{ id: E_ACTION_TYPE.refresh, title: 'Обновить список' },
    //         <ICardAction>{ id: E_ACTION_TYPE.start, title: 'Запустить' },
    //         <ICardAction>{ id: E_ACTION_TYPE.stop, title: 'Остановить' },
    //     ];
    // }

    // , direction: ISearchDirection.Desc
    protected getUndefinedSort(): IOrderByItemType[] {
        return [
            { field: FT_SUBSYSTEMUID.key, },
            { field: 'group' },
            { field: 'value' },
        ];
    }
}
