import { Component, OnInit } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { IAppeal, IOrderByItemType, ISearchDirection, IAppealTypeEnum } from 'src/generated/types.graphql-gen';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { APPEAL_UL_DESCRIPTOR } from 'src/app/model/datatypes/appeals/appeal-organizations.const';
import { SUBSYSTEM_ORG_ID } from 'src/app/services/subsystems.service';
import { FT_ID } from 'src/app/model/datatypes/fields.const';
import { IClaim, CLAIM_SS_ORG_APPEAL } from 'src/app/services/claims.const';
import { SETTINGS_SS_ORG_ADDEN, SETTINGS_SS_ORG_DELEN } from 'src/app/services/setting-names.const';

@Component({
    selector: 'app-appeals-organizations',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./appeals-organizations.component.scss']
})




export class AppealsOrganizationsComponent extends MenuBaseListComponent implements OnInit {

    appeals: IAppeal[];
    _isAddEnabled: boolean;
    _isDelEnabled: boolean;

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.agGrid.gridOptions.sortingOrder =  ['desc', 'asc'];
        this.setDataSource();
        this.gridApi.sizeColumnsToFit();
    }


    public isUpdating(): boolean {
        return !this.appeals;
    }

    isFilterControlVisible() {
        return true;
    }

    protected settingsSubscriptions () {
        return {
            [SETTINGS_SS_ORG_DELEN]: (value) => {this._isDelEnabled = value === 'true'; },
            [SETTINGS_SS_ORG_ADDEN]: (value) => {this._isAddEnabled = value === 'true'; }
        };
    }

    protected getUndefinedSort(): IOrderByItemType[] {
        return [{ field: FT_ID.key, direction: ISearchDirection.Desc }];
    }

    protected getFilter() {
        const value = super.getFilter();
        value['subsystem.uID'] = SUBSYSTEM_ORG_ID;
        value['appealType'] = IAppealTypeEnum.Normal;
        return value;
    }
    protected getDescriptor(): BaseTableDescriptor {
        return this.descrSrv.descriptorForEntity(APPEAL_UL_DESCRIPTOR.id);
    }

    protected actionPermissions(id: E_ACTION_TYPE): IClaim[] {
        switch (id) {
            case E_ACTION_TYPE.delete:
            case E_ACTION_TYPE.add:
                return [CLAIM_SS_ORG_APPEAL];
        }
        return [];
    }

    protected isActionHidden(id: E_ACTION_TYPE, selectionInfo: any): boolean {
        if (id === E_ACTION_TYPE.add) {
            return !this._isAddEnabled;
        } else if (id === E_ACTION_TYPE.delete) {
            return !this._isDelEnabled;
        }
        return super.isActionHidden(id, selectionInfo);
    }
}
