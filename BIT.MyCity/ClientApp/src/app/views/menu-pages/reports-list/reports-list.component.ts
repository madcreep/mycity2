import { REPORTS_DESCRIPTOR } from '../../../model/datatypes/reports.const';
import { ICardAction } from '../../cards/card.interfaces';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { ICardActionButton } from 'src/app/views/cards/card.interfaces';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { IListRange } from 'src/app/services/data.service';

@Component({
    selector: 'app-reports-list',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./reports-list.component.scss']
})

export class ReportsListComponent extends MenuBaseListComponent implements OnInit, OnDestroy {

    ngOnInit(): void {
        super.ngOnInit();
    }

    public isUpdating(): boolean {
        return false;
    }

    public getRowNodeId(item) {
        return item.reportId;
    }
    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.setDataSource();
        this.gridApi.sizeColumnsToFit();
    }
    public onActionClick(button: ICardActionButton) {

    }
    protected getdefaultColDef() {
        return [];
    }

    // protected getUndefinedSort(): IOrderByItemType[] {
    // return [{ field: FT_ID.key, direction: ISearchDirection.Desc }];
    // }
    protected getActionList(): ICardAction[] {
        return [
            <ICardAction>{ id: E_ACTION_TYPE.refresh, title: 'Обновить список' },
        ];
    }

    protected getDescriptor(): BaseTableDescriptor {
        return REPORTS_DESCRIPTOR;
    }

    protected override setDataSource(forsedRefresh: boolean = true) {
        const dataService = this.dataService;
        const parentId = this.getParentId();
        const filter = this.getFilter();
        this.forsedRefresh = forsedRefresh;
        this.gridApi.deselectAll();
        this.recalcActions();
        this.gridApi.setDatasource({
            getRows: function (params) {
                dataService.getPgList(this.getDescriptor().id,
                    this.getOrderByKey(),
                    filter,
                    <IListRange>{ offset: params.startRow, length: params.endRow - params.startRow, },
                    this.showDeleted,
                    this.forsedRefresh).then(result => {
                        this.totalCount = result?.length ?? -1;
                        params.successCallback(result, this.totalCount);
                    }).then(d => {
                        this.forsedRefresh = true; // пока отключил кеши - непонятно как обновлять пагинированное
                    });
            }.bind(this)
        });
    }
}
