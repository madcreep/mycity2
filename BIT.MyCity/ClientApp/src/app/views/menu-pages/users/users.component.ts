import { ICardAction, ICardActionButton } from '../../cards/card.interfaces';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { USER_DESCRIPTOR } from 'src/app/model/datatypes/user.const';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { IUser, IOrderByItemType, ISearchDirection } from 'src/generated/types.graphql-gen';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { FT_NAME, FT_ID } from 'src/app/model/datatypes/fields.const';
import { AppUtils } from 'src/app/helpers/app.static';
import { IConfirmWindow } from 'src/app/common/confirm-window/confirm-window.component';
import { BUTTON_RESULT_CANCEL, BUTTON_RESULT_YES } from 'src/app/consts/confirmations.const';
import { Message, MessageType } from 'src/app/services/message';

const CONFIRM_OP_BLOCK: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Вы действительно хотите заблокировать выбранного пользователя?',
    bodyAfterList: 'Продолжить?',
    buttons: [
        { title: 'Отмена', result: BUTTON_RESULT_CANCEL, },
        { title: 'Заблокировать', result: BUTTON_RESULT_YES, isDefault: true, },
    ],
};

const CONFIRM_OP_UNBLOCK: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Вы действительно хотите разблокировать выбранного пользователя?',
    bodyAfterList: 'Продолжить?',
    buttons: [
        { title: 'Отмена', result: BUTTON_RESULT_CANCEL, },
        { title: 'Разблокировать', result: BUTTON_RESULT_YES, isDefault: true, },
    ],
};


@Component({
    selector: 'app-users',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./users.component.scss']
})

export class UsersComponent extends MenuBaseListComponent implements OnInit, OnDestroy {

    users: IUser[];

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        // const userService = this.userService;
        // const filter = this.filter;
        // if (this.envSettings) return;
        // this.dataService.envSettings()
        //   .then(data => {
        //     this.envSettings = data;
        //     const groupCol = this.columnDefs.find(c => c.headerName === 'Роли');
        //     if (groupCol) {
        //       for (let i = 0; i < groupCol.children.length; i++) {
        //         const currentCol = groupCol.children[i];
        //         const roleName = currentCol.field;
        //         if (!this.envSettings.roles[roleName].use) {
        //           currentCol.visible = false;
        //         }
        //         currentCol.headerName = this.envSettings.roles[roleName].caption;
        //       }
        //     }
        //      this.gridColumnApi.setColumnVisible('admin', this.envSettings.roles.admin.use);
        //     if (this.envSettings.roles.moderator.use) {
        //       this.columnDefs.push({ headerName: 'Рубрики',
        //         field:'statusRubrics',
        //         width: 400,
        //         sortable: false,
        //         valueFormatter: objectFormatter,
        //         cellStyle: function() {
        //           return {'white-space': 'normal'};
        //         }
        //       });
        //     }
        //     this.gridApi.setColumnDefs(this.columnDefs);
        //     this.gridColumnApi.setColumnVisible('moderator', this.envSettings.roles.moderator.use);
        //     this.gridColumnApi.setColumnVisible('mainModerator', this.envSettings.roles.mainModerator.use);
        //     this.gridColumnApi.setColumnVisible('rubricModerator', this.envSettings.roles.rubricModerator.use);
        //     this.gridColumnApi.setColumnVisible('clientAdmin', this.envSettings.roles.clientAdmin.use);
        //     const showGroup = this.envSettings.roles.moderator.use || this.envSettings.roles.rubricModerator.use || this.envSettings.roles.clientAdmin.use;
        //     this.dataService.setCollapseUserRoles(showGroup);
        //     // if (showGroup) {
        //     //   this.gridColumnApi.setColumnVisible
        //     // }
        this.setDataSource();
        this.gridApi.sizeColumnsToFit();
        //   });
    }

    public ngOnInit() {
        super.ngOnInit();
    }

    public isUpdating(): boolean {
        return (!this.users);
    }

    public onActionClick(button: ICardActionButton) {
        switch (button.action.id) {
            case E_ACTION_TYPE.stop: {
                this.setUserDisconnect(this.gridApi.getSelectedRows(), true);
                break;
            }
            case E_ACTION_TYPE.start: {
                this.setUserDisconnect(this.gridApi.getSelectedRows(), false);
                break;
            }
            default: {
                super.onActionClick(button);
                break;
            }
        }

    }

    protected setUserDisconnect(rows: any[], disconnect: boolean) {
        if (rows && rows.length) {
            const mess = disconnect ? CONFIRM_OP_BLOCK : CONFIRM_OP_UNBLOCK;
            const confirmation: IConfirmWindow = Object.assign({}, mess);
            confirmation.bodyList = [];
            rows.forEach(r =>
                confirmation.bodyList.push(
                    this.descriptor.genRecordTitle(
                        { [this.descriptor.entityName]: r }, false)
                ));

            return this.confirmSrv.confirm(confirmation).then((confirm) => {
                if (confirm.result === BUTTON_RESULT_YES) {

                    for (const row of rows) {
                        const id: string = row.id;
                        this.dataService.updateRecord(USER_DESCRIPTOR.entityName, id, { disconnected: disconnect }).then(
                            (result) => {
                                const msg = new Message('Блокировка', 'Доступ пользователя изменен', MessageType.success);
                                this.msgSrv.addMessage(msg);
                                this.setDataSource();
                            }).catch(
                                (error) => {
                                    return this.errSrv.passGlobalErrors(error).then(() => {
                                        this.setDataSource();
                                    });
                                });
                    }
                }
            });
        }
    }

    protected getdefaultColDef() {
        return [];
    }

    protected getUndefinedSort(): IOrderByItemType[] {
        return [{ field: FT_ID.key, direction: ISearchDirection.Desc }];
    }

    protected getActionList(): ICardAction[] {
        return [
            <ICardAction>{ id: E_ACTION_TYPE.add, title: 'Новый пользователь' },
            <ICardAction>{ id: E_ACTION_TYPE.stop, title: 'Заблокировать' },
            <ICardAction>{ id: E_ACTION_TYPE.start, title: 'Разблокировать' },
            // <ICardAction > { id: E_ACTION_TYPE.protocol,  title: 'Протокол' },
            <ICardAction>{ id: E_ACTION_TYPE.refresh, title: 'Обновить список' },
        ];
    }

    protected getDescriptor(): BaseTableDescriptor {
        return USER_DESCRIPTOR;
    }

    protected getFilter() {
        const value = super.getFilter();
        const allLS = !!AppUtils.getValueByPath(value, 'notOnlyForms');

        let res = AppUtils.deepCopy(value);

        AppUtils.deleteValue(res, 'notOnlyForms');

        if (!allLS) {
            res = AppUtils.mergeObjects(res, { 'allClaims.value': 'Form', 'allClaims.type': 'ls' })
        }
        return res;
    }

    protected isActionHidden(id: E_ACTION_TYPE, selectionInfo: any): boolean {
        switch (id) {
            case E_ACTION_TYPE.start: {
                if (selectionInfo.selected && selectionInfo.selected.length) {
                    return !selectionInfo.selected[0].disconnected;
                }
                return true;
            }
            case E_ACTION_TYPE.stop: {
                if (selectionInfo.selected && selectionInfo.selected.length) {
                    return selectionInfo.selected[0].disconnected;
                }
                return true;
            }
        }

        return super.isActionHidden(id, selectionInfo);
    }

}
