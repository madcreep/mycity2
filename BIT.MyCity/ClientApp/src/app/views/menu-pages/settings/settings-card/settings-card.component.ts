import { Component, Input, OnInit } from '@angular/core';
import { BaseCardFormComponent } from 'src/app/views/cards/card-form/base-card-form.component';
import { E_FIELD_TYPE, IEditPageDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';
import { IDynamicInputOptions } from 'src/app/modules/dynamic-inputs/fasade/dynamic-input.interfaces';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { ISettingNodeType, ISettings } from 'src/generated/types.graphql-gen';



export interface ISettingElement {
    key: string;
    type: ISettingNodeType;
    children: any[];
    weight: number;
    element: ISettings;
    positioned?: boolean;
}


@Component({
    selector: 'app-settings-card',
    templateUrl: './settings-card.component.html',
    styleUrls: ['./settings-card.component.scss']
})


export class SettingsCardComponent extends BaseCardFormComponent implements OnInit {
    @Input() elements: ISettingElement[];

    types = E_FIELD_TYPE;
    nodetypes = ISettingNodeType;
    viewOpts: IDynamicInputOptions = { hideLabel: true, inputClass: 'simple-thin-input' };
    // pagesFields: any;


    ngOnInit(): void {

        // this.pagesFields = this.descriptor.fieldsEdit();
        this.pageList = this.elements.map((el, i) => ({ id: i, title: el.element.description }));
        super.ngOnInit();
    }

    public getLabelFor(input: InputBase<any>) {
        return input.label || input.key;
    }

    public inputForKey(key: string) {
        return this.inputs.find(i => i.key === 'settings.' + key);
    }

    public accordionClass() {
        return 'accordion-thin';
    }
}
