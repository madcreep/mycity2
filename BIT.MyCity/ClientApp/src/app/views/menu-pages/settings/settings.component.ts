import {Component} from '@angular/core';
import {ICardAction} from 'src/app/views/cards/card.interfaces';
import {E_ACTION_TYPE} from 'src/app/model/datatypes/descriptors.interfaces';
import {CardComponent} from 'src/app/views/cards/card.component';
import {SETTINGS_DESCRIPTOR} from 'src/app/model/datatypes/settings.const';
import {ISettingsConfig} from 'src/app/services/settings.service';
import {ISettingType, ISettings, IApiInformation, ISettingNodeType} from 'src/generated/types.graphql-gen';
import {SETTINGS_GENERAL_ROOT} from 'src/app/services/setting-names.const';
import {APP_BUILD_DATE} from 'src/app/defines.gen';
import {ISettingElement} from './settings-card/settings-card.component';
import {environment} from '../../../../environments/environment';

export const PAGE_ABOUT_TITLE = 'Инфо';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})


export class SettingsComponent extends CardComponent {

    inputscfg: any[];
    versionInfo: IApiInformation = {};
    activePage = 0;
    // config: any;
    elements: ISettingElement[] = [];

    // version = 'Release date: ' + APP_BUILD_DATE + ' Build number: ' + APP_BUILD_NUMBER + (DEBUGANGULAR ? ' (DEBUG)' : '');
    version = 'Release date: ' + APP_BUILD_DATE + ' Version: ' + environment.appVersion;

    public actionList: ICardAction[] = [
        { id: E_ACTION_TYPE.cancel, title: 'Отменить' },
        { id: E_ACTION_TYPE.apply, title: 'Применить' },
    ];

    public activePageIndexChange(page: number) {
        this.activePage = page;

    }

    closeCard() {
        this.activePage = 1;
        this.updateForm(null);
        return false;
    }

    // protected updateActions() {
    //     for (const act of this.actionList) {
    //         switch (act.id) {
    //             // case E_ACTION_TYPE.save:
    //             //     act.hidden = !this.editMode;
    //             //     break;
    //             case E_ACTION_TYPE.cancel: {
    //                 // act.title = this.editMode ? 'Отмена' : 'Закрыть';
    //                 act.hidden = !this.cardForm.editedData;
    //                 break;
    //             }
    //
    //             default:
    //                 break;
    //         }
    //     }
    // }

    protected updateForm(params) {
        this.isUpdate = true;
        this.card.id = -1;
        return Promise.all(
            [
                this.dataSrv.getVersionsInfo().then(
                    (info) => this.versionInfo = info.apiInformation
                ),

                this.settingsSrv.getSettings(SETTINGS_GENERAL_ROOT, true).then((datacfg: ISettingsConfig) => {
                    const data = datacfg.value;
                    // this.config = datacfg.config;
                    this.elements = [
                        {key: '', children: [], type: ISettingNodeType.TabGroup, weight: -1, element: {weight: 0, description: PAGE_ABOUT_TITLE}},
                        ...this.configToElementsArray(datacfg.config)
                    ];

                    this.data = {[this.card.entityName]: data};
                    this.inputscfg = datacfg.config;

                    this.updateDiscriptorForConfig();

                    return this.inpSrv.getInputs(this.data, false).then((inputs) => {
                        this.inputs = inputs;
                        this.inpSrv.updateInputAndPageList(this.descriptor.entityName, this.inputs, this.pagesList);
                        this.formGroup = this.inpSrv.toFormGroup(this.inputs);
                        this.isUpdate = false;
                    });
                })
            ]
        ).catch(error => {
            return this.errSrv.passGlobalErrors(error).then(() => {
                this.closeCard();
                this.isUpdate = false;
            });
        });
    }

    protected configToElementsArray(config: any): ISettingElement[] {
        const arr = [];
        const elements: ISettingElement[] = [];
        for (const key in config) {
            if (Object.prototype.hasOwnProperty.call(config, key)) {
                const element: ISettings = config[key];
                arr.push({key, element});
                elements.push({key, element, type: element.settingNodeType, children: [], weight: element.weight, positioned: false});
            }
        }

        elements.forEach(el => {
            if (!el.positioned) {
                if (el.type === ISettingNodeType.TabGroup) {
                    el.positioned = true;
                    const children_groups = elements.filter(gr => gr.type === ISettingNodeType.Group && gr.key.startsWith(el.key) && !gr.positioned);

                    children_groups.forEach(gr => {
                        gr.positioned = true;
                        const children_els = elements.filter(e => e.type === ISettingNodeType.Value && e.key.startsWith(gr.key) && !e.positioned);
                        gr.children = children_els.sort((a, b) => a.weight > b.weight ? 1 : -1);
                        children_els.forEach(e => e.positioned = true);
                    });
                    el.children = [
                        ...children_groups.sort((a, b) => a.weight > b.weight ? 1 : -1),
                        ...elements.filter(gr => gr.key.startsWith(el.key) && !gr.positioned).sort((a, b) => a.weight > b.weight ? 1 : -1),
                    ];
                }
            }
        });

        const ungrouped = elements.filter(el => !el.positioned);
        if (ungrouped && ungrouped.length) {
            elements.push({
                key: '.', weight: 0, type: ISettingNodeType.Group, element: {description: 'Settings', weight: 0}, positioned: true,
                children: [
                    {key: '.', weight: 0, type: ISettingNodeType.Group, element: {description: 'Settings', weight: 0}, positioned: true, children: ungrouped}
                ],

            });
        }

        const res = [
            ...elements.filter(el => el.type === ISettingNodeType.TabGroup),
        ];

        // sort

        const res_sorted = res.sort((a, b) => a.weight > b.weight ? 1 : -1);


        return res_sorted;
    }

    protected updateDiscriptorForConfig() {
        const fields = [];
        for (const key in this.inputscfg) {
            if (this.inputscfg.hasOwnProperty(key)) {
                const field: ISettings = this.inputscfg[key];

                if (field.settingType === ISettingType.Value || field.settingType === ISettingType.Undefined) {
                    const inputDescriptor = this.inpSrv.settingToInputDescriptor(field);
                    if (inputDescriptor) {
                        fields.push(
                            inputDescriptor
                        );
                    }
                }
            }
        }

        SETTINGS_DESCRIPTOR.fields = fields;

        // SETTINGS_DESCRIPTOR.edit_list = [
        //     { title: PAGE_ABOUT_TITLE, fields: [] },
        //     { title: 'Основные', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.web.general.')).map(s => s.key) },
        //     { title: 'Файлы', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.attach.')).map(s => s.key) },
        //     { title: 'Рубрики', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.web.rubric.')).map(s => s.key) },
        //     { title: 'Территории', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.web.territory.')).map(s => s.key) },
        //     { title: 'Соц. сети', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.auth.')).map(s => s.key) },
        //     { title: 'Почта', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.smtp.')).map(s => s.key) },
        //     { title: 'system.api', fields: SETTINGS_DESCRIPTOR.fields.filter(s => s.key.startsWith('system.api.')).map(s => s.key) },
        //     // { title: 'Тест', fields: SETTINGS_DESCRIPTOR.fields.filter( s => s.key.startsWith('system.test.')).map(s => s.key)},
        // ];

        // const all_inputs: { key: string, obj: ISettings }[] = [];

        // for (const key in this.elements) {
        //     if (Object.prototype.hasOwnProperty.call(this.config, key)) {
        //         const obj = this.config[key];
        //         p2.push({ key, obj });

        //     }
        // }

        const all_inputs = [];
        for (const key in this.inputscfg) {
            if (Object.prototype.hasOwnProperty.call(this.inputscfg, key)) {
                const obj = this.inputscfg[key];
                if (obj.settingNodeType === ISettingNodeType.Value) {
                    all_inputs.push(key);
                }

            }
        }
        // SETTINGS_DESCRIPTOR.edit_list
        // SETTINGS_DESCRIPTOR.edit_list = [
        //     { title: PAGE_ABOUT_TITLE, fields: [] },
        //     ...p2.filter(p => (p.obj.settingNodeType === ISettingNodeType.TabGroup)).map(page => (
        //         {
        //             title: page.obj.description,
        //             fields: p2.filter(p => {
        //                 return (p.obj.settingNodeType as ISettingNodeType === ISettingNodeType.Value) && p.key.startsWith(page.key + '.');
        //             }

        //             ).map(p => p.key)
        //             // p2.filter(s => s.key.startsWith('system.web.territory.')).map(s => s.key)
        //         }

        //     )
        //     )

        // ];

        SETTINGS_DESCRIPTOR.edit_list = [
            {
                title: 'all unsorted',
                fields: all_inputs,
            },
        ];


        this.pagesList = SETTINGS_DESCRIPTOR.edit_list;
    }

    protected getEntityName(): string {
        return SETTINGS_DESCRIPTOR.id;
    }

    protected saveCard(): Promise<any> {
        return super.saveCard(false).then(() => {
            return this.settingsSrv.updateSettings([SETTINGS_GENERAL_ROOT], false);
        });
    }

}
