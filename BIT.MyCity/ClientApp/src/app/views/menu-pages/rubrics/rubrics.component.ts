import { IRubric } from '../../../../generated/types.graphql-gen';
import { Component, OnInit } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { SETTINGS_RUBRIC_ADDEN } from 'src/app/services/setting-names.const';
import { RUBRIC_ORG_DESCRIPTOR } from 'src/app/model/datatypes/rubric-organisations.const';
import { RUBRIC_MAIN_DESCRIPTOR } from 'src/app/model/datatypes/rubric-main.const';

@Component({
  selector: 'app-rubrics',
//   templateUrl: './rubrics.component.html',
  templateUrl: './../menu-base-list/menu-base-list.component.html',
  styleUrls: ['./rubrics.component.css']
})
export class RubricsComponent extends MenuBaseListComponent implements OnInit {

    rubrics: IRubric[];
    private _isAddEnabled: boolean;

    ngOnInit() {
        super.ngOnInit();
        this.subscribe(
            this.settSrv.subscribeToValue(SETTINGS_RUBRIC_ADDEN,
                (value) => {
                    this._isAddEnabled = value === 'true';
                }
            )
        );
    }

    public isUpdating(): boolean {
        return !this.rubrics;
    }
    protected getDescriptor(): BaseTableDescriptor {
        return RUBRIC_ORG_DESCRIPTOR;
    }


    protected isActionHidden(id: E_ACTION_TYPE, selectionInfo: any): boolean {
        if (id === E_ACTION_TYPE.add) {
            return !this._isAddEnabled;
        }
        return super.isActionHidden(id, selectionInfo);
    }

}
