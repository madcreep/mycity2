import { Component, OnInit } from '@angular/core';
import { ICardAction, ICardActionButton } from 'src/app/views/cards/card.interfaces';

@Component({
    selector: 'app-appointments-page',
    templateUrl: './appointments-page.component.html',
    styleUrls: ['./appointments-page.component.css']
})
export class AppointmentsPageComponent implements OnInit {

    pageList: ICardAction[] = [
        { id: 0, title: 'Время приема' },
        { id: 1, title: 'Заявки' },
    ];

    activePage = 0;


    constructor() { }

    ngOnInit() {
    }

    public onPageClick(button: ICardActionButton) {
        this.activePage = button.action.id;
    }
}
