import { Component } from '@angular/core';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { AppealsCitizensListComponent } from '../appeals-citizens/appeals-citizens.component';
import { APPEAL_CIT_EX_DESCRIPTOR } from 'src/app/model/datatypes/appeals/appeal-citizen-executors.const';
import { ICommentsTreeOptions } from 'src/app/views/cards/card-pages/comments-tree-view/comments-tree-view.component';
import { IMessageKindEnum } from 'src/generated/types.graphql-gen';

@Component({
  selector: 'app-appeals-citizens-executor',
  templateUrl: './../menu-base-list/menu-base-list.component.html',
  styleUrls: ['./appeals-citizens-executor.component.scss']
})
export class AppealsCitizensExecutorComponent extends AppealsCitizensListComponent {

    protected getDescriptor(): BaseTableDescriptor {
        return APPEAL_CIT_EX_DESCRIPTOR;
    }

    protected getAnswersOptions(): ICommentsTreeOptions {
        return {
            addAnswerEnable: false,
            readOnlyMode: true,
            useKindRoot: IMessageKindEnum.SystemUserMessage,
        };
    }
}
