import {IFieldDescriptor} from '../../model/datatypes/descriptors.interfaces';
import {BaseTableDescriptor} from '../../model/datatypes/descriptors.const';
import {InjectorInstance} from '../../helpers/app.static';
import {SettingsService} from '../../services/settings.service';


export class MenuPagesHelper {
    public static objectFormatter(params) {
        let result = '';
        if (params && params.value && params.value.length) {
            for (let i = 0; i < params.value.length; i++) {
                result += (i === 0 ? '' : ', ') + params.value[i].name;
            }
        }
        return result;
    }

    public static gridColumnsByDescriptor(descr: BaseTableDescriptor) {
        const fields = descr.fieldsView();
        return fields.map(f => MenuPagesHelper.gridColumnByDescriptor(f));
    }

    public static gridColumnByDescriptor(descr: IFieldDescriptor) {
        const res = descr.gridOption || {};

        if (res.headerName === undefined) {
            if (descr.titlePath) {
                const sett = InjectorInstance.get(SettingsService);
                res.headerName = sett.getValue(descr.titlePath);
            } else {
                res.headerName = descr.title;
            }
        }
        if (res.field === undefined) {
            res.field = descr.key;
        }

        return res;
    }

}
