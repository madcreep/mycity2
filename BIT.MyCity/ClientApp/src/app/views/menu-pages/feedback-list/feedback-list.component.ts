import { Component, OnInit } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { FEEDBACK_DESCRIPTOR } from 'src/app/model/datatypes/appeals/appeal-feedback.const';
import { IAppealTypeEnum, IOrderByItemType, ISearchDirection } from 'src/generated/types.graphql-gen';
import { FT_WEIGHT, FT_ID } from 'src/app/model/datatypes/fields.const';

@Component({
    selector: 'app-feedback-list',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./feedback-list.component.scss']
})
export class FeedbackListComponent extends MenuBaseListComponent implements OnInit {

    public isUpdating(): boolean {
        return false;
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.agGrid.gridOptions.sortingOrder =  ['desc', 'asc'];
        this.setDataSource();
        this.gridApi.sizeColumnsToFit();
    }

    protected getDescriptor(): BaseTableDescriptor {
        return FEEDBACK_DESCRIPTOR;
    }

    protected getFilter() {
        const value = super.getFilter();
        value['appealType'] = IAppealTypeEnum.Feedback;
        return value;
    }

    protected getUndefinedSort(): IOrderByItemType[] {
        return [{ field: FT_ID.key , direction: ISearchDirection.Desc}];
    }

}
