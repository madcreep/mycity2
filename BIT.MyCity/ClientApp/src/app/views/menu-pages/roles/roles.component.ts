import { ROLE_DESCRIPTOR } from '../../../model/datatypes/role.const';

import { Component } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { MessageType, Message } from 'src/app/services/message';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { ICardAction } from 'src/app/views/cards/card.interfaces';

@Component({
    selector: 'app-roles',
    // templateUrl: './roles.component.html',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./roles.component.css']
})
export class RolesComponent extends MenuBaseListComponent {

    public isUpdating(): boolean {
        return false;
    }


    protected getActionList(): ICardAction[] {
        return [
            <ICardAction>{ id: E_ACTION_TYPE.add, title: 'Добавить' },
            <ICardAction>{ id: E_ACTION_TYPE.delete, title: 'Удалить' },
            <ICardAction>{ id: E_ACTION_TYPE.refresh, title: 'Обновить список' },
        ];
    }



    public onCellDoubleClicked(params) {
        if (params.data && params.data.id) {
            if (params.data.editable) {
                this.openEntity(params.data);
            } else {
                const msg = new Message('Внимание', 'Данную запись нельзя редактировать', MessageType.info);
                this.msgSrv.addMessage(msg);
            }
        }
    }

    public deleteRecords(data: any[]): Promise<any> {
        for (let i = 0; i < data.length; i++) {
            const element = data[i];
            if (!element.editable) {
                const msg = new Message('Внимание', 'Данную запись нельзя удалять', MessageType.info);
                this.msgSrv.addMessage(msg);
                return null;
            }
        }
        return super.deleteRecords(data);
    }

    protected getDescriptor(): BaseTableDescriptor {
        return ROLE_DESCRIPTOR;
    }

    // protected getFilter () {
    //     const value = super.getFilter();
    //     value['disconnected'] = false;
    //     return value;
    // }


}
