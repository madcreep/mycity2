import { Component } from '@angular/core';
import { MenuBaseListComponent, ISelectionInfo } from '../menu-base-list/menu-base-list.component';
import { ICardAction, ICardActionButton } from 'src/app/views/cards/card.interfaces';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { IConfirmWindow } from 'src/app/common/confirm-window/confirm-window.component';
import { BUTTON_RESULT_CANCEL, BUTTON_RESULT_YES } from 'src/app/consts/confirmations.const';
import { MessageType, Message } from 'src/app/services/message';
import { INewsStateEnum } from 'src/generated/types.graphql-gen';


const CONFIRM_OP_PUBLISHNEWS: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Вы действительно хотите опубликовать выбранные новости?',
    bodyAfterList: 'Продолжить?',
    buttons: [
        { title: 'Нет', result: BUTTON_RESULT_CANCEL, },
        { title: 'Опубликовать', result: BUTTON_RESULT_YES, isDefault: true, },
    ],
};

const CONFIRM_OP_UNPUBLISHNEWS: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Вы действительно хотите снять с публикации выбранные новости?',
    bodyAfterList: 'Продолжить?',
    buttons: [
        { title: 'Нет', result: BUTTON_RESULT_CANCEL, },
        { title: 'Снять', result: BUTTON_RESULT_YES, isDefault: true, },
    ],
};


@Component({
    selector: 'app-news',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./news.component.css']
})

export class NewsComponent extends MenuBaseListComponent {

    public isUpdating(): boolean {
        return false;
    }

    public onActionClick(button: ICardActionButton) {
        switch (button.action.id) {
            case E_ACTION_TYPE.start: {
                return this.setNewsPublishState(this.gridApi.getSelectedRows(),
                    INewsStateEnum.Published,
                    CONFIRM_OP_PUBLISHNEWS,
                    'Новости опубликованы'
                );
            }
            case E_ACTION_TYPE.stop: {
                return this.setNewsPublishState(this.gridApi.getSelectedRows(),
                    INewsStateEnum.Unpublished,
                    CONFIRM_OP_UNPUBLISHNEWS,
                    'Новости сняты с публикации'
                );
            }

            default: {
                super.onActionClick(button);
                break;
            }
        }
    }



    protected getActionList(): ICardAction[] {
        return [
            <ICardAction>{ id: E_ACTION_TYPE.add, title: 'Добавить' },
            <ICardAction>{ id: E_ACTION_TYPE.delete, title: 'Удалить' },
            <ICardAction>{ id: E_ACTION_TYPE.refresh, title: 'Обновить список' },
            <ICardAction>{ id: E_ACTION_TYPE.start, title: 'Опубликовать' },
            <ICardAction>{ id: E_ACTION_TYPE.stop, title: 'Снять' },
        ];
    }

    protected isActionHidden(id: E_ACTION_TYPE, selectionInfo: ISelectionInfo): boolean {
        if (id === E_ACTION_TYPE.start || id === E_ACTION_TYPE.stop) {
            return selectionInfo.selected.length !== 1;
        }
    }

    private setNewsPublishState(data: any[], state: INewsStateEnum, win: IConfirmWindow, okMessage: string) {
        const rows = data.map(r => r.id);

        const confirmation: IConfirmWindow = Object.assign({}, win);
        confirmation.bodyList = [];
        data.forEach(r => confirmation.bodyList.push(
            this.descriptor.genRecordTitle(
                { [this.descriptor.entityName]: r }, false)
        ));

        return this.confirmSrv.confirm(confirmation).then((confirm) => {
            if (confirm.result === BUTTON_RESULT_YES) {
                if (rows && rows.length) {
                    const id: string = rows[0];
                    this.dataService.setNewsPublishState(id, state).then(
                        (result) => {
                            const msg = new Message('Новости', okMessage, MessageType.success);
                            this.msgSrv.addMessage(msg);
                            this.setDataSource();
                        }).catch(
                            (error) => {
                                return this.errSrv.passGlobalErrors(error).then(() => {
                                    this.setDataSource();
                                });
                            });
                }
            }
        });
    }

}
