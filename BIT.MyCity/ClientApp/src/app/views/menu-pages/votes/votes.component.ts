import { Component } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';
import { ICardAction, ICardActionButton } from 'src/app/views/cards/card.interfaces';
import { E_ACTION_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { IConfirmWindow } from 'src/app/common/confirm-window/confirm-window.component';
import { BUTTON_RESULT_YES, BUTTON_RESULT_CANCEL } from 'src/app/consts/confirmations.const';
import { MessageType, Message } from 'src/app/services/message';
import { FT_ID } from 'src/app/model/datatypes/fields.const';
import { ISearchDirection, IStatusEnum } from 'src/generated/types.graphql-gen';
import { VOTEROOTS_DESCRIPTOR } from 'src/app/model/datatypes/vote-roots.const';

const CONFIRM_OP_START: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Вы действительно хотите запустить выбранное голосование?',
    bodyAfterList: 'Продолжить?',
    buttons: [
        { title: 'Нет', result: BUTTON_RESULT_CANCEL, },
        { title: 'Запустить', result: BUTTON_RESULT_YES, isDefault: true, },
    ],
};

const CONFIRM_OP_STOP: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Вы действительно хотите остановить выбранное голосование?',
    bodyAfterList: 'Продолжить?',
    buttons: [
        { title: 'Нет', result: BUTTON_RESULT_CANCEL, },
        { title: 'Остановить', result: BUTTON_RESULT_YES, isDefault: true, },
    ],
};

const CONFIRM_OP_ARCHIVE: IConfirmWindow = {
    title: 'Внимание',
    bodyList: [],
    body: 'Вы действительно хотите архивировать выбранное голосование?',
    bodyAfterList: 'Продолжить?',
    buttons: [
        { title: 'Нет', result: BUTTON_RESULT_CANCEL, },
        { title: 'Архивировать', result: BUTTON_RESULT_YES, isDefault: true, },
    ],
};



@Component({
    selector: 'app-votes',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./votes.component.css']
})
export class VotesComponent extends MenuBaseListComponent {

    public isUpdating(): boolean {
        return false;
    }



    public onActionClick(button: ICardActionButton) {
        switch (button.action.id) {
            case E_ACTION_TYPE.start: {
                this.startVotes(this.gridApi.getSelectedRows());
                break;
            }
            case E_ACTION_TYPE.stop: {
                this.stopVotes(this.gridApi.getSelectedRows(), E_ACTION_TYPE.stop);
                break;
            }

            case E_ACTION_TYPE.archive: {
                this.stopVotes(this.gridApi.getSelectedRows(), E_ACTION_TYPE.archive);
                break;
            }

            default: {
                super.onActionClick(button);
                break;
            }
        }

    }

    public startVotes(data: any[]): Promise<any> {
        const rows = data.map(r => r.id);

        const confirmation: IConfirmWindow = Object.assign({}, CONFIRM_OP_START);
        confirmation.bodyList = [];
        data.forEach(r =>
            confirmation.bodyList.push(
                this.descriptor.genRecordTitle(
                    { [this.descriptor.entityName]: r }, false)
            ));

        return this.confirmSrv.confirm(confirmation).then((confirm) => {
            if (confirm.result === BUTTON_RESULT_YES) {
                if (rows && rows.length) {
                    const id: string = rows[0];
                    this.dataService.voteStart(id).then(
                        (result) => {
                            const msg = new Message('Голосование', 'Голосование запущено', MessageType.success);
                            this.msgSrv.addMessage(msg);
                            this.setDataSource();
                        }).catch(
                            (error) => {
                                return this.errSrv.passGlobalErrors(error).then(() => {
                                    this.setDataSource();
                                });
                            });
                }
            }
        });
    }
    public stopVotes(data: any[], type: E_ACTION_TYPE): Promise<any> {
        const rows = data.map(r => r.id);

        const confirmation: IConfirmWindow = Object.assign({}, type === E_ACTION_TYPE.archive ? CONFIRM_OP_ARCHIVE : CONFIRM_OP_STOP);

        confirmation.bodyList = [];
        data.forEach(r =>
            confirmation.bodyList.push(
                this.descriptor.genRecordTitle(
                    { [this.descriptor.entityName]: r }, false)
            ));

        return this.confirmSrv.confirm(confirmation).then((confirm) => {
            if (confirm.result === BUTTON_RESULT_YES) {
                if (rows && rows.length) {
                    const id: string = rows[0];
                    this.dataService.voteStop(id).then(
                        (result) => {
                            const msg = new Message('Голосование',
                                type === E_ACTION_TYPE.archive ? 'Голосование отправлено в архив' : 'Голосование остановлено',
                                MessageType.success);
                            this.msgSrv.addMessage(msg);
                            this.setDataSource();
                        }).catch(
                            (error) => {
                                return this.errSrv.passGlobalErrors(error).then(() => {
                                    this.setDataSource();
                                });
                            });
                }
            }
        });
    }

    protected getUndefinedSort() {
        return [{ field: 'state', direction: ISearchDirection.Asc }, { field: FT_ID.key, direction: ISearchDirection.Desc }];
    }
    protected getDescriptor(): BaseTableDescriptor {
        return VOTEROOTS_DESCRIPTOR;
    }

    protected getActionList(): ICardAction[] {
        return [
            <ICardAction>{ id: E_ACTION_TYPE.add, title: 'Добавить' },
            <ICardAction>{ id: E_ACTION_TYPE.delete, title: 'Удалить' },
            <ICardAction>{ id: E_ACTION_TYPE.refresh, title: 'Обновить список' },
            <ICardAction>{ id: E_ACTION_TYPE.start, title: 'Запустить' },
            <ICardAction>{ id: E_ACTION_TYPE.stop, title: 'Остановить' },
            <ICardAction>{ id: E_ACTION_TYPE.archive, title: 'В Архив' },
        ];
    }

    protected isActionHidden(id: E_ACTION_TYPE, selectionInfo: any): boolean {
        if (id === E_ACTION_TYPE.start) {
            return !(selectionInfo.selected && selectionInfo.selected.length &&
                (
                    selectionInfo.selected[0].state === IStatusEnum.Draft
                    // selectionInfo.selected[0].state === IStatusEnum.IsArchived ||
                    // selectionInfo.selected[0].state === IStatusEnum.IsDone
                )
            );
        } else if (id === E_ACTION_TYPE.stop) {
            return !(selectionInfo.selected && selectionInfo.selected.length &&
                (
                    (
                        selectionInfo.selected[0].state === IStatusEnum.InProgress ||
                        // selectionInfo.selected[0].state === IStatusEnum.IsArchived ||
                        selectionInfo.selected[0].state === IStatusEnum.IsPlanned
                    )
                )
            );
        } else if (id === E_ACTION_TYPE.archive) {
            return !(selectionInfo.selected && selectionInfo.selected.length &&
                (
                    (
                        selectionInfo.selected[0].state === IStatusEnum.IsDone
                    )
                )
            );
        }
        return super.isActionHidden(id, selectionInfo);
    }
}
