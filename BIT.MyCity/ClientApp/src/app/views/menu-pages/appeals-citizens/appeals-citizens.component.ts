import {Component, OnInit} from '@angular/core';
import {MenuBaseListComponent} from '../menu-base-list/menu-base-list.component';
import {IAppeal, IAppealTypeEnum, IOrderByItemType, ISearchDirection} from 'src/generated/types.graphql-gen';
import {BaseTableDescriptor} from 'src/app/model/datatypes/descriptors.const';
import {APPEAL_CIT_DESCRIPTOR} from 'src/app/model/datatypes/appeals/appeal-citizen.const';
import {E_ACTION_TYPE} from 'src/app/model/datatypes/descriptors.interfaces';
import {SUBSYSTEM_CITIZEN_ID} from 'src/app/services/subsystems.service';
import {FT_DELETED, FT_ID} from 'src/app/model/datatypes/fields.const';
import {CLAIM_SS_CIT_APPEAL, IClaim} from 'src/app/services/claims.const';
import {SETTINGS_SS_CIT_ADDEN, SETTINGS_SS_CIT_DELEN} from 'src/app/services/setting-names.const';
import {AppUtils} from 'src/app/helpers/app.static';
import {IListRange} from 'src/app/services/data.service';
import {ICardAction, ICardActionButton} from '../../cards/card.interfaces';
import {BUTTON_RESULT_OK} from '../../../consts/confirmations.const';
import {IConfirmButton} from '../../../common/confirm-window/confirm-window.component';

@Component({
    selector: 'app-appeals',
//   templateUrl: './appeals.component.html',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./appeals-citizens.component.css']
})

export class AppealsCitizensListComponent extends MenuBaseListComponent implements OnInit {

    appeals: IAppeal[];
    _isAddEnabled: boolean;
    _isDelEnabled: boolean;

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.agGrid.gridOptions.cacheBlockSize = 40;

        // this.setDataSource(); // wait for filter apply
        this.gridApi.sizeColumnsToFit();
        this.agGrid.gridOptions.sortingOrder = ['desc', 'asc'];
    }


    public isUpdating(): boolean {
        return !this.appeals;
    }


    isFilterControlVisible() {
        return true;
    }

    public override filterApply(filters) {
        this.active_filters = AppUtils.deepCopy(filters);
        if (this.active_filters[FT_DELETED.key] !== undefined) {
            delete this.active_filters[FT_DELETED.key];
        }
        this.showDeleted = !!filters[FT_DELETED.key];
        if (this.gridApi && this.gridColumnApi) {
            this.showDeleted = !!filters[FT_DELETED.key];
            this.setDataSource();
        }

    }

    public exportTableToExcel() {
        const button: IConfirmButton = {title: 'Закрыть', result: BUTTON_RESULT_OK, isDisabled: true};
        const form = this.confirmSrv.buildMessage('Экспорт отчета', 'Выполняется построение отчета', [], [button]);

        const filter = this.getFilter();
        const exportPromise = this.dataService.getPgList(this.getDescriptor().id,
            this.getOrderByKey(),
            filter,
            <IListRange>{offset: 0},
            this.showDeleted,
            true
        ).then(result => {
            if (result.total !== undefined) {
                this.totalCount = result.total;
            }
            if (result.items && result.items instanceof Array) {

                if (result['result']) {
                    result = result['result'];
                }
            }
            return result.items;
        }).then(data_array => {
            button.isDisabled = false;
            this.exportExcel.saveAppeals({title: 'Appeals', headers: [], data: data_array});
        });

        this.confirmSrv.confirm(form, exportPromise).then();
    }

    public onActionClick(button: ICardActionButton) {
        switch (button.action.id) {
            case E_ACTION_TYPE.exportExcel: {
                this.exportTableToExcel();
                break;
            }
        }
        super.onActionClick(button);
    }

    protected settingsSubscriptions() {
        return {
            [SETTINGS_SS_CIT_DELEN]: (value) => {
                this._isDelEnabled = value === 'true';
            },
            [SETTINGS_SS_CIT_ADDEN]: (value) => {
                this._isAddEnabled = value === 'true';
            }
        };
    }

    protected getFilter() {
        const value = super.getFilter();
        value['subsystem.uID'] = SUBSYSTEM_CITIZEN_ID;
        value['appealType'] = IAppealTypeEnum.Normal;
        return value;
    }

    protected getUndefinedSort(): IOrderByItemType[] {
        return [{field: FT_ID.key, direction: ISearchDirection.Desc}];
    }

    protected getDescriptor(): BaseTableDescriptor {
        return this.descrSrv.descriptorForEntity(APPEAL_CIT_DESCRIPTOR.id);
    }

    protected getActionList(): ICardAction[] {
        const actions = super.getActionList().concat(
            <ICardAction>{id: E_ACTION_TYPE.exportExcel, title: 'Экспорт Excel'},
        );

        return actions;
    }

    protected actionPermissions(id: E_ACTION_TYPE): IClaim[] {
        switch (id) {
            case E_ACTION_TYPE.delete:
            case E_ACTION_TYPE.add:
                return [CLAIM_SS_CIT_APPEAL];
        }
        return [];
    }

    protected isActionHidden(id: E_ACTION_TYPE, selectionInfo: any): boolean {
        if (id === E_ACTION_TYPE.add) {
            return !this._isAddEnabled;
        } else if (id === E_ACTION_TYPE.delete) {
            return !this._isDelEnabled;
        } else if (id === E_ACTION_TYPE.exportExcel) {
            // return this.totalCount <= 0;
            return false;
        }
        return super.isActionHidden(id, selectionInfo);
    }

    protected override setDataSource(forsedRefresh: boolean = true) {
        const dataService = this.dataService;
        const parentId = this.getParentId();
        const filter = this.getFilter();
        this.forsedRefresh = forsedRefresh;
        this.gridApi.deselectAll();
        this.recalcActions();
        this.totalCount = undefined;
        this.gridApi.setDatasource({
            // rowCont: null,
            getRows: function (params) {
                dataService.getPgList(this.getDescriptor().id,
                    this.getOrderByKey(),
                    filter,
                    <IListRange>{offset: params.startRow, length: params.endRow - params.startRow,},
                    this.showDeleted,
                    this.forsedRefresh).then(result => {
                    if (result.total !== undefined) {
                        this.totalCount = result.total;
                    }
                    if (result.items && result.items instanceof Array) {

                        if (result['result']) {
                            result = result['result'];
                        }
                        if (result.length < params.endRow - params.startRow) {
                            // lastRow = params.startRow + result.length;
                        }
                    }

                    params.successCallback(result.items, this.totalCount);
                }).then(d => {
                    this.forsedRefresh = true; // пока отключил кеши - непонятно как обновлять пагинированное
                });
            }.bind(this)
        });
    }
}
