import { Component } from '@angular/core';
import { MenuBaseListComponent } from '../menu-base-list/menu-base-list.component';
import { PROTOCOL_DESCRIPTOR } from 'src/app/model/datatypes/protocols.const';
import { ICardAction } from 'src/app/views/cards/card.interfaces';
import { BaseTableDescriptor } from 'src/app/model/datatypes/descriptors.const';

@Component({
    selector: 'app-protocols',
    templateUrl: './../menu-base-list/menu-base-list.component.html',
    styleUrls: ['./protocols.component.css']
})
export class ProtocolsComponent extends MenuBaseListComponent {

    public isUpdating(): boolean {
        return false;
    }
    // protected getDescriptor(): BaseTableDescriptor {
    //     return PROTOCOL_DESCRIPTOR;
    // }

    public onCellDoubleClicked(params) {
        if (params.data && params.data.id) {
            // this.openEntity(params.data);
        }
        params.node.childrenMapped = {data: params.node };
        // params.node.rowHeight = 100;
    }

    protected getActionList(): ICardAction[] {
        return [
            // <ICardAction>{ id: E_ACTION_TYPE.add,  title: 'Добавить' },
            // <ICardAction>{ id: E_ACTION_TYPE.delete,  title: 'Удалить' }
        ];
    }



}
