import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';
import {LoginComponent} from '../login/login.component';
import {ToggleService} from '../../toggle.service';
import {Subscription} from 'rxjs/internal/Subscription';
import {MenuPages} from '../menu-pages/menu-pages.const';
import {SettingsService} from '../../services/settings.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {AuthService} from '../../services/auth.service';

@Component({
    selector: 'app-nav-menu',
    templateUrl: './nav-menu.component.html',
    styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements OnInit, OnDestroy {
    show: boolean;
    toggled: boolean;
    title = '';
    private subscriptions: Subscription[] = [];

    constructor(
        public router: Router,
        private auth: AuthService,
        private toggleService: ToggleService,
        private settingsSrv: SettingsService,
        private modalService: BsModalService) {

        this.subscriptions.push(this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.title = '';
                const path = this.router['location'].path();
                if (path) {
                    for (let i = 0; i < MenuPages.length; i++) {
                        const element = MenuPages[i];
                        if ('/' + (element.link || element.id) === path) {
                            if (element.titlePath) {
                                this.subscriptions.push(
                                    this.settingsSrv.subscribeToValue(element.titlePath,
                                        (value) => this.title = value
                                    )
                                );
                                // this.title = element.titlePath ? this.settingsSrv.getValue(element.titlePath) : element.title;
                            } else {
                                this.title = element.title;
                            }
                            break;
                        }
                    }
                }
            }
        }));

    }

    ngOnDestroy(): void {
        this.unsubscribe();
    }

    unsubscribe() {
        this.subscriptions.forEach((subscription: Subscription) => {
            if (subscription != null && !subscription.closed) {
                subscription.unsubscribe();
            }
        });
        this.subscriptions = [];
    }

    toggle() {
        this.toggled = !this.toggled;
        this.toggleService.changeToggled(this.toggled);
        const element = document.getElementById('wrapper');
        if (element) {
            if (this.toggled) {
                element.classList.add('toggled');
            } else {
                element.classList.remove('toggled');
            }
        }
    }

    getLoggedInUser() {
        return this.auth.loggedInUser;
    }

    loggedIn() {
        return this.auth.isLoggedIn();
    }

    ngOnInit(): void {
        this.toggleService.currentMessage.subscribe(message => this.toggled = message);
    }

    login() {
        const form = this.modalService.show(LoginComponent);
        (<LoginComponent>form.content).isModal = true;
    }

    getTitle() {
        return this.title;
    }
}
