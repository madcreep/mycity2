import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { ErrorsService } from '../services/errors.service';
import { LoginComponent } from '../views/login/login.component';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(
        private _authSrv: AuthService,
        private _errSrv: ErrorsService,
        private router: Router
    ) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
            // const expectedRole = next.data.expectedRole;
        return this._authSrv.checkAuth().then ((result) => {

            return result;
        }).catch((error) => {
            return this._errSrv.passGlobalErrors(error).then(() => {
                return false;
            });
        }).then (result => {
            if (!result) {
                LoginComponent.doLoginForm(state.url);
            }
            return result;
        });
    }
}
