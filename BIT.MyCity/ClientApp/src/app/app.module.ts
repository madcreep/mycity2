import { FilterControlComponent } from 'src/app/views/filter-control/filter-control.component';
import { RegionsComponent } from './views/menu-pages/regions/regions.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './views/nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './views/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { JwtHelperService } from '@auth0/angular-jwt';
import { JwtModule } from '@auth0/angular-jwt';
import { RegisterComponent } from './views/register/register.component';
import { SideMenuComponent } from './views/side-menu/side-menu.component';
import { AgGridModule } from 'ag-grid-angular';
import { FilePondModule } from 'ngx-filepond';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { EmptyGuard } from './guards/empty.guard';
import { GraphQLModule } from './graphql.module';
import { ApplicationRouting } from './app-routing.const';
import { AppUtils } from './helpers/app.static';
import { CardComponent } from './views/cards/card.component';
import { SimpleCardComponent } from './views/cards/card-form/simple-card/simple-card.component';
import { UsersComponent } from './views/menu-pages/users/users.component';
import { TerritoriesComponent } from './views/menu-pages/territories/territories.component';
import { RolesComponent } from './views/menu-pages/roles/roles.component';
import { RubricsComponent } from './views/menu-pages/rubrics/rubrics.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppealsCitizensListComponent } from './views/menu-pages/appeals-citizens/appeals-citizens.component';
import { TopicsComponent } from './views/menu-pages/topics/topics.component';
import { SettingsComponent } from './views/menu-pages/settings/settings.component';
import { SimpleCardKeyValuesComponent } from './views/cards/card-form/simple-card-key-values/simple-card-key-values.component';
import { AppointmentsComponent } from './views/menu-pages/appointments/appointments.component';
import { AppointmentsCardComponent } from './views/cards/card-form/appointments-card/appointments-card.component';
import { AppointmentsPageComponent } from './views/menu-pages/appointments-page/appointments-page.component';
import { AppointmentRangeComponent } from './views/menu-pages/appointment-range/appointment-range.component';
import { NumericTextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { ProtocolsComponent } from './views/menu-pages/protocols/protocols.component';
import { AppealsOrganizationsComponent } from './views/menu-pages/appeals-organizations/appeals-organizations.component';
import { CommonModule } from '@angular/common';
import { ConfirmWindowComponent } from './common/confirm-window/confirm-window.component';
import { CanDeactivateGuard } from './guards/can-deactivate.guard';
import { FilepondGqlComponent } from './data-services/file-uploaders/filepond-gql/filepond-gql.component';
import { CommentsTreeViewComponent } from './views/cards/card-pages/comments-tree-view/comments-tree-view.component';
import { VotesComponent } from './views/menu-pages/votes/votes.component';
import { SourceTestingComponent } from './views/menu-pages/source-testing/source-testing.component';
import { FilepondHttpComponent } from './data-services/file-uploaders/filepond-http/filepond-http.component';
import { UserCardComponent } from './views/cards/card-form/user-card/user-card.component';
import { SubsystemsComponent } from './views/menu-pages/subsystems/subsystems.component';
import { AppealUlCardComponent } from './views/cards/card-form/appeal-ul-card/appeal-ul-card.component';
import { SubsystemCardComponent } from './views/cards/card-form/subsystem-card/subsystem-card.component';
import { SettingsPagesComponent } from './views/cards/card-pages/settings-pages/settings-pages.component';
import { VoteCardComponent } from './views/cards/card-form/vote-card/vote-card.component';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { VoteItemRecordComponent } from './views/cards/card-form/vote-card/vote-item-record/vote-item-record.component';
import { RubricCardComponent } from './views/cards/card-form/rubric-card/rubric-card.component';
import { CommentRecordComponent } from './views/cards/card-pages/comments-tree-view/comment-record/comment-record.component';
import { DescriptorsLoaderService } from './services/descriptors-loader.service';
import { SimpleEditFormComponent } from './views/cards/card-form/simple-card/simple-edit-form/simple-edit-form.component';
import { QuestionItemComponent } from './views/cards/card-form/vote-card/question-item/question-item.component';
import { BaseCardEditComponent } from './views/cards/card-form/base-card-edit/base-card-edit.component';
import { AccordionCardEditComponent } from './views/cards/card-form/accordion-card-edit/accordion-card-edit.component';
import { NewsComponent } from './views/menu-pages/news/news.component';
import { NewsCardComponent } from './views/cards/card-form/news-card/news-card.component';
import { FeedbackListComponent } from './views/menu-pages/feedback-list/feedback-list.component';
import { EnumVerbsListComponent } from './views/menu-pages/enum-verbs/enum-verbs-list.component';
import { RubricsMainComponent } from './views/menu-pages/rubrics-main/rubrics-main.component';
import { AppealsCitizensExecutorComponent } from './views/menu-pages/appeals-citizens-executor/appeals-citizens-executor.component';
import { AppealFeedbackCardComponent } from './views/cards/card-form/appeal-feedback-card/appeal-feedback-card.component';
import { SettingsCardComponent } from './views/menu-pages/settings/settings-card/settings-card.component';
import { EnvironmentLoadService } from './services/environment-load.service';
import { APP_INITIALIZER } from '@angular/core';
import { AppealCitizenCardComponent } from './views/cards/card-form/appeal-citizen-card/appeal-citizen-card.component';
import { TopicCardComponent } from './views/cards/card-form/topic-card/topic-card.component';
import { JournalViewComponent } from './views/cards/card-pages/journal-view/journal-view.component';
import { ReportsListComponent } from './views/menu-pages/reports-list/reports-list.component';
import { ReportCardComponent } from './views/cards/card-form/repord-card/report-card.component';
import { DynamicInputsModule } from './modules/dynamic-inputs/dynamic-inputs.module';
import { CardControlsModule } from './modules/card-controls/card-controls.module';
import {SectionUrlListComponent} from './views/section-url-list/section-url-list.component';
import {CardInlineComponent} from './views/cards/card-inline/card-inline.component';

function getBaseUrl(): string {
    return document.getElementsByTagName('base')[0].href;
}

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        NavMenuComponent,
        SideMenuComponent,
        SourceTestingComponent,
        LoginComponent,
        RegisterComponent,
        UsersComponent,
        SubsystemsComponent,
        RegionsComponent,
        TopicsComponent,
        AppealsCitizensListComponent,
        AppealsOrganizationsComponent,
        ProtocolsComponent,
        VotesComponent,
        VoteItemRecordComponent,
        AppointmentsComponent,
        AppointmentRangeComponent,
        AppointmentsPageComponent,
        SettingsComponent,
        TerritoriesComponent,
        RolesComponent,
        RubricsComponent,
        RubricsMainComponent,
        SettingsCardComponent,
        CardComponent,
        SectionUrlListComponent,
        FilterControlComponent,
        SimpleCardComponent,
        AppointmentsCardComponent,
        AppealUlCardComponent,
        AppealCitizenCardComponent,
        UserCardComponent,
        CommentsTreeViewComponent,
        CommentRecordComponent,
        SimpleCardKeyValuesComponent,
        ConfirmWindowComponent,
        FilepondGqlComponent,
        FilepondHttpComponent,
        SubsystemCardComponent,
        SettingsPagesComponent,
        VoteCardComponent,
        RubricCardComponent,
        TopicCardComponent,
        SimpleEditFormComponent,
        QuestionItemComponent,
        BaseCardEditComponent,
        AccordionCardEditComponent,
        NewsComponent,
        NewsCardComponent,
        FeedbackListComponent,
        EnumVerbsListComponent,
        AppealsCitizensExecutorComponent,
        AppealFeedbackCardComponent,
        JournalViewComponent,
        ReportsListComponent,
        ReportCardComponent,
        CardInlineComponent,
    ],
    imports: [
        BrowserModule,
        CommonModule,
        HttpClientModule,
        NumericTextBoxModule,
        ModalModule.forRoot(),
        RouterModule.forRoot(
            ApplicationRouting
        ),
        FilePondModule,
        BrowserAnimationsModule,
        AccordionModule.forRoot(),
        AgGridModule.withComponents([]),
        JwtModule.forRoot({
            config: {
                tokenGetter: function tokenGetter() {
                    return localStorage.getItem('jwt');
                },
                allowedDomains: ['localhost:5001'],
                disallowedRoutes: [
                    getBaseUrl() + 'api/auth/login',
                    getBaseUrl() + 'api/auth/register'
                ]
            }
        }),
        ToastrModule.forRoot({
            // timeOut: 100000,
            // disableTimeOut: true,
        }), // ToastrModule added
        GraphQLModule,
        FormsModule,
        ReactiveFormsModule,
        DynamicInputsModule,
        CardControlsModule,
    ],
    exports: [
        LoginComponent
    ],
    providers: [
        // EnvironmentLoadService,
        DescriptorsLoaderService,
        {
            provide: APP_INITIALIZER,
            useFactory: EnvironmentLoadService.initializeEnvironmentConfig,
            multi: true,
            deps: [EnvironmentLoadService]
        },
        {
            provide: APP_INITIALIZER,
            useFactory: DescriptorsLoaderService.initialize,
            multi: true,
            deps: [DescriptorsLoaderService, EnvironmentLoadService]
        },
        AuthGuard,
        EmptyGuard,
        JwtHelperService,
        BsModalRef,
        CanDeactivateGuard,
    ],
    bootstrap: [AppComponent],
    entryComponents: [LoginComponent, RegisterComponent, AppComponent, /*UserComponent, RoleComponent*/],
})

export class AppModule {
    constructor(
        private injector: Injector,
    ) {
        AppUtils.setInjectorInstance(this.injector);
    }
}
