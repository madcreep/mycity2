

// import { InMemoryCache, ApolloLink, defaultDataIdFromObject } from 'apollo-boost';
import {AuthService} from './services/auth.service';
// import { APP_INITIALIZER, NgModule } from '@angular/core';


import { APP_INITIALIZER, NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import { ApolloLink, defaultDataIdFromObject, InMemoryCache } from '@apollo/client/core';
import { EnvSettings } from './model/settings/envsettings';
import { EnvironmentLoadService } from './services/environment-load.service';
import { onError } from "@apollo/client/link/error";
import { WebSocketLink } from "@apollo/client/link/ws";
import { Apollo } from 'apollo-angular';
import {InjectorInstance} from './helpers/app.static';

export const gqlCache = new InMemoryCache({
    typePolicies: {
        Settings: {
            keyFields: ['key'],
        }
    }
    // dataIdFromObject(responseObject) {
    //     switch (responseObject.__typename) {
    //         case 'Settings': {
    //             return responseObject['key'];
    //         }
    //         //   case 'Product': return `Product:${object.upc}`;
    //         //   case 'Person': return `Person:${object.name}:${object.email}`;
    //         //   default: return defaultDataIdFromObject(object);
    //     }
    //     return defaultDataIdFromObject(responseObject);
    // }
});

export function provideApollo(httpLink: HttpLink) {

    const authSrv = InjectorInstance.get(AuthService);
    const activeToken = authSrv.getToken();
    if (!authSrv.tokenIsOk(activeToken)) {
        authSrv._clearAuth();
    }

    const authLink = new ApolloLink((operation, forward) => {
        const token = authSrv.getToken(); // localStorage.getItem(STORAGE_TOKEN_NAME);
            operation.setContext({
            headers: token ? {
                Authorization: 'Bearer ' + token,
            } : {

            }
        });
        return forward(operation);
    });

    // Create an http link:
    const http = httpLink.create({
        uri: EnvSettings.GRAPHQL_ROOT
    });


    // const errorLink = onError(({ graphQLErrors, networkError }) => {
    //     if (graphQLErrors)
    //         graphQLErrors.forEach(({ message, locations, path }) =>
    //             console.log(
    //                 `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
    //             ),
    //         );

    //     if (networkError) console.log(`[Network error]: ${networkError}`);
    // });

    const errorLink = onError((data) => {
    // console.log("errorLink -> data", data)

        // { graphQLErrors, networkError }
        if (data.graphQLErrors) {
            // data['extraInfo'] = data.operation;
            data.graphQLErrors['operation'] = data.operation;
        }
        //     graphQLErrors.map(({ message, locations, path }) =>
        //         console.log(
        //             `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
        //         ),
        //     );
        // }
        // if (networkError) {
        //     console.log(`[Network error]: ${networkError}`);
        // }
    });



    // const httpLinkWithErrorHandling = ApolloLink.from([
    //     errorLink,
    //     httpLink,
    // ]);

    // let wsuri = 'wss://' + location.host + '/subscription';
    // if (DEBUGANGULAR) {
    //     // wsuri = 'wss://' + location.hostname + ':6000/subscription';
    //     wsuri = EnvSettings.SUBSCRIPTIONS_ROOT;
    // }
    // Create a WebSocket link:

    const ws = new WebSocketLink({
        uri: EnvSettings.SUBSCRIPTIONS_ROOT,
        options: {
            reconnect: true,
            connectionParams: {
                Authorization: 'Bearer ' + localStorage.getItem(authSrv.getToken() /* localStorage.getItem(STORAGE_TOKEN_NAME); */)
            },
        }

    });


    const linksplit = ApolloLink.split(

        ({ query }) => {
            const def = query.definitions.find(d => d.kind === 'OperationDefinition');
            if (def) {
                return def['operation'] === 'subscription';
            }
            return true;
        },
        ws,
        http,
    );


    const link = ApolloLink.from([authLink, errorLink, linksplit]);




    return {
        link,
        // linksplit,
        cache: gqlCache,
    };
}


@NgModule({
    imports: [
        HttpClientModule,
    ],
    providers: [
    ],

})
export class GraphQLModule {
    constructor(
        apollo: Apollo,
        httpLink: HttpLink,
        env: EnvironmentLoadService,
      ) {
        env.init().then(data => {
            apollo.create(provideApollo(httpLink));
        });
      }
}
