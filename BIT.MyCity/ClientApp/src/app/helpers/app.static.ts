import { Injector } from '@angular/core';

export let InjectorInstance: Injector;

export class AppUtils {

    static joinSeparator(array: string[], separator: string = ','): string {
        let result = '';
        for (let i = 0; i < array.length; i++) {
            if (array[i] !== null && array[i] !== undefined) {
                result += array[i];
            }

            if (i < array.length - 1) {
                result += separator;
            }
        }

        return result;
    }

    static addDays(date: Date, days: number): Date {
        const result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    static addMonth(date: Date, months: number): Date {
        const year = date.getFullYear();
        const month = date.getMonth();
        const day = date.getDate();
        const totalMonths = year * 12 + month;
        const calculatedMonths = totalMonths + months;
        const result = new Date(Math.floor(calculatedMonths / 12), calculatedMonths % 12, day);
        return result;
    }

    static addQuarter(date: Date, quarters: number): Date {
        return this.addMonth(date, quarters * 3);
    }
    static addYear(date: Date, years: number): Date {
        const year = date.getFullYear();
        const month = date.getMonth();
        const day = date.getDate();
        const calculatedYear = year + years;
        const result = new Date(calculatedYear, month, day);
        return result;
    }


    // return new Date(date.getTime() + days * 24 * 60 * 60 * 1000);

    static flattenArray(data: any[], arr = []) {
        for (let i = 0; i < data.length; i++) {
            const element = data[i];
            if (element instanceof Array) {
                arr.push(... this.flattenArray(element));
            } else {
                arr.push(element);
            }
        }
        return arr;
    }

    static keyValueToObject(values: { key: string, value: string }[], obj = {}): any {
        for (let i = 0; i < values.length; i++) {
            const el = values[i];
            this.appendAtPath(obj, el.key, el.value);

        }

        return obj;
    }

    // static expand(str, val = {}) {
    //     return str.split('.') reduceRight((acc, currentValue) => {
    //       return { [currentValue]: acc};
    //     }, val);
    // }

    static appendAtPath(obj: {}, key: string, value: string) {
        const path = key.split('.').filter(s => s);
        let cobj = obj;
        for (let i = 0; i < path.length; i++) {
            const element = path[i];

            if (cobj[element] === null) {
                cobj[element] = {};
            }

            if (cobj[element] === undefined) {
                if (i < path.length - 1) {
                    cobj[element] = {};
                } else {
                    cobj[element] = value;
                    return;
                }
            }

            cobj = cobj[element];
        }
    }

    static objectToKeyValue(object: any, path = '', list = []) {
        if (path) {
            path += '.';
        }
        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                const element = object[key];
                if (typeof element === 'object') {
                    list.concat(this.objectToKeyValue(element, path + key, list));
                } else {
                    list.push({ key: path + key, value: String(element) });
                }
            }
        }
        return list;
    }

    static mergeValues(values: any[], todeepest: boolean = false): any {
        const o = {};
        if (!todeepest) {
            values.map(v => Object.assign(o, v));
            return o;
        }

    }

    static dateTimeToStringValue(date: Date | string): string {
        return this.dateToStringValue(date, true);
    }

    static dateToStringValue(date: Date | string, joinTime = false): string {
        if (typeof date === 'string') {
            date = new Date(date);
        }

        if (date instanceof Date && !isNaN(date.getTime())) {
            let res = [AppUtils.digitpad(date.getDate()), AppUtils.digitpad(date.getMonth() + 1), date.getFullYear()].join('.');
            if (joinTime) {
                res += ' ' + [
                    AppUtils.digitpad(date.getHours()),
                    AppUtils.digitpad(date.getMinutes()),
                    AppUtils.digitpad(date.getSeconds())].join(':');
            }
            return res;
        } else {
            return null;
        }
    }

    static dateToStringIso(date: Date| string, joinTime = false) {
        // Pad a number to length using padChar
        function pad(number, length = 2, padChar = '0') {
            if (typeof length === 'undefined') length = 2;
            if (typeof padChar === 'undefined') padChar = '0';
            var str = "" + number;
            while (str.length < length) {
                str = padChar + str;
            }
            return str;
        }

        function getOffsetFromUTC() {
            const offset = new Date().getTimezoneOffset();
            return ((offset < 0 ? '+' : '-')
                + pad(Math.abs(offset / 60), 2)
                + ':'
                + pad(Math.abs(offset % 60), 2))
        };

            // [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.
        if (typeof date === 'string') {
            date = new Date(date);
        }

        if (joinTime) {
            let localIsoString = date.getFullYear() + '-'
                + pad(date.getMonth() + 1) + '-'
                + pad(date.getDate()) + 'T'
                + pad(date.getHours()) + ':'
                + pad(date.getMinutes()) + ':'
                + pad(date.getSeconds());
                if (date.getTimezoneOffset() === 0) {
                    localIsoString += 'Z';
                } else {
                    localIsoString += getOffsetFromUTC();
                }
            return localIsoString;

        }

        if (date instanceof Date && !isNaN(date.getTime())) {
            let res = [date.getFullYear(), AppUtils.digitpad(date.getMonth() + 1), AppUtils.digitpad(date.getDate())].join('-');
            if (joinTime) {
                res += 'T' + [
                    AppUtils.digitpad(date.getHours()),
                    AppUtils.digitpad(date.getMinutes()),
                    AppUtils.digitpad(date.getSeconds())].join(':')
                    + date.getTimezoneOffset()
                    ;
            }
            return res;
        } else {
            return null;
        }
    }

    // static dateToStringIso(date: Date) {
    //     const res = [
    //         date.getFullYear(),
    //         AppUtils.digitpad(date.getMonth() + 1),
    //         AppUtils.digitpad(date.getDate()),
    //     ].join('-');
    //     return res;
    // }

    static digitpad(n: number): string {
        return n < 10 ? '0' + n : '' + n;
    }

    static isObjectEmpty(v: any): boolean {

        if (!v) { return true; }
        for (const key in v) {
            if (v.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }

    static setInjectorInstance(injector: Injector) {
        InjectorInstance = injector;
    }

    static deepCopy(obj: any, saveKeys = '', replaceKeys = ''): any {
        let copy;

        // Handle the 3 simple types, and null or undefined
        if (null === obj || 'object' !== typeof obj) {
            return obj;
        }

        // Handle Date
        if (obj instanceof Date) {
            copy = new Date();
            copy.setTime(obj.getTime());
            return copy;
        }

        // Handle Array
        if (obj instanceof Array) {
            copy = [];
            for (let i = 0, len = obj.length; i < len; i++) {
                copy[i] = AppUtils.deepCopy(obj[i]);
            }
            return copy;
        }

        // Handle Object
        if (obj instanceof Object) {
            copy = {};
            for (const attr in obj) {
                if (obj.hasOwnProperty(attr)) {
                    if (saveKeys && replaceKeys) {
                        copy[attr.replace(saveKeys, replaceKeys)] = AppUtils.deepCopy(obj[attr]);
                    } else {
                        copy[attr] = AppUtils.deepCopy(obj[attr]);
                    }

                }
            }
            return copy;
        }

        throw new Error('Unable to copy obj! Its type isn\'t supported.');
    }

    static deleteValue(data: any, path: string) {
        this.setValueByPath(data, path, undefined);
        // const _path = path.split('.').filter(v => !!v);
        // const tail = _path.splice(-1, 1);
        // data = data || {};

        // const foo = (data, key) => {

        // }
    }

    static setValueByPath(data: any, path: string, value: any): any {
        const _path = path.split('.').filter(v => !!v);
        const tail = _path.splice(-1, 1);
        data = data || {};
        let elem = data;
        if (_path.length) {
            elem = AppUtils.getValueByPath(data, _path.join('.'), true);
        }
        const key = AppUtils.getKeyIndex(tail[0]);
        if (key.idx === undefined) {
            if (value === undefined) {
                delete elem[key.value];
            } else {
                elem[key.value] = value;
            }
        } else {
            if (!(elem[key.value] instanceof Array)) {
                elem[key.value] = [];
            }
            if (value === undefined) {
                delete elem[key.value][key.idx];
            } else {
                elem[key.value][key.idx] = value;
            }

        }
        return data;
    }

    static getValueByPath(data: any, path: string, initPath = false): any {

        const _path = path.split('.').filter(v => !!v);
        let elem = data;
        for (let i = 0; i < _path.length && (elem !== undefined && elem !== null); i++) { // dive deep while property exist
            const key = AppUtils.getKeyIndex(_path[i]);
            if (initPath) {
                if (key.idx === undefined) {
                    if (elem[key.value] === undefined) {
                        elem[key.value] = {};
                    }
                } else {
                    if (elem[key.value] === undefined) {
                        elem[key.value] = [];
                    }
                    if (elem[key.value][key.idx] === undefined) {
                        elem[key.value][key.idx] = {};
                    }
                }
            }
            elem = (key.idx === undefined) ? elem[key.value] : elem[key.value][key.idx];
        }
        return elem;
    }

    static getKeyIndex(key: string): { value: string, idx: number } {
        let aKey: string;
        let aIdx: number;
        if (key.indexOf('[') === -1) {
            aKey = key;
        } else {
            const tmpPath = key.split('[');
            aKey = tmpPath[0];
            aIdx = Number.parseInt(tmpPath[1], 10);
            if (isNaN(aIdx)) {
                aIdx = undefined;
            }
        }
        return { value: aKey, idx: aIdx };
    }

    static levelByDue(due: string): any {
        if (due) {
            return due.split('.').length - 1;
        }
        return 0;
    }

    static mergeObjects(...objs): any {
        if (objs.length === 0) {
            return {};
        } else if (objs.length === 1) {
            return objs[0];
        } else {
            const dst = objs[0];
            for (let i = 1; i < objs.length; i++) {
                const obj = objs[i];
                // if (obj instanceof Function) {
                //     objs[0] = obj;
                // } else {

                // }
                for (const key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        const val = obj[key];
                        if (val instanceof Function) {
                            dst[key] = val;
                        } else if (val instanceof Object) {
                            dst[key] = this.mergeObjects(dst[key] || {}, val);
                        } else {
                            dst[key] = val;
                        }
                    }
                }
            }
        }
        return objs[0];
    }

    static wordVariant(value: any): number {

        const mod100 = value % 100;

        if (mod100 >= 10 && mod100 <= 20) {
            return 0;
        }

        const mod10 = value % 10;
        switch (mod10) {
            case 1:
                return 1;
            case 2:
            case 3:
            case 4:
                return 2;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 0:
            default:
                return 0;
        }

    }

    static dateQuarterRange(date: Date): [Date, Date] {
        const year = date.getFullYear();
        const month = date.getMonth();
        if ( month >= 0 && month <= 2 ) {
            const firstDay = new Date(year, 0, 1);
            const lastDay = new Date(year, 3, 0);
            return [firstDay, lastDay];
        } else if ( month >= 3 && month <= 5 ) {
            const firstDay = new Date(year, 3, 1);
            const lastDay = new Date(year, 6, 0);
            return [firstDay, lastDay];
        } else if ( month >= 5 && month <= 8 ) {
            const firstDay = new Date(year, 6, 1);
            const lastDay = new Date(year, 9, 0);
            return [firstDay, lastDay];
        } else {
            const firstDay = new Date(year, 9, 1);
            const lastDay = new Date(year, 12, 0);
            return [firstDay, lastDay];
        }
    }
    static dateYearRange(date: Date): [Date, Date] {
        const year = date.getFullYear();
        const firstDay = new Date(year, 0, 1);
        const lastDay = new Date(year, 11, 31);
        return [firstDay, lastDay];
    }

    static dateMonthRange(date: Date): [Date, Date] {
        return [this.dateFirstDayOfMonth(date), this.dateLastDayOfMonth(date)];
    }

    static dateWeekRange(date: Date): [Date, Date] {
        const startDate = this.dateFirstDayOfWeek(date, 1);
        const endDate = this.addDays(startDate, 6);
        return [startDate, endDate];
    }

    static dateFirstDayOfMonth(date: Date): Date {
        const year = date.getFullYear();
        const month = date.getMonth();
        const firstDay = new Date(year, month, 1);
        return firstDay;
    }

    static dateLastDayOfMonth(date: Date): Date {
        const year = date.getFullYear();
        const month = date.getMonth();
        const lastDay = new Date(year, month + 1, 0);
        return lastDay;
    }

    static dateFirstDayOfWeek(dateObject: Date, firstDayOfWeekIndex: number): Date {
        const dayOfWeek = dateObject.getDay(),
            firstDayOfWeek = new Date(dateObject),
            diff = dayOfWeek >= firstDayOfWeekIndex ?
                dayOfWeek - firstDayOfWeekIndex :
                6 - dayOfWeek;

        firstDayOfWeek.setDate(dateObject.getDate() - diff);
        firstDayOfWeek.setHours(0, 0, 0, 0);

        return firstDayOfWeek;
    }
}
