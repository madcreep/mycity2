import {InputBase} from '../../modules/dynamic-inputs/types/input-base';
import {IReportSettingsControlType, IReportSingleSettings} from '../../../generated/types.graphql-gen';
import {E_FIELD_TYPE, IFieldDescriptor} from '../../model/datatypes/descriptors.interfaces';
import {VALIDATOR_TYPE, ValidatorsControl} from '../validators/validators-control';
import {ReportSettingsOptions, ReportSettingsOptionsRangeType, ReportSettingsOptionsTextMode, ReportSettingsSelectMode, ReportSettingsSelectOnlyMode} from './report-settings-options';
import {InputBuilder} from '../../services/input.service/input-builder';
import {ReportSettingOption} from './report-setting-option-value';
import {TreeListInputOption} from '../../modules/dynamic-inputs/types/tree-list-input';
import {DueHelper, IDueRecord} from '../due/due-helper';
import {DateRangeExtendedModes} from '../../modules/dynamic-inputs/components/data-range-extended/date-range-extended-modes';

export class ReportSettingsConverter {

    static toInputBase(value: IReportSingleSettings): InputBase<any> {
        const options = JSON.parse(value.options) as ReportSettingsOptions;
        const descriptor = this.makeInputDescriptorFor(value, options);
        if (!!descriptor) {
            const input = InputBuilder.inputForDescr(descriptor, '', descriptor.defaultValue);
            return input;
        }
        return null;
    }

    private static makeInputString(value: IReportSingleSettings, options: ReportSettingsOptions) {
        const descriptor: IFieldDescriptor = {
            type: options.textMode === ReportSettingsOptionsTextMode.singleRow ? E_FIELD_TYPE.string : E_FIELD_TYPE.text,
            key: value.name,
            title: value.title,
            defaultValue: value.value,
            validators: [],
            length: options.textLength
        };
        if (value.required) {
            descriptor.validators.push(ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED));
        }
        const input = InputBuilder.inputForDescr(descriptor, '', descriptor.defaultValue);
        return input;
    }

    private static makeInputDateRange(value: IReportSingleSettings, options: ReportSettingsOptions) {
        const descriptor: IFieldDescriptor = {
            type: options.textMode === ReportSettingsOptionsTextMode.singleRow ? E_FIELD_TYPE.string : E_FIELD_TYPE.text,
            key: value.name,
            title: value.title,
            defaultValue: value.value,
            validators: [],
            length: options.textLength
        };
        if (value.required) {
            descriptor.validators.push(ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED));
        }
        const input = InputBuilder.inputForDescr(descriptor, '', descriptor.defaultValue);
        return input;
    }

    private static makeInputDescriptorFor(reportSetting: IReportSingleSettings, options: ReportSettingsOptions): IFieldDescriptor {
        const descriptor: IFieldDescriptor = {
            key: reportSetting.name,
            title: reportSetting.title,
            type: undefined,
            defaultValue: reportSetting.value,
            validators: []
        };

        if (reportSetting.required) {
            descriptor.validators.push(ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED));
        }

        if (reportSetting.controlType === IReportSettingsControlType.CustomText) {
            descriptor.length = options.textLength;
            if (options.textMode === ReportSettingsOptionsTextMode.singleRow) {
                descriptor.type = E_FIELD_TYPE.string;
            } else if (options.textMode === ReportSettingsOptionsTextMode.multiRows) {
                descriptor.type = E_FIELD_TYPE.text;
            }
        } else if (reportSetting.controlType === IReportSettingsControlType.DateRange) {
            const selectedRange = options.dateRangeMode ?? DateRangeExtendedModes.All;
            descriptor.type = E_FIELD_TYPE.dateRangeExt;
            descriptor.options = options.availableDateRanges ?? [];
            descriptor.mode = selectedRange;
            descriptor.defaultValue = this.decodeArray(reportSetting.value, ':')
            .map( stringDate => {
                const dateNum = Date.parse(stringDate);
                const date = isNaN(dateNum) ? null : new Date(dateNum);
                return date;
            });

        } else if (reportSetting.controlType === IReportSettingsControlType.SingleCheckbox) {
            descriptor.type = E_FIELD_TYPE.boolean;
        } else if (reportSetting.controlType === IReportSettingsControlType.NumberRange) {
            descriptor.minValue = options.minValue;
            descriptor.maxValue = options.maxValue;
            if (options.rangeType === ReportSettingsOptionsRangeType.dual) {
                descriptor.type = E_FIELD_TYPE.numberRange;
                descriptor.length = options.decimalPlaces ?? 0;
                descriptor.defaultValue = this.decodeArray(reportSetting.value, ':')
                    .map(value => Number.parseFloat(value));
            } else {
                descriptor.type = E_FIELD_TYPE.number;
            }
        } else if (reportSetting.controlType === IReportSettingsControlType.DiscreteSelector) {
            descriptor.mode = { countElementsInRow: options.countElementsInRow, width: options.elementWith };
            if (options.selectorType === 0) {
                descriptor.type = E_FIELD_TYPE.radioGroup;
                descriptor.options = this.decodeValues(reportSetting.values);
                const option = descriptor.options.find(item => item.value === descriptor.defaultValue);
                if (!!option) {
                    option.checked = true;
                }
            } else if (options.selectorType === 2) {
                descriptor.type = E_FIELD_TYPE.select;
                descriptor.options = this.decodeValues(reportSetting.values);
            } else {
                descriptor.type = E_FIELD_TYPE.checkBoxGroup;
                descriptor.defaultValue = this.decodeArray(reportSetting.value);
                descriptor.options = this.decodeValues(reportSetting.values);
                descriptor.defaultValue.forEach(checkedValue => {
                    const option = descriptor.options.find(item => item.value === checkedValue);
                    if (!!option) {
                        option.checked = true;
                    }
                });
            }
        } else if (reportSetting.controlType === IReportSettingsControlType.ListSelect) {
            if (options.selectMode === ReportSettingsSelectOnlyMode.multi) {
                descriptor.type = E_FIELD_TYPE.multiselect;
            } else {
                descriptor.type = E_FIELD_TYPE.select;
            }
            descriptor.options = this.decodeValues(reportSetting.values);
            // descriptor.options = this.decodeValues('1:Параметр 1:1:0.:False|2:Параметр 2:2:0.:False|3:Параметр 3:3:0.:False|4:Параметр 4:4:0.:False');
        } else if (reportSetting.controlType === IReportSettingsControlType.TreeSelect) {
            descriptor.type = E_FIELD_TYPE.treeList;
            descriptor.defaultValue = this.decodeArray(reportSetting.value);
            descriptor.mode = {
                selectOnlyMode: options.selectOnlyMode ?? ReportSettingsSelectMode.all,
                selectMode: options.selectMode ?? ReportSettingsSelectOnlyMode.multi,
            };
            const treeElementList = this.decodeValues(reportSetting.values)
                .sort( (x, y) => {
                    return x.weight === y.weight
                        ? 0
                        : x.weight > y.weight
                            ? 1
                            : -1;
                });
            const tree = this.makeTreeForElements(treeElementList);
            descriptor.options = tree;
        } else {
            console.log('Unsupported report setting');
            console.log(reportSetting);
        }
        return descriptor;
    }

    private static decodeValues(value: String): TreeListInputOption[] {
        // key:name:weight:due:isNode
        const records = value.split('|');
        const decodedRecords = records.map(record => ReportSettingOption.ToModel(record))
            .map(
                model => ({
                    title: model.name,
                    value: model.key,
                    weight: model.weight,
                    id: model.key,
                    due: model.due,
                    isNode: model.isNode
                } as TreeListInputOption)
            );
        return decodedRecords;
    }

    private static makeTreeForElements(elementList: TreeListInputOption[]): TreeListInputOption[] {
        return DueHelper.makeTreeForElements(elementList as IDueRecord[]) as TreeListInputOption[];
    }

    private static decodeArray(value: string, separator: string = '|'): any[] {
        return value?.split(separator) ?? [];
    }
}

// {
//     "data": {
//     "reportSettings": {
//         "reportId": "0380493f-141d-4a13-9ef8-0f6d2bca47b3",
//             "parameters": [
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "CUSTOM_TEXT",
//                 "name": "CUSTOM_TEXT1",
//                 "options": "{\"textMode\":\"SingleRow\",\"textLength\":255,\"textAreaRows\":null}",
//                 "required": false,
//                 "title": "Введите строку",
//                 "value": "Всем привет!",
//                 "values": null,
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "CUSTOM_TEXT",
//                 "name": "CUSTOM_TEXT2",
//                 "options": "{\"textMode\":\"MultiRows\",\"textLength\":2000,\"textAreaRows\":5}",
//                 "required": true,
//                 "title": "Введите тест",
//                 "value": "Всем привет!",
//                 "values": null,
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "DATE_RANGE",
//                 "name": "DATE_RANGE1",
//                 "options": "{\"dateRangePlusN\":0,\"dateRangeMode\":\"Range\",\"availableDateRanges\":null}",
//                 "required": true,
//                 "title": "Диапазон дат (обязательный)",
//                 "value": "null:null",
//                 "values": null,
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "DATE_RANGE",
//                 "name": "DATE_RANGE2",
//                 "options": "{\"dateRangePlusN\":-3,\"dateRangeMode\":\"PlusNAndLater\",\"availableDateRanges\":[\"Range\",\"CurrentDayPlusN\",\"PlusNAndLater\",\"PlusNAndBefore\",\"CurrentMonth\",\"LastMonth\",\"PrevMonth\"]}",
//                 "required": false,
//                 "title": "Диапазон дат (не обязательный)",
//                 "value": "null:null",
//                 "values": null,
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "DATE_RANGE",
//                 "name": "DATE_RANGE3",
//                 "options": "{\"dateRangePlusN\":0,\"dateRangeMode\":\"Range\",\"availableDateRanges\":[\"Range\"]}",
//                 "required": false,
//                 "title": "Диапазон дат (предустановленный)",
//                 "value": "23/02/2023:null",
//                 "values": null,
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "SINGLE_CHECKBOX",
//                 "name": "IS_SHORT",
//                 "options": null,
//                 "required": false,
//                 "title": "Краткий вывод",
//                 "value": "true",
//                 "values": null,
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "DISCRETE_SELECTOR",
//                 "name": "SELECTOR_1",
//                 "options": "{\"countElementsInRow\":2,\"elementWith\":2,\"selectorType\":1}",
//                 "required": true,
//                 "title": "Селектор 1 (Checkbox)",
//                 "value": "2|4",
//                 "values": "1:Параметр 1:1:0.:False|2:Параметр 2:2:0.:False|3:Параметр 3:3:0.:False|4:Параметр 4:4:0.:False",
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "DISCRETE_SELECTOR",
//                 "name": "SELECTOR_2",
//                 "options": "{\"countElementsInRow\":4,\"elementWith\":3,\"selectorType\":0}",
//                 "required": true,
//                 "title": "Селектор 2 (Radio)",
//                 "value": "3",
//                 "values": "1:Параметр 1:1:0.:False|2:Параметр 2:2:0.:False|3:Параметр 3:3:0.:False|4:Параметр 4:4:0.:False",
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "DISCRETE_SELECTOR",
//                 "name": "SELECTOR_3",
//                 "options": "{\"countElementsInRow\":4,\"elementWith\":3,\"selectorType\":2}",
//                 "required": true,
//                 "title": "Селектор 2 (Combo)",
//                 "value": "4",
//                 "values": "1:Параметр 1:1:0.:False|2:Параметр 2:2:0.:False|3:Параметр 3:3:0.:False|4:Параметр 4:4:0.:False",
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "NUMBER_RANGE",
//                 "name": "NUMBERS_1",
//                 "options": "{\"maxValue\":100.0,\"minValue\":0.0,\"rangeType\":\"Dual\",\"decimalPlaces\":2}",
//                 "required": false,
//                 "title": "Диапазон чисел",
//                 "value": "11.11:33.33",
//                 "values": null,
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": null,
//                 "allSelected": null,
//                 "controlType": "NUMBER_RANGE",
//                 "name": "NUMBER_2",
//                 "options": "{\"maxValue\":null,\"minValue\":null,\"rangeType\":\"Single\",\"decimalPlaces\":0}",
//                 "required": false,
//                 "title": "Число",
//                 "value": "10:null",
//                 "values": null,
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": true,
//                 "allSelected": false,
//                 "controlType": "LIST_SELECT",
//                 "name": "LIST_1",
//                 "options": "{\"selectMode\":\"Multi\"}",
//                 "required": true,
//                 "title": "Справочник 1 (List)",
//                 "value": "",
//                 "values": "",
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": false,
//                 "allSelected": false,
//                 "controlType": "LIST_SELECT",
//                 "name": "LIST_2",
//                 "options": "{\"selectMode\":\"Single\"}",
//                 "required": false,
//                 "title": "Справочник 2 (List single)",
//                 "value": "",
//                 "values": "",
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": false,
//                 "allSelected": false,
//                 "controlType": "TREE_SELECT",
//                 "name": "TREE_1",
//                 "options": "{\"selectOnlyMode\":\"Both\",\"selectMode\":\"Multi\"}",
//                 "required": true,
//                 "title": "Иерархический справочник",
//                 "value": "",
//                 "values": "3:Элемент 0.3.:2:0.3.:True|5:Элемент 0.2.:1:0.2.:True|12:Элемент 0.1.:3:0.1.:True|34:Элемент 0.3.3.:2:0.3.3.:False|87:Элемент 0.3.2.:1:0.3.2.:True|22:Элемент 0.3.2.1.:2:0.3.2.1.:False|1234563:Элемент 0.3.1.:3:0.3.1.:True|654:Элемент 0.3.1.1.:200:0.3.1.1.:False",
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": false,
//                 "allSelected": false,
//                 "controlType": "TREE_SELECT",
//                 "name": "TREE_2",
//                 "options": "{\"selectOnlyMode\":\"Leaves\",\"selectMode\":\"Single\"}",
//                 "required": true,
//                 "title": "Иерархический справочник (только один лист)",
//                 "value": "",
//                 "values": "3:Элемент 0.3.:2:0.3.:True|5:Элемент 0.2.:1:0.2.:True|12:Элемент 0.1.:3:0.1.:True|34:Элемент 0.3.3.:2:0.3.3.:False|87:Элемент 0.3.2.:1:0.3.2.:True|22:Элемент 0.3.2.1.:2:0.3.2.1.:False|1234563:Элемент 0.3.1.:3:0.3.1.:True|654:Элемент 0.3.1.1.:200:0.3.1.1.:False",
//                 "__typename": "ReportSingleSettings"
//             },
//             {
//                 "allEnable": false,
//                 "allSelected": false,
//                 "controlType": "TREE_SELECT",
//                 "name": "TREE_3",
//                 "options": "{\"selectOnlyMode\":\"Nodes\",\"selectMode\":\"Single\"}",
//                 "required": true,
//                 "title": "Иерархический справочник (только одну вершину)",
//                 "value": "",
//                 "values": "3:Элемент 0.3.:2:0.3.:True|5:Элемент 0.2.:1:0.2.:True|12:Элемент 0.1.:3:0.1.:True|34:Элемент 0.3.3.:2:0.3.3.:False|87:Элемент 0.3.2.:1:0.3.2.:True|22:Элемент 0.3.2.1.:2:0.3.2.1.:False|1234563:Элемент 0.3.1.:3:0.3.1.:True|654:Элемент 0.3.1.1.:200:0.3.1.1.:False",
//                 "__typename": "ReportSingleSettings"
//             }
//         ],
//             "__typename": "ReportSettings"
//     }
// }
// }
