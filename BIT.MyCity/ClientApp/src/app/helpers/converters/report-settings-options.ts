import {DateRangeExtendedModes} from '../../modules/dynamic-inputs/components/data-range-extended/date-range-extended-modes';

export enum ReportSettingsOptionsTextMode {
    singleRow = 'SingleRow',
    multiRows = 'MultiRows'
}
export class ReportSettingsOptions {
    // {\"textMode\":\"SingleRow\",\"textLength\":255,\"textAreaRows\":null}
    textMode: ReportSettingsOptionsTextMode;
    textLength: number;
    textAreaRows: any;

    /// 0 - radiobox, 1 - checkbox group
    selectorType?: number;
    countElementsInRow?: number;
    elementWith?: number;

    rangeType?: ReportSettingsOptionsRangeType;
    minValue: number;
    maxValue: number;
    decimalPlaces: number;
    selectMode: ReportSettingsSelectOnlyMode;
    availableDateRanges?: DateRangeExtendedModes[];
    dateRangeMode?: DateRangeExtendedModes;
    selectOnlyMode?: ReportSettingsSelectMode;
}

export enum ReportSettingsOptionsRangeType {
    dual = 'Dual',
    single = 'Single'
}

export enum ReportSettingsSelectOnlyMode {
    single = 'Single',
    multi = 'Multi'
}

export enum ReportSettingsSelectMode {
    leaves = 'Leaves',
    nodes = 'Nodes',
    all = 'Both'
}
