

export interface IReportSettingValueModel {
    key: string | number;
    name: string;
    weight: number;
    due: string;
    isNode: boolean;
}


export class ReportSettingOption {

    // key:name:weight:due:isNode
    public static ToModel(stringValue: string, separator: string = ':'): IReportSettingValueModel {
        // key:name:weight:due:isNode
        const columns = stringValue.split(separator);

        const optionValue: IReportSettingValueModel = {
            key: columns[0],
            name: columns[1],
            weight: Number.parseInt(columns[2], 10),
            due: columns[3],
            isNode: columns[4].toLowerCase() === 'true',
        };
        return optionValue;
    }
}

