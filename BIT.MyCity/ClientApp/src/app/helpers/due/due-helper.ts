import {TreeListInputOption} from '../../modules/dynamic-inputs/types/tree-list-input';


export interface IDueRecord {
    due: string;
    children?: IDueRecord[];
}

export class DueHelper {
    static Constants = {
        rootDue: '0.',
        dueSeparator: '.'
    };

    static makeTreeForElements(elementList: IDueRecord[]): IDueRecord[] {
        const dictionary: Record<string, IDueRecord> = {};
        const rootDue = DueHelper.Constants.rootDue;
        elementList.reduce(
            (acc, value, index) => {
                acc[value.due] = value;
                return acc;
            }
            , dictionary
        );

        if (!dictionary[rootDue]) {
            dictionary[rootDue] = { due: rootDue, children: [] };
        }

        elementList.forEach( value => {
           const parentDue = this.parentDueFor(value.due);
           const parentElement = dictionary[parentDue];
           if (!!parentElement) {
               if (!parentElement.children) {
                   parentElement.children = [];
               }
               parentElement.children.push(value);
           }
        });

        return dictionary[rootDue].children;
    }

    static parentDueFor(due: string): string {
        const dueSeparator = DueHelper.Constants.dueSeparator;
        const dueWithoutLastDot = due[due.length - 1] === dueSeparator
            ? due.substr(0, due.length - 1)
            : due;
        const indexOfLastDot = dueWithoutLastDot.lastIndexOf(dueSeparator);
        const parentDue = due.substring(0, indexOfLastDot) + dueSeparator;
        return parentDue;
    }
}
