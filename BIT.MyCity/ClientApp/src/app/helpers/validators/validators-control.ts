import { ValidatorFn, AbstractControl, Validators } from '@angular/forms';

export enum VALIDATOR_TYPE {
    EXTENSION_DOT,
    REQUIRED,
    PATTERN,
    NON_EMPTY_ARRAY,
    MIN_LENGTH,
    REQUIRED_IF_NOTCHECKED,
    VALIDATORSIFNOTEMPTY
}

export interface ValidatorOptions {
    ignoreCase?: boolean;
    ignoreWhiteSpace?: boolean;
}
export class ValidatorsControl {
    static optionInvalidValue = -0xDEADBEEF;

    static appendValidator(control: AbstractControl, fn: ValidatorFn | ValidatorFn[]): any {
        const v = Array.isArray(fn) ? fn : [fn];

        if (control.validator) {
            v.push(control.validator);
        }
        control.setValidators(v);
        control.updateValueAndValidity();
        if (!control.valid) {
            control.markAsDirty();
        }
    }

    static existValidator(type: VALIDATOR_TYPE, message: string = '', obj: any = null): ValidatorFn {
        switch (type) {
            case VALIDATOR_TYPE.EXTENSION_DOT:
                return (control: AbstractControl): { [key: string]: any } => {
                    const v = control.value;
                    if (v && v !== '' && v.indexOf('.') === -1) {
                        return { valueError: message || 'Поле должно иметь символ "."' };
                    }
                };
            case VALIDATOR_TYPE.REQUIRED:
                return (control: AbstractControl): { [key: string]: any } => {
                    const res = Validators.required(control);
                    if (res) {
                        return { valueError: message || 'Обязательное поле' };
                    }
                };

            case VALIDATOR_TYPE.PATTERN:
                return (control: AbstractControl): { [key: string]: any } => {
                    const res = Validators.pattern(obj).call(this, control);
                    if (res) {
                        return { valueError: message || 'Неверное значение' };
                    }
                };
            case VALIDATOR_TYPE.NON_EMPTY_ARRAY:
                return (control: AbstractControl): { [key: string]: any } => {
                    const res = control.value && control.value.length;
                    if (!res) {
                        return { valueError: message || 'Неверное значение' };
                    }
                };
            case VALIDATOR_TYPE.MIN_LENGTH:
                return (control: AbstractControl): { [key: string]: any } => {
                    const res = control.value && control.value.length;
                    if (!res || res < obj) {
                        return { valueError: message || 'Неверное значение' };
                    }
                };
            case VALIDATOR_TYPE.REQUIRED_IF_NOTCHECKED:
                return (control: AbstractControl): { [key: string]: any } => {
                    const cbcontrol = <AbstractControl>obj;
                    if (cbcontrol.value) {
                        return null;
                    } else {
                        return this.existValidator(VALIDATOR_TYPE.REQUIRED, message, null)(control);
                    }
                };
            case VALIDATOR_TYPE.VALIDATORSIFNOTEMPTY:
                return (control: AbstractControl): { [key: string]: any } => {
                    const res = control.value && control.value.length;
                    if (res) {
                        for (const v of obj as ValidatorFn[]) {
                            const validator_result = v(control);
                            if (validator_result) {
                                return validator_result;
                            }
                        }
                        return null;
                    }
                };
            default:
                break;
        }
    }

    static onlyOneOfControl(control1: any, control2: any, err: string): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            if (control1 && control2) {
                if (control1.value && control2.value) {
                    return { valueError: err };
                }
            }
            return null;
        };
    }

    static controlsNonUniq(control1: any, control2: any, err: string, opts: ValidatorOptions): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            if (control1 && control2) {
                if (control1.value && control2.value) {
                    let v1: string = String(control1.value);
                    let v2: string = String(control2.value);
                    if (opts && opts.ignoreCase) {
                        v1 = v1.toUpperCase();
                        v2 = v2.toUpperCase();
                    }
                    if (opts && opts.ignoreWhiteSpace) {
                        v1 = v1.trim();
                        v2 = v2.trim();
                    }
                    if (v1 === v2) {
                        return { valueError: err };
                    }
                }
            }
            return null;
        };
    }

    static optionsCorrect(options: any[]) {
        return (control: AbstractControl): { [key: string]: any } => {
            if (!control.value) {
                return null;
            }
            const t = options.find(o => String(o.value) === String(control.value));
            if (!t) {
                return { valueError: 'Deleted option' };
            }
            return null;
        };
    }

    static optionCustomValidate(err: string) {
        return (control: AbstractControl): { [key: string]: any } => {
            if (control.value === ValidatorsControl.optionInvalidValue) {
                return { valueError: err };
            }
            return null;
        };
    }
}
