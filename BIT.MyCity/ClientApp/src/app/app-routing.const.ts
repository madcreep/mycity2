import { HomeComponent } from './home/home.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './views/login/login.component';
import { CardComponent } from './views/cards/card.component';
import { MenuPagesRoutes } from './views/menu-pages/menu-pages.const';
import { CanDeactivateGuard } from './guards/can-deactivate.guard';
import {CLAIM_MU_USERS, CLAIM_SA_ADMIN} from './services/claims.const';
import {SectionUrlListComponent} from './views/section-url-list/section-url-list.component';
import {SettingsComponent} from './views/menu-pages/settings/settings.component';

export const ApplicationRouting = [
    {
        path: '', canActivate: [AuthGuard], children: [
            { path: '', component: HomeComponent, pathMatch: 'full' },
            { path: 'home', component: HomeComponent, pathMatch: 'full' },

            ...MenuPagesRoutes,

            { path: 'sections/:id', component: SectionUrlListComponent,
                link: 'sections/:id',
                id: 'sections', title: 'Настройки', class: 'fa fa-info-circle fa-fw',
                canDeactivate: [CanDeactivateGuard] },

            // { path: ':entityName/:id', component: CardComponent,
            //     isEntity: true,
            //     data: { needClaims: [CLAIM_MU_USERS]},
            // canDeactivate: [CanDeactivateGuard] },
        ],
    },
    { path: 'login', component: LoginComponent },
    { path: 'logout', component: LoginComponent },
    { path: '**' , redirectTo : '/home'},
];


// {
//     id: 'settings', title: 'Настройки', class: 'fa fa-cogs fa-fw',
//     route: {
//     component: SettingsComponent,
//         data: {needClaims: [CLAIM_SA_ADMIN/*CLAIM_MU_SETTINGS*/]},
//     canActivate: [AuthGuard],
//         canDeactivate: [CanDeactivateGuard],
// },
// }, {
