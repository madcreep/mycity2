import {CitizenViewSettings} from './view.settings';


export class LinkObject {
    title: string;
    url: string;
}

export class  LinksSection {
    title: string;
    urls: LinkObject[] = [];
}

export class EnvSettings {

    static GRAPHQL_ROOT = 'https';
    static SUBSCRIPTIONS_ROOT = 'wss';
    static ATTACHES_ROOT = 'https';
    static Citizens: CitizenViewSettings;
    static URLsSections: LinksSection[];
    static load(config) {
        this.GRAPHQL_ROOT = EnvSettings.makeUrlFor(config.GRAPHQL_ROOT, '/graphql');
        this.SUBSCRIPTIONS_ROOT = EnvSettings.makeUrlFor(config.SUBSCRIPTIONS_ROOT, '/subscription');
        this.ATTACHES_ROOT = EnvSettings.makeUrlFor(config.ATTACHES_ROOT, '/api/file');
        this.Citizens = config.Citizens || new CitizenViewSettings;
        this.URLsSections = config.URLsSections || [];
    }
    static makeUrlFor(cfg, defaultpath) {
        const justprotocol = ['http', 'https', 'ws', 'wss'].findIndex(c => c === cfg);
        if (justprotocol !== -1) {
            return cfg + '://' + window.location.host + defaultpath;
        }
        return cfg;
    }
}
