import {IEditPageDescriptor} from '../datatypes/descriptors.interfaces';

export class CitizenViewSettings {
    /* Список полей для отображения в таблице */
    viewList: string[] | undefined = undefined;

    /* Список полей для отображения в фильтрах таблицы */
    filterList: string[] | undefined = undefined;

    /* форма редактирования карточки */
    editList: IEditPageDescriptor[] | undefined = undefined;
}
