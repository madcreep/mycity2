import { IFieldDescriptor, E_FIELD_TYPE, E_ACTION_TYPE } from './descriptors.interfaces';
import { BaseTableDescriptor } from './descriptors.const';
import { FT_DATECREATED, FT_ID, FT_WEIGHT, FT_DELETED, FT_NAME, FT_SNAME, FT_TEXT, FT_DATEUPDATED, FT_ATTACHED_FILES } from './fields.const';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';



const VoteFieldsColumnDefs: IFieldDescriptor[] = [
    FT_ID,
    FT_DATECREATED,
    FT_DATEUPDATED,
    FT_WEIGHT,
    FT_DELETED,
    FT_NAME,
    // FT_SNAME,
    FT_TEXT,

    {
        key: 'mode',
        title: 'Режим',
        type: E_FIELD_TYPE.select,
        options: [
            { value: 'ONE_SELECT', title: 'Можно голосовать только за 1 пункт' },
            { value: 'MANY_SELECT', title: 'Можно голосовать за несколько пунктов' },
        ],
        defaultValue: 'ONE_SELECT',
    }, {
        key: 'modeLimitValue',
        title: 'Максимальное число пунктов', // в режиме MANY_SELECT
        type: E_FIELD_TYPE.number,
        defaultValue: 1,
    }, {
        key: 'offerEnabled',
        title: 'Можно предложить свой вариант',
        type: E_FIELD_TYPE.boolean,
        defaultValue: false,
    }, {
        key: 'voteItems',
        title: '',
        type: E_FIELD_TYPE.checkBoxGroup,
        defaultValue: [],
        hidden: true,
    },
    Object.assign({}, FT_ATTACHED_FILES, {
        onGetValue: BaseTableDescriptor.valueToIdsFn('vote.attachedFiles'),
    }), {
        key: 'importSection',
        type: E_FIELD_TYPE.text,
        title: 'Построчный импорт',
        hidden: true,
        readonly: true,
    }



];


export const VOTE_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'vote';
    title = 'Голосования';
    entityName = 'vote';
    updateEntityName = 'updateVote';

    card_action_list = [
        E_ACTION_TYPE.close,
        E_ACTION_TYPE.apply,
    ];


    fields = VoteFieldsColumnDefs;
    edit_list = [
        {
            title: 'Основные', fields: [
                FT_NAME.key,
                ['mode', 'modeLimitValue', ],
                [ 'offerEnabled'],
                FT_TEXT.key,
                FT_ATTACHED_FILES.key,
                'textResult',
                'importSection',
                'voteItems',
            ]
        },
        // {
        //     title: 'Голосования', fields: [
        //         [FT_SNAME.key, 'modeLimitValue', ],
        //         ['mode', 'offerEnabled'],
        //         FT_TEXT.key,
        //         FT_ATTACHED_FILES.key,
        //         'textResult',
        //         'voteItems',
        //     ]
        // }

        // {
        //     title: MESSAGES_TAB,
        //     fields: []
        // },
    ];
    // insert_list = [
    //     { title: 'Основные', fields: [
    //         'state',
    //         FT_NAME.key,
    //         [FT_SNAME.key, 'modeLimitValue', ],
    //         ['mode', 'offerEnabled'],
    //         FT_TEXT.key,
    //         FT_ATTACHED_FILES.key,
    //         'textResult',
    //         'voteItems',
    //     ] },
    // ];

    view_list = [FT_ID.key, FT_DATECREATED.key, FT_NAME.key, FT_SNAME.key, 'state'];
    filter_layout = ['state'];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            entity: this.entityName,
            id: this.id,
            create: gqlSdk.createVote,
            list: gqlSdk.getVotesList,
            query: gqlSdk.getVote,
            update: gqlSdk.updateVote,
            // updateKey: 'updateVote',
        };
    }

};


function statusFormatter(data) {
    const result = '';
    // const vote: IVote = data.data;
    // switch (vote.state) {
    //     case IStatusEnum.Draft: {
    //         return 'Черновик';
    //     }
    //     case IStatusEnum.IsPlanned: {
    //         if (vote.startAt) {
    //             return 'Запуск ' + AppUtils.dateTimeToStringValue(vote.startAt);
    //         }
    //         return 'Ожидает запуска';
    //     }
    //     case IStatusEnum.InProgress: {
    //         if (vote.endAt) {
    //             return 'Активно. Завершение ' + AppUtils.dateTimeToStringValue(vote.endAt);
    //         }
    //         return 'Активно';
    //     }
    //     case IStatusEnum.IsDone: {
    //         if (vote.archiveAt) {
    //             return 'Завершено. Архивирование ' + AppUtils.dateTimeToStringValue(vote.archiveAt);
    //         }
    //         return 'Завершено';
    //     }
    //     case IStatusEnum.IsArchived: {
    //         return 'Архив';
    //     }
    //     default: {
    //         break;
    //     }
    // }

    return result;
}
