import { BaseTableDescriptor } from './descriptors.const';
import { IFieldDescriptor } from './descriptors.interfaces';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';


const SettingsDefs: IFieldDescriptor[] = [
            // maked in SettingsComponent
];
export const SETTINGS_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'settings';
    entityName = 'settings';
    title = 'Настройки';
    fields = SettingsDefs;
    edit_list = [
    ];
    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return null;
    }
}();
