import { FT_ID, FT_DESCRIPTION, FT_NAME, FT_WEIGHT } from './fields.const';
import { BaseTableDescriptor } from './descriptors.const';
import { IFieldDescriptor, E_FIELD_TYPE as E_FIELD_TYPE } from './descriptors.interfaces';
import { GridFormatters } from './grid-formatters';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';



const RoleFieldsDefs: IFieldDescriptor[] = [

    FT_ID,
    FT_DESCRIPTION,
    FT_WEIGHT,
    Object.assign ({}, FT_NAME, {
        title: 'Роль',
    }),
    {
        key: 'isAdminRegister',
        title: 'Возможность назначать создаваемым пользователям',
        type: E_FIELD_TYPE.boolean,
        readonly: true,
        defaultValue: true,
        gridOption: {
            width: 120,
            cellRenderer: GridFormatters.booleanFormatter
        }
    }, {
        key: 'editable',
        title: 'Возможность редактирования',
        type: E_FIELD_TYPE.boolean,
        gridOption: {
            width: 120,
            cellRenderer: GridFormatters.booleanFormatter
        }
    }, {
        key: 'disconnected',
        title: 'Признак блокированной роли',
        type: E_FIELD_TYPE.boolean,
        gridOption: {
            width: 120,
            cellRenderer: GridFormatters.booleanFormatter
        }
    }, {
        title: 'Разрешения',
        key: 'claims',
        type: E_FIELD_TYPE.claimSelect,

        editOptions: {
            arrayFilter: 'ROLE',
            arraySource: {
                arrayList: 'claims',
                arrayTitle: (opt) => {
                    return opt.description + ' (' + opt.claimType + '-' + opt.claimEntity + ')';
                },
                arrayKey: 'claimEntity',
            }
        },

    },

];


export const ROLE_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'role';
    entityName = 'role';
    updateEntityName = 'updateRole';
    title = 'Роли';
    fields = RoleFieldsDefs;
    edit_list = [
        { title: 'Основные', fields: [
            // 'isAdminRegister',

        'disconnected', 'name', FT_DESCRIPTION.key, 'claims'] },
    ];

    view_list = ['id', 'name', FT_DESCRIPTION.key,
    // 'isAdminRegister',
    'disconnected', 'editable'];

    recordTitleField = FT_DESCRIPTION.key;
    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            list: gqlSdk.getRoleList,
            create: gqlSdk.createRole,
            query: gqlSdk.getRole,
            update: gqlSdk.updateRole,
            delete: gqlSdk.deleteRole,
        };
    }

}();


