import { ValidatorFn } from '@angular/forms';
import { IOrderByItemType } from 'src/generated/types.graphql-gen';
import { InputBase } from '../../modules/dynamic-inputs/types/input-base';
import { IClaim } from '../../services/claims.const';


export enum E_FIELD_TYPE {
    string,
    text,
    number,
    numberRange,
    date,
    boolean,
    buttons,
    dictionaryLink,
    select,
    multiselect,
    dateRange,
    dateRangeExt,
    claimSelect,
    moderatorsSubS,
    filesPond,
    table,
    datetime,
    mapPoint,
    separator,
    textRich,
    radioGroup,
    checkBoxGroup,
    list,
    treeList,
}




export enum E_ACTION_TYPE {
    cancel,
    save,
    add,
    delete,
    undelete,
    protocol,
    reject,
    exportExcel,
    accept,
    refresh,
    start,
    stop,
    import,
    archive,
    apply,
    close,
    moveUp,
    moveDown,
    register,
    send,
}

export interface IGridOptions {
    // https://www.ag-grid.com/javascript-grid-column-properties/
    headerName?: string;
    field?: string;
    columnGroupShow?: string;
    sortable?: boolean;
    cellRenderer?: string | Function;
    suppressMenu?: boolean;
    checkboxSelection?: boolean;
    pinned?: string;
    width?: number;
    children?: any[];


    /// ....
    minWidth?: number;
    maxWidth?: number;
    resizable?: boolean;
    valueFormatter?: any;
    autoHeight?: boolean;
    cellStyle?: any;
    cellClass?: string;
    hide?: boolean;
    visible?: boolean;


    // is_ld_col?: boolean;
}

export interface IFieldEditOptionsArray {
    arrayList: string; // список откуда брать
    arrayTitle: string | Function;
    arrayKey: string;
    orderBy?: IOrderByItemType[];
    arrayFilter?: any;
}
export interface ITableGridOptions {
    newRow?: (data: any) => any ;
    columns?: any; // columnDefs for ad-grid
    orderBy?: string;
}

export interface IFieldEditOptions {
    // Array
    arraySource?: IFieldEditOptionsArray;
    arrayFilter?: string; // фильтр только для claimselector - как то улучшить.
    arrayGrid?: ITableGridOptions;
    hideAttaches?: boolean; // вспомогательное поле для комментариев

    format?: string;
}

export interface IFieldFilterOptions {

    title?: string;
    type?: E_FIELD_TYPE; // Тип контрола, как будет отображаться в фильтре, если не базовый.
    options?: any[]; // опции если вдруг тип селект и выпадающий список отличается.
    defaultValue?: any; // фильтр значение по умолчанию
    readonly?: boolean;
    key?: string; // если нужен другой ключ для сохранения (например deleted => showDeleted)
    onFilterGetValue?: Function; // обработка
    onFilterSetValue?: (data, value) => void; // обработка
}

export interface IFieldDescriptor {
    onGetValue?: Function; // если генерировать прочтенное значение нужно. (например - читаем rubrics: obj[] а нужно писать rubricIds: int[])
    onFilterGetValue?: Function; // эвент для значения фильтра
    onInitControl?: Function; // Эвент при создании dynamic-input-component
    onInputCreate?: Function;
    initOptions?: Function;

    title: string; // Label of field
    titlePath?: string; // path to settings, where title stored
    key: string;
    type: E_FIELD_TYPE;


    // List
    gridOption?: IGridOptions;

    // Edit
    editOptions?: IFieldEditOptions;
    filterOptions?: IFieldFilterOptions;
    readonly?: boolean;

    /// Вспомогательное поле для режима (например, для list - single, multiple)
    mode?: any;
    noStoreValue?: boolean; // не сохранять результат (фейковое или вспомогательное поле)
    manualLayout?: boolean; // Не ставить автоматом в simple form
    hidden?: boolean;
    options?: any[];
    optionsFn?: Function; // Promise<any[]>;
    disabled?: boolean;
    length?: number;
    needPermissions?: IClaim | IClaim[] | IClaim[][];
    isPassword?: boolean;
    // isOnlyInsert?: boolean; // поле только для добавления.
    // isOnlyUpdate?: boolean; // поле только для обновления.
    minValue?: number;
    maxValue?: number;
    descriptor?: any;
    // Insert
    defaultValue?: any;
    validators?: ValidatorFn[];
    // required?: boolean;
    // pattern?: RegExp;
    keydescriptor?: string;
}


export interface IEditPageDescriptor {
    onDataUpdate?(data);
    title: string;
    fields: any[];
    readonly?: boolean;
    hidden?: boolean;
}
