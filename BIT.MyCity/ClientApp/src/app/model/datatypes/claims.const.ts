import { BaseTableDescriptor } from './descriptors.const';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';

export const CLAIMS_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'claims';
    entityName = 'claims';

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            entity: 'claims',
            id: 'claims',
            list: gqlSdk.getClaimInfos,
            listKey: 'claimsInfo',
        };
    }
}();
