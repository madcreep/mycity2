import { BaseTableDescriptor } from './descriptors.const';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';

export const RESETPASSWORD_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'resetPassword';
    entityName = 'resetPassword';
    title = 'resetPassword';
    fields = [];
    edit_list = [];
    insert_list = [];
    view_list = [];
    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            entity: this.id,
            id: this.id,
            update: gqlSdk.resetPassword,
        };
    }
}();


