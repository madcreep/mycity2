import { BaseTableDescriptor } from './descriptors.const';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';





export const MODERATORS_DESCRIPTOR = new class extends BaseTableDescriptor {
    id: 'moderators';
    entityName: 'moderators';
    updateEntityName = 'updateModerator';
    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            entity: 'moderators',
            id: 'moderators',
            list: gqlSdk.availableModerators,
            listKey: 'availableModerators',
        };

    }
}();

