import { BaseTableDescriptor } from './descriptors.const';
import { IFieldDescriptor, E_FIELD_TYPE } from './descriptors.interfaces';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';


const AppoimentRangeFieldsColumnDefs: IFieldDescriptor[] = [
    {
        key: 'rubricName',
        title: 'Орган власти',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'officer.name',
        title: 'Должностное лицо',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'officer.position',
        title: 'Должностное лицо',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'officer.authority',
        title: 'Полномочия',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'officer.place',
        title: 'Место проведения',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'date',
        title: 'Дата приема',
        type: E_FIELD_TYPE.date,
    }, {
        key: 'appointments',
        title: 'Количество заявок',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'start',
        title: 'start',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'end',
        title: 'end',
        type: E_FIELD_TYPE.string,
    }


    // end
    // date
    // appointments
    // registeredAppointments {
    //     id
    // }
    // officer {
    //     id
    //     name
    //     due
    //     place
    //     position
    //     authority
    //     #   rubric {
    //     #       ...RubricLiteFragment
    //     #   }
    // }

];


export const APPOINTMENTRANGE_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'appointment-range';
    entityName = 'appointment-range';
    title = 'Время приема';
    fields = AppoimentRangeFieldsColumnDefs;
    card_action_list = [];

    edit_list = [];

    view_list = ['rubricName', 'officer.name', 'officer.position', 'officer.authority',
        'officer.place', 'date', 'appointments', 'start', 'end', ];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            list: gqlSdk.getAppointmentRangeList,
            listKey: 'appointmentRange',
        };
    }

};
