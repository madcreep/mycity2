import { IFieldDescriptor, E_FIELD_TYPE, E_ACTION_TYPE } from './descriptors.interfaces';
import { FT_DATECREATED } from './fields.const';
import { GridFormatters } from './grid-formatters';
import { RUBRIC_TITLE } from './rubric-organisations.const';
import { BaseTableDescriptor } from './descriptors.const';
import { IProtocolRecord } from 'src/generated/types.graphql-gen';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';


// type ProtocolRecord {
//     action: String
//     appeal: Appeal
//     appointment: Appointment
//     appointmentRange: AppointmentRange

//     """Дата создания объекта"""
//     createdAt: Date!
//     data: String

//     """Удален"""
//     deleted: Boolean
//     deloDate: Date
//     deloId: String

//     """ДУЕ"""
//     due: String
//     extId: String
//     extNumber: String

//     """Скрыт"""
//     hidden: Boolean

//     """Идентификатор объекта"""
//     id: ID

//     """Является папкой"""
//     isNode: Boolean
//     officer: Officer
//     public: Boolean
//     region: Region
//     rejected: Boolean
//     rejectionReason: String
//     rubric: Rubric
//     siteStatus: Int

//     """Для особой сортировки"""
//     sortingOrder: Int
//     source: String
//     status: Int
//     territory: Territory
//     topic: Topic

//     """Наименование типа"""
//     type: String

//     """Дата изменения объекта"""
//     updatedAt: Date
//     user: User
//   }


const ProtocolFieldsColumnDefs: IFieldDescriptor[] = [
    {
        key: 'user.fullName',
        title: 'Пользователь',
        type: E_FIELD_TYPE.string,
        readonly: true,
        gridOption: {
            suppressMenu: false,
            width: 50,
            // cellStyle: { 'white-space': 'normal' }
        },
    },
    FT_DATECREATED,
    {
        title: 'Обращение',
        key: 'appeal.number',
        type: E_FIELD_TYPE.string,
        readonly: true,
        gridOption: {
            suppressMenu: false,
            valueFormatter: GridFormatters.appealFormatter,
            cellStyle: { 'white-space': 'normal' }
        }
    },
    {
        title: 'Запись на прием',
        key: 'appointment.authorName',
        type: E_FIELD_TYPE.string,
        gridOption: {
            suppressMenu: false,
            valueFormatter: GridFormatters.appointmentFormatter,
            cellStyle: { 'white-space': 'normal' }
        }
    },
    {
        title: RUBRIC_TITLE,
        key: 'rubric.name',
        type: E_FIELD_TYPE.string,
        gridOption: {
            suppressMenu: false,
            cellStyle: { 'white-space': 'normal' }
        },
    },
    {
        title: 'ДЛ',
        key: 'officer.name',
        type: E_FIELD_TYPE.string,
        gridOption: {

        suppressMenu: false,
        cellStyle: { 'white-space': 'normal' }
        }
    },
    {
        title: 'Действие',
        key: 'action',
        type: E_FIELD_TYPE.string,
        gridOption: {
            suppressMenu: false,
            valueFormatter: GridFormatters.actionFormatter,
            cellStyle: { 'white-space': 'normal' }
        }
    },
    {
        title: 'Публ.',
        key: 'public',
        type: E_FIELD_TYPE.boolean,
        gridOption: {
            suppressMenu: false,
            cellRenderer: GridFormatters.booleanFormatter
        }
    },
    {
        title: 'Причина отказа',
        key: 'rejectionReason',
        type: E_FIELD_TYPE.string,
            gridOption: {
            suppressMenu: false,
            cellStyle: { 'white-space': 'normal' }
        }
    }, {
        key: 'eventText',
        title: 'Событие',
        type: E_FIELD_TYPE.string,
        gridOption: {
            valueFormatter: eventTextFormatter,
        }
    }
];



export const PROTOCOL_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'protocol';
    entityName = 'protocol';
    updateEntityName = 'updateProtocol';
    title = 'Протоколы';
    fields = ProtocolFieldsColumnDefs;
    card_action_list = [
        E_ACTION_TYPE.cancel,
        E_ACTION_TYPE.exportExcel,
    ];

    // edit_list = [];

    view_list = [
        FT_DATECREATED.key,
        'user.fullName',
    // 'appeal.number', 'appointment.authorName', 'rubric.name',
    // 'officer.name', 'action', 'public', 'rejectionReason',
    'eventText',
    ];


    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            list: gqlSdk.getProtocols,
            listKey: 'protocolRecord',
        };
    }
};


function eventTextFormatter (data) {
    let res = '';

    const event = <IProtocolRecord>data['data'];
    if (event) {
        res += event.action + ' ';

        if (event.appeal) {
            const appeal = event.appeal;
            res += '#' + appeal.id + ' ';
        //     if (event.appeal.rubric) {
        //         res += ' (Рубрика: ' + event.appeal.rubric.shortName + ')';
        //     }
        }
        if (event.attachedFile) {
            const obj = event.attachedFile;
            res += '#' + obj.id + ' ' + obj.name;
        }

        res += event.data || '';
    }
    return res;
}
