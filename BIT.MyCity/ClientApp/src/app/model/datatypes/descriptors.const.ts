import { ICardAction } from '../../views/cards/card.interfaces';
import { IFieldDescriptor, E_ACTION_TYPE, IEditPageDescriptor, E_FIELD_TYPE } from './descriptors.interfaces';
import { AppUtils, InjectorInstance } from '../../helpers/app.static';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { DescriptorsService } from '../../services/descriptors.service';



export const CardActionsList: ICardAction [] = [

    {id: E_ACTION_TYPE.cancel, title: 'Отменить', },
    {id: E_ACTION_TYPE.close, title: 'Закрыть', },
    {id: E_ACTION_TYPE.apply, title: 'Применить', class: 'btn-success' },
    {id: E_ACTION_TYPE.save, title: 'Сохранить', class: 'btn-success' },
    {id: E_ACTION_TYPE.protocol, title: 'Протокол', },
    {id: E_ACTION_TYPE.exportExcel, title: 'В Excel', },
    {id: E_ACTION_TYPE.reject, title: 'Отказать', },
    {id: E_ACTION_TYPE.accept, title: 'Подтвердить', },
    { id: E_ACTION_TYPE.register, title: 'Регистрация', },
    { id: E_ACTION_TYPE.send, title: 'Отправить', class: 'btn-success' },

];



export abstract class BaseTableDescriptor {



    id: string;
    keyField = 'id';
    entityName: string;
    updateEntityName: string;
    // TODO: [MYC-160] рефактор - обьеденить дескрипторы и MenuPages
    title: string;
    titlePath?: string;
    recordTitleField?: string;
    fields: IFieldDescriptor[];
    edit_list: IEditPageDescriptor[];
    insert_list?: IEditPageDescriptor[];
    view_list: string[];
    filter_layout: any[];
    card_action_list = [
        E_ACTION_TYPE.cancel,
        E_ACTION_TYPE.save,
    ];

    // protected gqlSdk: GraphQLClient;
    constructor(

    ) {
        // this.gqlSdk = InjectorInstance.get(GraphQLClient);
        // const api = this.getApiDescriptor();
        // if (api) {
            // this.gqlSdk.addApiDescriptor(api);
        // }
        DescriptorsService.preRegisterDescriptor(this.id, this);
    }

    static valueToIdsFn(path: string): Function {
        return (data: any) => {
            const val = AppUtils.getValueByPath(data, path);
            if (val && val instanceof Array) {
                return val.map ((v) => v.id);
            }
            return [];
        };
    }

    static valueToIdFn(path: string): Function {
        return (data: any) => {
            const val = AppUtils.getValueByPath(data, path);
            return val;
        };
    }


    static genNewData(initialData: any, list: IFieldDescriptor[]): any {
        const data = {};
        for (const field of list) {
            if (initialData && initialData[field.key] !== undefined) {
                data[field.key] = initialData[field.key];
            } else if (field.defaultValue !== undefined) {
                if (field.defaultValue instanceof Function) {
                    data[field.key] = field.defaultValue.call(field);
                } else {
                    data[field.key] = AppUtils.deepCopy(field.defaultValue);
                }
            } else if (!field.readonly) {
                data[field.key] = AppUtils.deepCopy(BaseTableDescriptor.getDefaultValue(field, field.defaultValue));
            }
        }
        return data;
    }

    static ListOf(entity: string, fields: IFieldDescriptor[], list: string[]) {
        const fullPref = '/' + entity + '.';
        return list.map( s => fields.find(
            f => {
                if (s[0] === '/') {
                    return (fullPref + f.key) === s;
                } else {
                    return f.key === s;
                }
            }
            )).filter (f => !!f);
    }

    static getDefaultValue(field: IFieldDescriptor, defaultValue: any): any {
        if (defaultValue !== undefined) {
            return defaultValue;
        }

        switch (field.type) {
            case E_FIELD_TYPE.checkBoxGroup: return [];
            case E_FIELD_TYPE.claimSelect: return [];
            case E_FIELD_TYPE.boolean: return false;
            case E_FIELD_TYPE.string: return '';
            default: return null;
        }
    }

    get_edit_list(): IEditPageDescriptor[] {
        return this.edit_list;
    }

    getAbsPathesPageList(isNewRecord: boolean, data: any = null): IEditPageDescriptor[] {
        const res = AppUtils.deepCopy(isNewRecord ? this.insert_list || this.get_edit_list() : this.get_edit_list());
        const pref = '/' + this.entityName + '.';

        for (let page = 0; page < res.length; page++) {
            const element = res[page];
            _updateInputAndPageListFields(element.fields, page);
        }

        function _updateInputAndPageListFields(fields, page) {

            for (let i = 0; i < fields.length; i++) {
                const f = fields[i];
                if (f instanceof Array) {
                    _updateInputAndPageListFields(f, page);
                } else if (f[0] !== '/' && f !== '-') {
                    fields[i] = (pref + f);
                }
            }
        }

        return res;
    }

    fieldsEdit () {
        let res = [];
        const list = this.get_edit_list();
        for (let i = 0; i < list.length; i++) {
            res = res.concat(... AppUtils.flattenArray(list[i].fields));
        }
        const uniq = new Set<string>(res);
        return BaseTableDescriptor.ListOf(this.entityName, this.fields, Array.from(uniq));
    }

    fieldsInsert () {
        if (this.insert_list === undefined) {
            return this.fieldsEdit();
        }

        let res = [];
        for (let i = 0; i < this.insert_list.length; i++) {
            res = res.concat(... AppUtils.flattenArray(this.insert_list[i].fields));
        }
        const uniq = new Set<string>(res);
        return BaseTableDescriptor.ListOf(this.entityName, this.fields, Array.from(uniq));
    }

    fieldsView () {
        return BaseTableDescriptor.ListOf(this.entityName, this.fields, this.view_list);
    }

    fieldsFilter(): IFieldDescriptor[] {
        const flat = this.filter_layout ? AppUtils.flattenArray(this.filter_layout) : [];
        const a = flat ? BaseTableDescriptor.ListOf(this.entityName, this.fields, flat) : [];
        const b = a.map (v => Object.assign({}, v));
        b.forEach( v => {
            if (v.filterOptions) {
                // if (v.filterOptions.key !== undefined) {
                //     v.key = v.filterOptions.key;
                // }
                if (v.filterOptions.title !== undefined) {
                    v.title = v.filterOptions.title;
                }
                if (v.filterOptions.type !== undefined) {
                    v.type = v.filterOptions.type;
                }
                if (v.filterOptions.options !== undefined) {
                    v.options = v.filterOptions.options;
                }
                if (v.filterOptions.defaultValue !== undefined) {
                    v.defaultValue = v.filterOptions.defaultValue;
                }
                if (v.filterOptions.onFilterGetValue !== undefined) {
                    v.onFilterGetValue = v.filterOptions.onFilterGetValue;
                }
                // if (v.filterOptions.readonly !== undefined) {
                //     v.readonly = v.filterOptions.readonly;
                // }

            }
            v.validators = [];
            if (v.readonly !== undefined) {
                delete v.readonly;
            }
        });
        return b;
    }

    actionsCard() {
        return this.card_action_list.map( t => CardActionsList.find(ca => ca.id === t));
    }

    genNewRecord(initialData: any): any {
        const list = this.fieldsInsert();
        return BaseTableDescriptor.genNewData(initialData, list);
    }

    genRecordTitle(data: any, isNewRecord: boolean): string {
       if (!isNewRecord && this.recordTitleField && data[this.entityName] && data[this.entityName][this.recordTitleField]) {
            return data[this.entityName][this.recordTitleField];
        } else if (data[this.entityName] && data[this.entityName].name) {
            return data[this.entityName].name;
        }
        return this.title + (isNewRecord ? ' - новая запись' : ' #' + data.id);
    }

    public abstract getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries;

}


