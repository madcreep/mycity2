

export class GridFormatters {

    static dateTimeFormatter(params) {
        let result = '';
        if (params && params.value && params.value) {
            const dateTime = new Date(params.value);
            result = dateTime.toLocaleDateString('ru') + ' ' + dateTime.toLocaleTimeString('ru');
        }
        return result;
    }

    static dateFormatter(date: any) {
        if (!date || date.value === null || date.value === undefined) {
            return '';
        } else if (date instanceof Date) {
            return date.toLocaleDateString('ru');
        } else if (date.value) {
            const dateTime = new Date(date.value);
            return dateTime.toLocaleDateString('ru');
        } else {
            const dateTime = new Date(date);
            return dateTime.toLocaleDateString('ru');
        }

    }

    static dateFormatter1(date: any) {
        if (!date) {
            return '';
        } else if (date instanceof Date) {
            return date.toLocaleDateString('ru');
        } else if (date.value) {
            const dateTime = new Date(date.value);
            return dateTime.toLocaleDateString('ru');
        } else {
            const dateTime = new Date(date);
            return dateTime.toLocaleDateString('ru');
        }

    }

    static appealFormatter(params) {
        if (!params.data || !params.data.appeal) {
            return '';
        }
        let result = params.data.appeal.id + ' от ' + GridFormatters.dateFormatter(params.data.appeal.createdAt);
        if (params.data.appeal.extNumber && params.data.appeal.extDate) {
            result += ' (' + params.data.appeal.extNumber + ' / ' + GridFormatters.dateFormatter(params.data.appeal.extDate) + ')';
        }
        return result;
    }

    static appointmentFormatter(params) {
        if (!params.data || !params.data.appointment) {
            return '';
        }
        const appointment = params.data.appointment;
        return appointment.number + ' на ' + GridFormatters.dateTimeFormatter(appointment.date) + ' (' + appointment.authorName + ')';
    }


    static actionFormatter(params) {
        const item = params.data;
        if (!item || !item.action) {
            return '';
        }
        const result = item.action + ': ';
        if (item.action === 'approve') {
            if (item.public === 1) {
                return result + 'Одобрено';
            }
            return result + 'Отклонено';
        }
        if (item.action === 'approveComment') {
            return result + 'Одобрен комментарий';
        }
        if (item.action === 'deleteComment') {
            return result + 'Удален комментарий';
        }
        if (item.action === 'setStatus') {
            return result + 'Установлено состояние';
        }
        if (item.action === 'save') {
            return result + 'Записано';
        }
        if (item.action === 'createAppeal') {
            return result + 'Создано обращение';
        }
        if (item.action === 'saveModerator') {
            return result + 'Записано';
        }
        if (item.action === 'process') {
            return result + 'Обработано';
        }
        if (item.action === 'delete') {
            return result + 'Удалено';
        }
        if (item.action === 'togglePublic') {
            return result + 'Изменен признак публичности';
        }
        if (item.action === 'saveAppointment') {
            return result + 'Сохранена заявка на личный прием';
        }
        if (item.action === 'saveRange') {
            return result + 'Сохранено время приема';
        }
        if (item.action === 'deleteRange') {
            return result + 'Удалено время приема';
        }
        if (item.action === 'saveOfficer') {
            return result + 'Сохранено должностноное лицо';
        }
        if (item.action === 'deleteOfficer') {
            return result + 'Удалено должностноное лицо';
        }
        if (item.action === 'saveRubric') {
            return result + 'Сохранен адресат';
        }
        if (item.action === 'deleteRubric') {
            return result + 'Удален адресат';
        }
    }

    public static booleanFormatter(params) {
        if (params.value) {
            return '<i class="fa fa-check"></i>';
        }
        return '';
    }


}
