import { BaseTableDescriptor } from './descriptors.const';
import { IFieldDescriptor, E_FIELD_TYPE, IEditPageDescriptor } from './descriptors.interfaces';
import { FT_TYPE, FT_DATECREATED, FT_DELETED, FT_DATEUPDATED, FT_WEIGHT, FT_NAME, FT_DESCRIPTION } from './fields.const';
import { InputBase } from '../../modules/dynamic-inputs/types/input-base';
import { AppUtils, InjectorInstance } from '../../helpers/app.static';
import { ISettings, ISettingType } from 'src/generated/types.graphql-gen';
import { SETTINGS_DESCRIPTOR } from './settings.const';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';

import { SettingsService } from '../../services/settings.service';
import { GridFormatters } from './grid-formatters';
import { InputsService } from '../../services/input.service/inputs.service';


const SubSystemFieldsColumnDefs: IFieldDescriptor[] = [
    {
        key: 'uID',
        title: 'UID',
        readonly: true,
        type: E_FIELD_TYPE.string,
        gridOption: {
            width: 140,
            maxWidth: 200,
        }
    },

    FT_DATECREATED,
    FT_DELETED,
    FT_DATEUPDATED,
    FT_WEIGHT,
    Object.assign({}, FT_NAME, {
        readonly: true
    }),
    Object.assign({}, FT_DESCRIPTION, {
        readonly: true
    }),
    Object.assign({}, FT_TYPE, {
        readonly: true,
    }),
    {
        key: 'activated',
        readonly: true,
        title: 'Вкл',
        type: E_FIELD_TYPE.boolean,
        gridOption: {
            width: 50,
            valueFormatter: (data) => {
                const sssrv = InjectorInstance.get(SettingsService);
                const a = sssrv.getValue('subsystem.' + data.data.uID + '.enable');
                // return sssrv.titleForSubsistem(data.node.data.subsystemUID);
                // return data.node.data.subsystem.uID;
                // return GridFormatters.booleanFormatter({ value: a } );
                return a;
            },
            cellRenderer: (data) => {
                return GridFormatters.booleanFormatter( { value: data.valueFormatted === 'true' });
            }
        }
    }
];


export const SUBSYSTEM_DESCRIPTOR = new class extends BaseTableDescriptor {

    id = 'subsystem';
    keyField = 'uID';
    entityName = 'subsystem';
    updateEntityName = 'updateSubsystem';
    title = 'Подсистемы';
    fields = SubSystemFieldsColumnDefs;
    edit_list = [
        {
            title: 'Основные',
            fields: [
                ['uID', FT_NAME.key],
                FT_DESCRIPTION.key,
            ]
        },
    ];

    view_list = [
        'activated',
        FT_DATECREATED.key,
        'uID',
        FT_NAME.key,
    ];
    // filter_list = ['fullName'];
    recordTitleField = 'uID';

    genRecordTitle(data: any, isNewRecord: boolean): string {
        if (isNewRecord) {
            return super.genRecordTitle(data, isNewRecord);
        }
        return '(' + data[this.id]['uID'] + ')';
    }


    getAbsPathesPageList(isNewRecord: boolean, data): IEditPageDescriptor[] {
        const l = super.getAbsPathesPageList(isNewRecord);
        if (!data) {
            return l;
        }

        const inputs = <InputBase<any>[]>data;
        return [
            l[0],
            {
                title: 'Настройки',
                fields: inputs.map(i => i.key).filter(k => k.startsWith(SETTINGS_DESCRIPTOR.entityName)).map (
                    k => '/' + k
                )
            }

        ];
    }




    __GENERAteSubsTMPFoo(descriptor: BaseTableDescriptor, id: string | number, targetData: any, _settingsSrv: any): any {
        // const _settingsSrv = InjectorInstance.get(SettingsService);
        const keys = [SUBSYSTEM_DESCRIPTOR.entityName + '.' + id];
        return _settingsSrv.updateSettings(keys, true).then((datacfg) => {

            const inpSrv = InjectorInstance.get(InputsService);
            targetData[SETTINGS_DESCRIPTOR.entityName] = datacfg.value;

            let inputscfg = {};
            Object.keys(datacfg.config)
                .sort(((k1, k2) => {
                    return datacfg.config[k1].weight >= datacfg.config[k2].weight ? 1 : -1;
                })).forEach(function (v, i) {
                    inputscfg[v] = datacfg.config[v];
                });

            this.__updateDiscriptorForConfig(inputscfg, id, inpSrv);

            descriptor.edit_list = descriptor.edit_list.concat(SETTINGS_DESCRIPTOR.edit_list);
            return targetData;
        });
    }

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            entity: this.entityName,
            id: this.id,
            // create: this.createSubSystem,
            update: gqlSdk.updateSubSystem,
            list: gqlSdk.getSubSystemList,
            query: gqlSdk.getSubSystem,
        };
    }

    private __updateDiscriptorForConfig(inputscfg, id, inpSrv: InputsService) {
        const fields = [];
        for (const key in inputscfg) {
            if (inputscfg.hasOwnProperty(key)) {
                const field: ISettings = inputscfg[key];
                if (field.settingType === ISettingType.Value || field.settingType === ISettingType.Undefined) {
                    const inputDescriptor = inpSrv.settingToInputDescriptor(field);
                    if (inputDescriptor) {
                        fields.push(inputDescriptor);
                    }

                }
            }
        }

        SETTINGS_DESCRIPTOR.fields = fields;
        const settingsPages = [
            { title: 'Настройки', keys: [SUBSYSTEM_DESCRIPTOR.entityName + '.' + id] },
        ];

        SETTINGS_DESCRIPTOR.edit_list = settingsPages.map(p => {
            return {
                title: p.title, fields:
                    AppUtils.flattenArray(
                        fields.filter(s => p.keys.find(key => s.key.startsWith(key)))
                    ).map(s => /*SETTINGS_DESCRIPTOR.entityName + '.' +*/ '/' + SETTINGS_DESCRIPTOR.entityName + '.' + s.key)
            };
        });

    }


};


