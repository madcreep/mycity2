import { IFieldDescriptor } from './descriptors.interfaces';
import { FT_DUE, FT_SNAME, FT_WEIGHT, FT_DATECREATED, FT_DATEUPDATED, FT_DELETED, FT_ID, FT_ISNODE, FT_NAME, FT_TYPE, FT_EXTID, FT_SUBSYSTEM, FT_SUBSYSTEMS_F } from './fields.const';
import { BaseTableDescriptor } from './descriptors.const';
import { SUBSYSTEM_CITIZEN_ID } from '../../services/subsystems.service';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GetTopicListDocument } from 'src/generated/types.graphql-gen';


const TopicFieldsColumnDefs: IFieldDescriptor[] = [
    FT_DATECREATED,
    FT_DELETED,
    FT_DUE,
    FT_ID,
    FT_ISNODE,
    FT_NAME,
    FT_SNAME,
    FT_WEIGHT,
    FT_TYPE,
    FT_DATEUPDATED,
    FT_EXTID,
    // FT_SUBSYSTEMS,
    Object.assign({}, FT_SUBSYSTEM, {
        defaultValue: SUBSYSTEM_CITIZEN_ID,
        hidden: true,
    }),
];


export const TOPIC_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'topic';
    entityName = 'topic';
    updateEntityName = 'updateTopic';
    title = 'Темы обращений';
    fields = TopicFieldsColumnDefs;
    edit_list = [
        // { title: 'Основные', fields: [ FT_SUBSYSTEMS.key, FT_NAME.key, [FT_SNAME.key, FT_EXTID.key, ], FT_SUBSYSTEM.key, ] },
    ];

    view_list = [FT_NAME.key, FT_SNAME.key ];
    filter_layout = [FT_DELETED.key, FT_NAME.key];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            listKey: this.entityName,
            list: gqlSdk.getTopicList,
            query: gqlSdk.getTopic,
            update: gqlSdk.updateTopic,
            create: gqlSdk.createTopic,
            cacheupdate: [{ obj: GetTopicListDocument, }]
        };
    }
};

