import { BaseTableDescriptor } from './descriptors.const';
import { IFieldDescriptor, E_FIELD_TYPE } from './descriptors.interfaces';
import {
    FT_ID, FT_WEIGHT, FT_NAME, FT_SNAME, FT_DELETED, FT_ATTACHED_FILES, FT_TEXT, FT_SHORTTEXT, FT_DATECREATED,
    FT_DATEUPDATED, FT_TERRITORYLINK, FT_RUBRICLINK, FT_TOPICLINK, FT_REGIONLINK, FTS_4RUBRICS, FT_TEXT_RICH
} from './fields.const';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GetNewsModListDocument, INewsStateEnum, INews } from 'src/generated/types.graphql-gen';
import { AppUtils } from '../../helpers/app.static';



// author {
//     fullName
// }
// likeWIL {
//     liked
// }

// viewWIV {
//     viewed
// }




const NewsStateEnumArray = [
    { value: INewsStateEnum.Draft, title: 'Черновик' },
    { value: INewsStateEnum.Moderate, title: 'Модерация' },
    { value: INewsStateEnum.Published, title: 'Публикация' },
    { value: INewsStateEnum.Unpublished, title: 'Снято с публикации' },
];

const NewsFieldsColumnDefs: IFieldDescriptor[] = [
    FT_ID,
    FT_WEIGHT,
    FT_NAME,
    FT_SNAME,
    FT_DELETED,
    FT_TEXT_RICH,
    FT_SHORTTEXT,
    FT_DATECREATED,
    FT_DATEUPDATED,
    ...FTS_4RUBRICS('news'),
    Object.assign({}, FT_ATTACHED_FILES, {
        onGetValue: BaseTableDescriptor.valueToIdsFn('news.attachedFiles'),
    }),
    {
        key: 'commentsAllowed',
        title: 'Пользователям разрешено оставлять комментарии',
        type: E_FIELD_TYPE.boolean,
        defaultValue: true,
    }, {
        key: 'state',
        title: 'Статус',
        type: E_FIELD_TYPE.select,
        readonly: true,
        options: NewsStateEnumArray,
        filterOptions: {
            options: [
                { value: null, title: 'Все' },
                ...NewsStateEnumArray
            ],
            defaultValue: null,
        },
        gridOption: {
            valueFormatter: statusFormatter,
        }
    }, {
        key: 'likesCount',
        title: 'Счетчик лайков',
        type: E_FIELD_TYPE.number,
        readonly: true,
    }, {
        key: 'viewsCount',
        title: 'Счетчик просмотров',
        type: E_FIELD_TYPE.number,
        readonly: true,
    }
];


export const NEWS_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'news';
    entityName = 'news';
    entityType = 'updateNews';
    title = 'Новости';
    fields = NewsFieldsColumnDefs;
    edit_list = [
        {
            title: 'Основные', fields: [
                [FT_DATECREATED.key, FT_DATEUPDATED.key, ],
                [FT_NAME.key, 'state', ],
                // FT_SNAME.key,

                [FT_TERRITORYLINK.key, FT_REGIONLINK.key, ],
                [FT_TOPICLINK.key, FT_RUBRICLINK.key, ],

                FT_TEXT.key,




                [FT_ATTACHED_FILES.key, FT_SHORTTEXT.key, ],
                'commentsAllowed',
                ['likesCount', 'viewsCount', ],
            ]
        },
    ];

    view_list = [FT_DELETED.key, FT_NAME.key, 'state', FT_DATECREATED.key, FT_RUBRICLINK.key, FT_TERRITORYLINK.key, 'likesCount',
        'viewsCount', ];
    filter_layout = [FT_DELETED.key, FT_NAME.key];
    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            create: gqlSdk.createNews,
            query: gqlSdk.getNewsModById,
            update: gqlSdk.updateNews,
            list: gqlSdk.getNewsModList,
            cacheupdate: [{ obj: GetNewsModListDocument, }]
        };
    }

}();



function statusFormatter(data) {
    const result = '';
    const news: INews = data.data;
    const state = NewsStateEnumArray.find(s => s.value === news.state);
    if (!state) {
        return '';
    }
    switch (state.value) {
        // case INewsStateEnum.Draft: {
        // return state.title;
        // }
        // case INewsStateEnum.Moderate: {
        // return state.title;
        // }
        default: {
            return state.title;
        }
    }
}
