import { GraphQLClient } from "../../data-services/graphql/graphql.client";
import { GqlApiQueries } from "../../data-services/graphql/sdk-map.interfaces";
import { ValidatorsControl, VALIDATOR_TYPE } from "../../helpers/validators/validators-control";
import { BaseTableDescriptor } from "./descriptors.const";
import {E_ACTION_TYPE, E_FIELD_TYPE, IFieldDescriptor} from './descriptors.interfaces';
import { FT_DESCRIPTION, FT_ID, FT_UUID } from "./fields.const";

const ReportFieldsColumnDefs: IFieldDescriptor[] = [
    Object.assign({}, FT_UUID, { key: 'reportId'}),
    FT_DESCRIPTION,
    {
        title: 'Наименование',
        key: 'name',
        type: E_FIELD_TYPE.string,
        validators: [
            ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED),
        ],
        gridOption: {
            width: 150,
            sortable: true,
        },

    },
];

export const REPORTS_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'reports';
    entityName = 'reports';
    updateEntityName = 'updateReport';
    title = 'Отчеты';
    fields = ReportFieldsColumnDefs;
    keyField = 'reportId';
    edit_list = [
        { title: 'Основные', fields: ['reportId', 'name', FT_DESCRIPTION.key] },
    ];
    insert_list = [
        { title: 'Основные', fields: ['reportId', 'name', FT_DESCRIPTION.key] },
    ];

    view_list = ['reportId', 'name', FT_DESCRIPTION.key];
    filter_layout = [];
    recordTitleField = 'name';

    card_action_list = [
        E_ACTION_TYPE.cancel,
        E_ACTION_TYPE.send,
    ];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            create: null,
            query: gqlSdk.getReportSettings,
            update: null,
            list: gqlSdk.getReportsList,
            listKey: 'reports'
            // cacheupdate: [{ obj: GetUserListDocument, }]
        };
    }

};



