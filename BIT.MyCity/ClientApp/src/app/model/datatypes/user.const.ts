import { FT_ID, FILTER_OPTS_LIKE } from './fields.const';
import { BaseTableDescriptor } from './descriptors.const';
import { IFieldDescriptor, E_FIELD_TYPE as E_FIELD_TYPE } from './descriptors.interfaces';
import { IRoleType, GetUserListDocument } from 'src/generated/types.graphql-gen';
import { ValidatorsControl, VALIDATOR_TYPE } from '../../helpers/validators/validators-control';
import { EMAIL_PATTERN } from '../../consts/common.consts';
import { GridFormatters } from './grid-formatters';
import { CLAIM_SA_ADMIN } from '../../services/claims.const';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';



const UserFieldsColumnDefs: IFieldDescriptor[] = [
    FT_ID,
    {
        title: 'Login',
        key: 'userName',
        type: E_FIELD_TYPE.string,
        validators: [
            ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED),
        ],
        gridOption: {
            width: 150,
            sortable: true,
        },

    }, {
        title: 'Пароль',
        key: 'password',
        type: E_FIELD_TYPE.string,
        isPassword: true,
        // isOnlyInsert: true,
        validators: [
            ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED),
            ValidatorsControl.existValidator(VALIDATOR_TYPE.MIN_LENGTH, 'Минимальная длина пароля 6 символов', 6),
        ],
    }, {
        title: 'ID',
        key: 'extId',
        type: E_FIELD_TYPE.number,
        gridOption: {
            width: 50,
            sortable: false,
        },
    }, {
        title: 'Пользователь отключен',
        key: 'disconnected',
        type: E_FIELD_TYPE.boolean,
        gridOption: {
            headerName: 'Отключен',
            width: 100,
            cellRenderer: GridFormatters.booleanFormatter
        }

    }, {
        title: 'Фамилия Имя Отчество',
        key: 'fullName',
        type: E_FIELD_TYPE.string,
        validators: [
            ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED),
        ],
        gridOption: {
            width: 350,
            cellRenderer: 'loadingCellRenderer',
            sortable: true,
            suppressMenu: false,
            // checkboxSelection: true,
            pinned: 'left',
        },
        filterOptions: FILTER_OPTS_LIKE,
    }, {
        title: 'E-mail',
        key: 'email',
        type: E_FIELD_TYPE.string,
        validators: [
            // ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED),
            ValidatorsControl.existValidator(VALIDATOR_TYPE.PATTERN, 'Введите верный E-mail', EMAIL_PATTERN),
        ],
        gridOption: {
            width: 200,
            sortable: true,
            suppressMenu: true,
        }
        // onCellDoubleClicked: this.onCellDoubleClicked
    }, {
        title: 'Телефон',
        key: 'phoneNumber',
        type: E_FIELD_TYPE.string,
        defaultValue: '+7',
        gridOption: {
            width: 100,
            sortable: false,
            suppressMenu: true,
        }
    }, {
        title: 'URL подтверждения email',
        key: 'confirmEmailUrl',
        type: E_FIELD_TYPE.string,
        defaultValue: '',
    }, {
        title: 'email подтвержден',
        key: 'emailConfirmed',
        type: E_FIELD_TYPE.boolean,
        defaultValue: false,
    }, {
        title: 'Разрешения',
        key: 'claims',
        type: E_FIELD_TYPE.claimSelect,

        editOptions: {
            arrayFilter: 'USER',
            arraySource: {
                arrayList: 'claims',
                arrayTitle: (opt) => {
                    return opt.description + ' (' + opt.claimType + '-' + opt.claimEntity + ')';
                },
                arrayKey: 'claimEntity',
            }
        },
        needPermissions: [CLAIM_SA_ADMIN]

    }, {
        title: 'Роли',
        key: 'roles1',
        type: E_FIELD_TYPE.checkBoxGroup,
        editOptions: {
            arraySource: {
                arrayList: 'role',
                arrayTitle: 'name',
                arrayKey: 'name',
            }
        },
        validators: [
            ValidatorsControl.existValidator(VALIDATOR_TYPE.NON_EMPTY_ARRAY, 'Должна быть выбрана хотя бы одна роль.'),
        ],
        gridOption: {
            //   headerGroupComponent: 'appealsHeaderGroupComponent',
            children: [
                {
                    headerName: 'Администратор', field: 'admin', width: 120,
                    cellRenderer: GridFormatters.booleanFormatter, columnGroupShow: 'open',
                },
                {
                    headerName: 'Модератор', field: 'moderator', width: 120,
                    cellRenderer: GridFormatters.booleanFormatter
                },
                {
                    headerName: 'Гл. модератор', field: 'mainModerator', width: 120,
                    cellRenderer: GridFormatters.booleanFormatter, columnGroupShow: 'open',
                },
                {
                    headerName: 'Модератор ведомства', field: 'rubricModerator', width: 120,
                    cellRenderer: GridFormatters.booleanFormatter, columnGroupShow: 'open'
                },
                { headerName: 'Админ. клиентов', field: 'clientAdmin', width: 120, cellRenderer: GridFormatters.booleanFormatter, columnGroupShow: 'open' },
            ]
        }
    }, {
        key: 'resetPassword.newPassword',
        title: 'Новый пароль',
        type: E_FIELD_TYPE.string,
        isPassword: true,
        validators: [
            ValidatorsControl.existValidator(VALIDATOR_TYPE.VALIDATORSIFNOTEMPTY, '', [
                ValidatorsControl.existValidator(VALIDATOR_TYPE.REQUIRED),
                ValidatorsControl.existValidator(VALIDATOR_TYPE.MIN_LENGTH, 'Минимальная длина пароля 6 символов', 6),
            ]),
        ],
        // oldPassword: String

        // """Новый пароль"""
        // newPassword: String!

        // """Повторение нового пароля"""
        // confirmPassword: String
    }, {
        title: 'Искать во всех системах авторизации',
        key: 'notOnlyForms',
        type: E_FIELD_TYPE.boolean,
        filterOptions: {
            defaultValue: false,
        }
    }, {
        title: 'Роли',
        key: 'roleIds',
        type: E_FIELD_TYPE.multiselect,
        defaultValue: [],
        editOptions: {
            arraySource: {
                arrayList: 'role',
                arrayTitle: 'name',
                arrayKey: 'id',
                arrayFilter: { isAdminRegister: true },
                // arrayFilterHardGQL:
                // [ { where:`
                //     el=>(
                //         (el.IdentityClaims.Any(x=>x.ClaimType=="ss"&& x.ClaimValue=="citizens.available") ||
                //         el.UserRole.Any(x=>x.Role.IdentityClaims.Any(y=>y.ClaimType=="ss" &&
                //         y.ClaimValue=="citizens.available"))) &&
                //         (el.IdentityClaims.Any(x=>x.ClaimType=="md" &&
                //         x.ClaimValue=="citizens.enable") ||
                //         el.UserRole.Any(x=>x.Role.IdentityClaims.Any(y=>y.ClaimType == "md" &&
                //         y.ClaimValue == "citizens.enable")))) ||
                //         ((el.IdentityClaims.Any(x=>x.ClaimType=="ss" &&
                //         x.ClaimValue=="organizations.available") ||
                //         el.UserRole.Any(x=>x.Role.IdentityClaims.Any(y=>y.ClaimType == "ss" &&
                //         y.ClaimValue=="organizations.available"))) &&
                //         (el.IdentityClaims.Any(x=>x.ClaimType=="md" &&
                //         x.ClaimValue=="organizations.enable") ||
                //         el.UserRole.Any(x=>x.Role.IdentityClaims.Any(y=>y.ClaimType=="md" &&
                //         y.ClaimValue == "organizations.enable"))))`

                // }],
            }
        },
        onGetValue: (data) => {
            if (!data.user.roles) {
                return [];
            }
            return data.user.roles.map((r: IRoleType) => r.id);
        },
        validators: [
            // ValidatorsControl.existValidator(VALIDATOR_TYPE.NON_EMPTY_ARRAY, 'Должна быть выбрана хотя бы одна роль.'),
        ],
    }];


export const USER_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'user';
    entityName = 'user';
    updateEntityName = 'updateUser';
    title = 'Пользователи';
    fields = UserFieldsColumnDefs;
    edit_list = [
        { title: 'Основные', fields: ['-', 'roleIds', 'disconnected', 'emailConfirmed', '-', ['userName', 'email',], 'fullName', ['resetPassword.newPassword'], 'claims'] },
        // { title: 'Разрешения', fields: ['roleIds', 'claims', ] },
    ];
    insert_list = [
        { title: 'Основные', fields: ['-', 'roleIds', 'disconnected', 'emailConfirmed', '-', ['userName', 'email',], 'fullName', 'password', 'claims'] },
        // { title: 'Разрешения', fields: ['roleIds', 'claims', ] },

    ];

    view_list = ['ID', 'userName', 'fullName', 'email', 'phoneNumber', 'phoneNumberConfirmed', 'extId', 'roles-list', 'disconnected'];
    filter_layout = [
        'fullName',
        'notOnlyForms',
    ];
    recordTitleField = 'fullName';

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            create: gqlSdk.createUser,
            query: gqlSdk.getUserById,
            update: gqlSdk.updateUser,
            list: gqlSdk.getUserList,
            cacheupdate: [{ obj: GetUserListDocument, }]
        };
    }

};



