import { FT_ID, FT_DUE, FT_DESCRIPTION, FT_WEIGHT, FT_NAME, FT_SNAME, FT_EXTID, FT_DELETED, FT_SUBSYSTEMS_F } from './fields.const';
import { IFieldDescriptor, E_FIELD_TYPE } from './descriptors.interfaces';
import { BaseTableDescriptor } from './descriptors.const';
import { SETTINGS_RUBRIC_TITLE } from '../../services/setting-names.const';
import { CLAIM_SA_ADMIN, CLAIM_MD_ORGANIZATIONS, CLAIM_MD_ORG_SUPER, CLAIM_MD_CITIZEN, CLAIM_MD_CIT_SUPER } from '../../services/claims.const';
import { SUBSYSTEM_ORG_ID, SUBSYSTEM_CITIZEN_ID } from '../../services/subsystems.service';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { AppUtils } from '../../helpers/app.static';
import {environment} from '../../../environments/environment';

export const RUBRIC_TITLE = 'Орган власти';

const FT_SUBSYSTEMS_RUBRIC = FT_SUBSYSTEMS_F('rubric', <IFieldDescriptor>{
    defaultValue: [SUBSYSTEM_ORG_ID, SUBSYSTEM_CITIZEN_ID]
});

const RubricFieldsDefs: IFieldDescriptor[] = [
    FT_ID,
    FT_DUE,
    FT_DELETED,
    FT_DESCRIPTION,
    Object.assign({}, FT_WEIGHT, {
        type: E_FIELD_TYPE.string,
        readonly: true,
    }),
    FT_NAME,
    FT_SNAME,
    FT_EXTID,
    FT_SUBSYSTEMS_RUBRIC,
    {
        key: 'docgroup',
        title: 'Группа документов',
        type: E_FIELD_TYPE.string,
        length: 255,
    }, {
        key: 'extType',
        title: 'extType',
        type: E_FIELD_TYPE.string,
        length: 255,
    }, {
        key: 'isNode',
        title: 'isNode',
        type: E_FIELD_TYPE.boolean,
        readonly: true,
    }, {
        key: 'moderators',
        title: 'Модераторы',
        type: E_FIELD_TYPE.moderatorsSubS,
        needPermissions: [[CLAIM_MD_CITIZEN, CLAIM_MD_CIT_SUPER], [CLAIM_MD_ORGANIZATIONS, CLAIM_MD_ORG_SUPER, ], ],
        onGetValue: (data: any) => {
            const val = AppUtils.getValueByPath(data, 'rubric.moderators');
            if (val && val instanceof Array) {

                return val.map ( m => ({subsystemUid: m.subsystemUid, userIds: m.users.map( u => u.id) }));
            }
            return [];
        },

    // }, {
    //     key: 'moderatorIds',
    //     title: 'Модераторы',
    //     type: E_FIELD_TYPE.multiselect,
    //     editOptions: {
    //         arraySource: {
    //             arrayList: 'moderators',
    //             arrayTitle: 'fullName',
    //             arrayKey: 'id',
    //             orderBy: [{ field: 'fullName' }],
    //         }
    //     },
    //     needPermissions: [[CLAIM_MD_CITIZEN, CLAIM_MD_CIT_SUPER], [CLAIM_MD_ORGANIZATIONS, CLAIM_MD_ORG_SUPER,],],
    //     onGetValue: BaseTableDescriptor.valueToIdsFn('rubric.moderators'),
    },  {
        key: 'executorIds',
        title: 'Исполнители',
        type: E_FIELD_TYPE.multiselect,
        editOptions: {
            arraySource: {
                arrayList: 'executors',
                arrayTitle: 'fullName',
                arrayKey: 'id',
                orderBy: [{ field: 'fullName' }],
            }
        },
        needPermissions: [[CLAIM_MD_CITIZEN, CLAIM_MD_CIT_SUPER], [CLAIM_MD_ORGANIZATIONS, CLAIM_MD_ORG_SUPER,],],
        onGetValue: BaseTableDescriptor.valueToIdsFn('rubric.executors'),
    }, {
        key: 'receivers',
        title: 'receivers',
        type: E_FIELD_TYPE.string,
        needPermissions: [CLAIM_SA_ADMIN],
    }, {
        key: 'type',
        title: 'Наименование типа',
        type: E_FIELD_TYPE.string,

    }, {
        key: 'useForPA',
        title: 'Запись на прием',
        type: E_FIELD_TYPE.boolean,
    },
    // доп поля
    // {
    //     key: 'elDocForUL',
    //     title: 'Электронные документы для ЮЛ',
    //     type: E_FIELD_TYPE.boolean,
    // },

    {
        key: 'docGroupForUL',
        title: 'Группа документов для ЮЛ',
        type: E_FIELD_TYPE.string,
    }

    // """Электронные документы для ЮЛ"""
    // elDocForUL: Boolean

    // """Группа документов для ЮЛ"""
    // docGroupForUL: String
];


export const RUBRIC_ORG_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'rubric';
    entityName = 'rubric';
    updateEntityName = 'updateRubric';

    titlePath = SETTINGS_RUBRIC_TITLE;
    fields = RubricFieldsDefs;
    edit_list = [
        {
            title: 'Основные', fields: [
                FT_SUBSYSTEMS_RUBRIC.key,
                'moderatorIds',
                'moderators',
                'executorIds',
                FT_NAME.key,
                [FT_SNAME.key, FT_WEIGHT.key,],
                FT_DESCRIPTION.key,
                ['docgroup', FT_EXTID.key,],
                // 'receivers',
                // 'useForPA',
            ]
        },
        {
            hidden: true,
            title: 'Юр лица', fields: [
                //     ['', ''],
                //     // 'elDocForUL',
                'docGroupForUL'],
            onDataUpdate(data) {
                this.hidden = !data.rubric.subsystems.find(s => s.uID === SUBSYSTEM_ORG_ID);
            }
        },
        // { title: 'Модераторы', fields: ['moderatorIds'] },
    ];

    view_list = [
        FT_DELETED.key,
        ...environment.production ? [] : [FT_ID.key],
        FT_WEIGHT.key,
        FT_NAME.key, FT_SNAME.key, FT_DESCRIPTION.key];

    filter_layout = [FT_DELETED.key, FT_SUBSYSTEMS_RUBRIC.key, FT_NAME.key];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            list: gqlSdk.getRubricList,
            query: gqlSdk.getRubricById,
            update: gqlSdk.updateRubric,
            create: gqlSdk.createRubric,
            // cacheupdate: [{ obj: GetRubricListDocument, }]
        };
    }
}();


