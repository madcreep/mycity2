import { IJournalItemType } from '../../../generated/types.graphql-gen';
import { IFieldDescriptor, E_FIELD_TYPE, E_ACTION_TYPE } from './descriptors.interfaces';
import { FILTER_OPTS_DATERANGE, FT_DATECREATED } from './fields.const';
import { GridFormatters } from './grid-formatters';
import { RUBRIC_TITLE } from './rubric-organisations.const';
import { BaseTableDescriptor } from './descriptors.const';
import { IProtocolRecord } from 'src/generated/types.graphql-gen';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';




const JournalsFieldsColumnDefs: IFieldDescriptor[] = [
    {
        key: 'timestamp',
        title: 'Дата',
        type: E_FIELD_TYPE.datetime,
        readonly: true,
        gridOption: {
            sortable: true,
            suppressMenu: false,
            valueFormatter: GridFormatters.dateTimeFormatter,
            width: 170,
            maxWidth: 170,
        },
        filterOptions: FILTER_OPTS_DATERANGE,
    }, {
        key: 'user.fullName',
        title: 'Пользователь',
        type: E_FIELD_TYPE.string,
        readonly: true,
        gridOption: {
            suppressMenu: false,
            width: 200,
        },
    }, {
        key: 'eventText',
        title: 'Событие',
        type: E_FIELD_TYPE.string,
        gridOption: {
            width: 600,
            valueFormatter: journalTextFormatter,
        }
    }
];



export const JOURNAL_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'journal';
    entityName = 'journal';
    title = 'Протоколы';
    fields = JournalsFieldsColumnDefs;
    card_action_list = [
        E_ACTION_TYPE.cancel,
        E_ACTION_TYPE.exportExcel,
    ];

    view_list = [
        'timestamp',
        'user.fullName',
        'eventText',
    ];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            list: gqlSdk.getProtocols,
            listKey: 'protocolRecord',
        };
    }
};


function journalTextFormatter (data) {
    let res = '';

    const event = <IJournalItemType>data['data'];
    if (event) {
        res += event.friendlyName + '(' + event.operationName + ') ';

        if (event.properties && Array.isArray(event.properties)) {
            event.properties.forEach(prop => {
                res += `${prop.friendlyName} [ ${prop.newValueFriendly} ]; `
            })
        }
    }
    return res;
}
