import { FT_ID, FT_DUE, FT_WEIGHT, FT_NAME, FT_SNAME, FT_DELETED, FT_EXTID, FT_SUBSYSTEMS_F } from './fields.const';
import { BaseTableDescriptor } from './descriptors.const';
import { IFieldDescriptor } from './descriptors.interfaces';
import { SUBSYSTEM_ORG_ID, SUBSYSTEM_CITIZEN_ID } from '../../services/subsystems.service';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GetRegionListDocument } from 'src/generated/types.graphql-gen';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import {SETTINGS_REGIONS_TITLE} from '../../services/setting-names.const';

const FT_SUBSYSTEMS = FT_SUBSYSTEMS_F('region', <IFieldDescriptor> {
    defaultValue: [SUBSYSTEM_CITIZEN_ID, SUBSYSTEM_ORG_ID]
});


const RegionFieldsColumnDefs: IFieldDescriptor[] = [
    FT_ID,
    FT_DUE,
    FT_WEIGHT,
    FT_NAME,
    FT_SNAME,
    FT_DELETED,
    FT_EXTID,
    FT_SUBSYSTEMS,
];


export const REGION_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'region';
    entityName = 'region';
    updateEntityName = 'updateRegion';
    titlePath = SETTINGS_REGIONS_TITLE;
    title = 'Регионы';
    fields = RegionFieldsColumnDefs;
    edit_list = [
        { title: 'Основные', fields: [
            FT_SUBSYSTEMS.key,
            FT_NAME.key, FT_SNAME.key,
            [FT_EXTID.key, FT_DUE.key, ] ] },
    ];

    view_list = [FT_DELETED.key, FT_NAME.key, FT_SNAME.key, ];
    filter_layout = [FT_DELETED.key, FT_SUBSYSTEMS.key, FT_NAME.key];
    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            create: gqlSdk.createRegion,
            query: gqlSdk.getRegionById,
            update: gqlSdk.updateRegion,
            list: gqlSdk.getRegionList,
            cacheupdate: [{ obj: GetRegionListDocument, }]
        };
    }

}();


