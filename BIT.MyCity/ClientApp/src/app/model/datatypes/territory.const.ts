import { BaseTableDescriptor } from './descriptors.const';
import { IFieldDescriptor, E_FIELD_TYPE as E_FIELD_TYPE } from './descriptors.interfaces';
import { FT_ID, FT_DUE, FT_WEIGHT, FT_NAME, FT_EXTID, FT_SNAME, FT_DELETED, FT_SUBSYSTEMS_F } from './fields.const';
import { SUBSYSTEM_ORG_ID, SUBSYSTEM_CITIZEN_ID } from '../../services/subsystems.service';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GetTerritoryListDocument } from 'src/generated/types.graphql-gen';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import {environment} from '../../../environments/environment';
import {SETTINGS_RUBRIC_TITLE, SETTINGS_TERRITORY_TITLE} from '../../services/setting-names.const';


const FT_SUBSYSTEMS = FT_SUBSYSTEMS_F('territory', <IFieldDescriptor>{
    defaultValue: [SUBSYSTEM_ORG_ID, SUBSYSTEM_CITIZEN_ID, ]
});

const TerritoryFieldsColumnDefs: IFieldDescriptor[] = [
    FT_ID,
    FT_DUE,
    FT_WEIGHT,
    FT_NAME,
    FT_SNAME,
    FT_EXTID,
    FT_DELETED,
    FT_SUBSYSTEMS,
];


export const TERRITORY_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'territory';
    entityName = 'territory';
    titlePath = SETTINGS_TERRITORY_TITLE;
    updateEntityName = 'updateTerritory';
    title = 'Территории';
    fields = TerritoryFieldsColumnDefs;
    edit_list = [
        {
            title: 'Основные', fields: [
                FT_SUBSYSTEMS.key,
                FT_NAME.key,
                [FT_SNAME.key, FT_EXTID.key, ]]
        },
    ];

    view_list = [
        FT_DELETED.key,
        ...environment.production ?  [] : [FT_ID.key, FT_WEIGHT.key, ],
        FT_NAME.key, FT_SNAME.key,
    ];
    filter_layout = [FT_DELETED.key, FT_SUBSYSTEMS.key, FT_NAME.key];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            create: gqlSdk.createTerritory,
            query: gqlSdk.getTerritoryById,
            update: gqlSdk.updateTerritory,
            list: gqlSdk.getTerritoryList,
            cacheupdate: [{ obj: GetTerritoryListDocument, }]
        };
    }

};


