import { IFieldDescriptor, E_FIELD_TYPE } from './descriptors.interfaces';
import { GridFormatters } from './grid-formatters';
import { CLAIM_SA_ADMIN } from '../../services/claims.const';
import { ISubsystem } from 'src/generated/types.graphql-gen';
import { AppUtils, InjectorInstance } from '../../helpers/app.static';
import { AuthService } from '../../services/auth.service';
import { ICoordPoint } from '../../modules/dynamic-inputs/types/map-point-input';
import { SubsystemsService } from '../../services/subsystems.service';
import {SETTINGS_REGIONS_TITLEONE} from '../../services/setting-names.const';



export const FILTER_OPTS_LIKE = {
    readonly: false,
    type: E_FIELD_TYPE.string,
    onFilterGetValue: (data, path, newValue) => {
        return '%' + newValue + '%';
    },
    onFilterSetValue: (data, value) => {
        if (typeof value === 'string' ) {
            if (value[0] === '%' && value[value.length - 1] === '%') {
                return value.substr(1, value.length - 2);
            }
        }
        return value;
    },
};

export const FILTER_OPTS_DATERANGE = {
    type: E_FIELD_TYPE.dateRange,
    onFilterSetValue: (data, value) => {
        if (value instanceof Object) {
            if (value['greaterOrEqual'] && value['less']) {
                const lessDay = AppUtils.dateToStringIso(AppUtils.addDays(value['less'], -1), false);
                return [value['greaterOrEqual'], lessDay];
            }
        }
        return value;
    },
};


export const FT_ID: IFieldDescriptor = {
    key: 'id',
    title: 'ID',
    type: E_FIELD_TYPE.number,
    readonly: true,
    gridOption: {
        sortable: true,
        width: 3,
        minWidth: 20,
    },
};

export const FT_UUID: IFieldDescriptor = {
    key: 'id',
    title: 'GUID',
    type: E_FIELD_TYPE.string,
    readonly: true,
    gridOption: {
        sortable: true,
        width: 3,
        minWidth: 20,
    },
};

export const FT_DUE: IFieldDescriptor = {
    key: 'due',
    title: 'Ключ иерархического объекта',
    type: E_FIELD_TYPE.string,
    readonly: true,
    length: 255,
};

// export const FT_HIDDEN: IFieldDescriptor = {
//     key: 'hidden',
//     title: 'Скрывать',
//     type: E_FIELD_TYPE.boolean,
// };

export const FT_DELETED: IFieldDescriptor = {
    key: 'deleted',
    title: 'Удалено',
    type: E_FIELD_TYPE.boolean,
    gridOption: {
        // is_ld_col : true,
        cellRenderer: GridFormatters.booleanFormatter,
        width: 30,
        minWidth: 80,
        // maxWidth: 30,
    },
    filterOptions: {
        // store_key: 'showDeleted',
        title: 'Показывать удаленные',
        type: E_FIELD_TYPE.boolean,
    }
};

export const FT_ISNODE: IFieldDescriptor = {
    key: 'isNode',
    title: 'Является папкой',
    type: E_FIELD_TYPE.boolean,
};

export const FT_TYPE: IFieldDescriptor = {
    key: 'type',
    title: 'Наименование типа',
    type: E_FIELD_TYPE.string,
};

export const FT_WEIGHT: IFieldDescriptor = {
    key: 'weight',
    title: 'Вес',
    type: E_FIELD_TYPE.number,
    defaultValue: 0,
    gridOption: {
        width: 3,
        minWidth: 20,
    },

};

export const FT_DESCRIPTION: IFieldDescriptor = {
    key: 'description',
    title: 'Описание',
    type: E_FIELD_TYPE.text,
    // length: 5000,
};

export const FT_NAME: IFieldDescriptor = {
    key: 'name',
    title: 'Название',
    type: E_FIELD_TYPE.string,
    length: 256,
    filterOptions: FILTER_OPTS_LIKE,
};

export const FT_SNAME: IFieldDescriptor = {
    key: 'shortName',
    title: 'Краткое название',
    type: E_FIELD_TYPE.string,
    length: 256,
    filterOptions: FILTER_OPTS_LIKE,
};

export const FT_DATECREATED: IFieldDescriptor = {
    key: 'createdAt',
    title: 'Дата создания',
    type: E_FIELD_TYPE.datetime,
    readonly: true,
    gridOption: {
        sortable: true,
        suppressMenu: false,
        valueFormatter: GridFormatters.dateTimeFormatter,
        width: 150,
        maxWidth: 150,
        // cellStyle: {'white-space': 'normal'}

    },
    filterOptions: FILTER_OPTS_DATERANGE,
};

export const FT_DATEUPDATED: IFieldDescriptor = {
    key: 'updatedAt',
    title: 'Дата изменения',
    type: E_FIELD_TYPE.datetime,
    readonly: true,
    filterOptions: FILTER_OPTS_DATERANGE,
};

// export const FT_DATESTART: IFieldDescriptor = {
//     key: 'start',
//     title: 'Дата начала',
//     type: E_FIELD_TYPE.date,
// };

// export const FT_DATEEND: IFieldDescriptor = {
//     key: 'end',
//     title: 'Дата окончания',
//     type: E_FIELD_TYPE.date,
// };

export const FT_TEXT: IFieldDescriptor = {
    key: 'text',
    title: 'Текст',
    type: E_FIELD_TYPE.text,
    defaultValue: '',
    length: 2000,
};

export const FT_TEXT_RICH: IFieldDescriptor = {
    key: 'text',
    title: 'Текст',
    type: E_FIELD_TYPE.textRich,
    defaultValue: '',
    length: 2000,
};

export const FT_SHORTTEXT: IFieldDescriptor = {
    key: 'shortText',
    title: 'Короткий Текст',
    type: E_FIELD_TYPE.text,
    defaultValue: '',
    length: 500,
};

export const FT_EXTID: IFieldDescriptor = {
    key: 'extId',
    title: 'Идентификатор во внешней системе',
    type: E_FIELD_TYPE.string,
    length: 1000,
    readonly: true,
    needPermissions: [CLAIM_SA_ADMIN],
};


export const FT_APPEAL_EXTNUMBER: IFieldDescriptor = {
    key: 'extNumber',
    title: 'Рег. № Дела',
    type: E_FIELD_TYPE.string,
    readonly: true,
    gridOption: {
        sortable: true,
        suppressMenu: false,
        width: 100,
    },
    filterOptions: FILTER_OPTS_LIKE,
};

export const FT_APPEAL_EXTDATE: IFieldDescriptor = {
    key: 'extDate',
    title: 'Рег. дата Дела',
    type: E_FIELD_TYPE.date,
    readonly: true,
    gridOption: {
        sortable: true,
        suppressMenu: false,
        valueFormatter: GridFormatters.dateFormatter,
        width: 80,
    },
    filterOptions: FILTER_OPTS_DATERANGE,
};

export const FT_SUBSYSTEM: IFieldDescriptor = {
    key: 'subsystemId',
    title: 'Подсистема',
    type: E_FIELD_TYPE.string,
    gridOption: {
        valueFormatter: (data) => {
            const sssrv = InjectorInstance.get(SubsystemsService);
            const ss = AppUtils.getValueByPath(data, 'node.data.subsystem.uID') || '---';
            return sssrv.titleForSubsistem(ss);

            // return data.node.data.subsystem.uID;
        }
    },
    filterOptions: {
        type: E_FIELD_TYPE.select,
        // defaultValue: '',
    }
    // onGetValue: ({ appeal }) => {
    //     return appeal.subsistem.uID;
    //     // if (!data || !data[path] || !data[path].subsystems) {
    //     //     return [];
    //     // }
    //     // return data[path].subsystems.map( (r: ISubsystem) => r.uID);
    // },

};

export const FT_SUBSYSTEMUID: IFieldDescriptor = {
    key: 'subsystemUID',
    title: 'Подсистема',
    type: E_FIELD_TYPE.select,
    editOptions: {
        arraySource: {
            arrayList: 'subsystem',
            arrayTitle: 'name',
            arrayKey: 'uID',
        }
    },
    gridOption: {
        valueFormatter: (data) => {
            const sssrv = InjectorInstance.get(SubsystemsService);
            const uid = data.node?.data?.subsystemUID ?? '';
            return sssrv.titleForSubsistem(uid);
        }
    }
};


export const MESSAGES_TEMPLATE: IFieldDescriptor = {
    key: 'MESSAGES_TEMPLATE',
    title: '',
    type: E_FIELD_TYPE.string,
    editOptions: {
        hideAttaches: true,
    }
};

const FT_SUBSYSTEMS: IFieldDescriptor = {
    key: 'subSystemUIDs',
    title: 'Подсистемы',
    type: E_FIELD_TYPE.multiselect,
    hidden: true,
    editOptions: {
        arraySource: {
            arrayList: 'subsystem',
            arrayTitle: 'name',
            arrayKey: 'uID',
        }
    },
    validators: [
        // ValidatorsControl.existValidator(VALIDATOR_TYPE.NON_EMPTY_ARRAY, 'Должна быть выбрана хотя бы одна роль.'),
    ],
    filterOptions: {
        key: 'subsystem.uID',
        type: E_FIELD_TYPE.select,
        defaultValue: '',
        onFilterSetValue: (data, value) => {
            if (data['subsystem'] && data['subsystem']['uID']) {
                data['subsystem.uID'] = data['subsystem']['uID'];
                delete data['subsystem']['uID'];
                delete data['subsystem'];
            }
            return data ? data['subsystem.uID'] : '';
        }
    },
    onInputCreate: (input) => {
        // input.hidden = false;
        const authSrv = InjectorInstance.get(AuthService);
        if (authSrv.isSuperAdmin()) {
            input.hidden = false;
        }
    },

};

export function FT_SUBSYSTEMS_F(path, initial = {}): IFieldDescriptor {

    return Object.assign({}, FT_SUBSYSTEMS, {
        onGetValue: (data) => {
            if (!data || !data[path] || !data[path].subsystems) {
                return [];
            }
            return data[path].subsystems.map( (r: ISubsystem) => r.uID);
        },
    },
    initial);

}


export const FT_ATTACHED_FILES: IFieldDescriptor = {
    key: 'attachedFilesIds',
    title: 'Прикрепленные файлы',
    type: E_FIELD_TYPE.filesPond,
    defaultValue: [],
};

export const FT_OLDSYSTEMID: IFieldDescriptor = {
    key: 'oldSystemId',
    title: 'ID (прошлая версия)',
    type: E_FIELD_TYPE.string,
    readonly: true,
    gridOption: {
        sortable: true,
        suppressMenu: false,
        width: 100,
    },
    filterOptions: FILTER_OPTS_LIKE,
};

export const FT_RUBRICLINK: IFieldDescriptor = {
    key: 'rubricId',
    title: 'Рубрика',
    // titlePath: SETTINGS_RUBRIC_TITLEONE,
    type: E_FIELD_TYPE.select,
    editOptions: {
        arraySource: {
            arrayList: 'rubric',
            arrayTitle: 'name',
            arrayKey: 'id',
            // arrayFilter: { 'subsystem.uID': SUBSYSTEM_CITIZEN_ID },
        }
    },
    filterOptions: {
        type: E_FIELD_TYPE.select,
        defaultValue: '',
    },
    gridOption: {
        sortable: true,
        suppressMenu: false,
        autoHeight: true,
        cellRenderer: (data) => {
            return AppUtils.getValueByPath(data, 'data.rubric.name');
        }
    },
    // onGetValue:  BaseTableDescriptor.valueToIdsFn('rubric.name'),
};

export const FT_REGIONLINK: IFieldDescriptor = {
    key: 'regionId',
    title: 'Регион',
    titlePath: SETTINGS_REGIONS_TITLEONE,
    type: E_FIELD_TYPE.select,
    editOptions: {
        arraySource: {
            arrayList: 'region',
            arrayTitle: 'name',
            arrayKey: 'id',
        }
    },
    gridOption: {
        sortable: true,
        suppressMenu: false,
        autoHeight: true,
        cellRenderer: (data) => {
            return AppUtils.getValueByPath(data, 'data.region.name');
        }
    },
    filterOptions: {
        type: E_FIELD_TYPE.multiselect,
    },
};

export const FT_TERRITORYLINK: IFieldDescriptor = {
    key: 'territoryId',
    title: 'Территория',
    type: E_FIELD_TYPE.select,
    editOptions: {
        arraySource: {
            arrayList: 'territory',
            arrayTitle: 'name',
            arrayKey: 'id',
        }
    },
    gridOption: {
        sortable: true,
        suppressMenu: false,
        autoHeight: true,
        cellRenderer: (data) => {
            return AppUtils.getValueByPath(data, 'data.territory.name');
        }
    },
    filterOptions: {
        type: E_FIELD_TYPE.multiselect,
    },
};

export const FT_TOPICLINK: IFieldDescriptor = {
    key: 'topicId',
    title: 'Тема',
    type: E_FIELD_TYPE.select,
    editOptions: {
        arraySource: {
            arrayList: 'topic',
            arrayTitle: 'name',
            arrayKey: 'id',

        }
    },
    gridOption: {
        sortable: true,
        suppressMenu: false,
        autoHeight: true,
        cellRenderer: (data) => {
            return AppUtils.getValueByPath(data, 'data.topic.name');
        }
    },
    filterOptions: {
        type: E_FIELD_TYPE.multiselect,
    },
};


export const FT_TOPICLINK_O: IFieldDescriptor = {
    key: 'topicId',
    title: 'Тема',
    type: E_FIELD_TYPE.select,
    editOptions: {
        arraySource: {
            arrayList: 'topic-o',
            arrayTitle: 'name',
            arrayKey: 'id',
        }
    },
    gridOption: {
        sortable: true,
        suppressMenu: false,
        autoHeight: true,
        cellRenderer: (data) => {
            return AppUtils.getValueByPath(data, 'data.topic.name');
        }
    },
    filterOptions: {
        type: E_FIELD_TYPE.multiselect,
    },
};

export function FTS_4RUBRICS (uID) {
    return [
        AppUtils.mergeObjects({}, FT_TERRITORYLINK, {
            editOptions: { arraySource: { arrayFilter: { 'subsystem.uID': uID }, } }
        }),
        AppUtils.mergeObjects({}, FT_RUBRICLINK, {
            editOptions: { arraySource: { arrayFilter: { 'subsystem.uID': uID }, } }
        }),
        AppUtils.mergeObjects({}, FT_REGIONLINK, {
            editOptions: { arraySource: { arrayFilter: { 'subsystem.uID': uID }, } }
        }),
        AppUtils.mergeObjects({}, FT_TOPICLINK, {
            editOptions: { arraySource: { arrayFilter: { 'subsystem.uID': uID }, } }
        }),
    ];
}



export const FT_MAP_TEXT: IFieldDescriptor = {
    key: 'place',
    title: 'Указанное месторасположение',
    type: E_FIELD_TYPE.string,
};

export const FT_MAP_POINT: IFieldDescriptor = {
    key: 'mappoint',
    title: 'Точка',
    type: E_FIELD_TYPE.mapPoint,
    // readonly: true,
    defaultValue: { coord: [0, 0], text: '' } as ICoordPoint,
    onGetValue: ({ appeal }) => {
        const val = { coord: [0, 0], text: '' } as ICoordPoint;
        val.coord[0] = appeal.longitude || 0;
        val.coord[1] = appeal.latitude || 0;
        val.text = appeal.place || '';
        return val;
        // return data[path].subsystems.map( (r: ISubsystem) => r.uID);
    },
};
