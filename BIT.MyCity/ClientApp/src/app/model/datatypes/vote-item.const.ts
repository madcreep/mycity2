import { IFieldDescriptor, E_FIELD_TYPE } from './descriptors.interfaces';
import { BaseTableDescriptor } from './descriptors.const';
import { FT_DATECREATED, FT_ID, FT_WEIGHT, FT_DELETED, FT_NAME, FT_SNAME, FT_DATEUPDATED, FT_ATTACHED_FILES, FT_TYPE, FT_DESCRIPTION } from './fields.const';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';



const VoteItemFieldsColumnDefs: IFieldDescriptor[] = [
    FT_DATECREATED,
    FT_DELETED,
    FT_DESCRIPTION,
    FT_ID,
    Object.assign({}, FT_NAME, {
        gridOption: {
            editable: true,
        }
    }),
    // Object.assign({}, FT_SNAME, {
    //     gridOption: {
    //         editable: true,
    //     }
    // }),
    Object.assign({}, FT_WEIGHT, {
        gridOption: {
            editable: true,
            sortable: true,
        }
    }),
    FT_TYPE,
    FT_DATEUPDATED,
    {
        key: 'counter',
        title: 'Сколько пользователей проголосовало за данный пункт',
        type: E_FIELD_TYPE.number,
        readonly: true,
    }, {
        key: 'vote',
        title: 'Голосование, к которому привязан данный пункт',
        type: E_FIELD_TYPE.number,
        readonly: true,
    },
    Object.assign ({}, FT_ATTACHED_FILES, {
        onGetValue: BaseTableDescriptor.valueToIdsFn('attachedFiles'),
    })

    // type VoteItem {
    //     """Присоединенные файлы"""
    //     attachedFiles: [AttachedFile]

    //     """Сколько пользователей проголосовало за данный пункт"""
    //     counter: Int!

    //     """Дата создания объекта"""
    //     createdAt: DateTime

    //     """Удален"""
    //     deleted: Boolean

    //     """Идентификатор объекта"""
    //     id: ID

    //     """Наименование"""
    //     name: String

    //     """Короткое наименование"""
    //     shortName: String

    //     """Наименование типа"""
    //     type: String!

    //     """Дата изменения объекта"""
    //     updatedAt: DateTime

    //     """Голосование, к которому привязан данный пункт"""
    //     vote: Vote

    //     """Для особой сортировки"""
    //     weight: Int
    //   }

];


export const VOTE_ITEM_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'vote-item';
    title = 'Голосования';
    entityName = 'vote';


    fields = VoteItemFieldsColumnDefs;
    edit_list = [
        {
            title: '',
            fields: [FT_NAME.key,
                // FT_SNAME.key,
                FT_ATTACHED_FILES.key, FT_DESCRIPTION.key]
        }

    ];

    view_list = [FT_NAME.key, FT_SNAME.key, FT_WEIGHT.key];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return null;
    }

};


