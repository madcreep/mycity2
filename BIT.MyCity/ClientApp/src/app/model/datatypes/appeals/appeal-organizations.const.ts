import { BaseAppealTableDescriptor } from './appeal-citizen.const';
import { FT_DATECREATED, FT_APPEAL_EXTDATE, FT_APPEAL_EXTNUMBER, FT_DESCRIPTION, FT_SUBSYSTEM, FT_ID, FT_DELETED, FILTER_OPTS_LIKE, FT_RUBRICLINK, FILTER_OPTS_DATERANGE, FT_TOPICLINK_O } from '../fields.const';
import { E_ACTION_TYPE, E_FIELD_TYPE, IFieldDescriptor } from '../descriptors.interfaces';
import { SUBSYSTEM_ORG_ID } from '../../../services/subsystems.service';
import { GridFormatters } from '../grid-formatters';
import { appealFieldsBasedOnSubsystem, CONTACT_TAB_LABEL, FT_APPEAL_TYPE, MESSAGES_TAB_LABEL } from './appeal-base.const';
import { GraphQLClient } from 'src/app/data-services/graphql/graphql.client';
import { GqlApiQueries } from 'src/app/data-services/graphql/sdk-map.interfaces';
import { GetAppealListDocument } from 'src/generated/types.graphql-gen';
import { EnumverbService } from 'src/app/services/enumverb.service';
import { AppUtils } from 'src/app/helpers/app.static';


const ENABLE_REGNUMDATE = true;
const FT_REGNUM = {
    key: 'regNumber',
    title: 'Рег. № организации',
    type: E_FIELD_TYPE.string,
    filterOptions: FILTER_OPTS_LIKE,
};

const FT_REGDATE = {
    // дата регистрации документа (текстовое поле обязательное к заполнению);
    key: 'regDate',
    title: 'Дата регистрации организации',
    type: E_FIELD_TYPE.date,
    filterOptions: FILTER_OPTS_DATERANGE,
    gridOption: {
        sortable: true,
        suppressMenu: false,
        valueFormatter: GridFormatters.dateFormatter,
        width: 140,
    },

};



export const APPEAL_UL_DESCRIPTOR = new class extends BaseAppealTableDescriptor {
    title = 'Электронные документы ЮЛ/ИП';
    id = 'appeal-ul';

    card_action_list = [
        E_ACTION_TYPE.close,
        E_ACTION_TYPE.save,
    ];

    fields: IFieldDescriptor[] = [

        ...appealFieldsBasedOnSubsystem(SUBSYSTEM_ORG_ID, EnumverbService.appealStatus),
        AppUtils.mergeObjects({}, FT_TOPICLINK_O, {
            title: 'Тема обращения',
            editOptions: {
                arraySource: {
                    arrayFilter: { 'subsystem.uID': SUBSYSTEM_ORG_ID },
                    arrayList: 'topic-o',
                }
            }
        }),
        Object.assign({}, FT_DESCRIPTION, {
            title: 'Заголовок к тексту',
        }),
        ... ENABLE_REGNUMDATE ? [ FT_REGDATE, FT_REGNUM ] : [],
        FT_APPEAL_EXTNUMBER,
        FT_APPEAL_EXTDATE,
        {
            key: 'wORegNumberRegDate',
            title: 'Без номера',
            type: E_FIELD_TYPE.boolean,
        }, {
            key: 'signatory',
            title: 'Подписал',
            type: E_FIELD_TYPE.string,
        }, {
            key: 'post',
            title: 'Должность',
            type: E_FIELD_TYPE.string,
        }, {
            key: 'addressee',
            title: 'Должностное лицо - адресат сообщения',
            length: 256,
            type: E_FIELD_TYPE.string,
        }, {
            key: 'organization.shortName',
            title: 'Краткое наименование организации',
            type: E_FIELD_TYPE.string,
            readonly: true,
            filterOptions: FILTER_OPTS_LIKE,
        }, {
            key: 'organization.fullName',
            title: 'Полное наименование организации',
            type: E_FIELD_TYPE.string,
            readonly: true,
            filterOptions: FILTER_OPTS_LIKE,
        }, {
            key: 'organization.email',
            title: 'E-Mail',
            type: E_FIELD_TYPE.string,
            readonly: true,
        }, {
            key: 'organization.addresses',
            title: 'Адрес',
            type: E_FIELD_TYPE.text,
            readonly: true,
            onGetValue: (data) => {
                if (data && data.appeal && data.appeal.organization) {
                    const org = data.appeal.organization;
                    const addressess = org.addresses;
                    if (addressess) {
                        let res = '';
                        addressess.forEach(a => {
                            res += a.fullAddress + '\n';
                        });
                        return res;
                    }
                }
                return '...';
            }
        }, {
            key: 'organization.inn',
            title: 'ИНН',
            type: E_FIELD_TYPE.string,
            readonly: true,
        }, {
            key: 'organization.ogrn',
            title: 'ОГРН',
            type: E_FIELD_TYPE.string,
            readonly: true,
        }
    ];


    edit_list = [
        {
            title: 'Основные',
            fields: [
                // 'approved', 'rejectionReason',
                // ['topicId', '', ],
                ['siteStatus', 'rubricId', ],
                // 'territoryId',
                'addressee',
                'wORegNumberRegDate',
                ['regNumber', 'regDate', ],
                ['signatory', 'post', ],
                [FT_APPEAL_EXTNUMBER.key, FT_APPEAL_EXTDATE.key, ],
                FT_DESCRIPTION.key, 'attachedFilesIds']
        },
        {
            title: CONTACT_TAB_LABEL,
            fields: [['author.fullName', 'organization.email', ],

            'organization.fullName',
            'organization.shortName',
            ['organization.ogrn', 'organization.inn'],
                'organization.addresses',

            ],
        },
        {
            title: MESSAGES_TAB_LABEL,
            fields: []
        },

    ];
    insert_list = [
        {
            title: 'Основные',
            fields: [
                ['topicId', '', ],
                ['siteStatus', 'rubricId', ],
                ['territoryId', ''],
                'addressee',
                'wORegNumberRegDate',
                ... ENABLE_REGNUMDATE ? [ [FT_REGNUM.key, FT_REGDATE.key] ] : [],
                ['signatory', 'post', ],
                FT_DESCRIPTION.key, 'attachedFilesIds',
                'platform', FT_SUBSYSTEM.key,
                FT_APPEAL_TYPE.key,
            ]
        },
    ];

    view_list = [
        FT_DELETED.key,
        FT_ID.key,
        FT_DATECREATED.key,
        'siteStatus',
        'rubricId',
        ... [FT_APPEAL_EXTNUMBER.key, FT_APPEAL_EXTDATE.key, ],
        'organization.shortName',



        ];

    // В данном разделе должны быть следующие фильтры:
    filter_layout: any[] = [
        [
            // FT_DELETED.key,
            'organization.shortName',
            // * Дата
            FT_DATECREATED.key,
        ],
        [
            // * Статус
            'siteStatus',
            // * Орган власти
            FT_RUBRICLINK.key,
        ],
        ... ENABLE_REGNUMDATE ? [ [FT_REGNUM.key, FT_REGDATE.key] ] : [],
        [FT_APPEAL_EXTNUMBER.key, FT_APPEAL_EXTDATE.key, ],
        // * Наименование ЮЛ/ИП
        // 'author.fullName',
    ];


    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            list: gqlSdk.getAppealOrgList,
            listKey: this.entityName,
            create: gqlSdk.createAppeal,
            update: gqlSdk.updateAppeal,
            query: gqlSdk.getAppeal,
            cacheupdate: [{ obj: GetAppealListDocument, }]
        };
    }

};




// type OrganizationType {
//     """Адреса"""
//     addresses: [AddressType]

//     """
//     Территориальная принадлежность ОГВ (только для государственных организаций,
//     код по справочнику «Субъекты Российской федерации» (ССРФ)
//     """
//     agencyTerRang: String

//     """тип ОГВ"""
//     agencyType: AgencyType

//     """
//     Идентификаторы руководителей организации (Policy = ManageUsersOrModerateOrganizations)
//     """
//     chiefIds: [Long!]

//     """Email"""
//     email: String

//     """Email подтвержден"""
//     emailVerified: Boolean

//     """Полное наименование"""
//     fullName: String

//     """Идентификатор объекта"""
//     id: Long!

//     """ИНН"""
//     inn: String

//     """Признак филиала"""
//     isBrunch: Boolean!

//     """КПП"""
//     kpp: String

//     """
//     Код организационно-правовой формы по общероссийскому классификатору организационно-правовых форм
//     """
//     leg: String

//     """ОГРН"""
//     ogrn: String

//     """Головная организация (Policy = ManageUsersOrModerateOrganizations)"""
//     parent: [OrganizationType]

//     """Телефоны"""
//     phones: [PhoneType]

//     """Краткое наименование"""
//     shortName: String

//     """
//     Идентификаторы сотрудников организации (Policy = ManageUsersOrModerateOrganizations)
//     """
//     userIds: [Long!]
//   }
















