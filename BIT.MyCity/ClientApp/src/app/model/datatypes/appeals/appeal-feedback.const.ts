import { appealFieldsBasedOnSubsystem, FT_APPEAL_TYPE, FT_SITESTATUS } from './appeal-base.const';
import { E_ACTION_TYPE, IFieldDescriptor } from '../descriptors.interfaces';
import { GraphQLClient } from 'src/app/data-services/graphql/graphql.client';
import { GqlApiQueries } from 'src/app/data-services/graphql/sdk-map.interfaces';
import { IAppealTypeEnum, GetAppealListDocument } from 'src/generated/types.graphql-gen';
import { BaseAppealTableDescriptor } from './appeal-citizen.const';
import { FT_DELETED, FT_DATECREATED, FT_SUBSYSTEM, FT_DESCRIPTION, MESSAGES_TEMPLATE } from '../fields.const';
import { EnumverbService } from 'src/app/services/enumverb.service';
import { AppUtils } from 'src/app/helpers/app.static';




const AppealFieldsColumnDefs: IFieldDescriptor[] = [
    ...appealFieldsBasedOnSubsystem('', EnumverbService.feedbackStatus),
];
export const FEEDBACK_DESCRIPTOR = new class extends BaseAppealTableDescriptor {

    id = 'appeal-feedback';
    entityName = 'appeal';
    title = 'Обратная связь';

    card_action_list = [
        E_ACTION_TYPE.close,
        E_ACTION_TYPE.save,
    ];

    fields = [
        ...AppealFieldsColumnDefs.map(field => {
            switch (field.key) {
                case FT_DESCRIPTION.key: return Object.assign({}, field, {
                    readonly: true,
                } as IFieldDescriptor);

                case 'email': return Object.assign({}, field, {
                    readonly: true,
                } as IFieldDescriptor);

            }
            return field;
        }),
        Object.assign({}, FT_APPEAL_TYPE, {
            default: IAppealTypeEnum.Feedback
        }),

        MESSAGES_TEMPLATE,
    ];


    edit_list = [
        {
            title: 'Основные',
            fields: [
                [FT_SITESTATUS.key, '', ],
                FT_DESCRIPTION.key,
                [ 'authorName', 'email'],
                MESSAGES_TEMPLATE.key,
            ]
        },
        // {
        //     title: 'Контактная информация',
        //     fields: [
        //         ['region1', 'zip', ], 'address', ['phone', 'email']],
        // },
        // {
        //     title: MESSAGES_TAB_APPEND_ON0,
        //     fields: []
        // },

    ];

    view_list = [FT_DELETED.key, 'id', FT_SUBSYSTEM.key, FT_SITESTATUS.key, FT_DATECREATED.key, 'authorName'];

    filter_layout: any[] = [
        [
            FT_DELETED.key,
            // * Дата
            '',
        ],
        [
            // FT_SUBSYSTEM.key,
            'siteStatus', FT_DATECREATED.key,
            // * Орган власти

        ],
        // * Наименование ЮЛ/ИП
        'authorName',
    ];
    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            list: gqlSdk.getAppealSList,
            listKey: this.entityName,
            create: gqlSdk.createAppeal,
            update: gqlSdk.updateAppeal,
            query: gqlSdk.getAppeal,
            cacheupdate: [{ obj: GetAppealListDocument, }]
        };
    }

    public genRecordTitle(data: any, isNewRecord: boolean): string {
        if (isNewRecord) {
            return 'Новое обращение';
        } else {
            const rec = data[this.entityName];
            let res = rec.id + ' от ' + AppUtils.dateToStringValue(rec.createdAt) + '. ';
            if (rec.authorName) {
                res += rec.authorName;
            } else if (rec.author) {
                res += rec.author.fullName;
            }
            return res;
        }

    }
}();
