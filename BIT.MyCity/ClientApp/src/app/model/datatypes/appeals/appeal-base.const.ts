import {
    FT_SUBSYSTEM, FT_RUBRICLINK, FT_TERRITORYLINK, FT_DATECREATED,
    FT_DELETED, FT_DESCRIPTION, FT_APPEAL_EXTDATE, FT_APPEAL_EXTNUMBER, FILTER_OPTS_LIKE, FT_ATTACHED_FILES, FT_TOPICLINK_O, FT_REGIONLINK
} from '../fields.const';
import {AppUtils, InjectorInstance} from '../../../helpers/app.static';
import {SETTINGS_REGIONS_TITLEONE, SETTINGS_RUBRIC_TITLEONE, SETTINGS_TERRITORY_TITLEONE} from '../../../services/setting-names.const';
import {E_FIELD_TYPE, IFieldDescriptor} from '../descriptors.interfaces';
import {IApprovedEnum, IAppealTypeEnum} from 'src/generated/types.graphql-gen';
import {BaseTableDescriptor} from '../descriptors.const';
import {EnumverbService} from 'src/app/services/enumverb.service';

export const CONTACT_TAB_LABEL = 'Контактная информация';
export const MESSAGES_TAB_LABEL = 'Ответы';
export const JOURNAL_TAB_LABEL = 'Журнал';

const authorRenderer = (params) => {
    if (!params.node || !params.node.data) {
        return params.value;
    }
    const registeredName = AppUtils.getValueByPath(params, 'node.data.author.fullName');
    if (registeredName) {
        return '(a)' + registeredName;
    }
    return params.value;
};


const SitestatusOptions = [
    {
        value: -1,
        statusClass: 'alert-dark',
        iconClass: 'fa fa-pencil-square',
        color: '',
        fontColor: ''
    }, {
        value: 0,
        statusClass: 'alert-dark',
        iconClass: 'fa fa-pencil-square',
    }, {
        value: 1,
        statusClass: 'alert-primary',
        iconClass: 'fa fa-thumbs-up',
    }, {
        value: 2,
        iconClass: 'fa fa-thumbs-up',
    }, {
        value: 3,
        statusClass: 'alert-primary',
        // color: 'Cyan', fontColor: 'Black',
    }, {
        value: 4,
        disabled: true,
        hidden: true,
    }, {
        value: 5, status: 4,
        statusClass: 'alert-success',
        iconClass: 'fa fa-check',
    }, {
        value: 6, status: 5,
        statusClass: 'alert-danger',
        iconClass: 'fa fa-ban',
    }, {
        value: 7,
    }, {
        value: 8,
        iconClass: 'fa fa-pencil-square',
    }, {
        value: 9,
        lineHeight: '12px',
    }
];

export const FT_SITESTATUS: IFieldDescriptor = {
    key: 'siteStatus',
    title: 'Статус',
    type: E_FIELD_TYPE.select,
    defaultValue: 0,
    gridOption: {
        headerName: 'Статус',
        field: 'siteStatus',
        sortable: true,
        suppressMenu: false,
        minWidth: 200,
        // maxWidth: 200,
        width: 200,
        resizable: true,
    },
    filterOptions: {
        type: E_FIELD_TYPE.select,
        defaultValue: null,
    }

};

export const FT_APPEAL_TYPE: IFieldDescriptor = {
    key: 'appealType',
    title: 'Тип записи обращения',
    type: E_FIELD_TYPE.select,
    options: [
        {value: IAppealTypeEnum.Normal, title: 'Обычное'},
        {value: IAppealTypeEnum.Feedback, title: 'Обратная связь'},
    ],
    defaultValue: IAppealTypeEnum.Normal,
    gridOption: {
        cellRenderer: (params) => {
            if (!params.node || !params.node.data) {
                return params.value;
            }
        }
    },
    hidden: true,
};

export function appealFieldsBasedOnSubsystem(subsystem, stateGroups: string) {
    return [
        Object.assign({}, FT_SUBSYSTEM, {
            defaultValue: subsystem,
            hidden: true,
        }),
        FT_APPEAL_TYPE,
        Object.assign({}, FT_DESCRIPTION, {
            title: 'Текст обращения',
        }),
        FT_DELETED,
        FT_APPEAL_EXTDATE,
        FT_APPEAL_EXTNUMBER,
        AppUtils.mergeObjects({},
            Object.assign({}, FT_SITESTATUS, {
                // readonly: subsystem === SUBSYSTEM_CITIZEN_ID,
                optionsFn: () => {
                    return EnumverbService.enum(stateGroups, subsystem).then(
                        (list) => {
                            return list.map(rec => ({value: Number(rec.value), title: rec.shortName}));
                        }
                    );
                },
            }),
            {
                gridOption: {cellRenderer: statusRenderer(subsystem, stateGroups)}
            }
        ),
        Object.assign({}, FT_DATECREATED, {
            title: 'Дата обращения',
        }),
        AppUtils.mergeObjects({}, FT_RUBRICLINK, {
            titlePath: SETTINGS_RUBRIC_TITLEONE,
            editOptions: {arraySource: {arrayFilter: {'subsystem.uID': subsystem}}}
        }),
        AppUtils.mergeObjects({}, FT_REGIONLINK, {
            titlePath: SETTINGS_REGIONS_TITLEONE,
            editOptions: {arraySource: {arrayFilter: {'subsystem.uID': subsystem}}}
        }),
        AppUtils.mergeObjects({}, FT_TERRITORYLINK, {
            titlePath: SETTINGS_TERRITORY_TITLEONE,
            editOptions: {arraySource: {arrayFilter: {'subsystem.uID': subsystem}}}
        }),
        {
            key: 'id',
            title: 'Номер',
            type: E_FIELD_TYPE.number,
            gridOption: {
                headerName: 'Номер',
                field: 'id',
                sortable: true,
                suppressMenu: false,
                // checkboxSelection: true,
                minWidth: 136,
                maxWidth: 136,
                width: 136,
                resizable: false,
                cellRenderer: appealRenderer
            },
            filterOptions: {
                type: E_FIELD_TYPE.string,
            }
        }, {
            key: 'authorName',
            title: 'Автор',
            type: E_FIELD_TYPE.string,
            readonly: true,
            gridOption: {
                sortable: true,
                suppressMenu: false,
                autoHeight: true,
                columnGroupShow: 'open',
                cellRenderer: authorRenderer,
            },
            filterOptions: FILTER_OPTS_LIKE,
        }, {
            key: 'publicGranted',
            title: 'Пользователь дал согласие на публикацию обращения',
            type: E_FIELD_TYPE.boolean,
            filterOptions: {
                type: E_FIELD_TYPE.select,
                defaultValue: 2,
                options: [
                    {id: 2, value: 2, title: 'Все'},
                    {id: 1, value: 1, title: 'Публичные'},
                    {id: 0, value: 0, title: 'Непубличные'},
                ],
            },
            gridOption: {
                headerName: 'Публичность',
                width: 20,
                cellRenderer: publicGrantedRenderer,
                cellStyle: {'padding': '0'},
            },
        }, {
            key: 'address',
            title: 'Адрес',
            type: E_FIELD_TYPE.string,
            length: 255,
        }, {
            key: 'zip',
            title: 'Почтовый индекс',
            type: E_FIELD_TYPE.string,
            length: 6,
        }, {
            key: 'region1',
            title: 'Район/город',
            type: E_FIELD_TYPE.string,
            length: 255,
        }, {
            key: 'phone',
            title: 'Телефон',
            type: E_FIELD_TYPE.string,
            length: 20,
        }, {
            key: 'email',
            title: 'Email',
            type: E_FIELD_TYPE.string,
            length: 255,
        }, {
            key: 'author.fullAddress',
            title: 'Адрес',
            type: E_FIELD_TYPE.string,
            readonly: true,
            gridOption: {
                sortable: true,
                suppressMenu: false,
                autoHeight: true,
                columnGroupShow: 'open',
            },

            filterOptions: FILTER_OPTS_LIKE,
        }, {
            key: 'author.fullName',
            title: 'Автор',
            type: E_FIELD_TYPE.string,
            readonly: true,
            gridOption: {
                sortable: true,
                suppressMenu: false,
                autoHeight: true,
                columnGroupShow: 'open',
            },

            filterOptions: FILTER_OPTS_LIKE,

        }, {
            key: 'platform',
            title: 'Платформа',
            type: E_FIELD_TYPE.string,
            hidden: true,
            defaultValue: 'Web',
        },

        Object.assign({}, FT_ATTACHED_FILES, {
            onGetValue: BaseTableDescriptor.valueToIdsFn('appeal.attachedFiles'),
        })


    ];
}

function gridLoadSpinner(params) {
    return '<div class="ag-custom-loading-cell" style="padding-left: 10px; line-height: 25px;">' +
        '   <i class="fa fa-spinner fa-pulse"></i> <span>' +
        ' Загрузка' +
        ' </span>' +
        '</div>';
}

function appealRenderer(params) {
    if (!params.node || !params.node.data) {
        if (params.rowIndex % params.api.gridOptionsWrapper.gridOptions.cacheBlockSize === 0) {
            return gridLoadSpinner(params);
        }
        return '';
    }


    const siteStatus = params.node.data.siteStatus;
    if (siteStatus === undefined) {
        return params.value;
    }
    const isFeedback = (params.data.appealType === IAppealTypeEnum.Feedback);
    const hasUnapprovedMessages = (params.data.hasUnapprovedMessages === true);
    // && (params.data.siteStatus !== 5 )); // костыль для сочи https://biz-it.atlassian.net/browse/MYC-624
    const isUnapproved = (params.data.publicGranted && (params.data.approved === IApprovedEnum.Undefined));

    const result = '<div class="' +
        getAlert(params.data.siteStatus) + ' appeal-alert alert-flat " style="border-radius: 3px; margin-bottom: 0; padding-left: 12px; padding-right: 12px;"><i class="' +
        getIcon(params.data.siteStatus) + '"></i> <i class="' + getSourceIcon(params.data) + '"></i> ' + params.value +

        ((isUnapproved || hasUnapprovedMessages) ? '<span style="padding-left: 2px">' : '') +

        (!isFeedback && hasUnapprovedMessages ? '<i style="padding-left: 2px; color: red;" class="fa fa-asterisk"></i>' : '') +
        (!isFeedback && isUnapproved ? '<i style="padding-left: 2px; color: darkred;" class="fa fa-file"></i>' : '') +

        ((isUnapproved || hasUnapprovedMessages) ? '</span>' : '') +


        ' </div>'
    ;
    return result;
}

function getIcon(status) {
    const rec = SitestatusOptions.find(s => s.value === status && s.iconClass);
    return rec ? rec.iconClass : '';
}

function getAlert(status) {
    const result = 'alert';
    const rec = SitestatusOptions.find(s => s.value === status && s.statusClass);

    return rec ? rec.statusClass : result;
}

function getSourceIcon(item) {
    switch (item.platform) {
        case 'Mobile':
            return 'fa fa-mobile';
        case 'iOS':
            return 'fa fa-apple';
        case 'Android':
            return 'fa fa-android';
        case 'Web':
            return 'fa fa-internet-explorer';
        case 'VK':
            return 'fa fa-vk';
        default:
            return 'fa fa-question';
    }
}

function statusRenderer(subs, group: string) {
    // return (params) => params.value;
    return (params) => {

        if (!params || params.value === undefined) {
            return '';
        }

        const status = String(params.value);

        const attr = SitestatusOptions.find(s => String(s.value) === status);
        if (!attr) {
            return status;
        }

        const instance = InjectorInstance.get(EnumverbService);
        const list = instance.getEnumsUnsafe(group);
        if (list && list.length) {
            const rec = list.find(s => s.value === String(status) && (!subs || s.subsystemUID === subs));
            if (rec) {
                return '<div class="alert-flat ' + getAlert(status) + '" ' +
                    ' style="border-radius: 3px; height: 100%; background-color: ' + attr.color + '; color: ' + attr.fontColor +
                    // '; white-space: normal; ' +
                    '; align-self: center;' +
                    (attr.lineHeight ? ' line-height: ' + attr.lineHeight + ';' : '') + '">' + rec.shortName + '</div>';
            }
        }

        return '';
    };
}

function publicGrantedRenderer(params) {
    const grant = AppUtils.getValueByPath(params, 'node.data.publicGranted');
    if (grant) {
        const approved: IApprovedEnum = AppUtils.getValueByPath(params, 'node.data.approved');
        if (approved === IApprovedEnum.Accepted) {
            return '<i class="fa fa-check" style="color: green"></i>';
        } else if (approved === IApprovedEnum.Rejected) {
            return '<i class="fa fa-ban" style="color: red"></i>';
        }
        return '<i class="fa fa-check" style="color: gray"></i>';
    }
    return '';

}

