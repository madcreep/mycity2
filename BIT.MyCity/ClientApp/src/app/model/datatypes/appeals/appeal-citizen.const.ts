// APPEAL_DESCRIPTOR

import { BaseTableDescriptor } from '../descriptors.const';
import {IFieldDescriptor, E_ACTION_TYPE, E_FIELD_TYPE, IEditPageDescriptor} from '../descriptors.interfaces';
import {
    FT_DESCRIPTION,
    FT_DATECREATED,
    FT_APPEAL_EXTDATE,
    FT_APPEAL_EXTNUMBER,
    FT_SUBSYSTEM,
    FT_DELETED,
    FT_RUBRICLINK,
    FT_TERRITORYLINK,
    FT_MAP_POINT,
    FT_MAP_TEXT,
    FT_TOPICLINK,
    FT_ID, FT_OLDSYSTEMID
} from '../fields.const';
import { AppUtils } from '../../../helpers/app.static';
import { SUBSYSTEM_CITIZEN_ID } from '../../../services/subsystems.service';
import { appealFieldsBasedOnSubsystem, FT_SITESTATUS, FT_APPEAL_TYPE, CONTACT_TAB_LABEL, MESSAGES_TAB_LABEL, JOURNAL_TAB_LABEL } from './appeal-base.const';
import { GqlApiQueries } from 'src/app/data-services/graphql/sdk-map.interfaces';
import {GetAppealListDocument, IApprovedEnum, IGetAppealListQueryVariables} from 'src/generated/types.graphql-gen';
import { GraphQLClient } from 'src/app/data-services/graphql/graphql.client';
import { EnumverbService } from 'src/app/services/enumverb.service';
import {QueryOptionsAlone} from 'apollo-angular/types';
import {EnvSettings} from '../../settings/envsettings';

const AppealFieldsColumnDefs: IFieldDescriptor[] = [
    ...appealFieldsBasedOnSubsystem(SUBSYSTEM_CITIZEN_ID, EnumverbService.appealStatus),
    AppUtils.mergeObjects({}, FT_TOPICLINK, {
        title: 'Тема обращения',
        editOptions: {
            arraySource: {
                arrayFilter: { 'subsystem.uID': SUBSYSTEM_CITIZEN_ID },
                arrayList: 'topic',
            }
        }
    }),
    FT_OLDSYSTEMID,
    Object.assign({}, FT_DESCRIPTION, {
        title: 'Текст обращения',
    }),
    {
        key: 'rejResVariants',
        title: 'Причина отклонения',
        type: E_FIELD_TYPE.select,
        manualLayout: true,
        noStoreValue: true,
        options: [
            'Обращение не содержит конкретных заявлений, жалоб, предложений',
            'Обращение содержит персональные данные',
            'В обращении содержатся нецензурные либо оскорбительные выражения',
            'Другое...',
        ].map (text => <any>{ value: text, title: text } ),

    }, {
        key: 'rejectionReason',
        title: 'Причина отказа',
        type: E_FIELD_TYPE.text,
        length: 2000,
        readonly: true,
        hidden: true,
        noStoreValue: true,
        manualLayout: true,
    }, {
        key: 'approved',
        title: '',
        type: E_FIELD_TYPE.select,
        options: [
            { value: IApprovedEnum.Accepted, title: 'Принято' },
            { value: IApprovedEnum.Rejected, title: 'Отклонено' },
            { value: IApprovedEnum.Undefined, title: 'Не решено' },
        ],
        hidden: true,
        manualLayout: true,
        defaultValue: 'null',
        readonly: true,
        noStoreValue: true,
        filterOptions: {
            type: E_FIELD_TYPE.select,
            defaultValue: 1,
            options: [
                { id: 2, value: 2, title: 'Все' },
                { id: 1, value: 1, title: 'Одобренные' },
                { id: 0, value: 0, title: 'Отклоненные' },
            ],
        }

    },

];



export class BaseAppealTableDescriptor extends BaseTableDescriptor {
    id = 'appeal-citizen';
    entityName = 'appeal';
    updateEntityName = 'updateAppeal';
    title = 'Обращения';
    fields = [
        ... AppealFieldsColumnDefs,
        FT_MAP_POINT,
        FT_MAP_TEXT,
    ];
    card_action_list = [
        E_ACTION_TYPE.close,
        E_ACTION_TYPE.save,
        E_ACTION_TYPE.register,
    ];

    edit_list = [ {
            title: 'Основные',
            fields: [
                [FT_SITESTATUS.key, FT_RUBRICLINK.key, ],
                [FT_TOPICLINK.key, FT_TERRITORYLINK.key, ],
                [FT_APPEAL_EXTNUMBER.key, FT_APPEAL_EXTDATE.key, ],
                [FT_DATECREATED.key, ''],
                ['publicGranted', ''],
                FT_DESCRIPTION.key, 'attachedFilesIds',
                'approved', 'rejResVariants', 'rejectionReason',
            ]
        }, {
            title: CONTACT_TAB_LABEL,
            fields: [
                // ['region1', 'zip', ],
                // 'address',
                // 'author.fullAddress',
                ['phone', 'email']],
        }, {
            title: JOURNAL_TAB_LABEL,
            fields: []
        }, {
            title: MESSAGES_TAB_LABEL,
            fields: []
        },
    ];

    insert_list = [ {
            title: 'Основные',
            fields: [
                [FT_SITESTATUS.key, FT_RUBRICLINK.key, ],
                [FT_TOPICLINK.key, FT_TERRITORYLINK.key, ],
                [FT_APPEAL_EXTNUMBER.key, FT_APPEAL_EXTDATE.key, ],
                ['publicGranted', ],
                FT_DESCRIPTION.key, 'attachedFilesIds',

                'platform', FT_SUBSYSTEM.key,
                FT_APPEAL_TYPE.key,
            ]
        }, {
            title: CONTACT_TAB_LABEL,
            fields: [['region1', 'zip',], ['phone', 'email']],
        },
    ];

    filter_layout: any[] = [
        [
            FT_DELETED.key,
            'siteStatus',
        ],
        [FT_ID.key , FT_DATECREATED.key, ],
        [ /* FT_TERRITORYLINK.key, */ FT_RUBRICLINK.key, ''],
        'author.fullName',
        [ FT_APPEAL_EXTNUMBER.key, FT_APPEAL_EXTDATE.key]
    ];

    view_list = [
        FT_DELETED.key,
        'id',
        FT_SITESTATUS.key,
        FT_DATECREATED.key,
        'publicGranted',
        // FT_TERRITORYLINK.key,
        FT_RUBRICLINK.key,
        FT_APPEAL_EXTDATE.key,
        FT_APPEAL_EXTNUMBER.key,
        'authorName'
    ];

    get_edit_list(): IEditPageDescriptor[] {
        const list = EnvSettings.Citizens?.editList ?? this.edit_list;
        return list;
    }

    fieldsView () {
        const list = EnvSettings.Citizens?.viewList ?? this.view_list;
        return BaseTableDescriptor.ListOf(this.entityName, this.fields, list);
    }

    fieldsFilter(): IFieldDescriptor[] {
        const list = EnvSettings.Citizens?.filterList ?? this.filter_layout;
        this.filter_layout = list;
        return super.fieldsFilter();
    }

    genRecordTitle(data: any, isNewRecord: boolean): string {
        if (isNewRecord) {
            return 'Новое обращение';
        } else {
            const rec = data[this.entityName];
            let res = rec.id + ' от ' + AppUtils.dateToStringValue(rec.createdAt) + '. ';
            if (rec.author && rec.author.fullName) {
                res += rec.author.fullName;
            } else if (rec.authorName) {
                res += rec.authorName;
            }
            return res;
        }
    }

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {

        function getAppealList(variables?, options?) {
            if (variables['skipTotal'] === true) {
                return gqlSdk.getAppealList(variables, options);
            }
            return gqlSdk.getAppealListWTotal(variables, options);
        }

        return {
            id: this.id,
            entity: this.entityName,
            list: getAppealList,
            listKey: this.entityName,
            create: gqlSdk.createAppeal,
            update: gqlSdk.updateAppeal,
            query: gqlSdk.getAppeal,
            cacheupdate: [{ obj: GetAppealListDocument, }]
        };
    }
}

export const APPEAL_CIT_DESCRIPTOR = new BaseAppealTableDescriptor();



