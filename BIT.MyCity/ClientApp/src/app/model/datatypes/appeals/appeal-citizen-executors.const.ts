// APPEAL_DESCRIPTOR

import { BaseTableDescriptor } from '../descriptors.const';
import { AppUtils } from '../../../helpers/app.static';
import { GqlApiQueries } from 'src/app/data-services/graphql/sdk-map.interfaces';
import { GetAppealListDocument } from 'src/generated/types.graphql-gen';
import { GraphQLClient } from 'src/app/data-services/graphql/graphql.client';
import { APPEAL_CIT_DESCRIPTOR, BaseAppealTableDescriptor } from './appeal-citizen.const';
import {FT_RUBRICLINK, FT_TOPICLINK, FT_MAP_POINT, FT_MAP_TEXT, FT_TERRITORYLINK, FT_DESCRIPTION, FT_ATTACHED_FILES, FT_REGIONLINK} from '../fields.const';


export class AppealExtTableDescriptor extends BaseAppealTableDescriptor {
    id = 'appeal-ex-c';
    title = 'Исполнитель';
    entityName = 'appeal';
    hidden = true;
        constructor () {
            super();

            this.fields.find(f => f.key === FT_RUBRICLINK.key).readonly = true;
            this.fields.find(f => f.key === FT_TOPICLINK.key).readonly = true;
            this.fields.find(f => f.key === FT_MAP_POINT.key).readonly = true;
            this.fields.find(f => f.key === FT_MAP_TEXT.key).readonly = true;
            this.fields.find(f => f.key === FT_TERRITORYLINK.key).readonly = true;
            this.fields.find(f => f.key === FT_REGIONLINK.key).readonly = true;
            this.fields.find(f => f.key === FT_DESCRIPTION.key).readonly = true;
            this.fields.find(f => f.key === FT_ATTACHED_FILES.key).readonly = true;
            this.fields.find(f => f.key === 'publicGranted').readonly = true;
            // this.fields.find(f => f.key === FT_a.key).readonly = true;
        }
        public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
            return {
                id: this.id,
                entity: this.entityName,
                list: gqlSdk.getExecutorAppealsList,
                listKey: 'appealWhereExecutor',
                create: gqlSdk.createAppeal,
                update: gqlSdk.updateAppeal,
                query: gqlSdk.getAppeal,
                cacheupdate: [{ obj: GetAppealListDocument, }]
            };
        }


}


export const APPEAL_CIT_EX_DESCRIPTOR = new AppealExtTableDescriptor();

// AppUtils.mergeObjects(
//     {},
//     APPEAL_CIT_DESCRIPTOR,
//     {
//         id: 'appeal-ex-c',
//         title: 'Исполнитель',
//         getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
//             return {
//                 id: this.id,
//                 entity: this.entityName,
//                 list: gqlSdk.getExecutorAppealsList,
//                 listKey: 'appealWhereExecutor',
//                 create: gqlSdk.createAppeal,
//                 update: gqlSdk.updateAppeal,
//                 query: gqlSdk.getAppeal,
//                 cacheupdate: [{ obj: GetAppealListDocument, }]
//             };
//         }
//     } as BaseTableDescriptor,
// );




