import { BaseTableDescriptor } from './descriptors.const';
import { IFieldDescriptor, E_FIELD_TYPE } from './descriptors.interfaces';
import { FT_NAME, FT_SNAME, FT_DESCRIPTION, FT_SUBSYSTEMUID, FT_ID } from './fields.const';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { AppUtils, InjectorInstance } from '../../helpers/app.static';
import { EnumverbService } from '../../services/enumverb.service';


const FieldsColumnDefs: IFieldDescriptor[] = [
    FT_NAME,
    Object.assign({}, FT_SNAME, {
        gridOption: {
            cellRenderer: (data) => {

                return data.value ||
                    (data.node.data?.name ?? '');
            }
        }
    }),
    FT_DESCRIPTION,
    FT_SUBSYSTEMUID,
    // FT_DELETED, // MYC-520
    FT_ID,
    {
        key: 'group',
        title: 'Группа',
        type: E_FIELD_TYPE.select,

        optionsFn: () => {
            return EnumverbService.enum(EnumverbService.groupDescriptor, '').then(
                (list) => {
                    return list.map(rec => ({ value: rec.value, title: rec.shortName }));
                }
            );
        },

        gridOption: {
            cellRenderer: (data) => {
                const srv = InjectorInstance.get(EnumverbService);
                const t = srv.enumGroupsTypes.find(e => e.value === data.value);
                return t ? t.title : data.value;
            }
        }
    }, {
        key: 'value',
        title: 'Значение',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'customSettings',
        title: 'Дополнительные настройки',
        type: E_FIELD_TYPE.text,
    }
];

export const ENUMVERBS_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'enumverbs';
    entityName = 'enumVerb';
    updateEntityName = 'updateEnumVerb';
    title = 'Статусы и Справочники';
    fields = FieldsColumnDefs;
    edit_list = [
        {
            title: 'Основные', fields: [
                ['group', FT_SUBSYSTEMUID.key],
                [FT_SNAME.key, FT_NAME.key],
                ['value', ''],
                FT_DESCRIPTION.key,
                'customSettings'
            ],
        },
    ];

    view_list = [
        // FT_DELETED.key,
        FT_SUBSYSTEMUID.key,
        'group',
        'value',
        FT_SNAME.key,
        ];
    filter_layout = [
        FT_SUBSYSTEMUID.key,
        'group',
        FT_SNAME.key
    ];
    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            create: gqlSdk.createEnumVerb,
            query: gqlSdk.getEnumVerbModById,
            update: (variables, opts) => {
                const vars = AppUtils.deepCopy(variables);
                if (vars['id'] && vars['enumVerb']) {
                    vars['enumVerb']['id'] = vars['id'];
                    delete vars['id'];
                }
                return gqlSdk.updateEnumVerb(vars, opts);
            },
            list: gqlSdk.getEnumVerbModList,
            listKey: 'enumVerb',
            // cacheupdate: [{ obj: GetNewsModListDocument, }]
        };
    }
    genRecordTitle(data: any, isNewRecord: boolean): string {
        if (isNewRecord) {
            return 'Новая запись справочника';
        } else {
            const rec = data[this.entityName];
            let res = rec.shortName;
            return res || '';
        }

    }
}();
