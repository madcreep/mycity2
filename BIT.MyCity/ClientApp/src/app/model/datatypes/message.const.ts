import { BaseTableDescriptor } from './descriptors.const';
import { E_FIELD_TYPE, IFieldDescriptor } from './descriptors.interfaces';
import { FT_ATTACHED_FILES } from './fields.const';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';


export enum MESSAGE_PARENTTYPE {
    appeal = 'Appeal',
    message = 'Message',
    vote = 'Vote',
}

const MessagesFieldsColumnDefs: IFieldDescriptor[] = [
    {
        key: 'text',
        title: 'Введите ответ:',
        type: E_FIELD_TYPE.text,
    },
    Object.assign ({}, FT_ATTACHED_FILES, {
        onGetValue: BaseTableDescriptor.valueToIdsFn('message.attachedFiles'),
    })

];

export const MESSAGE_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'message';
    entityName = 'message';
    updateEntityName = 'updateMessage';
    title = 'Комментарии';
    fields = MessagesFieldsColumnDefs;
    card_action_list = [];

    edit_list = [
        {
            title: 'appeal',
            fields: ['text', FT_ATTACHED_FILES.key]
        },
    ];

    view_list = ['text'];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            create: gqlSdk.createMessage,
            update: gqlSdk.updateMessage,
        };
    }
}();
