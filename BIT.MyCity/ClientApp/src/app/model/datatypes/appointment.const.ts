import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { CONTACT_TAB_LABEL, MESSAGES_TAB_LABEL } from './appeals/appeal-base.const';
import { BaseTableDescriptor } from './descriptors.const';
import { E_ACTION_TYPE, E_FIELD_TYPE, IFieldDescriptor } from './descriptors.interfaces';
import { FT_DESCRIPTION } from './fields.const';



const AppoimentStatusOptions = [
    { value: 0, title: 'Черновик', },
    { value: 1, title: 'Направлено на рассмотрение', },
    { value: 2, title: 'Подтверждено', },
    { value: 3, title: 'Отказано', },
    { value: 4, title: 'Отменено', },
    { value: 5, title: 'Архивная', },
];
const AppoimentFieldsColumnDefs: IFieldDescriptor[] = [
    {
        key: 'status',
        title: 'Состояние',
        type: E_FIELD_TYPE.select,
        options: AppoimentStatusOptions,
        defaultValue: 0,
        gridOption: {
            headerName: 'Состояние',
            field: 'status',
            sortable: false,
            suppressMenu: true,
            width: 150,
            cellRenderer: statusRenderer
        }
    }, {
        key: 'authorName',
        title: 'Заявитель',
        type: E_FIELD_TYPE.string,
        gridOption: {
            // headerName: 'Заявитель',
            // field: 'authorName',
            sortable: false,
            suppressMenu: true,
            width: 150
        },
    }, {
        key: 'number',
        title: 'Номер',
        type: E_FIELD_TYPE.number,
        readonly: true,
        gridOption: {
            // headerName: 'Номер',
            // field: 'number',
            sortable: false,
            suppressMenu: true,
            width: 50
        }
    }, {
        key: 'date',
        title: 'Дата',
        type: E_FIELD_TYPE.date,
        readonly: true,
        gridOption: {
            // headerName: 'Дата',
            // field: 'date',
            sortable: true,
            suppressMenu: false,
            // checkboxSelection: true,
            pinned: 'left',
            valueFormatter: dateFormatter,
            width: 100
        },
    }, {
        key: 'rejectionReason',
        title: 'Причина отказа',
        type: E_FIELD_TYPE.string,
        readonly: true,
    },
    FT_DESCRIPTION,
    {
        key: 'municipality',
        title: 'Орган власти',
        type: E_FIELD_TYPE.string,
        readonly: true,
    }, {
        key: 'officerName',
        title: 'Должностное лицо',
        type: E_FIELD_TYPE.string,

    }, {
        key: 'officer.position',
        title: 'Должностное лицо',
        type: E_FIELD_TYPE.string,
        gridOption: {
            // headerName: 'Должностное лицо',
            // field: 'officer.position',
            sortable: false,
            suppressMenu: true,
        },
    }, {
        key: 'officer.name',
        title: 'ФИО',
        type: E_FIELD_TYPE.string,
        gridOption: {
            // headerName: 'ФИО',
            // field: 'officer.name',
            sortable: false,
            suppressMenu: true,
            width: 150
        },
    }, {
        key: 'email',
        title: 'Email заявителя',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'region',
        title: 'Регион заявителя',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'address',
        title: 'Адрес заявителя',
        type: E_FIELD_TYPE.string,
    }, {
        key: 'phone',
        title: 'Телефон заявителя',
        type: E_FIELD_TYPE.string,
    }






];

export const APPOINTMENT_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'appointment';
    entityName = 'appointment';
    title = 'Заявки на прием';
    fields = AppoimentFieldsColumnDefs;
    card_action_list = [
        E_ACTION_TYPE.cancel,
        E_ACTION_TYPE.reject,
        // E_ACTION_TYPE.protocol,
        E_ACTION_TYPE.accept,
        E_ACTION_TYPE.save,
        E_ACTION_TYPE.exportExcel,
    ];

    edit_list = [
        {
            title: 'Основные',
            fields: ['status', 'authorName', 'number', 'date', 'rejectionReason', FT_DESCRIPTION.key, 'municipality', 'officerName']
        }, {
            title: 'Файлы',
            fields: []
        }, {
            title: CONTACT_TAB_LABEL,
            fields: ['authorName', 'email', 'region', 'phone']
        }, {
            title: MESSAGES_TAB_LABEL,
            fields: []
        }

    ];

    view_list = ['date', 'number', 'officer.position', 'officer.name', 'authorName', 'status', ];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            id: this.id,
            entity: this.entityName,
            list: gqlSdk.getAppointmentList,
            query: gqlSdk.getAppointment,
        };
    }
};


function dateFormatter(params) {
    let result = '';
    if (params && params.value && params.value) {
        const dateTime = new Date(params.value);
        result = dateTime.toLocaleDateString('ru');
    }
    return result;
}

function statusRenderer(params) {
    const status = params.value;
    if (!status) {
        return '';
    }
    let result = '<div class="alert ';
    switch (status) {
        case 0:
            result += 'alert-light">Черновик';
            break;
        case 1:
            result += 'alert-primary">Отправлено';
            break;
        case 2:
            result += 'alert-success">Подтверждено';
            break;
        case 3:
            result += 'alert-danger">Отказано';
            break;
        case 4:
            result += 'alert-warning">Отменено';
            break;
        case 5:
            result += 'alert-dark">В архиве';
            break;
        default:
            result += '>';
    }
    result += '</div>';
    return result;
}


// const t = gql`
// type Appointment {
//   address: String
//   admin: Boolean
//   answers: [AppointmentNotification]
//   appointmentRange: AppointmentRange
//   attachedFiles: [AttachedFile]
//   authorName: String
//   authorNameUpperCase: String

//   """Дата создания объекта"""
//   createdAt: Date!
//   date: Date

//   """Удален"""
//   deleted: Boolean
//   description: String

//   """ДУЕ"""
//   due: String
//   email: String

//   """Скрыт"""
//   hidden: Boolean

//   """Идентификатор объекта"""
//   id: Long!

//   """Является папкой"""
//   isNode: Boolean
//   municipality: String
//   number: Int
//   officer: Officer
//   phone: String
//   region: String
//   rejectionReason: String
//   rubric: Rubric

//   """Для особой сортировки"""
//   sortingOrder: Int
//   status: Int
//   topic: Topic

//   """Наименование типа"""
//   type: String

//   """Дата изменения объекта"""
//   updatedAt: Date
//   zip: String
// }
// `;
