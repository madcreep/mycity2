import { IFieldDescriptor, E_FIELD_TYPE, E_ACTION_TYPE } from './descriptors.interfaces';
import { BaseTableDescriptor } from './descriptors.const';
import { FT_DATECREATED, FT_ID, FT_WEIGHT, FT_DELETED, FT_NAME, FT_SNAME, FT_TEXT, FT_DATEUPDATED, FT_ATTACHED_FILES, FTS_4RUBRICS, FT_REGIONLINK, FT_TERRITORYLINK, FT_TOPICLINK, FT_RUBRICLINK } from './fields.const';
import { IStatusEnum, IVoteRoot } from 'src/generated/types.graphql-gen';
import { AppUtils } from '../../helpers/app.static';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { SUBSYSTEM_VOTE } from '../../services/subsystems.service';



const VoteRootsFieldsColumnDefs: IFieldDescriptor[] = [
    FT_ID,
    FT_DATECREATED,
    FT_DATEUPDATED,
    FT_WEIGHT,
    FT_DELETED,
    FT_NAME,
    FT_SNAME,
    FT_TEXT,
    ... FTS_4RUBRICS(SUBSYSTEM_VOTE),

    {
        key: 'startAt',
        title: 'Расписание - Запустить',
        type: E_FIELD_TYPE.datetime,
    }, {
        key: 'endAt',
        title: 'Расписание - Завершить',
        type: E_FIELD_TYPE.datetime,
    }, {
        key: 'archiveAt',
        title: 'Расписание - Убрать в Архив',
        type: E_FIELD_TYPE.datetime,

    }, {

    // {
    //     key: 'mode',
    //     title: 'Режим',
    //     type: E_FIELD_TYPE.select,
    //     options: [
    //         { value: 'ONE_SELECT', title: 'Можно голосовать только за 1 пункт' },
    //         { value: 'MANY_SELECT', title: 'Можно голосовать за несколько пунктов' },
    //     ],
    //     defaultValue: 'ONE_SELECT',
    // }, {
    //     key: 'modeLimitValue',
    //     title: 'Максимальное число пунктов', // в режиме MANY_SELECT
    //     type: E_FIELD_TYPE.number,
    //     defaultValue: 1,
    // }, {
    //     key: 'offerEnabled',
    //     title: 'Можно предложить свой вариант',
    //     type: E_FIELD_TYPE.boolean,
    //     defaultValue: false,

        key: 'state',
        title: 'Текущее состояние',
        type: E_FIELD_TYPE.select,
        readonly: true,
        options: [
            { value: IStatusEnum.Draft, title: 'Черновик' },
            { value: IStatusEnum.IsPlanned, title: 'Будет' },
            { value: IStatusEnum.InProgress, title: 'Идет' },
            { value: IStatusEnum.IsDone, title: 'Закончилось' },
            { value: IStatusEnum.IsArchived, title: 'Архив' },
        ],
        // defaultValue: 'IS_PLANNED',
        filterOptions: {
            options: [
                { value: null, title: 'Все' },
                { value: IStatusEnum.Draft, title: 'Черновик' },
                { value: IStatusEnum.IsPlanned, title: 'Будет' },
                { value: IStatusEnum.InProgress, title: 'Идет' },
                { value: IStatusEnum.IsDone, title: 'Закончилось' },
                { value: IStatusEnum.IsArchived, title: 'Архив' },
            ],
            defaultValue: null,
        },
        gridOption: {
            valueFormatter: statusFormatter,
        }
    }, {
        key: 'textResult',
        title: 'Описание итогов голосования',
        type: E_FIELD_TYPE.text,
        defaultValue: '',
        length: 5000,
    // }, {
    //     key: 'voteItems',
    //     title: '',
    //     type: E_FIELD_TYPE.checkbox-group,
    //     defaultValue: [],
    //     hidden: true,
    },
    Object.assign({}, FT_ATTACHED_FILES, {
        onGetValue: BaseTableDescriptor.valueToIdsFn('voteRoot.attachedFiles'),
    }),

    {
        key: 'votesIds',
        title: 'Список вопросов',
        type: E_FIELD_TYPE.checkBoxGroup,
        defaultValue: [],
        hidden: true,
    }, {
        key: 'importSection',
        type: E_FIELD_TYPE.text,
        title: 'Построчный импорт',
        hidden: true,
        readonly: true,
    }, {
        key: 'commentsAllowed',
        title: 'Можно оставлять комментарии',
        defaultValue: true,
        type: E_FIELD_TYPE.boolean,
    }

];


export const VOTEROOTS_DESCRIPTOR = new class extends BaseTableDescriptor {
    id = 'voteRoot';
    title = 'Голосования';
    entityName = 'voteRoot';


    fields = VoteRootsFieldsColumnDefs;
    edit_list = [
        {
            title: 'Основные', fields: [
                [FT_SNAME.key, 'state', ],
                FT_NAME.key,
                '-',
                ['startAt', 'endAt'],
                ['archiveAt', FT_DATEUPDATED.key],
                '-',
                [FT_TERRITORYLINK.key, FT_REGIONLINK.key, ],
                [FT_TOPICLINK.key, FT_RUBRICLINK.key, ],
                '-',
                FT_TEXT.key,

                FT_ATTACHED_FILES.key,
                ['commentsAllowed', '-'],
                'textResult',
            ]
        }, {
            title: 'Вопросы', fields: [
                'votesIds',
                'importSection'
            ]
        }, {
            title: 'Результаты', fields: [
            ]
        }
    ];
    card_action_list = [
        E_ACTION_TYPE.close,
        E_ACTION_TYPE.apply,
    ];
    insert_list = AppUtils.deepCopy([this.edit_list[0], this.edit_list[1]]);

    view_list = [FT_ID.key, FT_DATECREATED.key, FT_NAME.key, FT_SNAME.key, 'state'];
    filter_layout = ['state'];

    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            entity: this.entityName,
            id: this.id,
            create: gqlSdk.createVoteRootMod,
            list: gqlSdk.getVoteRootModList,
            query: gqlSdk.getVoteRootModId,
            update: gqlSdk.updateVoteRootMod,
        };
    }

};


function statusFormatter(data) {
    const result = '';
    const vote: IVoteRoot = data.data;
    switch (vote.state) {
        case IStatusEnum.Draft: {
            return 'Черновик';
        }
        case IStatusEnum.IsPlanned: {
            if (vote.startAt) {
                return 'Запуск ' + AppUtils.dateTimeToStringValue(vote.startAt);
            }
            return 'Ожидает запуска';
        }
        case IStatusEnum.InProgress: {
            if (vote.endAt) {
                return 'Активно. Завершение ' + AppUtils.dateTimeToStringValue(vote.endAt);
            }
            return 'Активно';
        }
        case IStatusEnum.IsDone: {
            if (vote.archiveAt) {
                return 'Завершено. Архивирование ' + AppUtils.dateTimeToStringValue(vote.archiveAt);
            }
            return 'Завершено';
        }
        case IStatusEnum.IsArchived: {
            return 'Архив';
        }
        default: {
            break;
        }
    }

    return result;
}
