import { BaseTableDescriptor } from './descriptors.const';
import { GraphQLClient } from '../../data-services/graphql/graphql.client';
import { GqlApiQueries } from '../../data-services/graphql/sdk-map.interfaces';





export const EXECUTORS_DESCRIPTOR = new class extends BaseTableDescriptor {
    id: 'executors';
    entityName: 'executors';
    updateEntityName = 'updateExecutor';
    public getApiDescriptor(gqlSdk: GraphQLClient): GqlApiQueries {
        return {
            entity: 'executors',
            id: 'executors',
            list: gqlSdk.availableExecutors,
            listKey: 'availableExecutors',
        };
    }
}();

