import { DynamicInputsModule } from './../dynamic-inputs/dynamic-inputs.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardToolbarComponent } from './components/card-toolbar/card-toolbar.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CardToolbarComponent,
  ],
  imports: [
    FormsModule,
    // ReactiveFormsModule,
    CommonModule,
    BrowserModule,
  ],
  exports: [
    CardToolbarComponent,
  ]
})
export class CardControlsModule { }
