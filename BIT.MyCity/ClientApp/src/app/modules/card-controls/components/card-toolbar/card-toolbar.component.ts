import { ICardAction, ICardActionButton } from '../../../../views/cards/card.interfaces';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

@Component({
    selector: 'app-card-toolbar',
    templateUrl: './card-toolbar.component.html',
    styleUrls: ['./card-toolbar.component.scss']
})
export class CardToolbarComponent implements OnInit, OnChanges {

    @Input() isTabs = false;
    @Input() actionList: ICardAction[] = [];
    @Output() actionClick: EventEmitter<ICardActionButton> = new EventEmitter<ICardActionButton>();

    public buttons: ICardActionButton[];

    constructor() { }

    ngOnChanges(changes: SimpleChanges): void {
        this.buttons = this.actionList.map ( (a, i) => <ICardActionButton>{
            action: a,
            toggled: false,
            active: i === 0,
            disabled: a.disabled ?? false,
            // hidden: a.hidden,
            class: a.class });
    }

    getClass(item: ICardActionButton): string {
        if (item.class) {
            return item.class;
        } else {
            return 'btn-outline-dark';
        }
        // {: !item.class, item.class: !!item.class }
    }

    ngOnInit() {

    }

    onClick(button: ICardActionButton) {
        if (this.isTabs) {
            this.buttons.forEach( b => b.active = false);
            button.active = true;
        }
        this.actionClick.emit(button);
    }
}
