import { InputBase } from './input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';


export interface ICoordPoint {
    coord: number[];
    text: string;
}

export class MapPointInput extends InputBase<ICoordPoint> {
    controlType = E_FIELD_TYPE.mapPoint;

    constructor(descr = null, keyPrefix: string, value: ICoordPoint) {
        super(descr, keyPrefix, null);
        // this.value = [null, null];
        this.value = value || {
            coord: [0, 0],
            text: '',
        };
    }
}
