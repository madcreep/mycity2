import { InputBase } from './input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';

export class DateInput extends InputBase<Date> {
    controlType = E_FIELD_TYPE.date;

    constructor(descr = null, keyPrefix: string, value: Date) {
        super(descr, keyPrefix, null);
        if (value instanceof Date) {
            this.value = value;
        } else if (value) {
            this.value = new Date(value);
        } else {
            this.value = null;
        }
    }
}
