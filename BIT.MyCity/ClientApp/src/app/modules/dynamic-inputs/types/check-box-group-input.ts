import { DataService } from '../../../services/data.service';
import { InjectorInstance } from '../../../helpers/app.static';
import { InputBase, ISelectOption } from './input-base';
import { E_FIELD_TYPE, IFieldDescriptor, IFieldEditOptionsArray } from 'src/app/model/datatypes/descriptors.interfaces';
import { FT_WEIGHT } from 'src/app/model/datatypes/fields.const';


export class CheckBoxGroupInput extends InputBase<any[]> {
    controlType = E_FIELD_TYPE.checkBoxGroup;
    disabled: boolean;

    constructor(descr: IFieldDescriptor = null, keyPrefix: string, value: any[]) {
        super(descr, keyPrefix, value);
    }

    public init(): Promise<any> {
        const descr = this.descriptor;
        const value = this.value;

        let updateOptions = Promise.resolve(null);

        if (descr.editOptions && descr.editOptions.arraySource) {
            const dict: IFieldEditOptionsArray = descr.editOptions.arraySource;
            updateOptions = updateOptions.then ( () => {
                const srv = InjectorInstance.get(DataService);

                return srv.getPgList(dict.arrayList,
                    dict.orderBy || [{ field: FT_WEIGHT.key }],
                    dict.arrayFilter || null,
                    null, null, false
                    ).then ((pglist) => {

                    const data = (pglist.total !== undefined && pglist.items) ? pglist.items : pglist;

                    if (!this.options) {
                        this.options = [];
                    }
                    for (let i = 0; i < data.length; i++) {
                        const el = data[i];
                        const title = (typeof dict.arrayTitle) === 'string' ? el[String(dict.arrayTitle)] :
                            (<Function>dict.arrayTitle)(el);
                        const option = <ISelectOption>{
                            value: el[dict.arrayKey],
                            title: title,
                        };

                        if (value) {
                            option.checked = this.value.find (v => v === el[dict.arrayKey]);
                        }
                        this.options.push(option);
                    }
                    return this.options;
                });
            });
        }

        return Promise.resolve(updateOptions);
    }
}
