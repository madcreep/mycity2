import { InputBase } from './input-base';
import { E_FIELD_TYPE, IFieldDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';


export class FileListInput extends InputBase<any[]> {
    controlType = E_FIELD_TYPE.filesPond;
    disabled: boolean;

    constructor(descr: IFieldDescriptor = null, keyPrefix: string, value: any[]) {
        super(descr, keyPrefix, value);
    }

    public init(): Promise<any> {

        const updateOptions = Promise.resolve(null);

        return Promise.resolve(updateOptions);
    }
}
