import { InputBase } from './input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';
import { ValidatorsControl, VALIDATOR_TYPE } from '../../../helpers/validators/validators-control';
import { NUMERIC_PATTERN } from 'src/app/consts/common.consts';

export class NumberInput extends InputBase<number> {
    controlType = E_FIELD_TYPE.number;

    constructor(descr = null, keyPrefix: string, value: number) {
        super(descr, keyPrefix, value);
        if (!this.validators) {
            this.validators = [];
            this.validators.push(
                ValidatorsControl.existValidator(VALIDATOR_TYPE.PATTERN, 'Неверное значение', NUMERIC_PATTERN)
            );
        }
    }
}
