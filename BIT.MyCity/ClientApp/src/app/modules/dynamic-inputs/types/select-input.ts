import { InputBase, ISelectOption } from './input-base';
import { E_FIELD_TYPE, IFieldDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';
import { AppUtils, InjectorInstance } from 'src/app/helpers/app.static';
import { DataService } from 'src/app/services/data.service';
import { FT_WEIGHT } from 'src/app/model/datatypes/fields.const';

export class SelectInput extends InputBase<string | number> {
    controlType = E_FIELD_TYPE.select;
    options: ISelectOption[] = [];
    extSelectOptions: any[];

    constructor(descr: IFieldDescriptor = null, keyPrefix: string, value: string | number, extSelectOptions: any[]) {
        super(descr, keyPrefix, value);
        this.options = AppUtils.deepCopy(descr.options);
        this.extSelectOptions = extSelectOptions;
        this.value = value;
    }

    public init(): Promise<any> {
        const descr = this.descriptor;
        const value = this.value;

        let updateOptions = super.init();
        if (this.optionsFn) {
            this.options = [];
            updateOptions = updateOptions.then ( () => {
                return this.optionsFn().then(
                    (list) => {
                        this.options = list;
                        return this.options;
                    }
                );
            });
        }
        // TODO: [MYC-516] Убрать descr.editOptions это как вариант optionsFn
        if (descr.editOptions && descr.editOptions.arraySource) {
            const dict = descr.editOptions.arraySource;
            updateOptions = updateOptions.then ( () => {
                const srv = InjectorInstance.get(DataService);
                return srv.getPgList(
                    dict.arrayList,
                    dict.orderBy || [{ field: FT_WEIGHT.key }],
                    dict.arrayFilter || null,
                    null,
                    false,
                    ).then ((pglist) => {
                    if (!this.options) {
                        this.options = [];
                    }
                    if (this.extSelectOptions) {
                        this.options = this.options.concat(this.extSelectOptions);
                    }
                    const data = (pglist.total !== undefined && pglist.items) ? pglist.items : pglist;

                    for (let i = 0; i < data.length; i++) {
                        const el = data[i];
                        const title = (typeof dict.arrayTitle) === 'string' ? el[String(dict.arrayTitle)] :
                            (<Function>dict.arrayTitle)(el);
                        const option = <ISelectOption>{
                            value: el[dict.arrayKey],
                            title: title,
                        };
                        if (el[dict.arrayKey] === value) {
                            option.checked = true;
                        }

                        this.options.push(option);
                    }
                    return this.options;
                });
            });
        }

        return Promise.resolve(updateOptions);

    }


}
