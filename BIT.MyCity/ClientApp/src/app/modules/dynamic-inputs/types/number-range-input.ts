import {InputBase, ISelectOption} from './input-base';
import {E_FIELD_TYPE, IFieldDescriptor, IFieldEditOptionsArray} from '../../../model/datatypes/descriptors.interfaces';
import {InjectorInstance} from '../../../helpers/app.static';
import {DataService} from '../../../services/data.service';
import {FT_WEIGHT} from '../../../model/datatypes/fields.const';

export class NumberRangeInput extends InputBase<number[]> {
    controlType = E_FIELD_TYPE.numberRange;
    disabled: boolean;

    constructor(descr: IFieldDescriptor = null, keyPrefix: string, value: number[]) {
        super(descr, keyPrefix, value);
    }
}
