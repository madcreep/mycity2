import { InputBase } from './input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';


export class CheckboxInput extends InputBase<boolean> {
    controlType = E_FIELD_TYPE.boolean;

    constructor(descr = null, keyPrefix: string, value: boolean) {
        super(descr, keyPrefix, value);
        if (typeof value === 'string') {
            if ((value as String).toLowerCase() === 'false' || value === '0') {
                this.value = false;
            } else {
                this.value = !!value;
            }
        } else {
            this.value = !!value;
        }

    }
}
