import { InputBase, ISelectOption } from './input-base';
import { E_FIELD_TYPE, IFieldDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';


export interface TreeListInputOption {
    title: string;
    value: string;
    id: string;
    due: string;
    weight?: number;
    children?: TreeListInputOption[];
}

export class TreeListInput extends InputBase<any[]> {
    controlType = E_FIELD_TYPE.treeList;
    constructor(descriptor = null, keyPrefix: string, value: any[]) {
        super(descriptor, keyPrefix, value);
    }
}
