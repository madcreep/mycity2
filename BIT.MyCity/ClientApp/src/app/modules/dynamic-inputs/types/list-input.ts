import { InputBase, ISelectOption } from './input-base';
import { E_FIELD_TYPE, IFieldDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';
import { AppUtils, InjectorInstance } from 'src/app/helpers/app.static';
import { DataService } from 'src/app/services/data.service';
import { FT_WEIGHT } from 'src/app/model/datatypes/fields.const';

export class ListInput extends InputBase<string | string[]> {
    controlType = E_FIELD_TYPE.list;
    options: ISelectOption[] = [];
    extSelectOptions: any[];

    constructor(descr: IFieldDescriptor = null, keyPrefix: string, value: string | string[], extSelectOptions: any[]) {
        super(descr, keyPrefix, value);
        this.options = AppUtils.deepCopy(descr.options);
        this.extSelectOptions = extSelectOptions;
        this.value = value;
        this.mode = descr.mode;
    }
}
