import { InputBase } from './input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';

export class StringInput extends InputBase<string> {
    controlType = E_FIELD_TYPE.string;

    constructor(descr = null, keyPrefix: string, value: string) {
        super(descr, keyPrefix, value);
    }
}
