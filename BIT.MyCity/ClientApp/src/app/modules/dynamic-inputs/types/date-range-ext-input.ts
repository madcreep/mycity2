import { InputBase } from './input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';

export class DateRangeExtInput extends InputBase<Date[]> {
    controlType = E_FIELD_TYPE.dateRangeExt;

    constructor(descr = null, keyPrefix: string, value: Date[]) {
        super(descr, keyPrefix, null);
        this.value = [null, null];
        if (value && value[0]) {
            if (value[0] instanceof Date) {
                this.value[0] = value[0];
            } else {
                this.value[0] = new Date(value[0]);
            }
        }

        if (value && value[1]) {
            if (value && value[1] && value[1] instanceof Date) {
                this.value[1] = value[1];
            } else {
                this.value[1] = new Date(value[1]);
            }
        }
    }
}
