import {InputBase} from './input-base';
import {E_FIELD_TYPE, IFieldDescriptor} from 'src/app/model/datatypes/descriptors.interfaces';


export class RadioGroupInput extends InputBase<boolean> {
    controlType = E_FIELD_TYPE.radioGroup;
    disabled: boolean;

    constructor(descr: IFieldDescriptor = null, keyPrefix: string, value: any) {
        super(descr, keyPrefix, value);
    }
}
