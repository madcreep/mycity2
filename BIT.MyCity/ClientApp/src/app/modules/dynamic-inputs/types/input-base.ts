import { ValidatorFn } from '@angular/forms';
import { E_FIELD_TYPE, IFieldDescriptor, IFieldEditOptions } from '../../../model/datatypes/descriptors.interfaces';
import { InjectorInstance } from 'src/app/helpers/app.static';
import { SettingsService } from 'src/app/services/settings.service';
import { Subscriptionable } from 'src/app/common/subscriptionable';
import { DynamicInputBaseComponent } from '../fasade/dynamic-input-base.component';


export interface ISelectOption {
    value: any;
    title: string;
    checked?: boolean;
    disabled?: boolean;
    ext?: any;
}

export class InputBase<T> extends Subscriptionable {
    value: T;
    key: string;
    label: string;
    controlType: E_FIELD_TYPE;
    descriptor: IFieldDescriptor;

    mode?: any;

    // pattern?: RegExp;
    readonly?: boolean;
    // required?: boolean;
    options?: any[];
    disabled?: boolean;
    length?: number;
    password?: boolean;
    minValue?: number;
    maxValue?: number;
    dib?: DynamicInputBaseComponent;

    hidden?: boolean;
    validators?: ValidatorFn[];
    editOptions?: IFieldEditOptions;
    pageIndex?: number;
    manualLayout: boolean;
    noStoreValue: boolean;
    optionsFn: Function; // Promise<any[]>;

    constructor (descr: IFieldDescriptor = null, keyPrefix: string, value: T) {
        super();
        if (descr) {
            this.value = value;
            this.key = keyPrefix ? keyPrefix + '.' + descr.key : descr.key;
            if (descr.titlePath) {
                this.label = '...'; // init later
            } else {
                this.label = descr.title;
            }
            this.readonly = descr.readonly;
            this.options = descr.options;
            this.disabled = descr.disabled;
            this.length = descr.length;
            this.password = descr.isPassword;
            this.minValue = descr.minValue;
            this.maxValue = descr.maxValue;
            this.manualLayout = descr.manualLayout;
            this.noStoreValue = descr.noStoreValue;
            // this.required = descr.required;
            this.validators = descr.validators;
            this.editOptions = descr.editOptions;
            this.hidden = descr.hidden;
            this.optionsFn = descr.optionsFn;
            this.descriptor = descr;
            this.mode = descr.mode;
            if (descr.onInputCreate) {
                descr.onInputCreate(this);
            }
        }
    }

    public init(): Promise<any> {
        if (this.descriptor.titlePath) {
            const settSrv = InjectorInstance.get(SettingsService);
            this.subscriptions.push(
                settSrv.subscribeToValue(this.descriptor.titlePath,
                    (val) => {
                        this.label = val;
                    }
                )
            );
        }
        return Promise.resolve(null);
    }
}
