import { InputBase } from './input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';

export class DateTimeInput extends InputBase<Date> {
    controlType = E_FIELD_TYPE.datetime;

    constructor(descr = null, keyPrefix: string, value: Date) {
        super(descr, keyPrefix, null);
        if (value instanceof Date) {
            this.value = value;
        } else if (typeof value === 'string') {
            if (value[(<string>value).length-1] === 'Z') {
                this.value = new Date(value);
            } else {
                this.value = new Date(value + 'Z');
            }
        } else if (value) {
            this.value = new Date(value);
        } else {
            this.value = null;
        }
    }
}
