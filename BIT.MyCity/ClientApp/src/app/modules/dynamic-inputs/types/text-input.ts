import { InputBase } from './input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';

export class TextInput extends InputBase<string> {
    controlType = E_FIELD_TYPE.text;
    type: string;
    height: number;

    constructor(descr = null, keyPrefix: string, value: string) {
        super(descr, keyPrefix, value);
        // this.type = options['type'] || '';
        // this.height = options['height'] || null;
    }
}
