import { DataService } from '../../../services/data.service';
import { InjectorInstance } from '../../../helpers/app.static';
import { InputBase, ISelectOption } from './input-base';
import { E_FIELD_TYPE, IFieldDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';
import { FT_WEIGHT } from 'src/app/model/datatypes/fields.const';


export class BaseOptionsInput extends InputBase<any[]> {
    controlType = E_FIELD_TYPE.claimSelect;
    disabled: boolean;

    constructor(descr: IFieldDescriptor = null, keyPrefix: string, value: any[]) {
        super(descr, keyPrefix, value);
    }
    public init(): Promise<any> {
        const descr = this.descriptor;
        const value = this.value;

        let updateOptions = Promise.resolve(null);

        if (descr.editOptions && descr.editOptions.arraySource) {
            const dict = descr.editOptions.arraySource;
            updateOptions = updateOptions.then ( () => {
                const srv = InjectorInstance.get(DataService);
                return srv.getPgList(dict.arrayList,
                    dict.orderBy || [{ field: FT_WEIGHT.key }],
                    dict.arrayFilter || null,
                         null, false
                    ).then ((pglist) => {
                        const data = (pglist.total !== undefined && pglist.items) ? pglist.items : pglist;
                        this.options = data;
                });
            });
        }

        return Promise.resolve(updateOptions);
    }
}
