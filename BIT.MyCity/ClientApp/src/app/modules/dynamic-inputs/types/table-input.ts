import { InputBase } from './input-base';
import { E_FIELD_TYPE } from 'src/app/model/datatypes/descriptors.interfaces';

export class TableInput extends InputBase<any[]> {
    controlType = E_FIELD_TYPE.table;
    // type: string;

    constructor(descr = null, keyPrefix: string, value: any[]) {
        super(descr, keyPrefix, value);
    }
}
