import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DateRangePickerModule, DateTimePickerModule, DatePickerModule} from '@syncfusion/ej2-angular-calendars';
import {MultiSelectModule, DropDownListModule, ListBoxModule} from '@syncfusion/ej2-angular-dropdowns';
import {NumericTextBoxModule} from '@syncfusion/ej2-angular-inputs';
import {DynamicInputStringComponent} from './components/string/dynamic-input-string.component';
import {DynamicInputTextComponent} from './components/text/dynamic-input-text.component';
import {DynamicInputCheckboxComponent} from './components/checkbox/dynamic-input-checkbox.component';
import {DynamicInputNumberComponent} from './components/number/dynamic-input-number.component';
import {DynamicCheckboxGroupComponent} from './components/checkbox-group/dynamic-checkbox-group.component';
import {DynamicInputDateComponent} from './components/date/dynamic-input-date.component';
import {DynamicInputComponent} from './fasade/dynamic-input.component';
import {DynamicInputModeratorsSubSComponent} from './components/moderatorsSubS/dynamic-input-moderators-subs.component';
import {DynamicInputSeparatorComponent} from './components/separator/dynamic-input-separator.component';
import {DynamicInputRichTextComponent} from './components/richtext/dynamic-input-richtext.component';
import {DynamicInputMultiselectComponent} from './components/multiselect/dynamic-input-multiselect.component';
import {DynamicInputSelectComponent} from './components/select/dynamic-input-select.component';
import {DynamicInputDateTimeComponent} from './components/datetime/dynamic-input-datetime.component';
import {DynamicInputDateRangeComponent} from './components/date-range/dynamic-input-date-range.component';
import {DynamicInputClaimSelectComponent} from './components/claimselect/dynamic-input-claimselect.component';
import {DynamicInputTableComponent} from './components/table/dynamic-input-table.component';
import {DynamicInputFilelistComponent} from './components/filelist/dynamic-input-filelist.component';
import {DynamicInputMapComponent} from './components/map/dynamic-input-map.component';
import {CardControlsModule} from '../card-controls/card-controls.module';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {AgGridModule} from 'ag-grid-angular';
import {FilePondModule} from 'ngx-filepond';
import {SimpleAttachesListComponent} from './components/simple-attaches-list/simple-attaches-list.component';
import {AccordionModule} from 'ngx-bootstrap/accordion';
import {DynamicRadioGroupComponent} from './components/radio-group/dynamic-radio-group.component';
import { DynamicInputNumberRangeComponent } from './components/number-range/dynamic-input-number-range.component';
import {DynamicInputListComponent} from './components/list/dynamic-input-list.component';
import {TreeViewModule} from '@syncfusion/ej2-angular-navigations';
import { DynamicInputTreeComponent } from './components/tree/dynamic-input-tree.component';
import { DataRangeExtendedComponent } from './components/data-range-extended/data-range-extended.component';
import {InputClearButtonComponent} from './components/input-clear-button/input-clear-button.component';

@NgModule({
    declarations: [
        DynamicInputComponent,
        DynamicInputStringComponent,
        DynamicInputTextComponent,
        DynamicInputCheckboxComponent,
        DynamicInputNumberComponent,
        DynamicCheckboxGroupComponent,
        DynamicInputDateComponent,
        DynamicInputModeratorsSubSComponent,
        DynamicInputSeparatorComponent,
        DynamicInputRichTextComponent,
        DynamicInputMultiselectComponent,
        DynamicInputSelectComponent,
        DynamicInputDateTimeComponent,
        DynamicInputDateRangeComponent,
        DynamicInputClaimSelectComponent,
        DynamicInputTableComponent,
        DynamicInputFilelistComponent,
        DynamicInputMapComponent,
        DynamicRadioGroupComponent,
        SimpleAttachesListComponent,
        DynamicInputNumberRangeComponent,
        DynamicInputListComponent,
        DynamicInputTreeComponent,
        DataRangeExtendedComponent,
        InputClearButtonComponent,
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        BrowserModule,
        NumericTextBoxModule,
        BrowserAnimationsModule,
        MultiSelectModule,
        DatePickerModule,
        DateRangePickerModule,
        DateTimePickerModule,
        CardControlsModule,
        AccordionModule.forRoot(),
        MultiSelectModule,
        DropDownListModule,
        AgGridModule.withComponents([]),
        CKEditorModule,
        FilePondModule,
        ListBoxModule,
        TreeViewModule,
    ],
    exports: [
        DynamicInputComponent,
        SimpleAttachesListComponent,
        DynamicInputFilelistComponent,
        // DynamicInputStringComponent,
        // DynamicInputTextComponent,
        // DynamicInputCheckboxComponent,
        // DynamicInputNumberComponent,
        // DynamicInputArrayComponent,
        // DynamicInputDateComponent,
    ]
})
export class DynamicInputsModule {
}
