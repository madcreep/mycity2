export interface IDynamicInputOptions {
    hideLabel?: boolean; //
    hideCounter?: boolean; //
    hidePicker?: boolean; // Date Picker
    inputClass?: string;
    formGroupClass?: string;
    additionalData?: any;
    isClearButtonVisible?: boolean;
}


// export class IDynamicInputOptions {
//     hideLabel?: boolean; // default: false;
//
//     selEmptyEn?: boolean; // default: false;
//     selectionEditable?: boolean; // default: false; Allow edit text value for select-2
//     // dropdownContainer?: string ; // контейнер для dropdown select. 'body' или не указано для локального положения
//     defaultValue?: { value: string, title: string , disabled?: boolean };
//     enRemoveButton?: boolean; // for dictlink second button
//     events?: IDynamicInputEvents;
//     // selEmptyValDis?: boolean; // default: false;
// }
