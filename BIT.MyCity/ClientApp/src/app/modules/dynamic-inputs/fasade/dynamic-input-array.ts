import {Component, Directive} from '@angular/core';
import { DynamicInputBaseComponent } from './dynamic-input-base.component';

@Component({ template: ''})
export class DynamicInputArray extends DynamicInputBaseComponent {
    removeValue (remove_value) {
        const value: any[] = this.control.value;
        const exist = value.find( v => v === remove_value);

        if (exist) {
            this.control.setValue (value.filter ( v => v !== remove_value));
            this.delayedTooltip();
        }
    }

}
