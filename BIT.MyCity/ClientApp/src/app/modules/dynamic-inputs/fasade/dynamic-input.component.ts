import { E_FIELD_TYPE } from '../../../model/datatypes/descriptors.interfaces';
import { Component, OnInit, Input } from '@angular/core';
import { DynamicInputBaseComponent } from './dynamic-input-base.component';
import { AppUtils } from 'src/app/helpers/app.static';
import { InputBase } from '../types/input-base';

@Component({
    selector: 'app-dynamic-input',
    templateUrl: './dynamic-input.component.html',
    styleUrls: ['./dynamic-input.component.scss']
})
export class DynamicInputComponent extends DynamicInputBaseComponent implements OnInit {

    @Input() getInputOptsFn: (input: InputBase<any>) => any;
    types = E_FIELD_TYPE;

    ngOnInit() {
        super.ngOnInit();
        if ((!this.viewOpts || AppUtils.isObjectEmpty(this.viewOpts)) && this.getInputOptsFn) {
            this.viewOpts = this.getInputOptsFn(this.input);
        }
    }

    getFormClass () {
        if (this.viewOpts && this.viewOpts.formGroupClass) {
            return this.viewOpts.formGroupClass;
        }

        switch (this.input.controlType) {
            case E_FIELD_TYPE.boolean:
                return 'no-marigin';
        }

        return '';
    }
}
