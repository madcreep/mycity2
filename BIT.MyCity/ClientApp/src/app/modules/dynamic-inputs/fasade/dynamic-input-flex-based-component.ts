import {Component, OnInit} from '@angular/core';
import {DynamicInputBaseComponent} from './dynamic-input-base.component';


@Component({ template: '' })
export abstract class DynamicInputFlexBasedComponent extends DynamicInputBaseComponent implements OnInit {
    cellCssClass = 'col-12';
    columnsCssClass = 'col-12';

    ngOnInit() {
        super.ngOnInit();
        this.calculatePositionClasses(this.input.mode?.countElementsInRow ?? 1,  this.input.mode?.width ?? 12);
    }

    private calculatePositionClasses(countInRow: number, width: number) {
        if (countInRow <= 0 || countInRow > 12) {
            countInRow = 1;
        }
        if (width <= 0 || width > 12) {
            width = 12;
        }
        this.cellCssClass = 'p-0 col-' + Math.round(12 / countInRow);
        this.columnsCssClass = 'p-0 col-' + Math.round(width * countInRow);
    }
}
