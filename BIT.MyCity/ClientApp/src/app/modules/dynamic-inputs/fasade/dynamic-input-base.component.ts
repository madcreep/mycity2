import {Input, OnChanges, OnDestroy, Output, EventEmitter, SimpleChanges, OnInit, Directive, Component} from '@angular/core';
import { InputBase } from '../types/input-base';
import { FormGroup, AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IDynamicInputOptions } from './dynamic-input.interfaces';
import { Subscriptionable } from 'src/app/common/subscriptionable';



export class ErrorTooltip {
    visible = false;
    message = '';
    placement = 'bottom';
    class = 'tooltip-error';
    container = '';
    force = false;
}

// @Directive()
@Component({ template: '' })
export abstract class DynamicInputBaseComponent extends Subscriptionable implements OnChanges, OnDestroy, OnInit {
    @Input() input: InputBase<any>;
    @Input() form: FormGroup;
    @Input() readonly: boolean;
    @Input() disabled: boolean;
    @Input() placeholder = '...';
    @Input() getAdditionalDataFn: (input: InputBase<any>) => void;

    @Input() viewOpts: IDynamicInputOptions = {};
    @Output() controlBlur: EventEmitter<any> = new EventEmitter<any>();
    @Output() controlFocus: EventEmitter<any> = new EventEmitter<any>();
    @Output() valueChanged: EventEmitter<any> = new EventEmitter<any>();

    public isFocused: boolean;
    public inputTooltip: ErrorTooltip = new ErrorTooltip();
    protected _additionalData: any;

    private _syncTimer: NodeJS.Timer;

    get currentValue(): any {
        const control = this.control;
        if (control) {
            return control.value;
        } else {
            return this.input.label;
        }
    }

    get isRequired(): boolean {
        let required = false;
        const control = this.control;
        if (control && control.errors) {
            required = !!this.control.errors['required'];
        }
        return required;
    }

    onClearButtonClick($event: any) {
        this.control.setValue(null);
    }

    hasValue(): boolean {
        const ctrl = this.control;
        return (ctrl && ctrl.value !== null && ctrl.value !== undefined);
    }

    onFocus() {
        this.isFocused = true;
        this.toggleTooltip();
        this.controlFocus.emit(this);
    }

    onBlur() {
        this.isFocused = false;
        this.updateMessage();
        this.toggleTooltip();
        this.controlBlur.emit(this);
    }

    onInputChanged() {
        this.valueChanged.emit(this);
    }

    onInput(event) {
        event.stopPropagation();
        this.delayedTooltip();
    }

    ngOnInit(): void {
        if (!this.input || !this.input.descriptor) {

        }
        if (this.input.descriptor  && this.input.descriptor.onInitControl) {
            this.input.descriptor.onInitControl(this);
        }
    }

    focus() {

    }

    public isLabelVisible(): boolean {
        return this.viewOpts ? !this.viewOpts.hideLabel  : true;
    }

    delayedTooltip(t = 1): void {
        this.updateMessage();
        if (this.inputTooltip.message !== '' && !this._syncTimer) {
            this.inputTooltip.visible = false;
            this._syncTimer = setTimeout(() => {
                this.inputTooltip.force = true;
                this.inputTooltip.visible = true;
                this._syncTimer = null;
            }, t);
        }
    }

    forceTooltip() {
        this.inputTooltip.force = true;
    }

    ngOnChanges(changes: SimpleChanges) {
        if (!this.input) {
            return;
        }
        if (this.getAdditionalDataFn) {
            this._additionalData = this.getAdditionalDataFn(this.input);
        }
        const control = this.control;
        this.input.dib = this;
        if (control) {
            setTimeout(() => {
                this.toggleTooltip();
            });

            this.ngOnDestroy();
            this.subscriptions.push(control.statusChanges.subscribe(() => {
                if (this.inputTooltip.force) {
                    this.updateMessage();
                    this.inputTooltip.visible = true;
                    this.inputTooltip.force = false;
                } else {
                    this.inputTooltip.visible = (this.inputTooltip.visible && control.invalid && control.dirty);
                }
            }));
        }
    }

    getCounterLabel(): string {
        if (this.input.length) {
            const control = this.control;
            if (control) {
                const len = this.control.value ? String(this.control.value).length : 0;
                const maxlen = this.input.length;
                return String(len) + '/' + String (maxlen);
            }
        }
        return '';
    }

    get control(): AbstractControl {
        return this.form.controls[this.input.key];
    }

    public getErrorMessage(): string {
        let msg = '';
        const control = this.control;
        if (control && control.errors) {
            // msg = getControlErrorMessage(control, {uniqueInDict: !!this.input.uniqueInDict, maxLength: this.input.length });

            msg = control.errors.valueError ||  'Неверное значение';
        }

        return msg;
    }


    protected updateMessage() {
        this.inputTooltip.message = this.getErrorMessage();
    }

    protected toggleTooltip() {
        if (!this.readonly) {
            const control = this.control;
            if (control) {
                this.updateMessage();
                this.inputTooltip.visible = /*!focused &&*/ control.invalid /*&& control.dirty*/;
                this.inputTooltip.force = true;
            } else {
                this.inputTooltip.visible = false;
            }
        } else {
            this.inputTooltip.visible = false;
        }
    }
}
