import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { AppUtils } from 'src/app/helpers/app.static';
import { IAngularMyDpOptions } from 'angular-mydatepicker';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';

@Component({
    selector: 'app-dynamic-input-date',
    templateUrl: 'dynamic-input-date.component.html'
})
export class DynamicInputDateComponent extends DynamicInputBaseComponent implements OnInit {
    placement = 'bottom';

    bsDate: Date;

    hidePicker = false;

    myDpOptions: IAngularMyDpOptions = {
        dateFormat: 'dd.mm.yyyy'
    };

    get currentValue(): string {
        const control = this.control;
        if (control) {
            if (control.value && control.value instanceof Date && !isNaN(control.value.getDay())) {
                return AppUtils.dateToStringValue(control.value);
            }
        }
        return '...';
    }

    constructor(
        // private localeService: BsLocaleService,
    ) {
        super();
        // this.bsConfig = {
        //     // locale: 'ru',
        //     showWeekNumbers: false,
        //     containerClass: 'theme-dark-blue',
        //     dateInputFormat: 'DD.MM.YYYY',
        //     isDisabled: this.readonly,
        //     minDate: new Date('01/01/1900'),
        //     maxDate: new Date('12/31/2100'),
        // };
    }

    dpChanged(value: Date) {
        this.form.controls[this.input.key].setValue(value);
        this.onBlur();
    }

    ngOnInit() {
        this.hidePicker = (this.viewOpts ? this.viewOpts.hidePicker : false);
        // this.localeService.use('ru');
        // this.updateDatePickerPlacement();
    }

    // private updateDatePickerPlacement() {
    //     if (this.datePickerWrapper) {
    //         const rect = this.datePickerWrapper.nativeElement.getBoundingClientRect();
    //         if (window.innerHeight - rect.bottom >= 308) {
    //             this.placement = 'bottom';
    //         } else if (rect.top >= 308) {
    //             this.placement = 'top';
    //         } else if (rect.left + rect.width - 24 >= 318) {
    //             this.placement = 'left';
    //         } else {
    //             this.placement = 'right';
    //         }
    //     }
    // }
}
