import { Component } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';

@Component({
    selector: 'app-dynamic-input-richtext',
    templateUrl: 'dynamic-input-richtext.component.html'
})
export class DynamicInputRichTextComponent extends DynamicInputBaseComponent {
    public Editor = ClassicEditor;
    get label(): string {
        return this.input.label;
    }
}
