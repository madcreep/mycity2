import {Component, OnInit, ViewChild} from '@angular/core';
import {DynamicInputBaseComponent} from '../../fasade/dynamic-input-base.component';
import {FieldsSettingsModel} from '@syncfusion/ej2-navigations/src/treeview/treeview-model';
import {DrawNodeEventArgs, NodeCheckEventArgs, TreeViewComponent} from '@syncfusion/ej2-angular-navigations';
import {ReportSettingsSelectMode, ReportSettingsSelectOnlyMode} from '../../../../helpers/converters/report-settings-options';

@Component({
  selector: 'app-dynamic-input-tree',
  templateUrl: './dynamic-input-tree.component.html',
  styleUrls: ['./dynamic-input-tree.component.scss']
})
export class DynamicInputTreeComponent extends DynamicInputBaseComponent implements OnInit {
    @ViewChild('tree')
    public tree?: TreeViewComponent;

    isUpdated = false;
    public hierarchicalData: { [key: string]: Object }[];
    public field: FieldsSettingsModel;

    ngOnInit() {
        this.hierarchicalData = this.input.options as any as { [key: string]: Object }[];
        this.field = { dataSource: this.hierarchicalData, id: 'id', text: 'title', child: 'children'};
        this.isUpdated = true;
        super.ngOnInit();
    }
    onNodeChecked(args: NodeCheckEventArgs) {
        if (!args.isInteracted) {
            return;
        }
        if ((this.input.mode.selectMode || ReportSettingsSelectOnlyMode.multi) === ReportSettingsSelectOnlyMode.single) {
            this.tree.checkedNodes = [args.data[0].id as string] ?? [];
            this.form.controls[this.input.key].setValue([args.data[0].id as string]);
        } else {

        }

        this.form.controls[this.input.key].setValue(this.tree?.checkedNodes ?? []);
        // console.log(this.form.value[this.input.key]);
    }
    onDrawNode(args: DrawNodeEventArgs) {
        if (!this.input.mode) { return; }
        if ((this.input.mode.selectOnlyMode || ReportSettingsSelectMode.all) === ReportSettingsSelectMode.all) { return; }

        let isCheckboxHidden = false;
        if ((this.input.mode.selectOnlyMode === ReportSettingsSelectMode.nodes && !args.nodeData.isNode)
            || (this.input.mode.selectOnlyMode === ReportSettingsSelectMode.leaves && args.nodeData.isNode)) {
            isCheckboxHidden = true;
        }

        if (isCheckboxHidden) {
            const element: HTMLElement = args.node.querySelector('.e-checkbox-wrapper') as HTMLElement;
            element.classList.add('e-checkbox-disabled');
            element.classList.add('d-none');
        }
    }
}
