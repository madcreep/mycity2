import {Component, OnInit} from '@angular/core';
import {DynamicInputBaseComponent} from '../../fasade/dynamic-input-base.component';

@Component({
    selector: 'app-dynamic-input-number-range',
    templateUrl: './dynamic-input-number-range.component.html',
    styleUrls: ['./dynamic-input-number-range.component.scss']
})
export class DynamicInputNumberRangeComponent extends DynamicInputBaseComponent implements OnInit {

    get label(): string {
        return this.input.label;
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    getFormat() {
        return 'n' + this.input.length;
    }
    getValue(): number[] {
        return this.control.value ?? [null, null];
    }
    getValuePart(index: number) {
        const value = this.getValue();
        return value[index];
    }
    onChange(index, item) {
        const value = this.getValue();
        const decimalsK = Math.pow(10, this.input.length);
        const newValue = Math.round(item.value * decimalsK) / decimalsK;
        value[index] = newValue;
        this.control.setValue (value);
        this.delayedTooltip();
    }
}
