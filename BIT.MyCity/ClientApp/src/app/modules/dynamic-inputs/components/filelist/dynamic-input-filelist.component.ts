import { Component, ViewChild, Output, EventEmitter, OnInit, SimpleChanges, OnChanges, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FileIoService } from 'src/app/services/file-io.service';
import { DynamicInputArray } from '../../fasade/dynamic-input-array';
import { AuthService } from 'src/app/services/auth.service';
import { EnvSettings } from '../../../../model/settings/envsettings';
import { IDynamicInputOptions } from '../../fasade/dynamic-input.interfaces';

@Component({
    selector: 'app-dynamic-input-filelist',
    templateUrl: 'dynamic-input-filelist.component.html'
})
export class DynamicInputFilelistComponent extends DynamicInputArray implements OnInit, OnChanges {

    @Input() viewOpts: IDynamicInputOptions = {};
    @ViewChild('myPond', { static: true }) myPond: any;
    @Output() oninit: EventEmitter<any> = new EventEmitter<any>();
    @Output() onaddfile: EventEmitter<any> = new EventEmitter<any>();

    files = [];

    attachesFiles = [];

    pondOptions = {
        class: 'my-filepond',
        multiple: true,
        'data-max-file-size': '3MB',
        'data-max-files': 3,
        labelIdle: 'Перетащите файлы сюда',
        labelFileProcessingComplete: '',
        labelTapToUndo: '',
        acceptedFileTypes: 'image/jpeg, image/png',

        allowDrop: true,
        allowReplace: true,
        // allowRevert: false,
        instantUpload: true,
        server: {
            url: '/api/file',
            process: (fieldName, file, metadata, load, error, progress, abort) => {

                const formData = new FormData();
                formData.append('uploadedFile', file, file.name);

                const request = new XMLHttpRequest();

                request.open('POST', EnvSettings.ATTACHES_ROOT); // '/api/file');
                request.setRequestHeader('Authorization', 'Bearer ' + this._authSrv.getToken());

                request.upload.onprogress = (e) => {
                    progress(e.lengthComputable, e.loaded, e.total);
                };

                const control = this.control;
                // Should call the load method when done and pass the returned server file id
                // this server file id is then used later on when reverting or restoring a file
                // so your server knows which file to return without exposing that info to the client
                request.onload = function () {
                    if (request.status >= 200 && request.status < 300) {
                        let id = -1;
                        if (request.responseText.startsWith('id=')) {
                            id = Number(request.responseText.substr(3));
                        }

                        load(id);
                        if (!control.value || !(control.value instanceof Array)) {
                            control.setValue([id]);
                        } else {
                            control.setValue([...control.value, id]);
                        }

                    } else {
                        error('oh no');
                    }
                }.bind(this);

                request.send(formData);

                return {
                    abort: () => {
                        // This function is entered if the user has tapped the cancel button
                        request.abort();

                        // Let FilePond know the request has been cancelled
                        abort();
                    }
                };
            },
            // revert: null,
            revert: (uniqueFileId, load, error) => {
                // this
                this.removeValue(uniqueFileId);
                return true;
                // console.log("DynamicInputFilelistComponent -> this", this)
                // return false;
                // error('nooo');
                // throw 'nooo';
                // console.log('DynamicInputFilelistComponent -> uniqueFileId', uniqueFileId)
                //     // Should remove the earlier created temp file here

                //     // Can call the error method if something is wrong, should exit after
                //     // MessageService.sendingFile = true;
                //     console.log(uniqueFileId);
                //     const request = new XMLHttpRequest();
                //     request.open('DELETE', '/appointment/removeFile/' + uniqueFileId);
                //     const files = this.files;
                //     request.onload = function () {
                //         if (request.status >= 200 && request.status < 300) {
                //             for (let i = 0; i < files.length; i++) {
                //                 if (files[i] === uniqueFileId) {
                //                     files.splice(i, 1);
                //                     break;
                //                 }
                //             }
                //             load();
                //         } else {
                //             // Can call the error method if something is wrong, should exit after
                //             error('oh no');
                //         }
                //         // MessageService.sendingFile = false;
                //     };
                //     request.send();
            },
            restore: (p1) => {
            },
            load: (p1) => {
            },
        }
    };

    // get attachesFiles (): IAttachedFile[] {
    //     return this.files.map(f => {

    //     });
    // }

    constructor(
        private _authSrv: AuthService,
        private http: HttpClient,
        private _fio: FileIoService,
    ) {
        super();
    }

    ngOnChanges(changes: SimpleChanges) {
        super.ngOnChanges(changes);

        this.rebuildFilelist();

    }

    rebuildFilelist() {
        this.files = [];

        if (!this._additionalData || !this.control.value) {
            return;
        }
        this.control.value.forEach(id => {
            const file = this._additionalData.find(f => f.id === id);
            if (file) {
                this.attachesFiles.push(file);
                this.files.push(
                    {
                        source: file.id,
                        options: {
                            type: 'limbo',
                            file: {
                                name: file.name,
                                size: file.len,
                                type: file.contentType,
                            },
                        },
                    },
                );
            }
        });

    }

    ngOnInit(): void {
    }

    pondHandleInit() {
    }

    pondHandleAddFile(event: any) {
    }

    pondHandleClickFile($event) {
        const file = $event.file;
        if ($event.file.status === 2) {
            $event.file.abortLoad();
            $event.pond.addFile(file.name, {
                type: 'limbo',
                file: {
                    name: file.file.name,
                    size: file.file.size,
                    type: file.file.type,
                },
            },);
        } else {
            this._fio.downloadFileWithAuth($event.file);
        }

    }
}

