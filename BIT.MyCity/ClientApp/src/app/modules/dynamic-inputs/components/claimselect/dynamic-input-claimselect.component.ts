import { Component, OnInit, OnChanges } from '@angular/core';
import { IClaimInfoType, IClaimType } from 'src/generated/types.graphql-gen';
import { InjectorInstance, AppUtils } from 'src/app/helpers/app.static';
import { SubsystemsService } from 'src/app/services/subsystems.service';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';


const SUBSYSTEM_NONE_TARGET = 'general';
@Component({
    selector: 'app-dynamic-input-claimselect',
    templateUrl: 'dynamic-input-claimselect.component.html',
    styleUrls: ['dynamic-input-claimselect.component.css'],
})

export class DynamicInputClaimSelectComponent extends DynamicInputBaseComponent implements OnChanges, OnInit {

    claimInfos: any[];
    systems: any[] = [];

    ngOnInit(): void {
        this.updateForRoleClaims();
    }

    checkedClaim(claimType: string, claimValue: string) {
        if (this.control && this.control.value && this.control.value.length) {
            return !!this.control.value.find(v => v.type === claimType && v.value === claimValue);
        }
        return false;
    }

    onChange(item, event) {
        const checked = event.currentTarget.checked;
        this.setChecked(item, checked);
    }

    setChecked(item: any, checked: any) {
        if (item.disabled) {
            return;
        }
        const key = item.value;
        const value: any[] = this.control.value;
        const exist = value.find(v => (v.value === key.value && v.type === key.type));
        item.checked = checked;
        if (checked && !exist) {
            value.push(key);
            this.control.setValue(value);
            this.delayedTooltip();
        } else if (!checked && exist) {

            this.control.setValue(value.filter(v => v !== exist));
            this.delayedTooltip();
        }
        this.recalcAccordions();
    }

    updateForRoleClaims(rolesClaims: IClaimType[] = []) {
        this.setValueMinusClaims(rolesClaims);
        this.fixValueTypename();
        this.claimInfos = [];

        for (const ci of this.input.options as IClaimInfoType[]) {
            const isReadonlyEntity = (this.input.editOptions.arrayFilter && !ci.claimEntity.find(ce => ce === this.input.editOptions.arrayFilter));
            const clInfo = Object.assign({}, ci, { targetsys: SUBSYSTEM_NONE_TARGET, claims: [] });

            if (clInfo.values && clInfo.values.length) {
                clInfo.values.forEach(cv => {
                    const isRoleChecked = !!rolesClaims.find(rc => rc.type === clInfo.claimType && rc.value === cv.value);
                    const isChecked = isRoleChecked || this.checkedClaim(clInfo.claimType, cv.value);
                    const isReadonly = isReadonlyEntity || isRoleChecked;
                    if (!isReadonly || isChecked) {
                        clInfo.claims.push(
                            {
                                value: <IClaimType>{ type: clInfo.claimType, value: cv.value },
                                title: cv.description,
                                disabled: isReadonly || this.input.readonly,
                                checked: isChecked,
                            }

                        );
                    }
                });
                const spl = clInfo.values[0].value.split('.');
                if (spl.length > 1) {
                    clInfo.targetsys = spl[0];
                }

            } else {
                const isRoleChecked = !!rolesClaims.find(rc => rc.type === clInfo.claimType && rc.value === '');
                const isReadonly = isReadonlyEntity || isRoleChecked;
                const isChecked = isRoleChecked || this.checkedClaim(clInfo.claimType, '');
                if (!isReadonly || isChecked) {
                    clInfo.claims.push(
                        {
                            value: <IClaimType>{ type: clInfo.claimType, value: '' },
                            title: clInfo.description,
                            disabled: isReadonly,
                            checked: isChecked,
                        }
                    );
                }
            }
            if (clInfo.claims && clInfo.claims.length) {
                this.claimInfos.push(clInfo);
            }
        }

        this.systems = this.claimsToSystems(this.claimInfos);
        this.recalcAccordions();
        if (this.systems.length) {
            this.systems[0].isOpen = true;
        }


    }
    recalcAccordions() {
        for (const block of this.systems) {
            const claims = AppUtils.flattenArray(block.items.map(i => i.claims));
            const checkedCount = claims.filter(c => c.checked).length;
            block['allChecked'] = checkedCount === claims.length;
            block['hasChecked'] = checkedCount > 0;
            block['noChecked'] = checkedCount === 0;
        }
    }

    claimsToSystems(claimInfos: any[]): any[] {
        const subsSrv = InjectorInstance.get(SubsystemsService);
        const result = [];
        const list = new Set<string>(claimInfos.map(c => c.targetsys));
        for (const l of list) {
            const title = subsSrv.titleForSubsistem(l);
            result.push({
                system: l,
                items: claimInfos.filter(c => c.targetsys === l),
                title: title || (l === SUBSYSTEM_NONE_TARGET ? 'Основные' : l),
            });
        }
        return result;
    }

    setValueMinusClaims(rolesClaims: IClaimType[]) {

    }

    fixValueTypename() {
        if (this.hasValue() && this.control.value.length) {
            this.control.value.forEach(v => {
                if (v.__typename) {
                    delete v.__typename;
                }
            });
        }
    }

    public customClass() {
        return 'filter-panel-primary';
    }
    public onOpenChange(block, isOpen) {
        block.isOpen = isOpen;
    }
    public claimToggle(item, toChecked = !item.claims[0].checked) {
        for (const i of item.claims) {
            this.setChecked(i, toChecked);
        }
    }
    public blockToggle(block, toChecked) {
        for (const i of block.items) {
            this.claimToggle(i, toChecked);
        }
    }
}
