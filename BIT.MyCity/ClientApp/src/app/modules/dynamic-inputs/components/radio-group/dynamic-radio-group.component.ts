import {Component, OnInit} from '@angular/core';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';
import {DynamicInputFlexBasedComponent} from '../../fasade/dynamic-input-flex-based-component';

@Component({
    selector: 'app-dynamic-input-radio-group',
    templateUrl: 'dynamic-radio-group.component.html'
})
export class DynamicRadioGroupComponent extends DynamicInputFlexBasedComponent implements OnInit {
    formControlName = '';
    ngOnInit() {
        super.ngOnInit();
        this.formControlName = 'rb_' + this.input.key;
    }

    onChange(item, event) {
        this.control.setValue (item.value);
        this.delayedTooltip();
    }
}
