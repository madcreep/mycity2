import {Component, Input, OnInit} from '@angular/core';
import {DynamicInputBaseComponent} from '../../fasade/dynamic-input-base.component';

@Component({
  selector: 'app-dynamic-input-list',
  templateUrl: './dynamic-input-list.component.html',
  styleUrls: ['./dynamic-input-list.component.scss']
})
export class DynamicInputListComponent extends DynamicInputBaseComponent {
    @Input() mode: string;

    fields =  { text: 'title', value: 'value', iconCss: 'disabled'};

    get selectionSettings() {
        return {
            mode: this.mode === 'Single' ? 'Single' : 'Multiple'
        };
    }

    onChange(item) {
    }
}
