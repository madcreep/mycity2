import {Component, OnInit} from '@angular/core';
import {DynamicInputFlexBasedComponent} from '../../fasade/dynamic-input-flex-based-component';

@Component({
    selector: 'app-dynamic-input-checkbox-group',
    templateUrl: 'dynamic-checkbox-group.component.html'
})
export class DynamicCheckboxGroupComponent extends DynamicInputFlexBasedComponent implements OnInit {

    ngOnInit() {
        super.ngOnInit();
    }
    onChange(item, event) {
        const checked = event.currentTarget.checked;
        const key = item.value;
        const value: any[] = this.control.value;
        const exist = value.find( v => v === key);
        if (checked && !exist) {
            value.push (key);
            this.control.setValue (value);
            this.delayedTooltip();
        } else if (! checked && exist) {
            this.control.setValue (value.filter ( v => v !== key));
            this.delayedTooltip();
        }
    }
}
