import {Component, OnInit} from '@angular/core';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';
import {DynamicInputFlexBasedComponent} from '../../fasade/dynamic-input-flex-based-component';

@Component({
    selector: 'app-dynamic-input-select',
    templateUrl: 'dynamic-input-select.component.html'
})
export class DynamicInputSelectComponent extends DynamicInputFlexBasedComponent implements OnInit {

    fields =  { text: 'title', value: 'value', iconCss: 'disabled'};

    ngOnInit() {
        super.ngOnInit();
    }
    get currentValue(): string {
        let value = '...'; // ..this.input.label;
        const ctrl = this.control;
        if (ctrl) {
            let optValue = this.input.options.find((option) => option.value === ctrl.value);
            if (!optValue && typeof ctrl.value === 'string') {
                optValue = this.input.options.find((option) => option.value.toString() === ctrl.value);
            }
            if (optValue) {
                value = optValue.title;
            }
        }
        return value;
    }

    selectClick(evt: Event) {
        if (this.readonly) {
            evt.stopImmediatePropagation();
            evt.stopPropagation();
            evt.preventDefault();
        }
    }
}
