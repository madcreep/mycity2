import { Component } from '@angular/core';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';

@Component({
    selector: 'app-dynamic-input-number',
    templateUrl: 'dynamic-input-number.component.html'
})
export class DynamicInputNumberComponent extends DynamicInputBaseComponent {

    get label(): string {
        return this.input.label;
    }

    inputCheck(e) {
        if (e.metaKey || e.ctrlKey || e.altKey) {
            return true;
        }
        return !(/^[А-Яа-яA-Za-z ]$/.test(e.key));
    }
}
