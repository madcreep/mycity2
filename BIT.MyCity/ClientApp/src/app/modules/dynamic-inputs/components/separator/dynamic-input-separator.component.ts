import { Component, ViewChild, ElementRef } from '@angular/core';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';

@Component({
    selector: 'app-dynamic-input-separator',
    templateUrl: 'dynamic-input-separator.component.html'
})
export class DynamicInputSeparatorComponent extends DynamicInputBaseComponent {
    @ViewChild('inputEl') inputEl: ElementRef;

    get label(): string {
        return this.input.label;
    }

    focus() {
        this.inputEl.nativeElement.focus();
    }
}
