import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-input-clear-button',
    templateUrl: './input-clear-button.component.html',
    styleUrls: ['./input-clear-button.component.scss']
})
export class InputClearButtonComponent implements OnInit {
    @Output() click = new EventEmitter<any>();

    constructor() {
    }

    ngOnInit(): void {
    }

    onButtonClick(event: MouseEvent) {
        this.click.emit();
        event.stopPropagation();
    }
}
