import { Component, ViewChild, ElementRef } from '@angular/core';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';

@Component({
    selector: 'app-dynamic-input-string',
    templateUrl: 'dynamic-input-string.component.html'
})
export class DynamicInputStringComponent extends DynamicInputBaseComponent {
    @ViewChild('inputEl') inputEl: ElementRef;

    get textType(): string {
        if (this.input.password) {
            return 'password';
        }
        return 'text';
    }

    get label(): string {

        // return this.input.groupLabel && this.isGroup ? this.input.groupLabel :
        return this.input.label;
    }

    focus() {
        this.inputEl.nativeElement. focus();
    }
}
