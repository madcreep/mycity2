import { Component, OnInit, ViewChild } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { FT_NAME } from 'src/app/model/datatypes/fields.const';
import { ICardAction, ICardActionButton } from '../../../../views/cards/card.interfaces';
import { E_ACTION_TYPE, ITableGridOptions } from 'src/app/model/datatypes/descriptors.interfaces';
import { GridApi } from 'ag-grid-community';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';

@Component({
    selector: 'app-dynamic-input-table',
    templateUrl: './dynamic-input-table.component.html',
    styleUrls: ['./dynamic-input-table.component.css']
})

export class DynamicInputTableComponent extends DynamicInputBaseComponent implements OnInit {
    @ViewChild('agGrid') agGrid: AgGridAngular;

    columnDefs: any;
    public defaultColDef: { sortable: boolean, resizable: boolean };
    components: any;
    gridApi: GridApi;
    gridColumnApi: any;
    actionList: any;
    ctrlGridOpts: ITableGridOptions;

    ngOnInit() {
        this.columnDefs = {};
        if (this.input.editOptions && this.input.editOptions.arrayGrid) {
            this.ctrlGridOpts = this.input.editOptions.arrayGrid;
            if (this.ctrlGridOpts.columns) {
                this.columnDefs = this.ctrlGridOpts.columns;
            }

        }
        // const input = this.input.editOptions

        this.components = {
            loadingCellRenderer: function (params) {
                const val = params.getValue();
                if (val !== undefined) {
                    return val;
                } else {
                    // tslint:disable-next-line:max-line-length
                    return '<img *ngIf="dataLoading" src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" alt="loading"/>';
                }
            }
        };

        this.actionList = this.getActionList();
    }

    public hasActions() {
        return this.actionList && this.actionList.length;
    }

    onCellDoubleClicked($event) {
    }

    public getTableStyle(): string {
        const vpx = 164;
        return 'width: 100%; font-size: 14px; height: calc(100vh - 164px); ';
    }

    public onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;

        this.gridApi.sizeColumnsToFit();
        this.agGrid.gridOptions.groupSelectsChildren = true;
        this.agGrid.gridOptions.editType = 'fullRow';
        // this.agGrid.gridOptions.sortingOrder = [this.ctrlGridOpts.orderBy];
        // this.agGrid.gridOptions.enableSorting = true;
        // this.gridColumnApi.getColumn(this.ctrlGridOpts.orderBy).setSort("asc");
        this.gridApi.setSortModel({ colId: this.ctrlGridOpts.orderBy, sort: 'asc' },);
        this.setDataSource();
    }

    public setDataSource() {
        const rowData = this.control.value;
        rowData.forEach(element => {
            if (element.__typename !== undefined) {
                delete element.__typename;
            }
        });
        const orderBy = this.ctrlGridOpts.orderBy;
        this.gridApi.setRowData(rowData.sort((a, b) => a[orderBy] > b[orderBy] ? 1 : -1));
    }

    public getData() {
        const rowData = [];
        this.gridApi.forEachNode(function (node) {
            rowData.push(node.data);
        });
        return rowData;
    }

    public onActionClick(button: ICardActionButton) {
        const data = this.getData();
        switch (button.action.id) {
            case E_ACTION_TYPE.add: {
                const newItems = [this.createNewRowData(data)];
                this.gridApi.updateRowData({ add: newItems });
                this.gridApi.startEditingCell({
                    rowIndex: data.length,
                    colKey: FT_NAME.key,
                });
                break;
            }
            case E_ACTION_TYPE.delete: {
                const selectedData = this.gridApi.getSelectedRows();
                this.gridApi.updateRowData({ remove: selectedData });
                this.onChanged();
                break;
            }
        }
    }

    createNewRowData(data) {
        if (this.ctrlGridOpts.newRow) {
            return this.ctrlGridOpts.newRow(data);
        }
        return {};
    }

    // onChange(item, event) {
    //     const checked = event.currentTarget.checked;
    //     const key = item.value;
    //     const value: any[] = this.control.value;
    //     const exist = value.find(v => v === key);
    //     if (checked && !exist) {
    //         value.push(key);
    //         this.control.setValue(value);
    //         this.delayedTooltip();
    //     } else if (!checked && exist) {
    //         this.control.setValue(value.filter(v => v !== key));
    //         this.delayedTooltip();
    //     }
    // }

    onChanged() {
        this.control.setValue(this.getData());
        this.delayedTooltip();
    }

    onRowDataChanged($event) {
        this.onChanged();
    }

    protected getActionList(): ICardAction[] {
        return [
            <ICardAction>{ id: E_ACTION_TYPE.add, title: 'Добавить пункт' },
            <ICardAction>{ id: E_ACTION_TYPE.delete, title: 'Удалить' },
        ];
    }
}
