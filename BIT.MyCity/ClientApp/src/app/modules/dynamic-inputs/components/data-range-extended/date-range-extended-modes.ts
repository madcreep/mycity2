
export enum DateRangeExtendedModes {
    All = 'All', /// Все
    Range = 'Range', // Диапазон
    SingleDay = 'SingleDay', // Один день
    CurrentDay = 'CurrentDay', // Сегодня
    CurrentDayPlusN = 'CurrentDayPlusN', // Сегодня + N дней
    PlusNAndBefore = 'PlusNAndBefore', // N дней и ранее
    PlusNAndLater = 'PlusNAndLater', // N дней и позднее
    CurrentWeek = 'CurrentWeek', // Текущая неделя
    CurrentMonth = 'CurrentMonth', // Текущий месяц
    CurrentQuarter = 'CurrentQuarter', // Текущий квартал
    CurrentYear = 'CurrentYear', // Текущий год
    PrevWeek = 'PrevWeek', // Предыдущая неделя
    PrevMonth = 'PrevMonth', // Предыдущий месяц
    PrevQuarter = 'PrevQuarter', // Предыдущий квартал
    PrevYear = 'PrevYear', // Предыдущий год
    LastWeek = 'LastWeek', // Последняя неделя
    LastMonth = 'LastMonth', // Последний месяц
    LastQuarter = 'LastQuarter', // Последний квартал
    LastYear = 'LastYear', // Последний год
}
export const DateRangeHelperTitles = [
    { value: DateRangeExtendedModes.All, title: 'Все' },
    { value: DateRangeExtendedModes.Range, title: 'Диапазон' },
    { value: DateRangeExtendedModes.SingleDay, title: 'Один день' },
    { value: DateRangeExtendedModes.CurrentDay, title: 'Сегодня' },
    { value: DateRangeExtendedModes.CurrentDayPlusN, title: 'Сегодня + N дней' },
    { value: DateRangeExtendedModes.PlusNAndBefore, title: 'N дней и ранее' },
    { value: DateRangeExtendedModes.PlusNAndLater, title: 'N дней и позднее' },
    { value: DateRangeExtendedModes.CurrentWeek, title: 'Текущая неделя' },
    { value: DateRangeExtendedModes.CurrentMonth, title: 'Текущий месяц' },
    { value: DateRangeExtendedModes.CurrentQuarter, title: 'Текущий квартал' },
    { value: DateRangeExtendedModes.CurrentYear, title: 'Текущий год' },
    { value: DateRangeExtendedModes.PrevWeek, title: 'Предыдущая неделя' },
    { value: DateRangeExtendedModes.PrevMonth, title: 'Предыдущий месяц' },
    { value: DateRangeExtendedModes.PrevQuarter, title: 'Предыдущий квартал' },
    { value: DateRangeExtendedModes.PrevYear, title: 'Предыдущий год' },
    { value: DateRangeExtendedModes.LastWeek, title: 'Последняя неделя' },
    { value: DateRangeExtendedModes.LastMonth, title: 'Последний месяц' },
    { value: DateRangeExtendedModes.LastQuarter, title: 'Последний квартал' },
    { value: DateRangeExtendedModes.LastYear, title: 'Последний год' },
];

