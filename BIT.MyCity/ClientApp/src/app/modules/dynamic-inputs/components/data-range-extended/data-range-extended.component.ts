import {Component, OnInit} from '@angular/core';
import {DynamicInputBaseComponent} from '../../fasade/dynamic-input-base.component';
import {SelectInput} from '../../types/select-input';
import {E_FIELD_TYPE, IFieldDescriptor} from '../../../../model/datatypes/descriptors.interfaces';
import {DateRangeExtendedModes, DateRangeHelperTitles} from './date-range-extended-modes';
import {FormGroup} from '@angular/forms';
import {InputsService} from '../../../../services/input.service/inputs.service';
import {IDynamicInputOptions} from '../../fasade/dynamic-input.interfaces';
import {DateInput} from '../../types/date-input';
import {NumberInput} from '../../types/number-input';
import {AppUtils} from '../../../../helpers/app.static';

@Component({
    selector: 'app-data-range-extended',
    templateUrl: './data-range-extended.component.html',
    styleUrls: ['./data-range-extended.component.scss']
})
export class DataRangeExtendedComponent extends DynamicInputBaseComponent implements OnInit {

    static Constants = {
        SelectKey: 'selectRangeType',
        DateStartKey: 'dateStart',
        DateEndKey: 'dateEnd',
        IntegerValueKey: 'integerValue'
    };

    currentMode: DateRangeExtendedModes = DateRangeExtendedModes.All;
    DateRangeExtendedModes = DateRangeExtendedModes;

    selectInput: SelectInput;
    dateStartInput: DateInput;
    dateEndInput: DateInput;
    integerValueInput: NumberInput;

    selectInputViewOptions = <IDynamicInputOptions>{
        hideLabel: true
    };
    innerForm: FormGroup;

    isConfigured = false;

    constructor(
        protected inpSrv: InputsService,
    ) {
        super();

    }

    ngOnInit(): void {
        // this.innerForm.controls[DataRangeExtendedComponent.Constants.SelectKey].setValue(this.currentMode);
        this.currentMode = this.input.mode ?? DateRangeExtendedModes.All;
        this._configureForm(this.inpSrv);
        this.selectInput.label = this.input.label;


        this.subscriptions.push(
            this.innerForm.controls[DataRangeExtendedComponent.Constants.SelectKey]
                .valueChanges.subscribe(this.typeSelectChanged.bind(this)),
            this.innerForm.controls[DataRangeExtendedComponent.Constants.IntegerValueKey]
                .valueChanges.subscribe(this.integerValueChanged.bind(this)),
            this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey]
                .valueChanges.subscribe(this.dateValueChanged.bind(this)),
            this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey]
                .valueChanges.subscribe(this.dateValueChanged.bind(this)),
        );
        this.switchToMode(this.currentMode);
        if (this.input.value[0]) {
            this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(this.input.value[0]);
        }
        if (this.input.value[1]) {
            this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(this.input.value[1]);
        }

        this.isConfigured = true;
    }

    typeSelectChanged(value) {
        const mode = value as DateRangeExtendedModes;
        this.switchToMode(mode);
    }

    updateControlValue() {
        const startDate = this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].value as Date;
        const endDate = this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].value as Date;
        const nValue = this.innerForm.controls[DataRangeExtendedComponent.Constants.IntegerValueKey].value as number;
        const optionValue = this.innerForm.controls[DataRangeExtendedComponent.Constants.SelectKey].value;
        const value = {
            nValue: nValue,
            optionValue: optionValue,
            dateRange: [startDate, endDate]
        };
        this.form.controls[this.input.key].setValue(value);
    }

    dateValueChanged(value) {
        this.updateControlValue();
    }

    integerValueChanged(value) {
        const todayDate = new Date(Date.now());
        switch (this.currentMode) {
            case DateRangeExtendedModes.CurrentDayPlusN: {
                const startDate = (value >= 0) ? todayDate : AppUtils.addDays(todayDate, value);
                const endDate = (value < 0) ? todayDate : AppUtils.addDays(todayDate, value);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.PlusNAndBefore: {
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(null);
                const endDate = AppUtils.addDays(todayDate, value);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.PlusNAndLater: {
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(null);
                const startDate = AppUtils.addDays(todayDate, value);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                break;
            }
            default:
                break;
        }
        this.updateControlValue();
    }

    switchToMode(mode: DateRangeExtendedModes) {
        this.currentMode = mode;
        const todayDate = new Date(Date.now());
        switch (mode) {
            case DateRangeExtendedModes.All:
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(null);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(null);
                break;
            case DateRangeExtendedModes.Range:
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(todayDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(todayDate);
                break;
            case DateRangeExtendedModes.SingleDay:
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(todayDate);
                break;
            case DateRangeExtendedModes.CurrentDay:
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(todayDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(todayDate);
                break;
            case DateRangeExtendedModes.CurrentDayPlusN:
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(todayDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(todayDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.IntegerValueKey].setValue(0);
                break;
            case DateRangeExtendedModes.PlusNAndBefore:
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(null);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(todayDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.IntegerValueKey].setValue(0);
                break;
            case DateRangeExtendedModes.PlusNAndLater:
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(todayDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(null);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.IntegerValueKey].setValue(0);
                break;
            case DateRangeExtendedModes.CurrentWeek: {
                const [startDate, endDate] = AppUtils.dateWeekRange(todayDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.CurrentMonth: {
                const [startDate, endDate] = AppUtils.dateMonthRange(todayDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.CurrentQuarter: {
                const [startDate, endDate] = AppUtils.dateQuarterRange(todayDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.CurrentYear: {
                const [startDate, endDate] = AppUtils.dateYearRange(todayDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.PrevWeek: {
                const [startDate, endDate] = AppUtils.dateWeekRange(AppUtils.addDays(todayDate, -7));
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.PrevMonth: {
                const year = todayDate.getFullYear();
                const month = todayDate.getMonth();
                const firstDayDate = new Date(year, month, 1);
                const deltaDate = AppUtils.addDays(firstDayDate, -1);
                const [startDate, endDate] = AppUtils.dateMonthRange(deltaDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.PrevQuarter: {
                const [currentQuarterStart, currentQuarterEnd] = AppUtils.dateQuarterRange(todayDate);
                const deltaDate = AppUtils.addDays(currentQuarterStart, -1);
                const [startDate, endDate] = AppUtils.dateQuarterRange(deltaDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.PrevYear: {
                const year = todayDate.getFullYear();
                const [startDate, endDate] = AppUtils.dateYearRange(new Date(year - 1, 0, 1));
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.LastWeek: {
                const endDate = todayDate;
                const startDate = AppUtils.addDays(endDate, -6);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.LastMonth: {
                const endDate = todayDate;
                const startDate = AppUtils.addMonth(endDate, -1);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.LastQuarter: {
                const endDate = todayDate;
                const startDate = AppUtils.addQuarter(endDate, -1);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            case DateRangeExtendedModes.LastYear: {
                const endDate = todayDate;
                const startDate = AppUtils.addYear(endDate, -1);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateStartKey].setValue(startDate);
                this.innerForm.controls[DataRangeExtendedComponent.Constants.DateEndKey].setValue(endDate);
                break;
            }
            default:
                break;
        }
        this.updateControlValue();
    }

    private _configureForm(inpSrv: InputsService) {
        const selectDescriptor = <IFieldDescriptor>{
            type: E_FIELD_TYPE.select,
            options: this.input.options.length
                ? DateRangeHelperTitles.filter(option => this.input.options.findIndex(allowed => option.value === allowed) !== -1)
                : DateRangeHelperTitles,
            defaultValue: DateRangeExtendedModes.All,
            title: '',
            key: DataRangeExtendedComponent.Constants.SelectKey
        };
        this.selectInput = new SelectInput(selectDescriptor, '', this.currentMode, []);

        const dateStartDescriptor = <IFieldDescriptor>{
            type: E_FIELD_TYPE.date,
            defaultValue: Date.now(),
            title: '',
            key: DataRangeExtendedComponent.Constants.DateStartKey
        };
        this.dateStartInput = new DateInput(dateStartDescriptor, '', dateStartDescriptor.defaultValue);

        const dateEndDescriptor = <IFieldDescriptor>{
            type: E_FIELD_TYPE.date,
            defaultValue: Date.now(),
            title: '',
            key: DataRangeExtendedComponent.Constants.DateEndKey
        };
        this.dateEndInput = new DateInput(dateEndDescriptor, '', dateEndDescriptor.defaultValue);

        const integerValueDescriptor = <IFieldDescriptor>{
            type: E_FIELD_TYPE.number,
            defaultValue: 1,
            minValue: -400,
            maxValue: 400,
            editOptions: {format: '###'},
            title: '',
            key: DataRangeExtendedComponent.Constants.IntegerValueKey
        };
        this.integerValueInput = new NumberInput(integerValueDescriptor, '', integerValueDescriptor.defaultValue);

        this.innerForm = inpSrv.toFormGroup([
            this.selectInput,
            this.dateStartInput,
            this.dateEndInput,
            this.integerValueInput
        ]);
    }

}
