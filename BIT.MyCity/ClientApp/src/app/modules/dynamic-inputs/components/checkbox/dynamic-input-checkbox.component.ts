import { Component } from '@angular/core';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';

@Component({
    selector: 'app-dynamic-input-checkbox',
    templateUrl: 'dynamic-input-checkbox.component.html'
})
export class DynamicInputCheckboxComponent extends DynamicInputBaseComponent {
}
