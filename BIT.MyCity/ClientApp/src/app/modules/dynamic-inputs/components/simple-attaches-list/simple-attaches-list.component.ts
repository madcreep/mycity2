import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { IAttachedFile } from 'src/generated/types.graphql-gen';
import { IDwnFilee, FileIoService } from 'src/app/services/file-io.service';
import { InputsService } from 'src/app/services/input.service/inputs.service';
import { AppUtils, InjectorInstance } from 'src/app/helpers/app.static';
import { E_FIELD_TYPE, IFieldDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';
import { InputBase } from 'src/app/modules/dynamic-inputs/types/input-base';
import { Editable } from 'src/app/common/editable';
import { GraphQLClient } from 'src/app/data-services/graphql/graphql.client';
import { DataService } from 'src/app/services/data.service';
import {InputBuilder} from '../../../../services/input.service/input-builder';


const KEY_PREFIX = 'isPublic';

@Component({
    selector: 'app-simple-attaches-list',
    templateUrl: './simple-attaches-list.component.html',
    styleUrls: ['./simple-attaches-list.component.scss']
})

export class SimpleAttachesListComponent extends Editable implements OnInit, OnChanges {

    @Input() attachesFiles: IAttachedFile[];
    @Input() isPreviewEnabled: boolean = false;
    @Input() readonly = false;

    inputs: InputBase<any>[];

    constructor(
        private _fio: FileIoService,
        protected inpSrv: InputsService,
        protected dataSrv: DataService,
    ) {
        super();
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.inputs = [];
        this.attachesFiles.forEach(f => {
            const key = KEY_PREFIX + f.id;
            // AppUtils.setValueByPath(data, key, f.isPublic);
            // });
            const descr: IFieldDescriptor = {
                type: E_FIELD_TYPE.boolean,
                title: 'Публичное',
                key: key,
            };
            const input = InputBuilder.inputForDescr(descr, '', f.isPublic);
            this.inputs.push(input);
            // this.inpSrv.getInputs(data, false, '').then(inputs => {
            // this.inputs = inputs;
        });

        this.form = this.inpSrv.toFormGroup(this.inputs);
        this.resubscribe();
    }

    ngOnInit() {
    }

    inputFor(item: IAttachedFile) {
        const key = KEY_PREFIX + item.id;
        return this.inputs.find(i => i.key === key);
    }

    download($event, item: IAttachedFile) {
        const file: IDwnFilee = {
            filename: item.name,
            serverId: item.id,
            file: { type: item.contentType },
        };
        this._fio.downloadFileWithAuth(file);
        $event.preventDefault();
    }

    protected onValueChanged(path: string, newValue: any, prevValue: any, initialValue: any): boolean {
        if (path.startsWith(KEY_PREFIX)) {
            const key = path.substr(KEY_PREFIX.length);
            this.dataSrv.attachSetIsPublic(key, newValue).then(
                (data) => data
            );
        }
        return true;
    }

}
