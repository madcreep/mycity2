import { Component, OnInit, OnDestroy } from '@angular/core';
import { MapLoaderService } from 'src/app/services/map-loader.service';
import { Subscription } from 'rxjs';
import { ICoordPoint } from '../../types/map-point-input';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';

const ZOOM_IN_VALUE = 15;

@Component({
    selector: 'app-dynamic-input-map',
    templateUrl: './dynamic-input-map.component.html',
    styleUrls: ['./dynamic-input-map.component.css']
})
export class DynamicInputMapComponent extends DynamicInputBaseComponent implements OnInit, OnDestroy {

    myMap: any;

    subscr: Subscription;
    hasCoord = false;

    private myPlacemark: any;

    constructor(
        private _mapLoaderService: MapLoaderService
    ) {
        super();
    }

    ngOnInit() {
        super.ngOnInit();

        let viewCoord = (this.currentValue as ICoordPoint).coord;
        let zoom;
        if (!viewCoord || !viewCoord[0] || !viewCoord[1]) {
            viewCoord = [0, 0],
                this.hasCoord = false;
            zoom = 7;
        } else {
            this.hasCoord = true;
            zoom = ZOOM_IN_VALUE;
        }

        this.subscr = this._mapLoaderService.getMap(
            'map',
            viewCoord,
            zoom,
        ).subscribe(map => {
            if (map) {
                this.initMap(map);
            }

        });

    }

    public findAddress(address: string, setThisValue: boolean) {
        ymaps.geocode(address, {
            /**
             * Опции запроса
             * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
             */
            // Сортировка результатов от центра окна карты.
            // boundedBy: myMap.getBounds(),
            // strictBounds: true,
            // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy.
            // Если нужен только один результат, экономим трафик пользователей.
            results: 1
        }).then(
            (res) => {
                const firstGeoObject = res.geoObjects.get(0);
                if (firstGeoObject) {
                    // Координаты геообъекта.
                    const coords = firstGeoObject.geometry.getCoordinates();
                    // Область видимости геообъекта.
                    const bounds = firstGeoObject.properties.get('boundedBy');

                    this.myMap.setBounds(bounds, {
                        // Проверяем наличие тайлов на данном масштабе.
                        checkZoomRange: true
                    });

                    this.myMap.setCenter(coords, ZOOM_IN_VALUE);

                    if (setThisValue) {
                        this.setValue({ coord: coords, text: address } as ICoordPoint);

                    }

                }
            }
        );
    }

    setValue(val: ICoordPoint) {
        if (!this.myPlacemark) {
            this.myPlacemark = this.createPlacemark(val.coord);
            this.myMap.geoObjects.add(this.myPlacemark);
        }

        this.myPlacemark.geometry.setCoordinates(val.coord);
        this.myPlacemark.properties
            .set({
                // Формируем строку с данными об объекте.
                iconCaption: val.text,
                // В качестве контента балуна задаем строку с адресом объекта.
                balloonContent: val.text,
            });
        this.storeCoord(val.coord);
        this.storeText(val.text);
    }

    initMap(myMap: any) {
        this.myMap = myMap;
        if (!this.hasCoord) {
            this._mapLoaderService.showGeolocation();
        } else {
            const point = (this.currentValue as ICoordPoint);
            this.myPlacemark = this.createPlacemark(point.coord);
            this.myPlacemark.properties
                .set({
                    // Формируем строку с данными об объекте.
                    iconCaption: point.text, // [
                    //     // Название населенного пункта или вышестоящее административно-территориальное образование.
                    //     firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                    //     // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                    //     firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                    // ].filter(Boolean).join(', '),
                    // В качестве контента балуна задаем строку с адресом объекта.
                    balloonContent: point.text,
                });
            this.myMap.geoObjects.add(this.myPlacemark);
        }

        if (!this.input.readonly && !this.disabled) {
            myMap.events.add('click', (e) => {
                let coords = e.get('coords');

                // Если метка уже создана – просто передвигаем ее.
                if (this.myPlacemark) {
                    this.myPlacemark.geometry.setCoordinates(coords);
                } else { // Если нет – создаем.
                    this.myPlacemark = this.createPlacemark(coords);
                    this.myMap.geoObjects.add(this.myPlacemark);
                    // Слушаем событие окончания перетаскивания на метке.
                    this.myPlacemark.events.add('dragend', () => {
                        coords = this.myPlacemark.geometry.getCoordinates();
                        this.getAddress(coords);
                    });
                }
                this.storeCoord(coords);
                this.getAddress(coords);
            });
        }

    }
    getAddress(coords) {
        this.myPlacemark.properties.set('iconCaption', 'поиск...');

        ymaps.geocode(coords).then((res) => {
            const firstGeoObject = res.geoObjects.get(0);
            const text = firstGeoObject.getAddressLine();
            this.storeText(text);
            this.myPlacemark.properties
                .set({
                    // Формируем строку с данными об объекте.
                    iconCaption: [
                        // Название населенного пункта или вышестоящее административно-территориальное образование.
                        firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                        // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                        firstGeoObject.getThoroughfare() || firstGeoObject.getPremise()
                    ].filter(Boolean).join(', '),
                    // В качестве контента балуна задаем строку с адресом объекта.
                    balloonContent: text,
                });
        });
    }

    createPlacemark(coords: any): any {
        return new ymaps.Placemark(coords, {
            iconCaption: 'поиск...'
        }, {
            preset: 'islands#violetDotIconWithCaption',
            draggable: true
        });
    }

    ngOnDestroy() {
        if (this.subscr) {
            this.subscr.unsubscribe();
        }

        super.ngOnDestroy();
    }

    private storeCoord(coord: number[]) {
        const val: ICoordPoint = this.currentValue;
        val.coord = coord;
        this.control.setValue(val);
    }

    private storeText(text: any) {
        const val: ICoordPoint = this.currentValue;
        val.text = text;
        this.control.setValue(val);
    }
}
