import { Component } from '@angular/core';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';

@Component({
    selector: 'app-dynamic-input-text',
    templateUrl: 'dynamic-input-text.component.html'
})
export class DynamicInputTextComponent extends DynamicInputBaseComponent {
    get label(): string {
        return this.input.label;
    }
}
