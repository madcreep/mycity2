import { Component, OnChanges, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { InjectorInstance } from 'src/app/helpers/app.static';
import { E_FIELD_TYPE, IFieldDescriptor } from 'src/app/model/datatypes/descriptors.interfaces';
import { DataService } from 'src/app/services/data.service';
import { InputsService } from 'src/app/services/input.service/inputs.service';
import { SubsystemsService } from 'src/app/services/subsystems.service';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';
import { MultiselectInput } from '../../types/multiselect-input';

@Component({
    selector: 'app-dynamic-input-moderators-subs',
    templateUrl: './dynamic-input-moderators-subs.component.html',
    styleUrls: ['./dynamic-input-moderators-subs.component.scss']
})
export class DynamicInputModeratorsSubSComponent extends DynamicInputBaseComponent implements OnChanges, OnInit {
    intform: FormGroup = null;
    ssinputs: any[];
    private _isInternalChange: boolean;
    ngOnInit() {
        this.refreshInputs();

        this.subscriptions.push(this.control.valueChanges.subscribe((newValue) => {
            if (!this._isInternalChange) {
                this.refreshInputs();
            }
        }));

    }
    refreshInputs() {

        this.ssinputs = [];

        const subSrv = InjectorInstance.get(SubsystemsService);
        const api = InjectorInstance.get(DataService);

        this.currentValue.forEach(el => {
            const field: IFieldDescriptor = {
                title: 'Модераторы - ' + subSrv.titleForSubsistem(el['subsystemUid']),
                type: E_FIELD_TYPE.multiselect,
                keydescriptor: el['subsystemUid'],
                key: this.input.key + '.' + el['subsystemUid'],

                initOptions: (initinput) => {
                    return api.availableModeratorsFor([el.subsystemUid]).then(
                        (result) => {
                            const moderators = result.availableModerators;
                            initinput.options = moderators.items.map(m => {
                                return { title: m.fullName, value: m.id };
                            });
                            return initinput;
                        }
                    );
                },
            }
            this.ssinputs.push(new MultiselectInput(field, '', el['userIds']));
        });

        const inpSrv = InjectorInstance.get(InputsService);
        inpSrv.initInputs(this.ssinputs).then(
            () => {
                this.intform = inpSrv.toFormGroup(this.ssinputs);

                for (const key in this.intform.controls) {
                    if (this.intform.controls.hasOwnProperty(key)) {
                        const control = this.intform.controls[key];
                        this.subscriptions.push(control.valueChanges.subscribe((newValue) => {
                            const val = this.currentValue;
                            const subsystemUid = control['input'].descriptor.keydescriptor;
                            const index = val.findIndex(v => v.subsystemUid === subsystemUid);
                            val[index].userIds = newValue;
                            this._isInternalChange = true;
                            this.control.setValue(
                                [... val] // new instance for fire onValueChanged
                            );
                            this._isInternalChange = false;
                        }));
                    }
                }

            }
        );

    }

    ngOnChanges(changes) {
        super.ngOnChanges(changes);
    }



    onClose(e) {
    }

    protected onValueChanged(path: string, newValue: any, prevValue: any /*, initialValue: any*/) {
        return false;
    }

}
