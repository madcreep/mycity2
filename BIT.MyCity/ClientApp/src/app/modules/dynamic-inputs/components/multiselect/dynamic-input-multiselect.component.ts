import { Component, OnChanges, ViewChild } from '@angular/core';
import { Query } from '@syncfusion/ej2-data';
import { EmitType } from '@syncfusion/ej2-base';
import { FilteringEventArgs } from '@syncfusion/ej2-angular-dropdowns';
import { DynamicInputBaseComponent } from '../../fasade/dynamic-input-base.component';

@Component({
  selector: 'app-dynamic-input-multiselect',
  templateUrl: './dynamic-input-multiselect.component.html',
  styleUrls: ['./dynamic-input-multiselect.component.css']
})

export class DynamicInputMultiselectComponent extends DynamicInputBaseComponent implements OnChanges {
    @ViewChild('multiselect') multiselect;
    datasource;
    fields =  { text: 'title', value: 'value' };

    ngOnChanges (changes) {
        super.ngOnChanges(changes);
        this.datasource = this.input.options;
    }

    onChange(e) {
    }

    onClose(e) {
        // valuechange fire
        this.multiselect.focusOut();
    }

    public onFiltering: EmitType<FilteringEventArgs> =  (e: FilteringEventArgs) => {
        let query = new Query();
        query = (e.text !== '') ? query.where('title', 'startswith', e.text, true) : query;
        e.updateData(this.datasource, query);
    }
}
