import { Injectable, OnDestroy } from '@angular/core';

import { Subscriptionable } from './subscriptionable';
import { FormGroup } from '@angular/forms';
import { AppUtils } from '../helpers/app.static';

@Injectable()
export abstract class Editable extends Subscriptionable implements OnDestroy {

    public form: FormGroup;
    public isChanged = false;
    public editedData: any = {};
    public initial = {};
    public prevValues = {};

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    public setValue(key: string, value: any) {
        const control = this.form.controls[key];
        if (control) {
            control.setValue(value);
        }
    }

    public getValue(key: string) {
        const control = this.form.controls[key];
        if (control) {
            return control.value;
        }
        return null;
    }

    protected abstract onValueChanged(path: any, newValue: any, prevValue: any, initialValue: any): boolean;

    protected resubscribe() {
        this.unsubscribe();
        for (const key in this.form.controls) {
            if (this.form.controls.hasOwnProperty(key)) {
                const control = <any>this.form.controls[key];
                this.subscriptions.push(control.valueChanges.subscribe((newValue) => {
                    const path = control['id'];
                    // this.isChanged = true;

                    const initial = AppUtils.getValueByPath(this.initial, path);
                    const prev = AppUtils.getValueByPath(this.prevValues, path);
                    if (prev !== newValue) {
                        if (!this.onValueChanged(path, newValue, prev, initial)) {
                            this.isChanged = true;
                            if (initial === newValue /*|| !newValue || newValue.length === 0*/) {
                                delete this.editedData[path];
                                AppUtils.setValueByPath(this.editedData, path, undefined);
                                AppUtils.setValueByPath(this.prevValues, path, initial);
                            } else {
                                AppUtils.setValueByPath(this.prevValues, path, newValue);
                                AppUtils.setValueByPath(this.editedData, path, newValue);
                            }
                        }
                    }

                }));
            }
        }
    }

}
