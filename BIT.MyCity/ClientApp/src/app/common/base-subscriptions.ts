import { Subscriptionable } from './subscriptionable';
import { InjectorInstance, AppUtils } from '../helpers/app.static';
import { Injector } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { IUser } from 'src/generated/types.graphql-gen';
import { DataService } from '../services/data.service';
import { SubsystemsService } from '../services/subsystems.service';

export class BaseSubscriptions extends Subscriptionable {

    isUserLogged = false;
    loggedUser: IUser;
    isAccessDenied = false;

    protected injector: Injector;
    protected authSrv: AuthService;
    protected dataSrv: DataService;
    protected sysSrv: SubsystemsService;

    constructor() {
        super();
        this.injector = InjectorInstance;
        this.authSrv = this.injector.get(AuthService);
        this.dataSrv = this.injector.get(DataService);
        this.sysSrv = this.injector.get(SubsystemsService);
        this.subscribe(
            this.sysSrv.onAfterInit.subscribe(
                (ready) => {
                    if (ready) {
                        this.subscribe (
                            this.authSrv.loginChanged.subscribe(
                                this.subLoginChanged()
                            )
                        );
                    }
                }
            )
        )

    }

    protected subLoginChanged(): (result) => any {
        return (result) => {
            this.isUserLogged = !AppUtils.isObjectEmpty(result);
            this.loggedUser = this.isUserLogged ? result : null;
            this.isAccessDenied = this.calcAccessDenied(this.loggedUser);
        };
    }

    protected calcAccessDenied(loggedUser: IUser): boolean {
        return false;
    }

}
