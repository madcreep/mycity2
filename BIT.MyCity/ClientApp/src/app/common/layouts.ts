export class LayoutsRoutine {

    static inputsToLayout(layout: any[], objects: { key: string }[]): any[] {
        return layout.map(
            (r) => {
                if (r instanceof Array) {
                    return this.inputsToLayout(r, objects);
                } else {
                    return objects.find( i => i.key === r);
                }
            }
        );
    }

}
