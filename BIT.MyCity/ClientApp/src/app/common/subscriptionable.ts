import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Injectable()
export abstract class Subscriptionable implements OnDestroy {

    protected subscriptions: Subscription[] = [];

    ngOnDestroy(): void {
        this.unsubscribe();
    }

    protected subscribe(s: Subscription) {
        this.subscriptions.push(s);
    }

    protected unsubscribe() {
        this.subscriptions.forEach((subscription: Subscription) => {
            if (subscription != null && !subscription.closed) {
                subscription.unsubscribe();
            }
        });
        this.subscriptions = [];
    }
}
