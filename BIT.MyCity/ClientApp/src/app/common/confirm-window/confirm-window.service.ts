import {Injectable} from '@angular/core';

import {BsModalService, BsModalRef} from 'ngx-bootstrap/modal';
import {IConfirmWindow, ConfirmWindowComponent, IConfirmButton} from './confirm-window.component';
import {Subscriptionable} from '../subscriptionable';
import {BUTTON_RESULT_CANCEL, BUTTON_RESULT_OK} from '../../consts/confirmations.const';

@Injectable({
    providedIn: 'root'
})

export class ConfirmWindowService extends Subscriptionable {

    constructor(private _bsModalSrv: BsModalService) {
        super();
    }

    confirm(content: IConfirmWindow, workPromise: Promise<any> = Promise.resolve()): Promise<IConfirmButton> {
        const config = {ignoreBackdropClick: true, animated: false, keyboard: true};
        const bsModalRef: BsModalRef = this._bsModalSrv.show(ConfirmWindowComponent, config);
        const _wnd: ConfirmWindowComponent = bsModalRef.content;

        Object.assign(_wnd, content);
        return workPromise.then(_ => {
            return new Promise((res, _rej) => {

                this.subscriptions.push(
                    this._bsModalSrv.onHide.subscribe(reason => {
                        this.unsubscribe();
                        if (reason === 'backdrop-click' || reason === 'esc') {
                            res(null);
                        }

                    }));

                this.subscriptions.push(
                    _wnd.confirmEvent.subscribe((confirm: IConfirmButton) => {
                        this.unsubscribe();
                        if (confirm !== undefined) {
                            res(confirm);
                        }

                    }))
                ;
            });
        });
    }

    buildMultilineMessage(title: string, body: string, strings: string[]): IConfirmWindow {
        return {
            title: title,
            bodyList: strings,
            body: '',
            // bodyAfterList: 'Продолжить?',
            buttons: [
                {title: 'Ok', result: BUTTON_RESULT_OK, } ,
            ],
        };
    }
    buildMessage(title: string, body: string, strings: string[], buttons: IConfirmButton []): IConfirmWindow {
        return {
            title: title,
            bodyList: strings,
            body: body,
            buttons: buttons
        };
    }
}
