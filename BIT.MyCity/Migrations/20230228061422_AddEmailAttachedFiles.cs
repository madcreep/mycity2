﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class AddEmailAttachedFiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "EmailId",
                schema: "public",
                table: "AttachedFiles",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_EmailId",
                schema: "public",
                table: "AttachedFiles",
                column: "EmailId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_Emails_EmailId",
                schema: "public",
                table: "AttachedFiles",
                column: "EmailId",
                principalSchema: "public",
                principalTable: "Emails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_Emails_EmailId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_EmailId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "EmailId",
                schema: "public",
                table: "AttachedFiles");
        }
    }
}
