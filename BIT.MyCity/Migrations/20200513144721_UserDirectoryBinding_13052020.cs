﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class UserDirectoryBinding_13052020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UserTerritories",
                schema: "public",
                table: "UserTerritories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserRubrics",
                schema: "public",
                table: "UserRubrics");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserRegions",
                schema: "public",
                table: "UserRegions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserModeratorTopics",
                schema: "public",
                table: "UserModeratorTopics");

            migrationBuilder.AddColumn<long>(
                name: "SubsystemId",
                schema: "public",
                table: "UserTerritories",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "SubsystemId",
                schema: "public",
                table: "UserRubrics",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "SubsystemId",
                schema: "public",
                table: "UserRegions",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "SubsystemId",
                schema: "public",
                table: "UserModeratorTopics",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserTerritories",
                schema: "public",
                table: "UserTerritories",
                columns: new[] { "UserId", "TerritoryId", "SubsystemId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserRubrics",
                schema: "public",
                table: "UserRubrics",
                columns: new[] { "UserId", "RubricId", "SubsystemId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserRegions",
                schema: "public",
                table: "UserRegions",
                columns: new[] { "UserId", "RegionId", "SubsystemId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserModeratorTopics",
                schema: "public",
                table: "UserModeratorTopics",
                columns: new[] { "UserId", "TopicId", "SubsystemId" });

            migrationBuilder.CreateIndex(
                name: "IX_UserTerritories_SubsystemId",
                schema: "public",
                table: "UserTerritories",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRubrics_SubsystemId",
                schema: "public",
                table: "UserRubrics",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRegions_SubsystemId",
                schema: "public",
                table: "UserRegions",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_UserModeratorTopics_SubsystemId",
                schema: "public",
                table: "UserModeratorTopics",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Subsystems_UID",
                schema: "public",
                table: "Subsystems",
                column: "UID");

            migrationBuilder.AddForeignKey(
                name: "FK_UserModeratorTopics_Subsystems_SubsystemId",
                schema: "public",
                table: "UserModeratorTopics",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserRegions_Subsystems_SubsystemId",
                schema: "public",
                table: "UserRegions",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserRubrics_Subsystems_SubsystemId",
                schema: "public",
                table: "UserRubrics",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTerritories_Subsystems_SubsystemId",
                schema: "public",
                table: "UserTerritories",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserModeratorTopics_Subsystems_SubsystemId",
                schema: "public",
                table: "UserModeratorTopics");

            migrationBuilder.DropForeignKey(
                name: "FK_UserRegions_Subsystems_SubsystemId",
                schema: "public",
                table: "UserRegions");

            migrationBuilder.DropForeignKey(
                name: "FK_UserRubrics_Subsystems_SubsystemId",
                schema: "public",
                table: "UserRubrics");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTerritories_Subsystems_SubsystemId",
                schema: "public",
                table: "UserTerritories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserTerritories",
                schema: "public",
                table: "UserTerritories");

            migrationBuilder.DropIndex(
                name: "IX_UserTerritories_SubsystemId",
                schema: "public",
                table: "UserTerritories");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserRubrics",
                schema: "public",
                table: "UserRubrics");

            migrationBuilder.DropIndex(
                name: "IX_UserRubrics_SubsystemId",
                schema: "public",
                table: "UserRubrics");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserRegions",
                schema: "public",
                table: "UserRegions");

            migrationBuilder.DropIndex(
                name: "IX_UserRegions_SubsystemId",
                schema: "public",
                table: "UserRegions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserModeratorTopics",
                schema: "public",
                table: "UserModeratorTopics");

            migrationBuilder.DropIndex(
                name: "IX_UserModeratorTopics_SubsystemId",
                schema: "public",
                table: "UserModeratorTopics");

            migrationBuilder.DropIndex(
                name: "IX_Subsystems_UID",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "SubsystemId",
                schema: "public",
                table: "UserTerritories");

            migrationBuilder.DropColumn(
                name: "SubsystemId",
                schema: "public",
                table: "UserRubrics");

            migrationBuilder.DropColumn(
                name: "SubsystemId",
                schema: "public",
                table: "UserRegions");

            migrationBuilder.DropColumn(
                name: "SubsystemId",
                schema: "public",
                table: "UserModeratorTopics");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserTerritories",
                schema: "public",
                table: "UserTerritories",
                columns: new[] { "UserId", "TerritoryId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserRubrics",
                schema: "public",
                table: "UserRubrics",
                columns: new[] { "UserId", "RubricId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserRegions",
                schema: "public",
                table: "UserRegions",
                columns: new[] { "UserId", "RegionId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserModeratorTopics",
                schema: "public",
                table: "UserModeratorTopics",
                columns: new[] { "UserId", "TopicId" });
        }
    }
}
