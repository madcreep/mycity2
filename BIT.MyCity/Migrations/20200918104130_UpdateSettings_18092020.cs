﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class UpdateSettings_18092020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Settings_Tag1",
                schema: "public",
                table: "Settings");

            migrationBuilder.DropIndex(
                name: "IX_Settings_Tag2",
                schema: "public",
                table: "Settings");

            migrationBuilder.DropIndex(
                name: "IX_Settings_Tag3",
                schema: "public",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "Tag1",
                schema: "public",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "Tag2",
                schema: "public",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "Tag3",
                schema: "public",
                table: "Settings");

            migrationBuilder.AddColumn<int>(
                name: "SettingNodeType",
                schema: "public",
                table: "Settings",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Weight",
                schema: "public",
                table: "Settings",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SettingNodeType",
                schema: "public",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "Weight",
                schema: "public",
                table: "Settings");

            migrationBuilder.AddColumn<string>(
                name: "Tag1",
                schema: "public",
                table: "Settings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tag2",
                schema: "public",
                table: "Settings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tag3",
                schema: "public",
                table: "Settings",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Settings_Tag1",
                schema: "public",
                table: "Settings",
                column: "Tag1");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_Tag2",
                schema: "public",
                table: "Settings",
                column: "Tag2");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_Tag3",
                schema: "public",
                table: "Settings",
                column: "Tag3");
        }
    }
}
