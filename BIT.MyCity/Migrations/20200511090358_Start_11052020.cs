﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BIT.MyCity.Migrations
{
    public partial class Start_11052020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Editable = table.Column<bool>(nullable: false),
                    IsAdminRegister = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DisconnectedTimestamp = table.Column<DateTime>(nullable: true),
                    Disconnected = table.Column<bool>(nullable: false),
                    Weight = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    MiddleName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    NormalizeFullName = table.Column<string>(nullable: true),
                    LoginSystem = table.Column<int>(nullable: false),
                    ProfileConfirmed = table.Column<bool>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DisconnectedTimestamp = table.Column<DateTime>(nullable: true),
                    Disconnected = table.Column<bool>(nullable: false),
                    Gender = table.Column<int>(nullable: true),
                    Inn = table.Column<string>(nullable: true),
                    Snils = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Emails",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Message = table.Column<string>(nullable: true),
                    Template = table.Column<string>(nullable: true),
                    SendingTimestamp = table.Column<DateTime>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Emails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EmailTemplates",
                schema: "public",
                columns: table => new
                {
                    Key = table.Column<string>(nullable: false),
                    Template = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmailTemplates", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Phones",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Type = table.Column<int>(nullable: false),
                    Number = table.Column<string>(nullable: true),
                    Additional = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Phones", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Regions",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentId = table.Column<long>(nullable: false),
                    ParentType = table.Column<string>(nullable: true),
                    IsNode = table.Column<bool>(nullable: false),
                    Due = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    ExtId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Regions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Rubrics",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentId = table.Column<long>(nullable: false),
                    ParentType = table.Column<string>(nullable: true),
                    IsNode = table.Column<bool>(nullable: false),
                    Due = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    ExtId = table.Column<string>(nullable: true),
                    ExtType = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Docgroup = table.Column<string>(nullable: true),
                    Receivers = table.Column<string>(nullable: true),
                    UseForPA = table.Column<bool>(nullable: false),
                    ElDocForUL = table.Column<bool>(nullable: true),
                    DocGroupForUL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rubrics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Settings",
                schema: "public",
                columns: table => new
                {
                    Key = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    Tag1 = table.Column<string>(nullable: true),
                    Tag2 = table.Column<string>(nullable: true),
                    Tag3 = table.Column<string>(nullable: true),
                    ReadingRightLevel = table.Column<int>(nullable: false),
                    ValueType = table.Column<string>(nullable: true),
                    Nullable = table.Column<bool>(nullable: false),
                    SettingType = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Subsystems",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    UID = table.Column<string>(nullable: true),
                    RegionIds = table.Column<List<long>>(nullable: true),
                    RubricIds = table.Column<List<long>>(nullable: true),
                    TerritoryIds = table.Column<List<long>>(nullable: true),
                    TopicIds = table.Column<List<long>>(nullable: true),
                    AppealPropertyNames = table.Column<List<string>>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subsystems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Territories",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentId = table.Column<long>(nullable: false),
                    ParentType = table.Column<string>(nullable: true),
                    IsNode = table.Column<bool>(nullable: false),
                    Due = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    ExtId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Territories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Topics",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentId = table.Column<long>(nullable: false),
                    ParentType = table.Column<string>(nullable: true),
                    IsNode = table.Column<bool>(nullable: false),
                    Due = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    ExtId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Topics", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserOrganizations",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ParentId = table.Column<long>(nullable: true),
                    ExtId = table.Column<string>(nullable: true),
                    ExtParentId = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    NormalizeShortName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    NormalizeFullName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    EmailVerified = table.Column<bool>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Ogrn = table.Column<string>(nullable: true),
                    Inn = table.Column<string>(nullable: true),
                    Leg = table.Column<string>(nullable: true),
                    Kpp = table.Column<string>(nullable: true),
                    AgencyTerRang = table.Column<string>(nullable: true),
                    AgencyType = table.Column<int>(nullable: true),
                    IsBrunch = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserOrganizations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserOrganizations_UserOrganizations_ParentId",
                        column: x => x.ParentId,
                        principalSchema: "public",
                        principalTable: "UserOrganizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    RoleId = table.Column<long>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "public",
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AppealExecutors",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentId = table.Column<long>(nullable: false),
                    ParentType = table.Column<string>(nullable: true),
                    IsNode = table.Column<bool>(nullable: false),
                    Due = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    DepartmentId = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    Organization = table.Column<string>(nullable: true),
                    ExtId = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppealExecutors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppealExecutors_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<long>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                schema: "public",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    RoleId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "public",
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserIdentityDocuments",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<long>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Verified = table.Column<bool>(nullable: false),
                    Series = table.Column<string>(nullable: true),
                    Number = table.Column<string>(nullable: true),
                    IssueDate = table.Column<DateTime>(nullable: true),
                    IssueId = table.Column<string>(nullable: true),
                    IssuedBy = table.Column<string>(nullable: true),
                    ExpiryDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserIdentityDocuments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserIdentityDocuments_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserRegions",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    RegionId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRegions", x => new { x.UserId, x.RegionId });
                    table.ForeignKey(
                        name: "FK_UserRegions_Regions_RegionId",
                        column: x => x.RegionId,
                        principalSchema: "public",
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRegions_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Officers",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ExtId = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    Authority = table.Column<string>(nullable: true),
                    Place = table.Column<string>(nullable: true),
                    RubricId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Officers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Officers_Rubrics_RubricId",
                        column: x => x.RubricId,
                        principalSchema: "public",
                        principalTable: "Rubrics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRubrics",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    RubricId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRubrics", x => new { x.UserId, x.RubricId });
                    table.ForeignKey(
                        name: "FK_UserRubrics_Rubrics_RubricId",
                        column: x => x.RubricId,
                        principalSchema: "public",
                        principalTable: "Rubrics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRubrics_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Votes",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    Start = table.Column<DateTime>(nullable: true),
                    End = table.Column<DateTime>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    TextResult = table.Column<string>(nullable: true),
                    State = table.Column<int>(nullable: false),
                    Mode = table.Column<int>(nullable: false),
                    ModeLimitValue = table.Column<int>(nullable: false),
                    OfferEnabled = table.Column<bool>(nullable: false),
                    OffersCount = table.Column<int>(nullable: false),
                    SubsystemId1 = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Votes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Votes_Subsystems_SubsystemId1",
                        column: x => x.SubsystemId1,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserTerritories",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    TerritoryId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTerritories", x => new { x.UserId, x.TerritoryId });
                    table.ForeignKey(
                        name: "FK_UserTerritories_Territories_TerritoryId",
                        column: x => x.TerritoryId,
                        principalSchema: "public",
                        principalTable: "Territories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserTerritories_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserModeratorTopics",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    TopicId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserModeratorTopics", x => new { x.UserId, x.TopicId });
                    table.ForeignKey(
                        name: "FK_UserModeratorTopics_Topics_TopicId",
                        column: x => x.TopicId,
                        principalSchema: "public",
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserModeratorTopics_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<long>(nullable: true),
                    UserOrganizationId = table.Column<long>(nullable: true),
                    AddrType = table.Column<int>(nullable: false),
                    ZipCode = table.Column<string>(nullable: true),
                    CountryId = table.Column<string>(nullable: true),
                    Building = table.Column<string>(nullable: true),
                    Frame = table.Column<string>(nullable: true),
                    House = table.Column<string>(nullable: true),
                    Flat = table.Column<string>(nullable: true),
                    FiasCode = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    District = table.Column<string>(nullable: true),
                    Area = table.Column<string>(nullable: true),
                    Settlement = table.Column<string>(nullable: true),
                    AdditionArea = table.Column<string>(nullable: true),
                    AdditionAreaStreet = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    PartialAddress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Addresses_UserOrganizations_UserOrganizationId",
                        column: x => x.UserOrganizationId,
                        principalSchema: "public",
                        principalTable: "UserOrganizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Appeals",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    RubricId = table.Column<long>(nullable: true),
                    TerritoryId = table.Column<long>(nullable: true),
                    TopicId = table.Column<long>(nullable: true),
                    RegionId = table.Column<long>(nullable: true),
                    OrganizationId = table.Column<long>(nullable: true),
                    AuthorId = table.Column<long>(nullable: false),
                    SubsystemId1 = table.Column<long>(nullable: true),
                    Public_ = table.Column<bool>(nullable: false),
                    Approved = table.Column<int>(nullable: false),
                    RejectionReason = table.Column<string>(nullable: true),
                    ModerationStage = table.Column<int>(nullable: false),
                    PublicGranted = table.Column<bool>(nullable: false),
                    LikesCount = table.Column<int>(nullable: false),
                    DislikesCount = table.Column<int>(nullable: false),
                    ViewsCount = table.Column<int>(nullable: false),
                    Signatory = table.Column<string>(nullable: true),
                    Post = table.Column<string>(nullable: true),
                    RegNumber = table.Column<string>(nullable: true),
                    RegDate = table.Column<DateTime>(nullable: true),
                    ExtDate = table.Column<DateTime>(nullable: true),
                    ResDate = table.Column<DateTime>(nullable: true),
                    PlanDate = table.Column<DateTime>(nullable: true),
                    FactDate = table.Column<DateTime>(nullable: true),
                    FilesPush = table.Column<DateTime>(nullable: true),
                    WORegNumberRegDate = table.Column<bool>(nullable: false),
                    Fulltext = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    ExtId = table.Column<string>(nullable: true),
                    NeedToUpdateInExt = table.Column<bool>(nullable: false),
                    ExtNumber = table.Column<string>(nullable: true),
                    Addressee = table.Column<string>(nullable: true),
                    AuthorName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Patronim = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    TopicUpdated = table.Column<bool>(nullable: false),
                    SiteStatus = table.Column<int>(nullable: false),
                    MarkSend = table.Column<bool>(nullable: false),
                    Latitude = table.Column<string>(nullable: true),
                    Longitude = table.Column<string>(nullable: true),
                    Place = table.Column<string>(nullable: true),
                    StatusConfirmed = table.Column<bool>(nullable: false),
                    PushesSent = table.Column<bool>(nullable: false),
                    Number = table.Column<int>(nullable: false),
                    Platform = table.Column<string>(nullable: true),
                    SentToDelo = table.Column<bool>(nullable: false),
                    AnswerByPost = table.Column<bool>(nullable: true),
                    ExternalExec = table.Column<bool>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Zip = table.Column<string>(nullable: true),
                    Region1 = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Municipality = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Views = table.Column<int>(nullable: false),
                    UnapprovedComments = table.Column<int>(nullable: false),
                    Uploaded = table.Column<bool>(nullable: true),
                    ClientType = table.Column<int>(nullable: false),
                    Inn = table.Column<string>(nullable: true),
                    Esia = table.Column<bool>(nullable: true),
                    DeloText = table.Column<string>(nullable: true),
                    DeloStatus = table.Column<string>(nullable: true),
                    Collective = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Appeals", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Appeals_AspNetUsers_AuthorId",
                        column: x => x.AuthorId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Appeals_UserOrganizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "public",
                        principalTable: "UserOrganizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appeals_Regions_RegionId",
                        column: x => x.RegionId,
                        principalSchema: "public",
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appeals_Rubrics_RubricId",
                        column: x => x.RubricId,
                        principalSchema: "public",
                        principalTable: "Rubrics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appeals_Subsystems_SubsystemId1",
                        column: x => x.SubsystemId1,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appeals_Territories_TerritoryId",
                        column: x => x.TerritoryId,
                        principalSchema: "public",
                        principalTable: "Territories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appeals_Topics_TopicId",
                        column: x => x.TopicId,
                        principalSchema: "public",
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrganizationUserPhones",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UserId = table.Column<long>(nullable: true),
                    OrganizationId = table.Column<long>(nullable: true),
                    PhoneId = table.Column<long>(nullable: false),
                    Confirmed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganizationUserPhones", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrganizationUserPhones_UserOrganizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalSchema: "public",
                        principalTable: "UserOrganizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrganizationUserPhones_Phones_PhoneId",
                        column: x => x.PhoneId,
                        principalSchema: "public",
                        principalTable: "Phones",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OrganizationUserPhones_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User2Organization",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    UserOrganizationId = table.Column<long>(nullable: false),
                    Chief = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User2Organization", x => new { x.UserId, x.UserOrganizationId });
                    table.ForeignKey(
                        name: "FK_User2Organization_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User2Organization_UserOrganizations_UserOrganizationId",
                        column: x => x.UserOrganizationId,
                        principalSchema: "public",
                        principalTable: "UserOrganizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AppointmentRanges",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentId = table.Column<long>(nullable: false),
                    ParentType = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    Start = table.Column<DateTime>(nullable: true),
                    End = table.Column<DateTime>(nullable: true),
                    Appointments = table.Column<int>(nullable: true),
                    OfficerName = table.Column<string>(nullable: true),
                    RubricOrder = table.Column<int>(nullable: true),
                    RubricName = table.Column<string>(nullable: true),
                    OfficerId = table.Column<long>(nullable: true),
                    DeleteMark = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppointmentRanges", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppointmentRanges_Officers_OfficerId",
                        column: x => x.OfficerId,
                        principalSchema: "public",
                        principalTable: "Officers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserVotes",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    VoteId = table.Column<long>(nullable: false),
                    VotepunktIds = table.Column<List<long>>(nullable: true),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserVotes", x => new { x.UserId, x.VoteId });
                    table.ForeignKey(
                        name: "FK_UserVotes_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserVotes_Votes_VoteId",
                        column: x => x.VoteId,
                        principalSchema: "public",
                        principalTable: "Votes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VoteItems",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    Number = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    VoteId = table.Column<long>(nullable: true),
                    Counter = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VoteItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VoteItems_Votes_VoteId",
                        column: x => x.VoteId,
                        principalSchema: "public",
                        principalTable: "Votes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Appeal2Executor",
                schema: "public",
                columns: table => new
                {
                    AppealId = table.Column<long>(nullable: false),
                    AppealExecutorId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Appeal2Executor", x => new { x.AppealExecutorId, x.AppealId });
                    table.ForeignKey(
                        name: "FK_Appeal2Executor_AppealExecutors_AppealExecutorId",
                        column: x => x.AppealExecutorId,
                        principalSchema: "public",
                        principalTable: "AppealExecutors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Appeal2Executor_Appeals_AppealId",
                        column: x => x.AppealId,
                        principalSchema: "public",
                        principalTable: "Appeals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Appeal2PrincipalExecutor",
                schema: "public",
                columns: table => new
                {
                    AppealId = table.Column<long>(nullable: false),
                    AppealPrincipalExecutorId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Appeal2PrincipalExecutor", x => new { x.AppealPrincipalExecutorId, x.AppealId });
                    table.ForeignKey(
                        name: "FK_Appeal2PrincipalExecutor_Appeals_AppealId",
                        column: x => x.AppealId,
                        principalSchema: "public",
                        principalTable: "Appeals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Appeal2PrincipalExecutor_AppealExecutors_AppealPrincipalExe~",
                        column: x => x.AppealPrincipalExecutorId,
                        principalSchema: "public",
                        principalTable: "AppealExecutors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Messages",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentId = table.Column<long>(nullable: false),
                    ParentType = table.Column<string>(nullable: true),
                    IsNode = table.Column<bool>(nullable: false),
                    Due = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    Approved = table.Column<int>(nullable: false),
                    ModerationStage = table.Column<int>(nullable: false),
                    Public_ = table.Column<bool>(nullable: false),
                    PublicGranted = table.Column<bool>(nullable: false),
                    Kind = table.Column<int>(nullable: false),
                    AuthorId = table.Column<long>(nullable: true),
                    AppealExecutorId = table.Column<long>(nullable: true),
                    LikesCount = table.Column<int>(nullable: false),
                    DislikesCount = table.Column<int>(nullable: false),
                    ViewsCount = table.Column<int>(nullable: false),
                    RootAppealId = table.Column<long>(nullable: true),
                    RootVoteId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Messages_AppealExecutors_AppealExecutorId",
                        column: x => x.AppealExecutorId,
                        principalSchema: "public",
                        principalTable: "AppealExecutors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Messages_AspNetUsers_AuthorId",
                        column: x => x.AuthorId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Messages_Appeals_RootAppealId",
                        column: x => x.RootAppealId,
                        principalSchema: "public",
                        principalTable: "Appeals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Messages_Votes_RootVoteId",
                        column: x => x.RootVoteId,
                        principalSchema: "public",
                        principalTable: "Votes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserLikeAppeals",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    AppealId = table.Column<long>(nullable: false),
                    Liked = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLikeAppeals", x => new { x.UserId, x.AppealId });
                    table.ForeignKey(
                        name: "FK_UserLikeAppeals_Appeals_AppealId",
                        column: x => x.AppealId,
                        principalSchema: "public",
                        principalTable: "Appeals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserLikeAppeals_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserModeratorAppeals",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    AppealId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserModeratorAppeals", x => new { x.UserId, x.AppealId });
                    table.ForeignKey(
                        name: "FK_UserModeratorAppeals_Appeals_AppealId",
                        column: x => x.AppealId,
                        principalSchema: "public",
                        principalTable: "Appeals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserModeratorAppeals_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Appointments",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentId = table.Column<long>(nullable: false),
                    ParentType = table.Column<string>(nullable: true),
                    Number = table.Column<int>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: true),
                    RejectionReason = table.Column<string>(nullable: true),
                    AuthorName = table.Column<string>(nullable: true),
                    AuthorNameUpperCase = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    Municipality = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Zip = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Admin = table.Column<bool>(nullable: true),
                    RubricId = table.Column<long>(nullable: true),
                    OfficerId = table.Column<long>(nullable: true),
                    AppointmentRangeId = table.Column<long>(nullable: true),
                    TopicId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Appointments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Appointments_AppointmentRanges_AppointmentRangeId",
                        column: x => x.AppointmentRangeId,
                        principalSchema: "public",
                        principalTable: "AppointmentRanges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appointments_Officers_OfficerId",
                        column: x => x.OfficerId,
                        principalSchema: "public",
                        principalTable: "Officers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appointments_Rubrics_RubricId",
                        column: x => x.RubricId,
                        principalSchema: "public",
                        principalTable: "Rubrics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Appointments_Topics_TopicId",
                        column: x => x.TopicId,
                        principalSchema: "public",
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserLikeMessage",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    MessageId = table.Column<long>(nullable: false),
                    Liked = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLikeMessage", x => new { x.UserId, x.MessageId });
                    table.ForeignKey(
                        name: "FK_UserLikeMessage_Messages_MessageId",
                        column: x => x.MessageId,
                        principalSchema: "public",
                        principalTable: "Messages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserLikeMessage_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserViewMessages",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    MessageId = table.Column<long>(nullable: false),
                    Viewed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserViewMessages", x => new { x.UserId, x.MessageId });
                    table.ForeignKey(
                        name: "FK_UserViewMessages_Messages_MessageId",
                        column: x => x.MessageId,
                        principalSchema: "public",
                        principalTable: "Messages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserViewMessages_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AppointmentNotifications",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentId = table.Column<long>(nullable: false),
                    ParentType = table.Column<string>(nullable: true),
                    AppointmentId = table.Column<long>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: true),
                    Html = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppointmentNotifications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppointmentNotifications_Appointments_AppointmentId",
                        column: x => x.AppointmentId,
                        principalSchema: "public",
                        principalTable: "Appointments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AttachedFiles",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    ParentId = table.Column<long>(nullable: false),
                    ParentType = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Path = table.Column<string>(nullable: true),
                    ExtId = table.Column<string>(nullable: true),
                    Private = table.Column<bool>(nullable: false),
                    StorageName = table.Column<string>(nullable: true),
                    StoragePath = table.Column<string>(nullable: true),
                    Len = table.Column<long>(nullable: false),
                    Incomplete = table.Column<bool>(nullable: false),
                    UploadedById = table.Column<long>(nullable: true),
                    Sequre = table.Column<bool>(nullable: false),
                    ContentType = table.Column<string>(nullable: true),
                    AppealId = table.Column<long>(nullable: true),
                    AppointmentId = table.Column<long>(nullable: true),
                    MessageId = table.Column<long>(nullable: true),
                    SettingsKey = table.Column<string>(nullable: true),
                    VoteItemId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AttachedFiles_Appeals_AppealId",
                        column: x => x.AppealId,
                        principalSchema: "public",
                        principalTable: "Appeals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AttachedFiles_Appointments_AppointmentId",
                        column: x => x.AppointmentId,
                        principalSchema: "public",
                        principalTable: "Appointments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AttachedFiles_Messages_MessageId",
                        column: x => x.MessageId,
                        principalSchema: "public",
                        principalTable: "Messages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AttachedFiles_Settings_SettingsKey",
                        column: x => x.SettingsKey,
                        principalSchema: "public",
                        principalTable: "Settings",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_AttachedFiles_AspNetUsers_UploadedById",
                        column: x => x.UploadedById,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AttachedFiles_VoteItems_VoteItemId",
                        column: x => x.VoteItemId,
                        principalSchema: "public",
                        principalTable: "VoteItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProtocolRecords",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    UserId = table.Column<long>(nullable: true),
                    AppealId = table.Column<long>(nullable: true),
                    AppointmentId = table.Column<long>(nullable: true),
                    OfficerId = table.Column<long>(nullable: true),
                    AppointmentRangeId = table.Column<long>(nullable: true),
                    RubricId = table.Column<long>(nullable: true),
                    RegionId = table.Column<long>(nullable: true),
                    TerritoryId = table.Column<long>(nullable: true),
                    TopicId = table.Column<long>(nullable: true),
                    ExtId = table.Column<string>(nullable: true),
                    ExtDate = table.Column<DateTime>(nullable: true),
                    ExtNumber = table.Column<string>(nullable: true),
                    Action = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: true),
                    SiteStatus = table.Column<int>(nullable: true),
                    Public = table.Column<bool>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    Rejected = table.Column<bool>(nullable: true),
                    RejectionReason = table.Column<string>(nullable: true),
                    Data = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProtocolRecords", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProtocolRecords_Appeals_AppealId",
                        column: x => x.AppealId,
                        principalSchema: "public",
                        principalTable: "Appeals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProtocolRecords_Appointments_AppointmentId",
                        column: x => x.AppointmentId,
                        principalSchema: "public",
                        principalTable: "Appointments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProtocolRecords_AppointmentRanges_AppointmentRangeId",
                        column: x => x.AppointmentRangeId,
                        principalSchema: "public",
                        principalTable: "AppointmentRanges",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProtocolRecords_Officers_OfficerId",
                        column: x => x.OfficerId,
                        principalSchema: "public",
                        principalTable: "Officers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProtocolRecords_Regions_RegionId",
                        column: x => x.RegionId,
                        principalSchema: "public",
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProtocolRecords_Rubrics_RubricId",
                        column: x => x.RubricId,
                        principalSchema: "public",
                        principalTable: "Rubrics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProtocolRecords_Territories_TerritoryId",
                        column: x => x.TerritoryId,
                        principalSchema: "public",
                        principalTable: "Territories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProtocolRecords_Topics_TopicId",
                        column: x => x.TopicId,
                        principalSchema: "public",
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProtocolRecords_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_UserId",
                schema: "public",
                table: "Addresses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_UserOrganizationId",
                schema: "public",
                table: "Addresses",
                column: "UserOrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_Appeal2Executor_AppealId",
                schema: "public",
                table: "Appeal2Executor",
                column: "AppealId");

            migrationBuilder.CreateIndex(
                name: "IX_Appeal2PrincipalExecutor_AppealId",
                schema: "public",
                table: "Appeal2PrincipalExecutor",
                column: "AppealId");

            migrationBuilder.CreateIndex(
                name: "IX_AppealExecutors_UserId",
                schema: "public",
                table: "AppealExecutors",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_AuthorId",
                schema: "public",
                table: "Appeals",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_OrganizationId",
                schema: "public",
                table: "Appeals",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_RegionId",
                schema: "public",
                table: "Appeals",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_RubricId",
                schema: "public",
                table: "Appeals",
                column: "RubricId");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_SubsystemId1",
                schema: "public",
                table: "Appeals",
                column: "SubsystemId1");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_TerritoryId",
                schema: "public",
                table: "Appeals",
                column: "TerritoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_TopicId",
                schema: "public",
                table: "Appeals",
                column: "TopicId");

            migrationBuilder.CreateIndex(
                name: "IX_AppointmentNotifications_AppointmentId",
                schema: "public",
                table: "AppointmentNotifications",
                column: "AppointmentId");

            migrationBuilder.CreateIndex(
                name: "IX_AppointmentRanges_OfficerId",
                schema: "public",
                table: "AppointmentRanges",
                column: "OfficerId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_AppointmentRangeId",
                schema: "public",
                table: "Appointments",
                column: "AppointmentRangeId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_OfficerId",
                schema: "public",
                table: "Appointments",
                column: "OfficerId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_RubricId",
                schema: "public",
                table: "Appointments",
                column: "RubricId");

            migrationBuilder.CreateIndex(
                name: "IX_Appointments_TopicId",
                schema: "public",
                table: "Appointments",
                column: "TopicId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                schema: "public",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                schema: "public",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoles_Weight",
                schema: "public",
                table: "AspNetRoles",
                column: "Weight",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                schema: "public",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                schema: "public",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                schema: "public",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_NormalizeFullName",
                schema: "public",
                table: "AspNetUsers",
                column: "NormalizeFullName");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                schema: "public",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                schema: "public",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_UserName",
                schema: "public",
                table: "AspNetUsers",
                column: "UserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_AppealId",
                schema: "public",
                table: "AttachedFiles",
                column: "AppealId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_AppointmentId",
                schema: "public",
                table: "AttachedFiles",
                column: "AppointmentId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_MessageId",
                schema: "public",
                table: "AttachedFiles",
                column: "MessageId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_SettingsKey",
                schema: "public",
                table: "AttachedFiles",
                column: "SettingsKey");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_UploadedById",
                schema: "public",
                table: "AttachedFiles",
                column: "UploadedById");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_VoteItemId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteItemId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_AppealExecutorId",
                schema: "public",
                table: "Messages",
                column: "AppealExecutorId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_AuthorId",
                schema: "public",
                table: "Messages",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_RootAppealId",
                schema: "public",
                table: "Messages",
                column: "RootAppealId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_RootVoteId",
                schema: "public",
                table: "Messages",
                column: "RootVoteId");

            migrationBuilder.CreateIndex(
                name: "IX_Officers_RubricId",
                schema: "public",
                table: "Officers",
                column: "RubricId");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationUserPhones_OrganizationId",
                schema: "public",
                table: "OrganizationUserPhones",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationUserPhones_PhoneId",
                schema: "public",
                table: "OrganizationUserPhones",
                column: "PhoneId");

            migrationBuilder.CreateIndex(
                name: "IX_OrganizationUserPhones_UserId",
                schema: "public",
                table: "OrganizationUserPhones",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_AppealId",
                schema: "public",
                table: "ProtocolRecords",
                column: "AppealId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_AppointmentId",
                schema: "public",
                table: "ProtocolRecords",
                column: "AppointmentId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_AppointmentRangeId",
                schema: "public",
                table: "ProtocolRecords",
                column: "AppointmentRangeId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_OfficerId",
                schema: "public",
                table: "ProtocolRecords",
                column: "OfficerId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_RegionId",
                schema: "public",
                table: "ProtocolRecords",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_RubricId",
                schema: "public",
                table: "ProtocolRecords",
                column: "RubricId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_TerritoryId",
                schema: "public",
                table: "ProtocolRecords",
                column: "TerritoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_TopicId",
                schema: "public",
                table: "ProtocolRecords",
                column: "TopicId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_UserId",
                schema: "public",
                table: "ProtocolRecords",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Regions_Due",
                schema: "public",
                table: "Regions",
                column: "Due");

            migrationBuilder.CreateIndex(
                name: "IX_Regions_Id",
                schema: "public",
                table: "Regions",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Rubrics_Due",
                schema: "public",
                table: "Rubrics",
                column: "Due");

            migrationBuilder.CreateIndex(
                name: "IX_Rubrics_Id",
                schema: "public",
                table: "Rubrics",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_Key",
                schema: "public",
                table: "Settings",
                column: "Key");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_Tag1",
                schema: "public",
                table: "Settings",
                column: "Tag1");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_Tag2",
                schema: "public",
                table: "Settings",
                column: "Tag2");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_Tag3",
                schema: "public",
                table: "Settings",
                column: "Tag3");

            migrationBuilder.CreateIndex(
                name: "IX_Territories_Due",
                schema: "public",
                table: "Territories",
                column: "Due");

            migrationBuilder.CreateIndex(
                name: "IX_Territories_Id",
                schema: "public",
                table: "Territories",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Topics_Due",
                schema: "public",
                table: "Topics",
                column: "Due");

            migrationBuilder.CreateIndex(
                name: "IX_Topics_Id",
                schema: "public",
                table: "Topics",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_User2Organization_UserOrganizationId",
                schema: "public",
                table: "User2Organization",
                column: "UserOrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_UserIdentityDocuments_UserId",
                schema: "public",
                table: "UserIdentityDocuments",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLikeAppeals_AppealId",
                schema: "public",
                table: "UserLikeAppeals",
                column: "AppealId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLikeMessage_MessageId",
                schema: "public",
                table: "UserLikeMessage",
                column: "MessageId");

            migrationBuilder.CreateIndex(
                name: "IX_UserModeratorAppeals_AppealId",
                schema: "public",
                table: "UserModeratorAppeals",
                column: "AppealId");

            migrationBuilder.CreateIndex(
                name: "IX_UserModeratorTopics_TopicId",
                schema: "public",
                table: "UserModeratorTopics",
                column: "TopicId");

            migrationBuilder.CreateIndex(
                name: "IX_UserOrganizations_Inn",
                schema: "public",
                table: "UserOrganizations",
                column: "Inn");

            migrationBuilder.CreateIndex(
                name: "IX_UserOrganizations_Kpp",
                schema: "public",
                table: "UserOrganizations",
                column: "Kpp");

            migrationBuilder.CreateIndex(
                name: "IX_UserOrganizations_NormalizeFullName",
                schema: "public",
                table: "UserOrganizations",
                column: "NormalizeFullName");

            migrationBuilder.CreateIndex(
                name: "IX_UserOrganizations_NormalizeShortName",
                schema: "public",
                table: "UserOrganizations",
                column: "NormalizeShortName");

            migrationBuilder.CreateIndex(
                name: "IX_UserOrganizations_Ogrn",
                schema: "public",
                table: "UserOrganizations",
                column: "Ogrn");

            migrationBuilder.CreateIndex(
                name: "IX_UserOrganizations_ParentId",
                schema: "public",
                table: "UserOrganizations",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRegions_RegionId",
                schema: "public",
                table: "UserRegions",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRubrics_RubricId",
                schema: "public",
                table: "UserRubrics",
                column: "RubricId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTerritories_TerritoryId",
                schema: "public",
                table: "UserTerritories",
                column: "TerritoryId");

            migrationBuilder.CreateIndex(
                name: "IX_UserViewMessages_MessageId",
                schema: "public",
                table: "UserViewMessages",
                column: "MessageId");

            migrationBuilder.CreateIndex(
                name: "IX_UserVotes_VoteId",
                schema: "public",
                table: "UserVotes",
                column: "VoteId");

            migrationBuilder.CreateIndex(
                name: "IX_VoteItems_VoteId",
                schema: "public",
                table: "VoteItems",
                column: "VoteId");

            migrationBuilder.CreateIndex(
                name: "IX_Votes_SubsystemId1",
                schema: "public",
                table: "Votes",
                column: "SubsystemId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Appeal2Executor",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Appeal2PrincipalExecutor",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AppointmentNotifications",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AttachedFiles",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Emails",
                schema: "public");

            migrationBuilder.DropTable(
                name: "EmailTemplates",
                schema: "public");

            migrationBuilder.DropTable(
                name: "OrganizationUserPhones",
                schema: "public");

            migrationBuilder.DropTable(
                name: "ProtocolRecords",
                schema: "public");

            migrationBuilder.DropTable(
                name: "User2Organization",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserIdentityDocuments",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserLikeAppeals",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserLikeMessage",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserModeratorAppeals",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserModeratorTopics",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserRegions",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserRubrics",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserTerritories",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserViewMessages",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserVotes",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetRoles",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Settings",
                schema: "public");

            migrationBuilder.DropTable(
                name: "VoteItems",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Phones",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Appointments",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Messages",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AppointmentRanges",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AppealExecutors",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Appeals",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Votes",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Officers",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AspNetUsers",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserOrganizations",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Regions",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Territories",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Topics",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Subsystems",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Rubrics",
                schema: "public");
        }
    }
}
