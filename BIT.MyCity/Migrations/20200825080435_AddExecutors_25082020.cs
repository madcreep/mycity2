﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class AddExecutors_25082020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ModeratorId",
                schema: "public",
                table: "Subsystems",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ExecutorRegions",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    SubsystemId = table.Column<long>(nullable: false),
                    RegionId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutorRegions", x => new { x.UserId, x.RegionId, x.SubsystemId });
                    table.ForeignKey(
                        name: "FK_ExecutorRegions_Regions_RegionId",
                        column: x => x.RegionId,
                        principalSchema: "public",
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExecutorRegions_Subsystems_SubsystemId",
                        column: x => x.SubsystemId,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExecutorRegions_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExecutorRubrics",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    SubsystemId = table.Column<long>(nullable: false),
                    RubricId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutorRubrics", x => new { x.UserId, x.RubricId, x.SubsystemId });
                    table.ForeignKey(
                        name: "FK_ExecutorRubrics_Rubrics_RubricId",
                        column: x => x.RubricId,
                        principalSchema: "public",
                        principalTable: "Rubrics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExecutorRubrics_Subsystems_SubsystemId",
                        column: x => x.SubsystemId,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExecutorRubrics_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExecutorTerritories",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    SubsystemId = table.Column<long>(nullable: false),
                    TerritoryId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutorTerritories", x => new { x.UserId, x.TerritoryId, x.SubsystemId });
                    table.ForeignKey(
                        name: "FK_ExecutorTerritories_Subsystems_SubsystemId",
                        column: x => x.SubsystemId,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExecutorTerritories_Territories_TerritoryId",
                        column: x => x.TerritoryId,
                        principalSchema: "public",
                        principalTable: "Territories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExecutorTerritories_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExecutorTopics",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    SubsystemId = table.Column<long>(nullable: false),
                    TopicId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutorTopics", x => new { x.UserId, x.TopicId, x.SubsystemId });
                    table.ForeignKey(
                        name: "FK_ExecutorTopics_Subsystems_SubsystemId",
                        column: x => x.SubsystemId,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExecutorTopics_Topics_TopicId",
                        column: x => x.TopicId,
                        principalSchema: "public",
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ExecutorTopics_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Subsystems_ModeratorId",
                schema: "public",
                table: "Subsystems",
                column: "ModeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutorRegions_RegionId",
                schema: "public",
                table: "ExecutorRegions",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutorRegions_SubsystemId",
                schema: "public",
                table: "ExecutorRegions",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutorRubrics_RubricId",
                schema: "public",
                table: "ExecutorRubrics",
                column: "RubricId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutorRubrics_SubsystemId",
                schema: "public",
                table: "ExecutorRubrics",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutorTerritories_SubsystemId",
                schema: "public",
                table: "ExecutorTerritories",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutorTerritories_TerritoryId",
                schema: "public",
                table: "ExecutorTerritories",
                column: "TerritoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutorTopics_SubsystemId",
                schema: "public",
                table: "ExecutorTopics",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutorTopics_TopicId",
                schema: "public",
                table: "ExecutorTopics",
                column: "TopicId");

            migrationBuilder.AddForeignKey(
                name: "FK_Subsystems_AspNetUsers_ModeratorId",
                schema: "public",
                table: "Subsystems",
                column: "ModeratorId",
                principalSchema: "public",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subsystems_AspNetUsers_ModeratorId",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropTable(
                name: "ExecutorRegions",
                schema: "public");

            migrationBuilder.DropTable(
                name: "ExecutorRubrics",
                schema: "public");

            migrationBuilder.DropTable(
                name: "ExecutorTerritories",
                schema: "public");

            migrationBuilder.DropTable(
                name: "ExecutorTopics",
                schema: "public");

            migrationBuilder.DropIndex(
                name: "IX_Subsystems_ModeratorId",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "ModeratorId",
                schema: "public",
                table: "Subsystems");
        }
    }
}
