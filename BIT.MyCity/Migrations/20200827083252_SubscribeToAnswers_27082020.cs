﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class SubscribeToAnswers_27082020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubscribeToAnswers",
                schema: "public",
                table: "VoteRoots",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubscribeToAnswers",
                schema: "public",
                table: "News",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubscribeToAnswers",
                schema: "public",
                table: "Messages",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubscribeToAnswers",
                schema: "public",
                table: "Appeals",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubscribeToAnswers",
                schema: "public",
                table: "VoteRoots");

            migrationBuilder.DropColumn(
                name: "SubscribeToAnswers",
                schema: "public",
                table: "News");

            migrationBuilder.DropColumn(
                name: "SubscribeToAnswers",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "SubscribeToAnswers",
                schema: "public",
                table: "Appeals");
        }
    }
}
