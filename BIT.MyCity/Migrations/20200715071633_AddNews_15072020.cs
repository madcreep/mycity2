﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BIT.MyCity.Migrations
{
    public partial class AddNews_15072020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "NewsRootId",
                schema: "public",
                table: "Messages",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "NewsId",
                schema: "public",
                table: "AttachedFiles",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "News",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    RubricId = table.Column<long>(nullable: true),
                    TerritoryId = table.Column<long>(nullable: true),
                    TopicId = table.Column<long>(nullable: true),
                    RegionId = table.Column<long>(nullable: true),
                    LikesCount = table.Column<int>(nullable: false),
                    DislikesCount = table.Column<int>(nullable: false),
                    ViewsCount = table.Column<int>(nullable: false),
                    AuthorId = table.Column<long>(nullable: false),
                    CommentsAllowed = table.Column<bool>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_News", x => x.Id);
                    table.ForeignKey(
                        name: "FK_News_AspNetUsers_AuthorId",
                        column: x => x.AuthorId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_News_Regions_RegionId",
                        column: x => x.RegionId,
                        principalSchema: "public",
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_News_Rubrics_RubricId",
                        column: x => x.RubricId,
                        principalSchema: "public",
                        principalTable: "Rubrics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_News_Territories_TerritoryId",
                        column: x => x.TerritoryId,
                        principalSchema: "public",
                        principalTable: "Territories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_News_Topics_TopicId",
                        column: x => x.TopicId,
                        principalSchema: "public",
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserLikeNews",
                schema: "public",
                columns: table => new
                {
                    NewsId = table.Column<long>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    Liked = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLikeNews", x => new { x.UserId, x.NewsId });
                    table.ForeignKey(
                        name: "FK_UserLikeNews_News_NewsId",
                        column: x => x.NewsId,
                        principalSchema: "public",
                        principalTable: "News",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserLikeNews_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserViewNews",
                schema: "public",
                columns: table => new
                {
                    NewsId = table.Column<long>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    Viewed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserViewNews", x => new { x.UserId, x.NewsId });
                    table.ForeignKey(
                        name: "FK_UserViewNews_News_NewsId",
                        column: x => x.NewsId,
                        principalSchema: "public",
                        principalTable: "News",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserViewNews_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Messages_NewsRootId",
                schema: "public",
                table: "Messages",
                column: "NewsRootId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_NewsId",
                schema: "public",
                table: "AttachedFiles",
                column: "NewsId");

            migrationBuilder.CreateIndex(
                name: "IX_News_AuthorId",
                schema: "public",
                table: "News",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_News_RegionId",
                schema: "public",
                table: "News",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_News_RubricId",
                schema: "public",
                table: "News",
                column: "RubricId");

            migrationBuilder.CreateIndex(
                name: "IX_News_TerritoryId",
                schema: "public",
                table: "News",
                column: "TerritoryId");

            migrationBuilder.CreateIndex(
                name: "IX_News_TopicId",
                schema: "public",
                table: "News",
                column: "TopicId");

            migrationBuilder.CreateIndex(
                name: "IX_UserLikeNews_NewsId",
                schema: "public",
                table: "UserLikeNews",
                column: "NewsId");

            migrationBuilder.CreateIndex(
                name: "IX_UserViewNews_NewsId",
                schema: "public",
                table: "UserViewNews",
                column: "NewsId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_News_NewsId",
                schema: "public",
                table: "AttachedFiles",
                column: "NewsId",
                principalSchema: "public",
                principalTable: "News",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_News_NewsRootId",
                schema: "public",
                table: "Messages",
                column: "NewsRootId",
                principalSchema: "public",
                principalTable: "News",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_News_NewsId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Messages_News_NewsRootId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropTable(
                name: "UserLikeNews",
                schema: "public");

            migrationBuilder.DropTable(
                name: "UserViewNews",
                schema: "public");

            migrationBuilder.DropTable(
                name: "News",
                schema: "public");

            migrationBuilder.DropIndex(
                name: "IX_Messages_NewsRootId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_NewsId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "NewsRootId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "NewsId",
                schema: "public",
                table: "AttachedFiles");
        }
    }
}
