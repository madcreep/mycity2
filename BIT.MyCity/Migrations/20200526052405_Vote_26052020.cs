﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class Vote_26052020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "End",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "Start",
                schema: "public",
                table: "Votes");

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchiveAt",
                schema: "public",
                table: "Votes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchivedAt",
                schema: "public",
                table: "Votes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndAt",
                schema: "public",
                table: "Votes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndedAt",
                schema: "public",
                table: "Votes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartAt",
                schema: "public",
                table: "Votes",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartedAt",
                schema: "public",
                table: "Votes",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                schema: "public",
                table: "VoteItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArchiveAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "ArchivedAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "EndAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "EndedAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "StartAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "StartedAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "Description",
                schema: "public",
                table: "VoteItems");

            migrationBuilder.AddColumn<DateTime>(
                name: "End",
                schema: "public",
                table: "Votes",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Start",
                schema: "public",
                table: "Votes",
                type: "timestamp without time zone",
                nullable: true);
        }
    }
}
