﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class VoteChange_23052020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "VoteId",
                schema: "public",
                table: "AttachedFiles",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_VoteId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_Votes_VoteId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteId",
                principalSchema: "public",
                principalTable: "Votes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_Votes_VoteId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_VoteId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "VoteId",
                schema: "public",
                table: "AttachedFiles");
        }
    }
}
