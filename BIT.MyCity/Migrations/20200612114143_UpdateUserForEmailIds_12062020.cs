﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class UpdateUserForEmailIds_12062020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ConfirmEmailMailId",
                schema: "public",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RestorePasswordMailId",
                schema: "public",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConfirmEmailMailId",
                schema: "public",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "RestorePasswordMailId",
                schema: "public",
                table: "AspNetUsers");
        }
    }
}
