﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class Add_OldSystemId_Fields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OldSystemId",
                schema: "public",
                table: "AspNetUsers",
                type: "character varying(256)",
                maxLength: 256,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OldSystemId",
                schema: "public",
                table: "Appeals",
                type: "character varying(256)",
                maxLength: 256,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_ExternalId",
                schema: "public",
                table: "AspNetUsers",
                column: "ExternalId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_OldSystemId",
                schema: "public",
                table: "AspNetUsers",
                column: "OldSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_OldSystemId",
                schema: "public",
                table: "Appeals",
                column: "OldSystemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_ExternalId",
                schema: "public",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_OldSystemId",
                schema: "public",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_Appeals_OldSystemId",
                schema: "public",
                table: "Appeals");

            migrationBuilder.DropColumn(
                name: "OldSystemId",
                schema: "public",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "OldSystemId",
                schema: "public",
                table: "Appeals");
        }
    }
}
