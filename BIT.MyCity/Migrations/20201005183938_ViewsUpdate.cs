﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class ViewsUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                CREATE VIEW ""AppealsView"" AS SELECT 
                    COUNT(""Id"") as ""AppealsCount"", 
                    ""SubsystemId1"" as ""SubsystemId"", 
                    ""RubricId"", 
                    ""TopicId"", 
                    ""TerritoryId"", 
                    ""RegionId"",
                    date_trunc('day', ""CreatedAt"") as ""CreatedAt"", 
                    ""Public_"" as ""Public"", 
                    ""Deleted"",
                    ""SiteStatus"", 
                    ""SentToDelo"",
                    ""Approved""
                FROM ""Appeals""
                GROUP BY ""SubsystemId1"", ""RubricId"", ""TerritoryId"", ""TopicId"", ""RegionId"", date_trunc('day', ""CreatedAt""), ""Public_"", ""Deleted"", ""SiteStatus"", ""SentToDelo"", ""Approved""");
            migrationBuilder.DropColumn(
                name: "AppealPropertyNames",
                schema: "public",
                table: "Subsystems");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW AppealsView");
            migrationBuilder.AddColumn<List<string>>(
                name: "AppealPropertyNames",
                schema: "public",
                table: "Subsystems",
                type: "text[]",
                nullable: true);
        }
    }
}
