using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class AtachedFilesToEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AttachedFilesToAppeals",
                schema: "public",
                columns: table => new
                {
                    AppealId = table.Column<long>(type: "bigint", nullable: false),
                    AttachedFileId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFilesToAppeals", x => new { x.AppealId, x.AttachedFileId });
                    table.ForeignKey(
                        name: "FK_AttachedFilesToAppeals_Appeals_AppealId",
                        column: x => x.AppealId,
                        principalSchema: "public",
                        principalTable: "Appeals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttachedFilesToAppeals_AttachedFiles_AttachedFileId",
                        column: x => x.AttachedFileId,
                        principalSchema: "public",
                        principalTable: "AttachedFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AttachedFilesToAppointments",
                schema: "public",
                columns: table => new
                {
                    AppointmentId = table.Column<long>(type: "bigint", nullable: false),
                    AttachedFileId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFilesToAppointments", x => new { x.AppointmentId, x.AttachedFileId });
                    table.ForeignKey(
                        name: "FK_AttachedFilesToAppointments_Appointments_AppointmentId",
                        column: x => x.AppointmentId,
                        principalSchema: "public",
                        principalTable: "Appointments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttachedFilesToAppointments_AttachedFiles_AttachedFileId",
                        column: x => x.AttachedFileId,
                        principalSchema: "public",
                        principalTable: "AttachedFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AttachedFilesToEmails",
                schema: "public",
                columns: table => new
                {
                    EmailId = table.Column<long>(type: "bigint", nullable: false),
                    AttachedFileId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFilesToEmails", x => new { x.EmailId, x.AttachedFileId });
                    table.ForeignKey(
                        name: "FK_AttachedFilesToEmails_AttachedFiles_AttachedFileId",
                        column: x => x.AttachedFileId,
                        principalSchema: "public",
                        principalTable: "AttachedFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttachedFilesToEmails_Emails_EmailId",
                        column: x => x.EmailId,
                        principalSchema: "public",
                        principalTable: "Emails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AttachedFilesToMessages",
                schema: "public",
                columns: table => new
                {
                    MessageId = table.Column<long>(type: "bigint", nullable: false),
                    AttachedFileId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFilesToMessages", x => new { x.MessageId, x.AttachedFileId });
                    table.ForeignKey(
                        name: "FK_AttachedFilesToMessages_AttachedFiles_AttachedFileId",
                        column: x => x.AttachedFileId,
                        principalSchema: "public",
                        principalTable: "AttachedFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttachedFilesToMessages_Messages_MessageId",
                        column: x => x.MessageId,
                        principalSchema: "public",
                        principalTable: "Messages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AttachedFilesToNews",
                schema: "public",
                columns: table => new
                {
                    NewsId = table.Column<long>(type: "bigint", nullable: false),
                    AttachedFileId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFilesToNews", x => new { x.NewsId, x.AttachedFileId });
                    table.ForeignKey(
                        name: "FK_AttachedFilesToNews_AttachedFiles_AttachedFileId",
                        column: x => x.AttachedFileId,
                        principalSchema: "public",
                        principalTable: "AttachedFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttachedFilesToNews_News_NewsId",
                        column: x => x.NewsId,
                        principalSchema: "public",
                        principalTable: "News",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AttachedFilesToSettings",
                schema: "public",
                columns: table => new
                {
                    SettingsKey = table.Column<string>(type: "text", nullable: false),
                    AttachedFileId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFilesToSettings", x => new { x.SettingsKey, x.AttachedFileId });
                    table.ForeignKey(
                        name: "FK_AttachedFilesToSettings_AttachedFiles_AttachedFileId",
                        column: x => x.AttachedFileId,
                        principalSchema: "public",
                        principalTable: "AttachedFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttachedFilesToSettings_Settings_SettingsKey",
                        column: x => x.SettingsKey,
                        principalSchema: "public",
                        principalTable: "Settings",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AttachedFilesToVoteItems",
                schema: "public",
                columns: table => new
                {
                    VoteItemId = table.Column<long>(type: "bigint", nullable: false),
                    AttachedFileId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFilesToVoteItems", x => new { x.VoteItemId, x.AttachedFileId });
                    table.ForeignKey(
                        name: "FK_AttachedFilesToVoteItems_AttachedFiles_AttachedFileId",
                        column: x => x.AttachedFileId,
                        principalSchema: "public",
                        principalTable: "AttachedFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttachedFilesToVoteItems_VoteItems_VoteItemId",
                        column: x => x.VoteItemId,
                        principalSchema: "public",
                        principalTable: "VoteItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AttachedFilesToVoteRoots",
                schema: "public",
                columns: table => new
                {
                    VoteRootId = table.Column<long>(type: "bigint", nullable: false),
                    AttachedFileId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFilesToVoteRoots", x => new { x.VoteRootId, x.AttachedFileId });
                    table.ForeignKey(
                        name: "FK_AttachedFilesToVoteRoots_AttachedFiles_AttachedFileId",
                        column: x => x.AttachedFileId,
                        principalSchema: "public",
                        principalTable: "AttachedFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttachedFilesToVoteRoots_VoteRoots_VoteRootId",
                        column: x => x.VoteRootId,
                        principalSchema: "public",
                        principalTable: "VoteRoots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AttachedFilesToVotes",
                schema: "public",
                columns: table => new
                {
                    VoteId = table.Column<long>(type: "bigint", nullable: false),
                    AttachedFileId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachedFilesToVotes", x => new { x.VoteId, x.AttachedFileId });
                    table.ForeignKey(
                        name: "FK_AttachedFilesToVotes_AttachedFiles_AttachedFileId",
                        column: x => x.AttachedFileId,
                        principalSchema: "public",
                        principalTable: "AttachedFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AttachedFilesToVotes_Votes_VoteId",
                        column: x => x.VoteId,
                        principalSchema: "public",
                        principalTable: "Votes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFilesToAppeals_AttachedFileId",
                schema: "public",
                table: "AttachedFilesToAppeals",
                column: "AttachedFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFilesToAppointments_AttachedFileId",
                schema: "public",
                table: "AttachedFilesToAppointments",
                column: "AttachedFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFilesToEmails_AttachedFileId",
                schema: "public",
                table: "AttachedFilesToEmails",
                column: "AttachedFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFilesToMessages_AttachedFileId",
                schema: "public",
                table: "AttachedFilesToMessages",
                column: "AttachedFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFilesToNews_AttachedFileId",
                schema: "public",
                table: "AttachedFilesToNews",
                column: "AttachedFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFilesToSettings_AttachedFileId",
                schema: "public",
                table: "AttachedFilesToSettings",
                column: "AttachedFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFilesToVoteItems_AttachedFileId",
                schema: "public",
                table: "AttachedFilesToVoteItems",
                column: "AttachedFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFilesToVoteRoots_AttachedFileId",
                schema: "public",
                table: "AttachedFilesToVoteRoots",
                column: "AttachedFileId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFilesToVotes_AttachedFileId",
                schema: "public",
                table: "AttachedFilesToVotes",
                column: "AttachedFileId");

            migrationBuilder.Sql(@"
INSERT INTO public.""AttachedFilesToAppeals""(
	""AppealId"", ""AttachedFileId"")
SELECT ent.""Id"", af.""Id""
FROM public.""AttachedFiles"" af
JOIN public.""Appeals"" ent ON af.""AppealId"" = ent.""Id"";

INSERT INTO public.""AttachedFilesToAppointments""(
	""AppointmentId"", ""AttachedFileId"")
SELECT ent.""Id"", af.""Id""
FROM public.""AttachedFiles"" af
JOIN public.""Appointments"" ent ON af.""AppointmentId"" = ent.""Id"";

INSERT INTO public.""AttachedFilesToMessages""(
	""MessageId"", ""AttachedFileId"")
SELECT ent.""Id"", af.""Id""
FROM public.""AttachedFiles"" af
JOIN public.""Messages"" ent ON af.""MessageId"" = ent.""Id"";

INSERT INTO public.""AttachedFilesToVoteItems""(
	""VoteItemId"", ""AttachedFileId"")
SELECT ent.""Id"", af.""Id""
FROM public.""AttachedFiles"" af
JOIN public.""VoteItems"" ent ON af.""VoteItemId"" = ent.""Id"";

INSERT INTO public.""AttachedFilesToVotes""(
	""VoteId"", ""AttachedFileId"")
SELECT ent.""Id"", af.""Id""
FROM public.""AttachedFiles"" af
JOIN public.""Votes"" ent ON af.""VoteId"" = ent.""Id"";

INSERT INTO public.""AttachedFilesToVoteRoots""(
	""VoteRootId"", ""AttachedFileId"")
SELECT ent.""Id"", af.""Id""
FROM public.""AttachedFiles"" af
JOIN public.""VoteRoots"" ent ON af.""VoteRootId"" = ent.""Id"";

INSERT INTO public.""AttachedFilesToNews""(
	""NewsId"", ""AttachedFileId"")
SELECT ent.""Id"", af.""Id""
FROM public.""AttachedFiles"" af
JOIN public.""News"" ent ON af.""NewsId"" = ent.""Id"";
");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_Appeals_AppealId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_Appointments_AppointmentId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_Emails_EmailId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_Messages_MessageId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_News_NewsId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_VoteItems_VoteItemId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_VoteRoots_VoteRootId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_Votes_VoteId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_AppealId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_AppointmentId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_EmailId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_MessageId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_NewsId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_VoteId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_VoteItemId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_VoteRootId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "AppealId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "AppointmentId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "EmailId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "MessageId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "NewsId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "VoteId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "VoteItemId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "VoteRootId",
                schema: "public",
                table: "AttachedFiles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AttachedFilesToAppeals",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AttachedFilesToAppointments",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AttachedFilesToEmails",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AttachedFilesToMessages",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AttachedFilesToNews",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AttachedFilesToSettings",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AttachedFilesToVoteItems",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AttachedFilesToVoteRoots",
                schema: "public");

            migrationBuilder.DropTable(
                name: "AttachedFilesToVotes",
                schema: "public");

            migrationBuilder.AddColumn<long>(
                name: "AppealId",
                schema: "public",
                table: "AttachedFiles",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "AppointmentId",
                schema: "public",
                table: "AttachedFiles",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "EmailId",
                schema: "public",
                table: "AttachedFiles",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MessageId",
                schema: "public",
                table: "AttachedFiles",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "NewsId",
                schema: "public",
                table: "AttachedFiles",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "VoteId",
                schema: "public",
                table: "AttachedFiles",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "VoteItemId",
                schema: "public",
                table: "AttachedFiles",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "VoteRootId",
                schema: "public",
                table: "AttachedFiles",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_AppealId",
                schema: "public",
                table: "AttachedFiles",
                column: "AppealId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_AppointmentId",
                schema: "public",
                table: "AttachedFiles",
                column: "AppointmentId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_EmailId",
                schema: "public",
                table: "AttachedFiles",
                column: "EmailId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_MessageId",
                schema: "public",
                table: "AttachedFiles",
                column: "MessageId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_NewsId",
                schema: "public",
                table: "AttachedFiles",
                column: "NewsId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_VoteId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_VoteItemId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteItemId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_VoteRootId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteRootId");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_Appeals_AppealId",
                schema: "public",
                table: "AttachedFiles",
                column: "AppealId",
                principalSchema: "public",
                principalTable: "Appeals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_Appointments_AppointmentId",
                schema: "public",
                table: "AttachedFiles",
                column: "AppointmentId",
                principalSchema: "public",
                principalTable: "Appointments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_Emails_EmailId",
                schema: "public",
                table: "AttachedFiles",
                column: "EmailId",
                principalSchema: "public",
                principalTable: "Emails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_Messages_MessageId",
                schema: "public",
                table: "AttachedFiles",
                column: "MessageId",
                principalSchema: "public",
                principalTable: "Messages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_News_NewsId",
                schema: "public",
                table: "AttachedFiles",
                column: "NewsId",
                principalSchema: "public",
                principalTable: "News",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_VoteItems_VoteItemId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteItemId",
                principalSchema: "public",
                principalTable: "VoteItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_VoteRoots_VoteRootId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteRootId",
                principalSchema: "public",
                principalTable: "VoteRoots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_Votes_VoteId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteId",
                principalSchema: "public",
                principalTable: "Votes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
