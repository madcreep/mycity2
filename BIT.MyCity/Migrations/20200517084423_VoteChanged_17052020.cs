﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class VoteChanged_17052020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "NextVoteId",
                schema: "public",
                table: "VoteItems",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RootVoteId",
                schema: "public",
                table: "VoteItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VoteItems_NextVoteId",
                schema: "public",
                table: "VoteItems",
                column: "NextVoteId");

            migrationBuilder.CreateIndex(
                name: "IX_VoteItems_RootVoteId",
                schema: "public",
                table: "VoteItems",
                column: "RootVoteId");

            migrationBuilder.AddForeignKey(
                name: "FK_VoteItems_Votes_NextVoteId",
                schema: "public",
                table: "VoteItems",
                column: "NextVoteId",
                principalSchema: "public",
                principalTable: "Votes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VoteItems_Votes_RootVoteId",
                schema: "public",
                table: "VoteItems",
                column: "RootVoteId",
                principalSchema: "public",
                principalTable: "Votes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VoteItems_Votes_NextVoteId",
                schema: "public",
                table: "VoteItems");

            migrationBuilder.DropForeignKey(
                name: "FK_VoteItems_Votes_RootVoteId",
                schema: "public",
                table: "VoteItems");

            migrationBuilder.DropIndex(
                name: "IX_VoteItems_NextVoteId",
                schema: "public",
                table: "VoteItems");

            migrationBuilder.DropIndex(
                name: "IX_VoteItems_RootVoteId",
                schema: "public",
                table: "VoteItems");

            migrationBuilder.DropColumn(
                name: "NextVoteId",
                schema: "public",
                table: "VoteItems");

            migrationBuilder.DropColumn(
                name: "RootVoteId",
                schema: "public",
                table: "VoteItems");
        }
    }
}
