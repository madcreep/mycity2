﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class RemoveUnusedLincs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_Settings_SettingsKey",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_SettingsKey",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "SettingsKey",
                schema: "public",
                table: "AttachedFiles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SettingsKey",
                schema: "public",
                table: "AttachedFiles",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_SettingsKey",
                schema: "public",
                table: "AttachedFiles",
                column: "SettingsKey");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_Settings_SettingsKey",
                schema: "public",
                table: "AttachedFiles",
                column: "SettingsKey",
                principalSchema: "public",
                principalTable: "Settings",
                principalColumn: "Key",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
