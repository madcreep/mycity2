﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class AppealDeleteGeo_24072020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Latitude",
                schema: "public",
                table: "Appeals");

            migrationBuilder.DropColumn(
                name: "Longitude",
                schema: "public",
                table: "Appeals");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Latitude",
                schema: "public",
                table: "Appeals",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Longitude",
                schema: "public",
                table: "Appeals",
                type: "text",
                nullable: true);
        }
    }
}
