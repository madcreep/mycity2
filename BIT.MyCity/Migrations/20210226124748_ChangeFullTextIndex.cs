﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NpgsqlTypes;

namespace BIT.MyCity.Migrations
{
    public partial class ChangeFullTextIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<NpgsqlTsVector>(
                name: "SearchVector",
                schema: "public",
                table: "Appeals",
                type: "tsvector",
                nullable: true)
                .Annotation("Npgsql:TsVectorConfig", "russian")
                .Annotation("Npgsql:TsVectorProperties", new[] { "Title", "Description" });

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_SearchVector",
                schema: "public",
                table: "Appeals",
                column: "SearchVector")
                .Annotation("Npgsql:IndexMethod", "GIN");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Appeals_SearchVector",
                schema: "public",
                table: "Appeals");

            migrationBuilder.DropColumn(
                name: "SearchVector",
                schema: "public",
                table: "Appeals");

            migrationBuilder.CreateIndex(
                name: "IX_Appeals_Title_Description",
                schema: "public",
                table: "Appeals",
                columns: new[] { "Title", "Description" })
                .Annotation("Npgsql:TsVectorConfig", "russian");
        }
    }
}
