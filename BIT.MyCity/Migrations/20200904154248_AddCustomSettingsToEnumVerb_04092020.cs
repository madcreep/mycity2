﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class AddCustomSettingsToEnumVerb_04092020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CustomSettings",
                schema: "public",
                table: "EnumVerbs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomSettings",
                schema: "public",
                table: "EnumVerbs");
        }
    }
}
