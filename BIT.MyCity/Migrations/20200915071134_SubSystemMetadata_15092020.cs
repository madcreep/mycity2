﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class SubSystemMetadata_15092020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPublicDefault",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "MainEntityName",
                schema: "public",
                table: "Subsystems",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PrivateOnly",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseAttaches",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseComments",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseDislikes",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseLikesForComments",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseLikesForMain",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseMaps",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseRates",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseRegionsDescription",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseRubricsDescription",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseTerritoriesDescription",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "UseTopicsDescription",
                schema: "public",
                table: "Subsystems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "SubsystemMainEntityFields",
                schema: "public",
                columns: table => new
                {
                    NameAPI = table.Column<string>(nullable: false),
                    SubsystemId = table.Column<long>(nullable: false),
                    NameDisplayed = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    NameAPISecond = table.Column<string>(nullable: true),
                    Required = table.Column<bool>(nullable: false),
                    Weight = table.Column<int>(nullable: false),
                    Placeholder = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubsystemMainEntityFields", x => new { x.SubsystemId, x.NameAPI });
                    table.ForeignKey(
                        name: "FK_SubsystemMainEntityFields_Subsystems_SubsystemId",
                        column: x => x.SubsystemId,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubsystemMainEntityFields",
                schema: "public");

            migrationBuilder.DropColumn(
                name: "IsPublicDefault",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "MainEntityName",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "PrivateOnly",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseAttaches",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseComments",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseDislikes",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseLikesForComments",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseLikesForMain",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseMaps",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseRates",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseRegionsDescription",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseRubricsDescription",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseTerritoriesDescription",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "UseTopicsDescription",
                schema: "public",
                table: "Subsystems");
        }
    }
}
