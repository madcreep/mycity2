﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class AddAddDirectorySubsystemBindingTables_27052020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Region2Subsystem_Regions_RegionId",
                schema: "public",
                table: "Region2Subsystem");

            migrationBuilder.DropForeignKey(
                name: "FK_Region2Subsystem_Subsystems_SubsystemId",
                schema: "public",
                table: "Region2Subsystem");

            migrationBuilder.DropForeignKey(
                name: "FK_Rubric2Subsystem_Rubrics_RubricId",
                schema: "public",
                table: "Rubric2Subsystem");

            migrationBuilder.DropForeignKey(
                name: "FK_Rubric2Subsystem_Subsystems_SubsystemId",
                schema: "public",
                table: "Rubric2Subsystem");

            migrationBuilder.DropForeignKey(
                name: "FK_Territory2Subsystem_Subsystems_SubsystemId",
                schema: "public",
                table: "Territory2Subsystem");

            migrationBuilder.DropForeignKey(
                name: "FK_Territory2Subsystem_Territories_TerritoryId",
                schema: "public",
                table: "Territory2Subsystem");

            migrationBuilder.DropForeignKey(
                name: "FK_Topic2Subsystem_Subsystems_SubsystemId",
                schema: "public",
                table: "Topic2Subsystem");

            migrationBuilder.DropForeignKey(
                name: "FK_Topic2Subsystem_Topics_TopicId",
                schema: "public",
                table: "Topic2Subsystem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Topic2Subsystem",
                schema: "public",
                table: "Topic2Subsystem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Territory2Subsystem",
                schema: "public",
                table: "Territory2Subsystem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Rubric2Subsystem",
                schema: "public",
                table: "Rubric2Subsystem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Region2Subsystem",
                schema: "public",
                table: "Region2Subsystem");

            migrationBuilder.RenameTable(
                name: "Topic2Subsystem",
                schema: "public",
                newName: "Topic2Subsystems",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "Territory2Subsystem",
                schema: "public",
                newName: "Territory2Subsystems",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "Rubric2Subsystem",
                schema: "public",
                newName: "Rubric2Subsystems",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "Region2Subsystem",
                schema: "public",
                newName: "Region2Subsystems",
                newSchema: "public");

            migrationBuilder.RenameIndex(
                name: "IX_Topic2Subsystem_SubsystemId",
                schema: "public",
                table: "Topic2Subsystems",
                newName: "IX_Topic2Subsystems_SubsystemId");

            migrationBuilder.RenameIndex(
                name: "IX_Territory2Subsystem_SubsystemId",
                schema: "public",
                table: "Territory2Subsystems",
                newName: "IX_Territory2Subsystems_SubsystemId");

            migrationBuilder.RenameIndex(
                name: "IX_Rubric2Subsystem_SubsystemId",
                schema: "public",
                table: "Rubric2Subsystems",
                newName: "IX_Rubric2Subsystems_SubsystemId");

            migrationBuilder.RenameIndex(
                name: "IX_Region2Subsystem_SubsystemId",
                schema: "public",
                table: "Region2Subsystems",
                newName: "IX_Region2Subsystems_SubsystemId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Topic2Subsystems",
                schema: "public",
                table: "Topic2Subsystems",
                columns: new[] { "TopicId", "SubsystemId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Territory2Subsystems",
                schema: "public",
                table: "Territory2Subsystems",
                columns: new[] { "TerritoryId", "SubsystemId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Rubric2Subsystems",
                schema: "public",
                table: "Rubric2Subsystems",
                columns: new[] { "RubricId", "SubsystemId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Region2Subsystems",
                schema: "public",
                table: "Region2Subsystems",
                columns: new[] { "RegionId", "SubsystemId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Region2Subsystems_Regions_RegionId",
                schema: "public",
                table: "Region2Subsystems",
                column: "RegionId",
                principalSchema: "public",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Region2Subsystems_Subsystems_SubsystemId",
                schema: "public",
                table: "Region2Subsystems",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rubric2Subsystems_Rubrics_RubricId",
                schema: "public",
                table: "Rubric2Subsystems",
                column: "RubricId",
                principalSchema: "public",
                principalTable: "Rubrics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rubric2Subsystems_Subsystems_SubsystemId",
                schema: "public",
                table: "Rubric2Subsystems",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Territory2Subsystems_Subsystems_SubsystemId",
                schema: "public",
                table: "Territory2Subsystems",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Territory2Subsystems_Territories_TerritoryId",
                schema: "public",
                table: "Territory2Subsystems",
                column: "TerritoryId",
                principalSchema: "public",
                principalTable: "Territories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Topic2Subsystems_Subsystems_SubsystemId",
                schema: "public",
                table: "Topic2Subsystems",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Topic2Subsystems_Topics_TopicId",
                schema: "public",
                table: "Topic2Subsystems",
                column: "TopicId",
                principalSchema: "public",
                principalTable: "Topics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Region2Subsystems_Regions_RegionId",
                schema: "public",
                table: "Region2Subsystems");

            migrationBuilder.DropForeignKey(
                name: "FK_Region2Subsystems_Subsystems_SubsystemId",
                schema: "public",
                table: "Region2Subsystems");

            migrationBuilder.DropForeignKey(
                name: "FK_Rubric2Subsystems_Rubrics_RubricId",
                schema: "public",
                table: "Rubric2Subsystems");

            migrationBuilder.DropForeignKey(
                name: "FK_Rubric2Subsystems_Subsystems_SubsystemId",
                schema: "public",
                table: "Rubric2Subsystems");

            migrationBuilder.DropForeignKey(
                name: "FK_Territory2Subsystems_Subsystems_SubsystemId",
                schema: "public",
                table: "Territory2Subsystems");

            migrationBuilder.DropForeignKey(
                name: "FK_Territory2Subsystems_Territories_TerritoryId",
                schema: "public",
                table: "Territory2Subsystems");

            migrationBuilder.DropForeignKey(
                name: "FK_Topic2Subsystems_Subsystems_SubsystemId",
                schema: "public",
                table: "Topic2Subsystems");

            migrationBuilder.DropForeignKey(
                name: "FK_Topic2Subsystems_Topics_TopicId",
                schema: "public",
                table: "Topic2Subsystems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Topic2Subsystems",
                schema: "public",
                table: "Topic2Subsystems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Territory2Subsystems",
                schema: "public",
                table: "Territory2Subsystems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Rubric2Subsystems",
                schema: "public",
                table: "Rubric2Subsystems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Region2Subsystems",
                schema: "public",
                table: "Region2Subsystems");

            migrationBuilder.RenameTable(
                name: "Topic2Subsystems",
                schema: "public",
                newName: "Topic2Subsystem",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "Territory2Subsystems",
                schema: "public",
                newName: "Territory2Subsystem",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "Rubric2Subsystems",
                schema: "public",
                newName: "Rubric2Subsystem",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "Region2Subsystems",
                schema: "public",
                newName: "Region2Subsystem",
                newSchema: "public");

            migrationBuilder.RenameIndex(
                name: "IX_Topic2Subsystems_SubsystemId",
                schema: "public",
                table: "Topic2Subsystem",
                newName: "IX_Topic2Subsystem_SubsystemId");

            migrationBuilder.RenameIndex(
                name: "IX_Territory2Subsystems_SubsystemId",
                schema: "public",
                table: "Territory2Subsystem",
                newName: "IX_Territory2Subsystem_SubsystemId");

            migrationBuilder.RenameIndex(
                name: "IX_Rubric2Subsystems_SubsystemId",
                schema: "public",
                table: "Rubric2Subsystem",
                newName: "IX_Rubric2Subsystem_SubsystemId");

            migrationBuilder.RenameIndex(
                name: "IX_Region2Subsystems_SubsystemId",
                schema: "public",
                table: "Region2Subsystem",
                newName: "IX_Region2Subsystem_SubsystemId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Topic2Subsystem",
                schema: "public",
                table: "Topic2Subsystem",
                columns: new[] { "TopicId", "SubsystemId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Territory2Subsystem",
                schema: "public",
                table: "Territory2Subsystem",
                columns: new[] { "TerritoryId", "SubsystemId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Rubric2Subsystem",
                schema: "public",
                table: "Rubric2Subsystem",
                columns: new[] { "RubricId", "SubsystemId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Region2Subsystem",
                schema: "public",
                table: "Region2Subsystem",
                columns: new[] { "RegionId", "SubsystemId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Region2Subsystem_Regions_RegionId",
                schema: "public",
                table: "Region2Subsystem",
                column: "RegionId",
                principalSchema: "public",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Region2Subsystem_Subsystems_SubsystemId",
                schema: "public",
                table: "Region2Subsystem",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rubric2Subsystem_Rubrics_RubricId",
                schema: "public",
                table: "Rubric2Subsystem",
                column: "RubricId",
                principalSchema: "public",
                principalTable: "Rubrics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rubric2Subsystem_Subsystems_SubsystemId",
                schema: "public",
                table: "Rubric2Subsystem",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Territory2Subsystem_Subsystems_SubsystemId",
                schema: "public",
                table: "Territory2Subsystem",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Territory2Subsystem_Territories_TerritoryId",
                schema: "public",
                table: "Territory2Subsystem",
                column: "TerritoryId",
                principalSchema: "public",
                principalTable: "Territories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Topic2Subsystem_Subsystems_SubsystemId",
                schema: "public",
                table: "Topic2Subsystem",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Topic2Subsystem_Topics_TopicId",
                schema: "public",
                table: "Topic2Subsystem",
                column: "TopicId",
                principalSchema: "public",
                principalTable: "Topics",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
