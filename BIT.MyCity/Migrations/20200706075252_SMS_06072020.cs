﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class SMS_06072020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SmsType",
                schema: "public",
                table: "SmsMessages",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ConfirmCode",
                schema: "public",
                table: "OrganizationUserPhones",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ConfirmCodeLifeTime",
                schema: "public",
                table: "OrganizationUserPhones",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SmsType",
                schema: "public",
                table: "SmsMessages");

            migrationBuilder.DropColumn(
                name: "ConfirmCode",
                schema: "public",
                table: "OrganizationUserPhones");

            migrationBuilder.DropColumn(
                name: "ConfirmCodeLifeTime",
                schema: "public",
                table: "OrganizationUserPhones");
        }
    }
}
