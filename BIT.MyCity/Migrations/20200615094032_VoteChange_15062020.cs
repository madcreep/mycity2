﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class VoteChange_15062020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Votes_Subsystems_SubsystemId1",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropIndex(
                name: "IX_Votes_SubsystemId1",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "SubsystemId1",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "ElDocForUL",
                schema: "public",
                table: "Rubrics");

            migrationBuilder.AddColumn<long>(
                name: "RootId",
                schema: "public",
                table: "Votes",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Votes_RootId",
                schema: "public",
                table: "Votes",
                column: "RootId");

            migrationBuilder.AddForeignKey(
                name: "FK_Votes_Votes_RootId",
                schema: "public",
                table: "Votes",
                column: "RootId",
                principalSchema: "public",
                principalTable: "Votes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Votes_Votes_RootId",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropIndex(
                name: "IX_Votes_RootId",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "RootId",
                schema: "public",
                table: "Votes");

            migrationBuilder.AddColumn<long>(
                name: "SubsystemId1",
                schema: "public",
                table: "Votes",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ElDocForUL",
                schema: "public",
                table: "Rubrics",
                type: "boolean",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Votes_SubsystemId1",
                schema: "public",
                table: "Votes",
                column: "SubsystemId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Votes_Subsystems_SubsystemId1",
                schema: "public",
                table: "Votes",
                column: "SubsystemId1",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
