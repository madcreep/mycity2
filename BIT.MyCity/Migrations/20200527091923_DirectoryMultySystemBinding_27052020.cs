﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class DirectoryMultySystemBinding_27052020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RegionIds",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "RubricIds",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.DropColumn(
                name: "TerritoryIds",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.CreateTable(
                name: "Region2Subsystem",
                schema: "public",
                columns: table => new
                {
                    RegionId = table.Column<long>(nullable: false),
                    SubsystemId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Region2Subsystem", x => new { x.RegionId, x.SubsystemId });
                    table.ForeignKey(
                        name: "FK_Region2Subsystem_Regions_RegionId",
                        column: x => x.RegionId,
                        principalSchema: "public",
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Region2Subsystem_Subsystems_SubsystemId",
                        column: x => x.SubsystemId,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Rubric2Subsystem",
                schema: "public",
                columns: table => new
                {
                    RubricId = table.Column<long>(nullable: false),
                    SubsystemId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rubric2Subsystem", x => new { x.RubricId, x.SubsystemId });
                    table.ForeignKey(
                        name: "FK_Rubric2Subsystem_Rubrics_RubricId",
                        column: x => x.RubricId,
                        principalSchema: "public",
                        principalTable: "Rubrics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Rubric2Subsystem_Subsystems_SubsystemId",
                        column: x => x.SubsystemId,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Territory2Subsystem",
                schema: "public",
                columns: table => new
                {
                    TerritoryId = table.Column<long>(nullable: false),
                    SubsystemId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Territory2Subsystem", x => new { x.TerritoryId, x.SubsystemId });
                    table.ForeignKey(
                        name: "FK_Territory2Subsystem_Subsystems_SubsystemId",
                        column: x => x.SubsystemId,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Territory2Subsystem_Territories_TerritoryId",
                        column: x => x.TerritoryId,
                        principalSchema: "public",
                        principalTable: "Territories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Topic2Subsystem",
                schema: "public",
                columns: table => new
                {
                    TopicId = table.Column<long>(nullable: false),
                    SubsystemId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Topic2Subsystem", x => new { x.TopicId, x.SubsystemId });
                    table.ForeignKey(
                        name: "FK_Topic2Subsystem_Subsystems_SubsystemId",
                        column: x => x.SubsystemId,
                        principalSchema: "public",
                        principalTable: "Subsystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Topic2Subsystem_Topics_TopicId",
                        column: x => x.TopicId,
                        principalSchema: "public",
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Region2Subsystem_SubsystemId",
                schema: "public",
                table: "Region2Subsystem",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Rubric2Subsystem_SubsystemId",
                schema: "public",
                table: "Rubric2Subsystem",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Territory2Subsystem_SubsystemId",
                schema: "public",
                table: "Territory2Subsystem",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_Topic2Subsystem_SubsystemId",
                schema: "public",
                table: "Topic2Subsystem",
                column: "SubsystemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Region2Subsystem",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Rubric2Subsystem",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Territory2Subsystem",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Topic2Subsystem",
                schema: "public");

            migrationBuilder.AddColumn<List<long>>(
                name: "RegionIds",
                schema: "public",
                table: "Subsystems",
                type: "bigint[]",
                nullable: true);

            migrationBuilder.AddColumn<List<long>>(
                name: "RubricIds",
                schema: "public",
                table: "Subsystems",
                type: "bigint[]",
                nullable: true);

            migrationBuilder.AddColumn<List<long>>(
                name: "TerritoryIds",
                schema: "public",
                table: "Subsystems",
                type: "bigint[]",
                nullable: true);
        }
    }
}
