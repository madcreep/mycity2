﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class AddSubscribeToAny_02092020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Subs2DueMessages",
                schema: "public",
                columns: table => new
                {
                    RootType = table.Column<string>(nullable: false),
                    Due = table.Column<string>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    Level = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subs2DueMessages", x => new { x.RootType, x.Due, x.UserId });
                    table.ForeignKey(
                        name: "FK_Subs2DueMessages_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Subs2DueMessages_Due",
                schema: "public",
                table: "Subs2DueMessages",
                column: "Due");

            migrationBuilder.CreateIndex(
                name: "IX_Subs2DueMessages_UserId",
                schema: "public",
                table: "Subs2DueMessages",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Subs2DueMessages",
                schema: "public");
        }
    }
}
