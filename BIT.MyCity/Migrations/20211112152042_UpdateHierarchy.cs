﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class UpdateHierarchy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Esia",
                schema: "public",
                table: "Appeals");

            migrationBuilder.AlterColumn<long>(
                name: "ParentId",
                schema: "public",
                table: "Topics",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddColumn<int>(
                name: "Layer",
                schema: "public",
                table: "Topics",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<long>(
                name: "ParentId",
                schema: "public",
                table: "Territories",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddColumn<int>(
                name: "Layer",
                schema: "public",
                table: "Territories",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<long>(
                name: "ParentId",
                schema: "public",
                table: "Rubrics",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddColumn<int>(
                name: "Layer",
                schema: "public",
                table: "Rubrics",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<long>(
                name: "ParentId",
                schema: "public",
                table: "Regions",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddColumn<int>(
                name: "Layer",
                schema: "public",
                table: "Regions",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Layer",
                schema: "public",
                table: "Topics");

            migrationBuilder.DropColumn(
                name: "Layer",
                schema: "public",
                table: "Territories");

            migrationBuilder.DropColumn(
                name: "Layer",
                schema: "public",
                table: "Rubrics");

            migrationBuilder.DropColumn(
                name: "Layer",
                schema: "public",
                table: "Regions");

            migrationBuilder.AlterColumn<long>(
                name: "ParentId",
                schema: "public",
                table: "Topics",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "ParentId",
                schema: "public",
                table: "Territories",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "ParentId",
                schema: "public",
                table: "Rubrics",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "ParentId",
                schema: "public",
                table: "Regions",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Esia",
                schema: "public",
                table: "Appeals",
                type: "boolean",
                nullable: true);
        }
    }
}
