﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BIT.MyCity.Migrations
{
    public partial class VoteChange_19062020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VoteItems_Votes_RootVoteId",
                schema: "public",
                table: "VoteItems");

            migrationBuilder.DropForeignKey(
                name: "FK_Votes_Votes_RootId",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropIndex(
                name: "IX_Votes_RootId",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropIndex(
                name: "IX_VoteItems_RootVoteId",
                schema: "public",
                table: "VoteItems");

            migrationBuilder.DropColumn(
                name: "ArchiveAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "ArchivedAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "EndAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "EndedAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "RootId",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "StartAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "StartedAt",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "State",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "RootVoteId",
                schema: "public",
                table: "VoteItems");

            migrationBuilder.AddColumn<long>(
                name: "VoteRootId",
                schema: "public",
                table: "Votes",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "VoteRootId",
                schema: "public",
                table: "AttachedFiles",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "VoteRoots",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    DeleteTimestamp = table.Column<DateTime>(nullable: true),
                    Deleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    TextResult = table.Column<string>(nullable: true),
                    StartAt = table.Column<DateTime>(nullable: true),
                    EndAt = table.Column<DateTime>(nullable: true),
                    ArchiveAt = table.Column<DateTime>(nullable: true),
                    StartedAt = table.Column<DateTime>(nullable: true),
                    EndedAt = table.Column<DateTime>(nullable: true),
                    ArchivedAt = table.Column<DateTime>(nullable: true),
                    State = table.Column<int>(nullable: false),
                    VoteEntryId = table.Column<long>(nullable: true),
                    VoteEntryId1 = table.Column<long>(nullable: true),
                    RegionId = table.Column<long>(nullable: true),
                    RubricId = table.Column<long>(nullable: true),
                    TerritoryId = table.Column<long>(nullable: true),
                    TopicId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VoteRoots", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VoteRoots_Regions_RegionId",
                        column: x => x.RegionId,
                        principalSchema: "public",
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VoteRoots_Rubrics_RubricId",
                        column: x => x.RubricId,
                        principalSchema: "public",
                        principalTable: "Rubrics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VoteRoots_Territories_TerritoryId",
                        column: x => x.TerritoryId,
                        principalSchema: "public",
                        principalTable: "Territories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VoteRoots_Topics_TopicId",
                        column: x => x.TopicId,
                        principalSchema: "public",
                        principalTable: "Topics",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VoteRoots_Votes_VoteEntryId1",
                        column: x => x.VoteEntryId1,
                        principalSchema: "public",
                        principalTable: "Votes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Votes_VoteRootId",
                schema: "public",
                table: "Votes",
                column: "VoteRootId");

            migrationBuilder.CreateIndex(
                name: "IX_AttachedFiles_VoteRootId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteRootId");

            migrationBuilder.CreateIndex(
                name: "IX_VoteRoots_RegionId",
                schema: "public",
                table: "VoteRoots",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_VoteRoots_RubricId",
                schema: "public",
                table: "VoteRoots",
                column: "RubricId");

            migrationBuilder.CreateIndex(
                name: "IX_VoteRoots_TerritoryId",
                schema: "public",
                table: "VoteRoots",
                column: "TerritoryId");

            migrationBuilder.CreateIndex(
                name: "IX_VoteRoots_TopicId",
                schema: "public",
                table: "VoteRoots",
                column: "TopicId");

            migrationBuilder.CreateIndex(
                name: "IX_VoteRoots_VoteEntryId1",
                schema: "public",
                table: "VoteRoots",
                column: "VoteEntryId1");

            migrationBuilder.AddForeignKey(
                name: "FK_AttachedFiles_VoteRoots_VoteRootId",
                schema: "public",
                table: "AttachedFiles",
                column: "VoteRootId",
                principalSchema: "public",
                principalTable: "VoteRoots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Votes_VoteRoots_VoteRootId",
                schema: "public",
                table: "Votes",
                column: "VoteRootId",
                principalSchema: "public",
                principalTable: "VoteRoots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AttachedFiles_VoteRoots_VoteRootId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Votes_VoteRoots_VoteRootId",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropTable(
                name: "VoteRoots",
                schema: "public");

            migrationBuilder.DropIndex(
                name: "IX_Votes_VoteRootId",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropIndex(
                name: "IX_AttachedFiles_VoteRootId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "VoteRootId",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "VoteRootId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchiveAt",
                schema: "public",
                table: "Votes",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ArchivedAt",
                schema: "public",
                table: "Votes",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndAt",
                schema: "public",
                table: "Votes",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndedAt",
                schema: "public",
                table: "Votes",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RootId",
                schema: "public",
                table: "Votes",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartAt",
                schema: "public",
                table: "Votes",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartedAt",
                schema: "public",
                table: "Votes",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "State",
                schema: "public",
                table: "Votes",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<long>(
                name: "RootVoteId",
                schema: "public",
                table: "VoteItems",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Votes_RootId",
                schema: "public",
                table: "Votes",
                column: "RootId");

            migrationBuilder.CreateIndex(
                name: "IX_VoteItems_RootVoteId",
                schema: "public",
                table: "VoteItems",
                column: "RootVoteId");

            migrationBuilder.AddForeignKey(
                name: "FK_VoteItems_Votes_RootVoteId",
                schema: "public",
                table: "VoteItems",
                column: "RootVoteId",
                principalSchema: "public",
                principalTable: "Votes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Votes_Votes_RootId",
                schema: "public",
                table: "Votes",
                column: "RootId",
                principalSchema: "public",
                principalTable: "Votes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
