﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class UpdateRnumVerbs_15082020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "public",
                table: "EnumVerbs",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DeleteTimestamp",
                schema: "public",
                table: "EnumVerbs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                schema: "public",
                table: "EnumVerbs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "public",
                table: "EnumVerbs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "public",
                table: "EnumVerbs");

            migrationBuilder.DropColumn(
                name: "DeleteTimestamp",
                schema: "public",
                table: "EnumVerbs");

            migrationBuilder.DropColumn(
                name: "Deleted",
                schema: "public",
                table: "EnumVerbs");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "public",
                table: "EnumVerbs");
        }
    }
}
