﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class AddJournalIndexex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_JournalProperties_Tag",
                schema: "public",
                table: "JournalProperties",
                column: "Tag");

            migrationBuilder.CreateIndex(
                name: "IX_Journal_Timestamp_Operation_EntityKey",
                schema: "public",
                table: "Journal",
                columns: new[] { "Timestamp", "Operation", "EntityKey" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_JournalProperties_Tag",
                schema: "public",
                table: "JournalProperties");

            migrationBuilder.DropIndex(
                name: "IX_Journal_Timestamp_Operation_EntityKey",
                schema: "public",
                table: "Journal");
        }
    }
}
