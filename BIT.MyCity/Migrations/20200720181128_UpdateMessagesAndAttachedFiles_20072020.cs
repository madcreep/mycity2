﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class UpdateMessagesAndAttachedFiles_20072020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Messages_News_NewsRootId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Appeals_RootAppealId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_NewsRootId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_RootAppealId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "NewsRootId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "RootAppealId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "ParentId",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "ParentType",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.AddColumn<long>(
                name: "AppealId",
                schema: "public",
                table: "Messages",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "NewsId",
                schema: "public",
                table: "Messages",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "VoteId",
                schema: "public",
                table: "Messages",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Messages_AppealId",
                schema: "public",
                table: "Messages",
                column: "AppealId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_NewsId",
                schema: "public",
                table: "Messages",
                column: "NewsId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_VoteId",
                schema: "public",
                table: "Messages",
                column: "VoteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Appeals_AppealId",
                schema: "public",
                table: "Messages",
                column: "AppealId",
                principalSchema: "public",
                principalTable: "Appeals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_News_NewsId",
                schema: "public",
                table: "Messages",
                column: "NewsId",
                principalSchema: "public",
                principalTable: "News",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Votes_VoteId",
                schema: "public",
                table: "Messages",
                column: "VoteId",
                principalSchema: "public",
                principalTable: "Votes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Appeals_AppealId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropForeignKey(
                name: "FK_Messages_News_NewsId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Votes_VoteId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_AppealId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_NewsId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_VoteId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "AppealId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "NewsId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "VoteId",
                schema: "public",
                table: "Messages");

            migrationBuilder.AddColumn<long>(
                name: "NewsRootId",
                schema: "public",
                table: "Messages",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "RootAppealId",
                schema: "public",
                table: "Messages",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ParentId",
                schema: "public",
                table: "AttachedFiles",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "ParentType",
                schema: "public",
                table: "AttachedFiles",
                type: "text",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Messages_NewsRootId",
                schema: "public",
                table: "Messages",
                column: "NewsRootId");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_RootAppealId",
                schema: "public",
                table: "Messages",
                column: "RootAppealId");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_News_NewsRootId",
                schema: "public",
                table: "Messages",
                column: "NewsRootId",
                principalSchema: "public",
                principalTable: "News",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Appeals_RootAppealId",
                schema: "public",
                table: "Messages",
                column: "RootAppealId",
                principalSchema: "public",
                principalTable: "Appeals",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
