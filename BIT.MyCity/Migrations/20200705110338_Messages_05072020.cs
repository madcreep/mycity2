﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class Messages_05072020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Votes_RootVoteId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_RootVoteId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "RootVoteId",
                schema: "public",
                table: "Messages");

            migrationBuilder.AddColumn<long>(
                name: "VoteRootId",
                schema: "public",
                table: "Messages",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Messages_VoteRootId",
                schema: "public",
                table: "Messages",
                column: "VoteRootId");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_VoteRoots_VoteRootId",
                schema: "public",
                table: "Messages",
                column: "VoteRootId",
                principalSchema: "public",
                principalTable: "VoteRoots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Messages_VoteRoots_VoteRootId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_VoteRootId",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "VoteRootId",
                schema: "public",
                table: "Messages");

            migrationBuilder.AddColumn<long>(
                name: "RootVoteId",
                schema: "public",
                table: "Messages",
                type: "bigint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Messages_RootVoteId",
                schema: "public",
                table: "Messages",
                column: "RootVoteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Votes_RootVoteId",
                schema: "public",
                table: "Messages",
                column: "RootVoteId",
                principalSchema: "public",
                principalTable: "Votes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
