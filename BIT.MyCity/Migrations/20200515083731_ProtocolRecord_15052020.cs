﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class ProtocolRecord_15052020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExtDate",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "ExtId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "ExtNumber",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "Public",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "Rejected",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "RejectionReason",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "SiteStatus",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "Source",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "Status",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.AddColumn<long>(
                name: "AppealExecutorId",
                schema: "public",
                table: "ProtocolRecords",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "AppointmentNotificationId",
                schema: "public",
                table: "ProtocolRecords",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "AttachedFileId",
                schema: "public",
                table: "ProtocolRecords",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MessageId",
                schema: "public",
                table: "ProtocolRecords",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "SubsystemId",
                schema: "public",
                table: "ProtocolRecords",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "VoteId",
                schema: "public",
                table: "ProtocolRecords",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "VoteItemId",
                schema: "public",
                table: "ProtocolRecords",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_AppealExecutorId",
                schema: "public",
                table: "ProtocolRecords",
                column: "AppealExecutorId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_AppointmentNotificationId",
                schema: "public",
                table: "ProtocolRecords",
                column: "AppointmentNotificationId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_AttachedFileId",
                schema: "public",
                table: "ProtocolRecords",
                column: "AttachedFileId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_MessageId",
                schema: "public",
                table: "ProtocolRecords",
                column: "MessageId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_SubsystemId",
                schema: "public",
                table: "ProtocolRecords",
                column: "SubsystemId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_VoteId",
                schema: "public",
                table: "ProtocolRecords",
                column: "VoteId");

            migrationBuilder.CreateIndex(
                name: "IX_ProtocolRecords_VoteItemId",
                schema: "public",
                table: "ProtocolRecords",
                column: "VoteItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProtocolRecords_AppealExecutors_AppealExecutorId",
                schema: "public",
                table: "ProtocolRecords",
                column: "AppealExecutorId",
                principalSchema: "public",
                principalTable: "AppealExecutors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProtocolRecords_AppointmentNotifications_AppointmentNotific~",
                schema: "public",
                table: "ProtocolRecords",
                column: "AppointmentNotificationId",
                principalSchema: "public",
                principalTable: "AppointmentNotifications",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProtocolRecords_AttachedFiles_AttachedFileId",
                schema: "public",
                table: "ProtocolRecords",
                column: "AttachedFileId",
                principalSchema: "public",
                principalTable: "AttachedFiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProtocolRecords_Messages_MessageId",
                schema: "public",
                table: "ProtocolRecords",
                column: "MessageId",
                principalSchema: "public",
                principalTable: "Messages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProtocolRecords_Subsystems_SubsystemId",
                schema: "public",
                table: "ProtocolRecords",
                column: "SubsystemId",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProtocolRecords_Votes_VoteId",
                schema: "public",
                table: "ProtocolRecords",
                column: "VoteId",
                principalSchema: "public",
                principalTable: "Votes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProtocolRecords_VoteItems_VoteItemId",
                schema: "public",
                table: "ProtocolRecords",
                column: "VoteItemId",
                principalSchema: "public",
                principalTable: "VoteItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProtocolRecords_AppealExecutors_AppealExecutorId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_ProtocolRecords_AppointmentNotifications_AppointmentNotific~",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_ProtocolRecords_AttachedFiles_AttachedFileId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_ProtocolRecords_Messages_MessageId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_ProtocolRecords_Subsystems_SubsystemId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_ProtocolRecords_Votes_VoteId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropForeignKey(
                name: "FK_ProtocolRecords_VoteItems_VoteItemId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropIndex(
                name: "IX_ProtocolRecords_AppealExecutorId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropIndex(
                name: "IX_ProtocolRecords_AppointmentNotificationId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropIndex(
                name: "IX_ProtocolRecords_AttachedFileId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropIndex(
                name: "IX_ProtocolRecords_MessageId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropIndex(
                name: "IX_ProtocolRecords_SubsystemId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropIndex(
                name: "IX_ProtocolRecords_VoteId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropIndex(
                name: "IX_ProtocolRecords_VoteItemId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "AppealExecutorId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "AppointmentNotificationId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "AttachedFileId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "MessageId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "SubsystemId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "VoteId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.DropColumn(
                name: "VoteItemId",
                schema: "public",
                table: "ProtocolRecords");

            migrationBuilder.AddColumn<DateTime>(
                name: "ExtDate",
                schema: "public",
                table: "ProtocolRecords",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExtId",
                schema: "public",
                table: "ProtocolRecords",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExtNumber",
                schema: "public",
                table: "ProtocolRecords",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Public",
                schema: "public",
                table: "ProtocolRecords",
                type: "boolean",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Rejected",
                schema: "public",
                table: "ProtocolRecords",
                type: "boolean",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RejectionReason",
                schema: "public",
                table: "ProtocolRecords",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SiteStatus",
                schema: "public",
                table: "ProtocolRecords",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Source",
                schema: "public",
                table: "ProtocolRecords",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                schema: "public",
                table: "ProtocolRecords",
                type: "integer",
                nullable: true);
        }
    }
}
