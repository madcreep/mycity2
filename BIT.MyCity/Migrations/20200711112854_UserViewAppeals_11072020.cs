﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class UserViewAppeals_11072020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CommentsAllowed",
                schema: "public",
                table: "Votes",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "CommentsAllowed",
                schema: "public",
                table: "VoteRoots",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "OpenLevel",
                schema: "public",
                table: "VoteRoots",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "UserViewAppeals",
                schema: "public",
                columns: table => new
                {
                    AppealId = table.Column<long>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    Viewed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserViewAppeals", x => new { x.UserId, x.AppealId });
                    table.ForeignKey(
                        name: "FK_UserViewAppeals_Appeals_AppealId",
                        column: x => x.AppealId,
                        principalSchema: "public",
                        principalTable: "Appeals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserViewAppeals_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserViewAppeals_AppealId",
                schema: "public",
                table: "UserViewAppeals",
                column: "AppealId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserViewAppeals",
                schema: "public");

            migrationBuilder.DropColumn(
                name: "CommentsAllowed",
                schema: "public",
                table: "Votes");

            migrationBuilder.DropColumn(
                name: "CommentsAllowed",
                schema: "public",
                table: "VoteRoots");

            migrationBuilder.DropColumn(
                name: "OpenLevel",
                schema: "public",
                table: "VoteRoots");
        }
    }
}
