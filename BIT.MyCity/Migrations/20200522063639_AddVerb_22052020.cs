﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BIT.MyCity.Migrations
{
    public partial class AddVerb_22052020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TopicIds",
                schema: "public",
                table: "Subsystems");

            migrationBuilder.AddColumn<long>(
                name: "SubsystemId_",
                schema: "public",
                table: "Topics",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "EnumVerbs",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Weight = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Group = table.Column<string>(nullable: true),
                    SubsystemUID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnumVerbs", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Topics_SubsystemId_",
                schema: "public",
                table: "Topics",
                column: "SubsystemId_");

            migrationBuilder.CreateIndex(
                name: "IX_EnumVerbs_Value_Group_SubsystemUID",
                schema: "public",
                table: "EnumVerbs",
                columns: new[] { "Value", "Group", "SubsystemUID" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Topics_Subsystems_SubsystemId_",
                schema: "public",
                table: "Topics",
                column: "SubsystemId_",
                principalSchema: "public",
                principalTable: "Subsystems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Topics_Subsystems_SubsystemId_",
                schema: "public",
                table: "Topics");

            migrationBuilder.DropTable(
                name: "EnumVerbs",
                schema: "public");

            migrationBuilder.DropIndex(
                name: "IX_Topics_SubsystemId_",
                schema: "public",
                table: "Topics");

            migrationBuilder.DropColumn(
                name: "SubsystemId_",
                schema: "public",
                table: "Topics");

            migrationBuilder.AddColumn<List<long>>(
                name: "TopicIds",
                schema: "public",
                table: "Subsystems",
                type: "bigint[]",
                nullable: true);
        }
    }
}
