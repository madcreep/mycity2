﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class AddScoreExecutors_24072020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "public",
                table: "UserViewNews",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "public",
                table: "UserViewNews",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "public",
                table: "UserViewMessages",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "public",
                table: "UserViewMessages",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "public",
                table: "UserViewAppeals",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "public",
                table: "UserViewAppeals",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "public",
                table: "UserLikeNews",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "public",
                table: "UserLikeNews",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "public",
                table: "UserLikeMessage",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "public",
                table: "UserLikeMessage",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                schema: "public",
                table: "UserLikeAppeals",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                schema: "public",
                table: "UserLikeAppeals",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "RegardAverage",
                schema: "public",
                table: "Messages",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "RegardsCount",
                schema: "public",
                table: "Messages",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RegardMode",
                schema: "public",
                table: "Appeals",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "MessageRegards",
                schema: "public",
                columns: table => new
                {
                    UserId = table.Column<long>(nullable: false),
                    MessageId = table.Column<long>(nullable: false),
                    Regard = table.Column<int>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageRegards", x => new { x.UserId, x.MessageId });
                    table.ForeignKey(
                        name: "FK_MessageRegards_Messages_MessageId",
                        column: x => x.MessageId,
                        principalSchema: "public",
                        principalTable: "Messages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MessageRegards_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalSchema: "public",
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MessageRegards_MessageId",
                schema: "public",
                table: "MessageRegards",
                column: "MessageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MessageRegards",
                schema: "public");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "public",
                table: "UserViewNews");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "public",
                table: "UserViewNews");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "public",
                table: "UserViewMessages");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "public",
                table: "UserViewMessages");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "public",
                table: "UserViewAppeals");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "public",
                table: "UserViewAppeals");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "public",
                table: "UserLikeNews");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "public",
                table: "UserLikeNews");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "public",
                table: "UserLikeMessage");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "public",
                table: "UserLikeMessage");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                schema: "public",
                table: "UserLikeAppeals");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                schema: "public",
                table: "UserLikeAppeals");

            migrationBuilder.DropColumn(
                name: "RegardAverage",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "RegardsCount",
                schema: "public",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "RegardMode",
                schema: "public",
                table: "Appeals");
        }
    }
}
