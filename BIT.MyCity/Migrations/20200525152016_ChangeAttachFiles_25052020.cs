﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace BIT.MyCity.Migrations
{
    public partial class ChangeAttachFiles_25052020 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Thumb",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.AddColumn<int>(
                name: "Format",
                schema: "public",
                table: "AttachedFiles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Height",
                schema: "public",
                table: "AttachedFiles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Width",
                schema: "public",
                table: "AttachedFiles",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ThumbNails",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Format = table.Column<int>(nullable: false),
                    Width = table.Column<int>(nullable: false),
                    Height = table.Column<int>(nullable: false),
                    Base64Data = table.Column<string>(nullable: true),
                    LastAccess = table.Column<DateTime>(nullable: false),
                    AttachedFileId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ThumbNails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ThumbNails_AttachedFiles_AttachedFileId",
                        column: x => x.AttachedFileId,
                        principalSchema: "public",
                        principalTable: "AttachedFiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ThumbNails_AttachedFileId",
                schema: "public",
                table: "ThumbNails",
                column: "AttachedFileId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ThumbNails",
                schema: "public");

            migrationBuilder.DropColumn(
                name: "Format",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "Height",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.DropColumn(
                name: "Width",
                schema: "public",
                table: "AttachedFiles");

            migrationBuilder.AddColumn<string>(
                name: "Thumb",
                schema: "public",
                table: "AttachedFiles",
                type: "text",
                nullable: true);
        }
    }
}
