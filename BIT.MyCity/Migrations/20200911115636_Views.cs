﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BIT.MyCity.Migrations
{
    public partial class Views : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                CREATE VIEW AppealsView AS SELECT 
                    COUNT(""Id"") AppealsCount, 
                    ""SubsystemId1"" SubsystemId, 
                    ""RubricId"", 
                    ""TopicId"", 
                    ""TerritoryId"", 
                    ""RegionId"",
                    date_trunc('day', ""CreatedAt"") CreatedAt, 
                    ""Public_"" Public, 
                    ""Deleted"", 
                    ""SiteStatus"", 
                    ""SentToDelo"",
                    ""Approved""
                FROM ""Appeals""
                GROUP BY ""SubsystemId1"", ""RubricId"", ""TerritoryId"", ""TopicId"", ""RegionId"", date_trunc('day', ""CreatedAt""), ""Public_"", ""Deleted"", ""SiteStatus"", ""SentToDelo"", ""Approved""");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW AppealsView");
        }
    }
}
