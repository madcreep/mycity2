using System;

namespace BIT.MyCity
{
    public class UserException : Exception
    {
        override public string Message { get; }
        
        public UserException(string message) : base()
        {
            Message = message;
        }
    }
}