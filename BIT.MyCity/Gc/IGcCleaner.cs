using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;

namespace BIT.MyCity.Gc
{
    public interface IGcCleaner
    {
        public Task ClearUnusedEntities();

        IQueryable<long> UsingFilesIdsQuery(DocumentsContext ctx);
    }
}
