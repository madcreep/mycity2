using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;

namespace BIT.MyCity.Gc
{
    public class EmailsGcCleaner : IGcCleaner
    {
        public async Task ClearUnusedEntities()
        { }

        public IQueryable<long> UsingFilesIdsQuery(DocumentsContext ctx)
        {
            return ctx.AttachedFilesToEmails
                .Select(el => el.AttachedFileId);
        }
    }
}
