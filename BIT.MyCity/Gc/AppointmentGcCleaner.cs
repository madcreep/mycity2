using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;

namespace BIT.MyCity.Gc
{
    public class AppointmentGcCleaner : IGcCleaner
    {
        public async Task ClearUnusedEntities()
        {}

        public IQueryable<long> UsingFilesIdsQuery(DocumentsContext ctx)
        {
            return ctx.AttachedFilesToAppointments
                .Select(el => el.AttachedFileId);
        }
    }
}
