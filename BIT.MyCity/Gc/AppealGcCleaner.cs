using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;

namespace BIT.MyCity.Gc
{
    public class AppealGcCleaner : IGcCleaner
    {
        public async Task ClearUnusedEntities()
        {}

        public IQueryable<long> UsingFilesIdsQuery(DocumentsContext ctx)
        {
            return ctx.AttachedFilesToAppeals
                .Select(el => el.AttachedFileId)
                .Union(ctx.AttachedFilesToMessages
                    .Where(el => el.Message.AppealId.HasValue)
                    .Select(el => el.AttachedFileId));
        }
    }
}
