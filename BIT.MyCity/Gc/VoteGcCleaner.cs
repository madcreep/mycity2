using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;

namespace BIT.MyCity.Gc
{
    public class VoteGcCleaner : IGcCleaner
    {
        public async Task ClearUnusedEntities()
        { }

        public IQueryable<long> UsingFilesIdsQuery(DocumentsContext ctx)
        {
            return ctx.AttachedFilesToVoteRoots
                .Select(el => el.AttachedFileId)
                .Union(ctx.AttachedFilesToVotes
                    .Select(el => el.AttachedFileId))
                .Union(ctx.AttachedFilesToVoteItems
                    .Select(el => el.AttachedFileId))
                .Union(ctx.AttachedFilesToMessages
                    .Where(el => el.Message.VoteRootId.HasValue || el.Message.VoteId.HasValue)
                    .Select(el => el.AttachedFileId));
        }
    }
}
