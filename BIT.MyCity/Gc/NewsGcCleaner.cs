using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;

namespace BIT.MyCity.Gc
{
    public class NewsGcCleaner : IGcCleaner
    {
        public async Task ClearUnusedEntities()
        {}

        public IQueryable<long> UsingFilesIdsQuery(DocumentsContext ctx)
        {
            return ctx.AttachedFilesToNews
                .Select(el => el.AttachedFileId)
                .Union(ctx.AttachedFilesToMessages
                    .Where(el => el.Message.NewsId.HasValue)
                    .Select(el => el.AttachedFileId));
        }
    }
}
