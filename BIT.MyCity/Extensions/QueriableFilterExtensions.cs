using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Subsystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using BIT.GraphQL.Extensions.Filters;
using LinqKit;

namespace BIT.MyCity.Extensions
{
    public static class QueriableFilterExtensions
    {
        public static IQueryable<T> FilterByReadAllowed<T>(this IQueryable<T> main, long? userId, ClaimsPrincipal claimsPrincipal)
        {
            return main;
        }

        public static IQueryable<T> FilterByReadAllowedSubsystemRightsOld<T>(
            this IQueryable<T> main,
            IEnumerable<Claim> claims,
            string ss = null
            ) where T : IHasRubricators
        {
            var result = main.Where(el => false);
            GetFromClaimsARs(claims, out List<string> ss_all_regions, out List<string> ss_all_rubrics, out List<string> ss_all_territories, out List<string> ss_all_topics);
            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                result = result.Concat(main.Where(el => el.RegionId.HasValue && ss_all_regions.Contains(((IHasSubsystem)el).Subsystem.UID)));
                result = result.Concat(main.Where(el => el.RubricId.HasValue && ss_all_rubrics.Contains(((IHasSubsystem)el).Subsystem.UID)));
                result = result.Concat(main.Where(el => el.TerritoryId.HasValue && ss_all_territories.Contains(((IHasSubsystem)el).Subsystem.UID)));
                result = result.Concat(main.Where(el => el.TopicId.HasValue && ss_all_topics.Contains(((IHasSubsystem)el).Subsystem.UID)));
            }
            else if (ss != null)
            {
                result = result.Concat(main.Where(el => el.RegionId.HasValue && ss_all_regions.Contains(ss)));
                result = result.Concat(main.Where(el => el.RubricId.HasValue && ss_all_rubrics.Contains(ss)));
                result = result.Concat(main.Where(el => el.TerritoryId.HasValue && ss_all_territories.Contains(ss)));
                result = result.Concat(main.Where(el => el.TopicId.HasValue && ss_all_topics.Contains(ss)));
            }
            return result;
        }

        public static ExpressionStarter<T> FilterByReadAllowedSubsystemRightsPredicate<T>(
            long? userId,
            IEnumerable<Claim> claims,
            string ss = null
        ) where T : IHasRubricators
        {
            IQueryable<T> result;
            var enableModerate = GetFromClaimsSsModerateAvailable(claims);
            GetFromClaimsARs(claims, out List<string> ss_all_regions, out List<string> ss_all_rubrics, out List<string> ss_all_territories, out List<string> ss_all_topics);
            ss_all_regions = ss_all_regions
                .Intersect(enableModerate)
                .ToList();

            ss_all_rubrics = ss_all_rubrics
                .Intersect(enableModerate)
                .ToList();

            ss_all_territories = ss_all_territories
                .Intersect(enableModerate)
                .ToList();

            ss_all_topics = ss_all_topics
                .Intersect(enableModerate)
                .ToList();

            var predicate = PredicateBuilder.New<T>();

            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                predicate = predicate.And(el =>
                        (ss_all_regions.Contains(((IHasSubsystem)el).Subsystem.UID) || (el.RegionId.HasValue &&
                                                                                         el.Region.UserModerator.Any(
                                                                                             um =>
                                                                                                 um.UserId ==
                                                                                                 userId.Value &&
                                                                                                 um.Subsystem.Id ==
                                                                                                 ((IHasSubsystem)el)
                                                                                                 .Subsystem.Id)))

                        && (ss_all_rubrics.Contains(((IHasSubsystem)el).Subsystem.UID) || (el.RubricId.HasValue &&
                                                                                            el.Rubric.UserModerator.Any(
                                                                                                um =>
                                                                                                    um.UserId ==
                                                                                                    userId.Value &&
                                                                                                    um.Subsystem.Id ==
                                                                                                    ((IHasSubsystem)el)
                                                                                                    .Subsystem.Id)))
                        && (ss_all_territories.Contains(((IHasSubsystem)el).Subsystem.UID) ||
                            (el.TerritoryId.HasValue &&
                             el.Territory.UserModerator.Any(um =>
                                 um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)))
                        && (ss_all_topics.Contains(((IHasSubsystem)el).Subsystem.UID) || (!el.TopicId.HasValue &&
                                                                                           el.Topic.UserModerator.Any(
                                                                                               um =>
                                                                                                   um.UserId ==
                                                                                                   userId.Value &&
                                                                                                   um.Subsystem.Id ==
                                                                                                   ((IHasSubsystem)el)
                                                                                                   .Subsystem.Id)))
                    );
            }
            else if (ss != null)
            {
                predicate = predicate.And(el =>
                        (!el.RegionId.HasValue || ss_all_regions.Contains(ss) ||
                         el.Region.UserModerator.Any(um =>
                             um.UserId == userId.Value && um.Subsystem.UID == ss))
                        && (!el.RubricId.HasValue || ss_all_rubrics.Contains(ss) ||
                            el.Rubric.UserModerator.Any(um =>
                                um.UserId == userId.Value && um.Subsystem.UID == ss))
                        && (!el.TerritoryId.HasValue ||
                            ss_all_territories.Contains(ss) ||
                            el.Territory.UserModerator.Any(um =>
                                um.UserId == userId.Value && um.Subsystem.UID == ss))
                        && (!el.TopicId.HasValue || ss_all_topics.Contains(ss) ||
                            el.Topic.UserModerator.Any(um =>
                                um.UserId == userId.Value && um.Subsystem.UID == ss))
                    );

            }
            
            return predicate;
        }

        public static IQueryable<T> FilterByReadAllowedSubsystemRights<T>(
            this IQueryable<T> main,
            long? userId,
            IEnumerable<Claim> claims,
            string ss = null
        ) where T : IHasRubricators
        {
            IQueryable<T> result;
            var enableModerate = GetFromClaimsSsModerateAvailable(claims);
            GetFromClaimsARs(claims, out List<string> ss_all_regions, out List<string> ss_all_rubrics, out List<string> ss_all_territories, out List<string> ss_all_topics);
            ss_all_regions = ss_all_regions
                .Intersect(enableModerate)
                .ToList();

            ss_all_rubrics = ss_all_rubrics
                .Intersect(enableModerate)
                .ToList();

            ss_all_territories = ss_all_territories
                .Intersect(enableModerate)
                .ToList();

            ss_all_topics = ss_all_topics
                .Intersect(enableModerate)
                .ToList();

            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                result = main
                    .Where(el =>
                        (ss_all_regions.Contains(((IHasSubsystem) el).Subsystem.UID) || (el.RegionId.HasValue &&
                                                                                         el.Region.UserModerator.Any(
                                                                                             um =>
                                                                                                 um.UserId ==
                                                                                                 userId.Value &&
                                                                                                 um.Subsystem.Id ==
                                                                                                 ((IHasSubsystem) el)
                                                                                                 .Subsystem.Id)))

                        && (ss_all_rubrics.Contains(((IHasSubsystem) el).Subsystem.UID) || (el.RubricId.HasValue &&
                                                                                            el.Rubric.UserModerator.Any(
                                                                                                um =>
                                                                                                    um.UserId ==
                                                                                                    userId.Value &&
                                                                                                    um.Subsystem.Id ==
                                                                                                    ((IHasSubsystem) el)
                                                                                                    .Subsystem.Id)))
                        && (ss_all_territories.Contains(((IHasSubsystem) el).Subsystem.UID) ||
                            (el.TerritoryId.HasValue &&
                             el.Territory.UserModerator.Any(um =>
                                 um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem) el).Subsystem.Id)))
                        && (ss_all_topics.Contains(((IHasSubsystem) el).Subsystem.UID) || (!el.TopicId.HasValue &&
                                                                                           el.Topic.UserModerator.Any(
                                                                                               um =>
                                                                                                   um.UserId ==
                                                                                                   userId.Value &&
                                                                                                   um.Subsystem.Id ==
                                                                                                   ((IHasSubsystem) el)
                                                                                                   .Subsystem.Id)))
                    );
            }
            else if (ss != null)
            {
                result = main
                    .Where(el =>
                        (!el.RegionId.HasValue || ss_all_regions.Contains(ss) ||
                         el.Region.UserModerator.Any(um =>
                             um.UserId == userId.Value && um.Subsystem.UID == ss))
                        && (!el.RubricId.HasValue || ss_all_rubrics.Contains(ss) ||
                            el.Rubric.UserModerator.Any(um =>
                                um.UserId == userId.Value && um.Subsystem.UID == ss))
                        && (!el.TerritoryId.HasValue ||
                            ss_all_territories.Contains(ss) ||
                            el.Territory.UserModerator.Any(um =>
                                um.UserId == userId.Value && um.Subsystem.UID == ss))
                        && (!el.TopicId.HasValue || ss_all_topics.Contains(ss) ||
                            el.Topic.UserModerator.Any(um =>
                                um.UserId == userId.Value && um.Subsystem.UID == ss))
                    );

            }
            else
            {
                result = main.Where(el => false);
            }

            return result;
        }

        public static IEnumerable<FilterItem> FilterByReadAllowedSubsystemRights1<T>(
            this IQueryable<T> main,
            long? userId,
            IEnumerable<Claim> claims,
            string ss = null
        ) where T : IHasRubricators
        {
            var enableModerate = GetFromClaimsSsModerateAvailable(claims);
            GetFromClaimsARs(claims, out var ss_all_regions, out var ss_all_rubrics, out var ss_all_territories, out var ss_all_topics);
            ss_all_regions = ss_all_regions
                .Intersect(enableModerate)
                .ToList();

            ss_all_rubrics = ss_all_rubrics
                .Intersect(enableModerate)
                .ToList();

            ss_all_territories = ss_all_territories
                .Intersect(enableModerate)
                .ToList();

            ss_all_topics = ss_all_topics
                .Intersect(enableModerate)
                .ToList();

            var result = new List<FilterItem>();

            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                result.AddRange(new []
                {
                    new FilterItem
                    {
                        Connector = FilterStatementConnector.And,
                        SubProperty = new []
                        {
                            new FilterItem
                            {
                                Operation = Operation.In,
                                PropertyPath = $"{nameof(IHasSubsystem.Subsystem)}.{nameof(Subsystem.UID)}",
                                Value = ss_all_regions
                            },
                            new FilterItem
                            {
                                Connector = FilterStatementConnector.Or,
                                ParenthesizedExpression = new []
                                {
                                    new FilterItem
                                    {
                                        IsNegative = true,
                                        Operation = Operation.Equal,
                                        PropertyPath = $"{nameof(IHasRubricators.RegionId)}",
                                        Value = new []{"null"}
                                    },
                                    new FilterItem
                                    {
                                        Connector = FilterStatementConnector.And,
                                        PropertyPath = $"{nameof(IHasRubricators.Region)}.{nameof(Region.UserModerator)}",
                                        SubProperty = new []
                                        {
                                            new FilterItem
                                            {
                                                PropertyPath = $"{nameof(UserRegion.UserId)}",
                                                Operation = Operation.Equal,
                                                Value = new []{ userId.Value.ToString() }
                                            },
                                            new FilterItem
                                            {
                                                Connector = FilterStatementConnector.And,
                                                PropertyPath = $"{nameof(IHasSubsystem.Subsystem)}.{nameof(Subsystem.Id)}",
                                                Operation = Operation.Equal,
                                                Value = new []{ userId.Value.ToString() }
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                });

                //result = main
                //    .Where(el =>
                //        (ss_all_regions.Contains(((IHasSubsystem)el).Subsystem.UID) || (el.RegionId.HasValue &&
                //                                                                         el.Region.UserModerator.Any(
                //                                                                             um =>
                //                                                                                 um.UserId == userId.Value &&
                //                                                                                 um.Subsystem.Id ==
                //                                                                                 ((IHasSubsystem)el)
                //                                                                                 .Subsystem.Id)))

                //        && (ss_all_rubrics.Contains(((IHasSubsystem)el).Subsystem.UID) || (el.RubricId.HasValue &&
                //                                                                            el.Rubric.UserModerator.Any(
                //                                                                                um =>
                //                                                                                    um.UserId ==
                //                                                                                    userId.Value &&
                //                                                                                    um.Subsystem.Id ==
                //                                                                                    ((IHasSubsystem)el)
                //                                                                                    .Subsystem.Id)))
                //        && (ss_all_territories.Contains(((IHasSubsystem)el).Subsystem.UID) ||
                //            (el.TerritoryId.HasValue &&
                //             el.Territory.UserModerator.Any(um =>
                //                 um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)))
                //        && (ss_all_topics.Contains(((IHasSubsystem)el).Subsystem.UID) || (!el.TopicId.HasValue &&
                //                                                                           el.Topic.UserModerator.Any(
                //                                                                               um =>
                //                                                                                   um.UserId ==
                //                                                                                   userId.Value &&
                //                                                                                   um.Subsystem.Id ==
                //                                                                                   ((IHasSubsystem)el)
                //                                                                                   .Subsystem.Id)))
                //    );
            }
            else if (ss != null)
            {
                //result = main
                //    .Where(el =>
                //        (!el.RegionId.HasValue || ss_all_regions.Contains(ss) ||
                //         el.Region.UserModerator.Any(um =>
                //             um.UserId == userId.Value && um.Subsystem.UID == ss))
                //        && (!el.RubricId.HasValue || ss_all_rubrics.Contains(ss) ||
                //            el.Rubric.UserModerator.Any(um =>
                //                um.UserId == userId.Value && um.Subsystem.UID == ss))
                //        && (!el.TerritoryId.HasValue ||
                //            ss_all_territories.Contains(ss) ||
                //            el.Territory.UserModerator.Any(um =>
                //                um.UserId == userId.Value && um.Subsystem.UID == ss))
                //        && (!el.TopicId.HasValue || ss_all_topics.Contains(ss) ||
                //            el.Topic.UserModerator.Any(um =>
                //                um.UserId == userId.Value && um.Subsystem.UID == ss))
                //    );

            }
            else
            {
                //result = main.Where(el => false);
            }

            return result;
        }

        public static IQueryable<T> FilterByReadAllowedRubricatorModerator1<T>(
            this IQueryable<T> main,
            long? userId,
            string subSystemUID = null
        ) where T : IHasRubricators
        {
            var moderatorIn = main.Where(el => false);
            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Rubric.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Region.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Territory.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Topic.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
            }
            else if (subSystemUID != null)
            {
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Rubric.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Region.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Territory.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Topic.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)));
            }
            return moderatorIn;
        }

        public static IQueryable<T> FilterByReadAllowedSubsystemRights<T>(
            this IQueryable<T> main,
            IEnumerable<Claim> claims,
            List<string> subsystemUIDs,
            string ss = null
            ) where T : IHasRubricators
        {
            var result = main.Where(el => false);
            GetFromClaimsARs(claims, subsystemUIDs, out List<string> ss_all_regions, out List<string> ss_all_rubrics, out List<string> ss_all_territories, out List<string> ss_all_topics);
            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                result = result.Concat(main.Where(el => el.RegionId.HasValue && ss_all_regions.Contains(((IHasSubsystem)el).Subsystem.UID)));
                result = result.Concat(main.Where(el => el.RubricId.HasValue && ss_all_rubrics.Contains(((IHasSubsystem)el).Subsystem.UID)));
                result = result.Concat(main.Where(el => el.TerritoryId.HasValue && ss_all_territories.Contains(((IHasSubsystem)el).Subsystem.UID)));
                result = result.Concat(main.Where(el => el.TopicId.HasValue && ss_all_topics.Contains(((IHasSubsystem)el).Subsystem.UID)));
            }
            else if (ss != null)
            {
                result = result.Concat(main.Where(el => el.RegionId.HasValue && ss_all_regions.Contains(ss)));
                result = result.Concat(main.Where(el => el.RubricId.HasValue && ss_all_rubrics.Contains(ss)));
                result = result.Concat(main.Where(el => el.TerritoryId.HasValue && ss_all_territories.Contains(ss)));
                result = result.Concat(main.Where(el => el.TopicId.HasValue && ss_all_topics.Contains(ss)));
            }
            return result;
        }

        public static void GetFromClaimsARs(IEnumerable<Claim> claims, out List<string> ss_all_regions, out List<string> ss_all_rubrics, out List<string> ss_all_territories, out List<string> ss_all_topics)
        {
            var ss_available = GetFromClaimsSsAvailable(claims);
            ss_all_regions = GetFromClaimsAR(claims, ss_available, $".{SSClaimSuffix.Md.all_regions}");
            ss_all_rubrics = GetFromClaimsAR(claims, ss_available, $".{SSClaimSuffix.Md.all_rubrics}");
            ss_all_territories = GetFromClaimsAR(claims, ss_available, $".{SSClaimSuffix.Md.all_territories}");
            ss_all_topics = GetFromClaimsAR(claims, ss_available, $".{SSClaimSuffix.Md.all_topics}");
        }
        public static void GetFromClaimsARs(IEnumerable<Claim> claims, List<string> subsystemUIDs, out List<string> ss_all_regions, out List<string> ss_all_rubrics, out List<string> ss_all_territories, out List<string> ss_all_topics)
        {
            var ss_available = GetFromClaimsSsAvailable(claims);
            if (subsystemUIDs != null && subsystemUIDs.Count > 0)
            {
                ss_available = ss_available.Intersect(subsystemUIDs).ToList();
            }
            ss_all_regions = GetFromClaimsAR(claims, ss_available, $".{SSClaimSuffix.Md.all_regions}");
            ss_all_rubrics = GetFromClaimsAR(claims, ss_available, $".{SSClaimSuffix.Md.all_rubrics}");
            ss_all_territories = GetFromClaimsAR(claims, ss_available, $".{SSClaimSuffix.Md.all_territories}");
            ss_all_topics = GetFromClaimsAR(claims, ss_available, $".{SSClaimSuffix.Md.all_topics}");
        }

        private static List<string> GetFromClaimsAR(IEnumerable<Claim> claims, List<string> ss_available, string _all_rubrics)
        {
            return claims?
                    .Where(el => el.Type == ClaimName.Moderate)
                    .Where(el => el.Value.EndsWith(_all_rubrics))
                    .Select(el => el.Value.Substring(0, el.Value.Length - _all_rubrics.Length))
                    .Where(el => ss_available.Contains(el))
                    .ToList() ?? new List<string>();
        }

        public static IEnumerable<string> GetFromClaimsSsExecutor(IEnumerable<Claim> claims)
        {
            if (claims == null)
            {
                return new string[0];
            }

            var claimsArray = claims as Claim[] ?? claims.ToArray();
            var ssAvailable = GetFromClaimsSsAvailable(claimsArray);
            var s = $".{SSClaimSuffix.Ss.executor}";
            var ssFbAdm = claimsArray
                .Where(el => el.Type == ClaimName.Subsystem && el.Value.EndsWith(s))
                .Select(el => el.Value.Substring(0, el.Value.Length - s.Length))
                .Where(el => ssAvailable.Contains(el))
                .ToList() ?? new List<string>();
            return ssFbAdm;
        }

        public static List<string> GetFromClaimsSsFeedbackAdmin(IEnumerable<Claim> claims)
        {
            var ssAvailable = GetFromClaimsSsAvailable(claims);
            var s = $".{SSClaimSuffix.Ss.feedback_admin}";
            var ssFbAdm = claims?
                    .Where(el => el.Type == ClaimName.Subsystem)
                    .Where(el => el.Value.EndsWith(s))
                    .Select(el => el.Value.Substring(0, el.Value.Length - s.Length))
                    .Where(el => ssAvailable.Contains(el))
                    .ToList() ?? new List<string>();
            return ssFbAdm;
        }

        public static List<string> GetFromClaimsSsAvailable(IEnumerable<Claim> claims)
        {
            var available = $".{SSClaimSuffix.Ss.available}";
            var ssAvailable = claims?
                .Where(el => el.Type == ClaimName.Subsystem)
                .Where(el => el.Value.EndsWith(available))
                .Select(el => el.Value.Substring(0, el.Value.Length - available.Length))
                .ToList();
            return ssAvailable ?? new List<string>();
        }

        public static List<string> GetFromClaimsSsModerateAvailable(IEnumerable<Claim> claims)
        {
            var enable = $".{SSClaimSuffix.Md.enable}";
            var ssAvailable = claims?
                .Where(el => el.Type == ClaimName.Moderate && el.Value.EndsWith(enable))
                .Select(el => el.Value.Substring(0, el.Value.Length - enable.Length))
                .ToList();
            return ssAvailable ?? new List<string>();
        }

        public static IQueryable<T> FilterByReadAllowedRubricatorModerator<T>(
            this IQueryable<T> main,
            long? userId,
            string subSystemUID = null
            ) where T : IHasRubricators
        {
            var moderatorIn = main.Where(el => false);
            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Rubric.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Region.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Territory.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Topic.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
            }
            else if (subSystemUID != null)
            {
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Rubric.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Region.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Territory.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Topic.UserModerator.Any(um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)));
            }
            return moderatorIn;
        }
        public static IQueryable<T> FilterByReadAllowedRubricatorModerator<T>(
            this IQueryable<T> main,
            long userId,
            List<string> subsystemUIDs,
            string ss = null
            ) where T : IHasRubricators
        {
            var moderatorIn = main.Where(el => false);
            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Rubric.UserModerator.Any(um => um.UserId == userId && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Region.UserModerator.Any(um => um.UserId == userId && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Territory.UserModerator.Any(um => um.UserId == userId && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Topic.UserModerator.Any(um => um.UserId == userId && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
            }
            else if (ss != null)
            {
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Rubric.UserModerator.Any(um => um.UserId == userId && um.Subsystem.UID == ss)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Region.UserModerator.Any(um => um.UserId == userId && um.Subsystem.UID == ss)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Territory.UserModerator.Any(um => um.UserId == userId && um.Subsystem.UID == ss)));
                moderatorIn = moderatorIn.Concat(main.Where(el => el.Topic.UserModerator.Any(um => um.UserId == userId && um.Subsystem.UID == ss)));
            }
            if (subsystemUIDs != null && subsystemUIDs.Count > 0)
            {
                moderatorIn = moderatorIn.Where(el =>
                    el.Rubric.UserModerator.Any(um => subsystemUIDs.Contains(um.Subsystem.UID)) ||
                    el.Region.UserModerator.Any(um => subsystemUIDs.Contains(um.Subsystem.UID)) ||
                    el.Territory.UserModerator.Any(um => subsystemUIDs.Contains(um.Subsystem.UID)) ||
                    el.Topic.UserModerator.Any(um => subsystemUIDs.Contains(um.Subsystem.UID))
                    );
            }
            return moderatorIn;
        }

        public static ExpressionStarter<T> FilterByReadAllowedRubricatorExecutorPredicate<T>(
            long? userId,
            string subSystemUid = null
        ) where T : IHasRubricators
        {
            var predicate = PredicateBuilder.New<T>();
            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                predicate = predicate.And(el =>
                    el.Rubric.UserExecutor.Any(um =>
                        um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)
                    || el.Region.UserExecutor.Any(um =>
                        um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)
                    || el.Territory.UserExecutor.Any(um =>
                        um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)
                    || el.Topic.UserExecutor.Any(um =>
                        um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)
                );
            }
            else if (subSystemUid != null)
            {
                predicate = predicate.And(el =>
                        el.Rubric.UserExecutor.Any(um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUid)
                        || el.Region.UserExecutor.Any(um =>
                            um.UserId == userId.Value && um.Subsystem.UID == subSystemUid)
                        || el.Territory.UserExecutor.Any(um =>
                            um.UserId == userId.Value && um.Subsystem.UID == subSystemUid)
                        || el.Topic.UserExecutor.Any(
                            um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUid)
                    );
            }
            
            return predicate;
        }

        public static IQueryable<T> FilterByReadAllowedRubricatorExecutor<T>(
            this IQueryable<T> main,
            long? userId,
            string subSystemUID = null
            ) where T : IHasRubricators
        {
            IQueryable<T> executorIn;

            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                executorIn = main.Where(el =>
                    el.Rubric.UserExecutor.Any(um =>
                        um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem) el).Subsystem.Id)
                    || el.Region.UserExecutor.Any(um =>
                        um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem) el).Subsystem.Id)
                    || el.Territory.UserExecutor.Any(um =>
                        um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem) el).Subsystem.Id)
                    || el.Topic.UserExecutor.Any(um =>
                        um.UserId == userId.Value && um.Subsystem.Id == ((IHasSubsystem) el).Subsystem.Id)
                );
            }
            else if (subSystemUID != null)
            {
                executorIn = main
                    .Where(el =>
                        el.Rubric.UserExecutor.Any(um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)
                        || el.Region.UserExecutor.Any(um =>
                            um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)
                        || el.Territory.UserExecutor.Any(um =>
                            um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)
                        || el.Topic.UserExecutor.Any(
                            um => um.UserId == userId.Value && um.Subsystem.UID == subSystemUID)
                    );
            }
            else
            {
                executorIn = main.Where(el => false);
            }
            return executorIn;
        }
        public static IQueryable<T> FilterByReadAllowedRubricatorExecutor<T>(
            this IQueryable<T> main,
            long userId,
            List<string> subsystemUIDs,
            string ss = null
            ) where T : IHasRubricators
        {
            var executorIn = main.Where(el => false);
            if (typeof(T).GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                executorIn = executorIn.Concat(main.Where(el => el.Rubric.UserExecutor.Any(um => um.UserId == userId && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                executorIn = executorIn.Concat(main.Where(el => el.Region.UserExecutor.Any(um => um.UserId == userId && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                executorIn = executorIn.Concat(main.Where(el => el.Territory.UserExecutor.Any(um => um.UserId == userId && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
                executorIn = executorIn.Concat(main.Where(el => el.Topic.UserExecutor.Any(um => um.UserId == userId && um.Subsystem.Id == ((IHasSubsystem)el).Subsystem.Id)));
            }
            else if (ss != null)
            {
                executorIn = executorIn.Concat(main.Where(el => el.Rubric.UserExecutor.Any(um => um.UserId == userId && um.Subsystem.UID == ss)));
                executorIn = executorIn.Concat(main.Where(el => el.Region.UserExecutor.Any(um => um.UserId == userId && um.Subsystem.UID == ss)));
                executorIn = executorIn.Concat(main.Where(el => el.Territory.UserExecutor.Any(um => um.UserId == userId && um.Subsystem.UID == ss)));
                executorIn = executorIn.Concat(main.Where(el => el.Topic.UserExecutor.Any(um => um.UserId == userId && um.Subsystem.UID == ss)));
            }
            if (subsystemUIDs != null && subsystemUIDs.Count > 0)
            {
                executorIn = executorIn.Where(el =>
                    el.Rubric.UserExecutor.Any(um => subsystemUIDs.Contains(um.Subsystem.UID)) ||
                    el.Region.UserExecutor.Any(um => subsystemUIDs.Contains(um.Subsystem.UID)) ||
                    el.Territory.UserExecutor.Any(um => subsystemUIDs.Contains(um.Subsystem.UID)) ||
                    el.Topic.UserExecutor.Any(um => subsystemUIDs.Contains(um.Subsystem.UID))
                    );
            }
            return executorIn;
        }

    }
}
