using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.GraphQL.Authorization;
using GraphQL.Types;

namespace BIT.MyCity.Extensions
{
    public static class GqlFieldTypeExtensions
    {
        public static IEnumerable<FieldType> AddPolicy(this IEnumerable<FieldType> fields, params string[] policies)
        {
            if (fields == null)
            {
                return null;
            }

            foreach (var item in fields)
            {
                var list = item.GetPolicies();

                foreach (var policy in policies)
                {
                    if (!list.Contains(policy))
                    {
                        list.Add(policy);
                    }

                    if (!(item is IFieldType field))
                    {
                        continue;
                    }

                    if (field.Description == null)
                    {
                        field.Description = $" (Policy = {policy})";
                    }
                    else
                    {
                        field.Description += $" (Policy = {policy})";
                    }
                }

                item.Metadata[AuthorizationMetadataExtensions.PolicyKey] = list;
            }

            return fields;
        }
    }
}
