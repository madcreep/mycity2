using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using BIT.MyCity.Database;

namespace BIT.MyCity.Extensions
{
  public static class UserManagerExtensions
  {
    public static async Task<User> GetUserByTokenAsync(this UserManager<User> userManager, ClaimsPrincipal principal)
    {
      var claim = principal.Claims.SingleOrDefault(el => el.Type == JwtRegisteredClaimNames.Jti);

      if (claim == null || string.IsNullOrWhiteSpace(claim.Value))
      {
        return null;
      }

      var result = await userManager.FindByIdAsync(claim.Value);

      return result;
    }
  }
}
