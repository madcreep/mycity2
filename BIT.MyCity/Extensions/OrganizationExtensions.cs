using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BIT.MyCity.Database;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Extensions
{
    public static class OrganizationExtensions
    {
        public static IQueryable<UserOrganization> IncludeByKeys([NotNull] this IQueryable<UserOrganization> organizations,
            GraphQlKeyDictionary keys)
        {
            if (keys == null)
            {
                return organizations;
            }

            if (keys.ContainsKey(nameof(UserOrganization.ChiefIds).ToJsonFormat())
                || keys.ContainsKey(nameof(UserOrganization.UserIds).ToJsonFormat()))
            {
                organizations = organizations
                    .Include(el => el.Users2Organizations);
            }

            if (keys.ContainsKey(nameof(UserOrganization.Addresses).ToJsonFormat()))
            {
                organizations = organizations
                    .Include(el => el.Addresses);
            }

            if (keys.ContainsKey(nameof(UserOrganization.Phones).ToJsonFormat()))
            {
                organizations = organizations
                    .Include(el => el.OrganizationPhones)
                    .ThenInclude(el => el.Phone);
            }

            if (keys.ContainsKey(nameof(UserOrganization.Parent).ToJsonFormat()))
            {
                var subKeys = keys[nameof(UserOrganization.Parent).ToJsonFormat()];

                organizations = organizations
                    .Include(el => el.Parent);

                if (subKeys.ContainsKey(nameof(UserOrganization.Addresses).ToJsonFormat()))
                {
                    organizations = organizations
                        .Include(el => el.Parent)
                        .ThenInclude(el=>el.Addresses);
                }

                if (subKeys.ContainsKey(nameof(UserOrganization.Phones).ToJsonFormat()))
                {
                    organizations = organizations
                        .Include(el => el.Parent)
                        .ThenInclude(el => el.OrganizationPhones)
                        .ThenInclude(el => el.Phone);
                }
            }

            if (keys.ContainsKey(nameof(UserOrganization.Branches).ToJsonFormat()))
            {
                var subKeys = keys[nameof(UserOrganization.Branches).ToJsonFormat()];

                organizations = organizations
                    .Include(el => el.Branches);

                if (subKeys.ContainsKey(nameof(UserOrganization.Addresses).ToJsonFormat()))
                {
                    organizations = organizations
                        .Include(el => el.Branches)
                        .ThenInclude(el => el.Addresses);
                }

                if (subKeys.ContainsKey(nameof(UserOrganization.Phones).ToJsonFormat()))
                {
                    organizations = organizations
                        .Include(el => el.Branches)
                        .ThenInclude(el => el.OrganizationPhones)
                        .ThenInclude(el=>el.Phone);
                }
            }

            return organizations;
        }
    }
}
