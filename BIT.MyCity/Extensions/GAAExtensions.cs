using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Subsystems;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BIT.MyCity.Extensions
{
    public static class GAAExtensions
    {
        #region IHasRubricators
        public static BaseConstants.AllowedActionFlags GetAMFSSAllRub<T>(
            this T obj,
            IEnumerable<Claim> claims,
            string ss
            ) where T : class, IHasRubricators
        {
            var flags = BaseConstants.AllowedActionFlags.None;

            if (obj == null || claims == null)
            {
                return flags;
            }

            if ((!obj.RubricId.HasValue || !claims.Any(el =>
                    el.Type == ClaimName.Moderate && el.Value == $"{ss}.{SSClaimSuffix.Md.all_rubrics}")) ||
                (!obj.RegionId.HasValue || !claims.Any(el =>
                    el.Type == ClaimName.Moderate && el.Value == $"{ss}.{SSClaimSuffix.Md.all_regions}")) ||
                (!obj.TerritoryId.HasValue || !claims.Any(el =>
                    el.Type == ClaimName.Moderate && el.Value == $"{ss}.{SSClaimSuffix.Md.all_territories}")) ||
                (!obj.TopicId.HasValue || !claims.Any(el =>
                    el.Type == ClaimName.Moderate && el.Value == $"{ss}.{SSClaimSuffix.Md.all_topics}")))
            {
                return flags;
            }

            flags |= BaseConstants.AllowedActionFlags.Read;
            flags |= BaseConstants.AllowedActionFlags.Moderate;

            return flags;
        }
        public static async Task<BaseConstants.AllowedActionFlags> GetAMFUMRub<T>(
            this T obj,
            long? userId,
            DocumentsContext ctx
            ) where T : class, IHasRubricators
        {
            BaseConstants.AllowedActionFlags flags = BaseConstants.AllowedActionFlags.None;
            if (obj == null || !userId.HasValue)
            {
                return flags;
            }
            do
            {
                if (obj.RegionId.HasValue)
                {
                    await obj.LoadReference(obj.Region, ctx);

                    if (obj.Region != null)
                    {

                        await ctx.Entry(obj.Region)
                            .Collection(el => el.UserModerator)
                            .Query()
                            .Where(el => el.RegionId == obj.RegionId.Value && el.UserId == userId.Value)
                            .LoadAsync();

                        if (obj.Region.UserModerator?.FirstOrDefault(el =>
                            obj.RegionId != null && el.UserId == userId.Value && el.RegionId == obj.RegionId.Value) != null)
                        {
                            flags |= BaseConstants.AllowedActionFlags.Read;
                            flags |= BaseConstants.AllowedActionFlags.Moderate;
                            break;
                        }
                    }
                }
                if (obj.RubricId.HasValue)
                {
                    await obj.LoadReference(obj.Rubric, ctx);
                    if (obj.Rubric != null)
                    {
                        await ctx.Entry(obj.Rubric).Collection(el => el.UserModerator).Query().Where(el => el.RubricId == obj.RubricId.Value && el.UserId == userId.Value).LoadAsync();
                        if (obj.Rubric.UserModerator?.FirstOrDefault(el => el.UserId == userId.Value && el.RubricId == obj.RubricId.Value) != null)
                        {
                            flags |= BaseConstants.AllowedActionFlags.Read;
                            flags |= BaseConstants.AllowedActionFlags.Moderate;
                            break;
                        }
                    }
                }
                if (obj.TerritoryId.HasValue)
                {
                    await obj.LoadReference(obj.Territory, ctx);
                    if (obj.Territory != null)
                    {
                        await ctx.Entry(obj.Territory).Collection(el => el.UserModerator).Query().Where(el => el.TerritoryId == obj.TerritoryId.Value && el.UserId == userId.Value).LoadAsync();
                        if (obj.Territory.UserModerator?.FirstOrDefault(el => el.UserId == userId.Value && el.TerritoryId == obj.TerritoryId.Value) != null)
                        {
                            flags |= BaseConstants.AllowedActionFlags.Read;
                            flags |= BaseConstants.AllowedActionFlags.Moderate;
                            break;
                        }
                    }
                }
                if (obj.TopicId.HasValue)
                {
                    await obj.LoadReference(obj.Topic, ctx);
                    if (obj.Topic != null)
                    {
                        await ctx.Entry(obj.Topic).Collection(el => el.UserModerator).Query().Where(el => el.TopicId == obj.TopicId.Value && el.UserId == userId.Value).LoadAsync();
                        if (obj.Topic.UserModerator?.FirstOrDefault(el => el.UserId == userId.Value && el.TopicId == obj.TopicId.Value) != null)
                        {
                            flags |= BaseConstants.AllowedActionFlags.Read;
                            flags |= BaseConstants.AllowedActionFlags.Moderate;
                            break;
                        }
                    }
                }

            } while (false);
            return flags;
        }
        #endregion

        public static BaseConstants.AllowedActionFlags GetAASS(
            string create,
            IEnumerable<Claim> claims,
            string ss
            )
        {
            var flags = BaseConstants.AllowedActionFlags.None;

            if (claims == null)
            {
                return flags;
            }

            var claimsArray = claims as Claim[] ?? claims.ToArray();

            if (claimsArray.FirstOrDefault(el =>
                    el.Type == ClaimName.Subsystem && el.Value == $"{ss}.{SSClaimSuffix.Ss.available}") ==
                null)
            {
                return flags;
            }
            
            if (claimsArray.FirstOrDefault(el => el.Type == ClaimName.Moderate && el.Value == $"{ss}.{SSClaimSuffix.Md.super}") != null)
            {
                flags |= BaseConstants.AllowedActionFlags.Read;
                flags |= BaseConstants.AllowedActionFlags.Moderate;
                flags |= BaseConstants.AllowedActionFlags.Edit;
            }
            if (claimsArray.FirstOrDefault(el => el.Type == ClaimName.Moderate && el.Value == $"{ss}.{SSClaimSuffix.Md.enable}") != null)
            {
                flags |= BaseConstants.AllowedActionFlags.Read;
                flags |= BaseConstants.AllowedActionFlags.Moderate;
            }

            if (
                claimsArray.FirstOrDefault(el => el.Type == ClaimName.Subsystem && el.Value == $"{ss}.{create}") != null
            )
            {
                flags |= BaseConstants.AllowedActionFlags.Create;
            }

            return flags;
        }

        public static async Task<BaseConstants.AllowedActionFlags> GetAllowedModerationActions<T>(
            this T obj,
            long? userId,
            IEnumerable<Claim> claims,
            SettingsManager sm,
            DocumentsContext ctx,
            string subSystemUID
            ) where T : class
        {
            var flags = BaseConstants.AllowedActionFlags.None;
            if (obj == null || userId == null || claims == null)
            {
                return flags;
            }
            string ss = null;
            if (obj.GetType().GetInterfaces().Contains(typeof(IHasSubsystem)))
            {
                await ((IHasSubsystem)obj).LoadReference(((IHasSubsystem)obj).Subsystem, ctx);
                ss = ((IHasSubsystem)obj).Subsystem?.UID ?? subSystemUID;
            }
            else
            {
                ss = subSystemUID;
            }
            do
            {
                flags |= obj.GetAllowedSuperAdminActions(claims);
                if (ss != null)
                {
                    if (claims.Any(el => el.Type == ClaimName.Subsystem && el.Value == $"{ss}.{SSClaimSuffix.Ss.available}"))
                    {
                        if (obj.GetType().GetInterfaces().Contains(typeof(IHasRubricators)))
                        {
                            flags |= GetAMFSSAllRub((IHasRubricators)obj, claims, ss);
                        }
                        flags |= GetAASS(
                            obj.GetType() == typeof(Appeal) ? SSClaimSuffix.Ss.create_appeal :
                            (obj.GetType() == typeof(Message) ? SSClaimSuffix.Ss.create_message :
                            (obj.GetType() == typeof(Vote) ? SSClaimSuffix.Ss.create_voting :
                            (obj.GetType() == typeof(VoteRoot) ? SSClaimSuffix.Ss.create_voting :
                            (obj.GetType() == typeof(News) ? SSClaimSuffix.Ss.create_news :
                            "default"))))
                            , claims, ss);
                    }
                }
                else
                {
                    if (obj.GetType().GetInterfaces().Contains(typeof(IHasRubricators)))
                    {
                        flags |= await GetAMFUMRub((IHasRubricators)obj, userId, ctx);
                    }
                }
                if (obj.GetType() == typeof(Appeal))
                {
                    flags |= await AppealExtensions.GetAMFUMA((Appeal)(object)obj, userId, ctx);
                }
            } while (false);
            obj.GetAMAFinal(sm, ref flags, ss);
            return flags;
        }

        public static void GetAMAFinal<T>(this T obj, SettingsManager sm, ref BaseConstants.AllowedActionFlags flags, string ss) where T : class
        {
            if (!flags.HasFlag(BaseConstants.AllowedActionFlags.Moderate))
            {
                if (ss != null && !BaseConstants.SubsystemIsEnabledInSettings(sm, ss))
                {
                    flags = BaseConstants.AllowedActionFlags.None;
                }
                if (obj.GetType().GetInterfaces().Contains(typeof(IDeletable)))
                {
                    if (((IDeletable)obj).Deleted)
                    {
                        flags = BaseConstants.AllowedActionFlags.None;
                    }
                }
                if (obj.GetType().GetInterfaces().Contains(typeof(IDisconnectableEntity)))
                {
                    if (((IDisconnectableEntity)obj).Disconnected)
                    {
                        flags = BaseConstants.AllowedActionFlags.None;
                    }
                }
            }
        }

        public static BaseConstants.AllowedActionFlags GetAllowedSuperAdminActions<T>(
            this T obj,
            ClaimsPrincipal claimsPrincipal
            )
        {
            BaseConstants.AllowedActionFlags flags = BaseConstants.AllowedActionFlags.None;
            if (obj == null || claimsPrincipal == null)
            {
                return flags;
            }
            if (claimsPrincipal.Claims.Any(el => el.Type == ClaimName.SuperAdmin))
            {
                flags |= BaseConstants.AllowedActionFlags.All;
            }
            return flags;
        }
        public static BaseConstants.AllowedActionFlags GetAllowedSuperAdminActions<T>(
            this T obj,
            IEnumerable<Claim> claims
            )
        {
            BaseConstants.AllowedActionFlags flags = BaseConstants.AllowedActionFlags.None;
            if (obj == null || claims == null)
            {
                return flags;
            }
            if (claims.Any(el => el.Type == ClaimName.SuperAdmin))
            {
                flags |= BaseConstants.AllowedActionFlags.All;
            }
            return flags;
        }

        public static BaseConstants.AllowedActionFlags GetAllowedModerationActions<T>(
            this T obj,
            long? userId,
            ClaimsPrincipal claimsPrincipal
            )
        {
            BaseConstants.AllowedActionFlags flags = BaseConstants.AllowedActionFlags.None;
            if (obj == null || userId == null || claimsPrincipal == null)
            {
                return flags;
            }
            flags |= obj.GetAllowedSuperAdminActions(claimsPrincipal);
            if (claimsPrincipal.Claims.Any(el => el.Type == ClaimName.Moderate))
            {
                flags |= BaseConstants.AllowedActionFlags.Read | BaseConstants.AllowedActionFlags.Edit | BaseConstants.AllowedActionFlags.Moderate;
            }
            return flags;
        }
        public static BaseConstants.AllowedActionFlags GetAllowedActions<T>(
            this T obj,
            long? userId,
            ClaimsPrincipal claimsPrincipal
            )
        {
            BaseConstants.AllowedActionFlags flags = BaseConstants.AllowedActionFlags.None;
            if (obj == null)
            {
                return flags;
            }
            flags |= obj.GetAllowedSuperAdminActions(claimsPrincipal);
            flags |= BaseConstants.AllowedActionFlags.Read;
            flags |= obj.GetAllowedModerationActions(userId, claimsPrincipal);
            return flags;
        }

    }
}
