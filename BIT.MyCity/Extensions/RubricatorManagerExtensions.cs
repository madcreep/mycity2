using System.Linq;
using BIT.MyCity.Database;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Extensions
{
    public static class RubricatorManagerExtensions
    {
        //public static async Task<IEnumerable<long>> GetModerators(this Region obj, DbSet<Region> db)
        //{
        //    while (obj != null)
        //    {
        //        if (obj.UserModerator.Count > 0)
        //        {
        //            return obj.UserModerator.Select(el => el.UserId);
        //        }

        //        if (obj.ParentId > 0 && obj.ParentType == obj.Type)
        //        {
        //            obj = await db
        //                .Where(el => !el.Deleted)
        //                .Where(el => el.Id == obj.ParentId)
        //                .Include(el => el.UserModerator)
        //                .SingleOrDefaultAsync();
        //        }
        //        else
        //        {
        //            break;
        //        }
        //    }
        //    return Array.Empty<long>();
        //}

        //public static async Task<IEnumerable<long>> GetModerators(this Rubric obj, DbSet<Rubric> db)
        //{
        //    while (obj != null)
        //    {
        //        if (obj.UserModerator.Count > 0)
        //        {
        //            return obj.UserModerator.Select(el => el.UserId).ToList();
        //        }
        //        else if (obj.ParentId > 0 && obj.ParentType == obj.Type)
        //        {
        //            obj = await db
        //                .Where(el => !el.Deleted)
        //                .Where(el => el.Id == obj.ParentId)
        //                .Include(el => el.UserModerator)
        //                .SingleOrDefaultAsync();
        //        }
        //        else
        //        {
        //            break;
        //        }
        //    }
        //    return Array.Empty<long>();
        //}

        //public static async Task<IEnumerable<long>> GetModerators(this Territory obj, DbSet<Territory> db)
        //{
        //    while (obj != null)
        //    {
        //        if (obj.UserModerator.Count > 0)
        //        {
        //            return obj.UserModerator.Select(el => el.UserId).ToList();
        //        }
        //        else if (obj.ParentId > 0 && obj.ParentType == obj.Type)
        //        {
        //            obj = await db
        //                .Where(el => !el.Deleted)
        //                .Where(el => el.Id == obj.ParentId)
        //                .Include(el => el.UserModerator)
        //                .SingleOrDefaultAsync();
        //        }
        //        else
        //        {
        //            break;
        //        }
        //    }
        //    return Array.Empty<long>();
        //}

        //public static async Task<IEnumerable<long>> GetModerators(this Topic obj, DbSet<Topic> db)
        //{
        //    while (obj != null)
        //    {
        //        if (obj.UserModerator.Count > 0)
        //        {
        //            return obj.UserModerator.Select(el => el.UserId).ToList();
        //        }
        //        else if (obj.ParentId > 0 && obj.ParentType == obj.Type)
        //        {
        //            obj = await db
        //                .Where(el => !el.Deleted)
        //                .Where(el => el.Id == obj.ParentId)
        //                .Include(el => el.UserModerator)
        //                .SingleOrDefaultAsync();
        //        }
        //        else
        //        {
        //            break;
        //        }
        //    }
        //    return Array.Empty<long>();
        //}

        public static IQueryable<IRubricator> IncludeByNotMappedFields<T>(this IQueryable<IRubricator> obj,
            GraphQlKeyDictionary fields)
            where T : IRubricator
        {
            if (fields == null || obj == null)
            {
                return obj;
            }

            if (fields.ContainsKey(nameof(IRubricatorFields.Moderators).ToJsonFormat()) ||
                fields.ContainsKey(nameof(IRubricatorFields.ModeratorsSet).ToJsonFormat()))
            {
                obj = obj
                    .Include("UserModerator.User.IdentityClaims")
                    .Include("UserModerator.User.UserRole.Role.IdentityClaims")
                    .Include("UserModerator.Subsystem")
                    .Include("Rubricator2Subsystems.Subsystem");
            }

            if (fields.ContainsKey(nameof(IRubricatorFields.ExecutorsOfSubsystems).ToJsonFormat()))
            {
                obj = obj
                    .Include("UserExecutor.User")
                    .Include("UserExecutor.Subsystem");
            }

            if (fields.ContainsKey(nameof(IRubricatorFields.Executors).ToJsonFormat()))
            {
                obj = obj
                    .Include("UserExecutor.User");
            }

            if (fields.ContainsKey(nameof(IRubricatorFields.Subsystems).ToJsonFormat()))
            {
                obj = obj
                    .Include("Rubricator2Subsystems.Subsystem");
            }

            return obj;
        }

        //public static IQueryable<Region> IncludeByNotMappedFields(this IQueryable<Region> obj, GraphQlKeyDictionary fields)
        //{
        //    if (fields != null && obj != null)
        //    {
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Moderators).ToJsonFormat()) || fields.ContainsKey(nameof(IRubricatorFields.ModeratorsSet).ToJsonFormat()))
        //        {
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.User)
        //                .ThenInclude(el => el.IdentityClaims);
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.User)
        //                .ThenInclude(el => el.UserRole)
        //                .ThenInclude(el => el.Role)
        //                .ThenInclude(el => el.IdentityClaims);
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.Subsystem);
        //            obj = obj.Include(el => el.Rubricator2Subsystems)
        //                .ThenInclude(el => el.Subsystem);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.ExecutorsOfSubsystems).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.User);
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.Subsystem);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Executors).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.User);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Subsystems).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.Rubricator2Subsystems).ThenInclude(el => el.Subsystem);
        //        }
        //    }
        //    return obj;
        //}
        //public static IQueryable<Rubric> IncludeByNotMappedFields(this IQueryable<Rubric> obj, GraphQlKeyDictionary fields)
        //{
        //    if (fields != null && obj != null)
        //    {
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Moderators).ToJsonFormat()) || fields.ContainsKey(nameof(IRubricatorFields.ModeratorsSet).ToJsonFormat()))
        //        {
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.User)
        //                .ThenInclude(el => el.IdentityClaims);
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.User)
        //                .ThenInclude(el => el.UserRole)
        //                .ThenInclude(el => el.Role)
        //                .ThenInclude(el => el.IdentityClaims);
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.Subsystem);
        //            obj = obj.Include(el => el.Rubricator2Subsystems)
        //                .ThenInclude(el => el.Subsystem);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.ExecutorsOfSubsystems).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.User);
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.Subsystem);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Executors).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.User);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Subsystems).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.Rubricator2Subsystems).ThenInclude(el => el.Subsystem);
        //        }
        //    }
        //    return obj;
        //}
        //public static IQueryable<Territory> IncludeByNotMappedFields(this IQueryable<Territory> obj, GraphQlKeyDictionary fields)
        //{
        //    if (fields != null && obj != null)
        //    {
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Moderators).ToJsonFormat()) || fields.ContainsKey(nameof(IRubricatorFields.ModeratorsSet).ToJsonFormat()))
        //        {
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.User)
        //                .ThenInclude(el => el.IdentityClaims);
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.User)
        //                .ThenInclude(el => el.UserRole)
        //                .ThenInclude(el => el.Role)
        //                .ThenInclude(el => el.IdentityClaims);
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.Subsystem);
        //            obj = obj.Include(el => el.Rubricator2Subsystems)
        //                .ThenInclude(el => el.Subsystem);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.ExecutorsOfSubsystems).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.User);
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.Subsystem);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Executors).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.User);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Subsystems).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.Rubricator2Subsystems).ThenInclude(el => el.Subsystem);
        //        }
        //    }
        //    return obj;
        //}
        //public static IQueryable<Topic> IncludeByNotMappedFields(this IQueryable<Topic> obj, GraphQlKeyDictionary fields)
        //{
        //    if (fields != null && obj != null)
        //    {
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Moderators).ToJsonFormat()) || fields.ContainsKey(nameof(IRubricatorFields.ModeratorsSet).ToJsonFormat()))
        //        {
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.User)
        //                .ThenInclude(el => el.IdentityClaims);
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.User)
        //                .ThenInclude(el => el.UserRole)
        //                .ThenInclude(el => el.Role)
        //                .ThenInclude(el => el.IdentityClaims);
        //            obj = obj
        //                .Include(el => el.UserModerator)
        //                .ThenInclude(el => el.Subsystem);
        //            obj = obj.Include(el => el.Rubricator2Subsystems)
        //                .ThenInclude(el => el.Subsystem);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.ExecutorsOfSubsystems).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.User);
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.Subsystem);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Executors).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.UserExecutor).ThenInclude(el => el.User);
        //        }
        //        if (fields.ContainsKey(nameof(IRubricatorFields.Subsystems).ToJsonFormat()))
        //        {
        //            obj = obj.Include(el => el.Rubricator2Subsystems).ThenInclude(el => el.Subsystem);
        //        }
        //    }
        //    return obj;
        //}
    }
}
