using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using log4net;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Extensions
{
    public static class AttachedFileExtensions
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(AttachedFileExtensions));

        public static async Task<BaseConstants.AllowedActionFlags> GetAllowedActions(
            this AttachedFile file,
            long? principalId,
            System.Security.Claims.ClaimsPrincipal principal,
            DocumentsContext ctx,
            SettingsManager sm)
        {
            var flags = BaseConstants.AllowedActionFlags.None;
            do
            {
                if (file == null)
                {
                    break;
                }
                if (file.Sequre)
                {
                    break;
                }
                
                if (principal?.Claims != null && principal.Claims.Any(el => el.Type == ClaimName.Service && el.Value == "delo"))
                {
                    flags = BaseConstants.AllowedActionFlags.All;
                    break;
                }

                if (file.IsPublic)
                {
                    flags = BaseConstants.AllowedActionFlags.Read;
                }

                var parentInfo = await GetFileParentInfo(ctx, file);

                _log.Debug($"FileParentInfo: {string.Join(" | ", parentInfo.Select(el=>$"{el.Key}:{string.Join(", ", el.Value)}"))}");

                if (parentInfo.Any())
                {
                    foreach (var parentType in parentInfo.Keys)
                    {
                        switch (parentType)
                        {
                            case nameof(Appeal):
                            {
                                var appealFlags = BaseConstants.AllowedActionFlags.None;

                                foreach (var parentId in parentInfo[parentType])
                                {
                                    appealFlags |= await AppealExtensions.GetAllowedActions<Appeal>(parentId,
                                        principalId, principal, ctx, sm);

                                }

                                flags |= appealFlags;

                                if (appealFlags.HasFlag(BaseConstants.AllowedActionFlags.Edit))
                                {
                                    flags |= BaseConstants.AllowedActionFlags.Create;
                                }

                                break;
                            }
                            case nameof(Message):
                            {
                                var messageFlags = BaseConstants.AllowedActionFlags.None;

                                foreach (var parentId in parentInfo[parentType])
                                {
                                    messageFlags |= await MessageExtensions.GetAllowedActions(parentId,
                                        principalId, principal, ctx, sm);

                                }
                                
                                flags |= messageFlags;

                                if (messageFlags.HasFlag(BaseConstants.AllowedActionFlags.Edit))
                                {
                                    flags |= BaseConstants.AllowedActionFlags.Create;
                                }

                                break;
                            }
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(file.ParentType))
                    {
                        if (principalId.HasValue)
                        {
                            flags |= BaseConstants.AllowedActionFlags.Create;
                        }
                    }
                }

                //switch (file.ParentType)
                //{
                //    case nameof(Appeal):
                //    {
                //        var appealFlags = await AppealExtensions.GetAllowedActions<Appeal>(file.ParentId, principalId, principal, ctx, sm);

                //        flags |= appealFlags;

                //        if (appealFlags.HasFlag(BaseConstants.AllowedActionFlags.Edit))
                //        {
                //            flags |= BaseConstants.AllowedActionFlags.Create;
                //        }

                //        break;
                //    }
                //    case nameof(Message):
                //    {
                //        var messageFlags = await MessageExtensions.GetAllowedActions<Message>(file.ParentId, principalId, principal, ctx, sm);

                //        flags |= messageFlags;

                //        if (messageFlags.HasFlag(BaseConstants.AllowedActionFlags.Edit))
                //        {
                //            flags |= BaseConstants.AllowedActionFlags.Create;
                //        }

                //        break;
                //    }
                //    default:
                //    {
                //        if (string.IsNullOrEmpty(file.ParentType))
                //        {
                //            if (principalId.HasValue)
                //            {
                //                flags |= BaseConstants.AllowedActionFlags.Create;
                //            }
                //        }

                //        break;
                //    }
                //}

                if (principalId.HasValue)
                {
                    //var user = await _um.CurrentUser();
                    //if (user.NormalizeFullName == "DELOSERVICE")
                    //{
                    //    break;
                    //}
                    if (principal != null && principal.HasClaim(el => el.Type == ClaimName.SuperAdmin))
                    {
                        flags |= BaseConstants.AllowedActionFlags.All;
                    }
                }
                /*обрабатываем новый флаг IsPublic - публичный файл
                 если установлен доступ по чтению и
                 флаг IsPublic сброшен и
                 не установлен доступ по модерации и
                 юзер не является хозяином файла
                - тогда сбрасываем доступ по чтению
                                 */
                if (
                    flags.HasFlag(BaseConstants.AllowedActionFlags.Read) &&
                    !file.IsPublic &&
                    !flags.HasFlag(BaseConstants.AllowedActionFlags.AuthorOf) &&
                    !flags.HasFlag(BaseConstants.AllowedActionFlags.Moderate) &&
                    (!principalId.HasValue || principalId != file.UploadedById)
                )
                {
                    flags &= ~BaseConstants.AllowedActionFlags.Read;
                }
                /*обрабатываем флаг Private - приватный файл
                 чтение разрешаем только хозяину файла*/
                if (
                    flags.HasFlag(BaseConstants.AllowedActionFlags.Read) &&
                    file.Private &&
                    (!principalId.HasValue || principalId != file.UploadedById)
                )
                {
                    flags &= ~BaseConstants.AllowedActionFlags.Read;
                }
            } while (false);

            _log.Debug($"AttachedFileFlags: {flags}");

            return flags;
        }

        public static async Task<bool> IsReadAllowed(
            this AttachedFile file,
            long? principalId,
            System.Security.Claims.ClaimsPrincipal principal,
            DocumentsContext ctx,
            SettingsManager sm)
        {
            return (await file.GetAllowedActions(principalId, principal, ctx, sm)).HasFlag(BaseConstants.AllowedActionFlags.Read);
        }

        private static async Task<Dictionary<string, long[]>> GetFileParentInfo(DocumentsContext ctx, AttachedFile file)
        {
            const string appealName = nameof(Appeal);
            const string messageName = nameof(Message);

            var d = await ctx.AttachedFilesToAppeals
                .Where(el => el.AttachedFileId == file.Id)
                .Select(el => new {EntityType = appealName, EntityId = el.AppealId})
                .Union(ctx.AttachedFilesToMessages
                    .Where(el => el.AttachedFileId == file.Id)
                    .Select(el => new {EntityType = messageName, EntityId = el.MessageId}))
                .ToArrayAsync();
                return d
                .GroupBy(el => el.EntityType)
                .ToDictionary(el => el.Key,
                    el => el.Select(x => x.EntityId).ToArray());
        }

    }
}
