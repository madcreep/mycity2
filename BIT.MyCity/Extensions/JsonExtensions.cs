using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BIT.MyCity.Extensions
{
    public static class JsonExtensions
    {
        public static bool IsJson(this string json)
        {
            try
            {
                var settings = new JsonSerializerSettings
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                    Formatting = Formatting.None,
                    Converters = new List<JsonConverter> {
                        new Newtonsoft.Json.Converters.StringEnumConverter()
                    }
                };

                JsonConvert.DeserializeObject(json, settings);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static T DeserializeJson<T>(this string json)
        {
            if (string.IsNullOrWhiteSpace(json))
            {
                return default;
            }

            var settings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Ignore,
                Converters = new List<JsonConverter> {
                    new Newtonsoft.Json.Converters.StringEnumConverter()
                }
            };

            return JsonConvert.DeserializeObject<T>(json, settings);
        }

        public static bool TryDeserializeJson<T>(this string json, out T result)
        {
            try
            {
                var settings = new JsonSerializerSettings
                {
                    MissingMemberHandling = MissingMemberHandling.Ignore,
                    Converters = new List<JsonConverter> {
                        new Newtonsoft.Json.Converters.StringEnumConverter()
                    }
                };

                result = JsonConvert.DeserializeObject<T>(json, settings);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

                result = default;

                return false;
            }
        }

        public static T TryDeserializeJson<T>(this string json)
        {
            return json.TryDeserializeJson<T>(out var result) ? result : default;
        }

        public static string SerializeJson(this object obj, bool format = true)
        {
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                Formatting = format ? Formatting.Indented : Formatting.None,
                Converters = new List<JsonConverter> {
                    new Newtonsoft.Json.Converters.StringEnumConverter()
                }
            };

            return JsonConvert.SerializeObject(obj, settings);
        }
    }
}
