using System.Linq;
using BIT.MyCity.Database;
using BIT.MyCity.Model;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Extensions
{
    public static class RoleExtensions
    {
        public static IQueryable<Role> ById(this IQueryable<Role> roles, long id)
        {
            return roles.Where(el => el.Id == id);
        }

        public static IQueryable<Role> IncludeByKeys(this IQueryable<Role> roles, GraphQlKeyDictionary keys)
        {
            if (keys == null)
            {
                return roles;
            }

            var result = roles;

            if (keys.ContainsKey(nameof(Role.IdentityClaims).ToJsonFormat())
                || keys.ContainsKey(nameof(Role.Claims).ToJsonFormat()))
            {
                result = result
                    .Include(el => el.IdentityClaims);
            }

            if (keys.ContainsKey(nameof(Role.Users).ToJsonFormat()))
            {
                result = result
                    .Include(el => el.UserRole)
                    .ThenInclude(el => el.User);
            }

            return result;
        }

        public static IQueryable<Role> Filtered(this IQueryable<Role> roles, RoleFilter filter)
        {
            if (filter == null)
            {
                return roles;
            }

            var result = roles;

            if (filter.Id != null && filter.Id.Any())
            {
                result = result
                    .Where(el => filter.Id.Contains(el.Id));
            }

            if (filter.UserId.HasValue)
            {
                result = result
                    .Where(el => el.UserRole.Any(x => x.UserId == filter.UserId.Value));
            }

            if (filter.Disconnected.HasValue)
            {
                result = filter.Disconnected.Value
                    ? result.Where(el => el.DisconnectedTimestamp != null)
                    : result.Where(el => el.DisconnectedTimestamp == null);
            }

            if (filter.Editable.HasValue)
            {
                result = result
                    .Where(el => el.Editable == filter.Editable.Value);
            }

            return result;
        }
    }
}
