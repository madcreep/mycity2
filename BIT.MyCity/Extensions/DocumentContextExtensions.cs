using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Extensions
{
    public static class DocumentContextExtensions
    {
        public static IQueryable<T> DbSet<T>(this DocumentsContext ctx, Type entityType)
        {
            var tableName = ctx.Model
                .FindEntityType(entityType)?
                .GetTableName();

            if (tableName == null || !(ctx.GetType().GetProperty(tableName)?.GetValue(ctx) is IQueryable<T> result))
            {
                return null;
            }

            return result;
        }

        public static IQueryable<T> DbSet<T>(this DocumentsContext ctx, string typeName)
        {
            if (string.IsNullOrWhiteSpace(typeName))
            {
                return null;
            }

            var tableName = ctx.Model
                .GetEntityTypes()
                .FirstOrDefault(el => el.Name.EndsWith($".{typeName}"))?
                .GetTableName();

            if (tableName == null || !(ctx.GetType().GetProperty(tableName)?.GetValue(ctx) is IQueryable<T> result))
            {
                return null;
            }

            return result;
        }

        public static IQueryable<T> DbSet<T>(this DocumentsContext ctx)
            where T: class
        {
            return ctx.DbSet<T>(typeof(T));
        }

        public static async Task SaveWeightAsync<T>(this DocumentsContext ctx, T obj) where T : class, IBaseIdEntity
        {
            if (obj.Weight == 0)
            {
                if (obj.Id == 0)
                {
                    await ctx.SaveChangesAsync();
                }

                obj.Weight = obj.Id;

                await ctx.SaveChangesAsync();
            }
        }

        public static void SaveWeight<T>(this DocumentsContext ctx, T obj) where T : class, IBaseIdEntity
        {
            if (obj.Weight == 0)
            {
                if (obj.Id == 0)
                {
                    ctx.SaveChanges();
                }
                obj.Weight = obj.Id;
            }
            ctx.SaveChanges();
        }
    }
}
