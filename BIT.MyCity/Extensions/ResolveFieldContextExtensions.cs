using GraphQL;
using GraphQL.Language.AST;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace BIT.MyCity.Extensions
{
    public static class ResolveFieldContextExtensions
    {
        public static GraphQlKeyDictionary SubFieldsKeys(this IResolveFieldContext context)
        {
            return GetSubKeys(context.FieldAst.SelectionSet, context.Fragments);
        }

        public static GraphQlKeyDictionary Keys(this IResolveFieldContext context, string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            var result = new GraphQlKeyDictionary();

            if (!context.Arguments.ContainsKey(key))
            {
                return result;
            }

            var fields = context.Arguments[key];

            var keys = (IEnumerable<string>)TypeDescriptor
                .GetProperties(fields)?
                .Find("Keys", true)?
                .GetValue(fields);

            if (keys == null)
            {
                return result;
            }

            foreach (var itemKey in keys)
            {
                result.Add(itemKey, new GraphQlKeyDictionary());
            }

            return result;
        }

        private static GraphQlKeyDictionary GetSubKeys(SelectionSet fields, Fragments fragments)
        {
            var result = new GraphQlKeyDictionary();
            foreach (var field in fields.Selections)
            {
                switch (field)
                {
                    case Field f:
                        {
                            if (!result.ContainsKey(f.Name))
                            {
                                result.Add(f.Name, GetSubKeys(f.SelectionSet, fragments));
                            }

                            break;
                        }
                    case FragmentSpread fs:
                        {
                            var fragment = fragments.First(el => el.Name == fs.Name);

                            var expanded = GetSubKeys(fragment.SelectionSet, fragments);

                            foreach (var a in expanded.Where(a => !result.ContainsKey(a.Key)))
                            {
                                result.Add(a.Key, a.Value);
                            }

                            break;
                        }
                }
            }
            return result;
        }
        
        private static IEnumerable<Field> GetFragmentsFields(INode node, Fragments fragments)
        {
            var result = new List<Field>();

            var fragmentFields = node
                .Children
                .Where(el => el is SelectionSet)
                .Cast<SelectionSet>()
                .Select(el => el.Selections.Where(x => x is FragmentSpread).Cast<FragmentSpread>().ToArray())
                .ToArray();

            foreach (var fragmentField in fragmentFields)
            {
                result.AddRange(GetFragmentsFields(fragmentField, fragments));
            }

            return result;
        }

        private static IEnumerable<Field> GetFragmentsFields(FragmentSpread[] spreadsArray, Fragments fragments)
        {
            var result = new List<Field>();

            foreach (var fSpread in spreadsArray)
            {
                result.AddRange(GetFragmentField(fSpread, fragments));
            }

            return result;
        }

        private static List<Field> GetFragmentField(FragmentSpread fSpread, Fragments fragments)
        {
            var result = new List<Field>();

            var fragment = fragments.First(el => el.Name == fSpread.Name);


            if (fragment.Children == null || !fragment.Children.Any())
            {
                return result;
            }

            var childrenChFields = fragment.Children
                .Where(el => el is SelectionSet)
                .Cast<SelectionSet>()
                .Select(el => el.Selections.Where(x => x is Field).Cast<Field>().ToArray())
                .ToList();

            foreach (var item in childrenChFields)
            {
                result.AddRange(item);
            }

            result.AddRange(GetFragmentsFields(fragment, fragments));

            return result;
        }
    }
}
