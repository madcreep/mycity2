using System.Collections.Generic;
using System.Linq;

namespace BIT.MyCity.Extensions
{
    public class GraphQlKeyDictionary : Dictionary<string, GraphQlKeyDictionary>
    {
        public GraphQlKeyDictionary()
            :base()
        { }

        public GraphQlKeyDictionary(IEnumerable<KeyValuePair<string, GraphQlKeyDictionary>> colection)
            :base(colection)
        { }

        public GraphQlKeyDictionary ByKey(IEnumerable<string> value)
        {
            return value == null
                ? this
                : ByKey(value as string[] ?? value.ToArray());
        }

        public GraphQlKeyDictionary ByKey(string requestString)
        {
            if (string.IsNullOrWhiteSpace(requestString))
            {
                return this;
            }

            var request = requestString.Split(':')
                .Select(el=>el?.Trim())
                .ToArray();

            return ByKey(request);
        }

        public GraphQlKeyDictionary ByKey(params string[] request)
        {
            if (request == null)
            {
                return this;
            }

            var result = this;

            foreach (var key in request)
            {
                if(!TryGetValue(key, out var tmp))
                {
                    return null;
                }

                result = tmp;
            }

            return result;
        }
    }

    
}
