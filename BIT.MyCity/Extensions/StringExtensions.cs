namespace BIT.MyCity.Extensions
{
    public static class StringExtensions
    {
        public static string ToJsonFormat(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return str;
            }
            return char.ToLower(str[0]) + str.Substring(1);
        }
    }
}
