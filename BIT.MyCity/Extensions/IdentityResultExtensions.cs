using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace BIT.MyCity.Extensions
{
    public static class IdentityResultExtensions
    {
        public static string ErrorToString(this IdentityResult ir, string separator = null)
        {
            return ir.Succeeded
                ? string.Empty
                : string.Join(separator ?? "; ", ir.Errors.Select(el => el.Description));
        }
    }
}
