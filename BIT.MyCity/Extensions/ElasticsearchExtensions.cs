﻿using BIT.MyCity.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nest;
using System;

namespace BIT.MyCity.Extensions
{
    public static class ElasticsearchExtensions
    {
        public static void AddElasticsearch(
            this IServiceCollection services, IConfiguration configuration)
        {
            string url = configuration["elasticsearch:url"];
            string defaultIndex = configuration["elasticsearch:index"];
            if(url != null)
            {
                ConnectionSettings settings = new ConnectionSettings(new Uri(url))
                    .DefaultIndex(defaultIndex)
                    .DefaultMappingFor<ESMessage>(m => m
                        .PropertyName(p => p.Id, "id")
                    )
                    .DefaultMappingFor<ESAppeal>(m => m
                        .PropertyName(p => p.Id, "id")
                    )
                    ;

                var client = new ElasticClient(settings);

                services.AddSingleton<IElasticClient>(client);
            }
        }
    }
}
