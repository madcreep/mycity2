using System;
using System.Collections.Generic;
using System.Linq;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Database;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Extensions
{
    public static class UserExtensions
    {
        public static IQueryable<User> ById(this IQueryable<User> users, long id)
        {
            return users
                .Where(el => el.Id == id);
        }

        public static IQueryable<User> ByIds(this IQueryable<User> users, params long[] ids)
        {
            return ids == null
                ? users
                : users
                    .Where(el => ids.Contains(el.Id));
        }

        public static IQueryable<User> ByUserName(this IQueryable<User> users, string name)
        {
            return users
                .Where(el => el.NormalizedUserName == name.ToUpper());
        }

        public static IQueryable<User> ByFullName(this IQueryable<User> users, string name)
        {
            return users
                .Where(el => el.NormalizeFullName.Contains(name.ToUpper()));
        }

        public static IQueryable<User> ByEmail(this IQueryable<User> users, string email)
        {
            if (email.ToLower() == "null")
            {
                return users
                    .Where(el => el.Email == null);
            }

            if (email.ToLower() == "not null")
            {
                return users
                    .Where(el => el.Email != null);
            }

            return users
                .Where(el => el.NormalizedEmail.Contains(email.ToUpper()));
        }

        public static IQueryable<User> ByLoginSystems(this IQueryable<User> users, params LoginSystem[] systems)
        {
            return systems == null
                ? users
                : users
                    .Where(el => systems.Contains(el.LoginSystem));
        }

        public static IQueryable<User> ByCreatedDate(this IQueryable<User> users, DateTime? start, DateTime? end)
        {
            return users
                .Where(el => (start == null || el.CreatedAt >= start.Value)
                             && (end == null || el.CreatedAt <= end.Value));
        }

        public static IQueryable<User> ByUpdatedDate(this IQueryable<User> users, DateTime? start, DateTime? end)
        {
            return users
                .Where(el => (start == null || el.UpdatedAt >= start.Value)
                             && (end == null || el.UpdatedAt <= end.Value));
        }

        public static IQueryable<User> ByRoleIds(this IQueryable<User> users, params long[] ids)
        {
            return users
                .Where(el => el.UserRole.Any(x => ids.Contains(x.RoleId)));
        }

        public static IQueryable<User> ByName(this IQueryable<User> users, string name)
        {
            return users
                .Where(el => el.UserName == name);
        }

        public static IQueryable<User> Disconnected(this IQueryable<User> users, bool? value)
        {
            if (!value.HasValue)
            {
                return users;
            }

            //return value.Value
            //    ? users.Where(el => el.DisconnectedTimestamp != null)
            //    : users.Where(el => el.DisconnectedTimestamp == null);
            return users
                .Where(el => el.Disconnected == value.Value);
        }

        public static IQueryable<User> ByClaims(this IQueryable<User> users, IEnumerable<IdentityUserClaim<long>> claims)
        {
            if (claims == null)
            {
                return users;
            }

            return claims
                .Aggregate(users, (current, claim) =>
                    current.Where(el =>
                        el.IdentityClaims.Any(x =>
                            x.ClaimType == claim.ClaimType &&
                            (claim.ClaimValue == null || claim.ClaimValue == x.ClaimValue))
                        || el.UserRole.Any(r => r.Role.IdentityClaims.Any(x =>
                            x.ClaimType == claim.ClaimType &&
                            (claim.ClaimValue == null || claim.ClaimValue == x.ClaimValue)))));
        }

        public static IQueryable<User> IncludeByKeys(this IQueryable<User> users, GraphQlKeyDictionary keys)
        {
            if (keys == null || !keys.Any())
            {
                return users;
            }

            var phonesName = nameof(User.Phones).ToJsonFormat();

            if (keys.ContainsKey(phonesName))
            {
                users = users
                    .Include(el => el.UserPhones)
                    .ThenInclude(el => el.Phone);

                var subKeys = keys[phonesName];

                if (subKeys.ContainsKey(nameof(Phone.UserOrganizations).ToJsonFormat()))
                {
                    users = users
                        .Include(el => el.UserPhones)
                        .ThenInclude(el => el.UserOrganization);
                }
            }

            if (keys.ContainsKey(nameof(User.Addresses).ToJsonFormat()))
            {
                users = users
                    .Include(el => el.Addresses);
            }

            if (keys.ContainsKey(nameof(User.RubricIds).ToJsonFormat()))
            {
                users = users
                    .Include(el => el.UserRubric)
                    .ThenInclude(el=>el.Subsystem);
            }

            if (keys.ContainsKey(nameof(User.TerritoryIds).ToJsonFormat()))
            {
                users = users
                    .Include(el => el.UserTerritory)
                    .ThenInclude(el => el.Subsystem);
            }

            if (keys.ContainsKey(nameof(User.RegionIds).ToJsonFormat()))
            {
                users = users
                    .Include(el => el.UserRegion)
                    .ThenInclude(el => el.Subsystem);
            }

            if (keys.ContainsKey(nameof(User.TopicIds).ToJsonFormat()))
            {
                users = users
                    .Include(el => el.UserModeratorTopic)
                    .ThenInclude(el => el.Subsystem);
            }

            if (keys.ContainsKey(nameof(User.Organizations).ToJsonFormat()))
            {
                var subKeys = keys[nameof(User.Organizations).ToJsonFormat()];

                users = users.Include(el => el.Users2Organizations)
                    .ThenInclude(el => el.UserOrganization);

                foreach (var key in subKeys.Keys)
                {
                    if (key == nameof(UserOrganization.Parent).ToJsonFormat())
                    {
                        users = users.Include(el => el.Users2Organizations)
                            .ThenInclude(el => el.UserOrganization)
                            .ThenInclude(el => el.Parent);
                    }
                    else if (key == nameof(UserOrganization.Branches).ToJsonFormat())
                    {
                        users = users.Include(el => el.Users2Organizations)
                            .ThenInclude(el => el.UserOrganization)
                            .ThenInclude(el => el.Branches);
                    }
                    else if (key == nameof(UserOrganization.Addresses).ToJsonFormat())
                    {
                        users = users.Include(el => el.Users2Organizations)
                            .ThenInclude(el => el.UserOrganization)
                            .ThenInclude(el => el.Addresses);
                    }
                    else if (key == nameof(UserOrganization.Phones).ToJsonFormat())
                    {
                        users = users.Include(el => el.Users2Organizations)
                            .ThenInclude(el => el.UserOrganization)
                            .ThenInclude(el => el.OrganizationPhones)
                            .ThenInclude(el => el.Phone);
                    }
                }
            }

            if (keys.ContainsKey(nameof(User.UserIdentityDocuments).ToJsonFormat()))
            {
                users = users.Include(el => el.UserIdentityDocuments);
            }

            if (keys.ContainsKey(nameof(User.Roles).ToJsonFormat())
            || keys.ContainsKey(nameof(User.RoleIds).ToJsonFormat())
            || keys.ContainsKey(nameof(User.AllClaims).ToJsonFormat()))
            {
                if ((keys.ContainsKey(nameof(User.Roles).ToJsonFormat())
                     && keys[nameof(User.Roles).ToJsonFormat()].ContainsKey(nameof(Role.Claims).ToJsonFormat()))
                    || keys.ContainsKey(nameof(User.AllClaims).ToJsonFormat()))
                {
                    users = users.Include(el => el.UserRole)
                        .ThenInclude(el => el.Role)
                        .ThenInclude(el => el.IdentityClaims);
                }
                else
                {
                    users = users.Include(el => el.UserRole)
                        .ThenInclude(el => el.Role);
                }
            }

            if (keys.ContainsKey(nameof(User.Claims).ToJsonFormat())
                || keys.ContainsKey(nameof(User.AllClaims).ToJsonFormat()))
            {
                users = users
                    .Include(el => el.IdentityClaims);
            }

            return users;
        }

        public static IQueryable<User> Filtered(this IQueryable<User> users, UserFilterModel filter)
        {
            if (filter == null)
            {
                return users;
            }

            if (filter.Id != null && filter.Id.Any())
            {
                users = users.ByIds(filter.Id.ToArray());
            }

            if (filter.UserName != null)
            {
                users = users.ByUserName(filter.UserName);
            }

            if (filter.FullName != null)
            {
                users = users.ByFullName(filter.FullName);
            }

            if (filter.Email != null)
            {
                users = users.ByEmail(filter.Email);
            }

            if (filter.LoginSystems != null)
            {
                users = users.ByLoginSystems(filter.LoginSystems.ToArray());
            }

            if (filter.Disconnected.HasValue)
            {
                users = users.Disconnected(filter.Disconnected);
            }

            if (filter.CreatedAtFrom.HasValue || filter.CreatedAtTo.HasValue)
            {
                users = users.ByCreatedDate(filter.CreatedAtFrom, filter.CreatedAtTo);
            }

            if (filter.UpdatedAtFrom.HasValue || filter.UpdatedAtTo.HasValue)
            {
                users = users.ByUpdatedDate(filter.UpdatedAtFrom, filter.UpdatedAtTo);
            }

            if (filter.RoleIds != null)
            {
                users = users.ByRoleIds(filter.RoleIds.ToArray());
            }

            if (filter.Claims != null)
            {
                users = users.ByClaims(filter.Claims);
            }

            return users;
        }
    }
}
