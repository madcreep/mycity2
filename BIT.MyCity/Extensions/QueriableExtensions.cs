using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using Z.EntityFramework.Plus;

namespace BIT.MyCity.Extensions
{
    public static class QueriableExtensions
    {
        public static IQueryable<TSource> OrderByFieldNames<TSource>(this IQueryable<TSource> query, string defaultName, IEnumerable<OrderByItem> names)
        {
            if (names == null || !names.Any())
            {
                return query
                    .OrderBy(defaultName, SearchDirection.Asc);
            }

            var isFirst = true;

            IOrderedQueryable<TSource> result = null;

            foreach (var name in names)
            {
                if (isFirst)
                {
                    result = query
                        .OrderBy(name.Field, name.Direction);

                    isFirst = false;
                }
                else
                {
                    result = result
                        .ThenBy(name.Field, name.Direction);
                }
            }

            return result ?? query;
        }

        public static IEnumerable<TSource> OrderByFieldNames<TSource>(this IEnumerable<TSource> query, string defaultName, IEnumerable<OrderByItem> names)
        {
            if (names == null || !names.Any())
            {
                return query
                    .OrderBy(defaultName, SearchDirection.Asc);
            }

            var isFirst = true;

            IOrderedEnumerable<TSource> result = null;

            foreach (var name in names)
            {
                if (isFirst)
                {
                    result = query
                        .OrderBy(name.Field, name.Direction);

                    isFirst = false;
                }
                else
                {
                    result = result
                        .ThenBy(name.Field, name.Direction);
                }
            }

            return result ?? query;
        }

        private static MemberExpression ConstructMEbyPath(Type entityType, string propertyPath, out Type currentType, out ParameterExpression arg)
        {
            arg = Expression.Parameter(entityType, "x");
            var pathSplitted = propertyPath.Split('.');
            currentType = entityType;
            MemberExpression property = null;
            for (int i = 0; i < pathSplitted.Length; i++)
            {
                PropertyInfo propertyInfo = currentType.GetPropInfo(pathSplitted[i]);
                if (propertyInfo == null)
                {
                    return null;
                }
                if (i == 0)
                {
                    property = Expression.MakeMemberAccess(arg, propertyInfo);
                }
                else
                {
                    property = Expression.MakeMemberAccess(property, propertyInfo);
                }
                currentType = propertyInfo.PropertyType;
            }
            return property;
        }

        public static IOrderedQueryable<TSource> OrderBy<TSource>(this IQueryable<TSource> query,
            string propertyName,
            SearchDirection direction)
        {
            var property = ConstructMEbyPath(typeof(TSource), propertyName, out Type currentType, out ParameterExpression arg);
            if (property == null)
            {
                return (IOrderedQueryable<TSource>)query;
            }
            var selector = Expression.Lambda(property, new ParameterExpression[] { arg });

            // Get System.Linq.Queryable.OrderBy() method.
            var methodName = direction == SearchDirection.Asc
                ? "OrderBy"
                : "OrderByDescending";

            var method = typeof(Queryable).GetMethods()
                .Where(m => m.Name == methodName && m.IsGenericMethodDefinition)
                .Where(m => m.GetParameters().ToList().Count == 2) // ensure selecting the right overload
                .Single();

            //The linq's OrderBy<TSource, TKey> has two generic types, which provided here
            var genericMethod = method.MakeGenericMethod(typeof(TSource), currentType);

            /* Call query.OrderBy(selector), with query and selector: x=> x.PropName
              Note that we pass the selector as Expression to the method and we don't compile it.
              By doing so EF can extract "order by" columns and generate SQL for it. */
            return (IOrderedQueryable<TSource>)genericMethod.Invoke(genericMethod, new object[] { query, selector });
        }
        public static IOrderedEnumerable<TSource> OrderBy<TSource>(this IEnumerable<TSource> query,
            string propertyName,
            SearchDirection direction)
        {
            var property = ConstructMEbyPath(typeof(TSource), propertyName, out Type currentType, out ParameterExpression arg);
            if (property == null)
            {
                return (IOrderedEnumerable<TSource>)query;
            }
            var selector = Expression.Lambda(property, new ParameterExpression[] { arg }).Compile();
            var methodName = direction == SearchDirection.Asc
                ? "OrderBy"
                : "OrderByDescending";

            var method = typeof(Enumerable).GetMethods()
                .Where(m => m.Name == methodName && m.IsGenericMethodDefinition)
                .Where(m => m.GetParameters().ToList().Count == 2) // ensure selecting the right overload
                .Single();
            var genericMethod = method.MakeGenericMethod(typeof(TSource), currentType);
            return (IOrderedEnumerable<TSource>)genericMethod.Invoke(genericMethod, new object[] { query, selector });
        }

        public static IOrderedQueryable<TSource> ThenBy<TSource>(this IOrderedQueryable<TSource> query,
            string propertyName,
            SearchDirection direction)
        {
            var property = ConstructMEbyPath(typeof(TSource), propertyName, out var currentType, out ParameterExpression arg);
            if (property == null)
            {
                return query;
            }
            var selector = Expression.Lambda(property, new ParameterExpression[] { arg });

            var methodName = direction == SearchDirection.Asc
                ? "ThenBy"
                : "ThenByDescending";

            // Get System.Linq.Queryable.OrderBy() method.
            var method = typeof(Queryable).GetMethods()
                .Where(m => m.Name == methodName && m.IsGenericMethodDefinition)
                .Where(m => m.GetParameters().ToList().Count == 2) // ensure selecting the right overload
                .Single();

            //The linq's OrderBy<TSource, TKey> has two generic types, which provided here
            var genericMethod = method.MakeGenericMethod(typeof(TSource), currentType);

            /* Call query.OrderBy(selector), with query and selector: x=> x.PropName
              Note that we pass the selector as Expression to the method and we don't compile it.
              By doing so EF can extract "order by" columns and generate SQL for it. */
            return (IOrderedQueryable<TSource>)genericMethod.Invoke(genericMethod, new object[] { query, selector });
        }
        public static IOrderedEnumerable<TSource> ThenBy<TSource>(this IOrderedEnumerable<TSource> query,
            string propertyName,
            SearchDirection direction)
        {
            var property = ConstructMEbyPath(typeof(TSource), propertyName, out var currentType, out ParameterExpression arg);
            if (property == null)
            {
                return query;
            }
            var selector = Expression.Lambda(property, new ParameterExpression[] { arg }).Compile();

            var methodName = direction == SearchDirection.Asc
                ? "ThenBy"
                : "ThenByDescending";
            var method = typeof(Enumerable).GetMethods()
                .Where(m => m.Name == methodName && m.IsGenericMethodDefinition)
                .Where(m => m.GetParameters().ToList().Count == 2) // ensure selecting the right overload
                .Single();
            var genericMethod = method.MakeGenericMethod(typeof(TSource), currentType);
            return (IOrderedEnumerable<TSource>)genericMethod.Invoke(genericMethod, new object[] { query, selector });
        }

        public enum CompareModeEnum
        {
            Contains,
            StartsWith,
            EndsWith,
            GreaterThan,
            GreaterOrEqual,
            LessThan,
            LessOrEqual,
            Equal,
            In,
        }

        public static IQueryable<TSource> FiltersBy<TSource>(this IQueryable<TSource> query, List<GraphQlQueryFilter> filters, bool withDeleted = false)
            where TSource : class
        {
            if (typeof(TSource).GetInterfaces().Contains(typeof(IDeletable))
                && !withDeleted
                && !WithDeletedInFilters(filters))
            {
                    query = query.Where(el => !((IDeletable)el).Deleted);
            }

            if (filters == null || !filters.Any())
            {
                return query;
            }

            if (filters[0].Stacked ?? false)
            {
                query = FiltersBy2(query, filters);
            }
            else
            {
                query = FiltersBy1(query, filters);
            }

            return query;
        }

        private static IQueryable<TSource> FiltersBy1<TSource>(IQueryable<TSource> query, List<GraphQlQueryFilter> filters) where TSource : class
        {
            if (filters == null)
            {
                return query;
            }

            IQueryable<TSource> lastQ = null;

            var baseQ = query;

            foreach (var f in filters)
            {
                if (lastQ == null)
                {
                    query = baseQ.FilterBy(f);
                }
                else
                {
                    query = f.Op switch
                    {
                        FilterNextOpEnum.OR => lastQ.Concat(baseQ.FilterBy(f)).Distinct(),
                        FilterNextOpEnum.EXCEPT => lastQ.Except(baseQ.FilterBy(f)),
                        _ => lastQ.FilterBy(f)
                    };
                }

                lastQ = query;
            }

            return query;
        }

        private static IQueryable<TSource> FiltersBy2<TSource>(IQueryable<TSource> query, List<GraphQlQueryFilter> filters) where TSource : class
        {
            var stack = new Stack<IQueryable<TSource>>();
            if (filters != null)
            {
                foreach (var f in filters)
                {
                    if (!string.IsNullOrEmpty(f.Name))
                    {
                        stack.Push(query.FilterBy(f));
                    }
                    switch (f.Op)
                    {
                        case FilterNextOpEnum.AND:
                        default:
                            if (stack.Count > 1)
                            {
                                var x = stack.Pop();
                                var y = stack.Pop();
                                stack.Push(y.Intersect(x));
                            }
                            break;
                        case FilterNextOpEnum.OR:
                            if (stack.Count > 1)
                            {
                                var x = stack.Pop();
                                var y = stack.Pop();
                                stack.Push(y.Concat(x).Distinct());
                            }
                            break;
                        case FilterNextOpEnum.EXCEPT:
                            if (stack.Count > 1)
                            {
                                var x = stack.Pop();
                                var y = stack.Pop();
                                stack.Push(y.Except(x));
                            }
                            break;
                        case FilterNextOpEnum.PUSH:
                            break;
                        case FilterNextOpEnum.POP:
                            if (stack.Count > 0)
                            {
                                _ = stack.Pop();
                            }
                            break;
                        case FilterNextOpEnum.XY:
                            if (stack.Count > 1)
                            {
                                var x = stack.Pop();
                                var y = stack.Pop();
                                stack.Push(x);
                                stack.Push(y);
                            }
                            break;
                    }

                }
            }
            return stack.Count > 0 ? stack.Pop() : query;
        }

        private static bool WithDeletedInFilters(List<GraphQlQueryFilter> filters)
        {
            if (filters == null)
            {
                return false;
            }

            return filters.Any(el => el.WithDeleted ?? false);
        }

        private static IQueryable<TSource> FilterBy<TSource>(this IQueryable<TSource> query, GraphQlQueryFilter f) where TSource : class
        {
            bool not = f.Not ?? false;
            if (f.In != null && f.In.Count > 0)
            {
                query = query.FilterBy(f.Name, f.In.ToArray(), CompareModeEnum.In, not);
            }
            if (f.Equal != null)
            {
                query = query.FilterBy(f.Name, new string[] { f.Equal }, CompareModeEnum.Equal, not);
            }
            if (f.Less != null)
            {
                query = query.FilterBy(f.Name, new string[] { f.Less }, CompareModeEnum.LessThan, not);
            }
            if (f.More != null)
            {
                query = query.FilterBy(f.Name, new string[] { f.More }, CompareModeEnum.GreaterThan, not);
            }
            if (f.LessOrEqual != null)
            {
                query = query.FilterBy(f.Name, new string[] { f.LessOrEqual }, CompareModeEnum.LessOrEqual, not);
            }
            if (f.GreaterOrEqual != null)
            {
                query = query.FilterBy(f.Name, new string[] { f.GreaterOrEqual }, CompareModeEnum.GreaterOrEqual, not);
            }
            if (f.Like != null)
            {
                if (f.Like.Length > 1 && f.Like.StartsWith('%') && !f.Like.EndsWith('%'))
                {
                    query = query.FilterBy(f.Name, new string[] { f.Like.Substring(1) }, CompareModeEnum.EndsWith, not);
                }
                else if (f.Like.Length > 1 && !f.Like.StartsWith('%') && f.Like.EndsWith('%'))
                {
                    query = query.FilterBy(f.Name, new string[] { f.Like.Substring(0, f.Like.Length - 1) }, CompareModeEnum.StartsWith, not);
                }
                else if (f.Like.Length > 2 && f.Like.StartsWith('%') && f.Like.EndsWith('%'))
                {
                    query = query.FilterBy(f.Name, new string[] { f.Like.Substring(1, f.Like.Length - 2) }, CompareModeEnum.Contains, not);
                }
                else
                {
                    query = query.FilterBy(f.Name, new string[] { f.Like }, CompareModeEnum.Equal, not);
                }
            }
            if (f.Where != null)
            {
                query = query.Where(f.Where);
            }

            return query;
        }

        private static IQueryable<TSource> FilterBy<TSource>(
            this IQueryable<TSource> query,
            string propertyPath,
            string[] template,
            CompareModeEnum mode,
            bool not
            ) where TSource : class
        {
            string[] pn = propertyPath.Split('.');
            var la = MakeExpr2<TSource>(pn, template, mode, not);
            return query.Where(la);
        }

        private static Expression<Func<TSource, bool>> MakeExpr2<TSource>(
            string[] pn,
            string[] template,
            CompareModeEnum mode,
            bool not,
            int level = 0)
        {
            var entityType = typeof(TSource);
            ParameterExpression entity = Expression.Parameter(entityType, "x" + level.ToString());
            bool isFirst = true;
            MemberExpression property = null;
            Type currentType = entityType;
            for (int i = 0; i < pn.Length; i++)
            {
                var propertyName = pn[i];
                var propertyInfo = currentType.GetPropInfo(propertyName);
                if (propertyInfo == null)
                {
                    throw new Exception($"Property {propertyName} not found in {currentType.Name}");
                }
                property = isFirst ? Expression.MakeMemberAccess(entity, propertyInfo) : Expression.MakeMemberAccess(property, propertyInfo);
                isFirst = false;
                currentType = propertyInfo.PropertyType;
                var d0 = currentType.GenericTypeArguments;
                if (d0 != null && d0.Length == 1 && i < pn.Length - 1)
                {
                    Type d1 = d0[0];/*если currentType - List, то d1 - тип элемента*/
                    //var laex = MakeExpr(d1, pn.Skip(i + 1).ToArray(), template, level + 1);
                    var laex = MakeExpr3(d1, pn.Skip(i + 1).ToArray(), template, mode, not, level + 1);
                    MethodInfo mw = typeof(Enumerable).GetMethods()
                        .Where(m => m.Name == "Any")
                        .Where(m => m.GetParameters().ToList().Count == 2)
                        .FirstOrDefault();
                    var mwg = mw.MakeGenericMethod(d1);
                    MethodCallExpression wcall = Expression.Call(mwg, property, laex);
                    return Expression.Lambda<Func<TSource, bool>>(wcall, new ParameterExpression[] { entity });
                }
            }
            if (property == null)
            {
                return null;
            }
            Expression call2 = NewMethod2(property, template, mode, not, currentType);
            var la = Expression.Lambda<Func<TSource, bool>>(call2, new ParameterExpression[] { entity });
            return la;
        }
        private static LambdaExpression MakeExpr3(
            Type entityType,
            string[] pn,
            string[] template,
            CompareModeEnum mode,
            bool not,
            int level = 0
            )
        {
            Type currentType = entityType;
            ParameterExpression entity = Expression.Parameter(entityType, "x" + level.ToString());
            bool isFirst = true;
            MemberExpression property = null;
            for (int i = 0; i < pn.Length; i++)
            {
                var propertyName = pn[i];
                var propertyInfo = currentType.GetPropInfo(propertyName);
                if (propertyInfo == null)
                {
                    throw new Exception($"Property {propertyName} not found in {currentType.Name}");
                }
                property = isFirst ? Expression.MakeMemberAccess(entity, propertyInfo) : Expression.MakeMemberAccess(property, propertyInfo);
                isFirst = false;
                currentType = propertyInfo.PropertyType;
                var d0 = currentType.GenericTypeArguments;
                if (d0 != null && d0.Length == 1 && i < pn.Length - 1)
                {
                    Type d1 = d0[0];
                    var laex = MakeExpr3(d1, pn.Skip(i + 1).ToArray(), template, mode, not, level + 1);
                    /*так и не смог прилепить Any() после Where(...). Хорошо что есть Any(...) с лямбдой*/
                    MethodInfo mw = typeof(Enumerable).GetMethods()
                        .Where(m => m.Name == "Any")
                        .Where(m => m.GetParameters().ToList().Count == 2)
                        .FirstOrDefault();
                    var mwg = mw.MakeGenericMethod(d1);
                    MethodCallExpression wcall = Expression.Call(mwg, property, laex);
                    //                    ParameterExpression e1 = Expression.Parameter(d1, "y");
                    return Expression.Lambda(wcall, new ParameterExpression[] { entity });
                }
            }
            if (property == null)
            {
                return null;
            }
            Expression call2 = NewMethod2(property, template, mode, not, currentType);
            var la = Expression.Lambda(call2, new ParameterExpression[] { entity });
            return la;
        }

        private static Expression NewMethod2(MemberExpression property, string[] template, CompareModeEnum mode, bool not, Type propertyType)
        {
            Expression call1 = null;
            Expression call2 = null;
            Expression call3 = null;
            switch (mode)
            {
                case CompareModeEnum.Contains:
                case CompareModeEnum.EndsWith:
                case CompareModeEnum.StartsWith:
                    MethodInfo method1 = typeof(string).GetMethods()
                        .Where(m => m.Name == "ToLower")
                        .Where(m => m.GetParameters().ToList().Count == 0)
                        .SingleOrDefault();
                    call1 = Expression.Call(property, method1);
                    string funcName = "Contains";
                    switch (mode)
                    {
                        case CompareModeEnum.EndsWith:
                            funcName = "EndsWith";
                            break;
                        case CompareModeEnum.StartsWith:
                            funcName = "StartsWith";
                            break;
                    }
                    MethodInfo method2 = typeof(string).GetMethods()
                        .Where(m => m.Name == funcName)
                        .Where(m => m.GetParameters().ToList().Count == 1)
                        .Where(m => m.GetParameters()[0].ParameterType == typeof(string))
                        .SingleOrDefault();
                    call2 = Expression.Call(call1, method2, new Expression[] { GetExpressionByPropertyType(template[0], propertyType) });
                    break;
                case CompareModeEnum.Equal:
                    if (propertyType == typeof(DateTime))
                    {
                        call2 = ExpressionDateEqual(property, template[0]);
                    }
                    else if (propertyType == typeof(string))
                    {
                        MethodInfo method3 = typeof(string).GetMethods()
                            .Where(m => m.Name == "Equals")
                            .Where(m => m.GetParameters().ToList().Count == 1)
                            .Where(m => m.GetParameters()[0].ParameterType == typeof(string))
                            .SingleOrDefault();
                        call2 = Expression.Call(property, method3, new Expression[] { Expression.Constant(template[0]) });
                    }
                    else
                    {
                        call2 = Expression.Equal(property, GetExpressionByPropertyType(template[0], propertyType));
                    }
                    break;
                case CompareModeEnum.GreaterThan:
                    call2 = Expression.GreaterThan(property, GetExpressionByPropertyType(template[0], propertyType));
                    break;
                case CompareModeEnum.GreaterOrEqual:
                    call2 = Expression.GreaterThanOrEqual(property, GetExpressionByPropertyType(template[0], propertyType));
                    break;
                case CompareModeEnum.LessThan:
                    call2 = Expression.LessThan(property, GetExpressionByPropertyType(template[0], propertyType));
                    break;
                case CompareModeEnum.LessOrEqual:
                    call2 = Expression.LessThanOrEqual(property, GetExpressionByPropertyType(template[0], propertyType));
                    break;
                case CompareModeEnum.In:
                    call2 = null;
                    foreach (var t in template)
                    {
                        if (call2 == null)
                        {
                            if (propertyType == typeof(DateTime))
                            {
                                call2 = ExpressionDateEqual(property, t);
                            }
                            else
                            {
                                call2 = Expression.Equal(property, GetExpressionByPropertyType(t, propertyType));
                            }
                        }
                        else
                        {
                            if (propertyType == typeof(DateTime))
                            {
                                call2 = Expression.Or(call2, ExpressionDateEqual(property, t));
                            }
                            else
                            {
                                call2 = Expression.Or(call2, Expression.Equal(property, GetExpressionByPropertyType(t, propertyType)));
                            }
                        }
                    }
                    break;
            }
            if (not)
            {
                call3 = Expression.Equal(call2, Expression.Constant(false));
            }
            else
            {
                call3 = call2;
            }
            return call3;
        }

        private static Expression ExpressionDateEqual(MemberExpression property, string template)
        {
            Expression call2;
            var date = DateTime.Parse(template);
            if (template.Length == 10)
            {
                Expression from = Expression.Constant(Convert.ToDateTime(date.ToString("yyyy-MM-dd")));
                Expression to = Expression.Constant(Convert.ToDateTime(date.AddDays(1).ToString("yyyy-MM-dd")));
                call2 = Expression.And(Expression.GreaterThanOrEqual(property, from), Expression.LessThan(property, to));
            }
            else
            {
                Expression from = Expression.Constant(DateTime.Parse(date.ToString("yyyy-MM-dd HH:mm:ss")));
                Expression to = Expression.Constant(DateTime.Parse(date.AddSeconds(1).ToString("yyyy-MM-dd HH:mm:ss")));
                call2 = Expression.And(Expression.GreaterThanOrEqual(property, from), Expression.LessThan(property, to));
            }
            return call2;
        }

        private static Expression GetExpressionByPropertyType(string txt, Type propertyType)
        {
            Expression c = null;
            if (txt == "null")
            {
                c = Expression.Constant(null);
            }
            else if (propertyType == typeof(string))
            {
                c = Expression.Constant(txt.ToLower());
            }
            else if (propertyType == typeof(short))
            {
                c = Expression.Constant(short.Parse(txt), typeof(short));
            }
            else if (propertyType == typeof(short?))
            {
                c = Expression.Constant(short.Parse(txt), typeof(short?));
            }
            else if (propertyType == typeof(ushort))
            {
                c = Expression.Constant(ushort.Parse(txt), typeof(ushort));
            }
            else if (propertyType == typeof(ushort?))
            {
                c = Expression.Constant(ushort.Parse(txt), typeof(ushort?));
            }
            else if (propertyType == typeof(int))
            {
                c = Expression.Constant(int.Parse(txt), typeof(int));
            }
            else if (propertyType == typeof(int?))
            {
                c = Expression.Constant(int.Parse(txt), typeof(int?));
            }
            else if (propertyType == typeof(uint))
            {
                c = Expression.Constant(uint.Parse(txt), typeof(uint));
            }
            else if (propertyType == typeof(uint?))
            {
                c = Expression.Constant(uint.Parse(txt), typeof(uint?));
            }
            else if (propertyType == typeof(long))
            {
                c = Expression.Constant(long.Parse(txt), typeof(long));
            }
            else if (propertyType == typeof(long?))
            {
                c = Expression.Constant(long.Parse(txt), typeof(long?));
            }
            else if (propertyType == typeof(ulong))
            {
                c = Expression.Constant(ulong.Parse(txt), typeof(ulong));
            }
            else if (propertyType == typeof(ulong?))
            {
                c = Expression.Constant(ulong.Parse(txt), typeof(ulong?));
            }
            else if (propertyType == typeof(bool))
            {
                c = Expression.Constant(bool.Parse(txt), typeof(bool));
            }
            else if (propertyType == typeof(bool?))
            {
                c = Expression.Constant(bool.Parse(txt), typeof(bool?));
            }
            else if (propertyType == typeof(DateTime))
            {
                var d = DateTime.Parse(txt, null, System.Globalization.DateTimeStyles.AdjustToUniversal);
                c = Expression.Constant(d, typeof(DateTime));
            }
            else if (propertyType == typeof(DateTime?))
            {
                var d = DateTime.Parse(txt, null, System.Globalization.DateTimeStyles.AdjustToUniversal);
                c = Expression.Constant(d, typeof(DateTime?));
            }
            else if (propertyType.BaseType == typeof(Enum))
            {
                foreach (var ev in propertyType.GetEnumValues())
                {
                    if (ev.ToString() == txt)
                    {
                        c = Expression.Constant(ev, propertyType);
                        break;
                    }
                }
                if (c == null)
                {
                    throw new Exception($"Value '{txt}' not found in enum {propertyType.Name}");
                }
            }
            return c;
        }

        private static PropertyInfo GetPropInfo(this Type src, string propertyName)
        {
            //if (src.IsInterface)
            //{
            //    var tmp1 = src.GetProperties();
            //    var tmp = src.GetProperty(propertyName, BindingFlags.IgnoreCase);
            //}

            var propertyInfo = src.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

            if (propertyInfo == null)
            {
                return null;
            }

            if (propertyInfo.DeclaringType != src)
            {
                propertyInfo = propertyInfo.DeclaringType.GetProperty(propertyName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            }

            if (propertyInfo == null)
            {
                return null;
            }

            var notMappedAttribute = propertyInfo
                .GetCustomAttribute<NotMappedAttribute>();

            if (notMappedAttribute != null)
            {
                return null;
            }
            return propertyInfo;
        }

        public static string GetPropertyPathTrueCase<TSource>(this IQueryable<TSource> query,
            string propertyPath)
        {
            var entityType = typeof(TSource);
            var pathSplitted = propertyPath.Split('.');
            var currentType = entityType;
            string pathResult = "";
            for (int i = 0; i < pathSplitted.Length - 1; i++)
            {
                if (currentType.GenericTypeArguments != null && currentType.GenericTypeArguments.Length == 1)
                {
                    currentType = currentType.GenericTypeArguments[0];
                }
                var propertyInfo = currentType.GetPropInfo(pathSplitted[i]);
                if (propertyInfo == null)
                {
                    throw new Exception($"Property {pathSplitted[i]} not found in {propertyPath}");
                }
                if (!string.IsNullOrEmpty(pathResult))
                {
                    pathResult += ".";
                }
                pathResult += propertyInfo.Name;
                currentType = propertyInfo.PropertyType;
            }
            return pathResult;
        }

        public static IQueryable<TSource> IncludeByPathIgnoreCase<TSource>(this IQueryable<TSource> query,
            string propertyPath, bool last = false) where TSource : class
        {
            var entityType = typeof(TSource);
            var pathSplitted = propertyPath.Split('.');
            var currentType = entityType;
            var pathResult = "";
            for (var i = 0; i < (last ? pathSplitted.Length : pathSplitted.Length - 1); i++)
            {
                if (currentType.GenericTypeArguments != null && currentType.GenericTypeArguments.Length == 1)
                {
                    currentType = currentType.GenericTypeArguments[0];
                }
                var propertyInfo = currentType.GetPropInfo(pathSplitted[i]);
                if (propertyInfo == null)
                {
                    break;
                }
                if (!string.IsNullOrEmpty(pathResult))
                {
                    pathResult += ".";
                }
                pathResult += propertyInfo.Name;
                currentType = propertyInfo.PropertyType;
            }
            if (!string.IsNullOrEmpty(pathResult))
            {
                query = query.Include(pathResult);
            }
            return query;
        }

        public static IQueryable<TSource> IncludeFilterByPathIgnoreCase<TSource>(this IQueryable<TSource> query,
            string propertyPath, bool last = false) where TSource : class
        {
            var entityType = typeof(TSource);
            var pathSplitted = propertyPath.Split('.');
            var currentType = entityType;
            var pathResult = "";
            for (var i = 0; i < (last ? pathSplitted.Length : pathSplitted.Length - 1); i++)
            {
                if (currentType.GenericTypeArguments != null && currentType.GenericTypeArguments.Length == 1)
                {
                    currentType = currentType.GenericTypeArguments[0];
                }
                var propertyInfo = currentType.GetPropInfo(pathSplitted[i]);
                if (propertyInfo == null)
                {
                    break;
                }
                if (!string.IsNullOrEmpty(pathResult))
                {
                    pathResult += ".";
                }
                pathResult += propertyInfo.Name;
                currentType = propertyInfo.PropertyType;
            }
            if (!string.IsNullOrEmpty(pathResult))
            {
                //query = query.IncludeFilterByPath(pathResult);
                var lambdaExpression = QueryIncludeFilterByPath.CreateLambdaExpression(typeof(TSource), pathResult.Split('.'), 0);
                var type = typeof(QueryIncludeFilterExtensions);
                var method = type.GetMethods(BindingFlags.NonPublic | BindingFlags.Static)
                    .Single(el => el.Name == "IncludeOptimizedSingle");
                MethodInfo methodInfo;
                if ((object)method == null)
                    methodInfo = (MethodInfo)null;
                else
                    methodInfo = method.MakeGenericMethod(typeof(TSource), lambdaExpression.Type.GetGenericArguments()[1]);
                object obj;
                if ((object)methodInfo == null)
                    obj = (object)null;
                else
                    obj = methodInfo.Invoke((object)null, new object[2]
                    {
                        (object) query,
                        (object) lambdaExpression
                    });
                query = (IQueryable<TSource>)obj;
            }

            return query;
        }

        public static string ToSql<TEntity>(this IQueryable<TEntity> query) where TEntity : class
        {
            var enumerator = query.Provider.Execute<IEnumerable<TEntity>>(query.Expression).GetEnumerator();
            var relationalCommandCache = enumerator.Private("_relationalCommandCache");
            var selectExpression = relationalCommandCache.Private<SelectExpression>("_selectExpression");
            var factory = relationalCommandCache.Private<IQuerySqlGeneratorFactory>("_querySqlGeneratorFactory");

            var sqlGenerator = factory.Create();
            var command = sqlGenerator.GetCommand(selectExpression);

            string sql = command.CommandText;
            return sql;
        }

        private static object Private(this object obj, string privateField) => obj?.GetType().GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(obj);
        private static T Private<T>(this object obj, string privateField) => (T)obj?.GetType().GetField(privateField, BindingFlags.Instance | BindingFlags.NonPublic)?.GetValue(obj);

        public static IQueryable<T> WhereEquals<T>(this IQueryable<T> source, string member, object value)
        {
            var item = Expression.Parameter(typeof(T), "item");

            var memberValue = member.Split('.').Aggregate((Expression)item, Expression.PropertyOrField);

            var memberType = memberValue.Type;

            if (value != null && value.GetType() != memberType)
            {
                value = Convert.ChangeType(value, memberType);
            }

            var condition = Expression.Equal(memberValue, Expression.Constant(value, memberType));

            var predicate = Expression.Lambda<Func<T, bool>>(condition, item);

            return source.Where(predicate);
        }

    }
}
