using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using static System.DateTime;

namespace BIT.MyCity.Extensions
{
    public static class AuthenticationExtension
    {
        private static ServiceProvider _serviceProvider;

        public static IServiceCollection AddTokenAuthentication(this IServiceCollection services,
            IConfiguration config)
        {
            _serviceProvider = services.BuildServiceProvider();

            var secret = config.GetSection("JwtConfig").GetSection("secret").Value;

            var key = Encoding.UTF8.GetBytes(secret);

            services.AddAuthentication(x =>
              {
                  x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                  x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
              })
              .AddJwtBearer(x =>
              {
                  x.SaveToken = true;
                  x.TokenValidationParameters = new TokenValidationParameters
                  {
                      IssuerSigningKey = new SymmetricSecurityKey(key),
                      ValidateIssuer = false,
                      ValidateAudience = false,
                      ValidateLifetime = true,
                      RequireSignedTokens = true,
                      LifetimeValidator = MyLifetimeValidator,
                      AudienceValidator = MyAudienceValidator,
                      IssuerValidator = MyIssuerValidator
                  };
              });

            return services;
        }

        static bool MyLifetimeValidator(
          DateTime? notBefore,
          DateTime? expires,
          SecurityToken securityToken,
          TokenValidationParameters validationParameters)
        {
            if (!validationParameters.ValidateLifetime)
            {
                return true;
            }

            if (!expires.HasValue || !notBefore.HasValue)
            {
                return false;
            }

            var now = UtcNow;

            return now >= notBefore && now <= expires.Value.AddSeconds(10);
        }

        static bool MyAudienceValidator(IEnumerable<string> audiences,
            SecurityToken securityToken,
            TokenValidationParameters validationParameters)
        {
            if (!validationParameters.ValidateAudience)
            {
                return true;
            }

            var httpContextAccessor = (IHttpContextAccessor)_serviceProvider.GetService(typeof(IHttpContextAccessor));

            var userIpAddress = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            return audiences.Any(el => el == userIpAddress);
        }

        static string MyIssuerValidator(string issuer,
            SecurityToken securityToken,
            TokenValidationParameters validationParameters)
        {
            if (!validationParameters.ValidateIssuer)
            {
                return issuer;
            }

            var httpContextAccessor = (IHttpContextAccessor)_serviceProvider.GetService(typeof(IHttpContextAccessor));

            var httpContext = httpContextAccessor.HttpContext;

            return issuer.Equals($"{httpContext.Request.Scheme}://{httpContext.Request.Host.Host}")
                ? issuer : throw new Exception();
        }
    }
}
