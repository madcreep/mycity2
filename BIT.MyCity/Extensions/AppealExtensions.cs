using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using BIT.MyCity.Authorization.Models;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using BIT.MyCity.Subsystems;
using LinqKit;
using Microsoft.EntityFrameworkCore;

namespace BIT.MyCity.Extensions
{
    public static class AppealExtensions
    {
        public static IQueryable<Appeal> IncludeByNotMappedFields(this IQueryable<Appeal> obj, GraphQlKeyDictionary fields)
        {
            if (fields != null && obj != null)
            {
                if (fields.ContainsKey(nameof(Appeal.Moderators).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.UserModeratorAppeal).ThenInclude(el => el.User);
                }
                if (fields.ContainsKey(nameof(Appeal.UsersLiked).ToJsonFormat()) || fields.ContainsKey(nameof(Appeal.LikeWIL).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.UserLikeAppeal).ThenInclude(el => el.User);
                }
                if (fields.ContainsKey(nameof(Appeal.UsersViewed).ToJsonFormat()) || fields.ContainsKey(nameof(Appeal.ViewWIV).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.UserViewAppeal).ThenInclude(el => el.User);
                }
                if (fields.ContainsKey(nameof(Appeal.AppealExecutors).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.Appeal2Executors).ThenInclude(el => el.AppealExecutor);
                }
                if (fields.ContainsKey(nameof(Appeal.AppealPrincipalExecutors).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.Appeal2PrincipalExecutors).ThenInclude(el => el.AppealPrincipalExecutor);
                }
                if (fields.ContainsKey(nameof(Appeal.AttachedFiles).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.AttachedFilesToAppeal).ThenInclude(el => el.AttachedFile);
                }
            }
            return obj;
        }

        public static IQueryable<Appeal> IncludeFilterByNotMappedFields(this IQueryable<Appeal> obj, GraphQlKeyDictionary fields)
        {
            if (fields != null && obj != null)
            {
                if (fields.ContainsKey(nameof(Appeal.Moderators).ToJsonFormat()))
                {
                    obj = obj
                        .Include(el => el.UserModeratorAppeal)
                        .ThenInclude(el => el.User);
                    //obj = obj.IncludeFilter(el => el.UserModeratorAppeal
                    //    .Select(x => x.User));
                }
                if (fields.ContainsKey(nameof(Appeal.UsersLiked).ToJsonFormat()) || fields.ContainsKey(nameof(Appeal.LikeWIL).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.UserLikeAppeal)
                        .ThenInclude(el => el.User);
                    //obj = obj.IncludeFilter(el => el.UserLikeAppeal
                    //    .Select(x => x.User));
                }
                if (fields.ContainsKey(nameof(Appeal.UsersViewed).ToJsonFormat()) || fields.ContainsKey(nameof(Appeal.ViewWIV).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.UserViewAppeal)
                        .ThenInclude(el => el.User);
                    //obj = obj.IncludeFilter(el => el.UserViewAppeal
                    //    .Select(x => x.User));
                }
                if (fields.ContainsKey(nameof(Appeal.AppealExecutors).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.Appeal2Executors)
                        .ThenInclude(el => el.AppealExecutor);
                    //obj = obj.IncludeFilter(el => el.Appeal2Executors
                    //    .Select(x => x.AppealExecutor));
                }
                if (fields.ContainsKey(nameof(Appeal.AppealPrincipalExecutors).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.Appeal2PrincipalExecutors)
                        .ThenInclude(el => el.AppealPrincipalExecutor);
                    //obj = obj.IncludeFilter(el => el.Appeal2PrincipalExecutors
                    //    .Select(x => x.AppealPrincipalExecutor));
                }
                if (fields.ContainsKey(nameof(Appeal.AttachedFiles).ToJsonFormat()))
                {
                    obj = obj.Include(el => el.AttachedFilesToAppeal)
                        .ThenInclude(el => el.AttachedFile);
                    //obj = obj.IncludeFilter(el => el.AttachedFilesToAppeal
                    //    .Select(x => x.AttachedFile));
                }
            }
            return obj;
        }

        public static IQueryable<Appeal> FilterByReadAllowedAsync(this IQueryable<Appeal> main, UserManager um, User user, SettingsManager sm, bool wlbo)
        {
            var claimsPrincipal = um.CurrentClaimsPrincipal();

            var userId = um.ClaimsPrincipalId(claimsPrincipal);

            QueriableFilterExtensions.GetFromClaimsARs(claimsPrincipal?.Claims, out var ssAllRegions, out var ssAllRubrics, out var ssAllTerritories, out var ssAllTopics);

            var predicate = PredicateBuilder.New<Appeal>();

            if (sm.SettingValue<bool>($"subsystem.{SubsystemCitizenAppeals.BaseSystemKey}.enable."))
            {
                var innerPredicate = PredicateBuilder.New<Appeal>();

                innerPredicate =
                    innerPredicate.And(el => el.Subsystem.UID == SubsystemCitizenAppeals.BaseSystemKey);

                if ((claimsPrincipal?.Identity?.IsAuthenticated ?? false)
                    && userId.HasValue
                    && claimsPrincipal.HasClaim(el =>
                        el.Type == ClaimName.Subsystem && el.Value ==
                        $"{SubsystemCitizenAppeals.BaseSystemKey}.{SSClaimSuffix.Ss.available}"))
                {
                    var hasClaimModerate = claimsPrincipal.HasClaim(el =>
                        el.Type == ClaimName.Moderate && el.Value ==
                        $"{SubsystemCitizenAppeals.BaseSystemKey}.{SSClaimSuffix.Md.enable}");

                    var hasClaimFbAdmin = claimsPrincipal.HasClaim(el =>
                        el.Type == ClaimName.Subsystem && el.Value ==
                        $"{SubsystemCitizenAppeals.BaseSystemKey}.{SSClaimSuffix.Ss.feedback_admin}");

                    if (hasClaimModerate || hasClaimFbAdmin)
                    {
                        var orPredicate = PredicateBuilder.New<Appeal>();

                        if (hasClaimModerate)
                        {
                            var moderatePredicate = PredicateBuilder.New<Appeal>();

                            moderatePredicate = moderatePredicate.And(el => el.AppealType == AppealTypeEnum.NORMAL);

                            if (!ssAllRubrics.Contains(SubsystemCitizenAppeals.BaseSystemKey))
                            {
                                moderatePredicate = moderatePredicate.And(el =>
                                    el.Rubric != null &&
                                    el.Rubric.UserModerator.Any(x => x.UserId == userId.Value));
                            }

                            if (!ssAllRegions.Contains(SubsystemCitizenAppeals.BaseSystemKey))
                            {
                                moderatePredicate = moderatePredicate.And(el =>
                                    el.Region != null &&
                                    el.Region.UserModerator.Any(x => x.UserId == userId.Value));
                            }

                            if (!ssAllTerritories.Contains(SubsystemCitizenAppeals.BaseSystemKey))
                            {
                                moderatePredicate = moderatePredicate.And(el =>
                                    el.Territory != null &&
                                    el.Territory.UserModerator.Any(x => x.UserId == userId.Value));
                            }

                            if (!ssAllTopics.Contains(SubsystemCitizenAppeals.BaseSystemKey))
                            {
                                moderatePredicate = moderatePredicate.And(el =>
                                    el.Topic != null &&
                                    el.Topic.UserModerator.Any(x => x.UserId == userId.Value));
                            }

                            orPredicate = orPredicate.Or(moderatePredicate);
                        }

                        if (hasClaimFbAdmin)
                        {
                            orPredicate = orPredicate.Or(el => el.AppealType == AppealTypeEnum.FEEDBACK);
                        }

                        innerPredicate = innerPredicate.And(orPredicate);
                    }
                    else
                    {
                        innerPredicate = innerPredicate.And(el => el.Public_ || el.AuthorId == userId.Value);
                    }
                }
                else
                {
                    innerPredicate = innerPredicate.And(el => el.Public_);
                }

                predicate = predicate.Or(innerPredicate);
            }

            if (sm.SettingValue<bool>($"subsystem.{SubsystemOrganizationAppeals.BaseSystemKey}.enable."))
            {
                var innerPredicate = PredicateBuilder.New<Appeal>();

                innerPredicate =
                    innerPredicate.And(el => el.Subsystem.UID == SubsystemOrganizationAppeals.BaseSystemKey);

                if ((claimsPrincipal?.Identity?.IsAuthenticated ?? false)
                    && userId.HasValue
                    && claimsPrincipal.HasClaim(el =>
                        el.Type == ClaimName.Subsystem && el.Value ==
                        $"{SubsystemOrganizationAppeals.BaseSystemKey}.{SSClaimSuffix.Ss.available}"))
                {
                    var hasClaimModerate = claimsPrincipal.HasClaim(el =>
                        el.Type == ClaimName.Moderate && el.Value ==
                        $"{SubsystemOrganizationAppeals.BaseSystemKey}.{SSClaimSuffix.Md.enable}");

                    var hasClaimFbAdmin = claimsPrincipal.HasClaim(el =>
                        el.Type == ClaimName.Moderate && el.Value ==
                        $"{SubsystemOrganizationAppeals.BaseSystemKey}.{SSClaimSuffix.Ss.feedback_admin}");

                    if (hasClaimModerate || hasClaimFbAdmin)
                    {
                        if (hasClaimModerate)
                        {
                            if (!ssAllRubrics.Contains(SubsystemOrganizationAppeals.BaseSystemKey))
                            {
                                innerPredicate = innerPredicate.And(el =>
                                    el.Rubric != null &&
                                    el.Rubric.UserModerator.Any(x => x.UserId == userId.Value));
                            }

                            if (!ssAllRegions.Contains(SubsystemOrganizationAppeals.BaseSystemKey))
                            {
                                innerPredicate = innerPredicate.And(el =>
                                    el.Region != null &&
                                    el.Region.UserModerator.Any(x => x.UserId == userId.Value));
                            }

                            if (!ssAllTerritories.Contains(SubsystemOrganizationAppeals.BaseSystemKey))
                            {
                                innerPredicate = innerPredicate.And(el =>
                                    el.Territory != null &&
                                    el.Territory.UserModerator.Any(x => x.UserId == userId.Value));
                            }

                            if (!ssAllTopics.Contains(SubsystemOrganizationAppeals.BaseSystemKey))
                            {
                                innerPredicate = innerPredicate.And(el =>
                                    el.Topic != null &&
                                    el.Topic.UserModerator.Any(x => x.UserId == userId.Value));
                            }
                        }
                    }
                    else
                    {
                        innerPredicate = innerPredicate.And(el => el.Public_
                                                                  || el.AuthorId == userId.Value
                                                                  || el.Organization.Users2Organizations.Any(u =>
                                                                      u.UserId == userId.Value));
                    }
                }
                else
                {
                    innerPredicate = innerPredicate.And(el => el.Public_);
                }

                predicate = predicate.Or(innerPredicate);
            }

            return main.Where(predicate);
        }


        public static IQueryable<Appeal> FilterByReadAllowedAsyncOld(this IQueryable<Appeal> main, UserManager um, User user, SettingsManager sm, bool wlbo)
        {
            var ss = new List<string> { SubsystemCitizenAppeals.BaseSystemKey, SubsystemOrganizationAppeals.BaseSystemKey };

            var ssEnabled = ss
                .Where(el => sm.SettingValue<bool>($"subsystem.{el}.enable."))
                .ToArray();

            var claimsPrincipal = um.CurrentClaimsPrincipal();

            // Неавторизованные
            if (!(claimsPrincipal?.Identity?.IsAuthenticated ?? false))
            {
                return main
                    .Where(el => el.Public_ && ssEnabled.Contains(el.Subsystem.UID));
            }

            var userId = um.ClaimsPrincipalId(claimsPrincipal);

            var ssAvail = ssEnabled.Intersect(ss
                .Where(el => claimsPrincipal.HasClaim(ClaimName.Subsystem, $"{el}.{SSClaimSuffix.Ss.available}")))
                .ToArray();

            // Не администраторы
            if (user.LoginSystem != LoginSystem.Form || user.SelfFormRegister)
            {
                return main
                    .Where(el => el.Subsystem.UID != null
                                 && ((el.Public_ && ssEnabled.Contains(el.Subsystem.UID)) //публичные
                                 || (ssAvail.Contains(el.Subsystem.UID) &&
                                     (el.AuthorId == userId.Value // Созданные пользователем
                                      || el.Organization.Users2Organizations.Any(u =>
                                          u.UserId == userId.Value)))) // по организации
                    );
            }

            // по модератору
            var moderatorInPredicate = PredicateBuilder.New<Appeal>()
                .And(el =>
                    el.AppealType == AppealTypeEnum.NORMAL && el.Subsystem.UID != null &&
                    ssAvail.Any(x => x == el.Subsystem.UID))
                .And(QueriableFilterExtensions.FilterByReadAllowedSubsystemRightsPredicate<Appeal>(userId,
                    claimsPrincipal.Claims, SubsystemCitizenAppeals.BaseSystemKey));

            //var moderatorIn = main
            //    .Where(el => el.AppealType == AppealTypeEnum.NORMAL && el.Subsystem.UID != null && ssAvail.Any(x => x == el.Subsystem.UID))
            //    .FilterByReadAllowedSubsystemRights(userId, claimsPrincipal.Claims, SubsystemCitizenAppeals.BaseSystemKey);

            // по исполнителю
            var ssExecutors = QueriableFilterExtensions.GetFromClaimsSsExecutor(claimsPrincipal.Claims)
                .Intersect(ssAvail);

            var executorInPredicate = PredicateBuilder.New<Appeal>()
                .And(el => el.AppealType == AppealTypeEnum.NORMAL && el.Subsystem.UID != null &&
                           ssExecutors.Any(x => x == el.Subsystem.UID))
                .And(QueriableFilterExtensions.FilterByReadAllowedRubricatorExecutorPredicate<Appeal>(userId,
                    SubsystemCitizenAppeals.BaseSystemKey));

            //var executorIn = main
            //    .Where(el => el.AppealType == AppealTypeEnum.NORMAL && el.Subsystem.UID != null && ssExecutors.Any(x => x == el.Subsystem.UID))
            //    .FilterByReadAllowedRubricatorExecutor(userId, SubsystemCitizenAppeals.BaseSystemKey);

            // обращения обратной связи

            var feedbackInPredicate = PredicateBuilder.New<Appeal>()
                .And(el => el.AppealType == AppealTypeEnum.FEEDBACK && el.Subsystem.UID != null &&
                           ssAvail.Any(x => x == el.Subsystem.UID))
                .And(FilterByReadAllowedFeedBackAdminPredicate(claimsPrincipal.Claims));
            //var feedbackIn = main
            //    .Where(el => el.AppealType == AppealTypeEnum.FEEDBACK && el.Subsystem.UID != null && ssAvail.Any(x => x == el.Subsystem.UID))
            //    .FilterByReadAllowedFeedBackAdmin(claimsPrincipal.Claims);

            var predicate = PredicateBuilder.New<Appeal>();

            predicate = predicate
                .Or(moderatorInPredicate)
                .Or(executorInPredicate)
                .Or(feedbackInPredicate);

            return main.Where(predicate);

            //return moderatorIn
            //    .Concat(executorIn)
            //    .Concat(feedbackIn)
            //    .Distinct();
        }

        public static IQueryable<Appeal> FilterByReadAllowedAsync1(this IQueryable<Appeal> main, UserManager um, User user, SettingsManager sm, bool wlbo)
        {
            var ss = new List<string> { SubsystemCitizenAppeals.BaseSystemKey, SubsystemOrganizationAppeals.BaseSystemKey };

            var ssEnabled = ss
                .Where(el => sm.SettingValue<bool>($"subsystem.{el}.enable."))
                .ToArray();

            var claimsPrincipal = um.CurrentClaimsPrincipal();

            // Неавторизованные
            if (!(claimsPrincipal?.Identity?.IsAuthenticated ?? false))
            {
                return main
                    .Where(el => el.Public_ && ssEnabled.Contains(el.Subsystem.UID));
            }

            var userId = um.ClaimsPrincipalId(claimsPrincipal);

            var ssAvail = ssEnabled.Intersect(ss
                .Where(el => claimsPrincipal.HasClaim(ClaimName.Subsystem, $"{el}.{SSClaimSuffix.Ss.available}")))
                .ToArray();

            // Не администраторы
            if (user.LoginSystem != LoginSystem.Form || user.SelfFormRegister)
            {
                return main
                    .Where(el =>
                        (el.Public_ && ssEnabled.Contains(el.Subsystem.UID)) //публичные
                        || (ssAvail.Contains(el.Subsystem.UID) &&
                            (el.AuthorId == userId.Value // Созданные пользователем
                                || el.Organization.Users2Organizations.Any(u => u.UserId == userId.Value))) // по организации
                    );
            }

            // по модератору
            var moderatorIn = main
                .Where(el => el.AppealType == AppealTypeEnum.NORMAL && el.Subsystem.UID != null && ssAvail.Any(x => x == el.Subsystem.UID))
                .FilterByReadAllowedSubsystemRights(userId, claimsPrincipal.Claims, SubsystemCitizenAppeals.BaseSystemKey);

            // по исполнителю
            var ssExecutors = QueriableFilterExtensions.GetFromClaimsSsExecutor(claimsPrincipal.Claims)
                .Intersect(ssAvail);
            var executorIn = main
                .Where(el => el.AppealType == AppealTypeEnum.NORMAL && el.Subsystem.UID != null && ssExecutors.Any(x => x == el.Subsystem.UID))
                .FilterByReadAllowedRubricatorExecutor(userId, SubsystemCitizenAppeals.BaseSystemKey);

            // обращения обратной связи
            var feedbackIn = main
                .Where(el => el.AppealType == AppealTypeEnum.FEEDBACK && el.Subsystem.UID != null && ssAvail.Any(x => x == el.Subsystem.UID))
                .FilterByReadAllowedFeedBackAdmin(claimsPrincipal.Claims);

            return moderatorIn
                .Concat(executorIn)
                .Concat(feedbackIn)
                .Distinct();
        }

        //private static bool HasModerateClaim(ClaimsPrincipal principal)
        //{
        //    var ssList = new List<string>
        //        {SubsystemCitizenAppeals.BaseSystemKey, SubsystemOrganizationAppeals.BaseSystemKey};

        //    return ssList.Any(ss => principal.HasClaim(ClaimName.Moderate, $"{ss}.{SSClaimSuffix.Md.enable}"));
        //}

        private static ExpressionStarter<Appeal> FilterByReadAllowedFeedBackAdminPredicate(IEnumerable<Claim> claims)
        {
            var ssFbAdm = QueriableFilterExtensions.GetFromClaimsSsFeedbackAdmin(claims);
            var predicate = PredicateBuilder.New<Appeal>()
                .And(el => el.Subsystem.UID != null && ssFbAdm.Contains(el.Subsystem.UID));
            return predicate;
        }

        private static IQueryable<Appeal> FilterByReadAllowedFeedBackAdmin(
            this IQueryable<Appeal> main,
            IEnumerable<Claim> claims
        )
        {
            var ssFbAdm = QueriableFilterExtensions.GetFromClaimsSsFeedbackAdmin(claims);
            return main.Where(el => ssFbAdm.Contains(el.Subsystem.UID));
        }
        public static IQueryable<Appeal> FilterByModeratorAsync(this IQueryable<Appeal> main, User moderator, List<string> subsystemUidList)
        {
            var result = main.Where(el => false);
            if (moderator == null)
            {
                return result;
            }
            result = result.Concat(main.FilterByReadAllowedSubsystemRights(moderator.AllClaims, subsystemUidList));
            result = result.Concat(main.FilterByReadAllowedRubricatorModerator(moderator.Id, subsystemUidList));
            result = result.Concat(main.Where(el => el.UserModeratorAppeal.Select(m => m.UserId).Contains(moderator.Id)));
            return result.Distinct();
        }
        public static IQueryable<Appeal> FilterByExecutorAsync(this IQueryable<Appeal> main, User moderator, List<string> subsystemUidList)
        {
            var result = main.Where(el => false);
            if (moderator == null)
            {
                return result;
            }
            result = result.Concat(main.FilterByReadAllowedRubricatorExecutor(moderator.Id, subsystemUidList));
            return result.Distinct();
        }

        public static async Task<BaseConstants.AllowedActionFlags> GetAllowedActions<Appeal>(
            long appealId,
            long? userId,
            ClaimsPrincipal claimsPrincipal,
            DocumentsContext ctx,
            SettingsManager sm
        )
        {
            var appeal = await ctx.Appeals
                .Where(el => el.Id == appealId)
                .FirstOrDefaultAsync();

            if (appeal == null)
            {
                return 0;
            }

            return await appeal.GetAllowedActions(userId, claimsPrincipal?.Claims, sm, ctx);
        }

        public static async Task<BaseConstants.AllowedActionFlags> GetAllowedActions(
            this Appeal appeal,
            long? userId,
            IEnumerable<Claim> claims,
            SettingsManager sm,
            DocumentsContext ctx
        )
        {
            var flags = BaseConstants.AllowedActionFlags.None;
            string ssUid = null;
            do
            {
                if (appeal == null)
                {
                    break;
                }
                if (appeal.Public_)
                {
                    flags |= BaseConstants.AllowedActionFlags.Read;
                }
                if (!userId.HasValue)
                {
                    break;
                }

                var claimsArray = claims as Claim[] ?? claims.ToArray();

                flags |= appeal.GetAllowedSuperAdminActions(claimsArray);

                if (userId.Value == appeal.AuthorId)
                {
                    flags |= BaseConstants.AllowedActionFlags.Read;
                    flags |= BaseConstants.AllowedActionFlags.AuthorOf;
                }
                ssUid = await GetSS_UID(appeal, ctx);
                if (
                    ssUid != null &&
                    !claimsArray.Any(el => el.Type == ClaimName.Subsystem && el.Value == $"{ssUid}.{SSClaimSuffix.Ss.available}"))
                {
                    break;
                }

                if (
                    ssUid != null &&
                    claimsArray.Any(el =>
                        el.Type == ClaimName.Subsystem && el.Value == $"{ssUid}.{SSClaimSuffix.Ss.create_appeal}")
                )
                {
                    flags |= BaseConstants.AllowedActionFlags.Create;
                }

                /*костылик для Ани*/
                if (
                    ssUid != null
                    && claimsArray.Any(el => el.Type == ClaimName.Subsystem && el.Value == $"{ssUid}.{SSClaimSuffix.Ss.feedback_admin}")
                    && appeal.AppealType == AppealTypeEnum.FEEDBACK
                )
                {
                    flags |= BaseConstants.AllowedActionFlags.Moderate;
                    flags |= BaseConstants.AllowedActionFlags.Edit;
                    flags |= BaseConstants.AllowedActionFlags.Read;
                }

                flags |= await appeal.GetAllowedModerationActions(userId, claimsArray, sm, ctx, ssUid);
                if (appeal.UserModeratorAppeal != null && appeal.UserModeratorAppeal.Select(el => el.UserId).Contains(userId.Value))
                {
                    flags |= BaseConstants.AllowedActionFlags.Moderate;
                    flags |= BaseConstants.AllowedActionFlags.Read;
                }
            } while (false);
            appeal.GetAMAFinal(sm, ref flags, ssUid);
            return flags;
        }

        private static async Task<string> GetSS_UID(Appeal appeal, DocumentsContext ctx)
        {
            if (appeal == null || ctx == null)
            {
                return null;
            }

            await appeal.LoadReference(appeal.Subsystem, ctx);

            return appeal.Subsystem?.UID;
        }
        //public static async Task<IEnumerable<long>> GelAllModerators(long appealId, DocumentsContext ctx)
        //{
        //    return await GetAllModerators(
        //        await ctx.Appeals
        //            .Where(el => !el.Deleted)
        //            .Where(el => el.Id == appealId)
        //            .Include(el => el.UserModeratorAppeal)
        //            .Include(el => el.Region).ThenInclude(el => el.UserModerator)
        //            .Include(el => el.Rubric).ThenInclude(el => el.UserModerator)
        //            .Include(el => el.Territory).ThenInclude(el => el.UserModerator)
        //            .Include(el => el.Topic).ThenInclude(el => el.UserModerator)
        //            .SingleOrDefaultAsync(), ctx);
        //}

        //public static async Task<IEnumerable<long>> GetAllModerators(this Appeal obj, DocumentsContext ctx)
        //{
        //    var moderators = Array.Empty<long>();
        //    moderators.Concat(obj.UserModeratorAppeal.Select(el => el.UserId));
        //    if (obj.Region != null)
        //    {
        //        moderators.Concat(await obj.Region.GetModerators(ctx.Regions));
        //    }
        //    if (obj.Rubric != null)
        //    {
        //        moderators.Concat(await obj.Rubric.GetModerators(ctx.Rubrics));
        //    }
        //    if (obj.Territory != null)
        //    {
        //        moderators.Concat(await obj.Territory.GetModerators(ctx.Territories));
        //    }
        //    if (obj.Topic != null)
        //    {
        //        moderators.Concat(await obj.Topic.GetModerators(ctx.Topics));
        //    }
        //    return moderators.Distinct();
        //}

        public static IQueryable<Appeal> MakeQueryEntity(this IQueryable<Appeal> obj, GraphQlKeyDictionary requestedFields, List<GraphQlQueryFilter> filters, SelectionRange range, bool last = false)
        {
            obj = obj
                .IncludeByFields(requestedFields, last)
                .IncludeByNotMappedFields(requestedFields)
                .FiltersBy(filters)
                .OrderByFieldNames(nameof(BaseIdEntity.Weight), range?.OrderBy)
                .GetRange(range?.Take, range?.Skip);
            return obj;
        }

        /*а не является ли юзер модератором из списка Appeal.UserModeratorAppeal*/
        public static async Task<BaseConstants.AllowedActionFlags> GetAMFUMA(this Appeal localObj, long? userId, DocumentsContext ctx)
        {
            var flags = BaseConstants.AllowedActionFlags.None;

            if (localObj == null || !userId.HasValue || ctx == null)
            {
                return flags;
            }

            if (ctx.Entry(localObj).State == EntityState.Detached)
            {
                return flags;
            }

            await ctx.Entry(localObj)
                .Collection(el => el.UserModeratorAppeal)
                .Query()
                .Where(el => el.AppealId == localObj.Id)
                .LoadAsync();

            if (localObj.UserModeratorAppeal?.FirstOrDefault(el => el.UserId == userId.Value) == null)
            {
                return flags;
            }

            flags |= BaseConstants.AllowedActionFlags.Read;
            flags |= BaseConstants.AllowedActionFlags.Moderate;

            return flags;
        }

    }
}
