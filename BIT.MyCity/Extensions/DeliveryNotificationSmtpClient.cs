using MailKit;
using MailKit.Net.Smtp;
using MimeKit;

namespace BIT.MyCity.Extensions
{
    public class DeliveryNotificationSmtpClient : SmtpClient
    {
        protected override DeliveryStatusNotification? GetDeliveryStatusNotifications(MimeMessage message, MailboxAddress mailbox)
        {
            return DeliveryStatusNotification.Success | DeliveryStatusNotification.Delay | DeliveryStatusNotification.Failure;
        }
    }
}
