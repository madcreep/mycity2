using BIT.MyCity.Database;
using System.Linq;

namespace BIT.MyCity.Extensions
{
    public static class QueryableIncludeExtensions
    {
        public static IQueryable<T> IncludeByNotMappedFields<T>(this IQueryable<T> obj, GraphQlKeyDictionary fields)
        {
            return obj;
        }
        public static IQueryable<T> IncludeByFields<T>(this IQueryable<T> obj, GraphQlKeyDictionary fields, bool last = false) where T : class
        {
            if (fields == null)
            {
                return obj;
            }

            var pl = fields.ToPropertyPathList(string.Empty);

            return pl.Aggregate(obj, (current, p) => current.IncludeByPathIgnoreCase(p, last));
        }

        public static IQueryable<T> IncludeFilterByFields<T>(this IQueryable<T> obj, GraphQlKeyDictionary fields, bool last = false) where T : class
        {
            if (fields == null)
            {
                return obj;
            }

            var pl = fields.ToPropertyPathList(string.Empty);

            return pl.Aggregate(obj, (current, p) => current.IncludeFilterByPathIgnoreCase(p, last));
        }
    }
}
