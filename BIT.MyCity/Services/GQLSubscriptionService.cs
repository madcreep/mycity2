using BIT.MyCity.Database;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading;
using BIT.MyCity.Authorization.Models;

namespace BIT.MyCity.Services
{
    public class ObjectChangedArgs : EventArgs
    {
        public object ChangedObject { get; }
        public ObjectChangedArgs(object obj)
        {
            ChangedObject = obj;
        }
    }
    public delegate void ObjectChangedHandler(object sender, ObjectChangedArgs args);
    public class GQLSubscriptionService
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(GQLSubscriptionService));
        private readonly IServiceProvider _serviceProvider;
        private readonly IServiceScopeFactory _scopeFactory;

        public event ObjectChangedHandler SettingChanged;
        public event ObjectChangedHandler MessageChanged;
        public event ObjectChangedHandler AuthorizeChanged;

        public GQLSubscriptionService(IServiceScopeFactory scopeFactory, IServiceProvider servicesProvider)
        {
            _scopeFactory = scopeFactory;
        }
        public void OnSettingsChanged(Settings changedSetting)
        {
            ThreadPool.QueueUserWorkItem((s) =>
            {
                SettingChanged?.Invoke(
                    this,
                    new ObjectChangedArgs(s));

            }, changedSetting);
        }
        public void OnMessageChanged(Message message)
        {
            ThreadPool.QueueUserWorkItem((s) =>
            {
                MessageChanged?.Invoke(
                    this,
                    new ObjectChangedArgs(s));

            }, message);
        }

        public void OnAuthorizeChanged(LoginResult loginResult)
        {
            if (string.IsNullOrWhiteSpace(loginResult.Key))
            {
                return;
            }

            ThreadPool.QueueUserWorkItem((s) =>
            {
                AuthorizeChanged?.Invoke(
                    this,
                    new ObjectChangedArgs(s));

            }, loginResult);
        }
    }
}
