using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using log4net;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Services
{
    public class ModeratorsNotificationService
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ModeratorsNotificationService));
        private readonly IServiceProvider _serviceProvider;
        
        public ModeratorsNotificationService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        
        private int? GetAppealInterval()
        {
            var sm = _serviceProvider.GetRequiredService<SettingsManager>();

            var intervalInHoursStr = sm.SettingValue(SettingsKeys.SystemApiNotificationAppealsAppealIntrval);

            var hasInterval = int.TryParse(intervalInHoursStr, out var interval)
                              && interval > 0;

            return hasInterval ? interval : (int?) null;
        } 

        public async Task SendEmails()
        {
            try
            {
                var appealInterval = GetAppealInterval();
                
                if (!appealInterval.HasValue)
                {
                    return;
                }

                var appeals = await GetNotModerateAppeals(appealInterval.Value);

                if (!appeals.Any())
                {
                    return;
                }

                foreach (var appeal in appeals)
                {
                    await SendEmail(appeal);
                }
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка отправки повторных уведомлений модераторам", ex);
            }
        }

        private async Task<Appeal[]> GetNotModerateAppeals(int intervalInHours)
        {
            var context = _serviceProvider.GetRequiredService<DocumentsContext>();

            var dateTo = DateTime.UtcNow.AddHours(-intervalInHours);

            return await context.Appeals
                .Where(el => el.Public_ && el.Approved == ApprovedEnum.UNDEFINED && el.CreatedAt <= dateTo && el.Deleted == false)
                .Include(el=>el.Subsystem)
                .ToArrayAsync();
        }

        private async Task SendEmail(Appeal appeal)
        {
            if (appeal == null)
            {
                return;
            }

            try
            {
                var appealManager = _serviceProvider.GetRequiredService<AppealManager>();

                var moderatorsQuery = appealManager.GetAllModeratorsQuery(appeal);

                var toSend = await moderatorsQuery
                    .Where(el=> el.Disconnected == false)
                    .Where(el => el.Email != null)
                    .ToArrayAsync();
                
                var context = _serviceProvider.GetRequiredService<DocumentsContext>();

                foreach (var moderator in toSend)
                {
                    var mailMessage = new MailMessage
                    {
                        Email = moderator.Email,
                        TemplateKey = "appealRegisteredModerator",
                        AddingEntity = new List<EmailEntityMetadata>
                        {
                            EmailEntityMetadata.Generate(context, moderator),
                            EmailEntityMetadata.Generate(context, appeal)
                        }
                    };

                    var emailService = _serviceProvider.GetRequiredService<MailService>();

                    await emailService.SendEmailAsync(mailMessage);
                }
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка отправки повторных сообщений о новом обращении ({appeal.Id}) модераторам", ex);
            }
        }
    }
}
