using BIT.MyCity.Database;
using log4net;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Gc;
using BIT.MyCity.Managers;

namespace BIT.MyCity.Services
{
    public class AttachService
    {
        private const int DefaultUncheckMinutes = -120;
        private readonly ILog _log = LogManager.GetLogger(typeof(AttachService));
        private readonly IServiceScopeFactory _scopeFactory;

        public AttachService(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task ClearNotUsedFiles()
        {
            using var scope = _scopeFactory.CreateScope();

            var freezingTime = GetFreezingTime(scope);

            var ctx = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

            var attachFileManager = scope.ServiceProvider.GetRequiredService<AttachedFileManager>();

            await DeleteIncompleteFiles(attachFileManager, ctx, freezingTime);

            await DeleteUnusedFiles(attachFileManager, scope.ServiceProvider, freezingTime);

            await DeleteUnusedDirectories(attachFileManager, ctx);
        }

        private async Task DeleteIncompleteFiles(AttachedFileManager attachFileManager, DocumentsContext ctx, DateTime freezingTime)
        {
            _log.Debug("DeleteIncompleteFiles Started");

            var files = ctx.AttachedFiles
                .Where(el =>
                    el.Incomplete && el.CreatedAt < freezingTime &&
                    (el.UpdatedAt == null || el.UpdatedAt < freezingTime))
                .ToArray();

            foreach (var file in files)
            {
                attachFileManager.DeleteAttach(file);
            }

            await ctx.SaveChangesAsync();
        }
        
        private async Task DeleteUnusedFiles(AttachedFileManager attachFileManager, IServiceProvider provider, DateTime freezingTime)
        {
            _log.Debug("DeleteUnusedFiles Started");

            var ctx = provider.GetRequiredService<DocumentsContext>();

            var cleaners = provider.GetServices<IGcCleaner>()
                .ToArray();

            IQueryable<long> usingIdQuery = null;

            if (cleaners.Any())
            {
                foreach (var cleaner in cleaners)
                {
                    if (usingIdQuery == null)
                    {
                        usingIdQuery = cleaner.UsingFilesIdsQuery(ctx);

                        continue;
                    }

                    usingIdQuery = usingIdQuery
                        .Union(cleaner.UsingFilesIdsQuery(ctx));
                }
            }

            var notUsedIdsQuery = ctx.AttachedFiles
                .Where(el => el.CreatedAt < freezingTime &&
                                        (el.UpdatedAt == null || el.UpdatedAt < freezingTime));

            if (usingIdQuery != null)
            {
                usingIdQuery = usingIdQuery
                    .Distinct();

                notUsedIdsQuery = notUsedIdsQuery
                    .Where(el => !usingIdQuery.Contains(el.Id));
            }
            
            var notUsedIds = await notUsedIdsQuery
                .Select(el=>el.Id)
                .ToArrayAsync();

            var loaded = 0;

            _log.Debug($"DeleteUnusedFiles notUsedIds : {string.Join(" ", notUsedIds)}");

            while (true)
            {
                var notUsedIdsPart = notUsedIds
                    .Skip(loaded)
                    .Take(20)
                    .ToArray();

                if (!notUsedIdsPart.Any())
                {
                    break;
                }

                loaded += notUsedIdsPart.Length;

                var notUsedFiles = await ctx.AttachedFiles
                    .Where(el => notUsedIdsPart.Contains(el.Id))
                    .ToArrayAsync();

                foreach (var file in notUsedFiles)
                {
                    attachFileManager.DeleteAttach(file);
                }

                await ctx.SaveChangesAsync();
            }
        }

        private async Task DeleteUnusedDirectories(AttachedFileManager attachFileManager, DocumentsContext ctx)
        {
            _log.Debug("DeleteUnusedDirectories Started");

            var allDirectories = attachFileManager.GetAllDirectories();

            var allDbDirectories = await ctx.AttachedFiles
                .GroupBy(el => el.StoragePath)
                .Select(el => Path.Combine(attachFileManager.FileStorageDirectory, el.Key))
                .ToArrayAsync();

            var toDeleteDirectories = allDirectories
                .Except(allDbDirectories)
                .Except(new[] { attachFileManager.TodayDirectory })
                .ToArray();

            if (toDeleteDirectories.Any())
            {
                foreach (var directory in toDeleteDirectories)
                {
                    attachFileManager.DeleteDirectory(directory);
                }
            }
        }

        private DateTime GetFreezingTime(IServiceScope scope)
        {
            var utcNow = DateTime.UtcNow;

            try
            {
                var settingsManager = scope.ServiceProvider.GetRequiredService<SettingsManager>();

                var uncheckMinutes = settingsManager.SettingValue<uint>(SettingsKeys.SystemOptionsFileCleanerUncheckPeriod);

                return utcNow.AddMinutes(-uncheckMinutes);
            }
            catch (Exception ex)
            {
                _log.Warn("Ошибка получения интервал заморозки файлов.", ex);

                return utcNow.AddMinutes(DefaultUncheckMinutes);
            }
        }
    }
}
