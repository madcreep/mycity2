using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using log4net;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.DependencyInjection;
using MimeKit;
using RazorLight;
using ContentDisposition = MimeKit.ContentDisposition;

namespace BIT.MyCity.Services
{
    public partial class MailService : IDisposable
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(MailService));

        private readonly Queue<Email> _messages = new Queue<Email>();
        
        private readonly IServiceProvider _serviceProvider;

        private readonly Thread _sendThread;
        
        private bool _work = true;

        public MailService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            _sendThread = new Thread(SendThread);

            _sendThread.Start();
        }

        public async Task<bool> InitializeDbAsync()
        {
            try
            {
                var dbKeys = await GetTemplateKeysFromDb();

                var files = GetTemplateFiles()
                    .ToDictionary(el => Path.GetFileNameWithoutExtension(el.Name).ToJsonFormat());

                var forAdd = (from f in files
                              join k in dbKeys on f.Key equals k into joinKeys
                              from joinKey in joinKeys.DefaultIfEmpty(null)
                              where joinKey == null
                              select f)
                    .ToArray();

                if (!forAdd.Any())
                {
                    return true;
                }

                var scopeFactory =
                    _serviceProvider.GetRequiredService<IServiceScopeFactory>();

                using var scope = scopeFactory.CreateScope();

                await using var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                foreach (var (key, file) in forAdd)
                {
                    if (!file.Exists)
                    {
                        continue;
                    }

                    using var stream = file.OpenText();

                    context.EmailTemplates.Add(new EmailTemplate
                    {
                        Key = key,
                        Template = stream.ReadToEnd().Trim()
                    });

                    stream.Close();

                    stream.Dispose();
                }

                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка инициализации таблицы \"EmailTemplates\"", ex);

                return false;
            }
        }

        public async Task<long> SendEmailAsync(MailMessage mailMessage)
        {
            var sm = _serviceProvider.GetRequiredService<SettingsManager>();

            if (!sm.SettingValue<bool>(SettingsKeys.SystemSmtpEnable))
            {
                return 0;
            }

            var message = await AddToDb(mailMessage);

            lock (_messages)
            {
                _messages.Enqueue(message);

                Monitor.Pulse(_messages);
            }

            return message.Id;
        }

        public async Task<long> SendEmailAsync(CustomEmail mailMessage)
        {
            var sm = _serviceProvider.GetRequiredService<SettingsManager>();

            if (!sm.SettingValue<bool>(SettingsKeys.SystemSmtpEnable))
            {
                return 0;
            }

            var message = await AddToDb(mailMessage);

            lock (_messages)
            {
                _messages.Enqueue(message);

                Monitor.Pulse(_messages);
            }

            return message.Id;
        }

        private void SendThread()
        {
            while (_work)
            {
                Email message = null;

                try
                {
                    lock (_messages)
                    {
                        if (_messages.Count == 0)
                        {
                            Monitor.Wait(_messages);
                        }

                        if (!_messages.TryPeek(out message))
                        {
                            continue;
                        }
                    }

                    if (message.Template == null)
                    {
                        message.Template = GenerateTemplate(message);

                        if (message.Template != null)
                        {
                            UpdateMessageDb(message);
                        }
                    }

                    if (message.Template != null)
                    {
                        var sendResult = SendEmail(message);

                        if (sendResult)
                        {
                            message.SendingTimestamp = DateTime.UtcNow;

                            UpdateMessageDb(message);
                        }
                    }

                    lock (_messages)
                    {
                        _ = _messages.Dequeue();
                    }
                }
                catch (Exception ex)
                {
                    if (message != null)
                    {
                        lock (_messages)
                        {
                            _messages.Dequeue();
                        }
                    }

                    _log.Error("Ошибка потока отправки сообщений электронной почты.", ex);

                    Thread.Sleep(500);
                }
            }
        }

        private bool SendEmail(Email message)
        {
            try
            {
                var scopeFactory =
                    _serviceProvider.GetRequiredService<IServiceScopeFactory>();

                using var scope = scopeFactory.CreateScope();

                var settingsManager = scope.ServiceProvider.GetRequiredService<SettingsManager>();

                var emailMessage = new MimeMessage();

                emailMessage.From.Add(new MailboxAddress(
                    message.MailMessage.FromName ?? settingsManager.SettingValue(SettingsKeys.SystemSmtpFromName),
                    message.MailMessage.FromEmail ?? settingsManager.SettingValue(SettingsKeys.SystemSmtpFrom)));

                emailMessage.To.Add(new MailboxAddress(
                    message.MailMessage.ToName,
                    message.MailMessage.Email));

                if (message.MailMessage.DuplicateToSender)
                {
                    emailMessage.Bcc.Add(new MailboxAddress(
                        settingsManager.SettingValue(SettingsKeys.SystemSmtpFromName),
                        settingsManager.SettingValue(SettingsKeys.SystemSmtpFrom)));
                }

                if (message.MailMessage.AdditionalRecipients != null && message.MailMessage.AdditionalRecipients.Any())
                {
                    emailMessage.To.AddRange(message.MailMessage.AdditionalRecipients
                        .Select(el=>new MailboxAddress(el.Name ?? el.Email, el.Email)));
                }

                emailMessage.Subject = string.IsNullOrWhiteSpace(message.MailMessage.Subject)
                    ? settingsManager.SettingValue(SettingsKeys.SystemSmtpFromName)
                    : message.MailMessage.Subject;

                

                var builder = new BodyBuilder
                {
                    HtmlBody = message.Template
                };
                
                if (!AddFiles(builder, message.MailMessage.FilesIds))
                {
                    throw new Exception("Ошибка добавления файлов.");
                }

                emailMessage.Body = builder.ToMessageBody();

                if (!int.TryParse(settingsManager.SettingValue(SettingsKeys.SystemSmtpPort), out var smtpPort))
                {
                    smtpPort = 0;
                }

                if (!bool.TryParse(settingsManager.SettingValue(SettingsKeys.SystemSmtpUseSsl), out var useSsl))
                {
                    useSsl = false;
                }

                _log.Debug($"Send Email UseSsl = {useSsl}");

                if (!bool.TryParse(settingsManager.SettingValue(SettingsKeys.SystemSmtpCheckCertificateRevocation),
                    out var checkCertificateRevocation))
                {
                    checkCertificateRevocation = true;
                }

                using var client = message.MailMessage.WithDeliveryNotice
                    ? new DeliveryNotificationSmtpClient()
                    : new SmtpClient();

                client.CheckCertificateRevocation = checkCertificateRevocation;

                //using var client = new SmtpClient
                //{
                //    CheckCertificateRevocation = checkCertificateRevocation
                //};

                if (!checkCertificateRevocation)
                {
                    client.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
                }

                client.Connect(
                    settingsManager.SettingValue(SettingsKeys.SystemSmtpHost),
                    smtpPort,
                    useSsl ? MailKit.Security.SecureSocketOptions.Auto : MailKit.Security.SecureSocketOptions.None);

                var userLogin = settingsManager.SettingValue(SettingsKeys.SystemSmtpUser);

                if (!string.IsNullOrWhiteSpace(userLogin))
                {
                    client.Authenticate(
                        userLogin,
                        settingsManager.SettingValue(SettingsKeys.SystemSmtpPassword));
                }

                try
                {
                    client.Send(emailMessage);

                    client.Disconnect(true);
                }
                finally
                {
                    client.Dispose();
                }

                return true;
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка отправки сообщения электронной почты (Id = {message?.Id})", ex);

                return false;
            }
        }

        private bool AddFiles(BodyBuilder builder, IReadOnlyCollection<long> mailMessageFilesIds)
        {
            try
            {
                if (mailMessageFilesIds == null || !mailMessageFilesIds.Any())
                {
                    return true;
                }

                var scopeFactory =
                    _serviceProvider.GetRequiredService<IServiceScopeFactory>();
                
                using var scope = scopeFactory.CreateScope();

                using var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                var files = context.AttachedFiles
                    .Where(el => mailMessageFilesIds.Contains(el.Id))
                    .ToArray();

                if (files.Length != mailMessageFilesIds.Count)
                {
                    var notFoundIds = mailMessageFilesIds.Except(files.Select(el => el.Id));

                    _log.Error(
                        $"Ошибка добавления файлов электронной почты. В БД отсутствуют файлы с Id = {string.Join(", ", notFoundIds)}");

                    return false;
                }

                if (files.Any(el => el.Incomplete))
                {
                    return false;
                }

                var attachedFileManager = scope.ServiceProvider.GetRequiredService<AttachedFileManager>();

                foreach (var item in files)
                {
                    var path = attachedFileManager.GetFileFullName(item);

                    if (!File.Exists(path))
                    {
                        _log.Error($"Ошибка добавления файла электронной почты. Файл <{path}> отсутствует");

                        return false;
                    }

                    var fileName = Path.GetFileName(item.Name);

                    var attachment = new MimePart(MimeTypes.GetMimeType(fileName))
                    {
                        Content = new MimeContent(File.OpenRead(path)),
                        ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                        ContentTransferEncoding = ContentEncoding.Base64,
                        FileName = fileName
                    };

                    builder.Attachments.Add(attachment);
                }

                return true;
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка добавления файлов в сообщение электронной почты", ex);

                return false;
            }
        }

        private async Task<Email> AddToDb(MailMessage mailMessage)
        {
            var message = new Email
            {
                MailMessage = mailMessage
            };

            var scopeFactory =
                _serviceProvider.GetRequiredService<IServiceScopeFactory>();
            
            using var scope = scopeFactory.CreateScope();

            await using var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

            context.Emails.Add(message);

            await context.SaveChangesAsync();

            return message;
        }

        private async Task<Email> AddToDb(CustomEmail mailMessage)
        {
            var scopeFactory =
                _serviceProvider.GetRequiredService<IServiceScopeFactory>();
            
            using var scope = scopeFactory.CreateScope();

            await using var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

            var innerMessage = new MailMessage
            {
                Email = mailMessage.To.First().Email,
                ToName = mailMessage.To.First().Name,
                Subject = mailMessage.Subject,
                FilesIds = mailMessage.FilesId?.ToArray() ?? Array.Empty<long>(),
                WithDeliveryNotice = mailMessage.WithDeliveryNotice,
                DuplicateToSender = mailMessage.DuplicateToSender
            };

            if (mailMessage.To.Count > 1)
            {
                innerMessage.AdditionalRecipients = mailMessage.To
                    .Where(el => el.Email != innerMessage.Email || el.Name != innerMessage.ToName)
                    .ToArray();
            }

            if (mailMessage.From != null)
            {
                innerMessage.FromEmail = mailMessage.From.Email;
                innerMessage.FromName = mailMessage.From.Name;
            }

            var message = new Email
            {
                Template = mailMessage.Message,
                MailMessage = innerMessage

            };

            if (mailMessage.FilesId != null && mailMessage.FilesId.Any())
            {
                message.AttachedFilesToEmails ??= new List<AttachedFilesToEmails>();

                var files = await context.AttachedFiles
                    .Where(el => mailMessage.FilesId.Contains(el.Id))
                    .ToArrayAsync();

                message.AttachedFilesToEmails.AddRange(files.Select(el=>new AttachedFilesToEmails{AttachedFile = el, AttachedFileId = el.Id}));
            }

            context.Emails.Add(message);

            await context.SaveChangesAsync();

            return message;
        }

        private string GenerateTemplate(Email mailMessage)
        {
            EmailTemplate template;

            dynamic model;

            if (mailMessage.MailMessage.TemplateKey == null)
            {
                return mailMessage.Message;
            }

            var scopeFactory =
                _serviceProvider.GetRequiredService<IServiceScopeFactory>();
            
            using (var scope = scopeFactory.CreateScope())
            {
                using var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                template = context.EmailTemplates
                    .SingleOrDefault(el => el.Key == mailMessage.MailMessage.TemplateKey);

                model = GenerateModel(scope.ServiceProvider, mailMessage);
            }

            if (template == null)
            {
                _log.Error($"Ошибка формирования сообщения электронной почты. Шаблон с ключом <{mailMessage.MailMessage.TemplateKey}> не найден.");

                return null;
            }

            var innerTempleteKey =
                $"{template.Key}_{(template.UpdatedAt ?? template.CreatedAt).Ticks}";

            try
            {
                var engine = new RazorLightEngineBuilder()
                    .UseMemoryCachingProvider()
                    .UseEmbeddedResourcesProject(typeof(Program))
                    .Build();

                var cacheResult = engine.Handler.Cache.RetrieveTemplate(innerTempleteKey);
                
                Task<string> task;

                if (cacheResult.Success)
                {
                    task = engine.RenderTemplateAsync(cacheResult.Template.TemplatePageFactory(), model);

                    task.Wait();

                    return task.Result;
                }
                                     
                task = engine.CompileRenderStringAsync(innerTempleteKey, template.Template, model);

                task.Wait();
                
                return task.Result;
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка формирования сообщения электронной почты", ex);

                return null;
            }
        }

        private dynamic GenerateModel(IServiceProvider serviceProvider, Email mailMessage)
        {
            var model = mailMessage
                .DynamicMessage
                .TemplateModel
                ?? new ExpandoObject();

            model.CreatedAt = mailMessage.CreatedAt.ToLocalTime();

            var addingEntity = mailMessage.MailMessage.AddingEntity;

            if (addingEntity == null)
            {
                return model;
            }

            var settings = GetSettings(mailMessage.MailMessage.TemplateKey);

            if (addingEntity.Any())
            {
                foreach (var metadata in addingEntity)
                {
                    var expandoDictionary = model as IDictionary<string, object>;

                    var entityType = TypeByName(metadata.TypeName);

                    var entityName = string.IsNullOrWhiteSpace(metadata.CustomName)
                        ? entityType.Name
                        : metadata.CustomName;

                    if (metadata.Entity != null)
                    {
                        expandoDictionary?.Add(entityName, metadata.Entity);

                        continue;
                    }

                    var method = typeof(MailService).GetMethod("GetEntity");

                    if (method == null)
                    {
                        continue;
                    }

                    var genericMethod = method.MakeGenericMethod(entityType);

                    var entityInclude = settings.EntityInclude != null && settings.EntityInclude.TryGetValue(entityName, out var value)
                        ? value
                        : null;

                    var entity = genericMethod
                        .Invoke(this, new object[] { metadata.KeyValue, entityInclude });

                    expandoDictionary?.Add(entityName, entity);
                }
            }

            model.SettingsManager = serviceProvider.GetRequiredService<SettingsManager>();

            GenerateSubject(mailMessage, model);

            return model;
        }

        private void UpdateMessageDb(Email message)
        {
            var scopeFactory =
                _serviceProvider.GetRequiredService<IServiceScopeFactory>();
            
            using var scope = scopeFactory.CreateScope();

            using var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

            var dbMessage = context.Emails
                .SingleOrDefault(el => el.Id == message.Id);

            if (dbMessage == null)
            {
                return;
            }

            dbMessage.SendingTimestamp = message.SendingTimestamp;
            dbMessage.Template = message.Template;
            dbMessage.MailMessage = message.MailMessage;

            context.SaveChanges();
        }

        internal async Task LoadNotSentFromDb()
        {
            var sm = _serviceProvider.GetRequiredService<SettingsManager>();

            if (!sm.SettingValue<bool>(SettingsKeys.SystemSmtpEnable))
            {
                return;
            }

            List<Email> result;

            long[] sendingIds;

            lock (_messages)
            {
                sendingIds = _messages
                    .Select(el => el.Id)
                    .ToArray();
            }

            var scopeFactory =
                _serviceProvider.GetRequiredService<IServiceScopeFactory>();
            
            using (var scope = scopeFactory.CreateScope())
            {
                await using var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                result = await context.Emails
                    .Include(el=>el.AttachedFilesToEmails)
                    .ThenInclude(el=>el.AttachedFile)
                    .Where(el => !sendingIds.Contains(el.Id) && el.SendingTimestamp == null)
                    .OrderByDescending(el => el.CreatedAt)
                    .ToListAsync();
            }

            if (!result.Any())
            {
                return;
            }

            lock (_messages)
            {
                foreach (var message in result)
                {
                    _messages.Enqueue(message);
                }

                Monitor.Pulse(_messages);
            }

        }

        private async Task<IEnumerable<string>> GetTemplateKeysFromDb()
        {
            var scopeFactory =
                _serviceProvider.GetRequiredService<IServiceScopeFactory>();
            
            using var scope = scopeFactory.CreateScope();

            await using var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

            var result = await context.EmailTemplates
                .Select(el => el.Key)
                .ToArrayAsync();

            return result;
        }

        private IEnumerable<FileInfo> GetTemplateFiles()
        {
            var environment = _serviceProvider.GetRequiredService<IWebHostEnvironment>();

            var templateDirPath = Path.Combine(environment.ContentRootPath, "EmailTemplates");

            var templateDir = new DirectoryInfo(templateDirPath);

            return !templateDir.Exists
                ? new FileInfo[0]
                : templateDir.GetFiles("*.cshtml");
        }

        public void Dispose()
        {
            if (!_work || _sendThread == null)
            {
                return;
            }

            _work = false;

            lock (_messages)
            {
                Monitor.Pulse(_messages);
            }

            if (!_sendThread.Join(new TimeSpan(0, 0, 5)))
            {
                _sendThread.Abort();
            }
        }

        public async Task<Email> GetMessageByIdAsync(long emailId)
        {
            var scopeFactory =
                _serviceProvider.GetRequiredService<IServiceScopeFactory>();
            
            using var scope = scopeFactory.CreateScope();

            await using var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

            var message = await context.Emails
                .SingleOrDefaultAsync(el => el.Id == emailId);

            return message;
        }

        private static Type TypeByName(string typeName)
        {
            if (string.IsNullOrWhiteSpace(typeName))
            {
                return typeof(object);
            }

            try
            {
                return Type.GetType(typeName);
            }
            catch
            {
                return typeof(object);
            }
        }

        public T GetEntity<T>(object[] keyObjects, params string[] includeEntity) where T : class
        {
            var scopeFactory =
                _serviceProvider.GetRequiredService<IServiceScopeFactory>();
            
            using var scope = scopeFactory.CreateScope();

            using var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

            var result = context.Set<T>()
                .Find(keyObjects);

            if (includeEntity == null || !includeEntity.Any())
            {
                return result;
            }

            foreach (var includeItem in includeEntity)
            {
                var entityEntry = context.Entry(result);

                var path = includeItem.Split('.')
                    .Select(el=>el.Trim())
                    .ToArray();

                LoadMembers(context, entityEntry, path, 0);
            }

            return result;
        }

        private static void LoadMembers(DbContext context, EntityEntry entry, IReadOnlyList<string> includeEntity, int index)
        {
            if (index >= includeEntity.Count)
            {
                return;
            }
            
            var isEnum = entry.Member(includeEntity[index]) is CollectionEntry;

            if (isEnum)
            {
                entry
                    .Collection(includeEntity[index])
                    .Load();
            }
            else
            {
                entry
                    .Reference(includeEntity[index])
                    .Load();
            }

            if (index >= includeEntity.Count - 1)
            {
                return;
            }

            if (isEnum)
            {
                foreach (var obj in (IEnumerable<object>) entry.Member(includeEntity[index]).CurrentValue)
                {
                    LoadMembers(context, context.Entry(obj), includeEntity, index + 1);
                }
            }
            else
            {
                LoadMembers(context, context.Entry(entry.Member(includeEntity[index]).CurrentValue), includeEntity,
                    index + 1);
            }
        }
    }
}
