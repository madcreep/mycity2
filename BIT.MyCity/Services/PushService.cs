using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using System.Threading;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Model;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using Microsoft.Extensions.Hosting;

namespace BIT.MyCity.Services
{
    public class PushService
    {
        
        private readonly ILog _log = LogManager.GetLogger(typeof(PushService));

        private readonly Queue<PushMessage> _messages = new Queue<PushMessage>();

        private readonly IServiceScopeFactory _scopeFactory;
        private readonly IServiceProvider _serviceProvider;
        
        private readonly Thread _sendThread;

        private Timer _stautsTimer;
        private object _stautsTimerSyncRoot = new object();

        private bool work = true;
        
        private Timer? _timer = null;
        private Task? _executingTask;
        private readonly CancellationTokenSource _stoppingCts = new CancellationTokenSource();

        public PushService(IServiceScopeFactory scopeFactory,
            IServiceProvider serviceProvider)
        {
            try
            {
                FirebaseApp.Create(new AppOptions()
                {
                    Credential = GoogleCredential.GetApplicationDefault(),
                });
            }
            catch (Exception ex)
            {
                _log.Error($"Error initializing Firebase messaging {ex.Message}", ex);
                //work = false;
            }

            _scopeFactory = scopeFactory;
            _serviceProvider = serviceProvider;

            _sendThread = new Thread(SendThread);
            
            _sendThread.Start();
        }
        
        public async Task SendAsync([NotNull] PushMessage message)
        {
            if (message == null)
            {
                return;
            }

            await AddToDb(message);

            lock (_messages)
            {
                _messages.Enqueue(message);

                Monitor.Pulse(_messages);
            }
        }

        private void SendThread()
        {
            PushMessage message = null;

            while (work)
            {
                try
                {
                    lock (_messages)
                    {
                        if (_messages.Count == 0)
                        {
                            RunStatusTimer();

                            Monitor.Wait(_messages);

                        }

                        if (!_messages.TryPeek(out message))
                        {
                            return;
                        }
                    }

                    using var scope = _scopeFactory.CreateScope();

                    var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                    var tokens = context.Tokens.Where(t => t.OwnerId == message.UserId).ToArray();
                    foreach (var token in tokens)
                    {

                        //if (cancellationToken.IsCancellationRequested) return;

                        var msg = new FirebaseAdmin.Messaging.Message
                        {
                            Notification = new Notification
                            {
                                Title = message.Title,
                                Body = message.Body
                            },
                            Token = token.Value
                        };


                        try
                        {
                            _log.Debug($"Pushing request: {msg.Token} on instance {FirebaseMessaging.DefaultInstance}");
                            var resultTask = FirebaseMessaging.DefaultInstance.SendAsync(msg);
                            resultTask.Wait();
                            var result = resultTask.Result;
                            _log.Debug($"Push sending result: {result}");
                        }
                        catch (FirebaseMessagingException ex)
                        {
                            _log.Error($"Token was not found and will be removed: {ex.Message}", ex);
                            context.Tokens.Remove(token);

                        }
                        catch (AggregateException ex)
                        {
                            if (ex.InnerException is FirebaseException)
                            {
                                _log.Error($"Token was not found and will be removed: {ex.Message}", ex);
                                context.Tokens.Remove(token);
                            }
                            else
                            {
                                _log.Error($"Unknown exception sending a push: {ex.Message}", ex);
                            }
                        }

                        catch (Exception ex)
                        {
                            _log.Error($"Unknown exception sending a push: {ex.Message}", ex);
                        }
                    }

                    context.SaveChanges();
                    RemoveFromDb(message);
                    _messages.Dequeue();
                }
                catch (Exception ex)
                {
                    if (message != null)
                    {
                        lock (_messages)
                        {
                            _messages.Dequeue();
                        }
                    }

                    _log.Error($"Ошибка потока отправки сообщения {ex.Message}", ex);

                    //await Task.Delay(TimeSpan.FromSeconds(1));
                    Thread.Sleep(200);
                }
            }
        }

        private async Task AddToDb(PushMessage pushMessage)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                context.PushMessages.Add(pushMessage);

                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                pushMessage.Status = PushMessageStatus.Error;

                pushMessage.ErrorMessage = $"Ошибка сохранения PUSH в БД.{Environment.NewLine}{ex}";
            }
        }

        private void UpdateToDb(PushMessage pushMessage)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                var dbMessage = context.PushMessages
                    .SingleOrDefault(el => el.Id == pushMessage.Id);

                if (dbMessage == null)
                {
                    context.PushMessages.Add(pushMessage);
                }
                else
                {
                    dbMessage.Status = pushMessage.Status;
                    dbMessage.ErrorMessage = pushMessage.ErrorMessage;
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка обновления PUSH в БД (Id={pushMessage.Id})", ex);
            }
        }

        private void RemoveFromDb(PushMessage pushMessage)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                var dbMessage = context.PushMessages
                    .SingleOrDefault(el => el.Id == pushMessage.Id);

                if (dbMessage != null)
                {
                    context.PushMessages.Remove(dbMessage);
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка обновления PUSH в БД (Id={pushMessage.Id})", ex);
            }
        }
        
        private void RunStatusTimer()
        {
            lock (_stautsTimerSyncRoot)
            {
                if (_stautsTimer != null)
                {
                    return;
                }

                _stautsTimer = new Timer(
                    LoadNotDeliveredFromDb,
                    null,
                    new TimeSpan(0, 0, 30),
                    new TimeSpan(0, 0, 30));
            }
        }

        private void LoadNotDeliveredFromDb(object obj)
        {
            try
            {

                using var scope = _scopeFactory.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                PushMessage[] toCheck;

                lock (_stautsTimerSyncRoot)
                {
                    toCheck = context.PushMessages
                        .Where(el => el.Status == PushMessageStatus.Created
                                      || el.Status == PushMessageStatus.Accepted
                                      || el.Status == PushMessageStatus.Queued).ToArray();

                    if (!toCheck.Any())
                    {

                        if (_stautsTimer == null)
                        {
                            return;
                        }

                        _stautsTimer.Dispose();

                        _stautsTimer = null;


                        return;
                    }
                }

                foreach (var message in toCheck)
                {
                    lock (_messages)
                    {
                        _messages.Enqueue(message);

                        Monitor.Pulse(_messages);
                    }

                }
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка получения сообщений PUSH из БД", ex);
            }

        }


    }
}
