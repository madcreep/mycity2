using BIT.MyCity.Subsystems;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using log4net;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Services
{
    public class SubsystemService
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(SubsystemService));

        private readonly Dictionary<string, IExternalSubsystem> _subsystems = new Dictionary<string, IExternalSubsystem>();

        private readonly IServiceProvider _serviceProvider;
        private readonly IServiceScopeFactory _scopeFactory;

        public SubsystemService(IServiceProvider serviceProvider, IServiceScopeFactory scopeFactory)
        {
            _serviceProvider = serviceProvider;
            _scopeFactory = scopeFactory;

            IExternalSubsystem ss = new SubsystemCitizenAppeals(serviceProvider, scopeFactory);

            _subsystems.Add(ss.SystemKey, ss);

            ss = new SubsystemOrganizationAppeals(serviceProvider, scopeFactory);

            _subsystems.Add(ss.SystemKey, ss);

            ss = new SubsystemVoting(serviceProvider, scopeFactory);

            _subsystems.Add(ss.SystemKey, ss);

            ss = new SubsystemNews(serviceProvider, scopeFactory);

            _subsystems.Add(ss.SystemKey, ss);

            UpdateSubsystemInfo();
        }

        public IEnumerable<IExternalSubsystem> Subsystems => _subsystems.Values;

        public IExternalSubsystem Subsystem(string subsystemKey)
        {
            return _subsystems.ContainsKey(subsystemKey)
                ? _subsystems[subsystemKey]
                : null;
        }

        private void UpdateSubsystemInfo()
        {
            var location = Assembly.GetEntryAssembly()?.Location;

            if (string.IsNullOrWhiteSpace(location))
            {
                _log.Warn("Не удалось получить базовуцю диекторию приложения.");

                return;
            }

            var appDirectory = Path.GetDirectoryName(location);

            if (string.IsNullOrWhiteSpace(appDirectory))
            {
                _log.Warn($"Не удалось получить базовуцю диекторию приложения (location = {location}).");

                return;
            }

            var path = Path.Combine(appDirectory, "ExternalSubsystems");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var files = Directory.GetFiles(path, "*.dll");
            
            foreach (var file in files)
            {
                try
                {
                    var assembly = Assembly.LoadFrom(file);

                    var types = assembly.GetTypes()
                        .Where(el => typeof(IExternalSubsystem).IsAssignableFrom(el))
                        .ToArray();

                    if (!types.Any())
                    {
                        continue;
                    }

                    foreach (var type in types)
                    {
                        var obj = (IExternalSubsystem)Activator.CreateInstance(type, _serviceProvider, _scopeFactory);

                        if (obj != null)
                        {
                            _subsystems.Add(obj.SystemKey, obj);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _log.Error("Ошибка плучения плагинов отчетов", ex);
                }
            }
        }
    }
}
