using BIT.MyCity.Database;
using log4net;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BIT.MyCity.Services
{
    public class VoteGarbageCollectorService
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(VoteGarbageCollectorService));
        private readonly IServiceProvider _serviceProvider;

        public VoteGarbageCollectorService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task Execute()
        {
            var oldest = DateTime.UtcNow.AddHours(-12);

            await GcVotes(oldest);
            await GcVoteItems(oldest);
        }

        private async Task GcVotes(DateTime oldest)
        {
            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            var votes = await ctx.Votes
                .Where(el => el.VoteRoot == null)
                .Where(el => (el.UpdatedAt ?? el.CreatedAt) != null && ((el.UpdatedAt ?? el.CreatedAt) < oldest))
                .Include(el => el.VoteItems)
                .Include(el => el.AttachedFilesToVotes)
                .Include(el => el.UserVote)
                .Include(el => el.ParentVoteItems)
                .ToListAsync();

            if (!votes.Any())
            {
                return;
            }

            foreach (var vote in votes)
            {
                try
                {
                    vote.VoteItems.Clear();

                    vote.AttachedFilesToVotes.Clear();

                    vote.UserVote.Clear();

                    vote.ParentVoteItems.Clear();

                    ctx.Votes.Remove(vote);
                }
                catch (Exception ex)
                {
                    _log.Error($"Очистка мусора. Ошибка удаления Vote (id = {vote.Id}).", ex);
                }
            }

            await ctx.SaveChangesAsync();
        }

        private async Task GcVoteItems(DateTime oldest)
        {
            var ctx = _serviceProvider.GetRequiredService<DocumentsContext>();

            var dirty = false;

            var voteItems = await ctx.VoteItems
                .Where(el => el.Vote == null)
                .Where(el => (el.UpdatedAt ?? el.CreatedAt) != null && ((el.UpdatedAt ?? el.CreatedAt) < oldest))
                .Include(el => el.AttachedFilesToVoteItems)
                .Include(el => el.NextVote)
                .ToListAsync();

            if (!voteItems.Any())
            {
                return;
            }

            foreach (var voteItem in voteItems)
            {
                try
                {
                    voteItem.AttachedFilesToVoteItems.Clear();

                    voteItem.NextVote = null;

                    ctx.VoteItems.Remove(voteItem);
                }
                catch (Exception ex)
                {
                    _log.Error($"Очистка мусора. Ошибка удаления VoteItem (id = {voteItem.Id}).", ex);
                }
            }

            await ctx.SaveChangesAsync();
        }
    }
}
