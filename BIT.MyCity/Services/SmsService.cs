using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using BIT.MyCity.Sms;
using BIT.MyCity.Sms.Models;
using BIT.MyCity.SMSHelpers;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using SmsMessage = BIT.MyCity.Database.SmsMessage;

namespace BIT.MyCity.Services
{
    public class SmsService
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(MailService));

        private readonly Queue<SmsMessage> _messages = new Queue<SmsMessage>();

        private readonly IServiceScopeFactory _scopeFactory;
        private readonly SettingsManager _settingsManager;
        private readonly IServiceProvider _serviceProvider;

        private readonly Thread _sendThread;

        private Timer _stautsTimer;
        private object _stautsTimerSyncRoot = new object();

        private bool work = true;

        public SmsService(IServiceScopeFactory scopeFactory,
            SettingsManager settingsManager,
            IServiceProvider serviceProvider)
        {
            _scopeFactory = scopeFactory;
            _settingsManager = settingsManager;
            _serviceProvider = serviceProvider;

            _sendThread = new Thread(SendThread);

            _sendThread.Start();
        }

        public async Task SendAsync([NotNull] SmsMessage message)
        {
            if (message == null)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(message.Message))
            {
                message.Status = SmsMessageStatus.FatalError;

                message.ErrorMessage = "Текст сообщения не может быть пустым";

                return;
            }

            if (!CheckNumber(message.PhoneNumber))
            {
                message.Status = SmsMessageStatus.FatalError;

                message.ErrorMessage = "Не верный формат номера";

                return;
            }

            await AddToDb(message);

            lock (_messages)
            {
                _messages.Enqueue(message);

                Monitor.Pulse(_messages);
            }
        }

        public async Task<Balance> GetBalanceAsync()
        {
            var smsHelper = GetHelper();

            var balance = smsHelper == null
                ? new Balance("Сервис отправки СМС не настроен")
                : await smsHelper.GetBalanceAsync();

            return balance;
        }

        private void SendThread()
        {
            ISmsHelper smsHelper = null;

            while (work)
            {
                SmsMessage message = null;

                try
                {
                    lock (_messages)
                    {
                        if (_messages.Count == 0)
                        {
                            RunStatusTimer();

                            Monitor.Wait(_messages);

                            smsHelper = GetHelper();

                            if (smsHelper == null)
                            {
                                _messages.Clear();

                                continue;
                            }
                        }

                        if (!_messages.TryPeek(out message))
                        {
                            continue;
                        }
                    }

                    if (message.LifeTime <= DateTime.UtcNow)
                    {
                        lock (_messages)
                        {
                            _messages.Dequeue();
                        }

                        continue;
                    }

                    var msg = new Sms.Models.SmsMessage
                    {
                        Sender = message.Sender,
                        PhoneNumber = message.PhoneNumber,
                        Message = message.Message,
                        ScheduleTime = message.ScheduleTime
                    };

                    var sendResultTask = smsHelper.SendAsync(msg);

                    sendResultTask.Wait();

                    var sendResult = sendResultTask.Result;

                    message.Status = sendResult.Status;
                    message.Service = smsHelper.Name;
                    message.ErrorMessage = sendResult.ErrorMessage;
                    message.ServiceId = sendResult.SmsId;

                    UpdateToDb(message);

                    _messages.Dequeue();
                }
                catch (Exception ex)
                {
                    if (message != null)
                    {
                        lock (_messages)
                        {
                            _messages.Dequeue();
                        }
                    }

                    _log.Error($"Ошибка потока отправки сообщения", ex);

                    Thread.Sleep(200);
                }
            }
        }

        private async Task AddToDb(SmsMessage smsMessage)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                context.SmsMessages.Add(smsMessage);

                await context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                smsMessage.Status = SmsMessageStatus.Error;

                smsMessage.ErrorMessage = $"Ошибка сохранения СМС в БД.{Environment.NewLine}{ex}";
            }
        }

        private void UpdateToDb(SmsMessage smsMessage)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                var dbMessage = context.SmsMessages
                    .SingleOrDefault(el => el.Id == smsMessage.Id);

                if (dbMessage == null)
                {
                    context.SmsMessages.Add(smsMessage);
                }
                else
                {
                    dbMessage.Service = smsMessage.Service;
                    dbMessage.Status = smsMessage.Status;
                    dbMessage.ServiceId = smsMessage.ServiceId;
                    dbMessage.ErrorMessage = smsMessage.ErrorMessage;
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                _log.Error($"Ошибка обновления СМС в БД (Id={smsMessage.Id})", ex);
            }
        }

        private void GetNotSentFromDb()
        {
            try
            {
                long[] hasMessages;

                lock (_messages)
                {
                    hasMessages = _messages
                        .Select(el => el.Id)
                        .ToArray();
                }

                using var scope = _scopeFactory.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                var toSend = context.SmsMessages
                    .Where(el => el.Status == SmsMessageStatus.Error &&
                                 (el.LifeTime == null || el.LifeTime > DateTime.UtcNow) &&
                                 !hasMessages.Contains(el.Id))
                    .ToArray();

                lock (_messages)
                {
                    foreach (var message in toSend)
                    {
                        _messages.Enqueue(message);
                    }

                    Monitor.Pulse(_messages);
                }
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка получения не отправленных СМС из БД", ex);
            }
        }

        private ISmsHelper GetHelper()
        {
            var helperName = _settingsManager.SettingValue(SettingsKeys.SystemSmsProvider);

            if (string.IsNullOrWhiteSpace(helperName))
            {
                return null;
            }

            var login = _settingsManager.SettingValue(SettingsKeys.SystemSmsLogin);
            var password = _settingsManager.SettingValue(SettingsKeys.SystemSmsPassword);
            var sender = _settingsManager.SettingValue(SettingsKeys.SystemSmsSender);

            switch (helperName)
            {
                case "Тестовый":
                    return new TestSmsHelper(login, password, sender);
                case "ЭКСПЕКТО":
                    return new ExpectoHelper(login, password, sender);
                default:
                    return null;
            }
        }

        private bool CheckNumber(string number)
        {
            if (string.IsNullOrWhiteSpace(number))
            {
                return false;
            }

            const string Pattern = "^(\\+\\d|8)\\d{10}$";

            var regex = new Regex(Pattern);

            return regex.IsMatch(number);
        }

        private void RunStatusTimer()
        {
            lock (_stautsTimerSyncRoot)
            {
                if (_stautsTimer != null)
                {
                    return;
                }

                _stautsTimer = new Timer(
                    LoadNotDeliveredFromDb,
                    null,
                    new TimeSpan(0, 0, 30),
                    new TimeSpan(0, 0, 30));
            }
        }

        private void LoadNotDeliveredFromDb(object obj)
        {
            try
            {
                var smsHelper = GetHelper();

                if (smsHelper == null)
                {
                    return;
                }

                using var scope = _scopeFactory.CreateScope();

                var context = scope.ServiceProvider.GetRequiredService<DocumentsContext>();

                Dictionary<string, SmsMessage> toCheck;

                lock (_stautsTimerSyncRoot)
                {
                    toCheck = context.SmsMessages
                        .Where(el => (el.Status == SmsMessageStatus.Created
                                      || el.Status == SmsMessageStatus.Accepted
                                      || el.Status == SmsMessageStatus.Queued)
                                     && el.Service == smsHelper.Name)
                        .ToDictionary(el => el.ServiceId);

                    if (!toCheck.Any())
                    {

                        if (_stautsTimer == null)
                        {
                            return;
                        }

                        _stautsTimer.Dispose();

                        _stautsTimer = null;


                        return;
                    }
                }

                var smsIdArray = toCheck.Values
                    .Where(el=>el.Status != SmsMessageStatus.Created)
                    .Select(el=>el.ServiceId)
                    .ToArray();

                var task =  smsHelper.GetStatusAsync(smsIdArray);

                task.Wait();

                var statuses = task.Result;

                foreach (var status in statuses)
                {
                    var message = toCheck[status.SmsId];

                    message.Status = status.Status;
                    message.ErrorMessage = status.ErrorMessage;
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка обновления статусов отправленных СМС", ex);
            }

        }
    }
}
