using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using BIT.MyCity.Reports;
using log4net;


namespace BIT.MyCity.Services
{
    public class ReportService
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ReportService));
        
        private readonly Dictionary<Guid, IReport> _reports = new Dictionary<Guid, IReport>();

        public ReportService()
        {
            UpdatePluginsInfo();
        }

        public IEnumerable<IReport> Reports
        {
            get
            {
                return _reports.Values
                    .OrderBy(el => el.Name);
            }
        }

        public IReport GetReport(Guid id)
        {
            return !_reports.ContainsKey(id)
                ? null
                : _reports[id];
        }
        
        internal void UpdatePluginsInfo()
        {
            var location = Assembly.GetEntryAssembly()?.Location;

            if (string.IsNullOrWhiteSpace(location))
            {
                _log.Warn("Не удалось получить базовуцю диекторию приложения.");

                return;
            }

            var appDirectory = Path.GetDirectoryName(location);

            if (string.IsNullOrWhiteSpace(appDirectory))
            {
                _log.Warn($"Не удалось получить базовуцю диекторию приложения (location = {location}).");

                return;
            }

            var path = Path.Combine(appDirectory, "ReportPlugins");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            
            var files = Directory.GetFiles(path, "*.dll");

            foreach (var file in files)
            {
                try
                {
                    var assembly = Assembly.LoadFrom(file);

                    var types = assembly.GetTypes()
                        .Where(el => typeof(IReport).IsAssignableFrom(el))
                        .ToArray();

                    if (!types.Any())
                    {
                        continue;
                    }

                    foreach (var type in types)
                    {
                        var obj = (IReport)Activator.CreateInstance(type);

                        if (obj != null)
                        {
                            _reports.Add(obj.Id, obj);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _log.Error("Ошибка плучения плагинов отчетов", ex);
                }
            }
        }
    }
}
