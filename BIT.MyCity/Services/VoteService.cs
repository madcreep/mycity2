using BIT.MyCity.Database;
using log4net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading;

namespace BIT.MyCity.Services
{
    public class VoteService
    {
        private readonly TimeSpan _nextPollDefault = new TimeSpan(0, 10, 0);
        private readonly ILog _log = LogManager.GetLogger(typeof(VoteService));
        private readonly IServiceScopeFactory _scopeFactory;

        private readonly Timer _timer1;
        public VoteService(IServiceScopeFactory scopeFactory, IConfiguration configuration)
        {
            _scopeFactory = scopeFactory;
            _timer1 = new Timer(
                Poll,
                null,
                new TimeSpan(0, 5, 0),
                _nextPollDefault
                );
            _log.Info($"{nameof(VoteService)} started");
        }

        private void Poll(object obj)
        {
            _log.Info($"{nameof(VoteService)} Poll");
            using var scope = _scopeFactory.CreateScope();
            var _ctx = scope.ServiceProvider.GetRequiredService<DocumentsContext>();
            var voteRoots = _ctx.VoteRoots
                .Where(el => !el.Deleted)
                .Where(el => el.State != VoteRoot.StatusEnum.IS_ARCHIVED && el.State != VoteRoot.StatusEnum.DRAFT)
                .ToList();
            var dirty = false;
            var t0 = DateTime.UtcNow;
            var nextPoll = _nextPollDefault;
            foreach (var vr in voteRoots)
            {
                if (vr.State == VoteRoot.StatusEnum.IS_PLANNED && vr.StartAt.HasValue)
                {
                    if (vr.StartAt.Value <= t0)
                    {
                        vr.State = VoteRoot.StatusEnum.IN_PROGRESS;
                        vr.StartedAt = t0;
                        _log.Info($"Vote {vr.Id} Started");
                        dirty = true;
                    }
                    else if (vr.StartAt.Value - t0 < nextPoll)
                    {
                        nextPoll = vr.StartAt.Value - t0;
                    }
                }
                if (vr.State == VoteRoot.StatusEnum.IN_PROGRESS && vr.EndAt.HasValue)
                {
                    if (vr.EndAt.Value <= t0)
                    {
                        vr.State = VoteRoot.StatusEnum.IS_DONE;
                        vr.EndedAt = t0;
                        _log.Info($"Vote {vr.Id} Ended");
                        dirty = true;
                    }
                    else if (vr.EndAt.Value - t0 < nextPoll)
                    {
                        nextPoll = vr.EndAt.Value - t0;
                    }
                }
                if (vr.State == VoteRoot.StatusEnum.IS_DONE && vr.ArchiveAt.HasValue)
                {
                    if (vr.ArchiveAt.Value <= t0)
                    {
                        vr.State = VoteRoot.StatusEnum.IS_ARCHIVED;
                        vr.ArchivedAt = t0;
                        _log.Info($"Vote {vr.Id} Archived");
                        dirty = true;
                    }
                    else if (vr.ArchiveAt.Value - t0 < nextPoll)
                    {
                        nextPoll = vr.ArchiveAt.Value - t0;
                    }
                }
            }
            if (dirty)
            {
                _ctx.SaveChanges();
            }
            var minPoll = new TimeSpan(0, 0, 10);
            if (nextPoll < minPoll)
            {
                nextPoll = minPoll;
            }
            _timer1.Change(nextPoll, _nextPollDefault);
        }
    }
}
