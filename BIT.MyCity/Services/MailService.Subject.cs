using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using BIT.MyCity.Database;
using BIT.MyCity.Extensions;
using BIT.MyCity.Managers;
using BIT.MyCity.Model;
using Microsoft.CSharp.RuntimeBinder;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.Services
{
    public partial class MailService
    {
        private void GenerateSubject(Email mailMessage, object model)
        {
            if (!string.IsNullOrWhiteSpace(mailMessage.MailMessage.Subject))
            {
                return;
            }

            var settings = GetSettings(mailMessage.MailMessage.TemplateKey);

            var subjectTemplate = settings.SubjectTemplate;

            var templateKeys = GetSubjectTemplateKeys(subjectTemplate);

            var keys = templateKeys as string[] ?? templateKeys.ToArray();

            if (!keys.Any())
            {
                mailMessage.MailMessage.Subject = subjectTemplate;

                return;
            }

            var values = GetKeysValues(keys, model, settings);

            foreach (var (key, value) in values)
            {
                subjectTemplate = subjectTemplate.Replace(key, value);
            }

            mailMessage.MailMessage.Subject = subjectTemplate;
        }

        private EmailTemplateSettings GetSettings(string templateName)
        {
            var scopeFactory =
                _serviceProvider.GetRequiredService<IServiceScopeFactory>();
            
            using var scope = scopeFactory.CreateScope();

            var enumVerbManager = scope.ServiceProvider.GetRequiredService<EnumVerbManager>();

            var getEnumVerbTask = enumVerbManager.GetEnumVerbAsync(templateName, null, "email");

            getEnumVerbTask.Wait();

            var enumVerb = getEnumVerbTask.Result;

            if (enumVerb == null)
            {
                return null;
            }

            return enumVerb.CustomSettings.TryDeserializeJson<EmailTemplateSettings>(out var result)
                ? result
                : null;
        }

        private static IEnumerable<string> GetSubjectTemplateKeys(string template)
        {
            const string pattern = @"\{.+?\}";

            var regex = new Regex(pattern);

            var matches = regex.Matches(template);

            return matches
                .Select(el => el.Value)
                .Distinct()
                .ToArray();
        }

        private Dictionary<string, string> GetKeysValues(IEnumerable<string> keys, object model, EmailTemplateSettings settings)
        {
            var result = new Dictionary<string, string>();

            foreach (var key in keys)
            {
                var obj = GetSettingsValue(key, model)
                          ?? GetKeyValue(
                              key
                                  .Trim('{', '}', ' ')
                                  .Split('.'),
                              model);

                var value = obj switch
                {
                    null => string.Empty,
                    DateTime date => date.ToLocalTime().ToString(settings.DateTimeFormat),
                    _ => obj.ToString()
                };

                result.Add(key, value);
            }

            return result;
        }

        private static object GetKeyValue(string[] keys, object model)
        {
            if (!keys.Any() || model == null)
            {
                return null;
            }

            var result = model;

            foreach (var key in keys)
            {
                var type = result!.GetType();

                if (result is IDynamicMetaObjectProvider provider)
                {

                    try
                    {
                        var param = Expression.Parameter(typeof(object));

                        var metaObject = provider.GetMetaObject(param);

                        var binder = (GetMemberBinder)Binder
                            .GetMember(0, key, type, new[] { CSharpArgumentInfo.Create(0, null) });

                        var ret = metaObject.BindGetMember(binder);

                        var final = Expression.Block(
                            Expression.Label(CallSiteBinder.UpdateLabel),
                            ret.Expression
                        );

                        var lambda = Expression.Lambda(final, param);

                        var del = lambda.Compile();

                        result = del.DynamicInvoke(result);
                    }
                    catch
                    {
                        return null;
                    }
                }
                else
                {
                    var propertyInfo = type.GetProperty(key);

                    if (propertyInfo == null)
                    {
                        return null;
                    }

                    result = propertyInfo.GetValue(result);

                    if (result == null)
                    {
                        return null;
                    }
                }
            }

            return result;
        }

        private string GetSettingsValue(string key, object model)
        {
            string settingsKey;

            switch (key)
            {
                case "{system.name}":
                    settingsKey = SettingsKeys.SystemWebGeneralMainTitle;
                    break;
                case "{subsystem.name}":
                    var subsystemUid = GetKeyValue(new[] {"Entity", "Subsystem", "UID"}, model) as Subsystem;

                    settingsKey = $"subsystem.{subsystemUid}.name.";

                    break;
                default:
                    settingsKey = null;
                    break;
            }

            if (settingsKey == null)
            {
                return null;
            }

            var scopeFactory =
                _serviceProvider.GetRequiredService<IServiceScopeFactory>();

            using var scope = scopeFactory.CreateScope();

            var settingsManager = scope.ServiceProvider.GetRequiredService<SettingsManager>();

            return settingsManager.SettingValue(settingsKey);
        }
    }
}
