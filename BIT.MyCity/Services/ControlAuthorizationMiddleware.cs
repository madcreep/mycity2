using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace BIT.MyCity.Services
{
    public class ControlAuthorizationMiddleware
    {
        private readonly RequestDelegate _next;

        public ControlAuthorizationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Headers.TryGetValue("Authorization", out var token) && !string.IsNullOrWhiteSpace(token)
                && !context.User.Identity.IsAuthenticated)
            {
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;

                var result = JsonConvert.SerializeObject(new { error = "Ошибка авторизации. Токен не принят системой." });

                context.Response.ContentType = "application/json";

                await context.Response.WriteAsync(result);

                return;
            }
            
            // Call the next delegate/middleware in the pipeline.
            await _next(context);
        }
    }

    public static class ControlAuthorizationMiddlewareExtensions
    {
        public static IApplicationBuilder UseControlAuthorization(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ControlAuthorizationMiddleware>();
        }
    }
}
