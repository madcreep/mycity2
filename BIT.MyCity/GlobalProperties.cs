﻿namespace BIT.MyCity
{
    public static class GlobalProperties
    {
        public static string ConnectionString { get; set; }

        public static string AppUrl { get; set; }
        
        public static string StoragePath { get; set; }
    }
}
