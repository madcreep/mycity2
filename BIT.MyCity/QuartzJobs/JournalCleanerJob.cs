using System;
using System.Linq;
using System.Threading.Tasks;
using BIT.MyCity.Database;
using BIT.MyCity.Managers;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Z.EntityFramework.Plus;

namespace BIT.MyCity.QuartzJobs
{
    public class JournalCleanerJob : IJob
    {
        private readonly IServiceProvider _provider;
        private readonly ILog _log = LogManager.GetLogger(typeof(JournalCleanerJob));

        public JournalCleanerJob(IServiceProvider provider)
        {
            _provider = provider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var sm = _provider.GetRequiredService<SettingsManager>();

                var period = -(int)sm.SettingValue<uint>(SettingsKeys.SystemOptionsJournalSavePeriod);

                var checkDate = DateTime.UtcNow
                    .AddMonths(period)
                    .Date;

                var dbContext = _provider.GetRequiredService<DocumentsContext>();

                await dbContext.Journal
                    .Where(x => x.Timestamp < checkDate)
                    .DeleteAsync();
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка очистки журнала изменений", ex);
            }   
        }
    }
}
