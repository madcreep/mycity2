using System;
using BIT.MyCity.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.QuartzJobs
{
    public class NotSendEmailPeriodicallyJob : IPeriodicallyJob
    {
        private readonly IServiceProvider _serviceProvider;

        public NotSendEmailPeriodicallyJob(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool Enable
        {
            get
            {
                var sm = _serviceProvider.GetRequiredService<SettingsManager>();

                return sm.SettingValue<bool>(SettingsKeys.SystemSmtpEnable);
            }
        }
        public Type JobType => typeof(NotSendEmailJob);

        public string TriggerCron
        {
            get
            {
                var sm = _serviceProvider.GetRequiredService<SettingsManager>();

                var period =  sm.SettingValue<uint>(SettingsKeys.SystemSmtpReSendPeriod);

                if (period < 1)
                {
                    period = 1;
                }

                if (period > 60)
                {
                    period = 60;
                }

                return $"0 */{period} * * * ?";
            }
        }
    }
}
