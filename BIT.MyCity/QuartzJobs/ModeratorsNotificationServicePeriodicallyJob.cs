using System;
using BIT.MyCity.Managers;
using BIT.MyCity.Subsystems;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.QuartzJobs
{
    public class ModeratorsNotificationServicePeriodicallyJob : IPeriodicallyJob

    {
    private readonly IServiceProvider _serviceProvider;

    public ModeratorsNotificationServicePeriodicallyJob(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public bool Enable {
        get
        {
            var sm = _serviceProvider.GetRequiredService<SettingsManager>();

            return sm.SettingValue<bool>($"subsystem.{SubsystemCitizenAppeals.SettingsKey.Enable}");
        }
    }

    public Type JobType => typeof(ModeratorsNotificationServiceJob);

    public string TriggerCron
    {
        get
        {
            var sm = _serviceProvider.GetRequiredService<SettingsManager>();

            var revisionTimeInStr =
                sm.SettingValue(SettingsKeys.SystemApiNotificationAppealsRevisionTime);

            var parts = string.IsNullOrWhiteSpace(revisionTimeInStr)
                ? new string[0]
                : revisionTimeInStr
                    .Trim()
                    .Split(':');

            if (parts.Length < 1 || !uint.TryParse(parts[0], out var hours) || hours > 23)
            {
                hours = 1;
            }

            if (parts.Length < 2 || !uint.TryParse(parts[1], out var minutes) || minutes > 59)
            {
                minutes = 0;
            }

            return $"0 {minutes} {hours} * * ?";
        }
    }
    }
}
