using System;
using BIT.MyCity.Managers;
using BIT.MyCity.Subsystems;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.QuartzJobs
{
    public class VotesCleanerPeriodicallyJob : IPeriodicallyJob
    {
        private readonly IServiceProvider _serviceProvider;

        public VotesCleanerPeriodicallyJob(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool Enable
        {
            get
            {
                var sm = _serviceProvider.GetRequiredService<SettingsManager>();

                return sm.SettingValue<bool>($"subsystem.{SubsystemVoting.SettingsKey.Enable}");
            }
        }

        public Type JobType => typeof(VotesCleanerJob);
        public string TriggerCron => "0 0 */3 * * ?";
    }
}
