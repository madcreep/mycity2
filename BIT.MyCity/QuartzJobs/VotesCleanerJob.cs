using System;
using System.Threading.Tasks;
using BIT.MyCity.Services;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace BIT.MyCity.QuartzJobs
{
    public class VotesCleanerJob : IJob
    {
        private readonly IServiceProvider _provider;
        private readonly ILog _log = LogManager.GetLogger(typeof(VotesCleanerJob));

        public VotesCleanerJob(IServiceProvider provider)
        {
            _provider = provider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var voteGc = _provider.GetRequiredService<VoteGarbageCollectorService>();

                await voteGc.Execute();
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка сборки мусора голосований.", ex);
            }
        }
    }
}
