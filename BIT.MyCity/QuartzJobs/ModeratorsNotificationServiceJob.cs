using System;
using System.Threading.Tasks;
using BIT.MyCity.Services;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace BIT.MyCity.QuartzJobs
{
    public class ModeratorsNotificationServiceJob : IJob
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(ModeratorsNotificationServiceJob));
        private readonly IServiceProvider _provider;

        public ModeratorsNotificationServiceJob(IServiceProvider provider)
        {
            _provider = provider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var notifyService = _provider.GetRequiredService<ModeratorsNotificationService>();

                await notifyService.SendEmails();
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка отправки повторных уведомлений модераторам", ex);
            }
        }
    }
}
