using System;
using BIT.MyCity.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.QuartzJobs
{
    public class JournalCleanerPeriodicallyJob : IPeriodicallyJob
    {
        private readonly IServiceProvider _serviceProvider;

        public JournalCleanerPeriodicallyJob(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool Enable => true;
        
        public Type JobType => typeof(JournalCleanerJob);

        public string TriggerCron
        {
            get
            {
                var sm = _serviceProvider.GetRequiredService<SettingsManager>();
                var hour = sm.SettingValue<uint>(SettingsKeys.SystemOptionsJournalClearHour);

                if (hour > 23)
                {
                    hour = 23;
                }
                
                return $"0 0 {hour} * * ?";
            }
        }
    }
}
