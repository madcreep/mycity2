using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl.Matchers;

namespace BIT.MyCity.QuartzJobs
{
    public class JobManager : IJob
    {
        private const string GroupName = "ManagedJobs";
        private readonly ILog _log = LogManager.GetLogger(typeof(JobManager));
        private readonly IServiceProvider _provider;

        public JobManager(IServiceProvider provider)
        {
            _provider = provider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var factory = _provider.GetRequiredService<ISchedulerFactory>();

                var scheduler = await factory.GetScheduler("Quartz ASP.NET Core MyCity Scheduler");

                if (scheduler == null)
                {
                    _log.Warn("Менеджер периодических задач. Не найден планировщик.");

                    return;
                }

                var allJobs = _provider.GetServices<IPeriodicallyJob>()
                    .Where(el=> typeof(IJob).IsAssignableFrom(el.JobType));

                var needJobs = allJobs
                    .Where(el => el.Enable && el.JobType?.FullName != null)
                    .ToDictionary(el => new JobKey(el.JobType.FullName, GroupName));

                var hasJobKeys = await scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(GroupName));

                var forDeleteKeys = hasJobKeys
                    .Except(needJobs.Keys)
                    .ToArray();

                _ = await scheduler.DeleteJobs(forDeleteKeys);

                foreach (var (key, job) in needJobs)
                {
                    try
                    {
                        await UpdateJobAndTrigger(hasJobKeys, key, scheduler, job);
                    }
                    catch (Exception ex)
                    {
                        _log.Error("Менеджер периодических задач.", ex);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка в работе менеджера периодических задач.", ex);
            }
        }

        private static async Task UpdateJobAndTrigger(IEnumerable<JobKey> hasJobKeys, JobKey key, IScheduler scheduler,
            IPeriodicallyJob job)
        {
            if (scheduler == null)
            {
                return;
            }

            if (hasJobKeys.Contains(key))
            {
                var trigger = (await scheduler.GetTriggersOfJob(key))
                    .SingleOrDefault();

                if (trigger == null)
                {
                    var triggerKey = new TriggerKey($"{job.JobType}_Trigger", GroupName);

                    var newTrigger = TriggerBuilder.Create()
                        .WithIdentity(triggerKey)
                        .WithCronSchedule(job.TriggerCron)
                        .ForJob(key)
                        .Build();

                    await scheduler.RescheduleJob(triggerKey, newTrigger);
                }
                else
                {
                    var needCronStr = job.TriggerCron;

                    if (trigger is ICronTrigger cronTrigger)
                    {
                        var cronStr = cronTrigger.CronExpressionString;
                        
                        if (cronStr == needCronStr)
                        {
                            return;
                        }
                    }

                    var tb = trigger.GetTriggerBuilder();

                    var newTrigger = tb
                        .WithIdentity(trigger.Key)
                        .WithCronSchedule(needCronStr)
                        .ForJob(trigger.JobKey)
                        .Build();

                    await scheduler.RescheduleJob(newTrigger.Key, newTrigger);
                }
            }
            else
            {
                var newJob = JobBuilder.Create(job.JobType)
                    .WithIdentity(key)
                    .Build();

                var triggerKey = new TriggerKey($"{job.JobType}_Trigger", GroupName);

                var newTrigger = TriggerBuilder.Create()
                    .WithIdentity(triggerKey)
                    .WithCronSchedule(job.TriggerCron)
                    .ForJob(key)
                    .Build();

                await scheduler.ScheduleJob(newJob, newTrigger);
            }
        }
    }
}
