using System;
using System.Threading.Tasks;
using BIT.MyCity.Services;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace BIT.MyCity.QuartzJobs
{
    public class NotSendEmailJob : IJob
    {
        private readonly IServiceProvider _provider;
        private readonly ILog _log = LogManager.GetLogger(typeof(NotSendEmailJob));

        public NotSendEmailJob(IServiceProvider provider)
        {
            _provider = provider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var mailService = _provider.GetRequiredService<MailService>();

                await mailService.LoadNotSentFromDb();
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка выполнения задания чтения не отправленных Email.", ex);
            }
        }
    }
}
