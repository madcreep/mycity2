using System;
using BIT.MyCity.Managers;
using Microsoft.Extensions.DependencyInjection;

namespace BIT.MyCity.QuartzJobs
{
    public class AttachedFilesCleanerPeriodicallyJob : IPeriodicallyJob
    {
        private readonly IServiceProvider _serviceProvider;

        public AttachedFilesCleanerPeriodicallyJob(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool Enable => true;
        public Type JobType => typeof(AttachedFilesCleanerJob);
        public string TriggerCron
        {
            get
            {
                var sm = _serviceProvider.GetRequiredService<SettingsManager>();
                var period = sm.SettingValue<uint>(SettingsKeys.SystemOptionsFileCleanerClearPeriod);

                if (period < 1)
                {
                    period = 1;
                }

                if (period > 60)
                {
                    period = 60;
                }

                return $"0 */{period} * * * ?";
            }
        }
    }
}
