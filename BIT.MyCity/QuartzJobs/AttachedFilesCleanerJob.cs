using System;
using System.Threading.Tasks;
using BIT.MyCity.Services;
using log4net;
using Microsoft.Extensions.DependencyInjection;
using Quartz;

namespace BIT.MyCity.QuartzJobs
{
    public class AttachedFilesCleanerJob : IJob
    {
        private readonly IServiceProvider _provider;
        private readonly ILog _log = LogManager.GetLogger(typeof(AttachedFilesCleanerJob));

        public AttachedFilesCleanerJob(IServiceProvider provider)
        {
            _provider = provider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                var attachService = _provider.GetRequiredService<AttachService>();

                await attachService.ClearNotUsedFiles();
            }
            catch (Exception ex)
            {
                _log.Error("Ошибка выполнения задания очистки неисползуемых файлов.", ex);
            }
        }
    }
}
