using System;

namespace BIT.MyCity.QuartzJobs
{
    public interface IPeriodicallyJob
    {
        public bool Enable { get; }
        
        public Type JobType { get; }

        public string TriggerCron { get; }
    }
}
