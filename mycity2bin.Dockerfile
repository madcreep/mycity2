FROM registry.biz-it.ru/aspbuildenv:60_12 AS build-env
WORKDIR /src

COPY . ./
RUN dotnet restore ./BIT.MyCity
RUN dotnet publish ./BIT.MyCity -o /publish

# FROM mcr.microsoft.com/dotnet/aspnet:6.0 as runtime
# FROM registry.biz-it.ru/aspnet:6.0 as runtime
# FROM registry.biz-it.ru/dotnet_sdk:6.0 as runtime

# just hold binaries in small image, not runtime image
FROM registry.biz-it.ru/alpine:3.7 as holder

WORKDIR /publish

COPY --from=build-env /publish .

ENTRYPOINT ["dotnet", "BIT.MyCity.dll"]
