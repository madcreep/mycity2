using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BIT.MyCity.Shared
{
    public class MailTask
    {
        [Key]
        public long Id { get; set; }

        public string ExtendedData;

        public string Title { get; set; }
        public string TemplateName { get; set; }
        public string Text { get; set; }

        [NotMapped]
        public JObject Data
        {
            get
            {
                return JsonConvert.DeserializeObject<JObject>(string.IsNullOrEmpty(ExtendedData) ? "{}" : ExtendedData);
            }
            set
            {
                ExtendedData = value.ToString();
            }
        }

        public string Addressee { get; set; }
    }
}

