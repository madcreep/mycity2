using System;
using System.Threading.Tasks;
using BIT.MyCity.Site.Managers.Api;
using BIT.MyCity.ApiInterface.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BIT.MyCity.Site.Controllers
{
    public class AuthorizeController : Controller
    {
        private readonly ApiAuthManager _authManager;
        private readonly GraphQlApiManager _gqlApiManager;

        public AuthorizeController([FromServices] ApiAuthManager authManager, GraphQlApiManager gqlApiManager)
        {
            _authManager = authManager;
            _gqlApiManager = gqlApiManager;
        }

        public async Task<IActionResult> Login()
        {
            try
            {
                ViewData["LoginSystems"] = await _gqlApiManager.GetAvailableAuthSystemsAsync();

                return View();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost, Route("LoginForm")]
        public async Task<IActionResult> LoginForm([FromForm] LoginModel model)
        {
            try
            {
                model.LoginSystem = LoginSystem.Form;

                var authResult = await _authManager.LoginAsync(model);

                return RedirectToAction("LoginCompleted", new { authResult.Token, authResult.ErrorMessage, authResult.LoginStatus });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [Route("LoginApi/{system}")]
        public async Task<IActionResult> LoginApi(LoginSystem system)
        {
            var callbackUrl = Url.ActionLink("LoginApiCallback", "Authorize", new { system }, HttpContext.Request.Scheme);
            
            var redirectUrl = await _gqlApiManager.GetRedirectUrl(system, callbackUrl);

            return Redirect(redirectUrl);
        }

        [Route("LoginApiCallback/{system}")]
        public async Task<IActionResult> LoginApiCallback(LoginSystem system)
        {
            //HttpContext.Request.Query.Keys

            string code;
            string error = null;

            switch (system)
            {
                case LoginSystem.Form:
                    return RedirectToAction("Login");
                case LoginSystem.Esia:
                    var errorDescription = HttpContext.Request.Query["error_description"].ToString();
                    var state = HttpContext.Request.Query["state"].ToString();
                    error = HttpContext.Request.Query["error"].ToString();
                    code = HttpContext.Request.Query["code"].ToString();
                    break;
                case LoginSystem.Vk:
                    code = HttpContext.Request.Query["code"];
                    break;
                case LoginSystem.Facebook:
                    code = HttpContext.Request.Query["code"];
                    break;
                case LoginSystem.Google:
                    return RedirectToAction("Login");
                default:
                    throw new ArgumentOutOfRangeException(nameof(system), system, null);
            }

            var callbackUrl = Url.ActionLink("LoginApiCallback", "Authorize", new { system }, HttpContext.Request.Scheme);

            var loginModel = new LoginModel
            {
                LoginSystem = system,
                LoginRequestSource = LoginRequestSource.Mobile,
                Code = code,
                CallbackUrl = callbackUrl,
                RequestCodeError = error
            };

            try
            {
                var authResult = await _gqlApiManager.LoginAsync(loginModel);

                if (authResult == null)
                {
                    return RedirectToAction("Login");
                }

                return RedirectToAction("LoginCompleted", new { authResult.Token, authResult.ErrorMessage, authResult.LoginStatus });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login");
            }
        }

        public async Task<IActionResult> LoginCompleted(LoginResult loginResult)
        {
            var userInfo = await _gqlApiManager.GetCurrentUser();

            return View(userInfo);
        }

        //[HttpPost]
        //public async Task<IActionResult> Register(
        //  [FromServices] IHttpContextAccessor httpContextAccessor,
        //  [FromServices] IUrlHelper url,
        //  [FromForm] RegisterUserModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }

        //    //model.Roles = new[] {"admin", "moderator"};

        //    var scheme = httpContextAccessor.HttpContext.Request.Scheme;

        //    var urlStr = url.Action("ConfirmEmail", "Authorize", (object)null, scheme);

        //    model.ConfirmEmailUrl = urlStr;

        //    var registerResult = await _authManager.Register(model);

        //    if (registerResult)
        //    {
        //        return RedirectToAction("Index", "Home");
        //    }

        //    return View(model);
        //}

        [HttpGet]
        public async Task<IActionResult> ConfirmEmail([FromQuery] ConfirmEmailModel model)
        {
            var confirmResult = await _authManager.ConfirmEmail(model);

            return View(confirmResult);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            var callbackUrl = Url.ActionLink("Index", "Home", HttpContext.Request.Scheme);

            var redirectUrl = await _gqlApiManager.GetLogoutUrl(LoginSystem.Esia, callbackUrl);

            return Redirect(redirectUrl);
        }

        [HttpGet]
        public async Task<IActionResult> ResetPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword([FromForm] ResetPasswordModel model)
        {
            var result = await _authManager.ResetPassword(model);

            if (result.Success)
            {
                return RedirectToAction("Index", "Home");
            }

            return View(model);
        }
    }
}
