using System;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using GraphQL;
using GraphQL.Client.Http;
using Microsoft.AspNetCore.Http;

namespace BIT.MyCity.Site.Managers.Api
{
    public class GraphQlApiManager
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        private GraphQLHttpClient graphQLClient = new GraphQLHttpClient(
            // "http://localhost:44327/graphql",
            // "https://mycity.biz-it.ru//graphql",
            // "http://localhost:5000/graphql"
            new GraphQLHttpClientOptions
            {
                EndPoint = new Uri("http://localhost:5001/graphql"),
                WebSocketEndPoint = new Uri("ws://localhost:5001/subscription"),
                UseWebSocketForQueriesAndMutations = false
            },
            new NewtonsoftJsonSerializer(options =>
            {
                //options.Converters.Add(new StringEnumConverter());
                //options.NullValueHandling = NullValueHandling.Ignore;
                //var contractResolver = new DefaultContractResolver
                //{
                //    NamingStrategy = new CamelCaseNamingStrategy()
                //};
                //options.ContractResolver = contractResolver;
                //options.Formatting = Formatting.Indented;
            }
            )
            );

        public GraphQlApiManager(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;

            graphQLClient.HttpClient.Timeout = new TimeSpan(0, 0, 2, 0);
        }

        public async Task<LoginSystem[]> GetAvailableAuthSystemsAsync()
        {


            var request = new GraphQLRequest
            {
                Query = @"query{loginModes}"
            };

            var response = await graphQLClient.SendQueryAsync<LoginModesResponse>(request);

            //var result = response.Data
            //    .Select(el => Enum.Parse<LoginSystem>(el));

            return response.Data.LoginModes;
        }

        public async Task<string> GetRedirectUrl(LoginSystem system, string callbackUrl)
        {
            var request = new GraphQLRequest
            {
                Query = "query {loginRedirectUrl(redirectUrl:{loginSystem:\"" +
                        system.ToString().ToUpper() +
                        "\", url: \"" +
                        callbackUrl +
                        "\"}){loginSystem url}}"
            };
            var response = await graphQLClient.SendQueryAsync<LoginRedirectUrlResponse>(request);

            return response.Data.LoginRedirectUrl.Url;
        }

        public async Task<string> GetLogoutUrl(LoginSystem system, string callbackUrl)
        {
            var request = new GraphQLRequest
            {
                Query = "query {logoutRedirectUrl(redirectUrl:{loginSystem:\"" +
                        system.ToString().ToUpper() +
                        "\", url: \"" +
                        callbackUrl +
                        "\"}){loginSystem url}}"
            };
            var response = await graphQLClient.SendQueryAsync<LogoutRedirectUrlResponse>(request);

            return response.Data.LogoutRedirectUrl.Url;
        }

        public async Task<LoginResult> LoginAsync(LoginModel loginModel)
        {
            try
            {
                var result1 = await SubscribeNew(loginModel);

                var tokenNew = result1?.Token?.Split(' ')[1];

                _httpContextAccessor.HttpContext.Session.SetString("JWToken", tokenNew);

                return result1;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);

                return null;
            }
        }

    //    private Task<LoginResult> Subscribe()
    //    {
    //        var waitMutex = new AutoResetEvent(false);

    //        var task = Task.Run(() =>
    //        {
    //            try
    //            {
    //                var userJoinedRequest = new GraphQLRequest
    //                {
    //                    Query = @"
    //subscription {
    //    loginResult (key:""123""){key token loginStatus errorMessage}
    //}"
    //                };

    //                var subscriptionStream
    //                    = graphQLClient.CreateSubscriptionStream<LoginResponseSub>(userJoinedRequest);

    //                var mutex = new AutoResetEvent(false);

    //                LoginResult result = null;

    //                var subscription = subscriptionStream.Subscribe(
    //                    response =>
    //                    {
    //                        //Console.WriteLine($"user '{response.Data?.Login?.Key}' joined");

    //                        result = response.Data?.Login;

    //                        mutex.Set();
    //                    },
    //                    () => { Console.WriteLine("Complated"); });

    //                waitMutex.Set();

    //                mutex.WaitOne(TimeSpan.FromSeconds(30));

    //                subscription.Dispose();

    //                return result;
    //            }
    //            catch (Exception ex)
    //            {
    //                waitMutex.Set();

    //                return null;
    //            }
    //        });

    //        waitMutex.WaitOne();

    //        return task;
    //    }

        private Task<LoginResult> SubscribeNew(LoginModel loginModel)
        {
            var waitMutex = new AutoResetEvent(false);

            var task = Task.Run(() =>
            {
                try
                {
                    var userJoinedRequest = new GraphQLRequest
                    {
                        Query = "subscription {" +
        "login (login:{subscribeKey:\"123\", code:\"" +
                        loginModel.Code +
                        "\",callbackUrl: \"" +
                        loginModel.CallbackUrl +
                        "\",loginSystem: " +
                        loginModel.LoginSystem.ToString().ToUpper() +
                        ",loginRequestSource: " +
                        loginModel.LoginRequestSource.ToString().ToUpper() +
                        ",requestCodeError: \"" +
                        loginModel.RequestCodeError +
                        "\"})" +
        "{ key token loginStatus errorMessage}}"
                    };

                    var subscriptionStream
                        = graphQLClient.CreateSubscriptionStream<LoginResponce>(userJoinedRequest);

                    var mutex = new AutoResetEvent(false);

                    LoginResult result = null;

                    var subscription = subscriptionStream.Subscribe(
                        response =>
                        {
                            Console.WriteLine($"user '{response.Data?.Login?.Key}' joined");
                            
                            result = response.Data?.Login;

                            if (result?.Key == "123")
                            {
                                mutex.Set();
                            }
                        },
                        () => { Console.WriteLine("Complated"); });

                    waitMutex.Set();

                    mutex.WaitOne(TimeSpan.FromSeconds(30));

                    subscription.Dispose();

                    return result;
                }
                catch (Exception ex)
                {
                    waitMutex.Set();

                    return null;
                }
            });

            waitMutex.WaitOne();

            return task;
        }

        public async Task<UserInfo> GetCurrentUser()
        {
            var request = new GraphQLRequest
            {
                Query = "query {currentUser {fullName}}"
            };


            var token = _httpContextAccessor.HttpContext.Session.GetString("JWToken");

            //request.Add("Authorization", token);
            graphQLClient.HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

            var response = await graphQLClient.SendQueryAsync<CurrentUserResponse>(request);

            return new UserInfo
            {
                LoginSystem = LoginSystem.Vk,
                UserName = response.Data.CurrentUser.FullName
            };
        }
    }



    public class LoginModesResponse
    {
        public LoginSystem[] LoginModes { get; set; }
    }

    public class LoginRedirectUrlResponse
    {
        public RedirectUrl LoginRedirectUrl { get; set; }
    }

    public class LogoutRedirectUrlResponse
    {
        public RedirectUrl LogoutRedirectUrl { get; set; }
    }

    public class CurrentUserResponse
    {

        public UserContent CurrentUser { get; set; }

        public class UserContent
        {
            public LoginSystem LoginSystem { get; set; }
            public string FullName { get; set; }
        }
    }

    public class LoginResponce
    {
        public LoginResult Login { get; set; }
    }
}
