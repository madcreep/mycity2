using System;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using GraphQL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace BIT.MyCity.Site.Managers.Api
{
    public class ApiManager
    {
        internal readonly IHttpContextAccessor HttpContextAccessor;
        private readonly string _baseApiUrl;

        public ApiManager([FromServices] IConfiguration configuration, [FromServices] IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
            _baseApiUrl = configuration["Api:BaseUrl"]
              .Trim()
              .Trim('/');
        }

        public async Task<T> GetAnswerAsync<T>(string cmd, string sendMethod = "GET", object bodyObject = null, string token = null)
        {
            var request = CreateRequest(cmd, sendMethod, bodyObject);

            var json = await GetResponseStringAsync(request);

            return DeserializeAnswer<T>(json);
        }

        private WebRequest CreateRequest(string cmd, string sendMethod, object bodyObject)
        {
            var myUri = GenerateUri(cmd);

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(myUri);

            httpWebRequest.Method = sendMethod.ToUpper();

            httpWebRequest.ContentType = MediaTypeNames.Application.Json;

            var token = HttpContextAccessor.HttpContext.Session.GetString("JWToken");

            if (!string.IsNullOrWhiteSpace(token))
            {
                httpWebRequest.Headers.Add("Authorization", $"Bearer {token}");
            }

            if (httpWebRequest.Method == "GET" || bodyObject == null)
            {
                return httpWebRequest;
            }

            var bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(bodyObject));

            httpWebRequest.ContentLength = bytes.Length;

            using (var os = httpWebRequest.GetRequestStream())
            {
                os.Write(bytes, 0, bytes.Length);

                os.Close();
            }

            //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //{
            //  var json = JsonConvert.SerializeObject(bodyObject);

            //  streamWriter.Write(json);
            //}

            return httpWebRequest;
        }

        private static async Task<string> GetResponseStringAsync(WebRequest request)
        {
            string result;

            try
            {
                var response = await request.GetResponseAsync();

                result = ReadResult(response);
            }
            catch (WebException ex)
            {
                result = ReadResult(ex.Response);
            }
            catch (Exception ex)
            {
                result = null;
            }

            return result;
        }

        private static string ReadResult(WebResponse response)
        {
            string result;
            using (var stream = response.GetResponseStream())
            {
                if (stream == null)
                {
                    result = null;
                }
                else
                {
                    var reader = new StreamReader(stream);

                    result = reader.ReadToEnd();
                }
            }

            return result;
        }

        private static T DeserializeAnswer<T>(string json)
        {
            T result;

            try
            {
                result = JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception ex)
            {
                result = default(T);
            }

            return result;
        }

        private Uri GenerateUri(string additionalPath)
        {
            var result = new Uri($"{_baseApiUrl}/api/{additionalPath}");

            return result;
        }
    }
}
