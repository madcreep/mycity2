using System;
using System.Threading.Tasks;
using BIT.MyCity.ApiInterface.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BIT.MyCity.Site.Managers.Api
{
    public class ApiAuthManager
    {
        private readonly ApiManager _apiManager;

        public ApiAuthManager([FromServices] ApiManager apiManager)
        {
            _apiManager = apiManager;
        }

        public async Task<LoginSystem[]> GetAvailableAuthSystemsAsync()
        {
            LoginSystem[] result;

            try
            {
                result = await _apiManager.GetAnswerAsync<LoginSystem[]>("auth/allLoginModes");
            }
            catch (Exception ex)
            {
                // TODO: Добавить логирование
                result = null;
            }

            return result;
        }

        //public async Task<string> GetRedirectUrl(LoginSystem system, string callbackUrl)
        //{
        //    var requestObject = new RedirectUrl
        //    {
        //        LoginSystem = system,
        //        Url = callbackUrl
        //    };

        //    var result = await _apiManager.GetAnswerAsync<RedirectUrl>(
        //      "auth/getRedirectUrl",
        //      "POST",
        //      requestObject);

        //    return result.Url;
        //}

        public async Task<LoginResult> LoginAsync(LoginModel loginModel)
        {
            var result = await _apiManager.GetAnswerAsync<LoginResult>(
              "auth/login",
              "POST",
              loginModel);

            _apiManager.HttpContextAccessor.HttpContext.Session.SetString("JWToken", result.Token);

            return result;
        }

        public async Task<UserInfo> GetCurrentUser()
        {
            var result = await _apiManager.GetAnswerAsync<UserInfo>(
              "user/getUserInfo");

            return result;
        }

        //public async Task<bool> Register(RegisterUserModel model)
        //{
        //    var result = await _apiManager.GetAnswerAsync<bool>(
        //      "auth/register",
        //      "POST",
        //      model);

        //    return result;
        //}

        public async Task<ConfirmEmailResult> ConfirmEmail(ConfirmEmailModel model)
        {
            var result = await _apiManager.GetAnswerAsync<ConfirmEmailResult>(
              "auth/confirmEmail",
              "POST",
              model);

            return result;
        }

        public async Task<bool> Logout()
        {
            var result = await _apiManager.GetAnswerAsync<bool>(
              "auth/logoff");

            if (result)
            {
                _apiManager.HttpContextAccessor.HttpContext.Session.Remove("JWToken");
            }

            return result;
        }

        public async Task<OperationResult<object>> ResetPassword(ResetPasswordModel model)
        {
            var result = await _apiManager.GetAnswerAsync<OperationResult<object>>(
              "auth/resetPassword",
              "POST",
              model);

            return result;
        }
    }
}
