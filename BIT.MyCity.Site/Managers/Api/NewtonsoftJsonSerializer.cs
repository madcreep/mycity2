using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GraphQL;
using GraphQL.Client.Abstractions;
using GraphQL.Client.Abstractions.Websocket;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace BIT.MyCity.Site.Managers.Api
{
    public class NewtonsoftJsonSerializer : IGraphQLWebsocketJsonSerializer
    {
        public static JsonSerializerSettings DefaultJsonSerializerSettings => new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver { IgnoreIsSpecifiedMembers = true },
            MissingMemberHandling = MissingMemberHandling.Ignore
        };

        public JsonSerializerSettings JsonSerializerSettings { get; }

        public NewtonsoftJsonSerializer() : this(DefaultJsonSerializerSettings) { }

        public NewtonsoftJsonSerializer(Action<JsonSerializerSettings> configure) : this(configure.AndReturn(DefaultJsonSerializerSettings)) { }

        public NewtonsoftJsonSerializer(JsonSerializerSettings jsonSerializerSettings)
        {
            JsonSerializerSettings = jsonSerializerSettings;
            ConfigureMandatorySerializerOptions();
        }

        // deserialize extensions to Dictionary<string, object>
        private void ConfigureMandatorySerializerOptions() => JsonSerializerSettings.Converters.Insert(0, new MapConverter());

        public string SerializeToString(GraphQLRequest request) => JsonConvert.SerializeObject(request, JsonSerializerSettings);

        public byte[] SerializeToBytes(GraphQLWebSocketRequest request)
        {
            var json = JsonConvert.SerializeObject(request, JsonSerializerSettings);
            return Encoding.UTF8.GetBytes(json);
        }

        public Task<WebsocketMessageWrapper> DeserializeToWebsocketResponseWrapperAsync(Stream stream) => DeserializeFromUtf8Stream<WebsocketMessageWrapper>(stream);

        public GraphQLWebSocketResponse<GraphQLResponse<TResponse>> DeserializeToWebsocketResponse<TResponse>(byte[] bytes) =>
            JsonConvert.DeserializeObject<GraphQLWebSocketResponse<GraphQLResponse<TResponse>>>(Encoding.UTF8.GetString(bytes),
                JsonSerializerSettings);

        public Task<GraphQLResponse<TResponse>> DeserializeFromUtf8StreamAsync<TResponse>(Stream stream, CancellationToken cancellationToken) => DeserializeFromUtf8Stream<GraphQLResponse<TResponse>>(stream);


        private Task<T> DeserializeFromUtf8Stream<T>(Stream stream)
        {
            using var sr = new StreamReader(stream);
            //var text = sr.ReadToEnd();
            //stream.Position = 0;
            using JsonReader reader = new JsonTextReader(sr);
            var serializer = JsonSerializer.Create(JsonSerializerSettings);
            return Task.FromResult(serializer.Deserialize<T>(reader));
        }
    }

    public class MapConverter : JsonConverter<Map>
    {
        public override void WriteJson(JsonWriter writer, Map value, JsonSerializer serializer) =>
            throw new NotImplementedException(
                "This converter currently is only intended to be used to read a JSON object into a strongly-typed representation.");

        public override Map ReadJson(JsonReader reader, Type objectType, Map existingValue,
            bool hasExistingValue, JsonSerializer serializer)
        {
            var rootToken = JToken.ReadFrom(reader);
            if (rootToken is JObject)
            {
                return ReadDictionary<Map>(rootToken);
            }
            else
                throw new ArgumentException("This converter can only parse when the root element is a JSON Object.");
        }

        private object ReadToken(JToken? token) =>
            token switch
            {
                JObject jObject => ReadDictionary<Dictionary<string, object>>(jObject),
                JArray jArray => ReadArray(jArray),
                JValue jValue => jValue.Value,
                JConstructor _ => throw new ArgumentOutOfRangeException(nameof(token.Type),
                    "cannot deserialize a JSON constructor"),
                JProperty _ => throw new ArgumentOutOfRangeException(nameof(token.Type),
                    "cannot deserialize a JSON property"),
                JContainer _ => throw new ArgumentOutOfRangeException(nameof(token.Type),
                    "cannot deserialize a JSON comment"),
                _ => throw new ArgumentOutOfRangeException(nameof(token.Type))
            };

        private TDictionary ReadDictionary<TDictionary>(JToken element) where TDictionary : Dictionary<string, object>
        {
            var result = Activator.CreateInstance<TDictionary>();
            foreach (var property in ((JObject)element).Properties())
            {
                if (IsUnsupportedJTokenType(property.Value.Type))
                    continue;
                result[property.Name] = ReadToken(property.Value);
            }
            return result;
        }

        private IEnumerable<object> ReadArray(JToken element)
        {
            foreach (var item in element.Values())
            {
                if (IsUnsupportedJTokenType(item.Type))
                    continue;
                yield return ReadToken(item);
            }
        }

        private bool IsUnsupportedJTokenType(JTokenType type) => type == JTokenType.Constructor || type == JTokenType.Property || type == JTokenType.Comment;
    }
}
