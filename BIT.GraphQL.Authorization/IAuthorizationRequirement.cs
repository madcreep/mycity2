﻿using System.Threading.Tasks;

namespace BIT.GraphQL.Authorization
{
    public interface IAuthorizationRequirement
    {
        Task Authorize(AuthorizationContext context);
    }
}