﻿using System.Security.Claims;

namespace BIT.GraphQL.Authorization
{
    public interface IProvideClaimsPrincipal
    {
        ClaimsPrincipal User { get; }
    }
}