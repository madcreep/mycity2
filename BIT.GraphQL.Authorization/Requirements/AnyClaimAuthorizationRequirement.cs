using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIT.GraphQL.Authorization.Requirements
{
    public class AnyClaimAuthorizationRequirement : IAuthorizationRequirement
    {
        private readonly ClaimRequirement[] _claims;

        public AnyClaimAuthorizationRequirement(params ClaimRequirement[] claims)
        {
            _claims = claims ?? throw new ArgumentNullException(nameof(claims));
        }

        public Task Authorize(AuthorizationContext context)
        {
            if (_claims == null || !_claims.Any())
            {
                return Task.CompletedTask;
            }

            var found = true;

            foreach (var claim in _claims)
            {
                if (string.IsNullOrWhiteSpace(claim.Type))
                {
                    found = true;

                    continue;
                }

                found = context.User.Claims.Any(
                    c => string.Equals(c.Type, claim.Type, StringComparison.OrdinalIgnoreCase)
                         && (claim.EqualityFunction?.Invoke(c.Value)
                             ?? claim.Values == null || !claim.Values.Any() || claim.Values.Contains(c.Value))
                         );

                if (found)
                {
                    break;
                }
            }

            var count = 0;

            if (!found)
            {
                var sb = new StringBuilder(512);
                sb.Append("Требующиеся клаймы");
                foreach (var claim in _claims)
                {
                    if (count > 0)
                    {
                        sb.Append(" или ");
                    }

                    if (string.IsNullOrWhiteSpace(claim.Type))
                    {
                        continue;
                    }

                    if (claim.EqualityFunction != null)
                    {
                        sb.Append($" {claim.NotEqualityMessage}");
                    }
                    else if(claim.Values == null || !claim.Values.Any())
                    {
                        sb.Append($" '{claim.Type}'");
                    }
                    else
                    {
                        var values = string.Join(", ", claim.Values);

                        sb.Append($" '{claim.Type}' с одним из значений '{values}'");
                    }

                    count++;
                }

                sb.Append(" отсутствуют.");

                context.ReportError(sb.ToString());
            }

            return Task.CompletedTask;
        }
    }

    public class ClaimRequirement
    {
        internal string Type { get; }

        internal string[] Values { get; }

        internal Func<string, bool> EqualityFunction { get; }

        internal string NotEqualityMessage { get; }

        private ClaimRequirement(string claimType, string notEqualityMessage, Func<string, bool> equalityFunction, string[] values)
        {
            if (string.IsNullOrWhiteSpace(claimType))
            {
                throw new ArgumentNullException(nameof(claimType));
            }

            Type = claimType;
            NotEqualityMessage = notEqualityMessage;
            EqualityFunction = equalityFunction;
            Values = values;
        }

        public ClaimRequirement(string claimType, params string[] values)
            : this(claimType, null, null,values)
        { }

        public ClaimRequirement(string claimType, string notEqualityMessage, Func<string, bool> equalityFunction)
            : this(claimType, notEqualityMessage, equalityFunction, null)
        { }
    }
}
